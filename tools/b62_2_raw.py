import sys
import json
import base64
from PIL import Image
import numpy as np
from pathlib import Path

if __name__ == "__main__":
	fname_in = sys.argv[1]
	fpath_in = Path(fname_in)
	fname_out = sys.argv[2]
	fpath_out = Path(fname_out)
	if fpath_in.is_file():
		f_in = open(fpath_in, "r")
		encoded_content = f_in.read()
		f_in.close()
		raw_content = base64.b64decode(encoded_content)
		array = np.frombuffer(raw_content, dtype=np.uint8)
		array.resize(1460,1920)
		new_image = Image.fromarray(array)
		new_image.save(fpath_out)
	else:
		print('File does not exist');
