import time
import paho.mqtt.client as mqtt #import the client1
import paho.mqtt.publish as publish
from settings import BROKER_ADDRESS
from settings import CLIENT2
from settings import CLIENT3
from tip_types import TIP_TYPE1

def send_command(command_payload):
	publish.single(topic="tbd/machine/command", payload=command_payload, qos=2, retain=False, hostname=BROKER_ADDRESS, port=1883, client_id="mypub1", keepalive=60)
	time.sleep(1) # wait

# Register as master...
# Should succeed:
send_command('{ "message_type": "register_as_master_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')

# Should succeed and answer "true":
send_command('{ "message_type": "does_master_client_exist", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')
'''
# Should return '{ "hardware_event_log" : [{"timestamp": ##########, "user_id": 0, "event_type": 0, "action": 0, "description": "hardware event log opened"}] }'
send_command('{ "message_type": "get_hardware_event_log", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# Should succeed but send no status.
send_command('{ "message_type": "reset_hardware_event_log", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# Should return "[]"
send_command('{ "message_type": "get_hardware_event_log", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# Should return '{ "alarms_log" : "[{"timestamp": ##########, "user_id": 0, "alarm_type": 0, "action": 0, "description": "alarm log opened"}] }'
send_command('{ "message_type": "get_alarms_log", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# Should succeed but send no status.
send_command('{ "message_type": "reset_alarms_log", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# Should return "[]"
send_command('{ "message_type": "get_alarms_log", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# should return xx_nm = 33
send_command('{ "message_type": "get_current_xx_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# should return yy_nm = 44
send_command('{ "message_type": "get_current_yy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "seat_tip", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ', "tip_axis": "enumZAxis" } }')

send_command('{ "message_type": "get_tip_skew", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ', "tip_axis": "enumZAxis" } }')
'''
# Should succeed:
send_command('{ "message_type": "unregister_as_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')
