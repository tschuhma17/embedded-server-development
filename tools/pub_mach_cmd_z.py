import time
import paho.mqtt.client as mqtt #import the client1
from settings import BROKER_ADDRESS
client = mqtt.Client("mypub1") #create new instance
client.connect(BROKER_ADDRESS) #connect to broker
client.publish("tbd/machine/command",
'{ "message_type": "move_to_z_step", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "practice", "machine_id": "CX-2345", "payload": { "z_step": 7, } }')#publish
time.sleep(1) # wait
client.publish("tbd/machine/command",
'{ "message_type": "move_to_z_relative_step", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "practice", "machine_id": "CX-2345", "payload": { "z_offset_step": -66, } }')#publish
time.sleep(1) # wait
client.publish("tbd/machine/command",
'{ "message_type": "get_current_z_step", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "practice", "machine_id": "CX-2345", "payload": {  } }')#publish
time.sleep(1) # wait
client.publish("tbd/machine/command",
'{ "message_type": "move_to_z_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "practice", "machine_id": "CX-2345", "payload": { "z_nm": 777, } }')#publish
time.sleep(1) # wait
client.publish("tbd/machine/command",
'{ "message_type": "move_to_z_relative_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "practice", "machine_id": "CX-2345", "payload": { "z_offset_nm": 545, } }')#publish
time.sleep(1) # wait
client.publish("tbd/machine/command",
'{ "message_type": "get_z_current_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "practice", "machine_id": "CX-2345", "payload": {  } }')#publish
time.sleep(1) # wait
