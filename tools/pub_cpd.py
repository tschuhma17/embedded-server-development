import sys
import time
import json
import paho.mqtt.client as mqtt #import the client1
from settings import BROKER_ADDRESS
from settings import CLIENT2
from pathlib import Path

# Example usage: python pub_cpd.py XYAxes.cellx_cpd 7 enumXYAxes
# Example usage: python pub_cpd.py /media/psf/Home/Desktop/wei/cpds/Camera.cellx_cpd 5 enumCamera

def upload_this(client, fname, cpd_id, enumComponentType):
	# Call upload_cpd
	cpd_body = Path(fname).read_text('utf-8')
	channel = 'tbd/machine/command'
	payload = '{ "cpd_id": "' + str(cpd_id) + '", "cpd_body": ' + cpd_body + ' }'
	message = '{ "message_type": "upload_cpd", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": ' + payload + ' }'
	client.publish(channel, message) # publish 


def enum_to_get_comps_command_name(enumComponentType):
	if enumComponentType == "enumCxD":
		return "get_cxd_properties"
	if enumComponentType == "enumXYAxes":
		return "get_xy_properties"
	if enumComponentType == "enumXXAxis":
		return "get_xx_properties"
	if enumComponentType == "enumYYAxis":
		return "get_yy_properties"
	if enumComponentType == "enumZAxis":
		return "get_z_properties"
	if enumComponentType == "enumZZAxis":
		return "get_zz_properties"
	if enumComponentType == "enumZZZAxis":
		return "get_zzz_properties"
	if enumComponentType == "enumAspirationPump1":
		return "get_aspiration_pump_properties"
	if enumComponentType == "enumDispensePump1":
		return "get_dispense_pump_1_properties"
	if enumComponentType == "enumDispensePump2":
		return "get_dispense_pump_2_properties"
	if enumComponentType == "enumDispensePump3":
		return "get_dispense_pump_3_properties"
	if enumComponentType == "enumPickingPump1":
		return "get_picking_pump_properties"
	if enumComponentType == "enumWhiteLightSource":
		return "get_white_light_source_properties"
	if enumComponentType == "enumFluorescenceLightFilter":
		return "get_fluorescence_light_filter_properties"
	if enumComponentType == "enumAnnularRingHolder":
		return "get_annular_ring_holder_properties"
	if enumComponentType == "enumCamera":
		return "get_camera_properties"
	if enumComponentType == "enumOpticalColumn":
		return "get_optical_column_properties"
	if enumComponentType == "enumIncubator":
		return "get_incubator_properties"
	if enumComponentType == "enumCxPM":
		return "get_cxpm_properties"
	if enumComponentType == "enumBarcodeReader":
		return "get_barcode_reader_properties"
	return ""


def upload_and_apply_this(client, fname, cpd_id, enumComponentType):
	# Call upload_cpd
	cpd_body = Path(fname).read_text('utf-8')
	channel = 'tbd/machine/command'
	payload = '{ "cpd_id": "' + str(cpd_id) + '", "cpd_body": ' + cpd_body + ' }'
	message = '{ "message_type": "upload_cpd", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": ' + payload + ' }'
	client.publish(channel, message)

	# Get component properties -- before they get updated from CPD.
	command = enum_to_get_comps_command_name(enumComponentType)
	fetch_message = '{ "message_type": "' + command + '", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }'
	client.publish(channel, fetch_message)

	# Call apply_cpd_to_component
	payload = '{ "cpd_id": "' + str(cpd_id) + '", "component_type": "' + enumComponentType + '" }'
	message = '{ "message_type": "apply_cpd_to_component", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": ' + payload + ' }'
	print(message)
	client.publish(channel, message)

	# Call get_component_properties -- after they were updated from CPD.
	client.publish(channel, fetch_message)


def upload_and_ut_1(client, fname, cpd_id, enumComponentType):
	# Call upload_cpd
	cpd_body = Path(fname).read_text('utf-8')
	channel = 'tbd/machine/command'
	payload = '{ "cpd_id": "' + str(cpd_id) + '", "cpd_body": ' + cpd_body + ' }'
	message = '{ "message_type": "upload_cpd", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": ' + payload + ' }'
	client.publish(channel, message) # publish 

	# Call upload_cpd -- sending different cpd_id
	cpd_body = Path(fname).read_text('utf-8')
	channel = 'tbd/machine/command'
	payload = '{ "cpd_id": "' + str(cpd_id + 2) + '", "cpd_body": ' + cpd_body + ' }'
	message = '{ "message_type": "upload_cpd", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": ' + payload + ' }'
	client.publish(channel, message) # publish 
	
	msgGetCpdList = {
		"message_type": "get_cpd_list_from_type",
		"transport_version": "1.0",
		"command_group_id": 234,
		"command_id": 1234,
		"session_id": CLIENT2,
		"machine_id": "CX-2345",
		"payload": { "cpd_type": enumComponentType }
	}
	client.publish(channel, json.dumps(msgGetCpdList))

	msgGetCpdContents = {
		"message_type": "get_cpd_contents",
		"transport_version": "1.0",
		"command_group_id": 234,
		"command_id": 1234,
		"session_id": CLIENT2,
		"machine_id": "CX-2345",
		"payload": { "cpd_id": cpd_id, "component_type": enumComponentType }
	}
	client.publish(channel, json.dumps(msgGetCpdContents))
	time.sleep(1)

	msgDeleteCpd1 = {
		"message_type": "delete_cpd",
		"transport_version": "1.0",
		"command_group_id": 234,
		"command_id": 1234,
		"session_id": CLIENT2,
		"machine_id": "CX-2345",
		"payload": { "cpd_id": cpd_id, "component_type": enumComponentType }
	}
	client.publish(channel, json.dumps(msgDeleteCpd1))

	msgDeleteCpd2 = {
		"message_type": "delete_cpd",
		"transport_version": "1.0",
		"command_group_id": 234,
		"command_id": 1234,
		"session_id": CLIENT2,
		"machine_id": "CX-2345",
		"payload": { "cpd_id": (cpd_id + 2), "component_type": enumComponentType }
	}
	client.publish(channel, json.dumps(msgDeleteCpd2))


if __name__ == "__main__":
	fname = sys.argv[1]
	cpdId = int(sys.argv[2])
	enumComponentType = sys.argv[3]
	fpath = Path(fname)

	client = mqtt.Client("mypub1") #create new instance
	client.connect(BROKER_ADDRESS) #connect to broker
	client.publish("tbd/machine/command", '{ "message_type": "register_as_master_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')#publish

	if fpath.is_file():
		# upload_this(client, fname, cpdId, enumComponentType)
		upload_and_apply_this(client, fname, cpdId, enumComponentType)
		# upload_and_ut_1(client, fname, cpdId, enumComponentType)
	else:
		print('File does not exist');

	client.publish("tbd/machine/command", '{ "message_type": "unregister_as_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')#publish
