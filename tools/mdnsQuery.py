from zeroconf import ServiceBrowser, Zeroconf
import time

class MyListener:

    def remove_service(self, zeroconf, type, name):
        print("Service %s removed" % (name,))

    def add_service(self, zeroconf, type, name):
        info = zeroconf.get_service_info(type, name)
        print("Service %s added, service info: %s" % (name, info))
        print("IP Address: " + str(info.parsed_addresses()))

    def update_service(self,zeroconf, type, name):
        info = zeroconf.get_service_info(type, name)
        print("Service %s updated, service info: %s" % (name, info))
        print("IP Address: " + str(info.parsed_addresses()))


zeroconf = Zeroconf()
listener = MyListener()
browser = ServiceBrowser(zeroconf, "_cellx-es._tcp.local.", listener) 
try:
    input("Press enter to exit...\n\n")
finally:
    zeroconf.close()