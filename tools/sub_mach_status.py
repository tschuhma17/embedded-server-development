import paho.mqtt.client as mqtt #import the client1
from datetime import datetime
import time
import json
import base64
from PIL import Image
import numpy as np
from settings import BROKER_ADDRESS
############
def save_to_file(array_image, message_payload_payload, height, width):
	# print("array_image size", array_image.size)
	# print("array_image shape", array_image.shape)
	# print("array_image dimensions", array_image.ndim)
	array_image.resize(height,width)
	picture = Image.fromarray(array_image)
	dt = datetime.now()
	print(dt)
	filename = f'pic_{dt.timestamp()}.tiff' # Unique file name in case this is part of a series.
	if "x_index" in message_payload_payload:
		x = message_payload_payload['x_index']
		if "y_index" in message_payload_payload:
			y = message_payload_payload['y_index']
			filename = f'pic_x{x}_y{y}.tiff' # A file name for parts of a montage.
			# filename = f'pic.tiff' # Keep overwriting the file for vary large montages.
	picture.save(filename)
	# picture.save('pic.tiff')
	print("Please see ", filename)

def on_message(client, userdata, message):
	message_payload = json.loads(message.payload.decode("utf-8"))
	message_type = message_payload['message_type']
	if message_type == "status" or message_type == "get_camera_image":
		print("message received is ", message_type)
		if "payload" in message_payload:
			message_payload_payload = message_payload['payload']
			if message_payload_payload is None:
				print("payload is NULL")
			else:
				if "image" in message_payload_payload:
					b64_image = message_payload_payload['image']
					raw_image = base64.b64decode(b64_image)
					array_size = len(raw_image)
					# print("array_size is ", array_size)
					if array_size == 2803200:
						# print("array_image is most likely 1920 x 1460")
						array_image = np.frombuffer(raw_image, dtype=np.uint8)
						save_to_file(array_image, message_payload_payload, 1460, 1920)
					elif array_size == 700800:
						# print("array_image is most likely 960 x 730")
						array_image = np.frombuffer(raw_image, dtype=np.uint8)
						save_to_file(array_image, message_payload_payload, 730, 960)
					elif array_size == 5606400:
						# print("array_image may be 16 bits per pixel")
						array_image = np.frombuffer(raw_image, dtype=np.uint16)
						save_to_file(array_image, message_payload_payload, 1460, 1920)
					else:
						print("array_image is ", array_size, " bytes. Can't tell what it is.")
						dt = datetime.now()
						filename = f'pic_{dt.timestamp()}.raw' # Unique file name in case this is part of a series.
						with open(filename, 'wb') as raw_file:
							raw_file.write(raw_image)
				# time.sleep(2)
	else:
		print("message received " ,str(message.payload.decode("utf-8")))
	print("message topic=", message.topic, "  qos=", message.qos, "  retain flag=", message.retain)
########################################
print("creating new instance")
client = mqtt.Client(client_id="myclient3", clean_session=False) #create new instance
client.on_message=on_message #attach function to callback
print("connecting to broker")
client.connect(BROKER_ADDRESS) #connect to broker
client.loop_start() #start the loop
print("Subscribing to topic","tbd/machine/status")
client.subscribe("tbd/machine/status", qos=2)
time.sleep(5000) # wait
client.loop_stop() #stop the loop
