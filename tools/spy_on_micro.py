import time
import json
import paho.mqtt.client as mqtt #import the client1
import paho.mqtt.publish as publish
from settings import BROKER_ADDRESS
from settings import CLIENT3
TOPIC_COMMAND="tbd/microscope/command"
TOPIC_ACK="tbd/microscope/ack"
TOPIC_STATUS="tbd/microscope/status"
COMMAND_GROUP_ID=234
MACHINE_ID="CX-2345"
g_client = mqtt.Client(CLIENT3) #create new instance

def on_message(client, userdata, message):
	des_payload = str(message.payload.decode("utf-8"))
	topic_in = bytes.decode(message._topic)
	if (topic_in == TOPIC_ACK):
		print("RECEIVED ACK   : ", des_payload)
	if (topic_in == TOPIC_STATUS):
		print("RECEIVED STATUS: ", des_payload)
	if (topic_in == TOPIC_COMMAND):
		print("RECEIVED COMMAND: ", des_payload)

def spy_on_microscope_channels():
	g_client.on_message=on_message #attach function to callback

	print("connecting to broker")
	g_client.connect(BROKER_ADDRESS) #connect to broker
	print("Subscribing to topics")
	g_client.subscribe([(TOPIC_ACK, 0), (TOPIC_STATUS, 0), (TOPIC_COMMAND, 0)])
	while True:
		g_client.loop()

spy_on_microscope_channels()
