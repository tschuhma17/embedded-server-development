import time
import json
import paho.mqtt.client as mqtt #import the client1
import paho.mqtt.publish as publish
from settings import BROKER_ADDRESS
from settings import CLIENT2
from tip_types import TIP_TYPE1
from plate_types import PLATE_TYPE1
from plate_types import PLATE_TYPE2

def send_command(command_payload):
	publish.single(topic="tbd/machine/command", payload=command_payload, qos=2, retain=False, hostname=BROKER_ADDRESS, port=1883, client_id="mypub1", keepalive=60)

# time.sleep(1) # wait

# Register as master...
# Should succeed:
send_command('{ "message_type": "register_as_master_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')
'''
send_command('{ "message_type": "add_plate_to_deck_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_id": "12", "primary_location": "enumNP", "sub_location": "1", "sub_sub_location": "1" } }')

send_command('{ "message_type": "add_plate_to_deck_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_id": "34", "primary_location": "enumNP", "sub_location": "1", "sub_sub_location": "2" } }')

send_command('{ "message_type": "add_plate_to_deck_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_id": "56", "primary_location": "enumNP", "sub_location": "2", "sub_sub_location": "3" } }')

send_command('{ "message_type": "add_plate_to_deck_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_id": "77", "primary_location": "enumNP", "sub_location": "2", "sub_sub_location": "" } }')

send_command('{ "message_type": "add_plate_to_deck_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_id": "88", "primary_location": "enumTS", "sub_location": "1", "sub_sub_location": "" } }')

# should return location "enumNP", "2", "7"
send_command('{ "message_type": "find_plate_location_in_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_id": "77" } }')

send_command('{ "message_type": "remove_plate_from_deck_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_id": "77" } }')

# Expect status 4043 "barcode not found"
send_command('{ "message_type": "find_plate_location_in_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_id": "77" } }')

send_command('{ "message_type": "move_plate_in_deck_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_id": "56", "primary_location": "enumNP", "sub_location": "2", "sub_sub_location": "1" } }')

send_command('{ "message_type": "get_plate_list_in_location", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "primary_location": "enumNP", "sub_location": "1", "sub_sub_location": "" } }')

send_command('{ "message_type": "get_plate_list_in_location", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "primary_location": "enumNP", "sub_location": "", "sub_sub_location": "1" } }')

send_command('{ "message_type": "get_plate_list_in_location", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "primary_location": "", "sub_location": "", "sub_sub_location": "" } }')
'''
send_command('{ "message_type": "add_tip_rack_to_deck_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "tip_rack_id": "22", "used_tip_count": 2, "primary_location": "enumVTS", "sub_location": "1", "sub_sub_location": "1" } }')
'''
send_command('{ "message_type": "add_tip_rack_to_deck_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "tip_rack_id": "33", "used_tip_count": 3, "primary_location": "enumVTS", "sub_location": "2", "sub_sub_location": "2" } }')

send_command('{ "message_type": "add_tip_rack_to_deck_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "tip_rack_id": "44", "used_tip_count": 4, "primary_location": "enumVTS", "sub_location": "2", "sub_sub_location": "3" } }')

send_command('{ "message_type": "move_tip_rack_in_deck_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "tip_rack_id": "44", "primary_location": "enumVTS", "sub_location": "2", "sub_sub_location": "1" } }')

send_command('{ "message_type": "get_tip_rack_list_in_location", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "primary_location": "enumVTS", "sub_location": "1", "sub_sub_location": "" } }')

send_command('{ "message_type": "get_tip_rack_list_in_location", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "primary_location": "enumVTS", "sub_location": "2", "sub_sub_location": "" } }')

send_command('{ "message_type": "find_tip_rack_location_in_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "tip_rack_id": "44" } }')

send_command('{ "message_type": "move_tip_rack", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ', "tip_rack_id": "44", "from_primary_location": "enumVTS", "from_sub_location": "2", "from_sub_sub_location": "1", "to_primary_location": "enumVTS", "to_sub_location": "2", "to_sub_sub_location": "1" } }')

send_command('{ "message_type": "get_tip_rack_list_in_location", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "primary_location": "enumVTS", "sub_location": "", "sub_sub_location": "1" } }')

send_command('{ "message_type": "get_tip_count_for_tip_rack", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "tip_rack_id": "44" } }')

send_command('{ "message_type": "set_tip_count_for_tip_rack", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "tip_rack_id": "44", "used_tip_count": 5 } }')

send_command('{ "message_type": "remove_tip_rack_from_deck_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "tip_rack_id": "22" } }')

# Should print the payload to stdout and return empty status
msgMovePlate1 = {
	"message_type": "move_plate",
	"transport_version": "1.0",
	"command_group_id": 234,
	"command_id": 1234,
	"session_id": CLIENT2,
	"machine_id": "CX-2345",
	"payload": {
		"plate_type": PLATE_TYPE2,
		"plate_id": "77",
		"from_primary_location": "enumNP",
		"from_sub_location": "1",
		"from_sub_sub_location": "2",
		"to_primary_location": "enumCxDSite",
		"to_sub_location": "1",
		"to_sub_sub_location": "1"
	}
}
send_command(json.dumps(msgMovePlate1))

# Should print the payload to stdou and return "read_barcode": "Barsoom"
msgMovePlate2 = {
	"message_type": "move_plate_read_barcode",
	"transport_version": "1.0",
	"command_group_id": 234,
	"command_id": 1234,
	"session_id": CLIENT2,
	"machine_id": "CX-2345",
	"payload": {
		"plate_type": PLATE_TYPE2,
		"plate_id": "77",
		"from_primary_location": "enumNP",
		"from_sub_location": "1",
		"from_sub_sub_location": "3",
		"to_primary_location": "enumCxDSite1",
		"to_sub_location": "1",
		"to_sub_sub_location": "2"
	}
}
send_command(json.dumps(msgMovePlate2))
'''
# Unregister.
# Should succeed:
send_command('{ "message_type": "unregister_as_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')

