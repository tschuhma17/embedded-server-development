import paho.mqtt.client as mqtt #import the client1
from settings import BROKER_ADDRESS
client = mqtt.Client("mypub1") #create new instance
client.connect(BROKER_ADDRESS) #connect to broker
client.publish("tbd/machine/command",
'{ "message_type": "get_tip", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "practice", "machine_id": "CX-2345", "payload": { "tip_type": 0, "tip_axis": "enumZAxis", "tip_number": 1 } }')#publish
