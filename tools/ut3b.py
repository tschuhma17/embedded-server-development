import time
import paho.mqtt.client as mqtt #import the client1
from settings import BROKER_ADDRESS
from settings import CLIENT2
from settings import CLIENT3
client = mqtt.Client("mypub1") #create new instance
client.connect(BROKER_ADDRESS) #connect to broker

# Register as master...
# Should succeed:
client.publish("tbd/machine/command",
'{ "message_type": "register_as_master_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')#publish
time.sleep(1) # wait

# Should succeed and answer "true":
client.publish("tbd/machine/command",
'{ "message_type": "does_master_client_exist", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')#publish
time.sleep(1) # wait

# should return empty payload
client.publish("tbd/machine/command",
'{ "message_type": "move_to_yy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "y_nm": 2000 } }')#publish
time.sleep(1) # wait

# should return empty payload
client.publish("tbd/machine/command",
'{ "message_type": "move_to_zz_relative_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "zz_offset_nm": 33 } }')#publish
time.sleep(1) # wait

# should return empty payload
client.publish("tbd/machine/command",
'{ "message_type": "move_to_zzz_relative_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "zzz_offset_nm": -333 } }')#publish
time.sleep(1) # wait

# should return empty payload
client.publish("tbd/machine/command",
'{ "message_type": "dispense_3_flow_rate_volume", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "flow_rate_ul_per_s": 2.424, "volume_ul": 4.242 } }')#publish
time.sleep(1) # wait

# should return empty payload
client.publish("tbd/machine/command",
'{ "message_type": "picking_pump_flow_rate_volume", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "flow_rate_ul_per_s": 3.737, "volume_ul": 7.373 } }')#publish
time.sleep(1) # wait

# Should succeed:
client.publish("tbd/machine/command",
'{ "message_type": "unregister_as_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')#publish
time.sleep(1) # wait
