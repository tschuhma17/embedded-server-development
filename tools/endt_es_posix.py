import time
from datetime import datetime
import json
import base64
from PIL import Image
import numpy as np
import paho.mqtt.client as mqtt #import the client1
import paho.mqtt.publish as publish
# BROKER_ADDRESS="192.168.100.240"
from settings import BROKER_ADDRESS
from settings import CLIENT2
from settings import CLIENT3
from tip_types import TIP_TYPE2
from plate_types import PLATE_TYPE2
TOPIC_COMMAND="tbd/machine/command"
TOPIC_ACK="tbd/machine/ack"
TOPIC_STATUS="tbd/machine/status"
COMMAND_GROUP_ID=234
MACHINE_ID="CX-2345"
FW_VERSION="0.1.29"
DURATION_IN_MIN=1

shoebox = {
	'ack' : {
		'received': False,
		'expected_status_code' : 200
	},
	'status' : {
		'received': False,
		'expected_status_code' : 200
	},
	'skipping' : False,
	'loop_count' : 0,
	'message_type' : ""
}

def save_to_file(array_image, message_payload_payload, height, width, filename):
	array_image.resize(height,width)
	picture = Image.fromarray(array_image)
	# print("writing image...")
	picture.save(filename)

g_client = mqtt.Client(CLIENT2) #create new instance

# send_command()
# Uses client to send the prepared command_payload to the TOPIC_COMMAND.
# The on_message command handles received messages. We expect to receive an ack
# and a status. This command waits until both are received to exit.
# userdata is an object shared with g_client so send_command() and on_message()
# so both have access to this shared object. (It is shoebox.)
def send_command(client, userdata, command_payload):
	command_as_string = json.dumps(command_payload)
	userdata['ack']['received'] = False
	userdata['status']['received'] = False
	if (userdata['status']['expected_status_code'] == 0):
		userdata['status']['received'] = True # expected_status_code of 0 means we don't expect a response, so don't wait for one.
	userdata['sent_cmd_id'] = command_payload["command_id"]
	userdata['passed'] = True;
	# print("SEND COMMAND   : ", command_as_string)
	client.publish(TOPIC_COMMAND, command_as_string)
	while (userdata['ack']['received'] == False) or (userdata['status']['received'] == False):
		g_client.loop()
	if (userdata['passed'] == True):
		print("passed: ", command_payload['message_type'], " ", str(userdata['loop_count']))
	else:
		print("FAILED: ", command_payload['message_type'], " ", str(userdata['loop_count']))


# on_message() is a handler function passed to g_client. It handles any message that
# is received. We only expect 2 kinds: acks and statuses returned in response to
# a command.
# For an ack, on_message() tests that the status_code matches the expected value.
# For a status, on_message() tests that both the status_code and the payload matches
# expected values. (Sometimes, bad status codes or payloads is expected.)
def on_message(client, userdata, message):
	# print("msg: ", str(message.payload.decode("utf-8")))
	des_payload = json.loads(message.payload.decode("utf-8"))
	message_type = des_payload['message_type']
	topic_in = bytes.decode(message._topic)
	sent_id = userdata['sent_cmd_id']
	received_id = des_payload['command_id']
	# print("message_type=", message_type, " topic_in=", topic_in, " sent_cmd_id=", sent_id)
	if (topic_in == TOPIC_ACK) and (sent_id == received_id):
		# print("RECEIVED ACK   : ", des_payload)
		# print("RECEIVED ACK")
		userdata['ack']['received'] = True
		if (des_payload["status_code"] != userdata['ack']['expected_status_code']):
			userdata['passed'] = False
			print("FAIL: ack status_code is wrong: ", des_payload["status_code"])
			userdata['status']['received'] = True # Not really, but we don't expect a response in this situation.
	elif (topic_in == TOPIC_STATUS) and (sent_id == received_id):
		# print("RECEIVED STATUS: ", des_payload)
		# print("RECEIVED STATUS")
		# print(message_type,"=?", userdata['message_type'])
		if (message_type == userdata['message_type']):
			userdata['status']['received'] = True
			if (des_payload["status_code"] != userdata['status']['expected_status_code']):
				userdata['passed'] = False
				print("FAIL: status's status_code is wrong: ", des_payload["status_code"])
		# Does the payload contain an image?
		if ((userdata['message_type'] == "get_camera_image") or (userdata['message_type'] == "collect_montage_images")):
			userdata['passed'] = payloads_match(des_payload['payload'], userdata['status']['expected_payload'])
			# print("result =", userdata['passed'])
			if "payload" in des_payload:
				message_payload_payload = des_payload['payload']
				if message_payload_payload is None:
					print("payload is NULL")
				else:
					if "image" in message_payload_payload:
						# Yes. It contains an image
						filename = f'picture.tiff'
						if (userdata['message_type'] == "get_camera_image"):
							# dt = datetime.now()
							# filename = f'pic_{dt.timestamp()}.tiff' # A file name for a single camera image.
							filename = f'picture.tiff' # Keep overwriting this file.
						if (userdata['message_type'] == "collect_montage_images"):
							if "x_index" in message_payload_payload:
								x = message_payload_payload['x_index']
							else:
								x = "0"
							if "y_index" in message_payload_payload:
								y = message_payload_payload['y_index']
							else:
								y = "0"
							filename = f'pic_x{x}_y{y}.tiff' # A file name for parts of a montage.
						b64_image = message_payload_payload['image']
						raw_image = base64.b64decode(b64_image)
						array_size = len(raw_image)
						if array_size == 2803200:
							array_image = np.frombuffer(raw_image, dtype=np.uint8)
							save_to_file(array_image, message_payload_payload, 1460, 1920, filename)
						elif array_size == 700800:
							array_image = np.frombuffer(raw_image, dtype=np.uint8)
							save_to_file(array_image, message_payload_payload, 730, 960, filename)
						elif array_size == 5606400:
							array_image = np.frombuffer(raw_image, dtype=np.uint16)
							save_to_file(array_image, message_payload_payload, 1460, 1920, filename)
						elif array_size == 0:
							array_size = 0
						else:
							print("array_size=", array_size)
							print("Saving as .RAW file")
							dt = datetime.now()
							filename = f'pic_{dt.timestamp()}.raw' # Unique file name in case this is part of a series.
							with open(filename, 'wb') as raw_file:
								raw_file.write(raw_image)
	else:
		print("RECEIVED OTHER")

# payloads_match() compares two objects for likeness. These are meant to be payload
# objects, which are supposed to be dict objects or Nones.
# The received payload may have key/value pairs that we cannot anticipate or that
# we just don't care about. So this test is focused on checking that every key/value
# pair in the expected dict match the ones in the received dict.
def payloads_match(received, expected):
	if (received is None):
		if (expected is None):
			return True
		else:
			print(received)
			return False
	# Received is something.
	if (expected is None):
		print(received)
		return False
	# Both received and expected are somethings.
	expectations_list = sorted(expected.items())
	for k, v in expectations_list:
		if (k in received.keys()):
			if (type(v) == type(received[k])):
				if (v != received[k]):
					print(received)
					return False
			else:
				print(received)
				return False
		else:
			print(received)
			return False
	return True

def properties_match(received_properties, expected_properties):
	matches = True
	# print(expected_properties)
	for k, v in expected_properties.items():
		if k not in received_properties.keys():
			print("match not found for ", k)
			matches = False
		elif (type(v) != type(received_properties[k])):
			matches = False
		elif ((type(v) == list) and (type(expected_properties[k]) == list)):
			matches = (matches and lists_match(k, received_properties[k], expected_properties[k]))
		else:
			matches = (matches and property_matches(k, received_properties[k], expected_properties[k]))
	return matches

def lists_match(key, received_list, expected_list):
	ex_len = len(expected_list)
	matches = True
	if (ex_len != len(received_list)):
		return False
	for i in range(ex_len):
		matches = (matches and properties_match(received_list[i], expected_list[i]))
	return matches

def property_matches(key, received_property, expected_property):
	matches = True
	matches = (matches and field_matches(key, "data_type", received_property, expected_property))
	matches = (matches and field_matches(key, "base_unit", received_property, expected_property))
	matches = (matches and field_matches(key, "es_min_value", received_property, expected_property))
	matches = (matches and field_matches(key, "es_max_value", received_property, expected_property))
	matches = (matches and field_matches(key, "setting", received_property, expected_property))
	matches = (matches and field_matches(key, "scale_factor", received_property, expected_property))
	matches = (matches and field_matches(key, "persist_type", received_property, expected_property))
	matches = (matches and field_matches(key, "editability_type", received_property, expected_property))
	matches = (matches and field_matches(key, "points_past_decimal", received_property, expected_property))
	matches = (matches and field_matches(key, "display_directive", received_property, expected_property))
	matches = (matches and field_matches(key, "display_min_value", received_property, expected_property))
	matches = (matches and field_matches(key, "display_max_value", received_property, expected_property))
	return matches

def field_matches(prop_key, field_key, received_property, expected_property):
	matches = True
	exp_val = expected_property[field_key]
	if exp_val == "":
		exp_val = None  # TODO: Confirm that it is OK equate "" with null.
	if exp_val == 0:
		exp_val = None
	rec_val = received_property[field_key]
	if rec_val == "":
		rec_val = None  # TODO: Confirm that it is OK equate "" with null.
	if rec_val == 0:
		rec_val = None
	if (exp_val != rec_val):
		print(prop_key, ":", field_key, " value [", expected_property[field_key], "] doesn't match [", received_property[field_key], "]", sep='')
		matches = False
	return matches


# Trying to be terse here, to pack a lot in. Here is a legend:
# 'mtp' is 'message_type'. 'pld' is the command's outgoing payload. 'ast' is the expected ack status_code. 'sst' is the expected status status_code.
# 'expld' is the payload expected to be returned in the status message. This should only contain the parts of the payload that can be known in advance.
setup_command_list = [
	{'mtp': 'register_as_master_client', 'pld': { 'requesting_client_guid': CLIENT2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': {'registration_successful': True}},
]
test_command_list = [
	# {'mtp': "collect_montage_images", 'pld': { "plate_type": PLATE_TYPE2, "plate_position": 1, "well": "B2", "x_start_offset_nm": 0, "x_step_count": 22, "x_step_increment_nm": 2500000, "y_start_offset_nm": 0, "y_step_count": 17, "y_step_increment_nm": 2700000, "scan_movement_delay_ms": 22.2 } , 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': "collect_montage_images", 'pld': { "plate_type": PLATE_TYPE2, "plate_position": 1, "well": "B2", "x_start_offset_nm": 0, "x_step_count": 2, "x_step_increment_nm": 2500000, "y_start_offset_nm": 0, "y_step_count": 2, "y_step_increment_nm": 2700000, "scan_movement_delay_ms": 22.2 } , 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_tip', 'pld':  { "tip_type": TIP_TYPE2, "tip_axis": "enumZAxis", "tip_number": 1 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_xx_nm', 'pld': { "xx_nm": 211000000 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_yy_nm', 'pld': { "yy_nm": 185000000 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_tip', 'pld':  { "tip_type": TIP_TYPE2, "tip_axis": "enumZZAxis", "tip_number": 1 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	#{'mtp': 'move_to_yy_nm', 'pld': { "yy_nm": 10000000 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_camera_image', 'pld': None, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'delay', 'pld': { 'seconds': 2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'move_to_xy_nm', 'pld': { "x_nm": 0, "y_nm": 0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'move_to_xy_nm', 'pld': { "x_nm": 0, "y_nm": 100000 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'move_to_xy_nm', 'pld': { "x_nm": 2000000, "y_nm": 150000 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'move_to_xy_nm', 'pld': { "x_nm": 2000000, "y_nm": 0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_current_xy_nm', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "x_nm": 15500, "y_nm": 16500 }},
	# {'mtp': 'is_system_booting', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "is_system_booting": True }},
]
teardown_command_list = [
	{'mtp': 'unregister_as_client', 'pld': { 'requesting_client_guid': CLIENT2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': {'unregistration_successful': True}}
]

def endurance_test_one(command_id, details):
	commandObj = {
		"message_type": details['mtp'],
		"transport_version": "1.0",
		"command_group_id": COMMAND_GROUP_ID,
		"command_id": command_id,
		"session_id": details['cid'],
		"machine_id": MACHINE_ID,
		"payload": details['pld']
	}
	shoebox['ack']['expected_status_code'] = details['ast']
	shoebox['status']['expected_status_code'] = details['sst']
	shoebox['sent_cmd_id'] : command_id
	shoebox['status']['expected_payload'] = details['expld']
	shoebox['message_type'] = details['mtp']
	
	if (details['mtp'] == 'start_skipping'):
		shoebox['skipping'] = True
	elif (details['mtp'] == 'stop_skipping'):
		shoebox['skipping'] = False
	elif (details['mtp'] == 'delay'):
		payload = details['pld']
		time_s = payload['seconds']
		time.sleep(time_s)
	else:
		if (shoebox['skipping'] == False):
			send_command(g_client, shoebox, commandObj)

def endurance_test_all():
	g_client.on_message=on_message #attach function to callback
	g_client.user_data_set(shoebox)

	print("connecting to broker")
	g_client.connect(host=BROKER_ADDRESS, keepalive=600) #connect to broker
	print("Subscribing to topic","tbd/microscope/command")
	g_client.subscribe([(TOPIC_ACK, 0), (TOPIC_STATUS, 0)])

	start_time = time.time()
	run_duration_time = DURATION_IN_MIN * 60 # runtime in minutes
	COMMAND=100
	for i in setup_command_list:
		endurance_test_one(COMMAND, i)
		COMMAND +=1
	while ((time.time() - start_time) < run_duration_time):
		for i in test_command_list:
			endurance_test_one(COMMAND, i)
			COMMAND +=1
		shoebox['loop_count'] = shoebox['loop_count'] + 1
	for i in teardown_command_list:
		endurance_test_one(COMMAND, i)
		COMMAND +=1


endurance_test_all()
