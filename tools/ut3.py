import time
import json
import paho.mqtt.client as mqtt #import the client1
import paho.mqtt.publish as publish
from settings import BROKER_ADDRESS
from settings import CLIENT2
from settings import CLIENT3

def send_command(command_payload):
	publish.single(topic="tbd/machine/command", payload=command_payload, qos=2, retain=False, hostname=BROKER_ADDRESS, port=1883, client_id="mypub1", keepalive=60)

# Should succeed and answer "false":
send_command('{ "message_type": "does_master_client_exist", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# Register as master...
# Should succeed:
send_command('{ "message_type": "register_as_master_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')
'''
# Should succeed and answer "true":
send_command('{ "message_type": "does_master_client_exist", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# should return empty payload
send_command('{ "message_type": "move_to_xx_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "xx_nm": 2030 } }')

# should return xx_nm = 2030
send_command('{ "message_type": "get_current_xx_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "move_to_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "x_nm": 0, "y_nm": 0 } }')

# should return empty payload
send_command('{ "message_type": "move_to_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "x_nm": 5000000, "y_nm": 7000000 } }')

send_command('{ "message_type": "move_to_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "x_nm": 00, "y_nm": -8000000 } }')

send_command('{ "message_type": "move_to_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "x_nm": 20000000, "y_nm": 0 } }')

send_command('{ "message_type": "move_to_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "x_nm": 0, "y_nm": 0 } }')

send_command('{ "message_type": "move_to_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "x_nm": 20000000, "y_nm": -20000000} }')

send_command('{ "message_type": "move_to_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "x_nm": 0, "y_nm": 20000000 } }')

send_command('{ "message_type": "move_to_xx_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "xx_nm": 22300000 } }')

send_command('{ "message_type": "move_to_xx_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "xx_nm": 1000000 } }')

send_command('{ "message_type": "move_to_yy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "yy_nm": 22300000 } }')

send_command('{ "message_type": "move_to_yy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "yy_nm": 1000000 } }')

send_command('{ "message_type": "get_current_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": null }')

# should return empty payload
send_command('{ "message_type": "move_to_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "x_nm": 50, "y_nm": 70 } }')

send_command('{ "message_type": "move_to_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "x_nm": 50, "y_nm": 71 } }')

send_command('{ "message_type": "get_current_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": null }')

# should return empty payload
send_command('{ "message_type": "move_to_yy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "yy_nm": 1230 } }')

# should return yy_nm = 1230 because of previous command
send_command('{ "message_type": "get_current_yy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "move_to_z_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "z_nm": 15432100 } }')

send_command('{ "message_type": "move_to_z_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "z_nm": 1000 } }')

# should return empty payload
send_command('{ "message_type": "move_to_zz_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "zz_nm": 54321 } }')

# should return empty payload
send_command('{ "message_type": "move_to_zz_relative_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "zz_offset_nm": 33 } }')

# should return zz_nm = 77
send_command('{ "message_type": "get_zz_current_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# should return empty payload
send_command('{ "message_type": "move_to_zzz_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "zzz_nm": 333 } }')

# should return empty payload
send_command('{ "message_type": "move_to_zzz_relative_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "zzz_offset_nm": -333 } }')
send_command('{ "message_type": "move_to_zzz_relative_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "zzz_offset_nm": 55 } }')

# should return zzz_nm = 777
send_command('{ "message_type": "get_current_zzz_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# should return x_trigger_location_nm = 555
send_command('{ "message_type": "move_x_to_tip_skew_trigger", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# should return y_trigger_location_nm = 444
send_command('{ "message_type": "move_y_to_tip_skew_trigger", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# should return empty payload
send_command('{ "message_type": "move_z_to_safe_height", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# should return empty payload
send_command('{ "message_type": "move_zz_to_safe_height", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# should return empty payload
send_command('{ "message_type": "move_zzz_to_safe_height", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')
'''
send_command('{ "message_type": "aspirate_zz_flow_rate_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "flow_rate_ul_per_s": 0.000, "time_s": 0.0 } }')

send_command('{ "message_type": "aspirate_zz_flow_rate_volume", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": false, "flow_rate_ul_per_s": 0.0, "volume_ul": 0.0 } }')

send_command('{ "message_type": "aspirate_zz_volume_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "volume_ul": 0.0, "time_s": 0.0 } }')
'''
# aspiration_pump section
send_command('{ "message_type": "aspirate_zz_flow_rate_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "flow_rate_ul_per_s": 300.000, "time_s": 2.0 } }')

send_command('{ "message_type": "aspirate_zz_flow_rate_volume", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": false, "flow_rate_ul_per_s": 300.0, "volume_ul": 600.0 } }')

send_command('{ "message_type": "aspirate_zz_volume_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "volume_ul": 600.0, "time_s": 2.0 } }')

send_command('{ "message_type": "is_aspiration_pump_running", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "aspiration_pump_start_stop", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "is_start_pump": true } }')

send_command('{ "message_type": "is_aspiration_pump_running", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "aspiration_pump_start_stop", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "is_start_pump": false } }')

send_command('{ "message_type": "is_aspiration_pump_running", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# dispense_pump_1 section
send_command('{ "message_type": "dispense_1_flow_rate_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "flow_rate_ul_per_s": 300.0, "time_s": 2.0 } }')

send_command('{ "message_type": "dispense_1_flow_rate_volume", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": false, "flow_rate_ul_per_s": 300.0, "volume_ul": 600.0 } }')

send_command('{ "message_type": "dispense_1_volume_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "volume_ul": 600.0, "time_s": 2.0 } }')

send_command('{ "message_type": "is_dispense_pump_1_running", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "dispense_pump_1_start_stop", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "is_start_pump": true } }')

send_command('{ "message_type": "is_dispense_pump_1_running", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "dispense_pump_1_start_stop", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "is_start_pump": false } }')

send_command('{ "message_type": "is_dispense_pump_1_running", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# dispense_pump_2 section
send_command('{ "message_type": "dispense_2_flow_rate_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "flow_rate_ul_per_s": 300.0, "time_s": 2.0 } }')

send_command('{ "message_type": "dispense_2_flow_rate_volume", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": false, "flow_rate_ul_per_s": 300.0, "volume_ul": 600.0 } }')

send_command('{ "message_type": "dispense_2_volume_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "volume_ul": 600.0, "time_s": 2.0} }')

send_command('{ "message_type": "is_dispense_pump_2_running", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "dispense_pump_2_start_stop", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "is_start_pump": true } }')
time.sleep(3)
send_command('{ "message_type": "is_dispense_pump_2_running", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "dispense_pump_2_start_stop", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "is_start_pump": false } }')

send_command('{ "message_type": "is_dispense_pump_2_running", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')
time.sleep(3)

# dispense_pump_3 section
send_command('{ "message_type": "dispense_3_flow_rate_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "flow_rate_ul_per_s": 150.0, "time_s": 2.0 } }')

send_command('{ "message_type": "dispense_3_flow_rate_volume", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": false, "flow_rate_ul_per_s": 150.0, "volume_ul": 300.0 } }')

send_command('{ "message_type": "dispense_3_volume_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "volume_ul": 300.0, "time_s": 2.0 } }')

send_command('{ "message_type": "is_dispense_pump_3_running", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "dispense_pump_3_start_stop", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "is_start_pump": true } }')
time.sleep(3)
send_command('{ "message_type": "is_dispense_pump_3_running", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "dispense_pump_3_start_stop", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_normal_direction": true, "is_start_pump": false } }')

send_command('{ "message_type": "is_dispense_pump_3_running", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')
time.sleep(3)

# should return empty payload
send_command('{ "message_type": "picking_pump_flow_rate_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "flow_rate_ul_per_s": 25.25, "time_s": 5.252 } }')

# should return empty payload
send_command('{ "message_type": "picking_pump_flow_rate_volume", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "flow_rate_ul_per_s": 3.737, "volume_ul": 7.373 } }')

# should return empty payload
send_command('{ "message_type": "picking_pump_volume_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "volume_ul": 4.848, "time_s": 8.484 } }')

# should ack = 404
send_command('{ "message_type": "set_white_light_source_on_off", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "is_turn_on": true } }')

# should ack = 404
send_command('{ "message_type": "is_white_light_source_on", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# should ack = 404
send_command('{ "message_type": "set_fluorescence_light_filter_on_off", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "filter_index": 0, "is_turn_on": true } }')

# should ack = 404
send_command('{ "message_type": "is_fluorescence_light_filter_on", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "filter_index": 1 } }')

# should return empty payload
send_command('{ "message_type": "move_to_focus_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "focus_nm": 35753 } }')

# should return empty payload
# Will error out if move_to_focus_nm not run prior.
send_command('{ "message_type": "move_to_focus_relative_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "focus_offset_nm": 225 } }')
send_command('{ "message_type": "move_to_focus_relative_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "focus_offset_nm": -125 } }')

# should return focus_nm = 99.86
send_command('{ "message_type": "get_current_focus_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# should return false
send_command('{ "message_type": "is_camera_image_available", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')
'''
# Should succeed:
send_command('{ "message_type": "unregister_as_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')
