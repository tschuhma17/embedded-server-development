import time
import json
import paho.mqtt.client as mqtt #import the client1
import paho.mqtt.publish as publish
from settings import BROKER_ADDRESS
from settings import CLIENT2
from settings import CLIENT3
from tip_types import TIP_TYPE1
from plate_types import PLATE_TYPE1
from plate_types import PLATE_TYPE2

def send_command(command_payload):
	publish.single(topic="tbd/machine/command", payload=command_payload, qos=2, retain=False, hostname=BROKER_ADDRESS, port=1883, client_id="mypub1", keepalive=60)
plate1_str = json.dumps(PLATE_TYPE1)
plate2_str = json.dumps(PLATE_TYPE2)
# is_system_booting

# Should succeed and answer "false":
send_command('{ "message_type": "does_master_client_exist", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# Register as master...
# Should succeed:
send_command('{ "message_type": "register_as_master_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')
'''
# Should succeed:
send_command('{ "message_type": "get_current_system_configuration", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# Should succeed:
send_command('{ "message_type": "synchronize_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "timestamp": ' + str(int(time.time())) + ' } }')

# Prep for get_plate_list_in_location
send_command('{ "message_type": "add_plate_to_deck_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_id": "12", "primary_location": "enumNP", "sub_location": "1", "sub_sub_location": "1" } }')

# Should succeed:
send_command('{ "message_type": "add_plate_to_deck_inventory", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_id": "34", "primary_location": "enumNP", "sub_location": "1", "sub_sub_location": "2" } }')

# Should return two plates:
send_command('{ "message_type": "get_plate_list_in_location", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "primary_location": "enumNP", "sub_location": "1", "sub_sub_location": "" } }')

# Should succeed:
send_command('{ "message_type": "get_firmware_version_number", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "who-am-i", "machine_id": "CX-2345", "payload": { } }')

# should return empty payload
send_command('{ "message_type": "move_to_focus_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "focus_nm": 3575 } }')

# should return focus_nm = 3575
send_command('{ "message_type": "get_current_focus_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# should return empty payload
send_command('{ "message_type": "move_to_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "x_nm": 1000, "y_nm": 2000 } }')

# should return status payload: { "x_nm": 1000, "y_nm": 2000 } because of previous command
send_command('{ "message_type": "get_current_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": null }')

# get_camera_properties
#send_command('{ "message_type": "get_camera_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# set_camera_properties
#send_command('{ "message_type": "set_camera_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "enumCamera" : { "gain": { "setting": 1 },  "exposure": { "setting": 500000 } } } }')

# get_camera_properties
#send_command('{ "message_type": "get_camera_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# set_camera_properties
send_command('{ "message_type": "set_camera_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "enumCamera" : { "gain": { "setting": 2 }, "exposure": { "setting": 700000 } } } }')

send_command('{ "message_type": "set_camera_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "enumCamera" : { "gain": { "data_type": null, "setting": 2, "display_directive": null, "display_min_value": null, "display_max_value": null } } } }')

send_command('{ "message_type": "set_camera_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "enumCamera" : { "exposure": { "editability_type": "enumEditableFixed", "persist_type": "enumNotPersist", "data_type": null, "display_directive": null, "display_min_value": null, "display_max_value": null, "setting": "20000" } } } }')

# send_command('{ "message_type": "set_camera_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "enumCamera" : { "vendor": null, "model": null, "version": null, "serial_number": null, "cpd_id": null, "pixel_count_x": null, "pixel_count_y": null, "pixel_size_x": null, "pixel_size_y": null, "sensor_height": null, "sensor_width": null, "camera_color_depth": null, "scaled_color_depth": null, "number_of_color_channels_used": null, } } }')

# send_command('{ "message_type": "set_camera_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "enumCamera" : { "gain": { "setting": 2 }, "exposure": { "setting": 700000 } } } }')

{
    "enumCamera": {
        "vendor": null,
        "model": null,
        "version": null,
        "serial_number": null,
        "cpd_id": null,
        "pixel_count_x": null,
        "pixel_count_y": null,
        "pixel_size_x": null,
        "pixel_size_y": null,
        "sensor_height": null,
        "sensor_width": null,
        "camera_color_depth": null,
        "scaled_color_depth": null,
        "number_of_color_channels_used": null,
        "gain": {
            "data_type": null,
            "setting": 2,
            "display_directive": null,
            "display_min_value": null,
            "display_max_value": null
        },
        "exposure": {
            "editability_type": "enumEditableFixed",
            "persist_type": "enumNotPersist",
            "data_type": null,
            "display_directive": null,
            "display_min_value": null,
            "display_max_value": null,
            "setting": "20000"
        },
        "bin_factor": {
            "data_type": null,
            "setting": 1,
            "display_directive": null,
            "display_min_value": null,
            "display_max_value": null
        },
        "calibration_type": null,
        "last_calibration_date": null,
        "calibration_coefficient_1": null,
        "calibration_coefficient_2": null,
        "calibration_coefficient_3": null
    }
}

 get_camera_properties
 send_command('{ "message_type": "get_camera_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')
'''
# get_camera_image
# Should return one .TIFF file.
send_command('{ "message_type": "get_camera_image", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": null }')
'''
#send_command('{ "message_type": "get_optical_column_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# set_optical_column_properties
send_command('{ "message_type": "set_optical_column_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "enumOpticalColumn" : { "active_turret_position": { "setting": "14", "editability_type": "enumEditableCPD_Application", "persist_type": "enumPersist", "data_type": "uint16_t", "es_min_value": "0", "es_max_value": "65535" } } } }')

# set_fluorescence_light_filter_properties
send_command('{ "message_type": "set_fluorescence_light_filter_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "enumFluorescenceLightFilter" : { "on_status": { "setting": TRUE, "editability_type": "enumEditableCPD_Application", "persist_type": "enumHardPersist", "data_type": "boolean", "es_min_value": NULL, "es_max_value": NULL }, "active_fluorescence_filter_position": { "setting": 4, "editability_type": "enumEditableCPD_Application", "persist_type": "enumHardPersist", "data_type": "int16_t", "es_min_value": -32768, "es_max_value": 32767 } } } }')

# set_annular_ring_holder_properties
send_command('{ "message_type": "set_annular_ring_holder_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "enumAnnularRingHolder" : { "Kei": { "setting": "21", "editability_type": "enumEditableCPD_Application", "persist_type": "enumPersist", "data_type": "uint32_t", "es_min_value": "1", "es_max_value": "91" }, "Yuri": { "setting": "19", "editability_type": "enumEditableApplicationOnly", "persist_type": "enumRemove", "data_type": "uint32_t", "es_min_value": "56", "es_max_value": "90" }, "Mughi": { "setting": "5", "editability_type": "enumEditableApplicationOnly", "persist_type": "enumPersist", "data_type": "uint32_t", "es_min_value": "1", "es_max_value": "1000" } } } }')

# move_XY_to_optical_eye_nm_A1_notation
plate1_str = json.dumps(PLATE_TYPE1)
send_command('{ "message_type": "move_XY_to_optical_eye_nm_A1_notation", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_type": ' + plate1_str + ', "plate_position": 1, "well": "B2", "x_offset_nm": 1020, "y_offset_nm": 2020 } }')

# set_white_light_source_properties
send_command('{ "message_type": "set_white_light_source_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "enumWhiteLightSource" : { "Kei": { "setting": "21", "editability_type": "enumEditableCPD_Application", "persist_type": "enumPersist", "data_type": "uint32_t", "es_min_value": "1", "es_max_value": "91" }, "Yuri": { "setting": "19", "editability_type": "enumEditableApplicationOnly", "persist_type": "enumRemove", "data_type": "uint32_t", "es_min_value": "56", "es_max_value": "90" }, "Mughi": { "setting": "5", "editability_type": "enumEditableApplicationOnly", "persist_type": "enumPersist", "data_type": "uint32_t", "es_min_value": "1", "es_max_value": "1000" } } } }')

send_command('{ "message_type": "move_to_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "x_nm": 10000000, "y_nm": 10000000 } }')

# collect_montage_images
# Should return 9 .TIFF files.

send_command('{ "message_type": "collect_montage_images", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_type": ' + plate2_str + ', "plate_position": 1, "well": "B2", "x_start_offset_nm": -100000, "x_step_count": 4, "x_step_increment_nm": 2500000, "y_start_offset_nm": 50000000, "y_step_count": 4, "y_step_increment_nm": 2700000, "scan_movement_delay_ms": 22.2 } }')

send_command('{ "message_type": "move_to_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "x_nm": 50000000, "y_nm": 60000000 } }')

send_command('{ "message_type": "move_to_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "x_nm": 100, "y_nm": 100 } }')

send_command('{ "message_type": "collect_montage_images", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_type": ' + plate2_str + ', "plate_position": 1, "well": "B2", "x_start_offset_nm": -100000, "x_step_count": 10, "x_step_increment_nm": 2500000, "y_start_offset_nm": 50000000, "y_step_count": 10, "y_step_increment_nm": 2700000, "scan_movement_delay_ms": 22.2 } }')
'''
# unregister_as_client
# Should succeed:
send_command('{ "message_type": "unregister_as_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')
