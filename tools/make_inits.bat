echo. > cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for CxD struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultCxD =
echo {
echo 	ComponentType::CxD,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\CxD.cellx_props" >> cppsrc\temp.cpp
echo 	{ "cxd.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/cxd.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for XX struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultAxisXX =
echo {
echo 	ComponentType::XXAxis,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\XX_Axis.cellx_props" >> cppsrc\temp.cpp
echo 	{ "axis_xx.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/axis_xx.cfg") }>> cppsrc\temp.cpp
echo };>>cppsrc\temp.cpp
echo.>>cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for YY struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultAxisYY =
echo {
echo 	ComponentType::YYAxis,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\YY_Axis.cellx_props" >> cppsrc\temp.cpp
echo 	{ "axis_yy.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/axis_yy.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for XY struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultAxesXY = 
echo {
echo 	ComponentType::XYAxes,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\XY_Axes.cellx_props" >> cppsrc\temp.cpp
echo 	{ "axis_xy.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/axis_xy.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for Z struct
echo /// each row:  name,  value,  editable,  persist,  dataType,  min,  max;
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultAxisZ = 
echo {
echo 	ComponentType::ZAxis,)  >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\Z_Axis.cellx_props" >> cppsrc\temp.cpp
echo 	{ "axis_z.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/axis_z.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for ZZ struct
echo /// each row:  name,  value,  editable,  persist,  dataType,  min,  max;
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultAxisZZ = 
echo {
echo 	ComponentType::ZZAxis,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\ZZ_Axis.cellx_props" >> cppsrc\temp.cpp
echo 	{ "axis_zz.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/axis_zz.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for ZZZ struct
echo /// each row:  name,  value,  editable,  persist,  dataType,  min,  max;
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultAxisZZZ = 
echo {
echo 	ComponentType::ZZZAxis,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\ZZZ_Axis.cellx_props" >> cppsrc\temp.cpp
echo 	{ "axis_zzz.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/axis_zzz.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for AspirationPump1 struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultAspirationPump1 = 
echo {
echo 	ComponentType::AspirationPump1,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\Aspiration_Pump_1.cellx_props" >> cppsrc\temp.cpp
echo 	{ "asp_pump.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/asp_pump.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for DispensePump1 struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultDispensePump1 = 
echo {
echo 	ComponentType::DispensePump1,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\Dispense_Pump_1.cellx_props" >> cppsrc\temp.cpp
echo 	{ "disp_pump1.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/disp_pump1.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for DispensePump2 struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultDispensePump2 = 
echo {
echo 	ComponentType::DispensePump2,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\Dispense_Pump_2.cellx_props" >> cppsrc\temp.cpp
echo 	{ "disp_pump2.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/disp_pump2.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for DispensePump3 struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultDispensePump3 = 
echo {
echo 	ComponentType::DispensePump3,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\Dispense_Pump_3.cellx_props" >> cppsrc\temp.cpp
echo 	{ "disp_pump3.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/disp_pump3.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for PickingPump1 struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultPickingPump1 =
echo {
echo 	ComponentType::PickingPump1,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\Picking_Pump_1.cellx_props" >> cppsrc\temp.cpp
echo 	{ "picking_pump1.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/picking_pump1.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for WhiteLightSource struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultWhiteLightSource = 
echo {
echo 	ComponentType::WhiteLightSource,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\White_Light_Source.cellx_props" >> cppsrc\temp.cpp
echo 	{ "white_light.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/white_light.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for FluorescenceLightFilter struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultFluorescenceLightFilter =
echo {
echo 	ComponentType::FluorescenceLightFilter,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\Fluorescence_Light_Filter.cellx_props" >> cppsrc\temp.cpp
echo 	{ "fluoro_filter.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/fluoro_filter.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for AnnularRingHolder struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultAnnularRingHolder =
echo {
echo 	ComponentType::AnnularRingHolder,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\Annular_Ring_Holder.cellx_props" >> cppsrc\temp.cpp
echo 	{ "ann_ring_hldr.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/ann_ring_hldr.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for OpticalColumn struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultOpticalColumn =
echo {
echo 	ComponentType::OpticalColumn,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\Optical_Column.cellx_props" >> cppsrc\temp.cpp
echo 	{ "optical_col.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/optical_col.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for Camera struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultRetigaR3Camera =
echo {
echo 	ComponentType::Camera,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\Camera.cellx_props" >> cppsrc\temp.cpp
echo 	{ "camera.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/camera.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for Incubator struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultIncubator =
echo {
echo 	ComponentType::Incubator,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\Incubator.cellx_props" >> cppsrc\temp.cpp
echo 	{ "incubator.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/incubator.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for CxPM struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultCxPM =
echo {
echo 	ComponentType::CxPM,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\CxPM.cellx_props" >> cppsrc\temp.cpp
echo 	{ "cxpm.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/cxpm.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp

(echo ///----------------------------------------------------------------------
echo /// Functions and defaults for BarcodeReader struct
echo ///----------------------------------------------------------------------
echo ComponentProperties defaultBarcodeReader =
echo {
echo 	ComponentType::BarcodeReader,) >> cppsrc\temp.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\Barcode_Reader.cellx_props" >> cppsrc\temp.cpp
echo 	{ "bcreader.cfg" },>> cppsrc\temp.cpp
echo 	{ (std::string (CONFIG_DIR) + "/bcreader.cfg") }>> cppsrc\temp.cpp
echo };>> cppsrc\temp.cpp
echo.>> cppsrc\temp.cpp


C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\Tip_Type.cellx_props" > cppsrc\TipType.cpp
C:\Users\Mark_gartland\AppData\Local\Programs\Python\Python38\python props2CppInit.py  "C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2023-09-29\Plate_Type.cellx_props" > cppsrc\PlateType.cpp
