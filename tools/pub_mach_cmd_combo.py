import paho.mqtt.client as mqtt #import the client1
from settings import BROKER_ADDRESS
client = mqtt.Client("mypub1") #create new instance
client.connect(BROKER_ADDRESS) #connect to broker
client.publish("tbd/machine/command",'{"message_type": "move_all_tips_to_safe_height", "transport_version": "1.0", "command_group_id": 345, "command_id": 2345, "session_id": "sup?", "machine_id": "CX-2345", "payload": { "word": "smoke em if you got em." } }')#publish
