import paho.mqtt.client as mqtt #import the client1
from settings import BROKER_ADDRESS
client = mqtt.Client("mypub1") #create new instance
client.connect(BROKER_ADDRESS) #connect to broker
client.publish("tbd/machine/command",'{"message_type": "move_to_z_tip_pick_force_newton", "transport_version": "1.0", "command_group_id": 222, "command_id": 2322, "session_id": "quiper", "machine_id": "CX-2345", "payload": { "slow_down_offset_um": 15.432 } }')#publish
