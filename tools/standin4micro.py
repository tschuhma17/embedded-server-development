import paho.mqtt.client as mqtt #import the client1
import time
import json
from settings import BROKER_ADDRESS

############
def on_message(client, userdata, message):
	des_payload = json.loads(message.payload.decode("utf-8"))
	message_type = des_payload['message_type']
	print('')
	print(des_payload)
	if message_type == "move_to_focus_nm" or \
		message_type == "get_current_focus_nm" or \
		message_type == "set_turret_position" or \
		message_type == "get_optical_column_status" or \
		message_type == "get_optical_column_event_log" or \
		message_type == "set_fluorescence_light_filter_on" or \
		message_type == "set_fluorescence_light_filter_position" or \
		message_type == "get_fluorescence_light_filter_status":
		print("RECEIVED command message_type is ", message_type)
		userdata['des_command']=des_payload
		# print(userdata)
		userdata['done'] = True; # Signal to stop subscribing and start publishing.
	else:
		print("message received " ,str(message.payload.decode("utf-8")))
	print("message topic=",message.topic)
	print("message qos=",message.qos)
	print("message retain flag=",message.retain)

########################################
# Wait for one microscope command, then...
# Send Ack ('ok', 200).
# Send Status (200).
# Exit.
########################################
def handle_one_command(client, userdata):
	userdata['done'] = False
	def make_status_details(componentStr):
		timestamp = str(int(time.time())-86400)
		# cycle through the userdata's list of descriptions--one index per call.
		i = userdata['describeID']
		i = i + 1
		if i > 6:
			i = 0
		userdata['describeID'] = i
		detailsStr = ""
		if (len(userdata['descriptions'][i]) > 0):
			detailsStr = '"timestamp": ' + timestamp + ', "component_type": "' + componentStr + '", "event_type": 10, "action": 10, "description": "' + userdata['descriptions'][i] + '"'
		return detailsStr
	# Loop until we receive a microscope command
	while userdata['done'] == False:
		client.loop()

	des_command = userdata['des_command'] # gain access to the message.
	cmd_message_type = des_command['message_type']
	cmd_transport_version = des_command['transport_version']
	cmd_command_group_id = des_command['command_group_id']
	cmd_command_id = des_command['command_id']
	cmd_session_id = des_command['session_id']
	cmd_machine_id = des_command['machine_id']
	cmd_payload = des_command['payload']
	print("message_type=", cmd_message_type)

	### Send an ACK
	# Reuse much of message_payload
	des_ack = {
		'message_type': cmd_message_type,
		'transport_version': cmd_transport_version,
		'command_group_id': cmd_command_group_id,
		'command_id': cmd_command_id,
		'session_id': cmd_session_id,
		'machine_id': cmd_machine_id,
		'status': 'ok',
		'status_code': 200,
		'payload': { },
		'timestamp': str(int(time.time())-86400)
	}
	des_ack_str = json.dumps(des_ack)
	print("Ack is ", des_ack_str)
	client.publish("tbd/microscope/ack", des_ack_str)

	### A breather.
	# time.sleep(1)

	### Send a STATUS
	# Reuse much of message_payload
	des_status = {
		'message_type': cmd_message_type,
		'transport_version': cmd_transport_version,
		'command_group_id': cmd_command_group_id,
		'command_id': cmd_command_id,
		'session_id': cmd_session_id,
		'machine_id': cmd_machine_id,
		'status_code': 200,
		'payload': { }
	}
	# The payload should be appropriate to the kind of command to which we are responding.
	if (cmd_message_type == 'get_current_focus_nm'):
		focus_pos_str = str(userdata['focus_nm'])
		message = 'CellXScopeCommands:scopefocusposIX83:ScopeGetFocus|IX83:ScopeSend:command=FP?|SUCCESS scoperesponse=FP ' + focus_pos_str + '|SUCCESS get Z scoperesponse=FP ' + focus_pos_str + ' value=' + focus_pos_str
		des_status['payload'] = {'status_code': 200, 'details': { 'status': 'SUCCESS', 'message': message, 'focuspos': userdata['focus_nm'] }}
	if (cmd_message_type == 'move_to_focus_nm'):
		old_pos = str(userdata['focus_nm'])
		userdata['focus_nm'] = cmd_payload['focus_nm']
		new_pos = str(userdata['focus_nm'])
		message = 'CellXScopeCommands:scopesetfocusIX83:ScopeGetFocus|IX83:ScopeSend:command=FP?|SUCCESS scoperesponse=FP ' + old_pos + '|SUCCESS get Z scoperesponse=FP ' + old_pos + ' value=' + old_pos + '|IX83:ScopeSend:command=FG ' + new_pos + '|SUCCESS scoperesponse=FG +'
		des_status['payload'] = {'focus_nm': userdata['focus_nm'], 'status_code':200, 'details': { 'status': 'SUCCESS', 'message': message, 'focuspos': userdata['focus_nm'] }}
	if (cmd_message_type == 'set_turret_position'):
		userdata['turret_position'] = cmd_payload['turret_position']
		zero_based_pos = str(userdata['turret_position'])
		one_based_pos = str(userdata['turret_position'] + 1)
		message = 'CellXScopeCommands:scopesetaccessory|type=objective name=objective value=' + zero_based_pos +'|IX83:ScopeSend:command=OBSEQ ' + one_based_pos + '|SUCCESS scoperesponse=OBSEQ +|SUCCESS command OBSEQ ' + one_based_pos + ' OBSEQ +'
		des_status['payload'] = {'turret_position': userdata['turret_position'], 'status_code':200, 'details': { 'status': 'SUCCESS', 'message': message } }
	if (cmd_message_type == 'get_optical_column_event_log'):
		timestamp1 = int(time.time())-86420
		timestamp2 = int(time.time())-86410
		des_status['payload'] = { 'hardware_event_log': [
			{ 'timestamp': timestamp1, 'component_type': 'enumOpticalColumn', 'event_type': 238, 'action': 0, 'description': 'buy Acme anvils, for all your bird hunting needs' },
			{ 'timestamp': timestamp2, 'component_type': 'enumOpticalColumn', 'event_type': 278, 'action': 0, 'description': 'Aargh! My sqeedlyspooch!' } ] }
	if (cmd_message_type == 'get_optical_column_status'):
		detailsStr = make_status_details('enumOpticalColumn')
		des_status['payload'] = { 'status': 200, 'details': detailsStr }
		print("status details = ", detailsStr)
	if (cmd_message_type == "get_fluorescence_light_filter_status"):
		detailsStr = make_status_details('enumFluorescenceLightFilter')
		print("status details = ", detailsStr)
		des_status['payload'] = { 'status': 200, 'details': detailsStr }
	if (cmd_message_type == "set_fluorescence_light_filter_on"):
		userdata['fluorescence_light_filter_on'] = cmd_payload['on_status']
		des_status['payload'] =  { 'on_status': userdata['fluorescence_light_filter_on'], 'status_code': 200, 'details': { 'shutter': { 'status': 'SUCCESS' }, 'lumencore': { 'status': 'SUCCESS' }, 'max': { 'status': 'SUCCESS' } } }
	if (cmd_message_type == 'set_fluorescence_light_filter_position'):
		userdata['fluorescence_light_filter_position'] = cmd_payload['fluorescence_filter_position']
		zero_based_pos = str(userdata['fluorescence_light_filter_position'])
		one_based_pos = str(userdata['fluorescence_light_filter_position'] + 1)
		message = 'CellXScopeCommands:scopesetaccessory|type=filter name=fluor value=' + zero_based_pos + '|IX83:ScopeSend:command=MUSEQ1 ' + one_based_pos + '|SUCCESS scoperesponse=MUSEQ1 +|SUCCESS command MUSEQ1 ' + one_based_pos + ' MUSEQ1 +'
		des_status['payload'] = {'fluorescence_filter_position': userdata['fluorescence_light_filter_position'], 'status_code':200, 'details': {'status': 'SUCCESS', 'message': message}}
	des_status_str = json.dumps(des_status)
	print("Status message is ", des_status_str)
	client.publish("tbd/microscope/status", des_status_str)

print("creating new instance")
g_client = mqtt.Client("myclient5") #create new instance
g_client.on_message=on_message #attach function to callback

shoebox = { 'done': False, 'describeID': 0, 'fluorescence_light_filter_position': 1, 'fluorescence_light_filter_on': False, 'focus_nm': 0, 'turret_position': 1 }
shoebox['descriptions'] = ['Go placidly amid the noise and the haste, and remember what peace there may be in silence.', '', '', 'Be yourself.', 'Be cheerful.', 'Keep interested in your own career, however humble; it is a real possession in the changing fortunes of time.', 'Nurture strength of spirit to shield you in sudden misfortune.']
g_client.user_data_set(shoebox)

print("connecting to broker")
g_client.connect(BROKER_ADDRESS) #connect to broker

print("Subscribing to topic","tbd/microscope/command")
g_client.subscribe("tbd/microscope/command")

while True:
	handle_one_command(g_client, shoebox)
