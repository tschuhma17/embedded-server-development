import time
import json
import paho.mqtt.client as mqtt #import the client1
import paho.mqtt.publish as publish
from settings import BROKER_ADDRESS
from settings import CLIENT2
from settings import CLIENT3

def send_command(command_payload):
	publish.single(topic="tbd/machine/alert", payload=command_payload, qos=2, retain=False, hostname=BROKER_ADDRESS, port=1883, client_id="mypub1", keepalive=60)

# Should succeed:
send_command('{ "message_type": "alert", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT3 + '", "machine_id": "CX-2345", "device_enum": "enumComponentUnknown", "status_code": 403, "is_caused_hold_flag": false }')

send_command('{ "message_type": "alert", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "", "machine_id": "CX-2345", "device_enum": "enumComponentUnknown", "status_code": 404, "is_caused_hold_flag": false }')
