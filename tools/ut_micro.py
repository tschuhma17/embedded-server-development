import time
import json
import paho.mqtt.client as mqtt #import the client1
import paho.mqtt.publish as publish
from settings import BROKER_ADDRESS
from settings import CLIENT2
from settings import CLIENT3
TOPIC_COMMAND="tbd/microscope/command"
TOPIC_ACK="tbd/microscope/ack"
TOPIC_STATUS="tbd/microscope/status"

shoebox = { 'acked': False, 'statused': False }

def send_command(client, userdata, command_payload):
	command_as_string = json.dumps(command_payload)
	userdata['acked'] = False
	userdata['statused'] = False
	userdata['sent_cmd_id'] = command_payload["command_id"]
	print("SEND COMMAND   : ", command_as_string)
	client.publish("tbd/microscope/command", command_as_string)
	while (userdata['acked'] == False) or (userdata['statused'] == False):
		g_client.loop()

############
def on_message(client, userdata, message):
	print("msg: ", str(message.payload.decode("utf-8")))
	des_payload = json.loads(message.payload.decode("utf-8"))
	topic_in = bytes.decode(message._topic)
	sent_id = userdata['sent_cmd_id']
	received_id = des_payload['command_id']
	if (topic_in == TOPIC_ACK) and (sent_id == received_id):
		print("RECEIVED ACK   : ", des_payload)
		userdata['acked'] = True
	if (topic_in == TOPIC_STATUS) and (sent_id == received_id):
		print("RECEIVED STATUS: ", des_payload)
		userdata['statused'] = True

g_client = mqtt.Client(CLIENT2) #create new instance
g_client.on_message=on_message #attach function to callback

g_client.user_data_set(shoebox)

print("connecting to broker")
g_client.connect(BROKER_ADDRESS) #connect to broker

print("Subscribing to topic","tbd/microscope/command")
g_client.subscribe([(TOPIC_ACK, 0), (TOPIC_STATUS, 0)])

def ut():
	COMMAND=100
	
	# should return empty payload
	commandObj = {
		"message_type": "move_to_focus_nm",
		"transport_version": "1.0",
		"command_group_id": 234,
		"command_id": COMMAND,
		"session_id": CLIENT2,
		"machine_id": "CX-2345",
		"payload": { "focus_nm": 35753 }
	}
	send_command(g_client, shoebox, commandObj)

	# should return empty payload
	COMMAND +=1
	commandObj["message_type"] = "set_turret_position"
	commandObj["command_id"] = COMMAND
	commandObj["payload"] = { "turret_position": 1 }
	send_command(g_client, shoebox, commandObj)


	# should return a payload with "focus_nm": long value.
	COMMAND +=1
	commandObj["message_type"] = "get_current_focus_nm"
	commandObj["command_id"] = COMMAND
	commandObj["payload"] = { }
	send_command(g_client, shoebox, commandObj)
	
	# should return empty payload
	COMMAND +=1
	commandObj["message_type"] = "set_fluorescence_light_filter_on"
	commandObj["command_id"] = COMMAND
	commandObj["payload"] = { "on_status": True }
	send_command(g_client, shoebox, commandObj)

	# should return empty payload
	COMMAND +=1
	commandObj["message_type"] = "set_fluorescence_light_filter_position"
	commandObj["command_id"] = COMMAND
	commandObj["payload"] = { "fluorescence_filter_position": 2 }
	send_command(g_client, shoebox, commandObj)
	'''
	# Not to be implemented on OWS
	COMMAND +=1
	send_command(g_client, shoebox, '{"message_type": "get_optical_column_event_log", "transport_version": "1.0", "command_group_id": 234, "command_id": ' + str(COMMAND) + ', "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "turret_position": 1 }}', COMMAND)
	COMMAND +=1
	
	# Not to be implemented on OWS
	send_command(g_client, shoebox, '{"message_type": "get_optical_column_status", "transport_version": "1.0", "command_group_id": 234, "command_id": ' + str(COMMAND) + ', "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { }}', COMMAND)
	COMMAND +=1

	# Not to be implemented on OWS
	send_command(g_client, shoebox, '{"message_type": "get_optical_column_status", "transport_version": "1.0", "command_group_id": 234, "command_id": ' + str(COMMAND) + ', "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { }}', COMMAND)
	COMMAND +=1

	# Not to be implemented on OWS
	send_command(g_client, shoebox, '{"message_type": "get_optical_column_status", "transport_version": "1.0", "command_group_id": 234, "command_id": ' + str(COMMAND) + ', "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { }}', COMMAND)
	COMMAND +=1

	# Not to be implemented on OWS
	send_command(g_client, shoebox, '{"message_type": "get_optical_column_status", "transport_version": "1.0", "command_group_id": 234, "command_id": ' + str(COMMAND) + ', "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { }}', COMMAND)
	COMMAND +=1

	# Not to be implemented on OWS
	send_command(g_client, shoebox, '{"message_type": "get_optical_column_status", "transport_version": "1.0", "command_group_id": 234, "command_id": ' + str(COMMAND) + ', "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { }}', COMMAND)
	COMMAND +=1
	'''
ut()
