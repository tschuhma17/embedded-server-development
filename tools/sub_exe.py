import paho.mqtt.client as mqtt #import the client1
import time
import os
from settings import BROKER_ADDRESS
############
def on_message(client, userdata, message):
	with open('pic.tar.gz', 'wb') as fd:
		fd.write(message.payload)
		os.system('tar xvfz pic.tar.gz')
		print("message topic=",message.topic)
		print("message qos=",message.qos)
		print("message retain flag=",message.retain)
########################################
print("creating new instance")
client = mqtt.Client("myclient2") #create new instance
client.on_message=on_message #attach function to callback
print("connecting to broker")
client.connect(BROKER_ADDRESS, 1883, 100) #connect to broker
client.loop_start() #start the loop
print("Subscribing to topic","tbd/machine/photo")
client.subscribe("tbd/machine/photo")
time.sleep(100) # wait
client.loop_stop() #stop the loop

