import time
import paho.mqtt.client as mqtt #import the client1
import paho.mqtt.publish as publish
from settings import BROKER_ADDRESS
from settings import CLIENT2
from settings import CLIENT3

def send_command(command_payload):
	publish.single(topic="tbd/machine/command", payload=command_payload, qos=2, retain=False, hostname=BROKER_ADDRESS, port=1883, client_id="mypub1", keepalive=60)
	time.sleep(1) # wait

send_command('{ "message_type": "does_master_client_exist", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "who-am-i", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "register_as_master_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')

send_command('{ "message_type": "does_master_client_exist", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "who-am-i", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "unregister_as_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')

send_command('{ "message_type": "does_master_client_exist", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "register_as_observer_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT3 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT3 + '" } }')

send_command('{ "message_type": "unregister_as_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT3 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT3 + '" } }')

# This lacks minimum payload, so expect rejection.
send_command('{ "message_type": "does_master_client_exist", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "who-am-i", "payload": { } }')

send_command('{ "message_type": "does_master_client_exist", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "who-am-i", "machine_id": NULL, "payload": { } }')
