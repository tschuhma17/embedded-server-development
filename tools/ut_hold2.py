import time
import paho.mqtt.client as mqtt #import the client1
import paho.mqtt.publish as publish
from settings import BROKER_ADDRESS
from settings import CLIENT2
from tip_types import TIP_TYPE1
from plate_types import PLATE_TYPE1

def send_command(command_payload):
	publish.single(topic="tbd/machine/command", payload=command_payload, qos=2, retain=False, hostname=BROKER_ADDRESS, port=1883, client_id="mypub1", keepalive=60)
plate1_str = json.dumps(PLATE_TYPE1)
plate2_str = json.dumps(PLATE_TYPE2)


# Register as master...
# Should succeed:
send_command('{ "message_type": "register_as_master_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')

# Try to collect 4 images.
# Expect most picures to be snapped after the 5 second pause completes.
send_command('{ "message_type": "collect_montage_images", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_type": ' + plate2_str + ', "plate_position": 1, "well": "B2", "x_start_offset_nm": 10000000, "x_step_count": 2, "x_step_increment_nm": 2500000, "y_start_offset_nm": 10000000, "y_step_count": 2, "y_step_increment_nm": 2700000, "scan_movement_delay_ms": 22.2 } }')

# Queue up a small command for after the montage finishes
send_command('{ "message_type": "move_z_to_safe_height", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": NULL }')

# Expect this to let the montages complete but hold the move command
send_command('{ "message_type": "hold_operation_stack", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": NULL }')
time.sleep(5)

# Queue up a small command for after the montage finishes
send_command('{ "message_type": "move_z_to_safe_height", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": NULL }')

# Queue up a small command for after the montage finishes
send_command('{ "message_type": "move_z_to_safe_height", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": NULL }')

# Expect the move command to happen after this is handled.
send_command('{ "message_type": "resume_all_operations", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": NULL }')

# Unregister.
# Should succeed:
send_command('{ "message_type": "unregister_as_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')

