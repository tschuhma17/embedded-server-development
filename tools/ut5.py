import time
import paho.mqtt.client as mqtt #import the client1
import paho.mqtt.publish as publish
from settings import BROKER_ADDRESS
from settings import CLIENT2
from tip_types import TIP_TYPE1
from plate_types import PLATE_TYPE1
from plate_types import PLATE_TYPE2
import json

def send_command(command_payload):
	publish.single(topic="tbd/machine/command", payload=command_payload, qos=2, retain=False, hostname=BROKER_ADDRESS, port=1883, client_id="mypub1", keepalive=60)

# time.sleep(1) # wait

# Register as master...
# Should succeed:
send_command('{ "message_type": "register_as_master_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')
'''
################   Commands using plate_type   ################

# Not implemented
send_command('{ "message_type": "move_XY_to_optical_eye_nm_A1_notation", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + PLATE_TYPE1 + ', "plate_position": 1, "well": 2, "x_offset_nm": 1000, "y_offset_nm": 2000 } }')


plate2_str = json.dumps(PLATE_TYPE2)
send_command('{ "message_type": "move_xy_to_optical_eye_nm_rc_notation", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_type": ' + plate2_str + ', "plate_position": 1, "well_row": 2, "well_column": 2, "x_offset_nm": 5000000, "y_offset_nm": 7000000 } }')

# Expect the following to print by wes: x_plate_1_right_edge_to_optical_path, y_plate_1_top_edge_to_optical_path
send_command('{ "message_type": "move_XY_to_optical_eye_nm_A1_notation", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + PLATE_TYPE1 + ', "plate_position": 1, "well": "B2", "x_offset_nm": 1020, "y_offset_nm": 2020 } }')

# Expect the following to print by wes: x_plate_1_right_edge_to_optical_path, y_plate_1_top_edge_to_optical_path
send_command('{ "message_type": "move_xy_to_optical_eye_nm_rc_notation", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + PLATE_TYPE1 + ', "plate_position": 1, "well_row": 3, "well_column": 3, "x_offset_nm": 1030, "y_offset_nm": 2030 } }')

# Expect the following to print by wes: x_plate_1_right_edge_to_optical_path, y_plate_1_top_edge_to_optical_path
send_command('{ "message_type": "move_XY_to_tip_working_location_nm_A1_notation", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + PLATE_TYPE1 + ', "plate_position": 1, "well": "A1", "x_offset_nm": 1010, "y_offset_nm": 2010 } }')

# Expect the following to print by wes: x_plate_1_right_edge_to_optical_path, y_plate_1_top_edge_to_optical_path
send_command('{ "message_type": "move_xy_to_tip_working_position_nm_rc_notation", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + PLATE_TYPE1 + ', "plate_position": 1, "well_row": 4, "well_column": 4, "x_offset_nm": 1040, "y_offset_nm": 2040 } }')

# Not implemented
send_command('{ "message_type": "move_xy_to_tip_working_position_nm_rc_notation", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + PLATE_TYPE1 + ', "plate_position": 1, "well_row": 2, "well_column": 3, "x_offset_nm": 1000, "y_offset_nm": 2000 } }')

################   Commands using plate_type *and* tip_type   ################

# Newly implemented
send_command('{ "message_type": "move_to_z_tip_well_bottom_force_newton", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ', ' + PLATE_TYPE1 + ' } }')

# Newly implemented
send_command('{ "message_type": "move_to_zz_tip_well_bottom_force_newton", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ', ' + PLATE_TYPE1 + ' } }')

################   Commands using tip_type   ################

# Newly implemented
send_command('{ "message_type": "move_to_z_axis_well_bottom_force_newton", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "error_height": 90909 } }')

# Newly implemented
send_command('{ "message_type": "move_to_zz_axis_well_bottom_force_newton", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "error_height": 80808 } }')

send_command('{ "message_type": "move_to_z_axis_force_newton", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ' } }')

send_command('{ "message_type": "move_to_zz_axis_force_newton", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ' } }')

send_command('{ "message_type": "move_to_z_tip_pick_force_newton", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ' } }')

send_command('{ "message_type": "move_to_zz_tip_pick_force_newton", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ' } }')

send_command('{ "message_type": "get_tip", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ', "tip_axis": "enumZAxis", "tip_number": 1 } }')

send_command('{ "message_type": "get_tip", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ', "tip_axis": "enumZZAxis", "tip_number": 1 } }')

# Expect the following tp print by wes: tip_axis = "enumZAxis", xx_z_tip_to_waste_3_position = 0, z_to_waste_3_height = -45000000, tip_waste_3_safety_gap = 2000000, and more
#send_command('{ "message_type": "move_tip_to_waste_location_3", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ', "tip_axis": "enumZZZAxis" } }')

# Expect the following tp print by wes: tip_axis = "enumZAxis", xx_z_tip_to_waste_2_position = 0, z_to_waste_2_height = -45000000, tip_waste_2_safety_gap = 2000000, and more
#send_command('{ "message_type": "move_tip_to_waste_location_2", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ', "tip_axis": "enumZZAxis" } }')

# Expect the following tp print by wes: tip_axis = "enumZAxis", xx_z_tip_to_waste_1_position = 39390000, z_to_waste_1_height = -45000000, tip_waste_1_safety_gap = 2000000, and more
send_command('{ "message_type": "move_tip_to_waste_location_1", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ', "tip_axis": "enumZAxis" } }')

send_command('{ "message_type": "move_to_zz_axis_seating_force_newton", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ' } }')

send_command('{ "message_type": "move_to_z_axis_seating_force_newton", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ' } }')

send_command('{ "message_type": "move_to_zz_tip_seating_force_newton", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ' } }')

send_command('{ "message_type": "move_to_z_tip_seating_force_newton", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ' } }')

send_command('{ "message_type": "seat_tip", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ', "tip_axis": "enumZAxis" } }')

send_command('{ "message_type": "seat_tip", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ', "tip_axis": "enumZZAxis" } }')

# Expect the following to print by wes: z_safeHeight_nm, zz_safeHeight_nm, zzz_safeHeight_nm, x_optical_sensor_to_tip_skew_position, y_optical_sensor_to_tip_skew_position,
#  x_find_tip_skew_travel_distance, y_find_tip_skew_travel_distance, x_find_tip_skew_speed, y_find_tip_skew_speed, x_find_tip_skew_acceleration, y_find_tip_skew_acceleration,
#  x_find_tip_skew_deceleration, tip_skew_safety_gap, xx_z_tip_to_tip_skew_position, z_to_tip_skew_sensor_height
send_command('{ "message_type": "get_tip_skew", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ', "tip_axis": "enumZAxis" } }')

send_command('{ "message_type": "move_to_xx_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "xx_nm": 3491328 } }')

send_command('{ "message_type": "move_to_xx_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "xx_nm": 0 } }')

# Newly implemented
send_command('{ "message_type": "remove_tip", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { ' + TIP_TYPE1 + ', "tip_axis": "enumZAxis" } }')

send_command('{ "message_type": "move_to_xx_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "xx_nm": 0 } }')

send_command('{ "message_type": "move_to_yy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "yy_nm": 0 } }')

################   Compound commands   ################

# Expect the following to print by wes: z_safeHeight_nm, zz_safeHeight_nm, zzz_safeHeight_nm
send_command('{ "message_type": "move_all_tips_to_safe_height", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": null }')

# Newly implemented
send_command('{ "message_type": "set_working_tip", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "tip_axis": "enumZAxis" } }')

# Sends 1 or more canned pictures.
send_command('{ "message_type": "collect_montage_images", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "plate_type": ' + plate2_str + ', "plate_position": 1, "well": "B2", "x_start_offset_nm": 10000000, "x_step_count": 3, "x_step_increment_nm": 2500000, "y_start_offset_nm": 10000000, "y_step_count": 3, "y_step_increment_nm": 2700000, "scan_movement_delay_ms": 22.2 } }')

send_command('{ "message_type": "set_log_suppression_level", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "log_suppression_level" : "trace"  } }')

# Sends 1 or more canned pictures.
send_command('{ "message_type": "get_auto_focus_image_stack", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": NULL }')

# Expect the following status payload: { "x_nm": 7077, "y_nm": 4242 }
send_command('{ "message_type": "get_current_xy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": NULL }')

send_command('{ "message_type": "set_optical_column_turret_position", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "turret_position": 1 } }')

# Not implemented
send_command('{ "message_type": "change_active_tip_rack", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "new_active_tip_rack_id": "TheRackJabbit" } }')

# Creates a pic.png file.
send_command('{ "message_type": "get_camera_image", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": null }')

# Should return true for the first three minutes.
send_command('{ "message_type": "is_system_booting", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": null }')

send_command('{"payload":{},"message_type":"is_system_booting","transport_version":"1.0","command_group_id":1,"command_id":57,"session_id":"d9e8d2c3-433b-42c0-be3c-e93a5410db47"}')

send_command('{ "message_type": "move_to_yy_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "yy_nm": 11000000 } }')

send_command('{ "message_type": "move_yy_to_tip_rack_grab_location", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "delid_plate", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": {' + PLATE_TYPE1 + ',"plate_primary_location": "enumNP", "plate_sub_location": "2", "plate_sub_sub_location": "3", "lid_primary_location": "enumLidStage1", "lid_sub_location": NULL, "lid_sub_sub_location": NULL } }')

send_command('{ "message_type": "relid_plate", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": {' + PLATE_TYPE1 + ',"plate_primary_location": "enumNP", "plate_sub_location": "4", "plate_sub_sub_location": "5", "lid_primary_location": "enumLidStage1", "lid_sub_location": NULL, "lid_sub_sub_location": NULL } }')

send_command('{ "message_type": "synchronize_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "timestamp": ' + str(int(time.time())+8) + ' } }')
send_command('{ "message_type": "synchronize_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "timestamp": ' + str(int(time.time())-1) + ' } }')
'''
send_command('{ "message_type": "synchronize_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "timestamp": ' + str(int(time.time())) + ' } }')
'''
#send_command('{ "message_type": "synchronize_time", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "timestamp": ' + str(int(time.time())-86400) + ' } }')
'''
# Unregister.
# Should succeed:
send_command('{ "message_type": "unregister_as_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')

