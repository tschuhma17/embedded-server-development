import time
import json
import paho.mqtt.client as mqtt #import the client1
import paho.mqtt.publish as publish
from settings import BROKER_ADDRESS
from settings import CLIENT2
from settings import CLIENT3
from tip_types import TIP_TYPE2
from plate_types import PLATE_TYPE2
TOPIC_COMMAND="tbd/machine/command"
TOPIC_ACK="tbd/machine/ack"
TOPIC_STATUS="tbd/machine/status"
COMMAND_GROUP_ID=234
MACHINE_ID="CX-2345"
FW_VERSION="0.1.29"
DEFAULT_PROPERTIES_PATH="C:\mdg\jobs\Cell-X\designs\working_cpd_files\JSON_Properties_2022-08-19"

shoebox = {
	'ack' : {
		'received': False,
		'expected_status_code' : 200
	},
	'status' : {
		'received': False,
		'expected_status_code' : 200
	},
	'skipping' : False
}

g_client = mqtt.Client(CLIENT2) #create new instance

# send_command()
# Uses client to send the prepared command_payload to the TOPIC_COMMAND.
# The on_message command handles received messages. We expect to receive an ack
# and a status. This command waits until both are received to exit.
# userdata is an object shared with g_client so send_command() and on_message()
# so both have access to this shared object. (It is shoebox.)
def send_command(client, userdata, command_payload):
	command_as_string = json.dumps(command_payload)
	userdata['ack']['received'] = False
	userdata['status']['received'] = False
	if (userdata['status']['expected_status_code'] == 0):
		userdata['status']['received'] = True # expected_status_code of 0 means we don't expect a response, so don't wait for one.
	userdata['sent_cmd_id'] = command_payload["command_id"]
	userdata['passed'] = True;
	# print("SEND COMMAND   : ", command_as_string)
	client.publish(TOPIC_COMMAND, command_as_string)
	while (userdata['ack']['received'] == False) or (userdata['status']['received'] == False):
		g_client.loop()
	if (userdata['passed'] == True):
		print("passed: ", command_payload['message_type'])
	else:
		print("FAILED: ", command_payload['message_type'])


# on_message() is a handler function passed to g_client. It handles any message that
# is received. We only expect 2 kinds: acks and statuses returned in response to
# a command.
# For an ack, on_message() tests that the status_code matches the expected value.
# For a status, on_message() tests that both the status_code and the payload matches
# expected values. (Sometimes, bad status codes or payloads is expected.)
def on_message(client, userdata, message):
	# print("msg: ", str(message.payload.decode("utf-8")))
	des_payload = json.loads(message.payload.decode("utf-8"))
	topic_in = bytes.decode(message._topic)
	sent_id = userdata['sent_cmd_id']
	received_id = des_payload['command_id']
	if (topic_in == TOPIC_ACK) and (sent_id == received_id):
		# print("RECEIVED ACK   : ", des_payload)
		userdata['ack']['received'] = True
		if (des_payload["status_code"] != userdata['ack']['expected_status_code']):
			userdata['passed'] = False
			print("FAIL: ack status_code is wrong: ", des_payload["status_code"])
			userdata['status']['received'] = True # Not really, but we don't expect a response in this situation.
	if (topic_in == TOPIC_STATUS) and (sent_id == received_id):
		# print("RECEIVED STATUS: ", des_payload)
		userdata['status']['received'] = True
		if (des_payload["status_code"] != userdata['status']['expected_status_code']):
			userdata['passed'] = False
			print("FAIL: status's status_code is wrong: ", des_payload["status_code"])
		else:
			userdata['passed'] = payloads_match(des_payload['payload'], userdata['status']['expected_payload'])
			# print("result =", userdata['passed'])


# payloads_match() compares two objects for likeness. These are meant to be payload
# objects, which are supposed to be dict objects or Nones.
# The received payload may have key/value pairs that we cannot anticipate or that
# we just don't care about. So this test is focused on checking that every key/value
# pair in the expected dict match the ones in the received dict.
def payloads_match(received, expected):
	if (received is None):
		if (expected is None):
			return True
		else:
			print(received)
			return False
	# Received is something.
	if (expected is None):
		print(received)
		return False
	# Both received and expected are somethings.
	expectations_list = sorted(expected.items())
	for k, v in expectations_list:
		if (k == "props_file_name"):
			return property_collections_match(received, v)
		if (k in received.keys()):
			if (type(v) == type(received[k])):
				if (v != received[k]):
					print(received)
					return False
			else:
				print(received)
				return False
		else:
			print(received)
			return False
	return True

def property_collections_match(received_payload, props_file_name):
	matches = False
	expected_props = None
	with open(DEFAULT_PROPERTIES_PATH + "\\" + props_file_name, "r") as f:
		expected_props = json.load(f)
	expected_first = list(expected_props.items())[0]
	received_first = list(received_payload.items())[0]
	# print(expected_first[0])
	# print(received_first[0])
	if (type(expected_first[1]) == dict) and (type(received_first[1]) == dict):
		# print(received_first[1]["vendor"])
		matches = properties_match(received_first[1], expected_first[1])
	else:
		print("expected_first[1] and received_first[1] are different types")
		matches = False
	return matches

def properties_match(received_properties, expected_properties):
	matches = True
	# print(expected_properties)
	for k, v in expected_properties.items():
		if k not in received_properties.keys():
			print("match not found for ", k)
			matches = False
		elif (type(v) != type(received_properties[k])):
			matches = False
		elif ((type(v) == list) and (type(expected_properties[k]) == list)):
			matches = (matches and lists_match(k, received_properties[k], expected_properties[k]))
		else:
			matches = (matches and property_matches(k, received_properties[k], expected_properties[k]))
	return matches

def lists_match(key, received_list, expected_list):
	ex_len = len(expected_list)
	matches = True
	if (ex_len != len(received_list)):
		return False
	for i in range(ex_len):
		matches = (matches and properties_match(received_list[i], expected_list[i]))
	return matches

def property_matches(key, received_property, expected_property):
	matches = True
	matches = (matches and field_matches(key, "data_type", received_property, expected_property))
	matches = (matches and field_matches(key, "base_unit", received_property, expected_property))
	matches = (matches and field_matches(key, "es_min_value", received_property, expected_property))
	matches = (matches and field_matches(key, "es_max_value", received_property, expected_property))
	matches = (matches and field_matches(key, "setting", received_property, expected_property))
	matches = (matches and field_matches(key, "scale_factor", received_property, expected_property))
	matches = (matches and field_matches(key, "persist_type", received_property, expected_property))
	matches = (matches and field_matches(key, "editability_type", received_property, expected_property))
	matches = (matches and field_matches(key, "points_past_decimal", received_property, expected_property))
	matches = (matches and field_matches(key, "display_directive", received_property, expected_property))
	matches = (matches and field_matches(key, "display_min_value", received_property, expected_property))
	matches = (matches and field_matches(key, "display_max_value", received_property, expected_property))
	return matches

def field_matches(prop_key, field_key, received_property, expected_property):
	matches = True
	exp_val = expected_property[field_key]
	if exp_val == "":
		exp_val = None  # TODO: Confirm that it is OK equate "" with null.
	if exp_val == 0:
		exp_val = None
	rec_val = received_property[field_key]
	if rec_val == "":
		rec_val = None  # TODO: Confirm that it is OK equate "" with null.
	if rec_val == 0:
		rec_val = None
	if (exp_val != rec_val):
		print(prop_key, ":", field_key, " value [", expected_property[field_key], "] doesn't match [", received_property[field_key], "]", sep='')
		matches = False
	return matches


# Trying to be terse here, to pack a lot in. Here is a legend:
# 'mtp' is 'message_type'. 'pld' is the command's outgoing payload. 'ast' is the expected ack status_code. 'sst' is the expected status status_code.
# 'expld' is the payload expected to be returned in the status message. This should only contain the parts of the payload that can be known in advance.
command_list = [
	##### General Commands
	# {'mtp': 'does_master_client_exist', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "master_client_exists": False }},
	# {'mtp': 'get_system_serial_number', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "serial_number": "NOT_SET" }},
	# {'mtp': 'get_firmware_version_number', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "firmware_version_number": FW_VERSION }},
	# {'mtp': 'register_as_master_client', 'pld': { 'requesting_client_guid': CLIENT2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': {'registration_successful': True}},
	# {'mtp': 'does_master_client_exist', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "master_client_exists": True }},
	# {'mtp': 'get_firmware_version_number', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "firmware_version_number": FW_VERSION }},
	# {'mtp': 'set_system_serial_number', 'pld': { "serial_number": "9908" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None},
	# {'mtp': 'get_system_serial_number', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "serial_number": "9908" }},
	# {'mtp': 'set_system_serial_number', 'pld': { "serial_number": "NOT_SET" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None},
	# {'mtp': 'unregister_as_client', 'pld': { 'requesting_client_guid': CLIENT2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': {'unregistration_successful': True}},
	# {'mtp': 'does_master_client_exist', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "master_client_exists": False }},
	# {'mtp': 'register_as_observer_client', 'pld': { 'requesting_client_guid': CLIENT3 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': {'registration_successful': True}},
	# {'mtp': 'get_firmware_version_number', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT3, 'expld': { "firmware_version_number": FW_VERSION }},
	# {'mtp': 'unregister_as_client', 'pld': { 'requesting_client_guid': CLIENT3 }, 'ast': 200, 'sst': 200, 'cid': CLIENT3, 'expld': {'unregistration_successful': True}},
	{'mtp': 'register_as_master_client', 'pld': { 'requesting_client_guid': CLIENT2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': {'registration_successful': True}},
	{'mtp': 'is_system_booting', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# GetCxDProperties
	# GetSystemSerialNumber
	# AppendToCalibrationLog
	# GetCalibrationLog
	# ResetCalibrationLog
	# AppendToMaintenanceLog
	# GetMaintenanceLog
	# ResetMaintenanceLog
	# GetHardwareEventLog
	# ResetHardwareEventLog
	# GetCurrentSystemConfiguration
	# GetFirmwareVersionNumber
	# SetSystemSerialNumber
	# HoldOperationStack
	# HoldAllOperations
	# ResumeAllOperations
	# KillPendingOperations
	# GetAlarmsLog
	# ResetAlarmsLog
	# GetConnectionLog
	# ResetConnectionLog
	##### Component Properties Definitions
	# UploadCPD
	# GetCPDListFromType
	# GetCPDContents
	# DeleteCPD
	# GetComponentProperties
	# SetComponentProperties
	# ApplyCPDToComponent
	{'mtp': 'start_skipping', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	##### XY Axis Motion
	{'mtp': 'move_to_xy_nm', 'pld': { "x_nm": 1000000, "y_nm": 2000000 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'get_current_xy_nm', 'pld': None, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "x_nm": 1000000, "y_nm": 2000000 }},
	{'mtp': 'swirl_xy_stage', 'pld': { "number_of_rotations": 4, "rotation_radius": 1000000.0, "rotation_period": 1.0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'swirl_xy_stage', 'pld': { "number_of_rotations": 1, "rotation_radius": 1000000.0, "rotation_period": 2.0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'swirl_xy_stage', 'pld': { "number_of_rotations": 0, "rotation_radius": 1000000.0, "rotation_period": 0.5 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'move_to_xy_nm', 'pld': { "x_nm": 0, "y_nm": 0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_xy_nm', 'pld': { "x_nm": 500, "y_nm": 1500 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_current_xy_nm', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "x_nm": 500, "y_nm": 1500 }},
	# {'mtp': 'move_to_xy_relative_nm', 'pld': { "x_nm": 100, "y_nm": -100 }, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_x_low_limit', 'pld': None, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_x_high_limit', 'pld': None, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_x_to_tip_skew_trigger', 'pld': { "xx_nm": 2030 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "x_trigger_location_nm": 5000000 }},
	# {'mtp': 'move_to_y_low_limit', 'pld': None, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_y_high_limit', 'pld': None, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_y_to_tip_skew_trigger', 'pld': { "xx_nm": 2030 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "y_trigger_location_nm": 5000000 }},
	{'mtp': 'move_XY_to_optical_eye_nm_A1_notation', 'pld': { "plate_type": PLATE_TYPE2, "plate_position": 1, "well": "A1", "x_offset_nm": 1000, "y_offset_nm": 2000 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'move_XY_to_optical_eye_nm_A1_notation', 'pld': { "plate_type": PLATE_TYPE2, "plate_position": 1, "well": "B2", "x_offset_nm": 1020, "y_offset_nm": 2020 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_xy_to_optical_eye_nm_rc_notation', 'pld': { "plate_type": PLATE_TYPE2, "plate_position": 1, "well_row": 2, "well_column": 3, "x_offset_nm": 1000, "y_offset_nm": 2000 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_xy_to_optical_eye_nm_rc_notation', 'pld': { "plate_type": PLATE_TYPE2, "plate_position": 1, "well_row": 3, "well_column": 3, "x_offset_nm": 1030, "y_offset_nm": 2030 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_XY_to_tip_working_location_nm_A1_notation', 'pld': { "plate_type": PLATE_TYPE2, "plate_position": 1, "well": "A1", "x_offset_nm": 1010, "y_offset_nm": 2010 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_XY_to_tip_working_location_nm_A1_notation', 'pld': { "plate_type": PLATE_TYPE2, "plate_position": 2, "well": "A1", "x_offset_nm": 1010, "y_offset_nm": 2010 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_xy_to_tip_working_position_nm_rc_notation', 'pld': { "plate_type": PLATE_TYPE2, "plate_position": 1, "well_row": 4, "well_column": 4, "x_offset_nm": 1040, "y_offset_nm": 2040 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_xy_to_tip_working_position_nm_rc_notation', 'pld': { "plate_type": PLATE_TYPE2, "plate_position": 1, "well_row": 2, "well_column": 3, "x_offset_nm": 1000, "y_offset_nm": 2000 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_xy_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "XY_Axes.cellx_props" }},
	# SetXYProperties
	##### XX Axis Motion
	{'mtp': 'move_to_xx_nm', 'pld': { "xx_nm": 2030000 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'get_current_xx_nm', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "xx_nm": 2030000 }},
	# {'mtp': 'move_to_xx_nm', 'pld': { "xx_nm": 1040 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_current_xx_nm', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "xx_nm": 1040 }},
	# {'mtp': 'move_to_xx_relative_nm', 'pld': { "xx_offset_nm": 100 }, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_xx_low_limit', 'pld': None, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_xx_high_limit', 'pld': None, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_xx_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "XX_Axis.cellx_props" }},
	# SetXXProperties
	##### YY Axis Motion
	{'mtp': 'move_to_yy_nm', 'pld': { "yy_nm": 1230000 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'get_current_yy_nm', 'pld': None, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "yy_nm": 1230000 }},
	# {'mtp': 'move_to_yy_nm', 'pld': { "yy_nm": 2520 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_current_yy_nm', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "yy_nm": 2520 }},
	# {'mtp': 'move_to_yy_relative_nm', 'pld': { "yy_offset_nm": 2520 }, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_yy_low_limit', 'pld': None, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_yy_high_limit', 'pld': None, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_yy_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "YY_Axis.cellx_props" }},
	# SetYYProperties
	# {'mtp': 'move_yy_to_tip_rack_grab_location', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	##### Z Axis Motion
	{'mtp': 'move_to_z_nm', 'pld': { "z_nm": -7770000, }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'get_current_z_nm', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "z_nm": -7770000 }},
	# {'mtp': 'move_to_z_relative_nm', 'pld': { "z_offset_nm": 222 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_current_z_nm', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "z_nm": 999 }},
	# {'mtp': 'move_to_z_low_limit', 'pld': None, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_z_high_limit', 'pld': None, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'move_z_to_safe_height', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_z_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "Z_Axis.cellx_props" }},
	# SetZProperties
	{'mtp': 'move_to_z_axis_force_newton', 'pld': { "tip_type": TIP_TYPE2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_z_axis_seating_force_newton', 'pld': { "tip_type": TIP_TYPE2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_z_axis_well_bottom_force_newton', 'pld': { "error_height": 90909 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	##### ZZ Axis Motion
	{'mtp': 'move_to_zz_nm', 'pld': { "zz_nm": -5432100 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'get_current_zz_nm', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "zz_nm": -5432100 }},
	# {'mtp': 'move_to_zz_relative_nm', 'pld': { "zz_offset_nm": 33 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_current_zz_nm', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "zz_nm": 54354 }},
	# {'mtp': 'move_to_zz_low_limit', 'pld': None, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_zz_high_limit', 'pld': None, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'move_zz_to_safe_height', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_zz_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "ZZ_Axis.cellx_props" }},
	# SetZZProperties
	# {'mtp': 'move_to_zz_tip_pick_force_newton', 'pld': { "tip_type": TIP_TYPE2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_zz_tip_seating_force_newton', 'pld': { "tip_type": TIP_TYPE2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'move_to_zz_axis_force_newton', 'pld': { "tip_type": TIP_TYPE2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_zz_axis_seating_force_newton', 'pld': { "tip_type": TIP_TYPE2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_zz_axis_well_bottom_force_newton', 'pld': { "error_height": 80808 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	##### ZZZ Axis Motion
	{'mtp': 'move_to_zzz_nm', 'pld': { "zzz_nm": -3330000 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'get_current_zzz_nm', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "zzz_nm": -3330000 }},
	# {'mtp': 'move_to_zzz_relative_nm', 'pld': { "zzz_offset_nm": 56 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_current_zzz_nm', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "zzz_nm": 389 }},
	# {'mtp': 'move_to_zzz_low_limit', 'pld': None, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_zzz_high_limit', 'pld': None, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_zzz_to_safe_height', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_zzz_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "ZZZ_Axis.cellx_props" }},
	# SetZZZProperties
	##### Aspiration Pump
	{'mtp': 'aspirate_zz_flow_rate_time', 'pld': { 'is_normal_direction': True, 'flow_rate_ul_per_s': 600.000, 'time_s': 2.0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'aspirate_zz_flow_rate_volume', 'pld': { 'is_normal_direction': False, 'flow_rate_ul_per_s': 600.0, 'volume_ul': 1200.0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'aspirate_zz_flow_rate_volume', 'pld': { 'is_normal_direction': False, 'flow_rate_ul_per_s': 1.0, 'volume_ul': 0.01 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'aspirate_zz_volume_time', 'pld': { 'is_normal_direction': True, 'volume_ul': 1200.0, 'time_s': 2.0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_aspiration_pump_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'Aspiration_Pump_1.cellx_props' }},
	# {'mtp': 'aspiration_pump_start_stop', 'pld': { 'is_normal_direction': True, 'is_start_pump': True }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'is_aspiration_pump_running', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "is_pump_running": True }},
	# {'mtp': 'delay', 'pld': { 'seconds': 3 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'aspiration_pump_start_stop', 'pld': { 'is_normal_direction': True, 'is_start_pump': False }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'is_aspiration_pump_running', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "is_pump_running": False }},
	# {'mtp': 'delay', 'pld': { 'seconds': 1 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# SetAspirationPumpProperties
	##### Dispense Pump 1
	{'mtp': 'dispense_1_flow_rate_time', 'pld': { 'is_normal_direction': True, 'flow_rate_ul_per_s': 600.000, 'time_s': 2.0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'dispense_1_flow_rate_volume', 'pld': { 'is_normal_direction': False, 'flow_rate_ul_per_s': 600.0, 'volume_ul': 1200.0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'dispense_1_flow_rate_volume', 'pld': { 'is_normal_direction': False, 'flow_rate_ul_per_s': 1.0, 'volume_ul': 0.01 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'dispense_1_volume_time', 'pld': { 'is_normal_direction': True, 'volume_ul': 1200.0, 'time_s': 2.0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_dispense_pump_1_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'Dispense_Pump_1.cellx_props' }},
	# {'mtp': 'dispense_pump_1_start_stop', 'pld': { 'is_normal_direction': True, 'is_start_pump': True }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'is_dispense_pump_1_running', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'is_pump_running': True }},
	# {'mtp': 'delay', 'pld': { 'seconds': 3 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'dispense_pump_1_start_stop', 'pld': { 'is_normal_direction': True, 'is_start_pump': False }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'is_dispense_pump_1_running', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'is_pump_running': False }},
	# {'mtp': 'delay', 'pld': { 'seconds': 1 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# SetDispensePump1Properties
	##### Dispense Pump 2
	# {'mtp': 'dispense_2_flow_rate_time', 'pld': { 'is_normal_direction': True, 'flow_rate_ul_per_s': 600.000, 'time_s': 2.0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'dispense_2_flow_rate_volume', 'pld': { 'is_normal_direction': False, 'flow_rate_ul_per_s': 600.0, 'volume_ul': 1200.0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'dispense_2_volume_time', 'pld': { 'is_normal_direction': True, 'volume_ul': 1200.0, 'time_s': 2.0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_dispense_pump_2_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'Dispense_Pump_2.cellx_props' }},
	# {'mtp': 'dispense_pump_2_start_stop', 'pld': { 'is_normal_direction': True, 'is_start_pump': True }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'is_dispense_pump_2_running', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'is_pump_running': True }},
	# {'mtp': 'delay', 'pld': { 'seconds': 3 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'dispense_pump_2_start_stop', 'pld': { 'is_normal_direction': True, 'is_start_pump': False }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'is_dispense_pump_2_running', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'is_pump_running': False }},
	# {'mtp': 'delay', 'pld': { 'seconds': 1 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# SetDispensePump2Properties
	##### Dispense Pump 3
	# {'mtp': 'dispense_3_flow_rate_time', 'pld': { 'is_normal_direction': True, 'flow_rate_ul_per_s': 300.000, 'time_s': 2.0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'dispense_3_flow_rate_volume', 'pld': { 'is_normal_direction': False, 'flow_rate_ul_per_s': 300.0, 'volume_ul': 600.0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'dispense_3_volume_time', 'pld': { 'is_normal_direction': True, 'volume_ul': 600.0, 'time_s': 2.0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_dispense_pump_3_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'Dispense_Pump_3.cellx_props' }},
	# {'mtp': 'dispense_pump_3_start_stop', 'pld': { 'is_normal_direction': True, 'is_start_pump': True }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'is_dispense_pump_3_running', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'is_pump_running': True }},
	# {'mtp': 'delay', 'pld': { 'seconds': 3 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'dispense_pump_3_start_stop', 'pld': { 'is_normal_direction': True, 'is_start_pump': False }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'is_dispense_pump_3_running', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'is_pump_running': False }},
	# {'mtp': 'delay', 'pld': { 'seconds': 1 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# SetDispensePump3Properties
	##### Picking Pump
	{'mtp': 'picking_pump_flow_rate_time', 'pld': { "is_aspirate": True, "tip_working_volume_nl": 1000000, "flow_rate_ul_per_s": 25.25, "time_s": 5.252 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'picking_pump_flow_rate_volume', 'pld': { "is_aspirate": False, "tip_working_volume_nl": 1000000, "flow_rate_ul_per_s": 3.737, "volume_ul": 7.373 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'picking_pump_volume_time', 'pld': { "is_aspirate": True, "tip_working_volume_nl": 1000000, "volume_ul": 4.848, "time_s": 8.484 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_picking_pump_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "Picking_Pump_1.cellx_props" }},
	# SetPickingPumpProperties
	##### White Light Source
	# {'mtp': 'set_white_light_source_on_off', 'pld': { "is_turn_on": True }, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'is_white_light_source_on', 'pld': { }, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_white_light_source_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "White_Light_Source.cellx_props" }},
	# SetWhiteLightSourceProperties
	##### Fluorescence Light Filter
	# {'mtp': 'set_fluorescence_light_filter_on_off', 'pld': { "filter_index": 0, "is_turn_on": True }, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'is_fluorescence_light_filter_on', 'pld': { }, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_fluorescence_light_filter_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "Fluorescence_Light_Filter.cellx_props" }},
	# SetFluorescenceLightSourceProperties
	##### Annular Ring Holder
	# SetAnnularRingHolderPosition
	# GetAnnularRingHolderPosition
	# {'mtp': 'get_annular_ring_holder_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "Annular_Ring_Holder.cellx_props" }},
	# SetAnnularRingHolderProperties
	##### Optical Column
	# {'mtp': 'move_to_focus_nm', 'pld': { "focus_nm": 35753 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_focus_relative_nm', 'pld': { "focus_offset_nm": 225 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_focus_relative_nm', 'pld': { "focus_offset_nm": -125 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_current_focus_nm', 'pld': { "focus_nm": 35753 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "focus_nm": 35853 }},
	# {'mtp': 'move_to_focus_low_limit', 'pld': None, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_to_focus_high_limit', 'pld': None, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	# RotateTurretToPosition
	# {'mtp': 'get_optical_column_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "Optical_Column.cellx_props" }},
	# SetOpticalColumnProperties
	# {'mtp': 'is_camera_image_available', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "is_image_available": False }},
	# {'mtp': 'get_camera_image', 'pld': None, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_camera_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "Camera.cellx_props" }},
	# SetCameraProperties
	##### Incubator
	# PlacePlateIntoIncubator
	# RetrievePlateFromIncubator
	# {'mtp': 'get_incubator_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "Incubator.cellx_props" }},
	# SetIncubatorProperties
	##### CxPM
	# {'mtp': 'get_cxpm_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "CxPM.cellx_props" }},
	# SetCxPMProperties
	##### Barcode Reader
	# GetBarcodeReaderProperties
	# {'mtp': 'get_barcode_reader_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "Barcode_Reader.cellx_props" }},
	# SetBarcodeReaderProperties
	##### Digital Inputs
	# Set DigitalInputActiveLevel
	# GetDigitalInputActiveLevel
	# GetDigitalInputState
	##### Digital Outputs
	# Set DigitalOutputActiveLevel
	# GetDigitalOutputActiveLevel
	# GetDigitalOutputState
	# SetDigitalOutputState
	##### Compund Commands
	# {'mtp': 'move_all_tips_to_safe_height', 'pld': None, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { } },
	{'mtp': 'get_tip', 'pld':  { "tip_type": TIP_TYPE2, "tip_axis": "enumZAxis", "tip_number": 1 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'move_tip_to_waste_location_3', 'pld': { "tip_type": TIP_TYPE2, "tip_axis": "enumZAxis" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'move_tip_to_waste_location_2', 'pld': { "tip_type": TIP_TYPE2, "tip_axis": "enumZAxis" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'move_tip_to_waste_location_1', 'pld': { "tip_type": TIP_TYPE2, "tip_axis": "enumZAxis" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'seat_tip', 'pld': { "tip_type": TIP_TYPE2, "tip_axis": "enumZAxis" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'stop_skipping', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'get_tip_skew', 'pld': { "tip_type": TIP_TYPE2, "tip_axis": "enumZAxis" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "x_skew_nm": 0, "y_skew_nm": 0 }},
	{'mtp': 'get_tip_skew', 'pld': { "tip_type": TIP_TYPE2, "tip_axis": "enumZZAxis" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "x_skew_nm": 0, "y_skew_nm": 0 }},
	{'mtp': 'move_y_to_tip_skew_trigger', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "y_trigger_location_nm": 0 }},
	{'mtp': 'move_x_to_tip_skew_trigger', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "x_trigger_location_nm": 0 }},
	{'mtp': 'start_skipping', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'set_working_tip', 'pld': { "tip_axis": "enumZAxis" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'remove_tip', 'pld': { "tip_type": TIP_TYPE2, "tip_axis": "enumZAxis" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# CollectMontageImages (formerly CreateMontage)
	{'mtp': 'move_to_z_tip_pick_force_newton', 'pld': { "tip_type": TIP_TYPE2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'move_to_z_tip_seating_force_newton', 'pld': { "tip_type": TIP_TYPE2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'move_to_z_tip_well_bottom_force_newton', 'pld': { "tip_type": TIP_TYPE2, "plate_type": PLATE_TYPE2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "well_bottom_height_nm": 0 }},
	{'mtp': 'move_to_zz_tip_pick_force_newton', 'pld': { "tip_type": TIP_TYPE2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'move_to_zz_tip_well_bottom_force_newton', 'pld': { "tip_type": TIP_TYPE2, "plate_type": PLATE_TYPE2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "well_bottom_height_nm": 0 }},
	{'mtp': 'move_to_zz_tip_seating_force_newton', 'pld': { "tip_type": TIP_TYPE2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# MoveToZZTipWellBottomForceNewton
	{'mtp': 'stop_skipping', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'set_optical_column_turret_position', 'pld': { "turret_position": 1 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# GetAutoFocusImageStack
	# {'mtp': 'change_active_tip_rack', 'pld': { "new_active_tip_rack_id": "TheRackJabbit" }, 'ast': 200, 'sst': 404, 'cid': CLIENT2, 'expld': { }},
	##### Plate Inventory
	# {'mtp': 'add_plate_to_deck_inventory', 'pld': { "plate_id": "12", "primary_location": "enumNP", "sub_location": "1", "sub_sub_location": "1" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'add_plate_to_deck_inventory', 'pld': { "plate_id": "34", "primary_location": "enumNP", "sub_location": "1", "sub_sub_location": "2" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'add_plate_to_deck_inventory', 'pld': { "plate_id": "56", "primary_location": "enumNP", "sub_location": "2", "sub_sub_location": "3" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'add_plate_to_deck_inventory', 'pld': { "plate_id": "77", "primary_location": "enumNP", "sub_location": "2", "sub_sub_location": "" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'add_plate_to_deck_inventory', 'pld': { "plate_id": "88", "primary_location": "enumTS", "sub_location": "1", "sub_sub_location": "" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'find_plate_location_in_inventory', 'pld': { "plate_id": "77" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "primary_location": "enumNP", "sub_location": "2", "sub_sub_location": "" }},
	# {'mtp': 'remove_plate_from_deck_inventory', 'pld': { "plate_id": "77" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'find_plate_location_in_inventory', 'pld': { "plate_id": "77" }, 'ast': 200, 'sst': 4043, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_plate_in_deck_inventory', 'pld': { "plate_id": "56", "primary_location": "enumNP", "sub_location": "2", "sub_sub_location": "1" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_plate_list_in_location', 'pld': { "primary_location": "enumNP", "sub_location": "1", "sub_sub_location": "" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "plate_list": [ { "id": "12", "primary_location": "enumNP", "sub_location": "1", "sub_sub_location": "1" }, { "id": "34", "primary_location": "enumNP", "sub_location": "1", "sub_sub_location": "2" } ] } },
	# {'mtp': 'get_plate_list_in_location', 'pld': { "primary_location": "enumNP", "sub_location": "", "sub_sub_location": "1" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "plate_list": [ { "id": "12", "primary_location": "enumNP", "sub_location": "1", "sub_sub_location": "1" }, { "id": "56", "primary_location": "enumNP", "sub_location": "2", "sub_sub_location": "1" } ] } },
	# {'mtp': 'get_plate_list_in_location', 'pld': { "primary_location": "", "sub_location": "", "sub_sub_location": "" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "plate_list": [ { "id": "12", "primary_location": "enumNP", "sub_location": "1", "sub_sub_location": "1" }, { "id": "34", "primary_location": "enumNP", "sub_location": "1", "sub_sub_location": "2" }, { "id": "56", "primary_location": "enumNP", "sub_location": "2", "sub_sub_location": "1" }, { "id": "88", "primary_location": "enumTS", "sub_location": "1", "sub_sub_location": "" } ] } },
	# {'mtp': 'remove_plate_from_deck_inventory', 'pld': { "plate_id": "12" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'find_plate_location_in_inventory', 'pld': { "plate_id": "12" }, 'ast': 200, 'sst': 4043, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'remove_plate_from_deck_inventory', 'pld': { "plate_id": "60" }, 'ast': 200, 'sst': 4043, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'remove_plate_from_deck_inventory', 'pld': { "plate_id": "34" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'remove_plate_from_deck_inventory', 'pld': { "plate_id": "56" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'remove_plate_from_deck_inventory', 'pld': { "plate_id": "88" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	##### Plate Lid Inventory
	# AddPlateLidToDeckInventory
	# RemovePlateLidFromDeckInventory
	# GetPlateLidListInLocation
	# FindPlateLidLocationInventory
	##### Tip Rack Inventory
	# {'mtp': 'add_tip_rack_to_deck_inventory', 'pld': { "tip_rack_id": "22", "used_tip_count": 2, "primary_location": "enumVTS", "sub_location": "1", "sub_sub_location": "1" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'add_tip_rack_to_deck_inventory', 'pld': { "tip_rack_id": "33", "used_tip_count": 3, "primary_location": "enumVTS", "sub_location": "2", "sub_sub_location": "2" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'add_tip_rack_to_deck_inventory', 'pld': { "tip_rack_id": "44", "used_tip_count": 4, "primary_location": "enumVTS", "sub_location": "2", "sub_sub_location": "3" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'add_tip_rack_to_deck_inventory', 'pld': { "tip_rack_id": "77", "used_tip_count": 5, "primary_location": "enumVTS", "sub_location": "1", "sub_sub_location": "" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'move_tip_rack_in_deck_inventory', 'pld': { "tip_rack_id": "44", "primary_location": "enumVTS", "sub_location": "2", "sub_sub_location": "1" }, 'ast': 200, 'sst': 0, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'get_tip_rack_list_in_location', 'pld': { "primary_location": "enumVTS", "sub_location": "1", "sub_sub_location": "" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "tip_rack_list": [ { "tip_rack_id": "22", "used_tip_count": 2, "primary_location": "enumVTS", "sub_location": "1", "sub_sub_location": "1" }, { "tip_rack_id": "77", "used_tip_count": 5, "primary_location": "enumVTS", "sub_location": "1", "sub_sub_location": "" } ] } },
	# {'mtp': 'get_tip_rack_list_in_location', 'pld': { "primary_location": "enumVTS", "sub_location": "2", "sub_sub_location": "" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "tip_rack_list": [ { "tip_rack_id": "33", "used_tip_count": 3, "primary_location": "enumVTS", "sub_location": "2", "sub_sub_location": "2" }, { "tip_rack_id": "44", "used_tip_count": 4, "primary_location": "enumVTS", "sub_location": "2", "sub_sub_location": "1" } ] } },
	# {'mtp': 'find_tip_rack_location_in_inventory', 'pld': { "tip_rack_id": "44" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "primary_location": "enumVTS", "sub_location": "2", "sub_sub_location": "1" } },
	# {'mtp': 'get_tip_rack_list_in_location', 'pld': { "primary_location": "enumVTS", "sub_location": "", "sub_sub_location": "1" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "tip_rack_list": [ { "tip_rack_id": "22", "used_tip_count": 2, "primary_location": "enumVTS", "sub_location": "1", "sub_sub_location": "1" }, { "tip_rack_id": "44", "used_tip_count": 4, "primary_location": "enumVTS", "sub_location": "2", "sub_sub_location": "1" } ] } },
	# {'mtp': 'get_tip_count_for_tip_rack', 'pld': { "tip_rack_id": "44" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "used_tip_count": 4 } },
	# {'mtp': 'get_tip_count_for_tip_rack', 'pld': { "tip_rack_id": "60" }, 'ast': 200, 'sst': 4043, 'cid': CLIENT2, 'expld': { } },
	# {'mtp': 'set_tip_count_for_tip_rack', 'pld': { "tip_rack_id": "44", "used_tip_count": 5 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { } },
	# {'mtp': 'set_tip_count_for_tip_rack', 'pld': { "tip_rack_id": "60", "used_tip_count": 5 }, 'ast': 200, 'sst': 4043, 'cid': CLIENT2, 'expld': { } },
	# {'mtp': 'remove_tip_rack_from_deck_inventory', 'pld': { "tip_rack_id": "22" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { } },
	# {'mtp': 'remove_tip_rack_from_deck_inventory', 'pld': { "tip_rack_id": "33" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { } },
	# {'mtp': 'remove_tip_rack_from_deck_inventory', 'pld': { "tip_rack_id": "44" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { } },
	# {'mtp': 'remove_tip_rack_from_deck_inventory', 'pld': { "tip_rack_id": "77" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { } },
	# GetTipCountForTipContainer
	# SetTipCountForTipContainer
	{'mtp': 'start_skipping', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'stop_skipping', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	##### Media Inventory
	# AddMediaToDeckInventory
	# RemoveMediaAtLocationFromDeckInventory
	# GetMediaTypeListInLocation
	# FindMediaTypeLocationsInInventory
	# GetMediaVolumeULInLocation
	# SetMediaVolumeULInLocation
	##### Plate Movements
	# {'mtp': 'move_plate', 'pld': { "plate_type": PLATE_TYPE2, "plate_id": "77", "from_primary_location": "enumNP", "from_sub_location": "2", "from_sub_sub_location": "7", "to_primary_location": "enumCxDSite", "to_sub_location": "1", "to_sub_sub_location": "1" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { } },
	# {'mtp': 'move_plate_read_barcode', 'pld': { "plate_type": PLATE_TYPE2, "plate_id": "77", "from_primary_location": "enumNP", "from_sub_location": "1", "from_sub_sub_location": "3", "to_primary_location": "enumCxDSite1", "to_sub_location": "1", "to_sub_sub_location": "2" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { } },
	# {'mtp': 'remove_plate_from_deck_inventory', 'pld': { "plate_id": "77" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'delid_plate', 'pld': { "plate_type": PLATE_TYPE2, "plate_primary_location": "enumNP", "plate_sub_location": "2", "plate_sub_sub_location": "3", "lid_primary_location": "enumLidStage1", "lid_sub_location": None, "lid_sub_sub_location": None }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'relid_plate', 'pld': { "plate_type": PLATE_TYPE2, "plate_primary_location": "enumNP", "plate_sub_location": "4", "plate_sub_sub_location": "5", "lid_primary_location": "enumLidStage1", "lid_sub_location": None, "lid_sub_sub_location": None }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	##### Tip Rack Movements
	# {'mtp': 'move_tip_rack', 'pld': { "tip_type": TIP_TYPE2, "tip_rack_id": "44", "from_primary_location": "enumVTS", "from_sub_location": "2", "from_sub_sub_location": "1", "to_primary_location": "enumVTS", "to_sub_location": "2", "to_sub_sub_location": "1" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { } },
	# {'mtp': 'remove_tip_rack_from_deck_inventory', 'pld': { "tip_rack_id": "44" }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { } },
	# MoveTipRackReadBarcode
	{'mtp': 'unregister_as_client', 'pld': { 'requesting_client_guid': CLIENT2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': {'unregistration_successful': True}}
]

def regression_test_one(command_id, details):
	commandObj = {
		"message_type": details['mtp'],
		"transport_version": "1.0",
		"command_group_id": COMMAND_GROUP_ID,
		"command_id": command_id,
		"session_id": details['cid'],
		"machine_id": MACHINE_ID,
		"payload": details['pld']
	}
	shoebox['ack']['expected_status_code'] = details['ast']
	shoebox['status']['expected_status_code'] = details['sst']
	shoebox['sent_cmd_id'] : command_id
	shoebox['status']['expected_payload'] = details['expld']
	
	if (details['mtp'] == 'start_skipping'):
		shoebox['skipping'] = True
	elif (details['mtp'] == 'stop_skipping'):
		shoebox['skipping'] = False
	elif (details['mtp'] == 'delay'):
		payload = details['pld']
		time_s = payload['seconds']
		time.sleep(time_s)
	else:
		if (shoebox['skipping'] == False):
			send_command(g_client, shoebox, commandObj)

'''
camera_defaults = None
with open(DEFAULT_PROPERTIES_PATH + "\Camera.cellx_props", "r") as f:
    camera_defaults = json.load(f)
print( camera_defaults["enumCamera"]["vendor"] )
'''

def regression_test_all():
	g_client.on_message=on_message #attach function to callback
	g_client.user_data_set(shoebox)

	print("connecting to broker")
	g_client.connect(BROKER_ADDRESS) #connect to broker
	print("Subscribing to topic","tbd/microscope/command")
	g_client.subscribe([(TOPIC_ACK, 0), (TOPIC_STATUS, 0)])

	COMMAND=100
	for i in command_list:
		regression_test_one(COMMAND, i)
		COMMAND +=1


regression_test_all()
