import time
import json
import paho.mqtt.client as mqtt #import the client1
import paho.mqtt.publish as publish
from settings import BROKER_ADDRESS
from settings import CLIENT2
from settings import CLIENT3

def send_command(command_payload):
	publish.single(topic="tbd/machine/command", payload=command_payload, qos=2, retain=False, hostname=BROKER_ADDRESS, port=1883, client_id="mypub1", keepalive=60)
'''
# Should succeed and answer "false":
send_command('{ "message_type": "does_master_client_exist", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "who-am-i", "machine_id": "CX-2345", "payload": { } }')

# Expect serial nuber '123':
send_command('{ "message_type": "get_system_serial_number", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "who-am-i", "machine_id": "CX-2345", "payload": { } }')

# Should succeed:
send_command('{ "message_type": "get_firmware_version_number", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "who-am-i", "machine_id": "CX-2345", "payload": { } }')

# Should fail with status_code 403:
send_command('{ "message_type": "get_current_system_configuration", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "who-am-i", "machine_id": "CX-2345", "payload": { } }')

# Should fail with status_code 403:
send_command('{ "message_type": "get_current_z_step", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "who-am-i", "machine_id": "CX-2345", "payload": { } }')

# Should fail with status_code 403:
send_command('{ "message_type": "get_tip", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "who-am-i", "machine_id": "CX-2345", "payload": { "tip_type": 0, "tip_axis": "enumZAxis", "tip_number": 1 } }')
'''
# Now register as master...
# Should succeed:
send_command('{ "message_type": "register_as_master_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')
'''
# Should succeed and answer "true":
send_command('{ "message_type": "does_master_client_exist", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# Should succeed:
send_command('{ "message_type": "get_firmware_version_number", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')
'''
# Should succeed:
send_command('{ "message_type": "get_current_system_configuration", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')
'''
# Should succeed (meaning sending a status, wich will still be empty):
send_command('{ "message_type": "get_current_z_step", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

# Should succeed (meaning sending a status, wich will still be empty):
send_command('{ "message_type": "get_tip", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "tip_type": 0, "tip_axis": "enumZAxis", "tip_number": 1 } }')

# Now add an observer...
# Should succeed:
send_command('{ "message_type": "register_as_observer_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT3 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT3 + '" } }')

# Should succeed:
send_command('{ "message_type": "get_firmware_version_number", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT3 + '", "machine_id": "CX-2345", "payload": { } }')

# Should succeed:
send_command('{ "message_type": "get_current_system_configuration", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT3 + '", "machine_id": "CX-2345", "payload": { } }')

# Should succeed (meaning sending a status, wich will still be empty):
send_command('{ "message_type": "get_current_z_step", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT3 + '", "machine_id": "CX-2345", "payload": { } }')

# Should fail with status_code 403:
send_command('{ "message_type": "move_to_z_step", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT3 + '", "machine_id": "CX-2345", "payload": { "z_step": 7, } }')

send_command('{ "message_type": "move_to_z_relative_step", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "z_offset_step": -66, } }')

send_command('{ "message_type": "move_to_z_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "z_nm": 777, } }')

send_command('{ "message_type": "move_to_z_relative_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "z_offset_nm": 545, } }')

send_command('{ "message_type": "get_z_current_nm", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": {  } }')

# Should fail with status_code 403:
send_command('{ "message_type": "get_tip", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT3 + '", "machine_id": "CX-2345", "payload": { "tip_type": 0, "tip_axis": "enumZAxis", "tip_number": 1 } }')

send_command('{ "message_type": "set_system_serial_number", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "serial_number": "9908", } }')

# Expect "9908"
send_command('{ "message_type": "get_system_serial_number", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "who-am-i", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "set_system_serial_number", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "serial_number": "123", } }')
'''
# Should succeed:
send_command('{ "message_type": "unregister_as_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')
'''
# Should succeed:
send_command('{ "message_type": "unregister_as_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT3 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT3 + '" } }')
'''
