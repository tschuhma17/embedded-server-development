import paho.mqtt.client as mqtt #import the client1
import time
from settings import BROKER_ADDRESS
############
def on_message(client, userdata, message):
    print("message received " ,str(message.payload.decode("utf-8")))
    print("message topic=",message.topic)
    print("message qos=",message.qos)
    print("message retain flag=",message.retain)
########################################
print("creating new instance")
client = mqtt.Client("myclient3") #create new instance
client.on_message=on_message #attach function to callback
print("connecting to broker")
client.connect(BROKER_ADDRESS) #connect to broker
client.loop_start() #start the loop
print("Subscribing to topic","$SYS/#")
client.subscribe("$SYS/#")
time.sleep(1000) # wait
client.loop_stop() #stop the loop


