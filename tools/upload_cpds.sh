#!/usr/bin/sh

CPD_PATH="/media/psf/Home/Desktop/wei/cpds"

python pub_cpd.py ${CPD_PATH}/XX\ Axis.cellx_cpd 1 enumXXAxis
sleep 1
python pub_cpd.py ${CPD_PATH}/YY\ Axis.cellx_cpd 2 enumYYAxis
sleep 1
python pub_cpd.py ${CPD_PATH}/Z\ Axis.cellx_cpd 3 enumZAxis
sleep 1
python pub_cpd.py ${CPD_PATH}/ZZ\ Axis.cellx_cpd 4 enumZZAxis
sleep 1
python pub_cpd.py ${CPD_PATH}/ZZZ\ Axis.cellx_cpd 5 enumZZZAxis
sleep 1
python pub_cpd.py ${CPD_PATH}/XY\ Axes.cellx_cpd 6 enumXYAxes
sleep 1
python pub_cpd.py ${CPD_PATH}/Aspiration\ Pump\ 1.cellx_cpd 7 enumAspirationPump1
sleep 1
python pub_cpd.py ${CPD_PATH}/Dispense\ Pump\ 1.cellx_cpd 8 enumDispensePump1
sleep 1
python pub_cpd.py ${CPD_PATH}/Dispense\ Pump\ 2.cellx_cpd 9 enumDispensePump2
sleep 1
python pub_cpd.py ${CPD_PATH}/Dispense\ Pump\ 3.cellx_cpd 10 enumDispensePump3
sleep 1
python pub_cpd.py ${CPD_PATH}/Picking\ Pump\ 1.cellx_cpd 11 enumPickingPump1
sleep 1
python pub_cpd.py ${CPD_PATH}/White\ Light\ Source.cellx_cpd 12 enumWhiteLightSource
sleep 1
python pub_cpd.py ${CPD_PATH}/Fluorescence\ Light\ Filter.cellx_cpd 13 enumFluorescenceLightFilter
sleep 1
python pub_cpd.py ${CPD_PATH}/Annular\ Ring\ Holder.cellx_cpd 14 enumAnnularRingHolder
sleep 1
python pub_cpd.py ${CPD_PATH}/Barcode\ Reader.cellx_cpd 15 enumBarcodeReader
sleep 1
python pub_cpd.py ${CPD_PATH}/Camera.cellx_cpd 16 enumCamera
sleep 1
python pub_cpd.py ${CPD_PATH}/CxD.cellx_cpd 17 enumCxD
sleep 1
python pub_cpd.py ${CPD_PATH}/CxPM.cellx_cpd 18 enumCxPM
sleep 1
# Not supported yet.
# python pub_cpd.py ${CPD_PATH}/Incubator.cellx_cpd 19 enumIncubator
# sleep 1
python pub_cpd.py ${CPD_PATH}/Optical\ Column.cellx_cpd 20 enumOpticalColumn
