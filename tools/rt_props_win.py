import time
import json
import paho.mqtt.client as mqtt #import the client1
import paho.mqtt.publish as publish
import math
from settings import BROKER_ADDRESS
from settings import CLIENT2
from settings import CLIENT3
from tip_types import TIP_TYPE1
from tip_types import TIP_TYPE2
from plate_types import PLATE_TYPE1
from plate_types import PLATE_TYPE2
TOPIC_COMMAND="tbd/machine/command"
TOPIC_ACK="tbd/machine/ack"
TOPIC_STATUS="tbd/machine/status"
COMMAND_GROUP_ID=234
MACHINE_ID="CX-2345"
FW_VERSION="0.1.29"
DEFAULT_PROPERTIES_PATH="C:/temp/json_props"
DEFAULT_CPDS_PATH="C:/temp"

shoebox = {
	'ack' : {
		'received': False,
		'expected_status_code' : 200
	},
	'status' : {
		'received': False,
		'expected_status_code' : 200
	},
	'skipping' : False
}

g_client = mqtt.Client(CLIENT2) #create new instance

# send_command()
# Uses client to send the prepared command_payload to the TOPIC_COMMAND.
# The on_message command handles received messages. We expect to receive an ack
# and a status. This command waits until both are received to exit.
# userdata is an object shared with g_client so send_command() and on_message()
# so both have access to this shared object. (It is shoebox.)
def send_command(client, userdata, command_payload):
	command_as_string = json.dumps(command_payload)
	# print(command_as_string)
	userdata['ack']['received'] = False
	userdata['status']['received'] = False
	if (userdata['status']['expected_status_code'] == 0):
		userdata['status']['received'] = True # expected_status_code of 0 means we don't expect a response, so don't wait for one.
	userdata['sent_cmd_id'] = command_payload["command_id"]
	userdata['passed'] = True;
	# print("SEND COMMAND   : ", command_as_string)
	client.publish(TOPIC_COMMAND, command_as_string)
	while (userdata['ack']['received'] == False) or (userdata['status']['received'] == False):
		g_client.loop()
	if (userdata['passed'] == True):
		print("passed: ", command_payload['message_type'])
	else:
		print("FAILED: ", command_payload['message_type'])


# on_message() is a handler function passed to g_client. It handles any message that
# is received. We only expect 2 kinds: acks and statuses returned in response to
# a command.
# For an ack, on_message() tests that the status_code matches the expected value.
# For a status, on_message() tests that both the status_code and the payload matches
# expected values. (Sometimes, bad status codes or payloads is expected.)
def on_message(client, userdata, message):
	# print("msg: ", str(message.payload.decode("utf-8")))
	des_payload = json.loads(message.payload.decode("utf-8"))
	topic_in = bytes.decode(message._topic)
	sent_id = userdata['sent_cmd_id']
	received_id = des_payload['command_id']
	if (topic_in == TOPIC_ACK) and (sent_id == received_id):
		# print("RECEIVED ACK   : ", des_payload)
		userdata['ack']['received'] = True
		if (des_payload["status_code"] != userdata['ack']['expected_status_code']):
			userdata['passed'] = False
			print("FAIL: ack status_code is wrong: ", des_payload["status_code"])
			userdata['status']['received'] = True # Not really, but we don't expect a response in this situation.
	if (topic_in == TOPIC_STATUS) and (sent_id == received_id):
		# print("RECEIVED STATUS: ", des_payload)
		userdata['status']['received'] = True
		if (des_payload["status_code"] != userdata['status']['expected_status_code']):
			userdata['passed'] = False
			print("FAIL: status's status_code is wrong: ", des_payload["status_code"])
		else:
			userdata['passed'] = payloads_match(des_payload['payload'], userdata['status']['expected_payload'])
			# print("result =", userdata['passed'])


# payloads_match() compares two objects for likeness. These are meant to be payload
# objects, which are supposed to be dict objects or Nones.
# The received payload may have key/value pairs that we cannot anticipate or that
# we just don't care about. So this test is focused on checking that every key/value
# pair in the expected dict match the ones in the received dict.
def payloads_match(received, expected):
	if (received is None):
		if (expected is None):
			return True
		else:
			print(received)
			return False
	# Received is something.
	if (expected is None):
		print(received)
		return False
	# Both received and expected are somethings.
	expectations_list = sorted(expected.items())
	for k, v in expectations_list:
		if (k == "props_file_name"):
			return property_collections_match(received, v)
		if (k in received.keys()):
			if (type(v) == type(received[k])):
				if (v != received[k]):
					print(received)
					return False
			else:
				print(received)
				return False
		else:
			print(received)
			return False
	return True

def property_collections_match(received_payload, props_file_name):
	matches = False
	expected_props = None
	with open(DEFAULT_PROPERTIES_PATH + "/" + props_file_name, "r") as f:
		expected_props = json.load(f)
	expected_first = list(expected_props.items())[0]
	received_first = list(received_payload.items())[0]
	# print(expected_first[0])
	# print(received_first[0])
	if (type(expected_first[1]) == dict) and (type(received_first[1]) == dict):
		# print(received_first[1]["vendor"])
		matches = properties_match(received_first[1], expected_first[1])
	else:
		print("expected_first[1] and received_first[1] are different types")
		matches = False
	return matches

def properties_match(received_properties, expected_properties):
	matches = True
	# print(expected_properties)
	for k, v in expected_properties.items():
		if k not in received_properties.keys():
			print("match not found for ", k)
			matches = False
		elif (type(v) != type(received_properties[k])):
			matches = False
		elif ((type(v) == list) and (type(expected_properties[k]) == list)):
			matches = (matches and lists_match(k, received_properties[k], expected_properties[k]))
		else:
			matches = (matches and property_matches(k, received_properties[k], expected_properties[k]))
	return matches

def lists_match(key, received_list, expected_list):
	ex_len = len(expected_list)
	matches = True
	if (ex_len != len(received_list)):
		return False
	for i in range(ex_len):
		matches = (matches and properties_match(received_list[i], expected_list[i]))
	return matches

def property_matches(key, received_property, expected_property):
	matches = True
	matches = (matches and field_matches(key, "data_type", received_property, expected_property))
	matches = (matches and field_matches(key, "base_unit", received_property, expected_property))
	matches = (matches and field_matches(key, "es_min_value", received_property, expected_property))
	matches = (matches and field_matches(key, "es_max_value", received_property, expected_property))
	matches = (matches and field_matches(key, "setting", received_property, expected_property))
	matches = (matches and field_matches(key, "scale_factor", received_property, expected_property))
	matches = (matches and field_matches(key, "persist_type", received_property, expected_property))
	matches = (matches and field_matches(key, "editability_type", received_property, expected_property))
	matches = (matches and field_matches(key, "points_past_decimal", received_property, expected_property))
	matches = (matches and field_matches(key, "display_directive", received_property, expected_property))
	matches = (matches and field_matches(key, "display_min_value", received_property, expected_property))
	matches = (matches and field_matches(key, "display_max_value", received_property, expected_property))
	return matches

def field_matches(prop_key, field_key, received_property, expected_property):
	matches = True
	exp_val = expected_property[field_key]
	if exp_val == "":
		exp_val = None  # TODO: Confirm that it is OK equate "" with null.
	if exp_val == 0:
		exp_val = None
	rec_val = received_property[field_key]
	if rec_val == "":
		rec_val = None  # TODO: Confirm that it is OK equate "" with null.
	if rec_val == 0:
		rec_val = None
	if (exp_val != rec_val):
		matches = False
		print("property=" + str(prop_key) + "|field=" + str(field_key) + "|expected-value=" + str(exp_val) + "|received-value=" + str(rec_val))
		# If the expected value is of type float, test it again in a float-specific way
		if isinstance(exp_val, float):
			if isinstance(rec_val, float):
				if math.isclose( exp_val, rec_val, rel_tol=1e-5 ):
					matches = True
			else:
				matches = False
		if not matches:
			print(prop_key, ":", field_key, " expected value [", expected_property[field_key], "] doesn't match received value [", received_property[field_key], "]", sep='')
	return matches


# Trying to be terse here, to pack a lot in. Here is a legend:
# 'mtp' is 'message_type'. 'pld' is the command's outgoing payload. 'ast' is the expected ack status_code. 'sst' is the expected status status_code.
# 'expld' is the payload expected to be returned in the status message. This should only contain the parts of the payload that can be known in advance.
command_list = [
	##### General Commands
	{'mtp': 'register_as_master_client', 'pld': { 'requesting_client_guid': CLIENT2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': {'registration_successful': True}},
	##### CxD
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 2, 'cpd_body': 'CxD.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_cxd_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'CxD.cellx_props' }},
	##### XY Axis Motion
	{'mtp': 'start_skipping', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 3, 'cpd_body': 'XY_Axes.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 3, 'component_type': 'enumXYAxes'  }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 3, 'cpd_body': 'XY_Axes.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_xy_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'XY_Axes.cellx_props' }},
	# SetXYProperties
	##### XX Axis Motion
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 4, 'cpd_body': 'XX_Axis.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 4, 'component_type': 'enumXXAxis' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 4, 'cpd_body': 'XX_Axis.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_xx_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'XX_Axis.cellx_props' }},
	# SetXXProperties
	##### YY Axis Motion
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 5, 'cpd_body': 'YY_Axis.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 5, 'component_type': 'enumYYAxis' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 5, 'cpd_body': 'YY_Axis.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_yy_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'YY_Axis.cellx_props' }},
	# SetYYProperties
	##### Z Axis Motion
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 6, 'cpd_body': 'Z_Axis.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 6, 'component_type': 'enumZAxis' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 6, 'cpd_body': 'Z_Axis.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_z_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'Z_Axis.cellx_props' }},
	# SetZProperties
	##### ZZ Axis Motion
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 7, 'cpd_body': 'ZZ_Axis.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 7, 'component_type': 'enumZZAxis' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 7, 'cpd_body': 'ZZ_Axis.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_zz_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'ZZ_Axis.cellx_props' }},
	# SetZZProperties
	##### ZZZ Axis Motion
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 8, 'cpd_body': 'ZZZ_Axis.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 8, 'component_type': 'enumZZZAxis' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 8, 'cpd_body': 'ZZZ_Axis.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_zzz_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'ZZZ_Axis.cellx_props' }},
	# SetZZZProperties
	##### Aspiration Pump
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 9, 'cpd_body': 'Aspiration_Pump_1.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 9, 'component_type': 'enumAspirationPump1' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 9, 'cpd_body': 'Aspiration_Pump_1.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_aspiration_pump_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'Aspiration_Pump_1.cellx_props' }},
	# SetAspirationPumpProperties
	##### Dispense Pump 1
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 10, 'cpd_body': 'Dispense_Pump_1.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 10, 'component_type': 'enumDispensePump1' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 10, 'cpd_body': 'Dispense_Pump_1.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_dispense_pump_1_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'Dispense_Pump_1.cellx_props' }},
	# SetDispensePump1Properties
	##### Dispense Pump 2
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 11, 'cpd_body': 'Dispense_Pump_2.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 11, 'component_type': 'enumDispensePump2' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 11, 'cpd_body': 'Dispense_Pump_2.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_dispense_pump_2_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'Dispense_Pump_2.cellx_props' }},
	# SetDispensePump2Properties
	##### Dispense Pump 3
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 12, 'cpd_body': 'Dispense_Pump_3.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 12, 'component_type': 'enumDispensePump3' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 12, 'cpd_body': 'Dispense_Pump_3.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_dispense_pump_3_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'Dispense_Pump_3.cellx_props' }},
	# SetDispensePump3Properties
	##### Picking Pump
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 13, 'cpd_body': 'Picking_Pump_1.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 13, 'component_type': 'enumPickingPump1' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 13, 'cpd_body': 'Picking_Pump_1.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_picking_pump_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'Picking_Pump_1.cellx_props' }},
	# SetPickingPumpProperties
	##### White Light Source
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 14, 'cpd_body': 'White_Light_Source.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 14, 'component_type': 'enumWhiteLightSource' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 14, 'cpd_body': 'White_Light_Source.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_white_light_source_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'White_Light_Source.cellx_props' }},
	# SetWhiteLightSourceProperties
	##### Fluorescence Light Filter
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 15, 'cpd_body': 'Fluorescence_Light_Filter.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 15, 'component_type': 'enumFluorescenceLightFilter' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 15, 'cpd_body': 'Fluorescence_Light_Filter.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_fluorescence_light_filter_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'Fluorescence_Light_Filter.cellx_props' }},
	# SetFluorescenceLightSourceProperties
	##### Annular Ring Holder
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 16, 'cpd_body': 'Annular_Ring_Holder.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 16, 'component_type': 'enumAnnularRingHolder' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 16, 'cpd_body': 'Annular_Ring_Holder.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_annular_ring_holder_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'Annular_Ring_Holder.cellx_props' }},
	# SetAnnularRingHolderProperties
	##### Camera
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 17, 'cpd_body': 'Camera.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 17, 'component_type': 'enumCamera' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 17, 'cpd_body': 'Camera.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_camera_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'Camera.cellx_props' }},
	# SetCameraProperties
	##### Optical Column
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 18, 'cpd_body': 'Optical_Column.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 18, 'component_type': 'enumOpticalColumn' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'stop_skipping', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 18, 'cpd_body': 'Optical_Column.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_optical_column_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'Optical_Column.cellx_props' }},
	{'mtp': 'start_skipping', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# SetOpticalColumnProperties
	##### Incubator
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 19, 'cpd_body': 'Incubator.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 19, 'component_type': 'enumIncubator' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 19, 'cpd_body': 'Incubator.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_incubator_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'Incubator.cellx_props' }},
	# SetIncubatorProperties
	##### CxPM
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 20, 'cpd_body': 'CxPM.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 20, 'component_type': 'enumCxPM' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 20, 'cpd_body': 'CxPM.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_cxpm_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'CxPM.cellx_props' }},
	# SetCxPMProperties
	##### Barcode Reader
	# {'mtp': 'upload_cpd', 'pld': { 'cpd_id': 21, 'cpd_body': 'Barcode_Reader.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# {'mtp': 'apply_cpd_to_component', 'pld': { 'cpd_id': 21, 'component_type': 'enumBarcodeReader' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'upload_and_apply_cpd', 'pld': { 'cpd_id': 21, 'cpd_body': 'Barcode_Reader.cellx_cpd' }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_barcode_reader_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { 'props_file_name': 'Barcode_Reader.cellx_props' }},
	##### Convenience functions. Copy these around to effectively comment out large swathes of this list.
	{'mtp': 'start_skipping', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	{'mtp': 'stop_skipping', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	##### Convenience function. Use this to insert a time-delay between commands. Is mostly useful for pump-start and pump-stop commands.
	{'mtp': 'delay', 'pld': { 'seconds': 0 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { }},
	# SetBarcodeReaderProperties
	##### Digital Inputs
	##### Digital Outputs
	##### Compund Commands
	##### Plate Inventory
	##### Plate Lid Inventory
	##### Tip Rack Inventory
	##### Media Inventory
	##### Plate Movements
	##### Tip Rack Movements
	{'mtp': 'unregister_as_client', 'pld': { 'requesting_client_guid': CLIENT2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': {'unregistration_successful': True}}
]

def regression_test_one(command_id, details):
	commandObj = {
		"message_type": details['mtp'],
		"transport_version": "1.0",
		"command_group_id": COMMAND_GROUP_ID,
		"command_id": command_id,
		"session_id": details['cid'],
		"machine_id": MACHINE_ID,
		"payload": details['pld']
	}
	shoebox['ack']['expected_status_code'] = details['ast']
	shoebox['status']['expected_status_code'] = details['sst']
	shoebox['sent_cmd_id'] : command_id
	shoebox['status']['expected_payload'] = details['expld']

	# If the command is 'upload_cpd', change 'cpd_body' from a file name to the file contents.
	if ((details['mtp'] == 'upload_cpd') or (details['mtp'] == 'upload_and_apply_cpd')):
		cpd_file_name = details['pld']['cpd_body']
		cpd_file_path = DEFAULT_CPDS_PATH + "/" + cpd_file_name
		if (shoebox['skipping'] == True):
			print(cpd_file_path)
		else:
			with open(cpd_file_path, "r") as f:
				commandObj['payload']['cpd_body'] = json.load(f)
				print (commandObj['payload']['cpd_id'])
	if (details['mtp'] == 'start_skipping'):
		shoebox['skipping'] = True
	elif (details['mtp'] == 'stop_skipping'):
		shoebox['skipping'] = False
	elif (details['mtp'] == 'delay'):
		payload = details['pld']
		time_s = payload['seconds']
		time.sleep(time_s)
	else:
		if (shoebox['skipping'] == False):
			send_command(g_client, shoebox, commandObj)

def regression_test_all():
	g_client.on_message=on_message #attach function to callback
	g_client.user_data_set(shoebox)

	print("connecting to broker")
	g_client.connect(BROKER_ADDRESS) #connect to broker
	print("Subscribing to topic",TOPIC_STATUS)
	g_client.subscribe([(TOPIC_ACK, 0), (TOPIC_STATUS, 0)])

	COMMAND=100
	for i in command_list:
		regression_test_one(COMMAND, i)
		COMMAND +=1


regression_test_all()
