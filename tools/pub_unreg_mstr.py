import paho.mqtt.client as mqtt #import the client1
from settings import BROKER_ADDRESS
client = mqtt.Client("mypub1") #create new instance
client.connect(BROKER_ADDRESS) #connect to broker
client.publish("tbd/machine/command",
'{ "message_type": "unregister_as_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "83386d73-21d1-49bd-9b73-3ecdfbc2b819", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "83386d73-21d1-49bd-9b73-3ecdfbc2b819" } }')#publish
