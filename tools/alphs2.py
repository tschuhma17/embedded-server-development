import sys
import json
from pathlib import Path
# Usage:   python alphs2.py json-file
# Example: python alphs2.py xx_props.json
# Debugging: python -m pdb alphs2.py json-file
def sort_and_print_one_level(key, value, indent):
	if(type(value) is dict):
		print (indent, key, ':')
		value_list = sorted(value.items())
		indent = indent + '\t';
		for k, v in value_list:
			sort_and_print_one_level(k, v, indent)
	else:
		print (indent, key, ':', value)
		return

def sort_and_print_all(fname):
	with open(fname) as file:
		# Parse the file into a python object.
		jdata1 = json.load(file)
		# Recursively parse the the contents.
		sort_and_print_one_level('', jdata1, '')

if __name__ == "__main__":
	fname = sys.argv[1]
	fpath = Path(fname)
	if fpath.is_file():
		sort_and_print_all(fname)
	else:
		print('File does not exist');
