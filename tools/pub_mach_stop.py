import paho.mqtt.client as mqtt #import the client1
from settings import BROKER_ADDRESS
client = mqtt.Client("mypub1") #create new instance
client.connect(BROKER_ADDRESS) #connect to broker
client.publish("tbd/machine/command",'{"message_type": "hold_all_operations", "command_id": 2345, "machine_id": "CX-2345", "payload": { } }')#publish
