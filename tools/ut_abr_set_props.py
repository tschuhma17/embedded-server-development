import time
import json
import paho.mqtt.client as mqtt #import the client1
import paho.mqtt.publish as publish
from settings import BROKER_ADDRESS
from settings import CLIENT2
from settings import CLIENT3
from tip_types import TIP_TYPE2
from plate_types import PLATE_TYPE2
TOPIC_COMMAND="tbd/machine/command"
TOPIC_ACK="tbd/machine/ack"
TOPIC_STATUS="tbd/machine/status"
COMMAND_GROUP_ID=234
MACHINE_ID="CX-2345"
FW_VERSION="0.1.29"
DEFAULT_PROPERTIES_PATH="/media/psf/Home/Desktop/wei/props"

shoebox = {
	'ack' : {
		'received': False,
		'expected_status_code' : 200
	},
	'status' : {
		'received': False,
		'expected_status_code' : 200
	}
}

g_client = mqtt.Client(CLIENT2) #create new instance

# send_command()
# Uses client to send the prepared command_payload to the TOPIC_COMMAND.
# The on_message command handles received messages. We expect to receive an ack
# and a status. This command waits until both are received to exit.
# userdata is an object shared with g_client so send_command() and on_message()
# so both have access to this shared object. (It is shoebox.)
def send_command(client, userdata, command_payload):
	command_as_string = json.dumps(command_payload)
	userdata['ack']['received'] = False
	userdata['status']['received'] = False
	if (userdata['status']['expected_status_code'] == 0):
		userdata['status']['received'] = True # expected_status_code of 0 means we don't expect a response, so don't wait for one.
	userdata['sent_cmd_id'] = command_payload["command_id"]
	userdata['passed'] = True;
	# print("SEND COMMAND   : ", command_as_string)
	client.publish(TOPIC_COMMAND, command_as_string)
	while (userdata['ack']['received'] == False) or (userdata['status']['received'] == False):
		g_client.loop()
	if (userdata['passed'] == True):
		print("passed: ", command_payload['message_type'])
	else:
		print("FAILED: ", command_payload['message_type'])


# on_message() is a handler function passed to g_client. It handles any message that
# is received. We only expect 2 kinds: acks and statuses returned in response to
# a command.
# For an ack, on_message() tests that the status_code matches the expected value.
# For a status, on_message() tests that both the status_code and the payload matches
# expected values. (Sometimes, bad status codes or payloads is expected.)
def on_message(client, userdata, message):
	# print("msg: ", str(message.payload.decode("utf-8")))
	des_payload = json.loads(message.payload.decode("utf-8"))
	topic_in = bytes.decode(message._topic)
	sent_id = userdata['sent_cmd_id']
	received_id = des_payload['command_id']
	if (topic_in == TOPIC_ACK) and (sent_id == received_id):
		# print("RECEIVED ACK   : ", des_payload)
		userdata['ack']['received'] = True
		if (des_payload["status_code"] != userdata['ack']['expected_status_code']):
			userdata['passed'] = False
			print("FAIL: ack status_code is wrong: ", des_payload["status_code"])
			userdata['status']['received'] = True # Not really, but we don't expect a response in this situation.
	if (topic_in == TOPIC_STATUS) and (sent_id == received_id):
		# print("RECEIVED STATUS: ", des_payload)
		userdata['status']['received'] = True
		if (des_payload["status_code"] != userdata['status']['expected_status_code']):
			userdata['passed'] = False
			print("FAIL: status's status_code is wrong: ", des_payload["status_code"])
		else:
			userdata['passed'] = payloads_match(des_payload['payload'], userdata['status']['expected_payload'])
			# print("result =", userdata['passed'])


# payloads_match() compares two objects for likeness. These are meant to be payload
# objects, which are supposed to be dict objects or Nones.
# The received payload may have key/value pairs that we cannot anticipate or that
# we just don't care about. So this test is focused on checking that every key/value
# pair in the expected dict match the ones in the received dict.
def payloads_match(received, expected):
	if (received is None):
		if (expected is None):
			return True
		else:
			print(received)
			return False
	# Received is something.
	if (expected is None):
		print(received)
		return False
	# Both received and expected are somethings.
	expectations_list = sorted(expected.items())
	for k, v in expectations_list:
		if (k == "props_file_name"):
			return property_collections_match(received, v)
		if (k in received.keys()):
			if (type(v) == type(received[k])):
				if (v != received[k]):
					print(received)
					return False
			else:
				print(received)
				return False
		else:
			print(received)
			return False
	return True

def property_collections_match(received_payload, props_file_name):
	matches = False
	expected_props = None
	with open(DEFAULT_PROPERTIES_PATH + "/" + props_file_name, "r") as f:
		expected_props = json.load(f)
	expected_first = list(expected_props.items())[0]
	received_first = list(received_payload.items())[0]
	# print(expected_first[0])
	# print(received_first[0])
	if (type(expected_first[1]) == dict) and (type(received_first[1]) == dict):
		# print(received_first[1])
		matches = properties_match(received_first[1], expected_first[1])
	else:
		print("expected_first[1] and received_first[1] are different types")
		matches = False
	return matches

def properties_match(received_properties, expected_properties):
	matches = True
	# print(expected_properties)
	for k, v in expected_properties.items():
		if k not in received_properties.keys():
			print("match not found for ", k)
			matches = False
		elif (type(v) != type(received_properties[k])):
			print("Types don't match")
			matches = False
		elif ((type(v) == list) and (type(expected_properties[k]) == list)):
			# print("processing a list")
			matches = (matches and lists_match(k, received_properties[k], expected_properties[k]))
		else:
			# print("processing a dictionary")
			matches = (matches and property_matches(k, received_properties[k], expected_properties[k]))
	return matches

def lists_match(key, received_list, expected_list):
	ex_len = len(expected_list)
	matches = True
	if (ex_len != len(received_list)):
		print("length check failed")
		return False
	for i in range(ex_len):
		matches = (matches and properties_match(received_list[i], expected_list[i]))
	return matches

def property_matches(key, received_property, expected_property):
	matches = True
	matches = (matches and field_matches(key, "data_type", received_property, expected_property))
	matches = (matches and field_matches(key, "base_unit", received_property, expected_property))
	matches = (matches and field_matches(key, "es_min_value", received_property, expected_property))
	matches = (matches and field_matches(key, "es_max_value", received_property, expected_property))
	matches = (matches and field_matches(key, "setting", received_property, expected_property))
	matches = (matches and field_matches(key, "scale_factor", received_property, expected_property))
	matches = (matches and field_matches(key, "persist_type", received_property, expected_property))
	matches = (matches and field_matches(key, "editability_type", received_property, expected_property))
	matches = (matches and field_matches(key, "points_past_decimal", received_property, expected_property))
	matches = (matches and field_matches(key, "display_directive", received_property, expected_property))
	matches = (matches and field_matches(key, "display_min_value", received_property, expected_property))
	matches = (matches and field_matches(key, "display_max_value", received_property, expected_property))
	return matches

def field_matches(prop_key, field_key, received_property, expected_property):
	matches = True
	exp_val = expected_property[field_key]
	if exp_val == "":
		exp_val = None  # TODO: Confirm that it is OK equate "" with null.
	if exp_val == 0:
		exp_val = None
	rec_val = received_property[field_key]
	if rec_val == "":
		rec_val = None  # TODO: Confirm that it is OK equate "" with null.
	if rec_val == 0:
		rec_val = None
	if (exp_val != rec_val):
		print(prop_key, ":", field_key, " value [", expected_property[field_key], "] doesn't match [", received_property[field_key], "]", sep='')
		matches = False
	return matches


# Trying to be terse here, to pack a lot in. Here is a legend:
# 'mtp' is 'message_type'. 'pld' is the command's outgoing payload. 'ast' is the expected ack status_code. 'sst' is the expected status status_code.
# 'expld' is the payload expected to be returned in the status message. This should only contain the parts of the payload that can be known in advance.
command_list = [
	{'mtp': 'register_as_master_client', 'pld': { 'requesting_client_guid': CLIENT2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': {'registration_successful': True}},

	{'mtp': 'set_z_properties', 'pld': { "enumZAxis" : { "Kei": { "setting": 21, "editability_type": "enumEditableCPD_Application", "persist_type": "enumPersist", "data_type": "uint32_t", "base_unit": None, "es_min_value": 1, "es_max_value": 91, "scale_factor": 1, "points_past_decimal": 0, "display_directive": None,	"display_min_value": 0,	"display_max_value": 90 } } }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_z_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "Kei1.cellx_props" } },
	{'mtp': 'set_z_properties', 'pld': { "enumZAxis" : { "Kei": { "setting": 22 } } }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_z_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "Kei2.cellx_props" } },

	{'mtp': 'set_z_properties', 'pld': { "enumZAxis" : { "Kei": { "data_type": "uint16_t" } } }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'set_z_properties', 'pld': { "enumZAxis" : { "Kei": { "base_unit": "cm" } } }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'set_z_properties', 'pld': { "enumZAxis" : { "Kei": { "es_min_value": 3 } } }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'set_z_properties', 'pld': { "enumZAxis" : { "Kei": { "es_max_value": 93 } } }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'set_z_properties', 'pld': { "enumZAxis" : { "Kei": { "scale_factor": 4 } } }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'set_z_properties', 'pld': { "enumZAxis" : { "Kei": { "points_past_decimal": 5 } } }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'set_z_properties', 'pld': { "enumZAxis" : { "Kei": { "display_directive": "close_up" } } }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'set_z_properties', 'pld': { "enumZAxis" : { "Kei": { "display_min_value": 2 } } }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'set_z_properties', 'pld': { "enumZAxis" : { "Kei": { "display_max_value": 92 } } }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'set_z_properties', 'pld': { "enumZAxis" : { "Kei": { "editability_type": "enumEditableApplicationOnly" } } }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	{'mtp': 'get_z_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "Kei3.cellx_props" } },

	{'mtp': 'set_z_properties', 'pld': { "enumZAxis" : { "Kei": { "persist_type": "enumRemove" } } }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': None },
	# Expect this next one to fail. We don't have a way to test for a lack of a property.
	{'mtp': 'get_z_properties', 'pld': { }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': { "props_file_name": "Kei3.cellx_props" } },

	{'mtp': 'unregister_as_client', 'pld': { 'requesting_client_guid': CLIENT2 }, 'ast': 200, 'sst': 200, 'cid': CLIENT2, 'expld': {'unregistration_successful': True}}
]

def test_one(command_id, details):
	commandObj = {
		"message_type": details['mtp'],
		"transport_version": "1.0",
		"command_group_id": COMMAND_GROUP_ID,
		"command_id": command_id,
		"session_id": details['cid'],
		"machine_id": MACHINE_ID,
		"payload": details['pld']
	}
	shoebox['ack']['expected_status_code'] = details['ast']
	shoebox['status']['expected_status_code'] = details['sst']
	shoebox['sent_cmd_id'] : command_id
	shoebox['status']['expected_payload'] = details['expld']
	
	send_command(g_client, shoebox, commandObj)

def test_all():
	g_client.on_message=on_message #attach function to callback
	g_client.user_data_set(shoebox)

	print("connecting to broker")
	g_client.connect(BROKER_ADDRESS) #connect to broker
	print("Subscribing to topic","tbd/microscope/command")
	g_client.subscribe([(TOPIC_ACK, 0), (TOPIC_STATUS, 0)])

	COMMAND=100
	for i in command_list:
		test_one(COMMAND, i)
		COMMAND +=1


test_all()
