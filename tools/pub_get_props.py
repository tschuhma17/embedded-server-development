import time
import paho.mqtt.client as mqtt #import the client1
import paho.mqtt.publish as publish
from settings import BROKER_ADDRESS
from settings import CLIENT2
# client = mqtt.Client("mypub1") #create new instance
# client.connect(BROKER_ADDRESS) #connect to broker

def send_command(command_payload):
	publish.single(topic="tbd/machine/command", payload=command_payload, qos=2, retain=False, hostname=BROKER_ADDRESS, port=1883, client_id="mypub1", keepalive=60)

send_command('{ "message_type": "register_as_master_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')

send_command('{ "message_type": "get_z_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')
'''
send_command('{ "message_type": "get_zz_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "get_zzz_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "get_xx_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "get_yy_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "get_xy_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "get_aspiration_pump_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "get_dispense_pump_1_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "get_dispense_pump_2_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "get_dispense_pump_3_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "get_picking_pump_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "get_white_light_source_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "get_fluorescence_light_filter_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "get_annular_ring_holder_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "get_optical_column_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')

send_command('{ "message_type": "get_camera_properties", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { } }')
'''
send_command('{ "message_type": "unregister_as_client", "transport_version": "1.0", "command_group_id": 234, "command_id": 1234, "session_id": "' + CLIENT2 + '", "machine_id": "CX-2345", "payload": { "requesting_client_guid": "' + CLIENT2 + '" } }')

