TIP_TYPE1=' "tip_type": { "tip_total_length": 96320000, "tip_working_length": 93647000, "tip_shoulder_length": 5960000, "tip_inner_diameter_at_insertion_end": 6477000, "tip_inner_diameter_at_working_end": 711000, "tip_maximum_working_volume": 1000000, "tip_rack_row_count": 8, "tip_rack_column_count": 12, "tip_rack_length": 127250000, "tip_rack_width": 88390000, "tip_rack_height": 90550000, "tip_rack_seat_to_tip_seat": 1100000, "tip_rack_seat_to_top_of_tip_rack": 9070000, "tip_rack_seat_to_top_of_tip": 6860000, "first_tip_x": 11585000, "first_tip_y": 10375000, "inter_tip_x": 9000000, "inter_tip_y": 9000000, "used_tip_count": 0 }'

TIP_TYPE2= {
	"tip_total_length": 54740000,
	"tip_working_length": 51170000,
	"tip_shoulder_length": 5960000,
	"tip_inner_diameter_at_insertion_end": 6477000,
	"tip_inner_diameter_at_working_end": 533000,
	"tip_maximum_working_volume": 300000,
	"tip_rack_row_count": 12,
	"tip_rack_column_count": 8,
	"tip_rack_length": 127250000,
	"tip_rack_width": 88390000,
	"tip_rack_height": 56750000,
	"tip_rack_seat_to_tip_seat": 1100000,
	"tip_rack_seat_to_top_of_tip_rack": 9070000,
	"tip_rack_seat_to_top_of_tip": 7060000,
	"first_tip_x": 11585000,
	"first_tip_y": 10375000,
	"inter_tip_x": 9000000,
	"inter_tip_y": 9000000,
	"used_tip_count": 0
}
