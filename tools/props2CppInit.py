import sys
import json
from pathlib import Path
# Usage:   python props2CppInit.py json-file
# Example: python props2CppInit.py "Z Axis cellx_props"
# Debugging: python -m pdb props2CppInit.py json-file

def process_one_field(key, component_property):
	if (component_property[key] is None):
		return '{}'
	else:
		return '{\"' + str(component_property[key]) + '\"}'

def rename_editable_type(editable_type):
	if (editable_type == 'enumEditableApplicationOnly'):
		return 'EditableType::EditableApplicationOnly'
	if (editable_type == 'enumEditableCPD_Application'):
		return 'EditableType::EditableCPD_Application'
	if (editable_type == 'enumEditableFixed'):
		return 'EditableType::EditableFixed'
	if (editable_type == 'enumEditableCPDOnly'):
		return 'EditableType::EditableCPDOnly'
	return 'EditableType::TypeUnknown'

def rename_persist_type(persist_type):
	if (persist_type == 'enumHardPersist'):
		return 'PersistType::HardPersist'
	if (persist_type == 'enumNotPersist'):
		return 'PersistType::NotPersist'
	if (persist_type == 'enumPersist'):
		return 'PersistType::Persist'
	if (persist_type == 'enumRemove'):
		return 'PersistType::Remove'
	return 'PersistType::TypeUnknown'

# Print the elements of the property as a C++ class initializer string.
# Sometimes the indices string is empty, and sometimes it has two elements. This is OK
# Because the objects being instantiated have two different constructors to handle the
# different cases.
def process_one_property(prefix, name, component_property, indices, suffix):
	w1 = ' {\"' + name + '\"}, '
	w2 = rename_editable_type(component_property['editability_type']) + ', '
	w3 = rename_persist_type(component_property['persist_type']) + ', '
	w4 = process_one_field('data_type', component_property) + ', '
	w5 = process_one_field('base_unit', component_property) + ', '
	w6 = process_one_field('es_min_value', component_property) + ', '
	w7 = process_one_field('es_max_value', component_property) + ', '
	w8 = process_one_field('setting', component_property) + ', '
	w9 = process_one_field('scale_factor', component_property) + ', '
	w10 = process_one_field('points_past_decimal', component_property) + ', '
	w11 = process_one_field('display_directive', component_property) + ', '
	w12 = process_one_field('display_min_value', component_property) + ', '
	w13 = process_one_field('display_max_value', component_property)
	print(prefix + w1 + w2 + w3 + w4 + w5 + w6 + w7 + w8 + w9 + w10 + w11 + w12 + w13 + indices + suffix, sep='')

# Process a list of elements, where each element will have the same number of component properties inside.
# Important! The C++ code in Components.cpp does not implement lists like JSON or Python. Lists do not cause a deeper hierarchy.
# All component properties from a list go into the initializing structure at the same level. It is a shallow structure.
# All such properties are given two extra member attributes that the other properties lack--their list name and their index
# location. This provices the C++ code with enough information to recreate the lists for the component properties in valid JSON format.
def process_one_list(name, property_list):
	# Caution. When this was written, only about 3 components had lists in their properties. In those cases,
	# the lists were somewere in the middle. There were always regular properties at the beginning and ending
	# of the files. If that ever changes, this function will start messing up the placement of curly brackets,
	# because the prefix and suffix parameters never change.
	index = 0;
	print('')
	for something in property_list:
		index_string = ', {\"' + name + '\"}, {' + str(index) + '} '
		for key in something:
			if isinstance(something[key], dict):
				process_one_property('\t  {', key, something[key], index_string, ' },')
		index +=1
		print('')

def process_one_component(name, component):
	# Identify the last key, so we know when to add an extra closing curly bracket
	lastKey = ''
	for key in component:
		 lastKey = key
	# Keep a count of keys, so we know when to add an extra opening curly bracket.
	keyCount = 0
	for key in component:
		if isinstance(component[key], dict):
			if keyCount == 0:
				process_one_property('\t{ {', key, component[key], '', ' },')
			elif key == lastKey:
				process_one_property('\t  {', key, component[key], '', ' } },')
			else:
				process_one_property('\t  {', key, component[key], '', ' },')
			keyCount += 1
		if isinstance(component[key], list):
			process_one_list(key, component[key])

def sort_and_print_all(fname):
	with open(fname) as file:
		# Parse the file into a python object.
		jdata1 = json.load(file)
		# Recursively parse the the contents.
		for key in jdata1:
			process_one_component(key, jdata1[key])

if __name__ == "__main__":
	fname = sys.argv[1]
	fpath = Path(fname)
	if fpath.is_file():
		sort_and_print_all(fname)
	else:
		print('File does not exist');

