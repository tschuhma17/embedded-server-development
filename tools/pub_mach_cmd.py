import paho.mqtt.client as mqtt #import the client1
from settings import BROKER_ADDRESS
client = mqtt.Client("mypub1") #create new instance
client.connect(BROKER_ADDRESS) #connect to broker
client.publish("tbd/machine/command",'{"message_type": "config", "transport_version": "1.0", "command group id": "234", "command_id": 1234, "session_id": "sup?", "machine_id": "CX-2345", "payload": { "word": "batten down the hatches" } }')#publish
