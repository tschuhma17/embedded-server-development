// Minimum.cpp
/*
	Note: Before getting started, Basler recommends reading the "Programmer's Guide" topic
	in the pylon C++ API documentation delivered with pylon.
	If you are upgrading to a higher major version of pylon, Basler also
	strongly recommends reading the "Migrating from Previous Versions" topic in the pylon C++ API documentation.

	----- CARRIED OVER FROM PARAMETRIZECAMERA_GENERICPARAMETERACCESS -----
    For camera configuration and for accessing other parameters, the pylon API
    uses the technologies defined by the GenICam standard hosted by the
    European Machine Vision Association (EMVA). The GenICam specification
    (http://www.GenICam.org) defines a format for camera description files.
    These files describe the configuration interface of GenICam compliant cameras.
    The description files are written in XML (eXtensible Markup Language) and
    describe camera registers, their interdependencies, and all other
    information needed to access high-level features such as Gain,
    Exposure Time, or Image Format by means of low-level register read and
    write operations.

    The elements of a camera description file are represented as software
    objects called Nodes. For example, a node can represent a single camera
    register, a camera parameter such as Gain, a set of available parameter
    values, etc. Each node implements the GenApi::INode interface.

    The nodes are linked together by different relationships as explained in the
    GenICam standard document available at www.GenICam.org. The complete set of
    nodes is stored in a data structure called Node Map.
    At runtime, a Node Map is instantiated from an XML description.

    This sample shows the 'generic' approach for configuring a camera
    using the GenApi nodemaps represented by the GenApi::INodeMap interface.

    The names and types of the parameter nodes can be found in the Basler pylon Programmer's Guide
    and API Reference, in the camera User's Manual, in the camera's document about
    Register Structure and Access Methodes (if applicable), and by using the pylon Viewer tool.

    See also the ParametrizeCamera_NativeParameterAccess sample for the 'native'
    approach for configuring a camera.
	
	----- CARRIED OVER FROM GRAB.CPP -----
	This sample illustrates how to grab and process images using the CInstantCamera class.
	The images are grabbed and processed asynchronously, i.e.,
	while the application is processing a buffer, the acquisition of the next buffer is done
	in parallel.

	The CInstantCamera class uses a pool of buffers to retrieve image data
	from the camera device. Once a buffer is filled and ready,
	the buffer can be retrieved from the camera object for processing. The buffer
	and additional image data are collected in a grab result. The grab result is
	held by a smart pointer after retrieval. The buffer is automatically reused
	when explicitly released or when the smart pointer object is destroyed.
*/

// Include files to use the pylon API.
#include <pylon/PylonIncludes.h>
#include "../include/SampleImageCreator.h"

// Namespace for using pylon objects.
using namespace Pylon;

// Namespace for using GenApi objects.
using namespace GenApi;

// Namespace for using cout.
using namespace std;

// Number of images to be grabbed.
static const uint32_t c_countOfImagesToGrab = 10;

void PrintSettings( CInstantCamera & camera )
{
	INodeMap& nodemap = camera.GetNodeMap();

	// Open the camera for accessing the parameters.
	camera.Open();

	// Get camera device information.
	cout << "Camera Device Information" << endl
		<< "=========================" << endl;
	cout << "Vendor           : "
		<< CStringParameter( nodemap, "DeviceVendorName" ).GetValue() << endl;
	cout << "Model            : "
		<< CStringParameter( nodemap, "DeviceModelName" ).GetValue() << endl;
	cout << "Firmware version : "
		<< CStringParameter( nodemap, "DeviceFirmwareVersion" ).GetValue() << endl << endl;

   // Camera settings.
	cout << "Camera Device Settings" << endl
		<< "======================" << endl;

	// Print the model name of the camera.
	cout << "Using device " << camera.GetDeviceInfo().GetModelName() << endl;
	
	// Close the camera.
	camera.Close();
}

void PrintParamsOfInterest( CInstantCamera & camera )
{
	INodeMap& nodemap = camera.GetNodeMap();

	// Open the camera for accessing the parameters.
	camera.Open();

	// Get the integer nodes describing the AOI.
	cout << "======================" << endl;
	CIntegerParameter offsetX( nodemap, "OffsetX" );
	CIntegerParameter offsetY( nodemap, "OffsetY" );
	CIntegerParameter width( nodemap, "Width" );
	CIntegerParameter height( nodemap, "Height" );
	CFloatParameter exposureTime( nodemap, "ExposureTime" );
	CIntegerParameter binningHorizontal( nodemap, "BinningHorizontal" );
	CIntegerParameter binningVertical( nodemap, "BinningVertical" );
    CFloatParameter gain( nodemap, "Gain" );
	CEnumParameter pixelFormat( nodemap, "PixelFormat" );

	cout << "OffsetX          : " << offsetX.GetValue() << endl;
	cout << "OffsetY          : " << offsetY.GetValue() << endl;
	cout << "Width            : " << width.GetValue() << endl;
	cout << "Height           : " << height.GetValue() << endl;
	cout << "ExposureTime     : " << exposureTime.GetValue() << endl;
	cout << "BinningHorizontal: " << binningHorizontal.GetValue() << endl;
	cout << "BinningVertical  : " << binningVertical.GetValue() << endl;
	cout << "Gain             : " << gain.GetValue() << endl;
	cout << "PixelFormatEnum  : " << pixelFormat.GetValue() << endl;
	
	// Close the camera.
	camera.Close();
}

void ConfigureParamsOfInterest( CInstantCamera & camera )
{
	INodeMap& nodemap = camera.GetNodeMap();

	// Open the camera for accessing the parameters.
	camera.Open();

	// Get the integer nodes describing the AOI.
	cout << "======================" << endl;
	CIntegerParameter offsetX( nodemap, "OffsetX" );
	CIntegerParameter offsetY( nodemap, "OffsetY" );
	CIntegerParameter width( nodemap, "Width" );
	CIntegerParameter height( nodemap, "Height" );
	CFloatParameter exposureTime( nodemap, "ExposureTime" );
	CIntegerParameter binningHorizontal( nodemap, "BinningHorizontal" );
	CIntegerParameter binningVertical( nodemap, "BinningVertical" );
	CFloatParameter gain( nodemap, "Gain" );
	
	// Some properties have restrictions.
	// We use API functions that automatically perform value corrections.
	// Alternatively, you can use GetInc() / GetMin() / GetMax() to make sure you set a valid value.
	cout << "width-inc        : " << width.GetInc() << endl;
	cout << "width-min        : " << width.GetMin() << endl;
	cout << "width-max        : " << width.GetMax() << endl;
	width.SetValue( 2100, IntegerValueCorrection_Nearest );
	cout << "height-inc       : " << height.GetInc() << endl;
	cout << "height-min       : " << height.GetMin() << endl;
	cout << "height-max       : " << height.GetMax() << endl;
	height.SetValue( 2100, IntegerValueCorrection_Nearest );
	cout << "offsetX-inc      : " << offsetX.GetInc() << endl;
	cout << "offsetX-min      : " << offsetX.GetMin() << endl;
	cout << "offsetX-max      : " << offsetX.GetMax() << endl;
	offsetX.SetValue( (3840-2100)/2, IntegerValueCorrection_Nearest );
	cout << "offsetY-inc      : " << offsetY.GetInc() << endl;
	cout << "offsetY-min      : " << offsetY.GetMin() << endl;
	cout << "offsetY-max      : " << offsetY.GetMax() << endl;
	offsetY.SetValue( (2160-2100)/2, IntegerValueCorrection_Nearest );

	cout << "OffsetX          : " << offsetX.GetValue() << endl;
	cout << "OffsetY          : " << offsetY.GetValue() << endl;
	cout << "Width            : " << width.GetValue() << endl;
	cout << "Height           : " << height.GetValue() << endl;
	cout << "ExposureTime     : " << exposureTime.GetValue() << endl;
	cout << "BinningHorizontal: " << binningHorizontal.GetValue() << endl;
	cout << "BinningVertical  : " << binningVertical.GetValue() << endl;
	cout << "Gain             : " << gain.GetValue() << endl;
	
	// Close the camera.
	camera.Close();
}

int main( int /*argc*/, char* /*argv*/[] )
{
	// The exit code of the sample application.
	int exitCode = 0;

	// Before using any pylon methods, the pylon runtime must be initialized.
	PylonInitialize();

	try
	{
		// Create an instant camera object with the camera device found first.
		CInstantCamera camera( CTlFactory::GetInstance().CreateFirstDevice() );
		
		PrintSettings( camera );
		
		PrintParamsOfInterest( camera );
		
		ConfigureParamsOfInterest( camera );

		// The parameter MaxNumBuffer can be used to control the count of buffers
		// allocated for grabbing. The default value of this parameter is 10.
		camera.MaxNumBuffer = 5;

		// Start the grabbing of c_countOfImagesToGrab images.
		// The camera device is parameterized with a default configuration which
		// sets up free-running continuous acquisition.
//		camera.StartGrabbing( c_countOfImagesToGrab );

		// This smart pointer will receive the grab result data.
		CGrabResultPtr ptrGrabResult;

		// Saving grabbed images.
		{
		    // Try to get a grab result.
		    cout << endl << "Waiting for an image to be grabbed." << endl;
		    try
		    {
		        // This smart pointer will receive the grab result data.
		        CGrabResultPtr ptrGrabResult;

		        if (camera.GrabOne( 5000, ptrGrabResult ))
		        {
		            // The pylon grab result smart pointer classes provide a cast operator to the IImage
		            // interface. This makes it possible to pass a grab result directly to the
		            // function that saves an image to disk.
		            CImagePersistence::Save( ImageFileFormat_Png, "GrabbedImage.png", ptrGrabResult );
		        }
		    }
		    catch (const GenericException& e)
		    {

		        cerr << "Could not grab an image: " << endl
		            << e.GetDescription() << endl;
		    }
		}
/*
		// Camera.StopGrabbing() is called automatically by the RetrieveResult() method
		// when c_countOfImagesToGrab images have been retrieved.
		while (camera.IsGrabbing())
		{
			// Wait for an image and then retrieve it. A timeout of 5000 ms is used.
			camera.RetrieveResult( 5000, ptrGrabResult, TimeoutHandling_ThrowException );

			// Image grabbed successfully?
			if (ptrGrabResult->GrabSucceeded())
			{
				// Access the image data.
				cout << "SizeX: " << ptrGrabResult->GetWidth() << endl;
				cout << "SizeY: " << ptrGrabResult->GetHeight() << endl;
				const uint8_t* pImageBuffer = (uint8_t*) ptrGrabResult->GetBuffer();
				cout << "Gray value of first pixel: " << (uint32_t) pImageBuffer[0] << endl << endl;
			}
			else
			{
				cout << "Error: " << std::hex << ptrGrabResult->GetErrorCode() << std::dec << " " << ptrGrabResult->GetErrorDescription() << endl;
			}
		}
*/
	}
	catch (const GenericException& e)
	{
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
		exitCode = 1;
	}

	// Comment the following two lines to disable waiting on exit.
//	cerr << endl << "Press enter to exit." << endl;
//	while (cin.get() != '\n');

	// Releases all pylon resources.
	PylonTerminate();

	return exitCode;
}
