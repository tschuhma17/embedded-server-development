DESCRIPTION = "Simple hello-world application"
LICENSE = "EPL-1.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/EPL-1.0;md5=57f8d5e2b3e98ac6e088986c12bf94e6"

DEPENDS += "paho-mqtt-c"

SRC_URI = "file://MQTTClient_subscribe.c"

S = "${WORKDIR}"
LIBS = " -lpaho-mqtt3a -lpaho-mqtt3as -lpaho-mqtt3c -lpaho-mqtt3cs"

do_compile() {
    echo "do_compile is trying"
	${CC} MQTTClient_subscribe.c ${CFLAGS} ${LDFLAGS} ${LIBS} -o MQTTClient_subscribe
	ls -al MQTTClient_subscribe
}

do_install() {
    echo "do_install is trying"
	ls -al MQTTClient_subscribe
	install -d ${D}${bindir}
	install -m 0755 MQTTClient_subscribe ${D}${bindir}
	ls -al ${D}${bindir}
}

