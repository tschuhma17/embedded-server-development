# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# Unable to find any files that looked like license statements. Check the accompanying
# documentation and source headers and set LICENSE and LIC_FILES_CHKSUM accordingly.
#
# NOTE: LICENSE is being set to "CLOSED" to allow you to at least start building - if
# this is not accurate with respect to the licensing of the software being built (it
# will not be in most cases) you must specify the correct value before using this
# recipe for anything other than initial testing/development!
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

# No information for SRC_URI yet (only an external source tree was specified)
# Notice that pylon-7.3.0.27189_linux-aarch64.tar.gz has its name changed to prevent
#	Yocto from extracting it. Our goal is to install it to the target in its current
#	form. We'll have to rename it later.
SRC_URI = " \
			file://pylon_wrapper.cpp \
			file://pylon_wrapper.hpp \
			file://pylon-7.3.0.27189_linux-aarch64.tar_gz_ \
           "
SRC = "pylon_wrapper.cpp"

do_configure () {
	# Specify any needed configure commands here
	:
}

S = "${WORKDIR}"

CXXFLAGS += " -g -fPIC"
LDFLAGS += ""
INCFLAGS += ""
do_compile () {
	${CXX} -c ${SRC} ${CXXFLAGS} ${INCFLAGS} ${LDFLAGS} -o  pylon_wrapper.o
	${CXX} -shared pylon_wrapper.o ${LDFLAGS} -o libPyWrap.so
}

do_install () {
	echo "installing"
	install -m 0755 -d ${D}${libdir}
	install ${WORKDIR}/libPyWrap.so ${D}${libdir}
	# oe_soinstall ${D}${bindir}/libPyWrap.so.${PV} ${D}${libdir}
	install -d ${D}${includedir}
	install -m 0755 ${WORKDIR}/*.hpp ${D}${includedir}
	install -d ${D}${datadir}/basler
	install -m 0644 ${WORKDIR}/pylon-7.3.0.27189_linux-aarch64.tar_gz_ ${D}${datadir}/basler/
}

FILES_${PN} += " \
				${libdir}/*.so \
                ${includedir}/*.hpp \
                ${datadir}/basler \
"
FILES_${PN}-dev = " \
				${includedir} \
				${datadir} \
"
