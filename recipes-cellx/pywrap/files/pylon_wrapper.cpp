/// pylon_wrapper.cpp
/// This file is meant to be incorporated into the Yocto build of the embedded server.
/// It creates a shared library that mocks a grab of a Basler camera image.
/// Another version will be built on the embedded server, and it will link in Basler's pylon_wrapper
/// library and provide real camera-captured images.
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include "pylon_wrapper.hpp"

// Namespace for using cout.
using namespace std;

#define CAM_WIDTH 3860
#define CAM_HEIGHT 2178
#define IMG_WIDTH 2100
#define IMG_HEIGHT 2100
PylonWrapper::PylonWrapper() :
	m_offsetX( (CAM_WIDTH-IMG_WIDTH) / 2 ),
	m_offsetY( (CAM_HEIGHT-IMG_HEIGHT) / 2 ),
	m_width( IMG_WIDTH ),
	m_height( IMG_HEIGHT ),
	m_bytesPerPixel(1),
	m_gain(12.0),
	m_exposureTime(150000.0), /// us
	m_binningHorizontal(1),
	m_binningVertical(1),
	m_isInitalized(false),
	m_hasCamera(false),
	m_cameraIsOpen(false),
	m_hasError(false)
{
	Initialize();
	/// Would open a camera here.
}

PylonWrapper::~PylonWrapper()
{
	/// Would close a camera here.
	Terminate();
}

int PylonWrapper::GetVersion( void )
{
	return PYWRAP_VERSION;
}

bool PylonWrapper::GetCameraVersion( std::string & GenCPVersion )
{
	GenCPVersion = "0.0";
	return true;
}

bool PylonWrapper::GetNumberOfCameras( int & numberOfCameras )
{
	numberOfCameras = 1;
	return true;
}

/// Creates a CBaslerUniversalInstantCamera, opens it. Does not call CenterX(), CenterY()
bool PylonWrapper::GetCamera( void )
{
	return true;
}

bool PylonWrapper::IsForReal( void )
{
	return false;
}

int PylonWrapper::GetOffsetX( void )
{
	return m_offsetX;
}

int PylonWrapper::GetOffsetY( void )
{
	return m_offsetY;
}

int PylonWrapper::GetWidth( void )
{
	return m_width;
}

bool PylonWrapper::SetWidth( int width )
{
	return true;
}

int PylonWrapper::GetHeight( void )
{
	return m_height;
}

bool PylonWrapper::SetHeight( int height )
{
	return true;
}

int PylonWrapper::GetBytesPerPixel( void )
{
	return m_bytesPerPixel;
}

int PylonWrapper::GetBinningHorizontal( void )
{
	return m_binningHorizontal;
}

int PylonWrapper::GetBinningVertical( void )
{
	return m_binningVertical;
}

float PylonWrapper::GetGain( void )
{
	return m_gain;
}

bool PylonWrapper::SetGain( float gain )
{
	return true;
}

float PylonWrapper::GetExposureTime( void )
{
	return m_exposureTime;
}

bool PylonWrapper::SetExpsureTime( float exposureTime )
{
	return true;
}

bool PylonWrapper::CenterX( void )
{
	return true;
}
bool PylonWrapper::CenterY( void )
{
	return true;
}
/// The client is in charge of allocating and deallocating space for the image and message.
/// Returns the number of image bytes in imageBufferOut.
/// imageBufferOut contains the image data after the call.
/// messageOut contains a message stream after the call.
long PylonWrapper::grabImage( void * imageBufferOut, long imageBufferSizeIn )
{
	return 0L;
}

bool PylonWrapper::Initialize()
{
	m_isInitalized = true;
	return m_isInitalized; /// Is Initalized
}

bool PylonWrapper::Terminate()
{
	m_isInitalized = false;
	return true; /// Succeeded
}

bool PylonWrapper::ReadParamsFromCamera()
{
	return (m_isInitalized && m_cameraIsOpen);
}

bool PylonWrapper::HasError()
{
	return m_hasError;
	m_hasError = false;
}

const char * PylonWrapper::GetErrorMsg()
{
	return m_errorMsg;
}

void PylonWrapper::RecordException( const char * description )
{
	stringstream msg;
	msg << "An EXCEPTION occurred. " << description;
	int msgLength = msg.str().length();
	if (msgLength >= ERROR_MESSAGE_BUFFER_SIZE)
	{
		msgLength = ERROR_MESSAGE_BUFFER_SIZE -1; /// Leave space for terminating null.
	}
	m_hasError = true;
	strncpy( m_errorMsg, msg.str().c_str(), msgLength );
}

void PylonWrapper::RecordError( const char * description )
{
	stringstream msg;
	msg << "An ERROR occurred. " << description;
	int msgLength = msg.str().length();
	if (msgLength >= ERROR_MESSAGE_BUFFER_SIZE)
	{
		msgLength = ERROR_MESSAGE_BUFFER_SIZE -1; /// Leave space for terminating null.
	}
	m_hasError = true;
	strncpy( m_errorMsg, msg.str().c_str(), msgLength );
}

