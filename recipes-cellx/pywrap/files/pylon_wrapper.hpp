///----------------------------------------------------------------------
/// pylon_wrapper.hpp
/// This file is meant to be incorporated into the Yocto build of the embedded server.
/// It creates a shared library that mocks a grab of a Basler camera image.
/// Another version will be built on the embedded server, and it will link in Basler's pylon_wrapper
/// library and provide real camera-captured images.
///----------------------------------------------------------------------
#ifndef _PYLON_WRAPPER_H
#define _PYLON_WRAPPER_H

#define ERROR_MESSAGE_BUFFER_SIZE	200
#define PYWRAP_VERSION				2
/// TODO: version 2 needs the following changes:
/// 1. Add a function to get camera version
/// 2. Split the PylonWrapper constructor to move everything after Initialize() to a new GetCameras() method.
/// Because CommandDoler calls these in succession:
/// Camera_Basler::InitDriver()
/// Camera_Basler::GetVersion()
/// Camera_Basler::GetCameras().
class PylonWrapper
{
public:
	/// Constructor
	PylonWrapper(); /// Change to just do construction (PYWRAP_VERSION 2)

	/// Destructor
	~PylonWrapper(); /// Closes and deletes the internal camera object.

	/// GetVersion() is for tracking changes in the class definition of PylonWrapper.
	int GetVersion( void );
	bool GetCameraVersion( std::string & GenCPVersion ); /// Add for (PYWRAP_VERSION 2)
	/// GetNumberOfCameras() returns 1. If support for more than 1 camera is needed some day, change this and GetCamera() accordingly.
	bool GetNumberOfCameras( int & numberOfCameras ); /// Add for (PYWRAP_VERSION 2)
	/// GetCamera() Creates a CBaslerUniversalInstantCamera, opens it. Calls CenterX(), CenterY()
	bool GetCamera( void ); /// Add for (PYWRAP_VERSION 2)
	/// IsForReal() returns true if compiled from the pylon_wrapper.cpp that links to pylon.so.
	/// IsForReal() returns false if compiled to be an empty shell of no-op methods.
	bool IsForReal( void );
	int GetOffsetX( void );
	int GetOffsetY( void );
	int GetWidth( void );
	bool SetWidth( int width );
	int GetHeight( void );
	bool SetHeight( int height );
	int GetBytesPerPixel( void );
	int GetBinningHorizontal( void );
	int GetBinningVertical( void );
	float GetGain( void );
	bool SetGain( float gain );
	float GetExposureTime( void );
	bool SetExpsureTime( float exposureTime );
	bool CenterX( void );
	bool CenterY( void );

	long grabImage( void * imageBufferOut, long imageBufferSizeIn );
	bool Initialize(); /// Calls PylonInitialize
	bool Terminate();
	bool ReadParamsFromCamera();

	/// Returns true if (m_hasError), meaning a method recently recorded an error.
	/// Calling HasError() clears m_hasError but does not clear the message in m_errorMsg.
	bool HasError();
	const char * GetErrorMsg();

private:
	int m_offsetX;
	int m_offsetY;
	int m_width;
	int m_height;
	int m_bytesPerPixel;
	float m_gain;
	float m_exposureTime;
	int m_binningHorizontal;
	int m_binningVertical;

	bool m_isInitalized;
	bool m_hasCamera;
	bool m_cameraIsOpen;

	bool m_hasError;
	char m_errorMsg[ERROR_MESSAGE_BUFFER_SIZE];
	void RecordException( const char * description );
	void RecordError( const char * description );
};

#endif 

