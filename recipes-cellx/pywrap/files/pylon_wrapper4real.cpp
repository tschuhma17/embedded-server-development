/// pylon_wrapper.cpp
/// This file is meant to be incorporated into the Yocto build of the embedded server.
/// It creates a shared library that mocks a grab of a Basler camera image.
/// Another version will be built on the embedded server, and it will link in Basler's pylon_wrapper
/// library and provide real camera-captured images.
#include <stdio.h>
#include <iostream>
#include <math.h>
#include "pylon_wrapper.hpp"

// Include files to use the pylon API.
#include <pylon/PylonIncludes.h>
#include <pylon/PylonImage.h>
#include <pylon/Pixel.h>
#include <pylon/ImageFormatConverter.h>
#include <pylon/BaslerUniversalInstantCamera.h>
#include <pylon/PylonBase.h>

// Namespace for using pylon objects.
using namespace Pylon;

// Namespace for using pylon universal instant camera parameters.
using namespace Basler_UniversalCameraParams;

// Namespace for using cout.
using namespace std;

CBaslerUniversalInstantCamera * g_Camera = nullptr;

#define CAM_WIDTH 3860
#define CAM_HEIGHT 2178
#define IMG_WIDTH 2100
#define IMG_HEIGHT 2100
PylonWrapper::PylonWrapper() :
	m_offsetX( (CAM_WIDTH-IMG_WIDTH) / 2 ),
	m_offsetY( (CAM_HEIGHT-IMG_HEIGHT) / 2 ),
	m_width( IMG_WIDTH ),
	m_height( IMG_HEIGHT ),
	m_bytesPerPixel(1),
	m_gain(12.0),
	m_exposureTime(150000.0), /// us
	m_binningHorizontal(1),
	m_binningVertical(1),
	m_isInitalized(false),
	m_hasCamera(false),
	m_cameraIsOpen(false),
	m_hasError(false)
{
	Initialize();
}

PylonWrapper::~PylonWrapper()
{
	/// Would close a camera here.
	if (m_cameraIsOpen)
	{
		try
		{
			g_Camera->Close();
			m_cameraIsOpen = false;
		}
		catch (const GenericException& e)
		{
			RecordException( e.GetDescription() );
			cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
			m_cameraIsOpen = false;
		}
	}
	if (g_Camera)
	{
		delete g_Camera;
	}
	Terminate();
}

int PylonWrapper::GetVersion( void )
{
	return PYWRAP_VERSION;
}

/// Return the DeviceGenCPVersionMajor
bool PylonWrapper::GetCameraVersion( std::string & GenCPVersion )
{
	GenCPVersion = "0.0";
	return true;
}

bool PylonWrapper::GetNumberOfCameras( int & numberOfCameras )
{
	numberOfCameras = 1;
	return true;
}

/// Creates a CBaslerUniversalInstantCamera, opens it. Calls CenterCameraFrame()
bool PylonWrapper::GetCamera( void )
{
	if (m_isInitalized)
	{
		try
		{
			g_Camera = new CBaslerUniversalInstantCamera( Pylon::CTlFactory::GetInstance().CreateFirstDevice() );
			if (g_Camera)
			{
				g_Camera->Open();
				m_cameraIsOpen = true;

				cout << "CenterX(true) ";
				if (CenterX()) { cout << "succeeded" << endl; } else { cout << "failed" << endl; cout << GetErrorMsg() << endl; }
				cout << "CenterY(true) ";
				if (CenterY()) { cout << "succeeded" << endl; } else { cout << "failed" << endl; cout << GetErrorMsg() << endl; }

				ReadParamsFromCamera();
				cout << "Using device " << g_Camera->GetDeviceInfo().GetModelName() << endl;
				/// Leave the camera open.
			}
		}
		catch (const GenericException& e)
		{
			RecordException( e.GetDescription() );
			cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
			m_cameraIsOpen = false;
		}
	}
	return m_cameraIsOpen;
}

bool PylonWrapper::IsForReal( void )
{
	return true; /// Meaning this is the wrapper4real implementation.
}

int PylonWrapper::GetOffsetX( void )
{
	/// If g_Camera==nullptr or !m_cameraIsOpen, let the exception-handling report an error.
	try
	{
		m_offsetX = g_Camera->OffsetX.GetValue();
	}
	catch (const GenericException& e)
	{
		RecordException( e.GetDescription() );
		cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
	}
	return m_offsetX;
}

int PylonWrapper::GetOffsetY( void )
{
	try
	{
		m_offsetY = g_Camera->OffsetY.GetValue();
	}
	catch (const GenericException& e)
	{
		RecordException( e.GetDescription() );
		cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
	}
	return m_offsetY;
}

int PylonWrapper::GetWidth( void )
{
	try
	{
		m_width = g_Camera->Width.GetValue();
	}
	catch (const GenericException& e)
	{
		RecordException( e.GetDescription() );
		cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
	}
	return m_width;
}

bool PylonWrapper::SetWidth( int width )
{
	try
	{
		g_Camera->Width.SetValue( width );
	}
	catch (const GenericException& e)
	{
		RecordException( e.GetDescription() );
		cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
	}
	return (GetWidth() == width);
}

int PylonWrapper::GetHeight( void )
{
	try
	{
		m_height = g_Camera->Height.GetValue();
	}
	catch (const GenericException& e)
	{
		RecordException( e.GetDescription() );
		cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
	}
	return m_height;
}

bool PylonWrapper::SetHeight( int height )
{
	try
	{
		g_Camera->Height.SetValue( height );
	}
	catch (const GenericException& e)
	{
		RecordException( e.GetDescription() );
		cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
	}
	return (GetHeight() == height);
}

int PylonWrapper::GetBytesPerPixel( void )
{
	try
	{
		PixelFormatEnums pixelFormat = g_Camera->PixelFormat.GetValue();
		if (pixelFormat == PixelFormat_Mono8) {
			m_bytesPerPixel = 1;
		} else if (pixelFormat == PixelFormat_Mono12) {
			m_bytesPerPixel = 2;
		} else if (pixelFormat == PixelFormat_Mono12p) {
			m_bytesPerPixel = 2; /// Technically 1.5 but in practice, we'll propabably handle it as 2.
		} else {
			m_bytesPerPixel = -1;
			stringstream msg;
			msg << "PixelFormat unknown (" << (int)pixelFormat << ")";
			RecordError( msg.str().c_str() );
		}
	}
	catch (const GenericException& e)
	{
		RecordException( e.GetDescription() );
		cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
	}
	return m_bytesPerPixel;
}

int PylonWrapper::GetBinningHorizontal( void )
{
	return m_binningHorizontal;
}

int PylonWrapper::GetBinningVertical( void )
{
	return m_binningVertical;
}

/// Query the camera for its current gain, because the existence of SetGain() means it might
/// not stay at the value we recorded during initialization.
float PylonWrapper::GetGain( void )
{
	try
	{
		if (g_Camera->Gain.IsReadable())
		{
			m_gain = g_Camera->Gain.GetValue();
		}
		else
		{
			m_gain = g_Camera->GainRaw.GetValue();
		}
	}
	catch (const GenericException& e)
	{
		RecordException( e.GetDescription() );
		cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
	}
	return m_gain;
}

/// Attempt to change the camera's gain.
/// Returns true if the camera's new gain value matches the given gain parameter.
bool PylonWrapper::SetGain( float gain )
{
	try
	{
		g_Camera->GainAuto.TrySetValue( GainAuto_Off );

		if (g_Camera->Gain.IsWritable())
		{
			g_Camera->Gain.SetValue( gain );
		}
		else
		{
			g_Camera->GainRaw.SetValue( gain );
		}
	}
	catch (const GenericException& e)
	{
		RecordException( e.GetDescription() );
		cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
	}
	return (fabs(GetGain()-gain) < 0.001);
}

/// Query the camera for its current exposure-time, because the existence of SetExpsureTime()
/// means it might not stay at the value we recorded during initialization.
float PylonWrapper::GetExposureTime( void )
{
	try
	{
		if (g_Camera->ExposureTime.IsReadable())
		{
			m_exposureTime = g_Camera->ExposureTime.GetValue();
		}
		else
		{
			m_exposureTime = g_Camera->ExposureTimeRaw.GetValue();
		}
	}
	catch (const GenericException& e)
	{
		RecordException( e.GetDescription() );
		cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
	}
	return m_exposureTime;
}

bool PylonWrapper::SetExpsureTime( float exposureTime )
{
	try
	{
		g_Camera->ExposureAuto.TrySetValue( ExposureAuto_Off );

		if (g_Camera->ExposureTime.IsWritable())
		{
			g_Camera->ExposureTime.SetValue( exposureTime );
		}
		else
		{
			g_Camera->ExposureTimeRaw.SetValue( exposureTime );
		}
	}
	catch (const GenericException& e)
	{
		RecordException( e.GetDescription() );
		cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
	}
	return (fabs(GetExposureTime()-exposureTime) < 0.001);
}

bool PylonWrapper::CenterX( void )
{
	bool success = false;
	try
	{
		success = g_Camera->BslCenterX.TryExecute();
	}
	catch (const GenericException& e)
	{
		RecordException( e.GetDescription() );
		cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
	}
	return success;
}

bool PylonWrapper::CenterY( void )
{
	bool success = false;
	try
	{
		success = g_Camera->BslCenterY.TryExecute();
	}
	catch (const GenericException& e)
	{
		RecordException( e.GetDescription() );
		cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
	}
	return success;
}


/// The client is in charge of allocating and deallocating space for the image and message.
/// Returns the number of image bytes in imageBufferOut.
/// imageBufferOut contains the image data after the call.
/// messageOut contains a message stream after the call.
// TODO: Change imageBufferSizeIn and messageSizeIn to size_t type.
long PylonWrapper::grabImage( void * imageBufferOut, long imageBufferSizeIn )
{
	CGrabResultPtr ptrGrabResult;
	size_t bytesToReturn = 0L;

	try
	{
		g_Camera->StartGrabbing( 1 );
		while (g_Camera->IsGrabbing())
		{
			g_Camera->RetrieveResult( 5000, ptrGrabResult, TimeoutHandling_ThrowException );
		}
	}
	catch (const GenericException& e)
	{
		RecordException( e.GetDescription() );
		cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
		return 0L;
	}

	if (ptrGrabResult && ptrGrabResult->GrabSucceeded())
	{
		cout << "Grab succeeded." << endl;
		CPylonImage image;
		image.AttachGrabResultBuffer( ptrGrabResult );
		EPixelType pixelType = ptrGrabResult->GetPixelType();
		uint32_t width = ptrGrabResult->GetWidth();
		uint32_t height = ptrGrabResult->GetHeight();
		if (pixelType != PixelType_Mono8)
		{
			stringstream msg;
			msg << "Wrong pixel type (" << (int) pixelType << ")";
			RecordError( msg.str().c_str() );
			return 0L;
		}
		bytesToReturn = ptrGrabResult->GetImageSize();
		if (bytesToReturn > imageBufferSizeIn)
		{
			stringstream msg;
			msg << "Image too big for buffer. width=" << width << "|height=" << height;
			RecordError( msg.str().c_str() );
			return 0L;
		}
		std::memcpy( imageBufferOut, ptrGrabResult->GetBuffer(), bytesToReturn );
	}
	return bytesToReturn;
}

/// Returns true if Pylon is initial
bool PylonWrapper::Initialize()
{
	if (m_isInitalized)
	{
		return m_isInitalized;	/// EARLY RETURN!	EARLY RETURN!
	}
	try
	{
		/// Before using any pylon methods, the pylon runtime must be initialized.
		PylonInitialize();
		m_isInitalized = true;
		cout << "Init succeeded" << endl;
	}
	catch (const GenericException& e)
	{
		RecordException( e.GetDescription() );
		cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
		m_isInitalized = false;
	}
	return m_isInitalized; /// Is Initialized
}

/// Returns true if Pylon is terminated.
bool PylonWrapper::Terminate()
{
	/// Releases all pylon resources.
	PylonTerminate();
	m_isInitalized = false;
	return true; /// Succeeded
}

bool PylonWrapper::ReadParamsFromCamera()
{
	bool succeeded = false;
	if (m_isInitalized && m_cameraIsOpen)
	{
		try
		{
			if (g_Camera->Gain.IsReadable()) {
				m_gain = g_Camera->Gain.GetValue();
			} else {
				m_gain = g_Camera->GainRaw.GetValue();
			}
			if (g_Camera->ExposureTime.IsReadable()) {
				m_exposureTime = g_Camera->ExposureTime.GetValue();
			} else {
				m_exposureTime = g_Camera->ExposureTimeRaw.GetValue();
			}

			m_offsetX = g_Camera->OffsetX.GetValue();
			m_offsetY = g_Camera->OffsetY.GetValue();
			m_width = g_Camera->Width.GetValue();
			m_height = g_Camera->Height.GetValue();

			PixelFormatEnums pixelFormat = g_Camera->PixelFormat.GetValue();
			if (pixelFormat == PixelFormat_Mono8) {
				m_bytesPerPixel = 1;
			} else if (pixelFormat == PixelFormat_Mono12) {
				m_bytesPerPixel = 2;
			} else if (pixelFormat == PixelFormat_Mono12p) {
				m_bytesPerPixel = 2; /// Technically 1.5 but in practice, we'll propabably handle it as 2.
			} else {
				m_bytesPerPixel = -1;
			}
			
			m_binningHorizontal = g_Camera->BinningHorizontal.GetValue();
			m_binningVertical = g_Camera->BinningVertical.GetValue();

		}
		catch (const GenericException& e)
		{
			RecordException( e.GetDescription() );
			cerr << "An exception occurred." << endl << e.GetDescription() << endl; // TODO: Delete this before release.
		}
	}
	return succeeded;
}

bool PylonWrapper::HasError()
{
	return m_hasError;
	m_hasError = false;
}

const char * PylonWrapper::GetErrorMsg()
{
	return m_errorMsg;
}

void PylonWrapper::RecordException( const char * description )
{
	stringstream msg;
	msg << "An EXCEPTION occurred. " << description;
	int msgLength = msg.str().length();
	if (msgLength >= ERROR_MESSAGE_BUFFER_SIZE)
	{
		msgLength = ERROR_MESSAGE_BUFFER_SIZE -1; /// Leave space for terminating null.
	}
	m_hasError = true;
	memset( m_errorMsg, 0, ERROR_MESSAGE_BUFFER_SIZE );
	memcpy( m_errorMsg, msg.str().c_str(), msgLength );
}

void PylonWrapper::RecordError( const char * description )
{
	stringstream msg;
	msg << "An ERROR occurred. " << description;
	int msgLength = msg.str().length();
	if (msgLength >= ERROR_MESSAGE_BUFFER_SIZE)
	{
		msgLength = ERROR_MESSAGE_BUFFER_SIZE -1; /// Leave space for terminating null.
	}
	m_hasError = true;
	memset( m_errorMsg, 0, ERROR_MESSAGE_BUFFER_SIZE );
	memcpy( m_errorMsg, msg.str().c_str(), msgLength );
}
