DESCRIPTION = "Prebuilt PVCAM Device Driver Interface library by Teledyne Photometrics."
# This file installs the libpvcamDDI library. I don't know whether it is actually needed.
# Best to keep it just in case.
SECTION = "libs"
# TODO: Revisit licensing
LICENSE_FLAGS = "commercial"
LICENSE = "CLOSED"

COMPATIBLE_HOST = "aarch64-tdx-linux"

SRC_URI += " \
           file://libpvcamDDI.so.3.0 \
           file://libpvcamDDI.version \
"

INSANE_SKIP:${PN} = "ldflags"
INHIBIT_PACKAGE_STRIP = "1"
INHIBIT_SYSROOT_STRIP = "1"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"

do_install() {
    echo "Base_lib directory is ${D}${base_libdir}"
    echo "lib directory is ${D}${libdir}"
    install -m 0755 -d ${D}${libdir}
    oe_soinstall ${WORKDIR}/libpvcamDDI.so.${PV} ${D}${libdir}
    install -m 0644 ${WORKDIR}/libpvcamDDI.version ${D}${libdir}
#	bbdebug 2 "this causes error."
}

FILES_${PN} += " \
                ${libdir}/* \
"
