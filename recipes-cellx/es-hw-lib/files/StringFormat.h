#ifndef _StringFormat_h_
#define _StringFormat_h_

#include <string>
#include <stdarg.h>

#pragma once

std::string format(const char *fmt, ...);

#endif
