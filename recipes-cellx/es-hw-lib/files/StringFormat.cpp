#include "StringFormat.h"
#include <stdio.h>
#include <string.h>

std::string format(const char *fmt, ...)
{
	va_list args;

	char buffer[2048];
	memset(buffer, 0, 2048);

	va_start(args, fmt);

#ifdef WIN32
	_vsnprintf(buffer, sizeof(buffer), fmt, args);
#else
	vsnprintf(buffer, sizeof(buffer), fmt, args);
#endif

	va_end(args);

	return std::string(buffer);
}
