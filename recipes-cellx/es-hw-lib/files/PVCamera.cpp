#include "Common.h"
#include "PVCamera.hpp"

#include <stdio.h> // for SayHi() and SingleImage_callbacks()
#include <string.h> // for memcpy()
#include <sstream>
#include <chrono>
#include <thread>

#include "StringFormat.h"
#include "PVCam_helper.h"

using namespace std;

PVCamera::PVCamera () : cameraHandle (0), cameraName (""), gainIdx(0), exposureTime_us(20), exposureSize (0), exposureFrame (NULL), binningX (1), binningY (1), frameWidth (0), frameHeight (0), 
clearCycles (2)
{
	m_pFrameInfo = NULL;
	exposureMultiplier = 1;
	frameTimoutTime = 1000; /// milliseconds
	errorCode = 0;
	errorMessage = "";
	version = "";
}

PVCamera::~PVCamera ()
{
}

/// To be called at start of runtime to initialize drivers supplied by camera manufacturers.
int PVCamera::InitDriver( std::string &errorMsg )
{
	int errorCode = 0;
	if (PV_OK == pl_pvcam_init())
	{
		errorMsg = "";
		errorCode = 0;
	}
	else
	{
		errorCode = pl_error_code();
		stringstream message;
		message << "error code=" << errorCode;
		errorMsg = message.str();
	}
	return errorCode;
}

int PVCamera::GetVersion( std::string &msg )
{
	/// Continue with the rest no matter how many other threads have done this.
	/// It just initializes CommandDolerThread class attrubutes. 
	unsigned short version;
	if (PV_OK == pl_pvcam_get_ver(&version))
	{
		stringstream msgtxt;
		msgtxt << "version: " << ((version >> 8) & 0xFF) << "." << ((version >> 4) & 0x0F) << "." << ((version >> 0) & 0x0F) << endl;
		msg = msgtxt.str();
	}
	return (int) version;
}

int PVCamera::GetNumberOfCameras( std::string &msg )
{
	short numCameras = 0;
	if (PV_OK == pl_cam_get_total(&numCameras))
	{
		stringstream msgtxt;
		msgtxt << "Number of cameras=" << numCameras;
		msg += msgtxt.str();
	}
	return numCameras;
}

bool PVCamera::GetCameraName( int camHandle, std::string & cameraName )
{
	char camName[256];
	if (PV_OK == pl_cam_get_name (camHandle, camName))
	{
		cameraName = camName;
		return true;
	}
	/// else
	return false;
}

/// To be called at end of runtime to tear down drivers supplied by camera manufacturers.
/// Returns 0 if successful.
/// Returns -1 if failed.
bool PVCamera::TerminateDriver( std::string &msg )
{
	bool succeeded = false;
	if (PV_OK == pl_pvcam_uninit())
	{
		succeeded = true;
	}
	return succeeded;
}

void PVCamera::Initialize( std::string &msg, char *camName, const short &camHandle )
{
	//LOG(format("PVCamera::initialize begin %s", camName).c_str());
	cameraHandle = camHandle;
	cameraName = camName;

	//open camera
	PVCAM_ERROR_CHECK (pl_cam_open (camName, &cameraHandle, OPEN_EXCLUSIVE), msg);
	msg += format ("Opened camera %s|", camName);

	CameraInfo (msg);

	CreateSpeedTable (msg);

	//initialize default settings.
	PVCAM_ERROR_CHECK (pl_set_param (cameraHandle, PARAM_READOUT_PORT, (void*)&speedTable[0].port.value), msg);
	msg += format ("Setting readout port to %s|", speedTable[0].port.name.c_str ());

	PVCAM_ERROR_CHECK (pl_set_param (cameraHandle, PARAM_SPDTAB_INDEX, (void*)&speedTable[0].speedIndex), msg);
	msg += format ("Setting readout speed index to %d|", speedTable[0].speedIndex);

	PVCAM_ERROR_CHECK (pl_set_param (cameraHandle, PARAM_GAIN_INDEX, (void*)&speedTable[0].gains[0]), msg);
	msg += format ("Setting gain index to %d|", speedTable[0].gains[0]);

	PVCAM_ERROR_CHECK (pl_get_param (cameraHandle, PARAM_SER_SIZE, ATTR_CURRENT, &frameWidth), msg);
	PVCAM_ERROR_CHECK (pl_get_param (cameraHandle, PARAM_PAR_SIZE, ATTR_CURRENT, &frameHeight), msg);
	msg += format ("Sensor size %dx%d|", frameWidth, frameHeight);

	PVCAM_ERROR_CHECK (pl_set_param (cameraHandle, PARAM_CLEAR_CYCLES, (void*)&clearCycles), msg);
	msg += format ("Setting clear cycles to %d|", clearCycles);
	
	{
		int shutter_status = 0;
		PVCAM_ERROR_CHECK(pl_get_param(cameraHandle, PARAM_SHTR_STATUS, ATTR_CURRENT, (void*)&shutter_status), msg);

		msg += format("Shutter Status: %d|", shutter_status);
	}

	int clear_mode = CLEAR_PRE_SEQUENCE;
	PVCAM_ERROR_CHECK(pl_set_param(cameraHandle, PARAM_CLEAR_MODE, (void*)&clear_mode), msg);

	int shutter_mode = OPEN_PRE_EXPOSURE;
	PVCAM_ERROR_CHECK(pl_set_param(cameraHandle, PARAM_SHTR_OPEN_MODE, (void*)&shutter_mode), msg);

	/*
	unsigned int feature_count;
	PVCAM_ERROR_CHECK(pl_get_param(cameraHandle, PARAM_PP_INDEX, ATTR_COUNT, &feature_count), msg);

	for (int i = 0; i < feature_count; i++) {
		PVCAM_ERROR_CHECK(pl_set_param(cameraHandle, PARAM_PP_INDEX, &i), msg);

		char feature_name[MAX_PP_NAME_LEN];
		PVCAM_ERROR_CHECK(pl_get_param(cameraHandle, PARAM_PP_FEAT_NAME, ATTR_CURRENT, feature_name), msg);

		int feature_id;
		PVCAM_ERROR_CHECK(pl_get_param(cameraHandle, PARAM_PP_FEAT_ID, ATTR_COUNT, &feature_id), msg);

		msg += format("Feature ID:%i Feature index:%i Feature Name:%s|", feature_id, i, feature_name);

		int param_index;
		PVCAM_ERROR_CHECK(pl_set_param(cameraHandle, PARAM_PP_PARAM_INDEX, &param_index), msg);

		for (int j = 0; j < param_index; j++) {
			PVCAM_ERROR_CHECK(pl_set_param(cameraHandle, PARAM_PP_PARAM_INDEX, &i), msg);

			char function_name[MAX_PP_NAME_LEN];
			PVCAM_ERROR_CHECK(pl_get_param(cameraHandle, PARAM_PP_PARAM_NAME, ATTR_CURRENT, function_name), msg);

			int function_id;
			PVCAM_ERROR_CHECK(pl_get_param(cameraHandle, PARAM_PP_PARAM_ID, ATTR_CURRENT, &function_id), msg);

			msg += format("  Function ID:%i Function Name:%s", function_id, function_name);
		}
	}
	*/

	// Default timing to be ms
	SetTiming(msg, TIMING::TIMING_MILLISECOND);

	PVCAM_ERROR_CHECK (pl_create_frame_info_struct ( (FRAME_INFO**) &m_pFrameInfo ), msg);

	//LOG(format("Initializing camera: end %s"));
}

void PVCamera::Release (std::string &msg)
{
	//LOG(format("PVCamera::release begin").c_str());

	ports.clear ();
	speedTable.clear ();

	if (exposureFrame)
		delete[] exposureFrame;

	if (m_pFrameInfo)
		PVCAM_ERROR_CHECK (pl_release_frame_info_struct ( (FRAME_INFO*) m_pFrameInfo ), msg);

	//close camera
	PVCAM_ERROR_CHECK (pl_cam_close (cameraHandle), msg);

	//LOG(format("PVCamera::release end").c_str());
}

size_t PVCamera::GetFrameSize( std::string &msg )
{
	/// Let us keep pixel boundaries alligned on byte boundaries. For example, pixels of 1, 2, or 8 bits
	/// will always have a full byte of space.
	short bitDepth;
	PVCAM_ERROR_CHECK (pl_get_param (cameraHandle, PARAM_BIT_DEPTH, ATTR_CURRENT, (void*)&bitDepth), msg);
	PVCAM_ERROR_CHECK (pl_get_param (cameraHandle, PARAM_SER_SIZE, ATTR_CURRENT, &frameWidth), msg);
	PVCAM_ERROR_CHECK (pl_get_param (cameraHandle, PARAM_PAR_SIZE, ATTR_CURRENT, &frameHeight), msg);
	unsigned long bytesPerPixel = 1 + ((bitDepth-1) / 8);
	return 	((frameWidth / binningX) * (frameHeight / binningY)) * bytesPerPixel;
}

/* DEPRECATED due to lack of use
unsigned short *Camera_Retiga::getFrame (std::string &msg)
{
	//LOG(format("Camera::getFrame begin").c_str());
	if (recreateBuffers)
	{
		//triggered when binned
		if (exposureFrame)
			delete[] exposureFrame;

		exposureFrame = new unsigned short[frameWidth*frameHeight];
	}

	//create region to capture
	rgn_type region;
	region.s1 = 0;
	region.s2 = frameWidth - 1;
	region.sbin = binningX;
	region.p1 = 0;
	region.p2 = frameHeight - 1;
	region.pbin = binningY;
	ulong64 scaledExposureTime = exposureTime_us;

	msg += format ("|region: %i,%i,%i  %i,%i,%i", region.s1, region.s2, region.sbin, region.p1, region.p2, region.pbin);

	//setup exposure sequence
	if (timing == TIMING::TIMING_MICROSECOND) {
		scaledExposureTime = exposureTime_us;
		msg += format("|exposure: %i microseconds multiplier=%i", scaledExposureTime*exposureMultiplier, exposureMultiplier);
		//LOG(format("exposure: %i microsecends multiplier:%i", scaledExposureTime*exposureMultiplier, exposureMultiplier).c_str());
	}
	else if (timing == TIMING::TIMING_MILLISECOND) {
		scaledExposureTime = exposureTime_us / 1000L;
		msg += format("|exposure: %i milliseconds multiplier=%i", scaledExposureTime*exposureMultiplier, exposureMultiplier);
		//LOG(format("exposure: %i milliseconds multiplier:%i", scaledExposureTime*exposureMultiplier, exposureMultiplier).c_str());
	}
	else if (timing == TIMING::TIMING_SECOND) {
		scaledExposureTime = exposureTime_us / 1000000L;
		msg += format("|exposure: %i seconds multiplier=%i", scaledExposureTime*exposureMultiplier, exposureMultiplier);
		//LOG(format("exposure: %i secends multiplier:%i", scaledExposureTime*exposureMultiplier, exposureMultiplier).c_str());
	}

	PVCAM_ERROR_CHECK (pl_exp_setup_seq (cameraHandle, 1, 1, &region, TIMED_MODE, scaledExposureTime*exposureMultiplier, &exposureSize), msg);
	msg += format ("|exposure in bytes:%i", exposureSize);
	
	unsigned short *ptr = new unsigned short[frameWidth*frameHeight];

	PVCAM_ERROR_CHECK (pl_exp_start_seq (cameraHandle, ptr), msg);

	unsigned int byte_cnt;
	short status;
	// Loop until we have captured our frame
	int loop_count = 0;
	while ((pl_exp_check_status(cameraHandle, &status, &byte_cnt) && status != READOUT_COMPLETE && status != READOUT_NOT_ACTIVE) &&
		   (loop_count < frameTimoutTime))
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
		loop_count++;
	}
	
	if (loop_count >= frameTimoutTime)
	{
		status = READOUT_FAILED;
		msg += format( "|Frame timed out" );
	}

	if (status == READOUT_FAILED)
		msg += format ("|Frame failed to read out");
	else {

		//copy to our buffer
		memcpy(exposureFrame, ptr, sizeof(unsigned short) * frameWidth * frameHeight);
	}
	printf("PVCamera::GetFrame(): calculates frame size as %ld.\n", (sizeof(unsigned short) * frameWidth * frameHeight));
	
	PVCAM_ERROR_CHECK(pl_exp_finish_seq(cameraHandle, ptr, 0), msg);

	delete[] ptr;

	//LOG(format("PVCamera::getFrame end").c_str());

	return exposureFrame;
}
*/

bool PVCamera::GetFrame (unsigned short *bufferOut, std::string &msgOut)
{
	bool succeeded = true;
	char tmp[ERROR_MSG_LEN];
	int error_code;
	msgOut += "|getFrame entered";

	//create region to capture
	rgn_type region;
	region.s1 = 0;
	region.s2 = frameWidth - 1;
	region.sbin = binningX;
	region.p1 = 0;
	region.p2 = frameHeight - 1;
	region.pbin = binningY;
	unsigned long scaledExposureTime = exposureTime_us;

	msgOut += format ("|region: %i,%i,%i  %i,%i,%i", region.s1, region.s2, region.sbin, region.p1, region.p2, region.pbin);

	//setup exposure sequence
	if (timing == TIMING::TIMING_MICROSECOND) {
		scaledExposureTime = exposureTime_us;
		msgOut += format("|exposure: %i microseconds multiplier=%i", scaledExposureTime * exposureMultiplier, exposureMultiplier);
	}
	else if (timing == TIMING::TIMING_MILLISECOND) {
		scaledExposureTime = exposureTime_us / 1000L;
		msgOut += format("|exposure: %i milliseconds multiplier=%i", scaledExposureTime * exposureMultiplier, exposureMultiplier);
	}
	else if (timing == TIMING::TIMING_SECOND) {
		scaledExposureTime = exposureTime_us / 1000000L;
		msgOut += format("|exposure: %i seconds multiplier=%i", scaledExposureTime * exposureMultiplier, exposureMultiplier);
	}

	if (!pl_exp_setup_seq (cameraHandle, 1, 1, &region, TIMED_MODE, scaledExposureTime * exposureMultiplier, &exposureSize))
	{
		error_code = pl_error_code();
		pl_error_message (error_code, tmp);
		msgOut += format("|Error[%i]:%s", error_code, tmp);
		succeeded = false;
	}
	
	msgOut += format ("|exposure in bytes:%i", exposureSize);
	
	if (!pl_exp_start_seq (cameraHandle, bufferOut))
	{
		error_code = pl_error_code();
		pl_error_message (error_code, tmp);
		msgOut += format("|Error[%i]:%s", error_code, tmp);
		succeeded = false;
	}

	unsigned int byte_cnt;
	short status;
	// Loop until we have captured our frame
	int loop_count = 0;
	while ((pl_exp_check_status(cameraHandle, &status, &byte_cnt) && status != READOUT_COMPLETE && status != READOUT_NOT_ACTIVE) &&
		   (loop_count < frameTimoutTime))
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
		loop_count++;
	}

	if (loop_count >= frameTimoutTime)
	{
		msgOut += format( "|Frame timed out" );
		succeeded = false;
	}

	if (status == READOUT_FAILED)
	{
		msgOut += format ("|Frame failed to read out");
		succeeded = false;
	}
	
	if (!pl_exp_finish_seq(cameraHandle, bufferOut, 0))
	{
		error_code = pl_error_code();
		pl_error_message (error_code, tmp);
		msgOut += format("|Error[%i]:%s", error_code, tmp);
		succeeded = false;
	}

	return succeeded;
}

/* DEPRECATED
void PVCamera::GetGainRange (std::string &msg, unsigned int &gainMin, unsigned int &gainMax)
{
	//LOG(format("Camera::getGainRange begin").c_str());
	std::vector<short> gainList = speedTable[0].gains;

	//returning min/max of the gains.
	gainMin = gainList[0];
	gainMax = gainList[gainList.size () - 1];

	//LOG(format("getGainRange: %i, %i", gainMin, gainMax).c_str());

	//LOG(format("PVCamera::getGainRange end").c_str());
}
*/

/* DEPRECATED
void PVCamera::GetExposureRange (std::string &msg, unsigned int &exposureMin, unsigned int &exposureMax)
{
	//LOG(format("Camera::getExposureRange begin").c_str());

	// microseconds
	exposureMin = 250;
	exposureMax = 3600000000;

	// Number of support resolutions
	unsigned short num_exposure_resolutions = 0;
	PVCAM_ERROR_CHECK(pl_get_param(cameraHandle, PARAM_EXP_RES_INDEX, ATTR_COUNT, &num_exposure_resolutions), msg);

	msg += format("|number exposure_resolutions:%i", num_exposure_resolutions);

	// Reading the range here:
	ulong64 min_exposure = 0;
	ulong64 max_exposure = 0;
	PVCAM_ERROR_CHECK(pl_get_param(cameraHandle, PARAM_EXPOSURE_TIME, ATTR_MIN, &min_exposure), msg);
	PVCAM_ERROR_CHECK(pl_get_param(cameraHandle, PARAM_EXPOSURE_TIME, ATTR_MAX, &max_exposure), msg);

	msg += format("|exposure_range: %i,%i", min_exposure, max_exposure);

	//LOG(format("getExposureRange: %i, %i", exposureMin, exposureMax).c_str());

	//LOG(format("PVCamera::getExposureRange end").c_str());
}
*/

int PVCamera::GetErrorState (std::string &msg)
{
	int errorCode = 0;
	unsigned short camVersion;
	if (!pl_get_param(cameraHandle, PARAM_DD_VERSION, ATTR_CURRENT, &camVersion))
	{
		char tmp[ERROR_MSG_LEN];
		errorCode = pl_error_code();
		pl_error_message (errorCode, tmp);
		stringstream stemp;
		stemp << "|Error[" << errorCode << "]:" << tmp;
		msg += stemp.str();
	}
	return errorCode;
}

void PVCamera::SetFrameWidth(const unsigned int &width)
{
	frameWidth = width;
}

void PVCamera::SetFrameHeight(const unsigned int &height)
{
	frameHeight = height;
}

void PVCamera::SetExposureTime(const unsigned long &expTime_us)
{
	exposureTime_us = expTime_us;
}

void PVCamera::SetGain (std::string &msg, const int &gainId)
{
	/// The Retiga R3 supports gains of 1 or 2. These exist in the camera object as values of gains[0] and gains[1], respectively.
	/// What we need to do here, is set the gainId (ie. index [0,1])
	if ((gainId < 1) || (gainId > 2))
	{
		stringstream stemp;
		stemp << "gainId " << gainId << " out of range";
		msg = stemp.str();
		return;
	}

	//LOG(format("Camera::setGain begin %i", gainId).c_str());
	gainIdx = gainId - 1;  /// Change to 0-based number.

	PVCAM_ERROR_CHECK (pl_set_param (cameraHandle, PARAM_GAIN_INDEX, (void*)&speedTable[0].gains[gainIdx]), msg);

	//LOG(format("setGain(%i)", gainIdx).c_str());

	//LOG(format("PVCamera::setGain end").c_str());
}

void PVCamera::SetBinFactor(const int &binX, const int &binY)
{
	binningX = binX;
	binningY = binY;
	recreateBuffers = true;
}


/* DEPRECATED
void Camera_Retiga::SetClearCycles (std::string &msg, const int &clear)
{
	clearCycles = clear;

	PVCAM_ERROR_CHECK (pl_set_param (cameraHandle, PARAM_CLEAR_CYCLES, (void*)&clearCycles), msg);

	//LOG(format("setClearCycles: %i", clear).c_str());
}
*/

void PVCamera::SetTiming (std::string &msg, const TIMING &t)
{
	//LOG(format("PVCamera::setTiming begin").c_str());
	timing = t;

	uns16 time = 0;
	if (timing == TIMING::TIMING_MICROSECOND) {
		time = EXP_RES_ONE_MICROSEC;
		exposureMultiplier = 1;

		msg += format("|Changed time to microseconds %i, %i, %i", t, time, exposureMultiplier);
	}
	else if (timing == TIMING_MILLISECOND) {
		time = EXP_RES_ONE_MILLISEC;
		exposureMultiplier = 1;

		msg += format("|Changed time to milliseconds %i, %i, %i", t, time, exposureMultiplier);
	}
	else if (timing == TIMING_SECOND)
	{
		time = EXP_RES_ONE_MILLISEC;
		exposureMultiplier = 1000;

		msg += format("|Changed time to seconds %i, %i, %i", t, time, exposureMultiplier);
	}

	// Modifying to index, which is 
	PVCAM_ERROR_CHECK (pl_set_param (cameraHandle, PARAM_EXP_RES_INDEX, (void*)&time), msg);

	//LOG(format("setTimingUnit(%i)", time).c_str());

	//LOG(format("PVCamera::setTiming end").c_str());
}

/* DEPRECATED
void PVCamera::SetShutter (std::string &msg, const unsigned short &type)
{
	//is parameter available?
	if (IsParameterAvailable (msg, PARAM_SHTR_OPEN_MODE, "PARAM_SHTR_OPEN_MODE"))
	{
		unsigned short shutterMode= type;
		PVCAM_ERROR_CHECK (pl_set_param (cameraHandle, PARAM_SHTR_OPEN_MODE, (void*)&shutterMode), msg);
	}
}
*/

void PVCamera::CameraInfo (std::string &msg)
{
	//grab some settings for camera
	if (IsParameterAvailable(msg, PARAM_DD_VERSION, "PARAM_DD_VERSION")) {
		unsigned short camVersion;
		PVCAM_ERROR_CHECK(pl_get_param(cameraHandle, PARAM_DD_VERSION, ATTR_CURRENT, &camVersion), msg);
		msg += format("|Camera device driver version: %d.%d.%d", ((camVersion >> 8) & 0xFF), ((camVersion >> 4) & 0x0F), ((camVersion >> 0) & 0x0F));
	}

	if (IsParameterAvailable(msg, PARAM_CHIP_NAME, "PARAM_CHIP_NAME")) {
		char chipName[CCD_NAME_LEN];
		PVCAM_ERROR_CHECK(pl_get_param(cameraHandle, PARAM_CHIP_NAME, ATTR_CURRENT, chipName), msg);
		msg += format("|Sensor chip name: %s", chipName);
	}

	if (IsParameterAvailable(msg, PARAM_CAM_FW_VERSION, "PARAM_CAM_FW_VERSION")) {
		unsigned short fwVersion;
		PVCAM_ERROR_CHECK(pl_get_param(cameraHandle, PARAM_CAM_FW_VERSION, ATTR_CURRENT, &fwVersion), msg);
		msg += format("|Camera firmware version: %d.%d", ((fwVersion >> 8) & 0xFF), ((fwVersion >> 0) & 0xFF));
	}

	if (IsParameterAvailable(msg, PARAM_HEAD_SER_NUM_ALPHA, "PARAM_HEAD_SER_NUM_ALPHA")) {
		char serial_number[MAX_ALPHA_SER_NUM_LEN];
		PVCAM_ERROR_CHECK(pl_get_param(cameraHandle, PARAM_HEAD_SER_NUM_ALPHA, ATTR_CURRENT, &serial_number), msg);
		msg += format("|Camera serial number: %s", serial_number);
	}
}

void PVCamera::CreateSpeedTable (std::string &msg)
{
	int count;
	PVCAM_ERROR_CHECK (pl_get_param (cameraHandle, PARAM_READOUT_PORT, ATTR_COUNT, (void*)&count), msg);

	for (int i = 0; i < count; i++)
	{
		unsigned int strLength;

		PVCAM_ERROR_CHECK (pl_enum_str_length (cameraHandle, PARAM_READOUT_PORT, i, &strLength), msg);

		char *pStr = new char[strLength];

		int value;
		PVCAM_ERROR_CHECK (pl_get_enum_param (cameraHandle, PARAM_READOUT_PORT, i, &value, pStr, strLength), msg);

		NVP nvp;
		nvp.value = value;
		nvp.name = pStr;
		ports.push_back (nvp);

		delete[] pStr;
	}

	//for all the ports
	for (unsigned long i = 0L; i < ports.size(); i++)
	{
		PVCAM_ERROR_CHECK (pl_set_param (cameraHandle, PARAM_READOUT_PORT, (void*)&ports[i].value), msg);

		unsigned int speedCount;
		PVCAM_ERROR_CHECK (pl_get_param (cameraHandle, PARAM_SPDTAB_INDEX, ATTR_COUNT, (void*)&speedCount), msg);

		for (unsigned int j = 0; j < speedCount; j++)
		{
			PVCAM_ERROR_CHECK (pl_set_param (cameraHandle, PARAM_SPDTAB_INDEX, (void*)&j), msg);

			unsigned short pixTime;
			PVCAM_ERROR_CHECK (pl_get_param (cameraHandle, PARAM_PIX_TIME, ATTR_CURRENT, (void*)&pixTime), msg);

			short bitDepth;
			PVCAM_ERROR_CHECK (pl_get_param (cameraHandle, PARAM_BIT_DEPTH, ATTR_CURRENT, (void*)&bitDepth), msg);

			short gainMin, gainMax, gainInc;
			PVCAM_ERROR_CHECK (pl_get_param (cameraHandle, PARAM_GAIN_INDEX, ATTR_MIN, (void*)&gainMin), msg);
			PVCAM_ERROR_CHECK (pl_get_param (cameraHandle, PARAM_GAIN_INDEX, ATTR_MAX, (void*)&gainMax), msg);
			PVCAM_ERROR_CHECK (pl_get_param (cameraHandle, PARAM_GAIN_INDEX, ATTR_INCREMENT, (void*)&gainInc), msg);

			ReadoutOption ro;
			ro.port = ports[i];
			ro.speedIndex = j;
			ro.readoutFrequency = 1000.f / float (pixTime);
			ro.bitDepth = bitDepth;
			ro.gains.clear ();

			short gainValue = gainMin;
			while (gainValue <= gainMax)
			{
				ro.gains.push_back (gainValue);
				gainValue += gainInc;
			}

			speedTable.push_back (ro);

			msg += format ("SpeedTable[%lu].Port = %ld (%d - %s)|", speedTable.size () - 1, i, ro.port.value, ro.port.name.c_str ());
			msg += format ("SpeedTable[%lu].SpeedIndex = %d|", speedTable.size () - 1, ro.speedIndex);
			msg += format ("SpeedTable[%lu].readoutFrequency = %.3f MHz|", speedTable.size () - 1, ro.readoutFrequency);
			msg += format ("SpeedTable[%lu].bitDepth = %d bit|", speedTable.size () - 1, ro.bitDepth);
			for (unsigned long z = 0L; z < ro.gains.size (); z++)
				msg += format ("SpeedTable[%lu].gains[%d] = %d|", speedTable.size () - 1, z, ro.gains[z]);
		}
	}
}

bool PVCamera::IsParameterAvailable (std::string &msg, unsigned int parameterID, const char *parameterName)
{
	if (parameterName == NULL)
		return false;

	rs_bool isAvailable = false;
	PVCAM_ERROR_CHECK (pl_get_param(cameraHandle, parameterID, ATTR_AVAIL, (void*)&isAvailable), msg);

	msg += format("|Parameter %s is available %i", parameterName, isAvailable);

	return isAvailable;
}
