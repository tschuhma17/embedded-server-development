/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
//#include "Common.h"
#include <stdio.h>
#include "es-camera.h"

/**
This sample demonstrates acquisition of multiple "snaps" using callback
notification approach. Each acquisition is started by the host with an
'emulated' software trigger (using the pl_exp_start_seq() function repeatedly,
without re-configuring the acquisition).
*/
int main(int argc, char* argv[])
{
	int h = 50;
	h = SayHi();

	printf("returned value is %d\n", h);
	return 0;

#ifdef SKIP_FOR_NOW
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    // Install the generic ctrl+c handler
    if (!InstallGenericCliTerminationHandler(contexts))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    /**
    Register a callback function to receive a notification when EOF event arrives.
    The handler function will be called by PVCAM. The last parameter can be used to hold
    a pointer to user-specific content that will be passed with the callback function
    once it is invoked by PVCAM.
    */
    if (PV_OK != pl_cam_register_callback_ex3(ctx->hcam, PL_CALLBACK_EOF,
                (void*)GenericEofHandler, ctx))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_register_callback() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("EOF callback handler registered on camera %d\n", ctx->hcam);

    uns32 exposureBytes;
    const uns32 exposureTime = 40; // milliseconds
    // Select the appropriate internal trigger mode for this camera.
    int16 expMode;
    if (!SelectCameraExpMode(ctx, expMode, TIMED_MODE, EXT_TRIG_INTERNAL))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    /**
    Prepare the acquisition. The pl_exp_setup_seq() function returns the size of the
    frame buffer required for the entire frame sequence. Since we are requesting a 
    single frame only, size of one frame will be reported.
    */
    if (PV_OK != pl_exp_setup_seq(ctx->hcam, 1, 1, &ctx->region, expMode,
                exposureTime, &exposureBytes))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_setup_seq() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Acquisition setup successful on camera %d\n", ctx->hcam);
    UpdateCtxImageFormat(ctx);

    // Allocate a buffer of the size reported by the pl_exp_setup_seq() function.
    uns8* frameInMemory = new (std::nothrow) uns8[exposureBytes];
    if (!frameInMemory)
    {
        printf("Unable to allocate buffer for camera %d\n", ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    bool errorOccurred = false;
    uns32 imageCounter = 0;
    while (imageCounter < 5)
    {
        /**
        Start the acquisition. Since the pl_exp_setup_seq() was configured to use
        the internal camera trigger, the acquisition is started immediately.
        In hardware triggering modes the camera would wait for an external trigger signal.
        */
        if (PV_OK != pl_exp_start_seq(ctx->hcam, frameInMemory))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_start_seq() error");
            errorOccurred = true;
            break;
        }
        printf("Acquisition started on camera %d\n", ctx->hcam);

        /**
        Here we need to wait for a frame readout notification signaled by the eofEvent
        in the CameraContext which is raised in the callback handler we registered.
        If the frame does not arrive within 5 seconds, or if the user aborts the acquisition
        with ctrl+c shortcut, the main 'while' loop is interrupted and the acquisition is
        aborted.
        */
        printf("Waiting for EOF event to occur on camera %d\n", ctx->hcam);
        if (!WaitForEofEvent(ctx, 5000, errorOccurred))
            break;

        printf("Frame #%u has been delivered from camera %d\n",
                imageCounter + 1, ctx->hcam);
        ShowImage(ctx, frameInMemory, exposureBytes);

        /**
        When acquiring sequences, call the pl_exp_finish_seq() after the entire sequence
        finishes. This call is not strictly necessary, especially with the latest camera models,
        therefore, if fast, repeated calls to pl_exp_start_seq() are needed, this call can
        be omitted. Omitting the call may increase frame rate with the 'emulated' software triggering
        where pl_exp_start_seq() is usually called in a loop. However, testing is recommended
        to ensure desired acquisition stability.
        */
        if (PV_OK != pl_exp_finish_seq(ctx->hcam, frameInMemory, 0))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_finish_seq() error");
        }
        else
        {
            printf("Acquisition finished on camera %d\n", ctx->hcam);
        }

        imageCounter++;
    }
    /**
    Here the pl_exp_abort() is not strictly required as correctly acquired sequence does not
    need to be aborted. However, it is kept here for situations where the acquisition
    is interrupted by the user or when it unexpectedly times out.
    */
    if (PV_OK != pl_exp_abort(ctx->hcam, CCS_HALT))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
    }

    // Cleanup before exiting the application.
    delete [] frameInMemory;

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
#endif
}
