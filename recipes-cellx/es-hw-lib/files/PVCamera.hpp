#ifndef _PVCAMERA_HPP
#define _PVCAMERA_HPP

#include <string>
#include <vector>

class PVCamera
{
public:
	PVCamera ();
	~PVCamera ();

	/// To be called at start of runtime to initialize drivers supplied by camera manufacturers.
	static int InitDriver( std::string &errorMsg );
	/// Get a version number.
	static int GetVersion( std::string &msg );
	static int GetNumberOfCameras( std::string &msg );
	static bool GetCameraName( int camHandle, std::string & cameraName );

	/// To be implemented higher up, by camera_retiga.h.
	// static ? GetCameras( ? );

	/// To be called at end of runtime to tear down drivers supplied by camera manufacturers.
	static bool TerminateDriver( std::string &msg );
	/// Get a picture
	bool GetFrame (unsigned short *bufferOut, std::string &msgOut);
	/// Get a string describing the most recent error.
	int GetErrorState(std::string &msg);

	unsigned short GetFrameWidth() { return frameWidth; }
	unsigned short GetFrameHeight() { return frameHeight; }
	unsigned int GetExposureTime() { return exposureTime_us; }
	unsigned int GetGain() { return gainIdx; }
	unsigned int GetBinningX() { return binningX; }
	unsigned int GetBinningY() { return binningY; }

	void SetFrameWidth(const unsigned int &width);
	void SetFrameHeight(const unsigned int &height);
	void SetExposureTime(const unsigned long &expTime_us);
	void SetGain (std::string &msg, const int &gainId);
	void SetBinFactor (const int &binX, const int &binY);

	void Initialize (std::string &msg, char *camName, const short &camId);
	void Release (std::string &msg);
	size_t GetFrameSize( std::string &msg );

protected:
	enum TIMING
	{
		TIMING_MICROSECOND,
		TIMING_MILLISECOND,
		TIMING_SECOND
	};

	void SetTiming (std::string &msg, const TIMING &t);
	inline TIMING getTiming () { return timing; }

protected:
	void CameraInfo (std::string &msg);
	void CreateSpeedTable (std::string &msg);
	bool IsParameterAvailable (std::string &msg, unsigned int parameterID, const char *parameterName);
	int errorCode;
	std::string errorMessage;
	std::string version;

	short cameraHandle;
	std::string cameraName;

	bool recreateBuffers;
	unsigned int gainIdx;
	unsigned long exposureTime_us;
	unsigned int exposureMultiplier;
	unsigned int exposureSize;
	unsigned short *exposureFrame;

	unsigned int binningX, binningY;
	unsigned short frameWidth, frameHeight;
	unsigned short clearCycles;
	TIMING timing;
	int frameTimoutTime;

//	FRAME_INFO *m_pFrameInfo;
	void *m_pFrameInfo;

	struct NVP
	{
		int value;
		std::string name;
	};

	std::vector<NVP> ports;

	struct ReadoutOption
	{
		NVP port;
		short speedIndex;
		float readoutFrequency;
		short bitDepth;
		std::vector<short> gains;
	};

	std::vector <ReadoutOption> speedTable;
};

#endif 
