DESCRIPTION = "Embedded Server Hardware Library"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

COMPATIBLE_HOST = "aarch64-tdx-linux"

DEPENDS += "pvcam pvcamddi"
RDEPENDS_${PN} += "pvcam pvcamddi"

FILESDIR = "camera"
SRC_URI = "file://PVCamera.cpp \
		   file://PVCamera.hpp \
		   file://StringFormat.cpp \
		   file://StringFormat.h \
		   file://PVCam_helper.h \
		   file://Common.cpp \
		   file://Common.h \
		   file://Semaphore.cpp \
		   file://Semaphore.h \
		   file://Task.cpp \
		   file://Task.h \
		   file://TaskSet_CopyMemory.cpp \
		   file://TaskSet_CopyMemory.h \
		   file://TaskSet.cpp \
		   file://TaskSet.h \
		   file://ThreadPool.cpp \
		   file://ThreadPool.h \
		   file://version.h \
           "
SRC = "Common.cpp Semaphore.cpp Task.cpp TaskSet_CopyMemory.cpp TaskSet.cpp ThreadPool.cpp StringFormat.cpp PVCamera.cpp"

INSANE_SKIP:${PN} = "build-deps"

OUT_FILE_NAME = "es-hw.a"

S = "${WORKDIR}"
CXXFLAGS += " -g -Wall -c -fpermissive"
LDFLAGS += " -ldl -lpthread -lrt -Llibs/aarch64/ -L${libdir}"
LDFLAGS += " -lpvcam -lpvcamDDI"
INCFLAGS += " -I."
INCFLAGS += " -I${WORKDIR}/recipe-sysroot/usr/include"


do_compile() {
    echo "ES-hw-lib is compiling."
	${CXX} -O0 ${SRC} ${INCFLAGS} ${CXXFLAGS} ${LDFLAGS}
	ar -r -o ${OUT_FILE_NAME} *.o
}

do_install() {
    echo "ES-hw-lib is installing"
	install -d ${D}${libdir}
	install -m 0755 *.o ${D}${libdir}
	install -m 0755 es-hw.a ${D}${libdir}
# Install heaader files to the target system.
	install -d ${D}${includedir}
	install -m 0755 ${WORKDIR}/*.h ${D}${includedir}
	install -m 0755 ${WORKDIR}/*.hpp ${D}${includedir}
}

FILES_${PN} += " \
                ${includedir}/*.h \
                ${includedir}/*.hpp \
                ${libdir}/*.a \
"
