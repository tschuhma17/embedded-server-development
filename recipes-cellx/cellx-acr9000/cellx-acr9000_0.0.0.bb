
REALPN = "cellx-acr9000"

LICENSE = "CLOSED"

S = "${WORKDIR}/${REALPN}-${PV}"

PREBUILD = "${TOPDIR}/../layers/meta-cellx/prebuilt_dotnet"

do_install() {
    install -d "${D}${bindir}/Acr9000"
    install -m 0755 ${PREBUILD}/Acr9000/*.exe ${D}${bindir}/Acr9000
    install -m 0755 ${PREBUILD}/Acr9000/*.dll ${D}${bindir}/Acr9000
    install -m 0644 ${PREBUILD}/Acr9000/*.xml ${D}${bindir}/Acr9000
    install -m 0644 ${PREBUILD}/Acr9000/*.txt ${D}${bindir}/Acr9000
    install -m 0644 ${PREBUILD}/Acr9000/*.csv ${D}${bindir}/Acr9000
    install -m 0644 ${PREBUILD}/Acr9000/*.config ${D}${bindir}/Acr9000
}
