DESCRIPTION = "SingleImage_callbacks cross-compiled for Toradex board."
# This file is a stepping stone. It builds one of Teledyne Photometrics' sample applications so it can run on the Toradex board.
# It demonstrates how we can either...
#  A. Change wes to link to pvcam libraries or
#  B. Make a middle library, which abstracts pvcam and implements a couple simple functions for wes to call.
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS += "pvcam pvcamddi libes-hw"
RDEPENDS_${PN} += "pvcam pvcamddi"

SRC_URI = "file://salve_main.cpp \
"

SRC = "salve_main.cpp"

S = "${WORKDIR}"
CXXFLAGS += " -g -std=c++14"
LDFLAGS = " -ldl -lpthread -lrt -Llibs/aarch64/"
LDFLAGS += " -lpvcam -lpvcamDDI"
LIBS += " ${WORKDIR}/recipe-sysroot/usr/lib/es-hw.a"
INCFLAGS += " -I${S}"
INCFLAGS += " -I${WORKDIR}/recipe-sysroot/usr/include"

do_compile() {
    echo "Salve, compiler"
    ls ${WORKDIR}/recipe-sysroot/usr/include/es-camera.h
	${CXX} -O0 ${SRC} ${INCFLAGS} ${CXXFLAGS} ${LDFLAGS} -o salve_app ${LIBS}
}

do_install() {
    echo "lib directory is ${D}${libdir}"
    echo "lib directory short is ${D}"
    echo "Salve, installer"
	install -d ${D}${bindir}
	install -m 0755 salve_app ${D}${bindir}
}

