#include <iostream>
#include <b64/encode.h>
#include <b64/decode.h>
#include <cstdio> // popen
#include "pylon_wrapper.hpp"

using namespace std;

int demoUseOfPopen( void )
{
	int bufferSize = 2803200;
	char buffer[bufferSize];
	int i = 0;
	FILE* fp = popen( "python3 rochacha.py", "r");
	if (fp == NULL)
	{
		cout << "null file ptr." << endl;
		return -1;
	}
	do {
		buffer[i] = fgetc( fp );
		i++;
	} while (i < bufferSize);
	
	if (i == bufferSize)
	{
		cout << "Collected full buffer" << endl;
	}
	else
	{
		cout << "Failed. Collected " << i << " bytes" << endl;
	}
	return 0;
}

void demoUseOfPywrap( void )
{
	PylonWrapper * pywrap = new PylonWrapper();
	int width = pywrap->getWidth();
	cout << "width = " << width << endl;
	delete pywrap;
}

int main(int argc, char **argv) {
	cout << "Moshimoshi" << endl;
	base64::encoder e;
//	return demoUseOfPopen();
	demoUseOfPywrap();
	return 0;
}
