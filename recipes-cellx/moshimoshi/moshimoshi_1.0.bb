DESCRIPTION = "Simple hello-world application"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS += "libb64"
DEPENDS += "pywrap"
RDEPENDS_${PN} += "pywrap"
B64_DIR = "${TOPDIR}/tmp/sysroots-components/aarch64/libb64/usr"
PYWRAP_DIR = "${TOPDIR}/tmp/sysroots-components/aarch64/pywrap/usr"

SRC_URI = "file://moshimoshi.cpp"

S = "${WORKDIR}"
LDFLAGS += " -L${B64_DIR}/lib -lb64"
LDFLAGS += " -L${PYWRAP_DIR}/lib -lPyWrap"
INCFLAGS += " -I${WORKDIR}/include"

do_compile() {
    echo "Moshimoshi, compiler"
	${CXX} moshimoshi.cpp ${LDFLAGS} -I${B64_DIR}/include/b64/ -o moshimoshi
}

do_install() {
    echo "Moshimoshi, installer"
	install -d ${D}${bindir}
	install -m 0755 moshimoshi ${D}${bindir}
}

