SUMMARY = "Basler PYLON library"
# This is a WIP based on "https://docs.yoctoproject.org/next/dev-manual/prebuilt-libraries.html". It is not really the way to go, though.
SECTION = "libs"
DESCRIPTION = "Library for controlling Basler cameras"
# Follow up. The GenICam license is a modified BSD. Find the elusive GenICam_license.pdf and incorporate it into this licensing setup.
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD;md5=3775480a712fc46a69647678acb234cb"

# Don't do this because it is not compatible with "inherit native"
# COMPATIBLE_HOST = "aarch64-tdx-linux"

# Basler's libraries depend on libdl.so and libc.so
inherit native

SRC_URI = " \
	file://libFirmwareUpdate_gcc_v3_1_Basler_pylon.so \
	file://libGCBase_gcc_v3_1_Basler_pylon.so \
	file://libGenApi_gcc_v3_1_Basler_pylon.so \
	file://libgxapi.so.7.2.1 \
	file://liblog4cpp_gcc_v3_1_Basler_pylon.so \
	file://libLog_gcc_v3_1_Basler_pylon.so \
	file://libMathParser_gcc_v3_1_Basler_pylon.so \
	file://libNodeMapData_gcc_v3_1_Basler_pylon.so \
	file://libpylonbase.so.7.2.1 \
	file://libpylonc.so.7.2.1 \
	file://libPylonDataProcessingCore.so.1.1.1 \
	file://libPylonDataProcessingGui.so.1.1.1 \
	file://libPylonDataProcessing.sig \
	file://libPylonDataProcessing.so.1.1.1 \
	file://libpylon_TL_camemu.so \
	file://libpylon_TL_gige.so \
	file://libpylon_TL_gtc.so \
	file://libpylon_TL_usb.so \
	file://libpylonutility.so.7.2.1 \
	file://libuxapi.so.7.2.1 \
	file://libXmlParser_gcc_v3_1_Basler_pylon.so \
	file://pylon-libusb-1.0.so \
	file://inc \
	"

INSANE_SKIP:${PN} = "ldflags"
INHIBIT_PACKAGE_STRIP = "1"
INHIBIT_SYSROOT_STRIP = "1"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"

do_install() {
	install -m 0755 -d ${D}${libdir}
	install -m 0755 ${WORKDIR}/libFirmwareUpdate_gcc_v3_1_Basler_pylon.so ${D}${libdir}
	install -m 0755 ${WORKDIR}/libGCBase_gcc_v3_1_Basler_pylon.so ${D}${libdir}
	install -m 0755 ${WORKDIR}/libGenApi_gcc_v3_1_Basler_pylon.so ${D}${libdir}
	oe_soinstall ${WORKDIR}/libgxapi.so.7.2.1 ${D}${libdir}
	install -m 0755 ${WORKDIR}/liblog4cpp_gcc_v3_1_Basler_pylon.so ${D}${libdir}
	install -m 0755 ${WORKDIR}/libLog_gcc_v3_1_Basler_pylon.so ${D}${libdir}
	install -m 0755 ${WORKDIR}/libMathParser_gcc_v3_1_Basler_pylon.so ${D}${libdir}
	install -m 0755 ${WORKDIR}/libNodeMapData_gcc_v3_1_Basler_pylon.so ${D}${libdir}
	oe_soinstall ${WORKDIR}/libpylonbase.so.7.2.1 ${D}${libdir}
	oe_soinstall ${WORKDIR}/libpylonc.so.7.2.1 ${D}${libdir}
	oe_soinstall ${WORKDIR}/libPylonDataProcessingCore.so.1.1.1 ${D}${libdir}
	oe_soinstall ${WORKDIR}/libPylonDataProcessingGui.so.1.1.1 ${D}${libdir}
	install -m 0755 ${WORKDIR}/libPylonDataProcessing.sig ${D}${libdir}
	oe_soinstall ${WORKDIR}/libPylonDataProcessing.so.1.1.1 ${D}${libdir}
	install -m 0755 ${WORKDIR}/libpylon_TL_camemu.so ${D}${libdir}
	install -m 0755 ${WORKDIR}/libpylon_TL_gige.so ${D}${libdir}
	install -m 0755 ${WORKDIR}/libpylon_TL_gtc.so ${D}${libdir}
	install -m 0755 ${WORKDIR}/libpylon_TL_usb.so ${D}${libdir}
	oe_soinstall ${WORKDIR}/libpylonutility.so.7.2.1 ${D}${libdir}
	oe_soinstall ${WORKDIR}/libuxapi.so.7.2.1 ${D}${libdir}
	install -m 0755 ${WORKDIR}/libXmlParser_gcc_v3_1_Basler_pylon.so ${D}${libdir}
	install -m 0755 ${WORKDIR}/pylon-libusb-1.0.so ${D}${libdir}
	install -d ${D}${includedir}
	# Copy all of the header files in ${WORKDIR}/inc/ to a place we hope is available to dependent recipes.
	cp -r ${WORKDIR}/inc/* ${D}${includedir}
}

# Ship the files
# This avoids error: "Files/directories were installed but not shipped in any package"
FILES_${PN} += " \
                ${libdir}/* \
                ${includedir}/*.h \
"
