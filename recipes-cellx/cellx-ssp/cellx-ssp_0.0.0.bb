
REALPN = "cellx-ssp"

LICENSE = "CLOSED"

S = "${WORKDIR}/${REALPN}-${PV}"

PREBUILD = "${TOPDIR}/../layers/meta-cellx/prebuilt_dotnet"

do_install() {
    install -d "${D}${bindir}/SSP"
    install -m 0755 ${PREBUILD}/SSP/*.exe ${D}${bindir}/SSP
    install -m 0755 ${PREBUILD}/SSP/*.dll ${D}${bindir}/SSP
    install -m 0644 ${PREBUILD}/SSP/*.xml ${D}${bindir}/SSP
    install -m 0644 ${PREBUILD}/SSP/*.txt ${D}${bindir}/SSP
    install -m 0644 ${PREBUILD}/SSP/*.reg ${D}${bindir}/SSP
    install -m 0644 ${PREBUILD}/SSP/*.config ${D}${bindir}/SSP
}
