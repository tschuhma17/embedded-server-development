DESCRIPTION = "Chris Venter's Base64 library"
LICENSE = "PD"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/PD;md5=b3597d12946881e13cb3b548d1173851"

SRC_URI = "file://cdecode.c \
		   file://b64/cdecode.h \
		   file://cencode.c \
		   file://b64/cencode.h \
		   file://b64/decode.h \
		   file://b64/encode.h \
           "

S = "${WORKDIR}/"
PACKAGES = "${PN}-staticdev"
RDEPENDS_${PN}-staticdev = ""

do_compile() {
    echo "libb64 is compiling."
    ls -l "${S}"
    ls -l "${S}/b64"
	${CC} -c cdecode.c -o cdecode.o
	${CC} -c cencode.c -o cencode.o
	${AR} rcs libb64.a cdecode.o cencode.o
}

FILES_${PN}-staticdev = "${libdir}/* ${includedir}/b64/*"

do_install() {
    echo "libb64 is installing"
	install -d ${D}${libdir}
	install -d ${D}${includedir}
	install -d ${D}${includedir}/b64
	install -m 0755 libb64.a ${D}${libdir}
	install -m 0555 ${S}/b64/encode.h ${D}${includedir}/b64
	install -m 0555 ${S}/b64/decode.h ${D}${includedir}/b64
	install -m 0555 ${S}/b64/cencode.h ${D}${includedir}/b64
	install -m 0555 ${S}/b64/cdecode.h ${D}${includedir}/b64
}

