DESCRIPTION = "Portions of LumaViewPro for use in controlling an Etaluma microscope"
# TODO: Research LumaViewPro's license.
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS = "python3-pip-native"

SRC_URI =  " \
			file://certifi-2023.5.7-py3-none-any.whl \
			file://Pillow-9.5.0-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl \
			file://charset_normalizer-3.1.0-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl \
			file://plyer-2.1.0-py2.py3-none-any.whl \
			file://docutils-0.20.1-py3-none-any.whl \
			file://Pygments-2.15.1-py3-none-any.whl \
			file://idna-3.4-py3-none-any.whl \
			file://pyserial-3.5-py2.py3-none-any.whl \
			file://Kivy-2.2.1-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl \
			file://requests-2.31.0-py3-none-any.whl \
			file://Kivy_Garden-0.1.5-py3-none-any.whl \
			file://scipy-1.10.1-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl \
			file://numpy-1.24.3-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl \
			file://urllib3-2.0.3-py3-none-any.whl \
			file://opencv_python-4.7.0.72-cp37-abi3-manylinux_2_17_aarch64.manylinux2014_aarch64.whl \
			file://api_test.py \
			file://labware.py \
			file://lumascope_api.py \
			file://lvp_logger.py \
			file://post_processing.py \
			file://script_example.py \
			file://image_stitcher.py \
			file://ledboard.py \
			file://lumaviewpro.py \
			file://motorboard.py \
			file://pyloncamera.py \
			file://install_wheels.sh \
           "

do_install() {
	install -d ${D}${datadir}/wheels
	install -d ${D}${bindir}
	install -m 0644 ${WORKDIR}/certifi-2023.5.7-py3-none-any.whl ${D}${datadir}/wheels/
	install -m 0644 ${WORKDIR}/Pillow-9.5.0-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl ${D}${datadir}/wheels/
	install -m 0644 ${WORKDIR}/charset_normalizer-3.1.0-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl ${D}${datadir}/wheels/
	install -m 0644 ${WORKDIR}/plyer-2.1.0-py2.py3-none-any.whl ${D}${datadir}/wheels/
	install -m 0644 ${WORKDIR}/docutils-0.20.1-py3-none-any.whl ${D}${datadir}/wheels/
	install -m 0644 ${WORKDIR}/Pygments-2.15.1-py3-none-any.whl ${D}${datadir}/wheels/
	install -m 0644 ${WORKDIR}/idna-3.4-py3-none-any.whl ${D}${datadir}/wheels/
	install -m 0644 ${WORKDIR}/pyserial-3.5-py2.py3-none-any.whl ${D}${datadir}/wheels/
	install -m 0644 ${WORKDIR}/Kivy-2.2.1-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl ${D}${datadir}/wheels/
	install -m 0644 ${WORKDIR}/requests-2.31.0-py3-none-any.whl ${D}${datadir}/wheels/
	install -m 0644 ${WORKDIR}/Kivy_Garden-0.1.5-py3-none-any.whl ${D}${datadir}/wheels/
	install -m 0644 ${WORKDIR}/scipy-1.10.1-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl ${D}${datadir}/wheels/
	install -m 0644 ${WORKDIR}/numpy-1.24.3-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl ${D}${datadir}/wheels/
	install -m 0644 ${WORKDIR}/urllib3-2.0.3-py3-none-any.whl ${D}${datadir}/wheels/
	install -m 0644 ${WORKDIR}/opencv_python-4.7.0.72-cp37-abi3-manylinux_2_17_aarch64.manylinux2014_aarch64.whl ${D}${datadir}/wheels/
	install -m 0644 ${WORKDIR}/api_test.py ${D}${bindir}
	install -m 0644 ${WORKDIR}/labware.py ${D}${bindir}
	install -m 0644 ${WORKDIR}/lumascope_api.py ${D}${bindir}
	install -m 0644 ${WORKDIR}/lvp_logger.py ${D}${bindir}
	install -m 0644 ${WORKDIR}/post_processing.py ${D}${bindir}
	install -m 0644 ${WORKDIR}/script_example.py ${D}${bindir}
	install -m 0644 ${WORKDIR}/image_stitcher.py ${D}${bindir}
	install -m 0644 ${WORKDIR}/ledboard.py ${D}${bindir}
	install -m 0644 ${WORKDIR}/lumaviewpro.py ${D}${bindir}
	install -m 0644 ${WORKDIR}/motorboard.py ${D}${bindir}
	install -m 0644 ${WORKDIR}/pyloncamera.py ${D}${bindir}
	install -m 0755 ${WORKDIR}/install_wheels.sh ${D}${datadir}/wheels/
}

FILES_${PN} += " \
	${datadir}/wheels \
	${bindir} \
"

