#!/usr/bin/python3

# ----------------------------------------------------
# lumascope_api testing. Not intended for customer
# ----------------------------------------------------

# libraries
# ----------------------------------------------------
from ledboard import LEDBoard
import time

led = LEDBoard()
# scope = lumascope_api.Lumascope()
time.sleep(2)

# For command set_fluorescence_light_filter_properties - on_status
led.led_on(led.color2ch('Blue'), 100) 
time.sleep(2)
led.led_off(led.color2ch('Blue')) 

led.led_on(led.color2ch('Green'), 100) 
time.sleep(2)
led.led_off(led.color2ch('Green')) 

led.led_on(led.color2ch('Red'), 100) 
time.sleep(2)
led.led_off(led.color2ch('Red'))

time.sleep(2)

# For command set white_light_source_properties - on_status
led.led_on(led.color2ch('BF'), 100) 
time.sleep(6)
led.led_off(led.color2ch('BF')) 
'''
led.led_on(led.color2ch('PC'), 1) 
time.sleep(2)
led.led_off(led.color2ch('PC')) 

led.led_on(led.color2ch('EP'), 1) 
time.sleep(2)
led.led_off(led.color2ch('EP')) 
'''
# verified functional
