#!/bin/sh

# Install Kivy's dependencies, then Kivy.
python3 -m pip install /usr/share/wheels/urllib3-2.0.3-py3-none-any.whl
python3 -m pip install /usr/share/wheels/charset_normalizer-3.1.0-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl
python3 -m pip install /usr/share/wheels/certifi-2023.5.7-py3-none-any.whl
python3 -m pip install /usr/share/wheels/idna-3.4-py3-none-any.whl
python3 -m pip install /usr/share/wheels/docutils-0.20.1-py3-none-any.whl
python3 -m pip install /usr/share/wheels/Pygments-2.15.1-py3-none-any.whl
python3 -m pip install /usr/share/wheels/requests-2.31.0-py3-none-any.whl
python3 -m pip install /usr/share/wheels/Kivy_Garden-0.1.5-py3-none-any.whl
python3 -m pip install /usr/share/wheels/Kivy-2.2.1-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl
# Install more libraries that LumaView depends on.
python3 -m pip install /usr/share/wheels/Pillow-9.5.0-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl
python3 -m pip install /usr/share/wheels/plyer-2.1.0-py2.py3-none-any.whl
python3 -m pip install /usr/share/wheels/pyserial-3.5-py2.py3-none-any.whl
python3 -m pip install /usr/share/wheels/numpy-1.24.3-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl
python3 -m pip install /usr/share/wheels/opencv_python-4.7.0.72-cp37-abi3-manylinux_2_17_aarch64.manylinux2014_aarch64.whl
python3 -m pip install /usr/share/wheels/scipy-1.10.1-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl

# Copy lumaview files to /usr/lib/python3.8/site-packages, which is a directory in Python's sys.path.
# cp /usr/share/wheels/*.py /usr/lib/python3.8/site-packages
# Leave evidence that this script has been run.
touch /usr/share/wheels/install_was_run.txt
# Make a log directory. Temporary work around for bug in lvp_logger.py.
mkdir /home/root/logs
mkdir /home/root/logs/LVP_log
