DESCRIPTION = "Prebuilt PVCAM library by Teledyne Photometrics."
# Based on https://docs.yoctoproject.org/dev-manual/common-tasks.html#versioned-libraries
# Two of the files in SRC_URI (libpvcam.so.2.6, and pvcam_usb.aarch64.umd) are prebuilt libraries supplied by Teledyne Photometrics.
# They were built for aarch64 architecture. It is hard to pull them out of the intstaller, but there is a trick that works.
# Extract the files from PVCAM-Linux-3-9-11-2.zip. Then run "pvcam/pvcam_3.9.11.2.run --keep". Doing this will install the libraries
# appropriate for the machine on which you ran it, but it will also keep the temporary directory in which everything was unpacked.
# Search that directory for aarch64 binaries.
SECTION = "libs"
# TODO: Revisit licensing
LICENSE_FLAGS = "commercial"
LICENSE = "CLOSED"

COMPATIBLE_HOST = "aarch64-tdx-linux"

DEPENDS += "libusb1"
RDEPENDS_${PN} += "libusb1"

# User Mode Directory -- where USB drivers go. (pvcam_usb.version and pvcam_usb.x86_64.umd)
UMD_DIR = "${libdir}/pvcam/user_mode"

SRC_URI += " \
           file://libpvcam.so.2.6 \
           file://libpvcam.version \
           file://master.h \
           file://pvcam.h \
           file://pvcam_helper_color.h \
           file://pvcam_helper_track.h \
           file://pvcam_usb.aarch64.umd \
           file://pvcam_usb.version \
           file://pvcam.sh \
"

INSANE_SKIP:${PN} = "ldflags"
INHIBIT_PACKAGE_STRIP = "1"
INHIBIT_SYSROOT_STRIP = "1"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"

do_install() {
    echo "lib directory is ${D}${libdir}"
# Install pvcam.so to the target system.
    install -m 0755 -d ${D}${libdir}
    oe_soinstall ${WORKDIR}/libpvcam.so.${PV} ${D}${libdir}
    install -m 0644 ${WORKDIR}/libpvcam.version ${D}${libdir}
# Install heaader files to the target system.
	echo "Installing pvcam headers to ${D}${includedir}"
	echo "D is ${D}"
	install -d ${D}${includedir}
	install -m 0755 ${WORKDIR}/*.h ${D}${includedir}
# Install pvcam's .umd file for connecting to camera via USB.
	install -d ${D}${UMD_DIR}
	install -m 0731 ${WORKDIR}/pvcam_usb.* ${D}${UMD_DIR}
# Install a shell script that sets pvcam environment variables on the target system.
	install -d ${D}${sysconfdir}/profile.d
	install -m 0755 ${WORKDIR}/pvcam.sh ${D}${sysconfdir}/profile.d
}

FILES_${PN} += " \
                ${libdir}/* \
                ${includedir}/*.h \
                ${UMD_DIR}/* \
                ${sysconfdir}/profile.d/* \
"
