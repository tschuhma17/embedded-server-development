DESCRIPTION = "Worker of Embedded Server"
LICENSE = "CLOSED"
inherit systemd

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE_${PN} = "wes.service"

SRC_URI_append = " file://wes.service "
FILES_${PN} += "${systemd_unitdir}/system/wes.service"

do_install_append() {
  install -d ${D}/${systemd_unitdir}/system
  install -m 0644 ${WORKDIR}/wes.service ${D}/${systemd_unitdir}/system
}

