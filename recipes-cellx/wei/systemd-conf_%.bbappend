FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " \
	file://wired.network \
"

FILES:${PN} += " \
	lib/systemd/network/wired.network \
"

do_install:append() {
	install -d ${D}/lib/systemd/network
	install -m 0644 ${WORKDIR}/wired.network ${D}/lib/systemd/network
}
