
ES_SERVICE = "/tmp/cellx.service"
FILES_avahi-daemon += "${sysconfdir}/avahi/services/cellx.service"

do_install_append() {
# Enable publish-address in avahi-daemon.conf
# To do this, we'll use sed to change one line.
# Sed requires write privileges in the directory containing the file, so
# We will temporarily copy the file to /tmp for that purpose.
	cp ${CONFFILES_avahi-daemon} /tmp
	echo "atempt sed"
	sed -i 's/#publish-addresses/publish-addresses/g' /tmp/avahi-daemon.conf
	echo "attempt copy back from /tmp"
	install -m 0644 /tmp/avahi-daemon.conf ${D}${CONFFILES_avahi-daemon}
#	grep publish-address ${D}/${CONFFILES_avahi-daemon}
	echo "Done with avahi-daemon.conf."

# Create a service configuration file. Name it cellx.server.
# It is difficult to get a path to files in other recipes (like wes)
# so we'll instead embed the text content here (below) and create the file in /tmp.
# CAUTION: When editing this, mind the escape characters, which help
# embed double-quotes in the file.
	rm -f ${ES_SERVICE}
	touch ${ES_SERVICE}
	echo "<?xml version=\"1.0\" standalone='no'?><!--*-nxml-*-->" >> ${ES_SERVICE}
	echo "<!DOCTYPE service-group SYSTEM \"avahi-service.dtd\">" >> ${ES_SERVICE}
	echo "<service-group>" >> ${ES_SERVICE}
	echo "    <name replace-wildcards=\"yes\">%h</name>" >> ${ES_SERVICE}
	echo "    <service>" >> ${ES_SERVICE}
	echo "        <type>_cellx-es._tcp</type>" >> ${ES_SERVICE}
	echo "        <port>1883</port> " >> ${ES_SERVICE}
	echo "    </service>" >> ${ES_SERVICE}
	echo "</service-group>" >> ${ES_SERVICE}
  
	install -d ${D}/${sysconfdir}/avahi/services
	install -m 0644 ${ES_SERVICE} ${D}${sysconfdir}/avahi/services
#	echo "FILES"
#	echo "${FILES_avahi-daemon}"
	echo "Done with cellx.server."
}
