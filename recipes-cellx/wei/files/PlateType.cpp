#include "PlateType.hpp"
#include "es_des.h"
#include "Sundry.hpp"
#include <string>

using namespace std;

/// Constructors
PlateType::PlateType() : rowCount(0), columnCount(0), plateLength(0), plateWidth(0),
	plateHeight(0), firstWellX(0), firstWellY(0), interWellX(0), interWellY(0), wellVolume(0),
	workingVolume(0), wellGeometry(PlateWellGeometryType::Unknown),	wellInnerDiameter(0),
	wellInnerHeight(0), swirlRadius(0), swirlVelocity(0), swirlAcceleration(0)
{
}

PlateType::~PlateType()
{
}

bool PlateType::PopulateFromJsonCObject( json_object* jsonInObj )
{
	bool succeeded = true;
	rowCount = GetInt( &succeeded, jsonInObj, ROW_COUNT );
	columnCount = GetInt( &succeeded, jsonInObj, COLUMN_COUNT );
	plateLength = GetInt( &succeeded, jsonInObj, PLATE_LENGTH );
	plateWidth = GetInt( &succeeded, jsonInObj, PLATE_WIDTH );
	plateHeight = GetInt( &succeeded, jsonInObj, PLATE_HEIGHT );
	firstWellX = GetInt( &succeeded, jsonInObj, FIRST_WELL_X );
	firstWellY = GetInt( &succeeded, jsonInObj, FIRST_WELL_Y );
	interWellX = GetInt( &succeeded, jsonInObj, INTER_WELL_X );
	interWellY = GetInt( &succeeded, jsonInObj, INTER_WELL_Y );
	wellVolume = GetInt( &succeeded, jsonInObj, WELL_VOLUME );
	workingVolume = GetInt( &succeeded, jsonInObj, WORKING_VOLUME );

	string wellGeometryText = json_object_get_string( json_object_object_get( jsonInObj, WELL_GEOMETRY ) );
	wellGeometry = PlateWellGeometryType::Unknown;
	if (wellGeometryText == ENUM_CIRCULAR) wellGeometry = PlateWellGeometryType::Circular;
	if (wellGeometryText == ENUM_RECTANGULAR) wellGeometry = PlateWellGeometryType::Rectangular;

	wellInnerDiameter = GetInt( &succeeded, jsonInObj, WELL_INNER_DIAMETER );
	wellInnerHeight = GetInt( &succeeded, jsonInObj, WELL_INNER_HEIGHT );
	swirlRadius = GetInt( &succeeded, jsonInObj, SWIRL_RADIUS );
	swirlVelocity = GetInt( &succeeded, jsonInObj, SWIRL_VELOCITY );
	swirlAcceleration = GetInt( &succeeded, jsonInObj, SWIRL_ACCELERATION );
	return succeeded;
}

/// A helper for PopulateFromJsonCObject.
int PlateType::GetInt( bool * succeeded, json_object* jsonInObj, const char* name )
{
	json_object* temp = json_object_object_get( jsonInObj, name );
	if (temp == nullptr) /// Name not found.
	{
		*succeeded = false;
		return 0;
	}
	return json_object_get_int( temp );
}

