#ifndef _ES_CAMERA_HPP
#define _ES_CAMERA_HPP

#include <string>
#include <vector>

class ES_CameraBase
{
public:
	ES_CameraBase() {}
	virtual ~ES_CameraBase() {};

	/// To be called at start of runtime to initialize drivers supplied by camera manufacturers.
	/// static int InitDriver();

	/// To be called at end of runtime to tear down drivers supplied by camera manufacturers.
	/// static int TerminateDriver();

	/// Get a picture
	virtual bool GetFrame (unsigned short *bufferOut, std::string &msgOut) = 0;
	/// Get a version number.
	static int GetVersion(std::string &msg) { return -1; }
	/// Get a string describing the most recent error.
	virtual int GetErrorState(std::string &msg) = 0;
	virtual void Release(std::string &msg) = 0;

	virtual unsigned short GetFrameWidth() = 0;
	virtual unsigned short GetFrameHeight() = 0;
	virtual unsigned int GetExposureTime() = 0;
	virtual unsigned int GetGain() = 0;
	virtual unsigned int GetBinningX() = 0;
	virtual unsigned int GetBinningY() = 0;

	virtual void SetFrameWidth(const unsigned int &width) = 0;
	virtual void SetFrameHeight(const unsigned int &height) = 0;
	virtual void SetExposureTime(const unsigned long &expTime_us) = 0;
	virtual void SetGain (std::string &msg, const int &gainId) = 0;
	virtual void SetBinFactor (const int &binX, const int &binY) = 0;
	virtual size_t GetFrameSize( std::string &msg ) = 0;

};

#endif 
