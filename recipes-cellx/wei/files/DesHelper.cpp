#include <iostream>
#include <sstream>
#include <cassert>
//#include <string.h>
#include <string>
#include <cstring>
#include <unordered_map>
#include "es_des.h"
#include "DesHelper.hpp"
#include "MsgQueue.hpp"
#include "Fault.hpp"
#include "Sundry.hpp"
#include "json.h"
#include "Components.hpp"

using namespace std;

//template <typename E>
//constexpr typename std::underlying_type<E>::type to_underlying(E e)
//{
//	return static_cast<typename std::underlying_type<E>::type>(e);
//}


/// Constructor
DesHelper::DesHelper() : m_names( new NameData("error", "error") )
{
//	cerr << "DesHelper(): m_machineId is " << m_names->m_machineId << endl;
}

/// Constructor
DesHelper::DesHelper(std::string machineId, std::string masterId) : m_names( new NameData(machineId, masterId) )
{
//	cerr << "DesHelper(id, id): m_machineId is " << m_names->m_machineId << endl;
}
    
/// Destructor
DesHelper::~DesHelper()
{
}

///----------------------------------------------------------------------
/// ParseJsonDesMessage
/// Convert a JSON string into a json_object.
///----------------------------------------------------------------------
json_object * DesHelper::ParseJsonDesMessage(const char* jsonDesMessage, bool* success)
{
    json_object *jobj = nullptr;
    *success = true;
	enum json_tokener_error jerr;

	jobj = json_tokener_parse_verbose( jsonDesMessage, &jerr );

	if (jerr == json_tokener_success)
	{
//		cout << "DesHelper::ParseJsonDesMessage() reported success." << endl;
	}
	else
	{
		*success = false;

		/// Send error description to cerr.
		cout << "DesHelper::ParseJsonDesMessage() reported error parsing message" << endl;
		cout << "error_type: " <<  json_tokener_error_desc(jerr) << endl;
		/// Save the whole message to file
		std::fstream dumpFile( "/home/root/dump.txt", std::ios::out );
		if (dumpFile.is_open())
		{
			dumpFile << jsonDesMessage;
			dumpFile.close();
		}
		/// Change jobj into a JSON object that contains details of the error
		jobj = json_object_new_object();
		int result = json_object_object_add( jobj, "error_type", json_object_new_string( json_tokener_error_desc(jerr) ) );
		LETS_ASSERT(result == 0);
	}
	
	return jobj;
}

///----------------------------------------------------------------------
/// JsonDesObjectToString
/// Convert a json_object into a JSON string.
///----------------------------------------------------------------------
std::string DesHelper::JsonDesObjectToString(json_object* jsonDesObject)
{
	return json_object_to_json_string(jsonDesObject);
}

///----------------------------------------------------------------------
/// SetMachineId
///----------------------------------------------------------------------
void DesHelper::SetMachineId(std::string machineId)
{
    m_names->m_machineId = machineId;
//	cerr << "DesHelper::SetMachineId to " << m_names->m_machineId << endl;
}

///----------------------------------------------------------------------
/// ComposeCommand
/// Creates a complete COMMAND message as a json_object.
/// The caller must delete the object using json_object_put().
///----------------------------------------------------------------------
json_object* DesHelper::ComposeCommand( std::string messageType,
										int commandGroupId,
										int commandId,
										std::string sessionId,
										json_object * payload )
{
	bool success = true;
	/// Create an empty ACK object.
	json_object *cmd_jobj = json_object_new_object();

	/// Add content to the ACK object.
    int result = json_object_object_add( cmd_jobj, MESSAGE_TYPE, json_object_new_string( messageType.c_str() ) );
    success = success && CheckResult( result, MESSAGE_TYPE, TEXT_COMMAND );
	result = json_object_object_add( cmd_jobj, TRANSPORT_VERSION, json_object_new_string( TRANSPORT_VERSION_VALUE ) );
    success = success && CheckResult( result, TRANSPORT_VERSION, TEXT_COMMAND );
	result = json_object_object_add( cmd_jobj, COMMAND_GROUP_ID, json_object_new_int64( commandGroupId ) );
    success = success && CheckResult( result, COMMAND_GROUP_ID, TEXT_COMMAND );
	result = json_object_object_add( cmd_jobj, COMMAND_ID,	json_object_new_int64( commandId ) );
    success = success && CheckResult( result, COMMAND_ID, TEXT_COMMAND );
	result = json_object_object_add( cmd_jobj, SESSION_ID, json_object_new_string( sessionId.c_str() ) );
    success = success && CheckResult( result, SESSION_ID, TEXT_COMMAND );
	result = json_object_object_add( cmd_jobj,
									 MACHINE_ID,
									 json_object_new_string( m_names->m_machineId.c_str() ) );
    success = success && CheckResult( result, MACHINE_ID, TEXT_COMMAND );
	result = json_object_object_add( cmd_jobj, PAYLOAD, payload );
    success = success && CheckResult( result, PAYLOAD, TEXT_COMMAND );
	return cmd_jobj;
}

///----------------------------------------------------------------------
/// ComposeAck
/// Creates a complete ACK message as a json_object.
/// The caller must delete the object using json_object_put().
///----------------------------------------------------------------------
json_object* DesHelper::ComposeAck( std::string messageType,
									int commandGroupId,
									int commandId,
									std::string sessionId,
									std::string status,
									int statusCode,
									std::chrono::system_clock::rep timestamp,
									json_object * payload )
{
	bool success = true;
	/// Create an empty ACK object.
	json_object *ack_jobj = json_object_new_object();

	/// Add content to the ACK object.
    int result = json_object_object_add( ack_jobj, MESSAGE_TYPE, json_object_new_string( messageType.c_str() ) );
    success = success && CheckResult( result, MESSAGE_TYPE, TEXT_ACK );
	result = json_object_object_add( ack_jobj, TRANSPORT_VERSION, json_object_new_string( TRANSPORT_VERSION_VALUE ) );
    success = success && CheckResult( result, TRANSPORT_VERSION, TEXT_ACK );
	result = json_object_object_add( ack_jobj, COMMAND_GROUP_ID, json_object_new_int64( commandGroupId ) );
    success = success && CheckResult( result, COMMAND_GROUP_ID, TEXT_ACK );
	result = json_object_object_add( ack_jobj, COMMAND_ID,	json_object_new_int64( commandId ) );
    success = success && CheckResult( result, COMMAND_ID, TEXT_ACK );
	result = json_object_object_add( ack_jobj, SESSION_ID, json_object_new_string( sessionId.c_str() ) );
    success = success && CheckResult( result, SESSION_ID, TEXT_ACK );
	result = json_object_object_add( ack_jobj,
									 MACHINE_ID,
									 json_object_new_string( m_names->m_machineId.c_str() ) );
    success = success && CheckResult( result, MACHINE_ID, TEXT_ACK );
	result = json_object_object_add( ack_jobj, TEXT_STATUS, json_object_new_string( status.c_str() ) );
    success = success && CheckResult( result, TEXT_STATUS, TEXT_ACK );
	result = json_object_object_add( ack_jobj, STATUS_CODE, json_object_new_int64( statusCode ) );
    success = success && CheckResult( result, STATUS_CODE, TEXT_ACK );
	result = json_object_object_add( ack_jobj, TIMESTAMP, json_object_new_int64( timestamp ) );
    success = success && CheckResult( result, TIMESTAMP, TEXT_ACK );
	result = json_object_object_add( ack_jobj, PAYLOAD, payload );
    success = success && CheckResult( result, PAYLOAD, TEXT_ACK );
	return ack_jobj;
}

///----------------------------------------------------------------------
/// ComposeAlert
/// Creates a complete ALERT message as a json_object.
/// The caller must delete the object using json_object_put().
/// Probably not needed.
///----------------------------------------------------------------------
json_object* DesHelper::ComposeAlert( std::string messageType,
									  int commandGroupId,
									  int commandId,
									  std::string sessionId,
									  ComponentType component,
									  int statusCode,
									  bool thisIsCausingHold )
{
	bool success = true;
	/// Create an empty ACK object.
	json_object *alertObj = json_object_new_object();

	/// Add content to the ACK object.
    int result = json_object_object_add( alertObj, MESSAGE_TYPE, json_object_new_string( messageType.c_str() ) );
    success = success && CheckResult( result, MESSAGE_TYPE, TEXT_ALERT );
	result = json_object_object_add( alertObj, TRANSPORT_VERSION, json_object_new_string( TRANSPORT_VERSION_VALUE ) );
    success = success && CheckResult( result, TRANSPORT_VERSION, TEXT_ALERT );
	result = json_object_object_add( alertObj, COMMAND_GROUP_ID, json_object_new_int64( commandGroupId ) );
    success = success && CheckResult( result, COMMAND_GROUP_ID, TEXT_ALERT );
	result = json_object_object_add( alertObj, COMMAND_ID,	json_object_new_int64( commandId ) );
    success = success && CheckResult( result, COMMAND_ID, TEXT_ALERT );
	result = json_object_object_add( alertObj, SESSION_ID, json_object_new_string( sessionId.c_str() ) );
    success = success && CheckResult( result, SESSION_ID, TEXT_ALERT );
	result = json_object_object_add( alertObj, MACHINE_ID, json_object_new_string( m_names->m_machineId.c_str() ) );
    success = success && CheckResult( result, MACHINE_ID, TEXT_ALERT );
	string componentTypeStr = ComponentTypeEnumToStringName( component );
	result = json_object_object_add( alertObj, DEVICE_ENUM, json_object_new_string( componentTypeStr.c_str() ) );
    success = success && CheckResult( result, DEVICE_ENUM, TEXT_ALERT );
	result = json_object_object_add( alertObj, STATUS_CODE, json_object_new_int64( statusCode ) );
    success = success && CheckResult( result, STATUS_CODE, TEXT_ALERT );
	result = json_object_object_add( alertObj, IS_CAUSED_HOLD_FLAG, json_object_new_boolean( thisIsCausingHold ) );
    success = success && CheckResult( result, IS_CAUSED_HOLD_FLAG, TEXT_ALERT );
	return alertObj;
}

bool DesHelper::SendAlert( json_object * alertMsgObj,
						   MsgQueue * outBox,
						   std::string originalTopic )
{
	bool succeeded = true;
	std::string alertMessageStr = json_object_to_json_string( alertMsgObj );
	int freed = json_object_put( alertMsgObj ); /// Free memory
	succeeded &= (freed == 1);

	LOG_WARN( "unk", "des", "SENDING AN ALERT!" );
	/// Re-use most of originalTopic, so we don't need to know its details ahead of time.
	/// Alerts always return to the '/alert' channel.
	std::string channelAlert = TEXT_ALERT;
	std::string alertTopic = ReplaceTopicChannel(originalTopic, channelAlert);

	std::shared_ptr<WorkMsg> alert =
		make_shared<WorkMsg>( alertMessageStr,
							  originalTopic,	/// originTopic
							  alertTopic,		/// destinationTopic
							  TEXT_ALERT,		/// messageType
							  "",				/// subMessageType
							  true );			/// theEnd

	/// Send the thread message.
	outBox->PushMsg( alert );
	return succeeded;
}

///----------------------------------------------------------------------
/// ComposeStatus
/// Creates a complete STATUS message as a json_object.
/// The caller must delete the object using json_object_put().
/// Pass in "" for machineId if you want it to be the ES's ID.
///----------------------------------------------------------------------
json_object* DesHelper::ComposeStatus( int commandGroupId,
									   int commandId,
									   std::string sessionId,
									   std::string messageType,
									   std::string machineId,
									   int statusCode,
									   json_object * payload )
{
	bool success = true;
	/// Create an empty ACK object.
	json_object *status_jobj = json_object_new_object();

	/// Add content to the ACK object. TODO: Add COMMAND_GROUP_ID, SESSION_ID
    int result = json_object_object_add( status_jobj, MESSAGE_TYPE, json_object_new_string( messageType.c_str() ) );
    success = success && CheckResult( result, MESSAGE_TYPE, TEXT_STATUS );
	result = json_object_object_add( status_jobj, TRANSPORT_VERSION, json_object_new_string( TRANSPORT_VERSION_VALUE ) );
    success = success && CheckResult( result, TRANSPORT_VERSION, TEXT_STATUS );
	result = json_object_object_add( status_jobj, COMMAND_GROUP_ID, json_object_new_int64( commandGroupId ) );
    success = success && CheckResult( result, COMMAND_GROUP_ID, TEXT_COMMAND );
	result = json_object_object_add( status_jobj, COMMAND_ID,	json_object_new_int64( commandId ) );
    success = success && CheckResult( result, COMMAND_ID, TEXT_STATUS );
	result = json_object_object_add( status_jobj, SESSION_ID, json_object_new_string( sessionId.c_str() ) );
    success = success && CheckResult( result, SESSION_ID, TEXT_COMMAND );
	result = json_object_object_add( status_jobj,
									 MACHINE_ID,
									 json_object_new_string( m_names->m_machineId.c_str() ) );
    success = success && CheckResult( result, MACHINE_ID, TEXT_STATUS );
	result = json_object_object_add( status_jobj, STATUS_CODE, json_object_new_int64( statusCode ) );
    success = success && CheckResult( result, STATUS_CODE, TEXT_STATUS );
	result = json_object_object_add( status_jobj, PAYLOAD, payload );
    success = success && CheckResult( result, PAYLOAD, TEXT_STATUS );
	return status_jobj;
}

std::string DesHelper::ComposeStatusString( int commandGroupId,
											int commandId,
											std::string sessionId,
											std::string messageType,
											std::string machineId,
											int statusCode,
											json_object * payload )
{
	json_object* jStatusObj = ComposeStatus( commandGroupId, commandId, sessionId, messageType, machineId, statusCode, payload ); /// Allocates mem.
	std::string payloadString = json_object_to_json_string( jStatusObj );
	int freed = json_object_put( jStatusObj ); /// Frees mem.
	LETS_ASSERT(freed == 1);
	return payloadString;
}

///----------------------------------------------------------------------
/// IsValidCommand
/// Use this to confirm the structure of a message coming in on a
/// /command channel. It is not an exhaustive check, so more work
/// might be needed here.
///----------------------------------------------------------------------
bool DesHelper::IsValidCommand(json_object* jsonDesObject, std::string & whyInvalid_out)
{
	bool isValid = true;
	json_object *fieldObject = nullptr;
	json_bool found = json_object_object_get_ex(jsonDesObject, MESSAGE_TYPE, &fieldObject);
	if (!found) whyInvalid_out = whyInvalid_out + "missing message_type|";
	isValid = isValid && found;
	found = json_object_object_get_ex(jsonDesObject, TRANSPORT_VERSION, &fieldObject);
	if (!found) whyInvalid_out = whyInvalid_out + "missing transport_version|";
	isValid = isValid && found;
	if (found)
	{
		isValid = isValid && ValidateTransportVersion( json_object_get_string( fieldObject ) );
	}
	found = json_object_object_get_ex(jsonDesObject, COMMAND_GROUP_ID, &fieldObject);
	if (!found) whyInvalid_out = whyInvalid_out + "missing command_group_id|";
	isValid = isValid && found;
	found = json_object_object_get_ex(jsonDesObject, COMMAND_ID, &fieldObject);
	if (!found) whyInvalid_out = whyInvalid_out + "missing command_id|";
	isValid = isValid && found;
	found = json_object_object_get_ex(jsonDesObject, SESSION_ID, &fieldObject);
	if (!found) whyInvalid_out = whyInvalid_out + "missing session_id|";
	isValid = isValid && found;
	found = json_object_object_get_ex(jsonDesObject, MACHINE_ID, &fieldObject);
	if (!found) whyInvalid_out = whyInvalid_out + "missing machine_id|";
	isValid = isValid && found;
	found = json_object_object_get_ex(jsonDesObject, PAYLOAD, &fieldObject);
	if (!found) whyInvalid_out = whyInvalid_out + "missing payload|";
	isValid = isValid && found;
	return isValid;
}

///----------------------------------------------------------------------
/// IsValidAck
/// Use this to confirm the structure of a message coming in on a
/// /ack channel.
/// This is probably not needed. (That's why it is not implemented yet.)
/// We expect the ES to receive acks from the microscope. Those
/// *could* be validated before they are forwarded to the broker.
///----------------------------------------------------------------------
bool DesHelper::IsValidAck(json_object* jsonDesObject)
{
	return false;
}

///----------------------------------------------------------------------
/// IsValidAlert
/// Use this to confirm the structure of a message coming in on a
/// /alert channel.
///----------------------------------------------------------------------
bool DesHelper::IsValidAlert(json_object* jsonDesObject)
{
	// TODO: Implement this.
	return false;
}

///----------------------------------------------------------------------
/// IsValidStatus
/// Use this to confirm the structure of a message coming in on a
/// /status channel.
/// This is probably not needed. (That's why it is not implemented yet.)
/// We expect the ES to receive statuses from the microscope. Those
/// *could* be validated before they are forwarded to the broker.
///----------------------------------------------------------------------
bool DesHelper::IsValidStatus(json_object* jsonDesObject)
{
	return false;
}

///----------------------------------------------------------------------
/// SplitCommand
/// Pulls out all call/command fields except payload.
/// Pass in a NULL for any fields you don't care about.
///----------------------------------------------------------------------
bool DesHelper::SplitCommand(json_object* jsonDesObject,
							 std::string* messageType,
							 std::string* transportVersion,
							 int* commandGroupId,
							 int* commandId,
							 std::string* sessionId,
							 std::string* machineId)
{
	json_object *field = nullptr;
	
	/// Extract the parts we want to copy from the parent.
	if (messageType != nullptr)
	{
		field = nullptr;
		if (json_object_object_get_ex(jsonDesObject, MESSAGE_TYPE, &field))
		{
			if (field)
			{
				*messageType = json_object_get_string(field);
			}
			else
			{
				*messageType = "";
			}
		}
	}
	if (transportVersion != nullptr)
	{
		field = nullptr;
		if (json_object_object_get_ex(jsonDesObject, TRANSPORT_VERSION, &field))
		{
			if (field)
			{
				*transportVersion = json_object_get_string(field);
			}
			else
			{
				*transportVersion = "";
			}
		}
	}
	if (commandGroupId != nullptr)
	{
		field = nullptr;
		if (json_object_object_get_ex(jsonDesObject, COMMAND_GROUP_ID, &field))
		{
			if (field)
			{
				*commandGroupId = json_object_get_int64(field);
			}
			else
			{
				*commandGroupId = 0;
			}
		}
	}
	if (commandId != nullptr)
	{
		field = nullptr;
		if (json_object_object_get_ex(jsonDesObject, COMMAND_ID, &field))
		{
			if (field)
			{
				*commandId = json_object_get_int64(field);
			}
			else
			{
				*commandId = 0;
			}
		}
	}
	if (sessionId != nullptr)
	{
		field = nullptr;
		if (json_object_object_get_ex(jsonDesObject, SESSION_ID, &field))
		{
			if (field)
			{
				*sessionId = json_object_get_string(field);
			}
			else
			{
				*sessionId = "";
			}
		}
	}
	if (machineId != nullptr)
	{
		field = nullptr;
		if (json_object_object_get_ex(jsonDesObject, MACHINE_ID, &field))
		{
			if (field)
			{
				*machineId = json_object_get_string(field);
			}
			else
			{
				*machineId = "";
			}
		}
	}
	return true;
}

///----------------------------------------------------------------------
/// SplitCommand
/// Pulls out all call/command fields except payload.
/// Pass in a NULL for any fields you don't care about.
///----------------------------------------------------------------------
bool DesHelper::SplitHeader( json_object* headerObj,
							std::string* cpdFileName,
							std::string* cpdVersion,
							std::string* cpdDescription )
{
	json_object *field = nullptr;
	bool succeeded = true;
	
	/// Extract the parts we want to copy from the header.
	if (cpdFileName != nullptr)
	{
		field = nullptr;
		if (json_object_object_get_ex(headerObj, CPD_FILE_NAME, &field))
		{
			*cpdFileName = json_object_get_string(field);
		}
		else
		{
			succeeded = false;
		}
	}
	if (cpdVersion != nullptr)
	{
		field = nullptr;
		if (json_object_object_get_ex(headerObj, VERSION, &field))
		{
			*cpdVersion = json_object_get_string(field);
		}
		else
		{
			succeeded = false;
		}
	}
	if (cpdDescription != nullptr)
	{
		field = nullptr;
		if (json_object_object_get_ex(headerObj, CPD_DESCRIPTION, &field))
		{
			*cpdDescription = json_object_get_string(field);
		}
		else
		{
			succeeded = false;
		}
	}
	return succeeded;
}

///----------------------------------------------------------------------
/// CheckResult
/// The result parameter should be the return value from a call to
/// json_object_object_get_ex(). See IsValidCommand() for examples
/// of CheckResult() at work.
///----------------------------------------------------------------------
bool DesHelper::CheckResult( int result, const char* fieldName, const char* channelType )
{
	if (result != 0)
	{
		stringstream msg;
		msg << "failed to add '" << fieldName << "' to " << channelType << " call." << endl;
		LOG_ERROR( "unk", "mqtt", msg.str().c_str() );
		return false;
	}
	return true;
}

///----------------------------------------------------------------------
/// ValidateTransportVersion
/// Compares a string (which should be a version number) against the
/// hard-coded value of "transport version".
///----------------------------------------------------------------------
bool DesHelper::ValidateTransportVersion( const char* transportVersion )
{
	if (strcmp( transportVersion, TRANSPORT_VERSION_VALUE) == 0)
	{
		return true;
	}
	else
	{
		cerr << "Received wrong " << TRANSPORT_VERSION << ". Value received is " << transportVersion << endl;
		return false;
	}
}

///----------------------------------------------------------------------
/// IsaMicroscopeCommand
/// Seeks a match between the given message type and all of the message
/// types that are handled by a microscope.
/// Returns true if a match is found.
///----------------------------------------------------------------------
bool DesHelper::IsaMicroscopeCommand(std::string messageType)
{
	bool matchFound = false;
	std::array<std::string, 11> microscopeCommandArray =
	{
		MOVE_TO_FOCUS_STEP,
		MOVE_TO_FOCUS_RELATIVE_STEP,
		GET_CURRENT_FOCUS_STEP,
		MOVE_TO_FOCUS_NM,
		MOVE_TO_FOCUS_RELATIVE_NM,
		GET_CURRENT_FOCUS_NM,
		MOVE_TO_FOCUS_LOW_LIMIT,
		MOVE_TO_FOCUS_HIGH_LIMIT,
		GET_OPTICAL_COLUMN_PROPERTIES,
		SET_OPTICAL_COLUMN_PROPERTIES,
	};
	
	for (const auto& s: microscopeCommandArray)
	{
		matchFound = matchFound || (messageType == s);
	}
	
	return matchFound;
}

///----------------------------------------------------------------------
/// IsAnImmediateCommand
/// Returns true if the given message type is one that requires
/// special handling. For example, HOLD_ALL_OPERATIONS must break the
/// normal flow and all pending work queues to be cleared.
///----------------------------------------------------------------------
bool DesHelper::IsAnImmediateCommand(std::string messageType)
{
	bool matchFound = false;
	/// Caution: include new message types with care. Any addition
	/// will need new, special, handler functions to be added to
	/// the main thread. 
	std::array<std::string, 5> immediateCommandArray =
	{
		SYNCHRONIZE_TIME,
		HOLD_OPERATION_STACK,
		HOLD_ALL_OPERATIONS,
		RESUME_ALL_OPERATIONS,
		KILL_PENDING_OPERATIONS,
	};
	
	for (const auto& s: immediateCommandArray)
	{
		matchFound = matchFound || (messageType == s);
	}
	
	return matchFound;
}

///----------------------------------------------------------------------
/// ComposeSetZToSafeHeightPayload
/// According the DES version 1.01, the safe height is defined by
/// ZAxis's z_safe_height property. So make an object that contains it.
///----------------------------------------------------------------------
json_object* DesHelper::ComposeSetZToSafeHeightPayload( unsigned long z_safeHeight_nm )
{
	json_object * payloadObj = json_object_new_object();
	json_object * valueObj = json_object_new_uint64( z_safeHeight_nm );
	int result = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, valueObj );
	LETS_ASSERT(result == 0);
	return payloadObj;
}

///----------------------------------------------------------------------
/// ComposeSetZZToSafeHeightPayload
/// According the DES version 1.01, the safe height is defined by
/// ZZAxis's zz_safe_height property. So make an object that contains it.
///----------------------------------------------------------------------
json_object* DesHelper::ComposeSetZZToSafeHeightPayload( unsigned long zz_safeHeight_nm )
{
	json_object * payloadObj = json_object_new_object();
	json_object * valueObj = json_object_new_uint64( zz_safeHeight_nm );
	int result = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, valueObj );
	LETS_ASSERT(result == 0);
	return payloadObj;
}

///----------------------------------------------------------------------
/// ComposeSetZZZToSafeHeightPayload
/// According the DES version 1.01, the safe height is defined by
/// ZZZAxis's zzz_safe_height property. So make an object that contains it.
///----------------------------------------------------------------------
json_object* DesHelper::ComposeSetZZZToSafeHeightPayload( unsigned long zzz_safeHeight_nm )
{
	json_object * payloadObj = json_object_new_object();
	json_object * valueObj = json_object_new_uint64( zzz_safeHeight_nm );
	int result = json_object_object_add( payloadObj, ZZZ_SAFE_HEIGHT, valueObj );
	LETS_ASSERT(result == 0);
	return payloadObj;
}

///----------------------------------------------------------------------
/// ComposeMoveToXXStepPayload
///----------------------------------------------------------------------
json_object* DesHelper::ComposeMoveToXXStepPayload( unsigned long xxStep )
{
	return nullptr; // TODO: Make a real implementation.
}

///----------------------------------------------------------------------
/// ComposeMoveToYYStepPayload
///----------------------------------------------------------------------
json_object* DesHelper::ComposeMoveToYYStepPayload( unsigned long yyStep )
{
	return nullptr; // TODO: Make a real implementation.
}

///----------------------------------------------------------------------
/// ComposeMoveToZTipPickForceNewtonPayload
///----------------------------------------------------------------------
//json_object* DesHelper::ComposeMoveToZTipPickForceNewtonPayload( float slowDownOffsetUm )
//{
//	return nullptr; // TODO: Make a real implementation.
//}

///----------------------------------------------------------------------
/// ComposeMoveToZZTipPickForceNewtonPayload
///----------------------------------------------------------------------
//json_object* DesHelper::ComposeMoveToZZTipPickForceNewtonPayload( float slowDownOffsetUm )
//{
//	return nullptr; // TODO: Make a real implementation.
//}

///----------------------------------------------------------------------
/// ComposeSetZAxisSpeedPayload
///----------------------------------------------------------------------
json_object* DesHelper::ComposeSetZAxisSpeedPayload( long zSpeed )
{
	json_object * payload_jobj =  json_object_new_object();
    int result = json_object_object_add( payload_jobj, Z_AXIS_SPEED, json_object_new_int64( zSpeed ) );
	if (result != 0)
	{
		 cerr << "failed to add 'speed' to " << Z_AXIS_SPEED << " call." << endl;
	}
	return payload_jobj;
}

///----------------------------------------------------------------------
/// ComposeUploadCpdError
/// Use this for making an element to later be inserted in the array that
/// becomes the payload of an UploadCPD status message.
///----------------------------------------------------------------------
json_object* DesHelper::ComposeUploadCpdError( const char * cpdId, const char * syntaxError )
{
	json_object * errorObj =  json_object_new_object();
	int result = json_object_object_add( errorObj, CPD_ID, json_object_new_string( cpdId ) );
	LETS_ASSERT(result == 0);
	result = json_object_object_add( errorObj, SYNTAX_ERROR_STRING, json_object_new_string( syntaxError ) );
	LETS_ASSERT(result == 0);
	return errorObj;
}

/// Create a payload full of prog0 input parameters for each of the following axes:
/// Z, ZZ, ZZZ, AspirationPump1, DispensePump1, DispensePump2, DispensePump3, PickingPump1
json_object* DesHelper::ComposeHomeAllSafeHwPayload( const char * threadName,
													 ComponentProperties * propsSH,
													 ComponentProperties * propsZ,
													 ComponentProperties * propsZZ,
													 ComponentProperties * propsZZZ,
													 ComponentProperties * propsAspPump1,
													 ComponentProperties * propsDispPump1,
													 ComponentProperties * propsDispPump2,
													 ComponentProperties * propsDispPump3,
													 ComponentProperties * propsPickPump1 )
{
	bool succeeded = true;
	int result = 0;
	json_object * payloadOutObj = json_object_new_object();

	/// SH
	succeeded = true;
	json_object * axisObj = json_object_new_object();
	FieldWithMetaData property = propsSH->GetFieldByName( HOME_LOCATION );
	result = json_object_object_add( axisObj, HOME_OFFSET, json_object_new_int64( property.GetValueAsLong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, CLEAR_KILLS_TIMEOUT, json_object_new_int64( DEFAULT_CLEAR_KILLS_TIMEOUT ) );
	succeeded = succeeded && (result == 0);
	property = propsSH->GetFieldByName( HOMING_ACCELERATION );
	result = json_object_object_add( axisObj, COORD_MOVE_ACCEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsSH->GetFieldByName( HOMING_DECELERATION ); /// We'll use this for both CoordMovDecel and CoordMovSTP
	result = json_object_object_add( axisObj, COORD_MOVE_DECEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, COORD_MOVE_STP, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsSH->GetFieldByName( HOMING_SPEED ); /// We'll use this for both CoordMovVel and HomeFinalVal
	result = json_object_object_add( axisObj, COORD_MOVE_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, HOME_FINAL_VEL, json_object_new_int64( property.GetValueAsULong()) );
	succeeded = succeeded && (result == 0);
	property = propsSH->GetFieldByName( "ar_jogging_acceleration" );
	result = json_object_object_add( axisObj, JOG_ACCEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsSH->GetFieldByName( "ar_jogging_deceleration" );
	result = json_object_object_add( axisObj, JOG_DECEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsSH->GetFieldByName( "ar_jogging_speed" );
	result = json_object_object_add( axisObj, JOG_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, JOG_HLDEC, json_object_new_int64( DEFAULT_JOG_HLDEC ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( payloadOutObj, HOME_SH_INPUTS, axisObj );
	succeeded = succeeded && (result == 0);
	if (!succeeded)
	{
		LOG_ERROR( threadName, "SH", "failed to create HOME_SH inputs" );
	}

	/// Z
	succeeded = true;
	axisObj = json_object_new_object();
	property = propsZ->GetFieldByName( HOME_LOCATION );
	result = json_object_object_add( axisObj, HOME_OFFSET, json_object_new_int64( property.GetValueAsLong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, CLEAR_KILLS_TIMEOUT, json_object_new_int64( DEFAULT_CLEAR_KILLS_TIMEOUT ) );
	succeeded = succeeded && (result == 0);
	property = propsZ->GetFieldByName( HOMING_ACCELERATION );
	result = json_object_object_add( axisObj, COORD_MOVE_ACCEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsZ->GetFieldByName( HOMING_DECELERATION ); /// We'll use this for both CoordMovDecel and CoordMovSTP
	result = json_object_object_add( axisObj, COORD_MOVE_DECEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, COORD_MOVE_STP, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsZ->GetFieldByName( HOMING_SPEED ); /// We'll use this for both CoordMovVel and HomeFinalVal
	result = json_object_object_add( axisObj, COORD_MOVE_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, HOME_FINAL_VEL, json_object_new_int64( property.GetValueAsULong()) );
	succeeded = succeeded && (result == 0);
	property = propsZ->GetFieldByName( "z_jogging_acceleration" );
	result = json_object_object_add( axisObj, JOG_ACCEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsZ->GetFieldByName( "z_jogging_deceleration" );
	result = json_object_object_add( axisObj, JOG_DECEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsZ->GetFieldByName( "z_jogging_speed" );
	result = json_object_object_add( axisObj, JOG_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, JOG_HLDEC, json_object_new_int64( DEFAULT_JOG_HLDEC ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( payloadOutObj, HOME_Z_INPUTS, axisObj );
	succeeded = succeeded && (result == 0);
	if (!succeeded)
	{
		LOG_ERROR( threadName, "Z", "failed to create HOME_Z inputs" );
	}

	/// ZZ
	succeeded = true;
	axisObj = nullptr;
	axisObj = json_object_new_object();
	property = propsZZ->GetFieldByName( HOME_LOCATION );
	result = json_object_object_add( axisObj, HOME_OFFSET, json_object_new_int64( property.GetValueAsLong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, CLEAR_KILLS_TIMEOUT, json_object_new_int64( DEFAULT_CLEAR_KILLS_TIMEOUT ) );
	succeeded = succeeded && (result == 0);
	property = propsZZ->GetFieldByName( HOMING_ACCELERATION );
	result = json_object_object_add( axisObj, COORD_MOVE_ACCEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsZZ->GetFieldByName( HOMING_DECELERATION ); /// We'll use this for both CoordMovDecel and CoordMovSTP
	result = json_object_object_add( axisObj, COORD_MOVE_DECEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, COORD_MOVE_STP, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsZZ->GetFieldByName( HOMING_SPEED ); /// We'll use this for both CoordMovVel and HomeFinalVal
	result = json_object_object_add( axisObj, COORD_MOVE_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, HOME_FINAL_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsZZ->GetFieldByName( "zz_jogging_acceleration" );
	result = json_object_object_add( axisObj, JOG_ACCEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsZZ->GetFieldByName( "zz_jogging_deceleration" );
	result = json_object_object_add( axisObj, JOG_DECEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsZZ->GetFieldByName( "zz_jogging_speed" );
	result = json_object_object_add( axisObj, JOG_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, JOG_HLDEC, json_object_new_int64( DEFAULT_JOG_HLDEC ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( payloadOutObj, HOME_ZZ_INPUTS, axisObj );
	succeeded = succeeded && (result == 0);
	if (!succeeded)
	{
		LOG_ERROR( threadName, "ZZ", "failed to create HOME_ZZ inputs" );
	}

	/// ZZZ
	succeeded = true;
	axisObj = nullptr;
	axisObj = json_object_new_object();
	property = propsZZZ->GetFieldByName( HOME_LOCATION );
	result = json_object_object_add( axisObj, HOME_OFFSET, json_object_new_int64( property.GetValueAsLong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, CLEAR_KILLS_TIMEOUT, json_object_new_int64( DEFAULT_CLEAR_KILLS_TIMEOUT ) );
	succeeded = succeeded && (result == 0);
	property = propsZZZ->GetFieldByName( HOMING_ACCELERATION );
	result = json_object_object_add( axisObj, COORD_MOVE_ACCEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsZZZ->GetFieldByName( HOMING_DECELERATION ); /// We'll use this for both CoordMovDecel and CoordMovSTP
	result = json_object_object_add( axisObj, COORD_MOVE_DECEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, COORD_MOVE_STP, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsZZZ->GetFieldByName( HOMING_SPEED ); /// We'll use this for both CoordMovVel and HomeFinalVal
	result = json_object_object_add( axisObj, COORD_MOVE_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, HOME_FINAL_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsZZZ->GetFieldByName( "zzz_jogging_acceleration" );
	result = json_object_object_add( axisObj, JOG_ACCEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsZZZ->GetFieldByName( "zzz_jogging_deceleration" );
	result = json_object_object_add( axisObj, JOG_DECEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsZZZ->GetFieldByName( "zzz_jogging_speed" );
	result = json_object_object_add( axisObj, JOG_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, JOG_HLDEC, json_object_new_int64( DEFAULT_JOG_HLDEC ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( payloadOutObj, HOME_ZZZ_INPUTS, axisObj );
	succeeded = succeeded && (result == 0);
	if (!succeeded)
	{
		LOG_ERROR( threadName, "ZZZ", "failed to create HOME_ZZZ inputs" );
	}
	
	/// PICK_PUMP1
	succeeded = true;
	axisObj = nullptr;
	axisObj = json_object_new_object();
	property = propsPickPump1->GetFieldByName( PUMP_VENDOR );
	result = json_object_object_add( axisObj, PUMP_VENDOR, json_object_new_string( property.setting.c_str() ) );
	succeeded = succeeded && (result == 0);
	property = propsPickPump1->GetFieldByName( PUMP_MODEL );
	result = json_object_object_add( axisObj, PUMP_MODEL, json_object_new_string( property.setting.c_str() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( payloadOutObj, HOME_PICK_PUMP1_INPUTS, axisObj );
	succeeded = succeeded && (result == 0);
	if (!succeeded)
	{
		LOG_ERROR( threadName, "PickingPump1", "failed to create HOME_PICK_PUMP1 inputs" );
	}
	// TODO: Add data for the aspirate and dispense pumps, if needed.

	return payloadOutObj;
}
/// Create a payload full of prog0 input parameters for each of the following axes:
/// Y, X, YY, XX
json_object* DesHelper::ComposeHomeAllUnsafeHwPayload( const char * threadName,
													   ComponentProperties * propsXY,
													   ComponentProperties * propsYY,
													   ComponentProperties * propsXX )
{
	bool succeeded = true;
	int result = 0;
	json_object * payloadOutObj = json_object_new_object();

	/// Y
	succeeded = true;
	json_object * axisObj = json_object_new_object();
	FieldWithMetaData property = propsXY->GetFieldByName( "y_home_location" );
	result = json_object_object_add( axisObj, HOME_OFFSET, json_object_new_int64( property.GetValueAsLong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, CLEAR_KILLS_TIMEOUT, json_object_new_int64( DEFAULT_CLEAR_KILLS_TIMEOUT ) );
	succeeded = succeeded && (result == 0);
	property = propsXY->GetFieldByName( "y_homing_acceleration" );
	result = json_object_object_add( axisObj, COORD_MOVE_ACCEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXY->GetFieldByName( "y_homing_deceleration" ); /// We'll use this for both CoordMovDecel and CoordMovSTP
	result = json_object_object_add( axisObj, COORD_MOVE_DECEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, COORD_MOVE_STP, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXY->GetFieldByName( "y_homing_speed" );
	result = json_object_object_add( axisObj, COORD_MOVE_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXY->GetFieldByName( "y_find_tip_speed" ); // TODO: Confirm that this is OK to us for HomeFinalVel
	result = json_object_object_add( axisObj, HOME_FINAL_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXY->GetFieldByName( "y_jogging_acceleration" );
	result = json_object_object_add( axisObj, JOG_ACCEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXY->GetFieldByName( "y_jogging_deceleration" );
	result = json_object_object_add( axisObj, JOG_DECEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXY->GetFieldByName( "y_jogging_speed" );
	result = json_object_object_add( axisObj, JOG_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, JOG_HLDEC, json_object_new_int64( DEFAULT_JOG_HLDEC ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( payloadOutObj, HOME_Y_INPUTS, axisObj );
	succeeded = succeeded && (result == 0);
	if (!succeeded)
	{
		LOG_ERROR( threadName, "Y", "failed to create HOME_Y inputs" );
	}
	
	/// X
	succeeded = true;
	axisObj = nullptr;
	axisObj = json_object_new_object();
	property = propsXY->GetFieldByName( "x_home_location" );
	result = json_object_object_add( axisObj, HOME_OFFSET, json_object_new_int64( property.GetValueAsLong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, CLEAR_KILLS_TIMEOUT, json_object_new_int64( DEFAULT_CLEAR_KILLS_TIMEOUT ) );
	succeeded = succeeded && (result == 0);
	property = propsXY->GetFieldByName( "x_homing_acceleration" );
	result = json_object_object_add( axisObj, COORD_MOVE_ACCEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXY->GetFieldByName( "x_homing_deceleration" ); /// We'll use this for both CoordMovDecel and CoordMovSTP
	result = json_object_object_add( axisObj, COORD_MOVE_DECEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, COORD_MOVE_STP, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXY->GetFieldByName( "x_homing_speed" );
	result = json_object_object_add( axisObj, COORD_MOVE_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXY->GetFieldByName( "x_find_tip_speed" ); // TODO: Confirm that this is OK to us for HomeFinalVel
	result = json_object_object_add( axisObj, HOME_FINAL_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXY->GetFieldByName( "x_jogging_acceleration" );
	result = json_object_object_add( axisObj, JOG_ACCEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXY->GetFieldByName( "x_jogging_deceleration" );
	result = json_object_object_add( axisObj, JOG_DECEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXY->GetFieldByName( "x_jogging_speed" );
	result = json_object_object_add( axisObj, JOG_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, JOG_HLDEC, json_object_new_int64( DEFAULT_JOG_HLDEC ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( payloadOutObj, HOME_X_INPUTS, axisObj );
	succeeded = succeeded && (result == 0);
	if (!succeeded)
	{
		LOG_ERROR( threadName, "X", "failed to create HOME_X inputs" );
	}
	
	/// YY
	succeeded = true;
	axisObj = nullptr;
	axisObj = json_object_new_object();
	property = propsYY->GetFieldByName( "home_location" );
	result = json_object_object_add( axisObj, HOME_OFFSET, json_object_new_int64( property.GetValueAsLong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, CLEAR_KILLS_TIMEOUT, json_object_new_int64( DEFAULT_CLEAR_KILLS_TIMEOUT ) );
	succeeded = succeeded && (result == 0);
	property = propsYY->GetFieldByName( "homing_acceleration" );
	result = json_object_object_add( axisObj, COORD_MOVE_ACCEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsYY->GetFieldByName( "homing_deceleration" ); /// We'll use this for both CoordMovDecel and CoordMovSTP
	result = json_object_object_add( axisObj, COORD_MOVE_DECEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, COORD_MOVE_STP, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsYY->GetFieldByName( "homing_speed" );
	result = json_object_object_add( axisObj, COORD_MOVE_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsYY->GetFieldByName( "homing_speed" ); // TODO: Confirm that this is OK to us for HomeFinalVel
	result = json_object_object_add( axisObj, HOME_FINAL_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsYY->GetFieldByName( "yy_jogging_acceleration" );
	result = json_object_object_add( axisObj, JOG_ACCEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsYY->GetFieldByName( "yy_jogging_deceleration" );
	result = json_object_object_add( axisObj, JOG_DECEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsYY->GetFieldByName( "yy_jogging_speed" );
	result = json_object_object_add( axisObj, JOG_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, JOG_HLDEC, json_object_new_int64( DEFAULT_JOG_HLDEC ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( payloadOutObj, HOME_YY_INPUTS, axisObj );
	succeeded = succeeded && (result == 0);
	if (!succeeded)
	{
		LOG_ERROR( threadName, "YY", "failed to create HOME_YY inputs" );
	}
	
	/// XX
	succeeded = true;
	axisObj = nullptr;
	axisObj = json_object_new_object();
	property = propsXX->GetFieldByName( "home_location" );
	result = json_object_object_add( axisObj, HOME_OFFSET, json_object_new_int64( property.GetValueAsLong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, CLEAR_KILLS_TIMEOUT, json_object_new_int64( DEFAULT_CLEAR_KILLS_TIMEOUT ) );
	succeeded = succeeded && (result == 0);
	property = propsXX->GetFieldByName( "homing_acceleration" );
	result = json_object_object_add( axisObj, COORD_MOVE_ACCEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXX->GetFieldByName( "homing_deceleration" ); /// We'll use this for both CoordMovDecel and CoordMovSTP
	result = json_object_object_add( axisObj, COORD_MOVE_DECEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, COORD_MOVE_STP, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXX->GetFieldByName( "homing_speed" );
	result = json_object_object_add( axisObj, COORD_MOVE_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXX->GetFieldByName( "homing_speed" ); // TODO: Confirm that this is OK to us for HomeFinalVel
	result = json_object_object_add( axisObj, HOME_FINAL_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXX->GetFieldByName( "xx_jogging_acceleration" );
	result = json_object_object_add( axisObj, JOG_ACCEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXX->GetFieldByName( "xx_jogging_deceleration" );
	result = json_object_object_add( axisObj, JOG_DECEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	property = propsXX->GetFieldByName( "xx_jogging_speed" );
	result = json_object_object_add( axisObj, JOG_VEL, json_object_new_int64( property.GetValueAsULong() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( axisObj, JOG_HLDEC, json_object_new_int64( DEFAULT_JOG_HLDEC ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( payloadOutObj, HOME_XX_INPUTS, axisObj );
	succeeded = succeeded && (result == 0);
	if (!succeeded)
	{
		LOG_ERROR( threadName, "XX", "failed to create HOME_XX inputs" );
	}

	return payloadOutObj;
}

bool DesHelper::ExtractHomeInputsFromPayload( json_object * payloadObj_in,
											  const char * homeInputsName_in,
											  unsigned long & homeOffset_out,
											  unsigned long & clearKillsTimeout_out,
											  unsigned long & coordMoveAccel_out,
											  unsigned long & coordMoveDecel_out,
											  unsigned long & coordMoveStp_out,
											  unsigned long & coordMoveVel_out,
											  unsigned long & homeFinalVel_out,
											  unsigned long & jogAccel_out,
											  unsigned long & jogDecel_out,
											  unsigned long & jogVel_out,
											  unsigned long & jogHLDC_out,
											  std::string & report )
{
	json_object * homeInputsObj = json_object_object_get( payloadObj_in, homeInputsName_in );
	if (homeInputsObj)
	{
		homeOffset_out = json_object_get_uint64( json_object_object_get( homeInputsObj, HOME_OFFSET ) );
		clearKillsTimeout_out = json_object_get_uint64( json_object_object_get( homeInputsObj, CLEAR_KILLS_TIMEOUT ) );
		coordMoveAccel_out = json_object_get_uint64( json_object_object_get( homeInputsObj, COORD_MOVE_ACCEL ) );
		coordMoveDecel_out = json_object_get_uint64( json_object_object_get( homeInputsObj, COORD_MOVE_DECEL ) );
		coordMoveStp_out = json_object_get_uint64( json_object_object_get( homeInputsObj, COORD_MOVE_STP ) );
		coordMoveVel_out = json_object_get_uint64( json_object_object_get( homeInputsObj, COORD_MOVE_VEL ) );
		homeFinalVel_out = json_object_get_uint64( json_object_object_get( homeInputsObj, HOME_FINAL_VEL ) );
		jogAccel_out = json_object_get_uint64( json_object_object_get( homeInputsObj, JOG_ACCEL ) );
		jogDecel_out = json_object_get_uint64( json_object_object_get( homeInputsObj, JOG_DECEL ) );
		jogVel_out = json_object_get_uint64( json_object_object_get( homeInputsObj, JOG_VEL ) );
		jogHLDC_out = json_object_get_uint64( json_object_object_get( homeInputsObj, JOG_HLDEC ) );
		stringstream summary;
		summary << "HomeOffset=" << homeOffset_out << "|ClrKillsTimeOut=" << clearKillsTimeout_out;
		summary << "|CoordMovAccel=" << coordMoveAccel_out << "|CoordMovDecel=" << coordMoveDecel_out;
		summary << "|CoordMovVel=" << coordMoveVel_out << "|CoordMovSTP=" << coordMoveStp_out;
		summary << "|HomeFinalVel=" << homeFinalVel_out << "|JogAccel=" << jogAccel_out;
		summary << "|JogDecel" << jogDecel_out << "|JogVel" << jogVel_out << "|JogHLDEC=" << jogHLDC_out;
		report = summary.str();
		return true;
	}
	return false;
}


void DesHelper::AddCpdError( json_object** errorList, const char * cpdId, const char * syntaxErrorDescription )
{
	if (*errorList == nullptr)
	{
//		cerr << "Creating array object" << endl;
		*errorList = json_object_new_array();
		if (*errorList == nullptr) { cerr << "Nuts. It didn't work" << endl; }
	}
//	cerr << "AddCpdError adding: " << syntaxErrorDescription << endl;
	int result = json_object_array_add( *errorList, DesHelper::ComposeUploadCpdError( cpdId,  syntaxErrorDescription ) );
//	cerr << "result of add is " << result << endl;
	LETS_ASSERT(result == 0);
}

/// Add the following component properties to payloadObj: set_speed (as VELOCITY), acceleration, and deceleration
/// (as both DECELERATION and STP). Print an error if the X and Y versions of any of those do not match each other.
bool DesHelper::AddXYSpeeds( const char * threadName, json_object * payloadObj_in_out, ComponentProperties * propsXY )
{
	bool succeeded = true;
	if (payloadObj_in_out)
	{
		/// Make a json object for grouping speeds for one axis
		json_object * groupObj = json_object_new_object();
		if (groupObj)
		{
			/// Get needed property values and add them to the payload.
			unsigned long x_set_speed = propsXY->GetFieldByName( X_SET_SPEED ).GetValueAsULong();
			if (x_set_speed != propsXY->GetFieldByName( Y_SET_SPEED ).GetValueAsULong())
			{
				LOG_WARN( threadName, "xy", "X and Y speeds don't match. Defaulting to X value" );
			}
			unsigned long x_acceleration = propsXY->GetFieldByName( X_ACCELERATION ).GetValueAsULong();
			if (x_acceleration != propsXY->GetFieldByName( Y_ACCELERATION ).GetValueAsULong())
			{
				LOG_WARN( threadName, "xy", "X and Y accelerations don't match. Defaulting to X value" );
			}
			unsigned long x_deceleration = propsXY->GetFieldByName( X_DECELERATION ).GetValueAsULong();
			if (x_deceleration != propsXY->GetFieldByName( Y_DECELERATION ).GetValueAsULong())
			{
				LOG_WARN( threadName, "xy", "X and Y decelerations don't match. Defaulting to X value" );
			}
			unsigned long x_axis_resolution = propsXY->GetFieldByName( X_AXIS_RESOLUTION ).GetValueAsULong();
			if (x_axis_resolution != propsXY->GetFieldByName( Y_AXIS_RESOLUTION ).GetValueAsULong())
			{
				LOG_WARN( threadName, "xy", "X and Y axis_resolution don't match. Defaulting to X value" );
			}
			long x_soft_travel_limit_position_plus = propsXY->GetFieldByName( X_SOFT_TRAVEL_LIMIT_POSITION_PLUS ).GetValueAsLong();
			long y_soft_travel_limit_position_plus = propsXY->GetFieldByName( Y_SOFT_TRAVEL_LIMIT_POSITION_PLUS ).GetValueAsLong();

			long x_soft_travel_limit_position_minus = propsXY->GetFieldByName( X_SOFT_TRAVEL_LIMIT_POSITION_MINUS ).GetValueAsLong();
			long y_soft_travel_limit_position_minus = propsXY->GetFieldByName( Y_SOFT_TRAVEL_LIMIT_POSITION_MINUS ).GetValueAsLong();

			int rc = json_object_object_add( groupObj, VELOCITY, json_object_new_int64( x_set_speed ) );
			succeeded &= (rc == 0);
			rc = json_object_object_add( groupObj, ACCELERATION, json_object_new_int64( x_acceleration ) );
			succeeded &= (rc == 0);
			rc = json_object_object_add( groupObj, DECELERATION, json_object_new_int64( x_deceleration ) );
			succeeded &= (rc == 0);
			rc = json_object_object_add( groupObj, STP, json_object_new_int64( x_deceleration ) );
			succeeded &= (rc == 0);
			rc = json_object_object_add( groupObj, AXIS_RESOLUTION, json_object_new_int64( x_axis_resolution ) );
			succeeded &= (rc == 0);
			/// Unlike speeds, the soft travel limits are not expected to be the same for X and Y. Pack them both.
			// TODO: Since this change will necessitate the creation of a new ExtractXYSpeeds() function, consider packing
			// the speeds for both X and Y too.
			rc = json_object_object_add( groupObj, X_SOFT_TRAVEL_LIMIT_POSITION_PLUS, json_object_new_int64( x_soft_travel_limit_position_plus ) );
			succeeded &= (rc == 0);
			rc = json_object_object_add( groupObj, Y_SOFT_TRAVEL_LIMIT_POSITION_PLUS, json_object_new_int64( y_soft_travel_limit_position_plus ) );
			succeeded &= (rc == 0);
			rc = json_object_object_add( groupObj, X_SOFT_TRAVEL_LIMIT_POSITION_MINUS, json_object_new_int64( x_soft_travel_limit_position_minus ) );
			succeeded &= (rc == 0);
			rc = json_object_object_add( groupObj, Y_SOFT_TRAVEL_LIMIT_POSITION_MINUS, json_object_new_int64( y_soft_travel_limit_position_minus ) );
			succeeded &= (rc == 0);

			/// Create a name for the group and add it to the payload.
			rc = json_object_object_add( payloadObj_in_out, XY_SPEEDS, groupObj );
			succeeded &= (rc == 0);
		}
		else
		{
			succeeded = false;
		}
	}
	else
	{
		succeeded = false;
	}
	return succeeded;
}

/// Add montage speeds to the payload. This should be called in addition to AddXYSpeeds() because the montage function
/// uses XYSpeeds for moving to the start position, then uses montage speeds thereafter.
bool DesHelper::AddMontageSpeeds( const char * threadName,
								  json_object * payloadObj_in_out,
								  ComponentProperties * propsXY,
								  ComponentProperties * propsCxD,
								  SpeedType speed )
{
	bool succeeded = true;
	if (payloadObj_in_out)
	{
		/// Make a json object for grouping speeds for one axis
		json_object * groupObj = json_object_new_object();
		if (groupObj)
		{
			/// Get needed property values and add them to the payload.
			unsigned long x_speed = 0L;
			unsigned long x_acceleration = 0L;
			unsigned long x_deceleration = 0L;
			unsigned long movement_delay_ms = 0L;
			switch (speed)
			{
				case SpeedType::Speed_medium :
					x_speed = propsXY->GetFieldByName( X_MONTAGE_SPEED_MEDIUM ).GetValueAsULong();
					if (x_speed != propsXY->GetFieldByName( Y_MONTAGE_SPEED_MEDIUM ).GetValueAsULong())
					{
						LOG_WARN( threadName, "xy", "X and Y speeds don't match. Defaulting to X value" );
					}
					x_acceleration = propsXY->GetFieldByName( X_MONTAGE_ACCELERATION_MEDIUM ).GetValueAsULong();
					if (x_acceleration != propsXY->GetFieldByName( Y_MONTAGE_ACCELERATION_MEDIUM ).GetValueAsULong())
					{
						LOG_WARN( threadName, "xy", "X and Y accelerations don't match. Defaulting to X value" );
					}
					x_deceleration = propsXY->GetFieldByName( X_MONTAGE_DECELERATION_MEDIUM ).GetValueAsULong();
					if (x_deceleration != propsXY->GetFieldByName( Y_MONTAGE_DECELERATION_MEDIUM ).GetValueAsULong())
					{
						LOG_WARN( threadName, "xy", "X and Y decelerations don't match. Defaulting to X value" );
					}
					movement_delay_ms = propsCxD->GetFieldByName( SCAN_MOVEMENT_DELAY_MEDIUM ).GetValueAsULong();
					break;
				case SpeedType::Speed_fast :
					x_speed = propsXY->GetFieldByName( X_MONTAGE_SPEED_FAST ).GetValueAsULong();
					if (x_speed != propsXY->GetFieldByName( Y_MONTAGE_SPEED_FAST ).GetValueAsULong())
					{
						LOG_WARN( threadName, "xy", "X and Y speeds don't match. Defaulting to X value" );
					}
					x_acceleration = propsXY->GetFieldByName( X_MONTAGE_ACCELERATION_FAST ).GetValueAsULong();
					if (x_acceleration != propsXY->GetFieldByName( Y_MONTAGE_ACCELERATION_FAST ).GetValueAsULong())
					{
						LOG_WARN( threadName, "xy", "X and Y accelerations don't match. Defaulting to X value" );
					}
					x_deceleration = propsXY->GetFieldByName( X_MONTAGE_DECELERATION_FAST ).GetValueAsULong();
					if (x_deceleration != propsXY->GetFieldByName( Y_MONTAGE_DECELERATION_FAST ).GetValueAsULong())
					{
						LOG_WARN( threadName, "xy", "X and Y decelerations don't match. Defaulting to X value" );
					}
					movement_delay_ms = propsCxD->GetFieldByName( SCAN_MOVEMENT_DELAY_FAST ).GetValueAsULong();
					break;
				case SpeedType::Speed_undefined :
					LOG_WARN( threadName, "xy", "Undefined speed. Using basic montage speeds and accelerations." );
				case SpeedType::Speed_slow :
				default:
					x_speed = propsXY->GetFieldByName( X_MONTAGE_SPEED_SLOW ).GetValueAsULong();
					if (x_speed != propsXY->GetFieldByName( Y_MONTAGE_SPEED_SLOW ).GetValueAsULong())
					{
						LOG_WARN( threadName, "xy", "X and Y speeds don't match. Defaulting to X value" );
					}
					x_acceleration = propsXY->GetFieldByName( X_MONTAGE_ACCELERATION_SLOW ).GetValueAsULong();
					if (x_acceleration != propsXY->GetFieldByName( Y_MONTAGE_ACCELERATION_SLOW ).GetValueAsULong())
					{
						LOG_WARN( threadName, "xy", "X and Y accelerations don't match. Defaulting to X value" );
					}
					x_deceleration = propsXY->GetFieldByName( X_MONTAGE_DECELERATION_SLOW ).GetValueAsULong();
					if (x_deceleration != propsXY->GetFieldByName( Y_MONTAGE_DECELERATION_SLOW ).GetValueAsULong())
					{
						LOG_WARN( threadName, "xy", "X and Y decelerations don't match. Defaulting to X value" );
					}
					movement_delay_ms = propsCxD->GetFieldByName( SCAN_MOVEMENT_DELAY_SLOW ).GetValueAsULong();
			}

			int rc = json_object_object_add( groupObj, VELOCITY, json_object_new_int64( x_speed ) );
			succeeded &= (rc == 0);
			rc = json_object_object_add( groupObj, ACCELERATION, json_object_new_int64( x_acceleration ) );
			succeeded &= (rc == 0);
			rc = json_object_object_add( groupObj, DECELERATION, json_object_new_int64( x_deceleration ) );
			succeeded &= (rc == 0);
			rc = json_object_object_add( groupObj, STP, json_object_new_int64( x_deceleration ) );
			succeeded &= (rc == 0);

			/// Create a name for the group and add it to the payload.
			rc = json_object_object_add( payloadObj_in_out, MONTAGE_SPEEDS, groupObj );
			succeeded &= (rc == 0);

			/// Overwrite or add SCAN_MOVEMENT_DELAY_MS to the payload object.
			json_object * scanMovementDelayObj = json_object_object_get( payloadObj_in_out, SCAN_MOVEMENT_DELAY_MS );
			if (scanMovementDelayObj)
			{
				json_object_object_del( payloadObj_in_out, SCAN_MOVEMENT_DELAY_MS );
				rc = json_object_object_add( payloadObj_in_out, SCAN_MOVEMENT_DELAY_MS, json_object_new_int64( movement_delay_ms ) );
				succeeded &= (rc == 0);
				stringstream msg;
				msg << "Overwriting movement_delay_ms with " << movement_delay_ms;
				LOG_TRACE( threadName, "xy", msg.str().c_str() );
			}
			else
			{
				rc = json_object_object_add( payloadObj_in_out, SCAN_MOVEMENT_DELAY_MS, json_object_new_int64( movement_delay_ms ) );
				succeeded &= (rc == 0);
				stringstream msg;
				msg << "Adding movement_delay_ms with " << movement_delay_ms;
				LOG_TRACE( threadName, "xy", msg.str().c_str() );
			}

		}
		else
		{
			succeeded = false;
		}
	}
	else
	{
		succeeded = false;
	}
	return succeeded;
}

/// Add the following component properties to payloadObj: set_speed (as VELOCITY), acceleration, and deceleration
/// (as both DECELERATION and STP).
/// Change: (2023-01-17) adding soft limits to the velocities because they are needed everywhere the velocities are
/// used.
/// In addition, for the Z and ZZ axes, add retraction speeds in case this is a retraction move. (Let the Doler
/// decide which speeds to actually use.)
bool DesHelper::AddSpeedsAndLimits( const char * threadName, json_object * payloadObj_in_out, const char * axisName, ComponentProperties * props )
{
	bool succeeded = true;
	if (payloadObj_in_out)
	{
		/// Make a json object for grouping speeds for one axis
		json_object * groupObj = json_object_new_object();
		if (groupObj)
		{
			/// Get needed property values and add them to the group.
			string name = axisName;
			name += "_set_speed";
			unsigned long value = props->GetFieldByName( name ).GetValueAsULong();
			// TODO: One of these may be redundant.
			int rc = json_object_object_add( groupObj, VELOCITY, json_object_new_int64( value ) );
			succeeded &= (rc == 0);
			/// While all the standard speeds and accelerations go into a named groupObj, any command that needs
			/// a *_set_speed will expect it to find it in the payload object.
			rc = json_object_object_add( payloadObj_in_out, name.c_str(), json_object_new_uint64( value ) );
			succeeded &= (rc == 0);

			name = axisName;
			name += "_acceleration";
			value = props->GetFieldByName( name ).GetValueAsULong();
			rc = json_object_object_add( groupObj, ACCELERATION, json_object_new_int64( value ) );
			succeeded &= (rc == 0);

			name = axisName;
			name += "_deceleration";
			value = props->GetFieldByName( name ).GetValueAsULong();
			rc = json_object_object_add( groupObj, DECELERATION, json_object_new_int64( value ) );
			succeeded &= (rc == 0);
			rc = json_object_object_add( groupObj, STP, json_object_new_int64( value ) );
			succeeded &= (rc == 0);

			name = axisName;
			name += "_axis_resolution";
			value = props->GetFieldByName( name ).GetValueAsULong();
			rc = json_object_object_add( groupObj, AXIS_RESOLUTION, json_object_new_int64( value ) );
			succeeded &= (rc == 0);
			
			name = SOFT_TRAVEL_LIMIT_POSITION_PLUS;
			long limit = props->GetFieldByName( name ).GetValueAsLong();
			rc = json_object_object_add( groupObj, SOFT_TRAVEL_LIMIT_POSITION_PLUS, json_object_new_int64( limit ) );
			succeeded &= (rc == 0);

			name = SOFT_TRAVEL_LIMIT_POSITION_MINUS;
			limit = props->GetFieldByName( name ).GetValueAsLong();
			rc = json_object_object_add( groupObj, SOFT_TRAVEL_LIMIT_POSITION_MINUS, json_object_new_int64( limit ) );
			succeeded &= (rc == 0);

			/// Create a name for the group and add it to the payload.
			string groupName = axisName;
			groupName += "_speeds";
			rc = json_object_object_add( payloadObj_in_out, groupName.c_str(), groupObj );
			succeeded &= (rc == 0);
		}
		else
		{
			LOG_ERROR( threadName, "speeds", "FAILED to create group object" );
			succeeded = false;
		}
	}
	else
	{
		succeeded = false;
	}
	return succeeded;
}

/// Replace speed, acceleration, and deceleration values in the payload's <axis>_speeds group with retraction variants from
/// component properties. This must be called *AFTER* AddSpeedsAndLimits() is called, to ensure that the <axis>_speeds group exists.
/// Note that only Z and ZZ axes have retraction speeds.
bool DesHelper::ReplaceWithRetractionSpeeds( const char * threadName, json_object * payloadObj_in_out, const char * axisName, ComponentProperties * props )
{
	bool succeeded = true;
	if (payloadObj_in_out)
	{
		/// Find the <axis>_speeds object.
		string groupName = axisName;
		groupName += "_speeds";
		json_object * groupObj = json_object_object_get( payloadObj_in_out, groupName.c_str() );
		if (groupObj)
		{
			/// Get needed property values and add them to the group (replacing the current values).
			string name = axisName;
			name += "_retraction_speed";
			unsigned long value = props->GetFieldByName( name ).GetValueAsULong();
			json_object * valueObj = json_object_object_get( groupObj, VELOCITY );
			if (valueObj)
			{
				int rc = json_object_set_int64( valueObj, value );
				succeeded &= (rc == 0);
			}
			else
			{
				succeeded = false;
			}

			name = axisName;
			name += "_retraction_acceleration";
			value = props->GetFieldByName( name ).GetValueAsULong();
			valueObj = json_object_object_get( groupObj, ACCELERATION );
			if (valueObj)
			{
				int rc = json_object_set_int64( valueObj, value );
				succeeded &= (rc == 0);
			}
			else
			{
				succeeded = false;
			}

			name = axisName;
			name += "_retraction_deceleration";
			value = props->GetFieldByName( name ).GetValueAsULong();
			valueObj = json_object_object_get( groupObj, DECELERATION );
			if (valueObj)
			{
				int rc = json_object_set_int64( valueObj, value );
				succeeded &= (rc == 0);
			}
			else
			{
				succeeded = false;
			}
		}
		else
		{
			LOG_ERROR( threadName, "speeds", "FAILED to create group object" );
			succeeded = false;
		}
	}
	else
	{
		succeeded = false;
	}
	return succeeded;
}

bool DesHelper::AddVendorModel( const char * threadName, json_object * payloadObj_in_out, ComponentType type, ComponentProperties * props )
{
	bool succeeded = true;
	json_object * axisObj = nullptr;
	string vendorStr = "";
	string modelStr = "";
	string objectStr = "";
	string axisNameStr = "";
	switch( type )
	{
		case ComponentType::XYAxes :
			vendorStr = CONTROLLER_VENDOR;
			modelStr = CONTROLLER_MODEL;
			objectStr = HOME_Y_INPUTS;		// TODO: Think what to do with this oddball.
			axisNameStr = "xy";
			break;
		case ComponentType::XXAxis :
			vendorStr = CONTROLLER_VENDOR;
			modelStr = CONTROLLER_MODEL;
			objectStr = HOME_XX_INPUTS;
			axisNameStr = "xx";
			break;
		case ComponentType::YYAxis :
			vendorStr = CONTROLLER_VENDOR;
			modelStr = CONTROLLER_MODEL;
			objectStr = HOME_YY_INPUTS;
			axisNameStr = "yy";
			break;
		case ComponentType::ZAxis :
			vendorStr = CONTROLLER_VENDOR;
			modelStr = CONTROLLER_MODEL;
			objectStr = HOME_Z_INPUTS;
			axisNameStr = "z";
			break;
		case ComponentType::ZZAxis :
			vendorStr = CONTROLLER_VENDOR;
			modelStr = CONTROLLER_MODEL;
			objectStr = HOME_ZZ_INPUTS;
			axisNameStr = "zz";
			break;
		case ComponentType::ZZZAxis :
			vendorStr = CONTROLLER_VENDOR;
			modelStr = CONTROLLER_MODEL;
			objectStr = HOME_ZZZ_INPUTS;
			axisNameStr = "zzz";
			break;
		case ComponentType::AspirationPump1 :
			vendorStr = PUMP_VENDOR;
			modelStr = PUMP_MODEL;
			objectStr = HOME_ASP_PUMP1_INPUTS;
			axisNameStr = "asp1";
			break;
		case ComponentType::DispensePump1 :
			vendorStr = PUMP_VENDOR;
			modelStr = PUMP_MODEL;
			objectStr = HOME_DISP_PUMP1_INPUTS;
			axisNameStr = "disp1";
			break;
		case ComponentType::DispensePump2 :
			vendorStr = PUMP_VENDOR;
			modelStr = PUMP_MODEL;
			objectStr = HOME_DISP_PUMP2_INPUTS;
			axisNameStr = "disp2";
			break;
		case ComponentType::DispensePump3 :
			vendorStr = PUMP_VENDOR;
			modelStr = PUMP_MODEL;
			objectStr = HOME_DISP_PUMP3_INPUTS;
			axisNameStr = "disp3";
			break;
		case ComponentType::PickingPump1 :
			vendorStr = PUMP_VENDOR;
			modelStr = PUMP_MODEL;
			objectStr = HOME_PICK_PUMP1_INPUTS;
			axisNameStr = "pick1";
			break;
		case ComponentType::WhiteLightSource :
			vendorStr = VENDOR;
			modelStr = MODEL;
			objectStr = HOME_WHITE_LIGHT_INPUTS;
			axisNameStr = "white";
			break;
		case ComponentType::FluorescenceLightFilter :
			vendorStr = VENDOR;
			modelStr = MODEL;
			objectStr = HOME_FLUORESCENCE_LIGHT_INPUTS;
			axisNameStr = "fluoro";
			break;
		case ComponentType::AnnularRingHolder :
			vendorStr = CONTROLLER_VENDOR;
			modelStr = CONTROLLER_MODEL;
			objectStr = HOME_SH_INPUTS;
			axisNameStr = "sh";
			break;
		case ComponentType::Camera :
			vendorStr = VENDOR;
			modelStr = MODEL;
			objectStr = HOME_CAMERA_INPUTS;
			axisNameStr = "camera";
			break;
		case ComponentType::OpticalColumn :
			vendorStr = VENDOR;
			modelStr = MODEL;
			objectStr = HOME_OPTICAL_COLUMN_INPUTS;
			axisNameStr = "oc";
			break;
		case ComponentType::Incubator :
			vendorStr = VENDOR;
			modelStr = MODEL;
			objectStr = HOME_INCUBATOR_INPUTS;
			axisNameStr = "incubator";
			break;
		case ComponentType::CxPM :
			vendorStr = VENDOR;
			modelStr = MODEL;
			objectStr = HOME_CXPM_INPUTS;
			axisNameStr = "cxpm";
			break;
		case ComponentType::BarcodeReader :
			vendorStr = VENDOR;
			modelStr = MODEL;
			objectStr = HOME_BARCODE_READER_INPUTS;
			axisNameStr = "bcode";
			break;
		default:
			succeeded = false;
	}
	axisObj = json_object_new_object();
	FieldWithMetaData property = props->GetFieldByName( vendorStr );
	int result = json_object_object_add( axisObj, vendorStr.c_str(), json_object_new_string( property.setting.c_str() ) );
	succeeded = succeeded && (result == 0);
	property = props->GetFieldByName( modelStr );
	result = json_object_object_add( axisObj, modelStr.c_str(), json_object_new_string( property.setting.c_str() ) );
	succeeded = succeeded && (result == 0);
	result = json_object_object_add( payloadObj_in_out, objectStr.c_str(), axisObj );
	succeeded = succeeded && (result == 0);

	if (!succeeded)
	{
		stringstream msg;
		msg << "Failed to create " << objectStr;
		LOG_ERROR( threadName, axisNameStr.c_str(), msg.str().c_str() );
	}
	return succeeded;
}


///----------------------------------------------------------------------
/// AddFlowRatesAndLimits
/// Extract the select component properties that the Doler will use
/// for pump control and add them to the json input-message.
///----------------------------------------------------------------------
bool DesHelper::AddFlowRatesAndLimits( const char * threadName_in, json_object * inputMsg_in_out, const char * axisName_in, ComponentProperties * props )
{
	bool succeeded = true;
	if (inputMsg_in_out)
	{
		/// Make a json object for grouping speeds for one axis
		json_object * groupObj = json_object_new_object();
		if (groupObj)
		{
			LOG_TRACE( threadName_in, axisName_in, "-" );
			/// Get needed property values and add them to the group.
			string name = CALIBRATION_COEFFICIENT_1;
			float fValue = props->GetFieldByName( name ).GetValueAsFloat();
			int rc = json_object_object_add( groupObj, CALIBRATION_COEFFICIENT_1, json_object_new_double( fValue ) );
			succeeded &= (rc == 0);

			name = CALIBRATION_COEFFICIENT_2;
			fValue = props->GetFieldByName( name ).GetValueAsFloat();
			rc = json_object_object_add( groupObj, CALIBRATION_COEFFICIENT_2, json_object_new_double( fValue ) );
			succeeded &= (rc == 0);

			name = ACCELERATION_TIME;
			long lValue = props->GetFieldByName( name ).GetValueAsULong();
			rc = json_object_object_add( groupObj, ACCELERATION_TIME, json_object_new_int64( lValue ) );
			succeeded &= (rc == 0);

			name = DECELERATION_TIME;
			lValue = props->GetFieldByName( name ).GetValueAsULong();
			rc = json_object_object_add( groupObj, DECELERATION_TIME, json_object_new_int64( lValue ) );
			succeeded &= (rc == 0);

			/// Create a name for the group and add it to the payload.
			string groupName = axisName_in;
			groupName += "_flow_rates";
			rc = json_object_object_add( inputMsg_in_out, groupName.c_str(), groupObj );
			succeeded &= (rc == 0);
		}
		else
		{
			LOG_ERROR( threadName_in, "flow_rates", "FAILED to create group object" );
			succeeded = false;
		}
	}
	else
	{
		succeeded = false;
	}
	return succeeded;
}

bool DesHelper::AddPickingPumpFlowParams( const char * threadName_in, json_object * inputMsg_in_out, const char * axisName_in, ComponentProperties * props )
{
	bool succeeded = true;
	if (inputMsg_in_out)
	{
		/// Make a json object for grouping speeds for one axis
		json_object * groupObj = json_object_new_object();
		if (groupObj)
		{
			LOG_TRACE( threadName_in, axisName_in, "-" );
			/// Get needed property values and add them to the group.
			string name = CALIBRATION_COEFFICIENT_1;
			float fValue = props->GetFieldByName( name ).GetValueAsFloat();
			int rc = json_object_object_add( groupObj, CALIBRATION_COEFFICIENT_1, json_object_new_double( fValue ) );
			succeeded &= (rc == 0);

			name = CALIBRATION_COEFFICIENT_2;
			fValue = props->GetFieldByName( name ).GetValueAsFloat();
			rc = json_object_object_add( groupObj, CALIBRATION_COEFFICIENT_2, json_object_new_double( fValue ) );
			succeeded &= (rc == 0);

			/// Create a name for the group and add it to the payload.
			string groupName = axisName_in;
			groupName += "_flow_rates";
			rc = json_object_object_add( inputMsg_in_out, groupName.c_str(), groupObj );
			succeeded &= (rc == 0);
		}
		else
		{
			LOG_ERROR( threadName_in, "flow_rates", "FAILED to create group object" );
			succeeded = false;
		}
	}
	else
	{
		succeeded = false;
	}
	return succeeded;
}

///----------------------------------------------------------------------
/// ComposeSetZToSafeHeightMessage
/// This function tries out brute-force copying from the parent object.
/// This approach relies on ComposeCommand(), which also assigns
/// transport-version and machine-ID. (Maybe we don't want to change the
/// machine-ID, though.)
///
/// During profiling test of Nov 4, 2021, all 3 variants were run 1000
/// times in a loop. This inflated their percentage of runtime, but the
/// purpose was to compare just these 3 functions.
/// This Z approach was the middle. It took 6.69% of runtime.
///----------------------------------------------------------------------
std::string DesHelper::ComposeSetZToSafeHeightMessage( json_object* parent, unsigned long z_safeHeight_nm )
{
	/// Extract the parts we want to copy from the parent.
	int commandGroupId = 0;
	int commandId = 0;
	std::string sessionId = "";
	bool succeeded = SplitCommand( parent,
								   nullptr,
								   nullptr,
								   &commandGroupId,
								   &commandId,
								   &sessionId,
								   nullptr );

	/// Make the parts we want to define.
	std::string messageType = SET_Z_TO_SAFE_HEIGHT;
	json_object * payload = DesHelper::ComposeSetZToSafeHeightPayload( z_safeHeight_nm );

	/// Compose a new object from all our parts.
	json_object *subCommand = ComposeCommand( messageType,
											  commandGroupId,
											  commandId,
											  sessionId,
											  payload );

	/// Convert the child obect to a JSON string, which we'll return.
	std::string fullMessage = json_object_to_json_string( subCommand );
	
	/// Be sure to free the object.
	int freed = json_object_put( subCommand );
	LETS_ASSERT(freed == 1);

	return fullMessage;
}

///----------------------------------------------------------------------
/// ComposeSetZZToSafeHeightMessage
/// This function makes a deep copy of the parent, then changes the two
/// fields we want to define. This is probably an efficient approach.
/// It preserves the machine-ID of the parent. It also preserves the
/// transport-version defined by the parent. This may be OK as long as
/// it was validated by the MQTT-Receiver thread.
///
/// During profiling test of Nov 4, 2021, all 3 variants were run 1000
/// times in a loop. This inflated their percentage of runtime, but the
/// purpose was to compare just these 3 functions.
/// This ZZ approach was the slowest. It took 7.74% of runtime.
///----------------------------------------------------------------------
std::string DesHelper::ComposeSetZZToSafeHeightMessage( json_object* parent, unsigned long zz_safeHeight_nm )
{
	/// Make a deep copy of the parent, because most of the child's
	/// data will be the same.
	json_object *child = nullptr;
	int rc = json_object_deep_copy( parent, &child, NULL );
	LETS_ASSERT(rc == 0);
	
	/// Change the child's messageType and payload.
	rc = json_object_object_add( child, MESSAGE_TYPE, json_object_new_string( SET_ZZ_TO_SAFE_HEIGHT ) );
	LETS_ASSERT(rc == 0);
	
	/// Create a payload.
	rc = json_object_object_add( child, PAYLOAD, DesHelper::ComposeSetZZToSafeHeightPayload( zz_safeHeight_nm ) );
	LETS_ASSERT(rc == 0);

	/// Convert the child obect to a JSON string, then free the object.
	std::string childMessage = json_object_to_json_string( child );
	rc = json_object_put( child );
	LETS_ASSERT(rc == 1);

	return childMessage;
}

///----------------------------------------------------------------------
/// ComposeSetZZZToSafeHeightMessage
/// Using json-c only for decomposing. Composing the new message 
/// manually, with string operations.
///
/// During profiling test of Nov 4, 2021, all 3 variants were run 1000
/// times in a loop. This inflated their percentage of runtime, but the
/// purpose was to compare just these 3 functions.
/// This ZZZ approach was the fastest. It took 3.29% of runtime. It is
/// at least twice as fast as the others.
///----------------------------------------------------------------------
std::string DesHelper::ComposeSetZZZToSafeHeightMessage( json_object* parent, unsigned long zzz_safeHeight_nm )
{
	/// Iterate over the named objects of the parent object
	
	std::stringstream child;
	struct json_object_iterator it;
	struct json_object_iterator itEnd;
	it = json_object_iter_init_default();
	it = json_object_iter_begin(parent);
	itEnd = json_object_iter_end(parent);
	int count = 0;
	
	child << "{ ";
	while (!json_object_iter_equal(&it, &itEnd))
	{
		count++;
		// cerr << "count: " << count << endl;
		std::string name = json_object_iter_peek_name(&it);
		if (name == MESSAGE_TYPE)
		{
			child << "\"" << MESSAGE_TYPE << "\": \"" << SET_ZZZ_TO_SAFE_HEIGHT << "\", ";
		}
		else
		{
			if (name == PAYLOAD)
			{
				child << "\"" << PAYLOAD << "\": { \"" << ZZZ_SAFE_HEIGHT << "\": " << std::to_string(zzz_safeHeight_nm) <<" }";
			}
			else
			{
				json_object * value = json_object_iter_peek_value(&it);
				LETS_ASSERT( value != NULL );
				json_type jtype = json_object_get_type( value );
				LETS_ASSERT( (jtype == json_type_int) || (jtype == json_type_string) );
				child << "\"" << name << "\": " << json_object_to_json_string( value ) << ", ";
			}
		}
		json_object_iter_next(&it);
		LETS_ASSERT(count < 50);
	}
	child << " }";
	return child.str();
}

// TODO: Change this to make a message with a generic long payload.
// TODO: Do that the next time such a message is needed.
///----------------------------------------------------------------------
/// ComposeSetZAxisSpeedMessage
///----------------------------------------------------------------------
std::string DesHelper::ComposeSetZAxisSpeedMessage( json_object* parent, long zSpeed  )
{
	/// Make a deep copy of the parent, because most of the child's
	/// data will be the same.
	json_object *child = nullptr;
	int rc = json_object_deep_copy( parent, &child, NULL );
	LETS_ASSERT(rc == 0);
	
	/// Change the child's messageType and payload.
	rc = json_object_object_add( child, MESSAGE_TYPE, json_object_new_string( SET_Z_AXIS_SPEED ) );
	LETS_ASSERT(rc == 0);
	
	/// Create a payload.
	rc = json_object_object_add( child, PAYLOAD, DesHelper::ComposeSetZAxisSpeedPayload( zSpeed ) );
	LETS_ASSERT(rc == 0);

	/// Convert the child obect to a JSON string, then free the object.
	std::string childMessage = json_object_to_json_string( child );
	rc = json_object_put( child );
	LETS_ASSERT(rc == 1);

	return childMessage;
}

// TODO: Consider this optimization: Make a version of ComposeMessageWithNullPayload
// that modifies the parent object instead of making a deep copy. Be sure that callers
// are extra careful about its use.
///----------------------------------------------------------------------
/// ComposeMessageWithNullPayload
/// Compose a new message (in JSON format) as a copy of parent, with
/// two changes: 1) Change messageType to the given value. 2) Set the
/// payload to null.
/// (I expect this function to get a lot of callers.)
///----------------------------------------------------------------------
std::string DesHelper::ComposeMessageWithNullPayload( json_object* parent, const char * messageType )
{
	/// Make a deep copy of the parent, because most of the child's
	/// data will be the same.
	json_object *child = nullptr;
	int rc = json_object_deep_copy( parent, &child, NULL );
	LETS_ASSERT(rc == 0);
	
	/// Change the child's messageType.
	rc = json_object_object_add( child, MESSAGE_TYPE, json_object_new_string( messageType ) );
	LETS_ASSERT(rc == 0);
	
	/// Create a payload.
	rc = json_object_object_add( child, PAYLOAD, NULL );
	LETS_ASSERT(rc == 0);

	/// Convert the child obect to a JSON string, then free the object.
	std::string childMessage = json_object_to_json_string( child );
	rc = json_object_put( child );
	LETS_ASSERT(rc == 1);

	return childMessage;
}

///----------------------------------------------------------------------
/// ComposeMessageWithGivenPayload
/// Compose a new message (in JSON format) as a copy of parent, but
/// replacing the parent's payload object with a new one provided
/// by the caller.
///----------------------------------------------------------------------
std::string DesHelper::ComposeMessageWithGivenPayload( json_object* parentObj,
												const char * messageType,
												json_object* payloadObj )
{
	/// Make a deep copy of the parent, because most of the child's
	/// data will be the same.
	json_object *childObj = nullptr;
	int rc = json_object_deep_copy( parentObj, &childObj, NULL );
	LETS_ASSERT(rc == 0);
	
	/// Change the child's messageType and payload.
	rc = json_object_object_add( childObj, MESSAGE_TYPE, json_object_new_string( messageType ) );
	LETS_ASSERT(rc == 0);
	
	/// Add the given payload to the child message
	rc = json_object_object_add( childObj, PAYLOAD, payloadObj );
	LETS_ASSERT(rc == 0);

	/// Convert the child obect to a JSON string, then free the object.
	std::string childMessage = json_object_to_json_string( childObj );
	rc = json_object_put( childObj );
	LETS_ASSERT(rc == 1);

	return childMessage;
	
}


CalibrationType DesHelper::IdentifyCalibrationType( const std::string type )
{
//	cout << "IdentifyCalibrationType entered." << endl;
	if (type == ENUM_NONE) return CalibrationType::None;
	if (type == ENUM_ZERO_ORDER_POLY) return CalibrationType::ZeroOrderPolynomial;
	if (type == ENUM_FIRST_ORDER_POLY_FORCE_0_INTERCEPT) return CalibrationType::FirstOrderPolynomialForceZeroIntercept;
	if (type == ENUM_FIRST_ORDER_POLY) return CalibrationType::FirstOrderPolynomial;
	if (type == ENUM_SECOND_ORDER_POLY) return CalibrationType::SecondOrderPolynomial;
//	cout << "IdentifyCalibrationType returning Unknown." << endl;
	return CalibrationType::Unknown;
}

std::string DesHelper::CalibrationTypeEnumToStringName( CalibrationType type )
{
	switch( type )
	{
		case CalibrationType::None :
			return ENUM_NONE;
			break;
		case CalibrationType::ZeroOrderPolynomial :
			return ENUM_ZERO_ORDER_POLY;
			break;
		case CalibrationType::FirstOrderPolynomialForceZeroIntercept :
			return ENUM_FIRST_ORDER_POLY_FORCE_0_INTERCEPT;
			break;
		case CalibrationType::FirstOrderPolynomial :
			return ENUM_FIRST_ORDER_POLY;
			break;
		case CalibrationType::SecondOrderPolynomial :
			return ENUM_SECOND_ORDER_POLY;
			break;
	}
	return ENUM_UNKNOWN;
}

///----------------------------------------------------------------------
/// ExtractCreateMontageParameters
/// Extact a payload object from inputMsg, then extract the values
/// that are expected to be in a CreateMontage payload.
///----------------------------------------------------------------------
bool DesHelper::ExtractCreateMontageParameters( const char * threadName,
												json_object* inputMsg,
												unsigned long* pixel_count_x,
												unsigned long* pixel_count_y,
												unsigned long* X_stepCount,
												unsigned long* Y_stepCount,
												long* X_start_nm,
												long* Y_start_nm,
												unsigned long* bin_factor,
												unsigned long* camera_color_depth,
												unsigned long* scaled_color_depth,
												float* X_stepIncrementUm,
												float* Y_stepIncrementUm,
												float* scanMovementDelayMs,
												long* velocity,
												long* acceleration,
												long* deceleration,
												long* stp,
												long* axisResolutionNm )
{

	bool succeeded = ExtractNmNotations(    threadName,
											inputMsg,
											true, /// for Optical
											true, /// use A1 notation
											*X_start_nm,
											*Y_start_nm,
											*velocity,
											*acceleration,
											*deceleration,
											*stp,
											*axisResolutionNm );
	json_object * payloadObj = json_object_object_get( inputMsg, PAYLOAD );
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string( payloadObj );
		cout << payloadStr << endl;
		succeeded &= ExtractULongValue( threadName, payloadObj, PIXEL_COUNT_X, *pixel_count_x );
		succeeded &= ExtractULongValue( threadName, payloadObj, PIXEL_COUNT_Y, *pixel_count_y );
		succeeded &= ExtractULongValue( threadName, payloadObj, X_STEP_COUNT, *X_stepCount );
		succeeded &= ExtractULongValue( threadName, payloadObj, Y_STEP_COUNT, *Y_stepCount );
		succeeded &= ExtractULongValue( threadName, payloadObj, BIN_FACTOR, *bin_factor );
		succeeded &= ExtractULongValue( threadName, payloadObj, CAMERA_COLOR_DEPTH, *camera_color_depth );
		succeeded &= ExtractULongValue( threadName, payloadObj, SCALED_COLOR_DEPTH, *scaled_color_depth );
		succeeded &= ExtractULongValue( threadName, payloadObj, SCALED_COLOR_DEPTH, *scaled_color_depth );
		succeeded &= ExtractFloatValue( threadName, payloadObj, X_STEP_INCREMENT_NM, *X_stepIncrementUm );
		succeeded &= ExtractFloatValue( threadName, payloadObj, Y_STEP_INCREMENT_NM, *Y_stepIncrementUm );
		succeeded &= ExtractFloatValue( threadName, payloadObj, SCAN_MOVEMENT_DELAY_MS, *scanMovementDelayMs );
	}
	return succeeded;
}

bool DesHelper::ExtractSpeedsAndLimits(	const char * threadName_in,
										json_object* inputMsg_in,
										const char * axisName_in,
										long &velocity_out,
										long &acceleration_out,
										long &deceleration_out,
										long &stp_out,
										long &axis_resolution_out,
										long &soft_travel_limit_plus,
										long &soft_travel_limit_minus )
{
	bool succeeded = false;
	json_object * payloadObj = json_object_object_get( inputMsg_in, PAYLOAD );
	if (payloadObj)
	{
		string groupName = axisName_in;
		groupName += "_speeds";
		json_object * groupObj = json_object_object_get( payloadObj, groupName.c_str() );
		if (groupObj)
		{
			succeeded = ExtractLongValue( threadName_in, groupObj, VELOCITY, velocity_out );
			succeeded &= ExtractLongValue( threadName_in, groupObj, ACCELERATION, acceleration_out );
			succeeded &= ExtractLongValue( threadName_in, groupObj, DECELERATION, deceleration_out );
			succeeded &= ExtractLongValue( threadName_in, groupObj, STP, stp_out );
			succeeded &= ExtractLongValue( threadName_in, groupObj, AXIS_RESOLUTION, axis_resolution_out );
			succeeded &= ExtractLongValue( threadName_in, groupObj, SOFT_TRAVEL_LIMIT_POSITION_PLUS, soft_travel_limit_plus );
			succeeded &= ExtractLongValue( threadName_in, groupObj, SOFT_TRAVEL_LIMIT_POSITION_MINUS, soft_travel_limit_minus );
		}
		else
		{
			stringstream msg;
			msg << "Failed to find object named " << groupName;
			LOG_ERROR( threadName_in, "speeds", msg.str().c_str() );
			succeeded = false;
		}
	}
	return succeeded;
}

bool DesHelper::ExtractMessageIDs(const char * threadName_in,
								  json_object* inputMsg_in,
								  const char * axisName_in,
								  long &command_id_out )
{
	bool succeeded = false;
	if (inputMsg_in)
	{
		succeeded = ExtractLongValue( threadName_in, inputMsg_in, COMMAND_ID, command_id_out );
		if (!succeeded)
		{
			stringstream msg;
			msg << "Failed to find command_id";
			LOG_ERROR( threadName_in, "message_IDs", msg.str().c_str() );
		}
	}
	return succeeded;
}

bool DesHelper::ExtractXYSpeeds(const char * threadName_in,
								json_object* inputMsg_in,
								const char * axisName_in,
								long &velocity_out,
								long &acceleration_out,
								long &deceleration_out,
								long &stp_out,
								long &axis_resolution_out,
								long &x_soft_travel_limit_plus,
								long &x_soft_travel_limit_minus,
								long &y_soft_travel_limit_plus,
								long &y_soft_travel_limit_minus )
{
	bool succeeded = false;
	json_object * payloadObj = json_object_object_get( inputMsg_in, PAYLOAD );
	if (payloadObj)
	{
		string groupName = axisName_in;
		groupName += "_speeds";
		json_object * groupObj = json_object_object_get( payloadObj, groupName.c_str() );
		if (payloadObj)
		{
			succeeded = ExtractLongValue( threadName_in, groupObj, VELOCITY, velocity_out );
			succeeded &= ExtractLongValue( threadName_in, groupObj, ACCELERATION, acceleration_out );
			succeeded &= ExtractLongValue( threadName_in, groupObj, DECELERATION, deceleration_out );
			succeeded &= ExtractLongValue( threadName_in, groupObj, STP, stp_out );
			succeeded &= ExtractLongValue( threadName_in, groupObj, AXIS_RESOLUTION, axis_resolution_out );
			succeeded &= ExtractLongValue( threadName_in, groupObj, X_SOFT_TRAVEL_LIMIT_POSITION_PLUS, x_soft_travel_limit_plus );
			succeeded &= ExtractLongValue( threadName_in, groupObj, X_SOFT_TRAVEL_LIMIT_POSITION_MINUS, x_soft_travel_limit_minus );
			succeeded &= ExtractLongValue( threadName_in, groupObj, Y_SOFT_TRAVEL_LIMIT_POSITION_PLUS, y_soft_travel_limit_plus );
			succeeded &= ExtractLongValue( threadName_in, groupObj, Y_SOFT_TRAVEL_LIMIT_POSITION_MINUS, y_soft_travel_limit_minus );
		}
		else
		{
			stringstream msg;
			msg << "Failed to find object named " << groupName;
			LOG_ERROR( threadName_in, "speeds", msg.str().c_str() );
			succeeded = false;
		}
	}
	else
	{
		LOG_ERROR( threadName_in, "speeds", "Failed to find payload" );
		succeeded = false;
	}
	return succeeded;
}

bool DesHelper::ExtractMontageSpeeds(	const char * threadName_in,
										json_object* inputMsg_in,
										const char * axisName_in,
										long &velocity_out,
										long &acceleration_out,
										long &deceleration_out,
										long &stp_out )
{
	bool succeeded = false;
	json_object * payloadObj = json_object_object_get( inputMsg_in, PAYLOAD );
	if (payloadObj)
	{
		json_object * groupObj = json_object_object_get( payloadObj, MONTAGE_SPEEDS );
		if (payloadObj)
		{
			succeeded = ExtractLongValue( threadName_in, groupObj, VELOCITY, velocity_out );
			succeeded &= ExtractLongValue( threadName_in, groupObj, ACCELERATION, acceleration_out );
			succeeded &= ExtractLongValue( threadName_in, groupObj, DECELERATION, deceleration_out );
			succeeded &= ExtractLongValue( threadName_in, groupObj, STP, stp_out );
		}
		else
		{
			stringstream msg;
			msg << "Failed to find object named " << MONTAGE_SPEEDS;
			LOG_ERROR( threadName_in, axisName_in, msg.str().c_str() );
			succeeded = false;
		}
	}
	else
	{
		LOG_ERROR( threadName_in, axisName_in, "Failed to find payload" );
		succeeded = false;
	}
	return succeeded;
}

///----------------------------------------------------------------------
/// ExtractFlowRatesAndLimits
/// Extract pump control values from the json input-message and return
/// them in output parameters.
///----------------------------------------------------------------------
bool DesHelper::ExtractFlowRatesAndLimits( const char * threadName_in,
											const json_object * inputMsg_in,
											const char * axisName_in,
											float & calCoeff1_out,
											float & calCoeff2_out,
											unsigned long & accelTime_ms_out,
											unsigned long & decelTime_ms_out )
{
	bool succeeded = false;
	json_object * payloadObj = json_object_object_get( inputMsg_in, PAYLOAD );
	if (payloadObj)
	{
		string groupName = axisName_in;
		groupName += "_flow_rates";
		json_object * groupObj = json_object_object_get( payloadObj, groupName.c_str() );
		if (payloadObj)
		{
			succeeded = ExtractFloatValue( threadName_in, groupObj, CALIBRATION_COEFFICIENT_1, calCoeff1_out );
			succeeded &= ExtractFloatValue( threadName_in, groupObj, CALIBRATION_COEFFICIENT_2, calCoeff2_out );
			succeeded &= ExtractULongValue( threadName_in, groupObj, ACCELERATION_TIME, accelTime_ms_out );
			succeeded &= ExtractULongValue( threadName_in, groupObj, DECELERATION_TIME, decelTime_ms_out );
		}
		else
		{
			stringstream msg;
			msg << "Failed to find object named " << groupName;
			LOG_ERROR( threadName_in, "speeds", msg.str().c_str() );
			succeeded = false;
		}
	}
	else
	{
		LOG_ERROR( threadName_in, "flow_rates", "Failed to find payload" );
		succeeded = false;
	}
	return succeeded;
}



bool DesHelper::ExtractNmNotations( const char * threadName,
									json_object* inputMsg,
		   							bool forOptical,
									bool useA1Notation,
									long &xOffsetNm,
									long &yOffsetNm,
									long &velocity,
									long &acceleration,
									long &deceleration,
									long &stp,
									long &axis_resolution )
{
	long platePosition = 0L;
	long well_row = 0L;
	long well_column = 0L;
	long x_offset_nm = 0;
	long y_offset_nm = 0;
	
	PlateType pt = ExtractPayloadPlateType( inputMsg );

	bool succeeded = false;
	json_object * payloadObj = json_object_object_get( inputMsg, PAYLOAD );
	if (payloadObj)
	{
		json_object * speedsObj = json_object_object_get( payloadObj, "xy_speeds" );
		if (speedsObj)
		{
			LOG_TRACE( "unk", "xy", "Found speeds" ); 
			succeeded = ExtractLongValue( threadName, speedsObj, VELOCITY, velocity );
			succeeded &= ExtractLongValue( threadName, speedsObj, ACCELERATION, acceleration );
			succeeded &= ExtractLongValue( threadName, speedsObj, DECELERATION, deceleration );
			succeeded &= ExtractLongValue( threadName, speedsObj, STP, stp );
			succeeded &= ExtractLongValue( threadName, speedsObj, AXIS_RESOLUTION, axis_resolution );
		}
		else
		{
			succeeded = false;
			LOG_ERROR( "unk", "xy", "Missing speeds" );
		}
		if (useA1Notation)
		{
			string wellInA1Notation = ExtractPayloadStringValue( inputMsg, WELL );
			succeeded &= A1ToRc( wellInA1Notation, well_row, well_column );
		}
		else /// use RC notation
		{
			succeeded &= ExtractLongValue( threadName, payloadObj, WELL_ROW, well_row );
			succeeded &= ExtractLongValue( threadName, payloadObj, WELL_COLUMN, well_column );
		}
		json_object * testObj = json_object_object_get( payloadObj, X_OFFSET_NM );
		bool found = false;
		if (testObj)
		{
			found = true;
			succeeded &= ExtractLongValue( threadName, payloadObj, X_OFFSET_NM, x_offset_nm );
		}
		else
		{
			testObj = json_object_object_get( payloadObj, X_START_OFFSET_NM );
			if (testObj)
			{
				found = true;
				succeeded &= ExtractLongValue( threadName, payloadObj, X_START_OFFSET_NM, x_offset_nm );
			}
		}
		succeeded &= found;
		if (!found) LOG_ERROR( threadName, "xy", "failed to find x_(start_)offset_nm" );

		testObj = json_object_object_get( payloadObj, Y_OFFSET_NM );
		found = false;
		if (testObj)
		{
			found = true;
			succeeded &= ExtractLongValue( threadName, payloadObj, Y_OFFSET_NM, y_offset_nm );
		}
		else
		{
			testObj = json_object_object_get( payloadObj, Y_START_OFFSET_NM );
			if (testObj)
			{
				found = true;
				succeeded &= ExtractLongValue( threadName, payloadObj, Y_START_OFFSET_NM, y_offset_nm );
			}
		}
		succeeded &= found;
		if (!found) LOG_ERROR( threadName, "xy", "failed to find y_(start_)offset_nm" );

		succeeded &= ExtractLongValue( threadName, payloadObj, PLATE_POSITION, platePosition );
	}
	if (succeeded)
	{
		if (platePosition == 1)
		{
			long x_plate_1_right_edge = 0L;
			long y_plate_1_top_edge = 0L;
			if (forOptical)
			{
				succeeded &= ExtractLongValue( threadName, payloadObj, X_PLATE_1_RIGHT_EDGE_TO_OPTICAL_PATH, x_plate_1_right_edge );
				succeeded &= ExtractLongValue( threadName, payloadObj, Y_PLATE_1_TOP_EDGE_TO_OPTICAL_PATH, y_plate_1_top_edge );
			}
			else /// for Tip
			{
				succeeded &= ExtractLongValue( threadName, payloadObj, X_PLATE_1_RIGHT_EDGE_TO_TIP_WORKING_POSITION, x_plate_1_right_edge );
				succeeded &= ExtractLongValue( threadName, payloadObj, Y_PLATE_1_TOP_EDGE_TO_TIP_WORKING_POSITION, y_plate_1_top_edge );
			}

			xOffsetNm = x_plate_1_right_edge + pt.firstWellX + ((well_column-1) * pt.interWellX) - x_offset_nm;
			yOffsetNm = y_plate_1_top_edge + pt.firstWellY + ((well_row-1) * pt.interWellY) - y_offset_nm;
		}
		else if (platePosition == 2)
		{
			long x_plate_2_left_edge = 0L;
			long y_plate_2_top_edge = 0L;
			if (forOptical)
			{
				succeeded &= ExtractLongValue( threadName, payloadObj, X_PLATE_2_LEFT_EDGE_TO_OPTICAL_PATH, x_plate_2_left_edge );
				succeeded &= ExtractLongValue( threadName, payloadObj, Y_PLATE_2_TOP_EDGE_TO_OPTICAL_PATH, y_plate_2_top_edge );
			}
			else /// for Tip
			{
				succeeded &= ExtractLongValue( threadName, payloadObj, X_PLATE_2_LEFT_EDGE_TO_TIP_WORKING_POSITION, x_plate_2_left_edge );
				succeeded &= ExtractLongValue( threadName, payloadObj, Y_PLATE_2_TOP_EDGE_TO_TIP_WORKING_POSITION, y_plate_2_top_edge );
			}

			xOffsetNm = x_plate_2_left_edge - pt.plateWidth + pt.firstWellX + ((well_column-1) * pt.interWellX) - x_offset_nm;
			yOffsetNm  = y_plate_2_top_edge + pt.firstWellY + ((well_row-1) * pt.interWellY) - y_offset_nm;
		}
		else
		{
			succeeded = false;
			stringstream msg;
			msg << "Invalid plate position: " << platePosition;
			LOG_ERROR( threadName, "xy", msg.str().c_str() );
			succeeded = false;
		}
	}
	return succeeded;
}

bool DesHelper::ExtractTipParameters(   const char * threadName,
										json_object* inputMsg,
										TipType & tipType,
										ComponentType & tipAxis,
										unsigned long & tipNumber )
{
	bool succeeded = false;
	json_object * payloadObj = json_object_object_get( inputMsg, PAYLOAD );
	if (payloadObj)
	{
		succeeded = true;
		json_object * tipTypeObj = json_object_object_get( payloadObj, TIP_TYPE );
		if (tipTypeObj)
		{
			succeeded = tipType.PopulateFromJsonCObject( threadName, tipTypeObj );
		}
		else
		{
			LOG_ERROR( "unk", "des", "Failed to find TIP_TYPE" );
			succeeded = false;
		}

		json_object * fieldObj = json_object_object_get( payloadObj, TIP_AXIS );
		if (fieldObj)
		{
			errno = 0;
			string tipAxisStr = json_object_get_string( fieldObj );
			succeeded &= (errno == 0);
			tipAxis = StringNameToComponentTypeEnum( tipAxisStr.c_str() );
		}
		else
		{
			LOG_ERROR( "unk", "des", "Failed to find TIP_AXIS" );
			succeeded = false;
		}

		json_object * tipNumberObj = json_object_object_get( payloadObj, TIP_NUMBER );
		if (tipNumberObj)
		{
			errno = 0;
			tipNumber = json_object_get_uint64( tipNumberObj );
			succeeded &= (errno == 0);
		}
		else
		{
			LOG_ERROR( "unk", "des", "Failed to find TIP_NUMBER" );
			succeeded = false;
		}
	}
	return succeeded;
}

///----------------------------------------------------------------------
/// NameData
///----------------------------------------------------------------------

NameData::NameData(std::string machineId, std::string masterId) :
m_machineId(machineId), m_masterId(masterId)
{
}

NameData::NameData(const char * machineId, const char * masterId):
m_machineId(machineId), m_masterId(masterId)
{
}

void DesHelper::ExtractSimpleField( json_object * fieldObj_in, string & fieldStr_out )
{
	if (fieldObj_in == nullptr)
	{
		fieldStr_out = "";
	}
	else
	{
		fieldStr_out = json_object_get_string( fieldObj_in );
	}
}

void DesHelper::ExtractFieldVerbatim( json_object * fieldObj_in, std::string & fieldStr_out )
{
	if (fieldObj_in == nullptr)
	{
		fieldStr_out = "";
	}
	else
	{
		fieldStr_out = json_object_to_json_string( fieldObj_in );
	}
}

// TODO: Consider deprecating this if DesHelper::UpdateProperty() works OK for all situations.
///----------------------------------------------------------------------
/// ExtractProperty
/// Copies 1 value and 5 pieces of metadata from a json object to a
/// FieldWithMetaData object. All CPD parameters get this treatment, even
/// though some of the metadata may not be applicable. (e.g. min & max)
/// The "editable" and "persist" fields are converted into class enums.
/// All other values continue to be stored in string value. Int and Float
/// values will be test-converted, here, to confirm  they are in good
/// shape. However, they will continue to be saved as strings until
/// they are needed.
///
/// If all values check out, they are returned. None are returned
/// otherwise.
/// The checks are as minimal as possible. Fields are checked for being
/// in range.
/// (The value is NOT range-checked against min and max values.)
/// The checks are more lenient if the persist field passes tests and
/// its value is "Remove".
///----------------------------------------------------------------------
/// This is the inverse of FieldWithMetaData::PropertyFieldsToJsonObject
bool DesHelper::ExtractProperty( json_object* cpdFieldObj, FieldWithMetaData* field )
{
	/// Temporary places to put values extracted from cpdFieldObj.
	/// In the end, these will get copied to the field object IFF they
	/// check out OK.
	std::string setting = "";
	EditableType editable = EditableType::EditableFixed;
	PersistType persist = PersistType::NotPersist;
	std::string dataType = "";
	std::string min = "";
	std::string max = "";
	std::string baseUnit = "";
	std::string scaleFactor = "";
	std::string pointsPastDecimal = "";
	std::string displayDirective = "";
	std::string displayMinValue = "";
	std::string displayMaxValue = "";
	FieldState valueState = FieldState::Missing;
	FieldState editableState = FieldState::Missing;
	FieldState persistState = FieldState::Missing;
	FieldState dataTypeState = FieldState::Missing;
	FieldState minState = FieldState::Missing;
	FieldState maxState = FieldState::Missing;

	/// Variables used for stepping through all of the elements of the
	/// json_object.
	struct json_object_iterator it;
	struct json_object_iterator itEnd;
	it = json_object_iter_init_default();
	it = json_object_iter_begin( cpdFieldObj );
	itEnd = json_object_iter_end( cpdFieldObj );
	struct json_object * objPtr = nullptr;
	int count = 0;

	while (!json_object_iter_equal(&it, &itEnd))
	{
		count++;
		std::string name = json_object_iter_peek_name( &it );
		if (name == FIELD_EDITABILITY_TYPE)
		{
			editableState = FieldState::OutOfRange;
			objPtr = json_object_iter_peek_value( &it );
			if (objPtr == nullptr)
			{
				editable = EditableType::Unknown; editableState = FieldState::Undefined;
				LOG_WARN( "unk", "properties", "editable_type=undefined" );
			}
			else
			{
				std::string type = json_object_get_string( objPtr );
				if (type == ENUM_EDITABLE_FIXED)
				{
					editable = EditableType::EditableFixed; editableState = FieldState::OK;
					LOG_TRACE( "unk", "properties", "editable_type=fixed" );
				}
				if (type == ENUM_EDITABLE_CPD_ONLY)
				{
					editable = EditableType::EditableCPDOnly; editableState = FieldState::OK;
					LOG_TRACE( "unk", "properties", "editable_type=cpd_only" );
				}
				if (type == ENUM_EDITABLE_APP_ONLY)
				{
					editable = EditableType::EditableApplicationOnly; editableState = FieldState::OK;
					LOG_TRACE( "unk", "properties", "editable_type=app_only" );
				}
				if (type == ENUM_EDITABLE_CPD_APP)
				{
					editable = EditableType::EditableCPD_Application; editableState = FieldState::OK;
					LOG_TRACE( "unk", "properties", "editable_type=cpd_application" );
				}
			}
		}
		if (name == FIELD_PERSIST_TYPE)
		{
			persistState = FieldState::OutOfRange;
			objPtr = json_object_iter_peek_value( &it );
			if (objPtr == nullptr)
			{
				field->persist = PersistType::Unknown; persistState = FieldState::Undefined;
				LOG_WARN( "unk", "properties", "persist_type=undefined" );
			}
			else
			{
				std::string type = json_object_get_string( objPtr );
				if (type == ENUM_NOT_PERSIST)
				{
					persist = PersistType::NotPersist; persistState = FieldState::OK;
					LOG_TRACE( "unk", "properties", "persist_type=not_persist" );
				}
				if (type == ENUM_PERSIST)
				{
					persist = PersistType::Persist; persistState = FieldState::OK;
					LOG_TRACE( "unk", "properties", "persist_type=persist" );
				}
				if (type == ENUM_HARD_PERSIST)
				{
					persist = PersistType::HardPersist; persistState = FieldState::OK;
					LOG_TRACE( "unk", "properties", "persist_type=hard_persist" );
				}
				if (type == ENUM_REMOVE)
				{
					persist = PersistType::Remove; persistState = FieldState::OK;
					LOG_TRACE( "unk", "properties", "persist_type=remove" );
				}
			}
		}
		if (name == FIELD_SETTING)
		{
			valueState = FieldState::Undefined; /// Not OK until further checks performed.
			objPtr = json_object_iter_peek_value( &it );
			ExtractSimpleField( objPtr, setting );
		}
		if (name == FIELD_DATA_TYPE)
		{
			dataTypeState = FieldState::Undefined; /// Not OK until further checks performed.
			objPtr = json_object_iter_peek_value( &it );
			ExtractSimpleField( objPtr, dataType );
		}
		if (name == FIELD_ES_MIN)
		{
			minState = FieldState::OK;
			objPtr = json_object_iter_peek_value( &it );
			ExtractSimpleField( objPtr, min );
		}
		if (name == FIELD_ES_MAX)
		{
			maxState = FieldState::OK;
			objPtr = json_object_iter_peek_value( &it );
			ExtractSimpleField( objPtr, max );
		}
		if (name == FIELD_BASE_UNIT)
		{
			objPtr = json_object_iter_peek_value( &it );
			ExtractSimpleField( objPtr, baseUnit );
		}
		if (name == FIELD_SCALE_FACTOR)
		{
			objPtr = json_object_iter_peek_value( &it );
			ExtractSimpleField( objPtr, scaleFactor );
		}
		if (name == FIELD_POINTS_PAST_DECIMAL)
		{
			objPtr = json_object_iter_peek_value( &it );
			ExtractFieldVerbatim( objPtr, pointsPastDecimal );
		}
		if (name == FIELD_DISPLAY_DIRECTIVE)
		{
			objPtr = json_object_iter_peek_value( &it );
			ExtractFieldVerbatim( objPtr, displayDirective );
		}
		if (name == FIELD_DISPLAY_MIN_VALUE)
		{
			objPtr = json_object_iter_peek_value( &it );
			ExtractFieldVerbatim( objPtr, displayMinValue );
		}
		if (name == FIELD_DISPLAY_MAX_VALUE)
		{
			objPtr = json_object_iter_peek_value( &it );
			ExtractFieldVerbatim( objPtr, displayMaxValue );
		}

		json_object_iter_next(&it);
		LETS_ASSERT(count < 20);
	}

	/// By now, we should have both the setting and data-type in hand.
	/// Set the data-type-state and value-state to "OK" if they check out.
	CheckValueDataType( dataType, setting, &dataTypeState, &valueState  );

	bool succeeded = false;
	if ((persistState == FieldState::OK) && (persist == PersistType::Remove))
	{
		/// Forego checking anything else because they no longer matter.
		succeeded = true;
	}
	else
	{
		if (valueState != FieldState::OK) cerr << "ExtractProperty: valueState not OK" << endl;
		if (editableState != FieldState::OK) cerr << "ExtractProperty: editableState not OK" << endl;
		if (persistState != FieldState::OK) cerr << "ExtractProperty: persistState not OK" << endl;
		if (dataTypeState != FieldState::OK) cerr << "ExtractProperty: dataTypeState not OK" << endl;
		succeeded = ((valueState == FieldState::OK) &&
					  (editableState == FieldState::OK) &&
					  (persistState == FieldState::OK) &&
					  (dataTypeState == FieldState::OK));
	}
	
	/// Always return the state values. They tell the error-reporting
	/// code what to write. These do not get saved to file.
	field->valueState = valueState;
	field->editableState = editableState;
	field->persistState = persistState;
	field->dataTypeState = dataTypeState;
	field->minState = minState;
	field->maxState = maxState;
	if (succeeded)
	{
		field->setting = setting;
		field->editable = editable;
		field->persist = persist;
		field->dataType = dataType;
		field->min = min;
		field->max = max;
		field->baseUnit = baseUnit;
		field->scale = scaleFactor;
		field->pointsPastDecimal = pointsPastDecimal;
		field->displayDirective = displayDirective;
		field->displayMin = displayMinValue;
		field->displayMax = displayMaxValue;
	}
	else
	{
		stringstream msg;
		msg << "setting=" << setting << " editable=" << EditableTypeEnumToStringName( editable ) << " persist=" << PersistTypeEnumToStringName( persist );
		msg << " dataType=" << dataType << " min=" << min << " max=" << max << " baseUnit=" << baseUnit << " scaleFactor=" << scaleFactor;
		msg << " pointsPastDecimal=" << pointsPastDecimal << " displayDirective=" << displayDirective << " displayMin=" << displayMinValue;
		msg << " displayMax=" << displayMaxValue;
		LOG_ERROR( "unk", "properties", msg.str().c_str() );
	}
	// TODO: Find a way to report errors via Status message.
	return succeeded;
}

int DesHelper::UpdateProperty( json_object* cpdFieldObj, FieldWithMetaData* field, bool fromCPD )
{
	FieldState valueState = (field->setting.length() == 0) ? FieldState::Missing : FieldState::OK;
	FieldState persistState = (field->persist == PersistType::Unknown) ? FieldState::Missing : FieldState::OK;
	FieldState dataTypeState = (field->dataType.length() == 0) ? FieldState::Missing : FieldState::OK;
	
	/// Variables used for stepping through all of the elements of the
	/// json_object.
	struct json_object_iterator it;
	struct json_object_iterator itEnd;
	it = json_object_iter_init_default();
	it = json_object_iter_begin( cpdFieldObj );
	itEnd = json_object_iter_end( cpdFieldObj );
	struct json_object * objPtr = nullptr;
	int count = 0;

	while (!json_object_iter_equal(&it, &itEnd))
	{
		count++;
		std::string name = json_object_iter_peek_name( &it );
		if (name == FIELD_SETTING)
		{
			valueState = FieldState::Undefined; /// Not OK until further checks performed.
			objPtr = json_object_iter_peek_value( &it );
			ExtractSimpleField( objPtr, field->setting );
		}
		if ((name == FIELD_ES_MIN) && fromCPD)
		{
			objPtr = json_object_iter_peek_value( &it );
			ExtractSimpleField( objPtr, field->min );
		}
		if ((name == FIELD_ES_MAX) && fromCPD)
		{
			objPtr = json_object_iter_peek_value( &it );
			ExtractSimpleField( objPtr, field->max );
		}
		json_object_iter_next(&it);
		LETS_ASSERT(count < 20);
	}

	/// By now, we should have both the setting and data-type in hand.
	/// Set the data-type-state and value-state to "OK" if they check out.
	CheckValueDataType( field->dataType, field->setting, &dataTypeState, &valueState  );

	bool succeeded = false;
	if ((persistState == FieldState::OK) && (field->persist == PersistType::Remove))
	{
		/// Forego checking anything else because they no longer matter.
		succeeded = true;
	}
	else
	{
		if (valueState != FieldState::OK) cerr << "ExtractProperty: valueState not OK" << endl;
		if (persistState != FieldState::OK) cerr << "ExtractProperty: persistState not OK" << endl;
		if (dataTypeState != FieldState::OK) cerr << "ExtractProperty: dataTypeState not OK" << endl;
		succeeded = ((valueState == FieldState::OK) &&
					  (persistState == FieldState::OK) &&
					  (dataTypeState == FieldState::OK));
	}
	
	if (!succeeded)
	{
		stringstream msg;
		msg << "setting=" << field->setting << " editable=" << EditableTypeEnumToStringName( field->editable ) << " persist=" << PersistTypeEnumToStringName( field->persist );
		msg << " dataType=" << field->dataType << " min=" << field->min << " max=" << field->max << " baseUnit=" << field->baseUnit << " scaleFactor=" << field->scale;
		msg << " pointsPastDecimal=" << field->pointsPastDecimal << " displayDirective=" << field->displayDirective << " displayMin=" << field->displayMin;
		msg << " displayMax=" << field->displayMax;
		LOG_ERROR( "unk", "properties", msg.str().c_str() );
	}
	// TODO: Find a way to report errors via Status message.
	return succeeded ? STATUS_OK : STATUS_DEVICE_ERROR; // TODO: Pick better failure codes.
}


void DesHelper::CheckValueDataType( std::string& dataType, std::string& value, FieldState* dataTypeState, FieldState* valueState )
{
	if ((*valueState == FieldState::Missing) ||
	    (*dataTypeState == FieldState::Missing))
	{
		LOG_ERROR( "unk", "des-help", "value or dataType couldn't be found" );
		/// EARLY RETURN!
		/// Let the "missing" state speak for itself.
		return;
	}
	if (dataType == STRING)
	{
		/// dataType matched one of its possible values.
		*dataTypeState = FieldState::OK;
		/// A string value is free to be anything, so it is OK by definition.
		*valueState = FieldState::OK;
		return;
	}
	if (dataType == FLOAT)
	{
		*dataTypeState = FieldState::OK;
		/// Check for successful parsing, then discard the number.
		*valueState = CheckStringAsFloat( value );
		return;
	}
	if (dataType == INT16)
	{
		*dataTypeState = FieldState::OK;
		*valueState = CheckStringAsInt16( value );
		return;
	}
	if (dataType == UINT16)
	{
		*dataTypeState = FieldState::OK;
		*valueState = CheckStringAsUint16 ( value );
		return;
	}
	if (dataType == INT32)
	{
		*dataTypeState = FieldState::OK;
		*valueState = CheckStringAsInt32( value );
		return;
	}
	if (dataType == UINT32)
	{
		*dataTypeState = FieldState::OK;
		*valueState = CheckStringAsUint32( value );
		return;
	}
	if (dataType == ENUM_CALIBRATION_TYPE)
	{
		*dataTypeState = FieldState::OK;
		*valueState = CheckStringAsEnumCalType( value );
		return;
	}
	if (dataType == ENUM_WHITE_LIGHT_TYPE)
	{
		*dataTypeState = FieldState::OK;
		*valueState = CheckStringAsWhiteLightType( value );
		return;
	}
	if (dataType == BOOLEAN)
	{
		*dataTypeState = FieldState::OK;
		*valueState = CheckStringAsBoolean( value );
		return;
	}
	if (dataType == ENUM_PUMP_TYPE)
	{
		*dataTypeState = FieldState::OK;
		*valueState = CheckStringAsEnumPumpType( value );
		return;
	}
	{
		stringstream msg;
		msg << "Unknown dataType: " << dataType;
		LOG_ERROR( "unk", "des-help", "parsing problem", msg.str().c_str() );
	}
}


///----------------------------------------------------------------------
/// ComposeAndAddCpdError
/// After a Field-with-metadata is created by ExtractProperty(), it may
/// have errors recorded in its state parameters. Use ComposeAndAddCpdError
/// to interpret them into explanatory strings and append them to
/// errorListObj.
/// ErrorListObj should be a JSON array. All elements in the array
/// are JSON objects with two fields: "cpd_id" and "syntax_error_string".
/// ErrorListObj will go out in the payload of an UploadCPD status
/// message.
///----------------------------------------------------------------------
void DesHelper::ComposeAndAddCpdError( FieldWithMetaData* fieldWithMetaData, std::string cpdId, json_object** errorListObj )
{
	if (fieldWithMetaData->editableState != FieldState::OK)
	{
		string syntaxErrorDescription = "ERROR while generating description";
		if (fieldWithMetaData->editableState == FieldState::Missing)
		{
			syntaxErrorDescription = (std::string( fieldWithMetaData->name ) + "'s 'editable' field is missing");
		}
		if (fieldWithMetaData->editableState == FieldState::OutOfRange)
		{
			syntaxErrorDescription = (std::string( fieldWithMetaData->name ) + "'s 'editable' field is out-of-range");
		}
		if (fieldWithMetaData->editableState == FieldState::Extra)
		{
			syntaxErrorDescription = (std::string( fieldWithMetaData->name ) + "'s 'editable' field is not needed or redundant");
		}
		if (fieldWithMetaData->editableState == FieldState::Undefined)
		{
			syntaxErrorDescription = (std::string( fieldWithMetaData->name ) + "'s 'editable' field is undefined");
		}
		DesHelper::AddCpdError( errorListObj, cpdId.c_str(), syntaxErrorDescription.c_str() );
	}
	if (fieldWithMetaData->persistState != FieldState::OK)
	{
		string syntaxErrorDescription = "ERROR while generating description";
		if (fieldWithMetaData->persistState == FieldState::Missing)
		{
			syntaxErrorDescription = (std::string( fieldWithMetaData->name ) + "'s 'persist' field is missing");
		}
		if (fieldWithMetaData->persistState == FieldState::OutOfRange)
		{
			syntaxErrorDescription = (std::string( fieldWithMetaData->name ) + "'s 'persist' field is out-of-range");
		}
		if (fieldWithMetaData->persistState == FieldState::Extra)
		{
			syntaxErrorDescription = (std::string( fieldWithMetaData->name ) + "'s 'persist' field is not needed or redundant");
		}
		if (fieldWithMetaData->persistState == FieldState::Undefined)
		{
			syntaxErrorDescription = (std::string( fieldWithMetaData->name ) + "'s 'persist' field is undefined");
		}
		DesHelper::AddCpdError( errorListObj, cpdId.c_str(), syntaxErrorDescription.c_str() );
	}
	if (fieldWithMetaData->valueState != FieldState::OK)
	{
		string syntaxErrorDescription = "ERROR while generating description";
		if (fieldWithMetaData->valueState == FieldState::Missing)
		{
			syntaxErrorDescription = (std::string( fieldWithMetaData->name ) + "'s 'setting' field is missing");
		}
		if (fieldWithMetaData->valueState == FieldState::OutOfRange)
		{
			syntaxErrorDescription = (std::string( fieldWithMetaData->name ) + "'s 'setting' field is out-of-range");
		}
		if (fieldWithMetaData->valueState == FieldState::Extra)
		{
			syntaxErrorDescription = (std::string( fieldWithMetaData->name ) + "'s 'setting' field is not needed or redundant");
		}
		if (fieldWithMetaData->valueState == FieldState::Undefined)
		{
			syntaxErrorDescription = (std::string( fieldWithMetaData->name ) + "'s 'setting' field is undefined");
		}
		DesHelper::AddCpdError( errorListObj, cpdId.c_str(), syntaxErrorDescription.c_str() );
	}
}

///----------------------------------------------------------------------
/// A1ToRc
/// Interpret the A1 input. Derive a 0-based column number from the
/// alpha character. Derive a 0-based row number from the numeric value.
///----------------------------------------------------------------------
bool DesHelper::A1ToRc( std::string a1Input, long &rowOutput, long &colOutput )
{
	if (a1Input.length() < 2) return false;		/// EARLY RETURN!

	char alphaChar = a1Input.c_str()[0];
	unsigned int col = 0;
	if ((alphaChar >= 'a') && (alphaChar <= 'z'))
	{
		colOutput = alphaChar - 'a' + 1;
	}
	else if ((alphaChar >= 'A') && (alphaChar <= 'Z'))
	{
		colOutput = alphaChar - 'A' + 1;
	}
	else
	{
		LOG_ERROR( "unk", "xy", "failed to find column" );
		return false;							/// EARLY RETURN!
	}

	string numberString = a1Input.substr(1);
	unsigned int row = StringToULong( numberString );
	if (row < 1)
	{
		LOG_ERROR( "unk", "xy", "failed to find row" );
		return false;					/// EARLY RETURN!
	}
	
	rowOutput = row;
	return true;
}

///----------------------------------------------------------------------
/// StoreComponent_Properties
///----------------------------------------------------------------------
int DesHelper::StoreComponent_Properties( json_object* inputCPD,
										  ComponentProperties* propsStore,
										  json_object** statusPayloadObj )
{
//	cerr << "StoreComponent_Properties entered." << endl;
	struct json_object_iterator it;
	struct json_object_iterator itEnd;
	it = json_object_iter_init_default();
	it = json_object_iter_begin(inputCPD);
	itEnd = json_object_iter_end(inputCPD);
	int count = 0;
	int statusCode = STATUS_OK; // TODO: Update this on errors.
	bool succeeded = true;
	
	while (!json_object_iter_equal(&it, &itEnd))
	{
		count++;
		LETS_ASSERT(count < 200);
		std::string name = json_object_iter_peek_name( &it );
		FieldWithMetaData field = propsStore->GetFieldByName( name.c_str() );
		if (field.name == "")
		{
			/// A header field is not an error, but it produces a null during the name lookup.
			/// Just ignore it.
			if (name == HEADER) { }
			/// Assume that any other null field was purposely added.
			/// We will *not* parse it, because we know nothing about it.
			/// We *will* write it to the CPD file.
			else
			{
				/// Add a new property
				bool extracted = ExtractProperty( json_object_iter_peek_value( &it ), &field );
				if (!extracted)
				{
					LOG_ERROR( "unk", "des-help", "something not extracted" );
					ComposeAndAddCpdError( &field, propsStore->GetFieldByName( CPD_ID ).setting, statusPayloadObj );
				}
				if (extracted)
				{
					cerr << "Field " << field.name << " extracted from json_object, ";
					propsStore->fieldVector.push_back( field );
				}
				succeeded &= extracted;
			}
			cerr << name << " not found in data store." << endl;
		}
		else /// Valid name
		{
			cerr << name << " found in data store." << endl;
			bool extracted = ExtractProperty( json_object_iter_peek_value( &it ), &field );
			if (!extracted)
			{
				LOG_ERROR( "unk", "des-help", "something not extracted" );
				ComposeAndAddCpdError( &field, propsStore->GetFieldByName( CPD_ID ).setting, statusPayloadObj );
			}
			if (extracted)
			{
				cerr << "Field " << field.name << " extracted from json_object, ";
				propsStore->SetField( field );
			}
			succeeded &= extracted;
		}
		json_object_iter_next(&it);
		cerr << endl;
	}
	cerr << "Cpd filename is " << propsStore->filePath << endl;
	std::fstream out( propsStore->filePath.c_str(), std::ios::out );
	{
		if (out.is_open())
		out << (*propsStore);
		out.close();
	}
	cerr << "StoreComponent_Properties exiting." << endl;
	return statusCode;
}

///----------------------------------------------------------------------
/// ExtractPayloadStringValue
///----------------------------------------------------------------------
string DesHelper::ExtractPayloadStringValue( json_object* jWorkOrderObj, string keyName )
{
	string value = "";
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		json_object * valueObj = json_object_object_get( payloadObj, keyName.c_str() );
		if (valueObj == NULL)
		{
			stringstream msg;
			msg << "failed to find " << keyName;
			LOG_WARN( "unk", "des-help", msg.str().c_str() );
		}
		else
		{
			value = json_object_get_string( valueObj );
		}
	}
	return value;	
}

double DesHelper::ExtractPayloadDoubleValue( json_object* jWorkOrderObj, string keyName )
{
	double value = 0.0;
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		json_object * valueObj = json_object_object_get( payloadObj, keyName.c_str() );
		if (valueObj == NULL)
		{
			stringstream msg;
			msg << "failed to find " << keyName;
			LOG_WARN( "unk", "des-help", msg.str().c_str() );
		}
		else
		{
			value = json_object_get_double( valueObj );
		}
	}
	return value;	
}

long DesHelper::ExtractPayloadLongValue( json_object* jWorkOrderObj, string keyName )
{
	long value = 0L;
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		json_object * valueObj = json_object_object_get( payloadObj, keyName.c_str() );
		if (valueObj == NULL)
		{
			stringstream msg;
			msg << "failed to find " << keyName;
			LOG_WARN( "unk", "des-help", msg.str().c_str() );
		}
		else
		{
			value = json_object_get_int64( valueObj );
		}
	}
	return value;	
}

// TODO: Consider changing keyName to a const char *
unsigned long DesHelper::ExtractPayloadULongValue( json_object* jWorkOrderObj, string keyName )
{
	unsigned long value = 0L;
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		json_object * valueObj = json_object_object_get( payloadObj, keyName.c_str() );
		if (valueObj == NULL)
		{
			stringstream msg;
			msg << "failed to find " << keyName;
			LOG_WARN( "unk", "des-help", msg.str().c_str() );
		}
		else
		{
			value = json_object_get_uint64( valueObj );
		}
	}
	return value;	
}

bool DesHelper::ExtractPayloadBooleanValue( json_object* jWorkOrderObj, std::string keyName )
{
	bool value = false;
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		json_object * valueObj = json_object_object_get( payloadObj, keyName.c_str() );
		if (valueObj == NULL)
		{
			stringstream msg;
			msg << "failed to find " << keyName;
			LOG_WARN( "unk", "des-help", msg.str().c_str() );
		}
		else
		{
			value = json_object_get_boolean( valueObj );
		}
	}
	return value;	
}

TipType DesHelper::ExtractPayloadTipType( json_object* jWorkOrderObj )
{
	TipType tt;
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		json_object * valueObj = json_object_object_get( payloadObj, TIP_TYPE );
		if (valueObj == NULL)
		{
			LOG_WARN( "unk", "des-help", "failed to find tip_type" );
		}
		else
		{
			tt.PopulateFromJsonCObject( valueObj );
		}
	}
	return tt;
}

TipType DesHelper::ExtractPayloadTipType( const char * threadName, json_object* jWorkOrderObj, bool * succeeded_out )
{
	TipType tt;
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		json_object * valueObj = json_object_object_get( payloadObj, TIP_TYPE );
		if (valueObj == NULL)
		{
			LOG_ERROR( threadName, "des-help", "failed to find tip_type" );
		}
		else
		{
			*succeeded_out = tt.PopulateFromJsonCObject( threadName, valueObj );
		}
	}
	else
	{
		*succeeded_out = false;
		LOG_ERROR( threadName, "des-help", "failed to find payload" );
	}
	return tt;
}

// TODO: Deprecate and replace with the overloaded version that has more error reporting.
PlateType DesHelper::ExtractPayloadPlateType( json_object* jWorkOrderObj )
{
	PlateType pt;
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		json_object * valueObj = json_object_object_get( payloadObj, PLATE_TYPE );
		if (valueObj == NULL)
		{
			LOG_WARN( "unk", "des-help", "failed to find plate_type" );
		}
		else
		{
			pt.PopulateFromJsonCObject( valueObj );
		}
	}
	return pt;
}

bool DesHelper::ExtractLongValue( const char * threadName, json_object* jObj, const char * keyName, long & value_out )
{
	bool succeeded = true;
	json_object * fieldObj = json_object_object_get( jObj, keyName );
	if (fieldObj)
	{
		errno = 0;
		value_out = json_object_get_int64( fieldObj );
		if (errno != 0)
		{
			succeeded = false;
			stringstream msg;
			msg << "Failed to get " << keyName;
			LOG_ERROR( threadName, "des-help", msg.str().c_str() );
		}
	}
	else
	{
		succeeded = false;
		stringstream msg;
		msg << "Missing " << keyName;
		LOG_ERROR( threadName, "des-help", msg.str().c_str() );
	}
	return succeeded;
}

bool DesHelper::ExtractULongValue( const char * threadName, json_object* jObj, const char * keyName, unsigned long & value_out )
{
	bool succeeded = true;
	json_object * fieldObj = json_object_object_get( jObj, keyName );
	if (fieldObj)
	{
		errno = 0;
		value_out = json_object_get_uint64( fieldObj );
		if (errno != 0)
		{
			succeeded = false;
			stringstream msg;
			msg << "Failed to get " << keyName;
			LOG_ERROR( threadName, "des-help", msg.str().c_str() );
		}
	}
	else
	{
		succeeded = false;
		stringstream msg;
		msg << "Missing " << keyName;
		LOG_ERROR( threadName, "des-help", msg.str().c_str() );
	}
	return succeeded;
}

bool DesHelper::ExtractFloatValue( const char * threadName, json_object* jObj, const char * keyName, float & value_out )
{
	bool succeeded = true;
	json_object * fieldObj = json_object_object_get( jObj, keyName );
	if (fieldObj)
	{
		errno = 0;
		value_out = json_object_get_double( fieldObj );
		if (errno != 0)
		{
			succeeded = false;
			stringstream msg;
			msg << "Failed to get " << keyName;
			LOG_ERROR( threadName, "des-help", msg.str().c_str() );
		}
	}
	else
	{
		succeeded = false;
		stringstream msg;
		msg << "Missing " << keyName;
		LOG_ERROR( threadName, "des-help", msg.str().c_str() );
	}
	return succeeded;
}

// TODO: Try making these a template function.
bool DesHelper::ExtractBoolValue( const char * threadName, json_object* jObj, const char * keyName, bool & value_out )
{
	bool succeeded = true;
	json_object * fieldObj = json_object_object_get( jObj, keyName );
	if (fieldObj)
	{
		errno = 0;
		value_out = json_object_get_boolean( fieldObj );
		if (errno != 0)
		{
			succeeded = false;
			stringstream msg;
			msg << "Failed to get " << keyName;
			LOG_ERROR( threadName, "des-help", msg.str().c_str() );
		}
	}
	else
	{
		succeeded = false;
		stringstream msg;
		msg << "Missing " << keyName;
		LOG_ERROR( threadName, "des-help", msg.str().c_str() );
	}
	return succeeded;
}

bool DesHelper::ExtractStringValue( const char * threadName, json_object* jObj, const char * keyName, std::string & value_out )
{
	bool succeeded = true;
	json_object * fieldObj = json_object_object_get( jObj, keyName );
	if (fieldObj)
	{
		errno = 0;
		value_out = json_object_get_string( fieldObj );
		if (errno != 0)
		{
			succeeded = false;
			stringstream msg;
			msg << "Failed to get " << keyName;
			LOG_ERROR( threadName, "des-help", msg.str().c_str() );
		}
	}
	else
	{
		succeeded = false;
		stringstream msg;
		msg << "Missing " << keyName;
		LOG_ERROR( threadName, "des-help", msg.str().c_str() );
	}
	return succeeded;
}


// TODO: Maybe refactor this to be a method of the ComponentProperties struct
/// Fill the given (presumably empty) collectionOutJsonObj with everthing in collectionIn, turning it
/// into a dictionary. Turn most properties into named objects of property fields. Turn others into
/// named arrays of dictionary objects.
/// Used for GetProperties command.
bool DesHelper::ComponentPropertiesToJsonObject( ComponentProperties * collectionIn, json_object* collectionOutJsonObj )
{
	bool succeeded = true;
	std::unordered_map<std::string,int> arrayMap;
//	cout << "ComponentPropertiesToJsonObject called" << endl;
	
	for (FieldWithMetaData property : collectionIn->fieldVector) {
		if (property.arrayName == "")
		{
			/// This property is not part of an array.
			/// Append it to the outgoing collectionOutJsonObj.
			succeeded &= property.PropertyFieldsToJsonObject(collectionOutJsonObj);
		}
		else
		{
			/// The property is marked as being part of an array.
			/// Delay adding it until later. For now, record it to learn the array size.
			if (arrayMap.count(property.arrayName) == 0)
			{
				arrayMap[property.arrayName] = property.arrayIndex;
			}
			else if (arrayMap[property.arrayName] < property.arrayIndex )
			{
				arrayMap[property.arrayName] = property.arrayIndex;
			}
		}
	}
	
	/// Now process each array.
	for ( auto it = arrayMap.cbegin(); it != arrayMap.cend(); ++it )
	{
//		cout << "An array start ---- [" << endl;
		/// Process one array. (Normally, there are only 0-1 arrays.)
		std::string arrayName = it->first;
		int arraySize = it->second + 1;
		json_object *jArray = json_object_new_array();
		for (int i = 0; i < arraySize; i++)
		{
			/// Add one element of this array. This requires collecting all of its
			/// properties from the collectionIn.
			json_object *jElementObject = json_object_new_object();
			for (FieldWithMetaData property : collectionIn->fieldVector) {
				if ((property.arrayName == arrayName) && (property.arrayIndex == i))
				{
//					cout << "   + one element " << endl;
					/// Add one property to the array-element.
					succeeded &= property.PropertyFieldsToJsonObject(jElementObject);
				}
			}
			/// Append the array-element to the array.
			int result1 = json_object_array_add(jArray, jElementObject);
			if (result1 == 0)
			{
//				cout << "One array element added." << endl;
			}
			else
			{
				LOG_ERROR( "unk", "des-help", "failed to add an array element" );
			}
		}
//		cout << "] --- an array end" << endl;
		/// Append the array to the outgoing object.
		int result2 = json_object_object_add(collectionOutJsonObj, arrayName.c_str(), jArray);
//		cout << "result of adding array is " << result2 << endl;
	}

	return succeeded;
}

/// payloadObj is expected to be an object containing a single named object where the name is property type
/// and the object is a collection of properties.
int DesHelper::CopyValidPropertiesPayloadFromJsonToWorkingPropertiesContainer( json_object * payloadInObj,
																				ComponentProperties * collectionOut,
																				json_object ** statusPayloadOutOjb,
																				ExtractionType extractionType,
																				bool fromCPD )
{
	int statusCode = STATUS_OK;
	struct json_object_iterator payIt;
	payIt = json_object_iter_init_default();
	payIt = json_object_iter_begin( payloadInObj );

	{
		stringstream msg;
		msg << "payload's first element is " << json_object_iter_peek_name(&payIt);
		LOG_TRACE( "unk", "des-help", msg.str().c_str() );
	}
	ComponentType componentType = StringNameToComponentTypeEnum( json_object_iter_peek_name( &payIt ) );
	{
		stringstream msg;
		msg << "testing whether " << componentType << "==" << collectionOut->componentType;
		LOG_TRACE( "unk", "des-help", msg.str().c_str() );
	}
	if (componentType == collectionOut->componentType)
	{
		json_object * propsCollectionObj = json_object_iter_peek_value(&payIt);
		statusCode = CopyValidPropertiesFromJsonToWorkingPropertiesContainer( propsCollectionObj,
																			  collectionOut,
																			  statusPayloadOutOjb,
																			  extractionType,
																			  fromCPD );
	}
	else
	{
		stringstream msg;
		msg << "'" << json_object_iter_peek_name(&payIt) << "' doesn't match '" << ComponentTypeEnumToStringName( collectionOut->componentType ) << "'";
		LOG_ERROR( "unk", "des-help", "parsing problem", msg.str().c_str() );
		statusCode = STATUS_REJECTED;
	}
	return statusCode;
}

// TODO: Maybe refactor this to be a method of the ComponentProperties struct
/// Called by CopyValidPropertiesPayloadFromJsonToWorkingPropertiesContainer as it iterates through the payload.
/// collectionInObj is expected to be an object that contains named property-objects and (optionally) arrays
/// of objects.
/// Objects in arrays are expected to conform with the definition of collectionInObj, so this function may
/// be used recursively. NOT doing regression, at present.
int DesHelper::CopyValidPropertiesFromJsonToWorkingPropertiesContainer( json_object * collectionInObj,
																		  ComponentProperties * collectionOut,
																		  json_object ** statusPayloadOutOjb,
																		  ExtractionType extractionType,
																		  bool fromCPD )
{
	int statusCode = STATUS_OK;
	string name;
	struct json_object_iterator propIt;
	struct json_object_iterator propItEnd;

	propIt = json_object_iter_init_default();
	propIt = json_object_iter_begin(collectionInObj);
	propItEnd = json_object_iter_end(collectionInObj);

	/// Copy properties from the json payload(s) to the working properties in RAM.
	while (!json_object_iter_equal(&propIt, &propItEnd))
	{
		json_type itType = json_object_get_type(json_object_iter_peek_value(&propIt));
		switch (itType)
		{
			case json_type_null: break;
			case json_type_boolean:	break;
			case json_type_double: break;
			case json_type_int: break;
			case json_type_object:
				name = json_object_iter_peek_name(&propIt);
//				cout << "property=" << name << ", " << endl;
				statusCode = SavePropertyObjectToPropertiesCollection( name.c_str(),
																	   json_object_iter_peek_value(&propIt),
																	   collectionOut,
																	   statusPayloadOutOjb,
																	   extractionType,
																	   "", /// arrayName is not applicable here.
																	   0, /// arrayIndex not applicable here.
																	   fromCPD );
				break;
			case json_type_array:
				name = json_object_iter_peek_name(&propIt);
//				cout << "array=" << name << ", " << endl;
				statusCode = SavePropertyArrayToPropertiesCollection( name.c_str(),
																	  json_object_iter_peek_value(&propIt),
																	  collectionOut,
																	  statusPayloadOutOjb,
																	  extractionType,
																	  fromCPD );
				break;
			case json_type_string: break;
		}

		json_object_iter_next(&propIt);
	}
	
	/// Write the modified working properties to file.
	{
		stringstream msg;
		msg << "Saving changes to " << collectionOut->filePath;
		LOG_TRACE( "unk", "set-properties", msg.str().c_str() );
	}
	std::fstream out( collectionOut->filePath, std::ios::out );
	if (out.is_open())
	{
		out << ( *collectionOut );
		out.close();
	}
	return statusCode;
}

// TODO: Maybe refactor this to be a method of the ComponentProperties struct
int DesHelper::SavePropertyArrayToPropertiesCollection( const char * arrayName,
													   json_object * arrayObj,
													   ComponentProperties * propertiesOut,
													   json_object ** statusPayloadOutObj,
													   ExtractionType extractionType,
													   bool fromCPD )
{
	LOG_TRACE( "unk", "set-properties", "-" );
	int statusCode = STATUS_OK;
	size_t array_len = json_object_array_length( arrayObj );
	size_t ii;
	string name = "";
	struct json_object_iterator propIt;
	struct json_object_iterator propItEnd;

	propIt = json_object_iter_init_default();
	for (ii = 0; ii < array_len; ii++)
	{
		json_object *child = json_object_array_get_idx( arrayObj, ii );
		propIt = json_object_iter_begin(child);
		propItEnd = json_object_iter_end(child);
		while (!json_object_iter_equal(&propIt, &propItEnd))
		{
			name = json_object_iter_peek_name( &propIt );
//			cout << "property=" << name << ", " << endl;
			statusCode = SavePropertyObjectToPropertiesCollection( name.c_str(),
																   json_object_iter_peek_value(&propIt),
																   propertiesOut,
																   statusPayloadOutObj,
																   extractionType,
																   arrayName,
																   ii,
																   fromCPD );
			json_object_iter_next(&propIt);
		}
	}
	return statusCode;
}


// TODO: Maybe refactor this to be a method of the ComponentProperties and/or FieldWithMetaData structs
/// Called by Set<comp>Properties commands. Therefore, we assume these changes always came from an APPLICATION.
int DesHelper::SavePropertyObjectToPropertiesCollection( const char * propertyName,
														 json_object * propertyObj,
														 ComponentProperties * propertiesOut,
														 json_object ** statusPayloadOutObj,
														 ExtractionType extractionType,
														 const char * arrayName,
														 int arrayIndex,
														 bool fromCPD )
{
	LOG_TRACE( "unk", "properties", "-" );
	int statusCode = STATUS_OK;
	/// Get a copy from the data store, if one exists.
	std::vector<FieldWithMetaData>::iterator theOldOne;
	std::vector<FieldWithMetaData>::iterator it;
	bool oldOneFound = false;
	for ( it = propertiesOut->fieldVector.begin(); ((!oldOneFound) && (it != propertiesOut->fieldVector.end())); ++it)
	{
		if (it->name == propertyName)
		{
			theOldOne = it;
			oldOneFound = true;
		}
	}

	/// Get a copy from the incoming JSON.
	FieldWithMetaData property_w_changes = { {""},
											EditableType::Unknown,
											PersistType::Unknown,
											{""},
											{""},
											{""},
											{""},
											{""},
											{""},
											{""},
											{""},
											{""},
											{""},
											{arrayName},
											{arrayIndex} };
	if (oldOneFound)
	{
		/// Use the copy constructor. If propertyObj only contains some fields,
		/// The other fields will be preserved.
		property_w_changes = FieldWithMetaData( *theOldOne );
	}

	bool extracted = false;
	if (oldOneFound)
	{
		/// Use a minimalist approach because the property probably only contains one or two fields.
		statusCode = UpdateProperty( propertyObj, &property_w_changes, fromCPD );
		extracted = (statusCode == STATUS_OK);
	}
	else
	{
		/// Use a full approach, which assumes all fields should be present and treats missing fields as errors.
		extracted = ExtractProperty( propertyObj, &property_w_changes );
	}
	property_w_changes.name = propertyName;
	if (!extracted)
	{
		stringstream msg;
		msg << "Property " << propertyName << " not extracted";
		LOG_TRACE( "unk", "properties", msg.str().c_str() );
		if ( (extractionType == ExtractionType::UploadCpd) && (statusPayloadOutObj != nullptr) )
		{
			ComposeAndAddCpdError( &property_w_changes, propertiesOut->GetFieldByName( CPD_ID ).setting, statusPayloadOutObj );
		}
	}
	else /// extracted
	{
		stringstream msg;
		msg << "Property: " << property_w_changes.name << ", " << property_w_changes.setting << ", " << property_w_changes.dataType << ", " << property_w_changes.min << ", " << property_w_changes.max << " extracted from json_object";
		LOG_TRACE( "unk", "properties", msg.str().c_str() );
	}
	if (!extracted)
	{
		statusCode = STATUS_DEVICE_ERROR;
	}
	
	if (!oldOneFound) /// The name was not found in the data store.
	{
		stringstream msg;
		msg << "DesHelper: adding " << propertyName << endl;
		LOG_DEBUG( "unk", "properties", msg.str().c_str() );
		/// Add a new property.
		/// Apply rules.
		/// Rule: An added parameter may not HardPersist.
		if (property_w_changes.persist == PersistType::HardPersist)
		{
			/// Reduce it to merely Persist and save it anyway.
			LOG_TRACE( "unk", "properties", "resisting change" );
			property_w_changes.persist = PersistType::Persist;
		}
		/// Rule? An added parameter marked Remove makes no sense, so don't save it.
		if (property_w_changes.persist != PersistType::Remove)
		{
			if (extractionType == ExtractionType::UploadCpd)
			{
				/// Rule: fields from UploadCPD are only saved if their editable types permit it?
				if  ( (property_w_changes.editable == EditableType::EditableCPDOnly) ||
					  (property_w_changes.editable == EditableType::EditableCPD_Application) )
				{
					stringstream msg;
					msg << "Adding new CPD property " << propertyName;
					LOG_TRACE( "unk", "properties", msg.str().c_str() );
					propertiesOut->fieldVector.push_back(property_w_changes);
				}
			}
			if (extractionType == ExtractionType::SetProperties)
			{
				/// fields from SetProperties (ie. application) are only saved if their editable types permit it?
				if ( (property_w_changes.editable == EditableType::EditableApplicationOnly) ||
					 (property_w_changes.editable == EditableType::EditableCPD_Application) )
				{
					stringstream msg;
					msg << "Setting property " << propertyName;
					LOG_TRACE( "unk", "properties", msg.str().c_str() );
					propertiesOut->fieldVector.push_back(property_w_changes);
				}
			}
		}
	}
	else /// An old one WAS found in the working properties collection.
	{
		/// Update a property.
		/// Apply rules.
		/// Rule: old persist value of HardPersist may not be changed.
		if (theOldOne->persist == PersistType::HardPersist)
		{
			/// HardPersist indicates it is a base property and must resist some changes.
			property_w_changes.persist = PersistType::HardPersist;
			property_w_changes.editable = theOldOne->editable;
			LOG_TRACE( "unk", "properties", "resisting change" );
		}
		/// Rule: If property_w_changes.persist is still Remove, try it now.
		if (property_w_changes.persist == PersistType::Remove)
		{
			if (extractionType == ExtractionType::UploadCpd)
			{
				/// Rule: fields from UploadCPD are only saved if their editable types permit it?
				if  ( (property_w_changes.editable == EditableType::EditableCPDOnly) ||
					  (property_w_changes.editable == EditableType::EditableCPD_Application) )
				{
					stringstream msg;
					msg << "deleting property " << propertyName;
					LOG_TRACE( "unk", "properties", msg.str().c_str() );
					propertiesOut->fieldVector.erase( theOldOne );
				}
			}
			if (extractionType == ExtractionType::SetProperties)
			{
				/// fields from SetProperties (ie. application) are only saved if their editable types permit it?
				if ( (property_w_changes.editable == EditableType::EditableApplicationOnly) ||
					 (property_w_changes.editable == EditableType::EditableCPD_Application) )
				{
					stringstream msg;
					msg << "deleting property " << propertyName;
					LOG_TRACE( "unk", "properties", msg.str().c_str() );
					propertiesOut->fieldVector.erase( theOldOne );
				}
			}
		}
		/// Rule: The application may lock itself out of future edits.
		/// Rule: Changes may only be applied if the old editable type allows an application to change it.
		else 
		{
			if (extractionType == ExtractionType::UploadCpd)
			{
				/// Rule: fields from UploadCPD are only saved if their editable types permit it?
				if  ( (property_w_changes.editable == EditableType::EditableCPDOnly) ||
					  (property_w_changes.editable == EditableType::EditableCPD_Application) )
				{
					stringstream msg;
					msg << "applying CPD property " << propertyName;
					LOG_TRACE( "unk", "properties", msg.str().c_str() );
					propertiesOut->fieldVector.erase( theOldOne );
					propertiesOut->fieldVector.push_back(property_w_changes);
				}
			}
			if (extractionType == ExtractionType::SetProperties)
			{
				/// fields from SetProperties (ie. application) are only saved if their editable types permit it?
				if ( (property_w_changes.editable == EditableType::EditableApplicationOnly) ||
					 (property_w_changes.editable == EditableType::EditableCPD_Application) )
				{
					stringstream msg;
					msg << "SETting property " << propertyName;
					LOG_TRACE( "unk", "properties", msg.str().c_str() );
					propertiesOut->fieldVector.erase( theOldOne );
					propertiesOut->fieldVector.push_back(property_w_changes);
				}
			}
		}
	}
	return statusCode;
}

bool DesHelper::IsTecanAdpDetect( const std::string vendor, const std::string model )
{
	string pumpVendor = vendor; /// Make an editable copy.
	string pumpModel = model;
	string TecanADP_vendor = "tecan"; /// The definitive test, in lower case.
	string TecanADP_model = "adp detect";

	std::transform( pumpVendor.begin(), pumpVendor.end(), pumpVendor.begin(), (int(*)(int)) std::tolower );
	std::transform( pumpModel.begin(), pumpModel.end(), pumpModel.begin(), (int(*)(int)) std::tolower );
	return ((TecanADP_vendor.compare( pumpVendor ) == 0) && (TecanADP_model.compare( pumpModel ) == 0 ));
}

