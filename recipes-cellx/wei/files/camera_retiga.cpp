
#include <stdio.h> // for SayHi() and SingleImage_callbacks()
#include <string.h> // for memcpy()
#include <sstream>
#include <chrono>
#include <thread>

#include "camera_retiga.hpp"
#include "StringFormat.h"
#include "PVCam_helper.h"

using namespace std;

Camera_Retiga::Camera_Retiga() : ES_CameraBase()
{
	PVCamera m_pvCamera;
}

Camera_Retiga::~Camera_Retiga()
{
}

/// To be called at start of runtime to initialize drivers supplied by camera manufacturers.
int Camera_Retiga::InitDriver( std::string &errorMsg )
{
	return PVCamera::InitDriver( errorMsg );
}

int Camera_Retiga::GetVersion( std::string &msg )
{
	return PVCamera::GetVersion( msg );
}

int Camera_Retiga::GetCameras( std::vector<ES_CameraBase *> &cameraList, std::string &msg )
{
	short numCameras = 0;
	// Add one camera to the vector for every camera found.
	if (PV_OK == pl_cam_get_total(&numCameras))
	{
		stringstream msgtxt;
		msgtxt << "Number of cameras=" << numCameras;
		msg += msgtxt.str();
	}

	for (int i = 0; i < numCameras; i++)
	{
		short camHandle = i;
		char camName[256];
		if (PV_OK == pl_cam_get_name (camHandle, camName))
		{
			/// Create and initalize the camera object.
			Camera_Retiga * camera_i = new Camera_Retiga();
			camera_i->Initialize( msg, camName, camHandle );
			cameraList.push_back( (ES_CameraBase *) camera_i );
		}
	}
	return numCameras;
}

/// To be called at end of runtime to tear down drivers supplied by camera manufacturers.
/// Returns 0 if successful.
/// Returns -1 if failed.
bool Camera_Retiga::TerminateDriver( std::vector<ES_CameraBase *> &cameraList, std::string &msg )
{
	for (int i = 0; i < cameraList.size(); i++)
	{
		cameraList[i]->Release( msg );
	}

	return PVCamera::TerminateDriver( msg );
}

void Camera_Retiga::Initialize( std::string &msg, char *camName, const short &camHandle )
{
	m_pvCamera.Initialize(  msg,  camName,  camHandle );
}

void Camera_Retiga::Release( std::string &msg )
{
	m_pvCamera.Release( msg );
}

size_t Camera_Retiga::GetFrameSize( std::string &msg )
{
	return m_pvCamera.GetFrameSize( msg );
}

bool Camera_Retiga::GetFrame (unsigned short *bufferOut, std::string &msgOut)
{
	return m_pvCamera.GetFrame( bufferOut, msgOut );
}

int Camera_Retiga::GetErrorState (std::string &msg)
{
	return m_pvCamera.GetErrorState( msg );
}

void Camera_Retiga::SetFrameWidth(const unsigned int &width)
{
	m_pvCamera.SetFrameWidth( width );
}

void Camera_Retiga::SetFrameHeight(const unsigned int &height)
{
	m_pvCamera.SetFrameHeight( height );
}

void Camera_Retiga::SetExposureTime(const unsigned long &expTime_us)
{
	m_pvCamera.SetExposureTime( expTime_us );
}

void Camera_Retiga::SetGain (std::string &msg, const int &gainId)
{
	m_pvCamera.SetGain( msg, gainId );
}

void Camera_Retiga::SetBinFactor(const int &binX, const int &binY)
{
	m_pvCamera.SetBinFactor( binX, binY );
}
/*
void Camera_Retiga::SetTiming (std::string &msg, const TIMING &t)
{
	m_pvCamera.SetTiming( msg, t );
}

void Camera_Retiga::CameraInfo (std::string &msg)
{
	m_pvCamera.CameraInfo( msg );
}

void Camera_Retiga::CreateSpeedTable (std::string &msg)
{
	m_pvCamera.CreateSpeedTable( msg );
}

bool Camera_Retiga::IsParameterAvailable (std::string &msg, unsigned int parameterID, const char *parameterName)
{
	return m_pvCamera.IsParameterAvailable( msg, parameterID, parameterName );
}
*/
