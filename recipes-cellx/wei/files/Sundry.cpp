///----------------------------------------------------------------------
/// Sundry
/// This is a catch-all file for anything that does not yet warrant
/// its own file.
///----------------------------------------------------------------------
#include <iostream>
#include <string>
#include <cstring>
#include <sstream>
#include <chrono>
#include <type_traits>
#include <stdlib.h>     /// std::getenv
#include <cctype> 		/// std::tolower
#include <sys/stat.h>	/// stat
#include <time.h>
#include <chrono>
#include "Sundry.hpp"

using namespace std;
using namespace std::chrono;

bool loggersInitialized = false;
std::shared_ptr<spdlog::logger> errorLogger = nullptr; 
SeverityType g_suppressLoggingAtThisLevelAndBelow = SeverityType::Undefined;

// See https://stackoverflow.com/questions/14315497/how-to-get-current-date-time-of-the-system-in-seconds/47880382
std::chrono::system_clock::rep TimeSinceEpoch()
{
    static_assert(
        std::is_integral<std::chrono::system_clock::rep>::value,
        "Representation of ticks isn't an integral value."
    );
    auto now = std::chrono::system_clock::now().time_since_epoch();
    return std::chrono::duration_cast<std::chrono::seconds>(now).count();
}

// 2023-04-18 08:00:37.6934
std::string NowAsYyyyMnDdHhMmSs()
{
	system_clock::time_point now = system_clock::now();
	timespec alsoNow;
	int rc = clock_gettime( CLOCK_REALTIME, &alsoNow );
	long mils = alsoNow.tv_nsec / _1MEG_L;
	time_t nowAsTime_t = system_clock::to_time_t( now );
	std::tm* t = std::gmtime( &nowAsTime_t );
	std::stringstream nowAsStrStream;
	nowAsStrStream << (1900 + t->tm_year) << "-";
	if (t->tm_mon < 9) nowAsStrStream << "0";
	nowAsStrStream << (1 + t->tm_mon) << "-";
	if (t->tm_mday < 10) nowAsStrStream << "0";
	nowAsStrStream << t->tm_mday << " ";
	if (t->tm_hour < 10) nowAsStrStream << "0";
	nowAsStrStream << t->tm_hour << ":";
	if (t->tm_min < 10) nowAsStrStream << "0";
	nowAsStrStream << t->tm_min << ":";
	if (t->tm_sec < 10) nowAsStrStream << "0";
	nowAsStrStream << t->tm_sec << ".";
	if (mils < 10) nowAsStrStream << "0";
	if (mils < 100) nowAsStrStream << "0";
	nowAsStrStream << mils;
	return nowAsStrStream.str();
}

///----------------------------------------------------------------------
/// String-handling section
///----------------------------------------------------------------------
/// ReplaceTopicChannel: old-device + "/" + old-channel -> old-device + "/" + NEW-channel
std::string ReplaceTopicChannel(std::string topic, std::string channel)
{
	/// Note: we are relying on topic being passed by value, so we can
	/// change it during this process.
	std::string delimiter = "/";
	std::string base = "";

	size_t pos = 0;
	std::string part;
	while ((pos = topic.find(delimiter)) != std::string::npos)
	{
		part = topic.substr(0, pos);
		base = base + part + delimiter;
		topic.erase(0, pos + delimiter.size());
	}
	/// At this point, base contains the entire old-device + "/"
	return base + channel;
}

/// ReplaceTopicDevice: old-device + "/" + old-channel -> NEW-device + "/" + old-channel
std::string ReplaceTopicDevice(std::string topic, std::string device)
{
	std::string delimiter = "/";
	std::string base = "";

	size_t pos = 0;
	std::string part;
	while ((pos = topic.find(delimiter)) != std::string::npos)
	{
		part = topic.substr(0, pos);
		base = base + part + delimiter;
		topic.erase(0, pos + delimiter.size());
	}
	/// At this point, topic has been winnowed down to just the old channel.
	return device + delimiter + topic;
}

/// old-device + "/" + old-channel -> old-channel
std::string ExtractChannel(std::string topic)
{
	std::string delimiter = "/";
	std::string base = "";

	size_t pos = 0;
	while ((pos = topic.find(delimiter)) != std::string::npos)
	{
		topic.erase(0, pos + delimiter.size());
	}
	/// At this point, topic has been winnowed down to just the old channel.
	return topic;
}

/// Use a system call to detect the host's private IP address.
std::string DetectHostIpAddress( void )
{
    char getIpAddressCommand[] = "ip -4 route get 8.8.8.8 | awk '{print $7}' | tr -d '\n'";
    FILE *fp = popen( getIpAddressCommand, "r" );
    char ipAddr[50] = {0};
    int charsRead = fscanf(fp, "%s", ipAddr);
    fclose( fp );
    string hostAddress = ipAddr;
    return hostAddress;
}

long StringToLong( std::string str )
{
	long retval = 0L;
	try
	{
		retval = stol( str, nullptr, 10 );
	}
	catch ( const std::invalid_argument& e )
	{
		stringstream msg;
		msg << "invalid_argument exception: str=" << str;
		LOG_ERROR( "unk", "sundry", msg.str().c_str() );
		LETS_ASSERT(false);
	}
	catch ( const std::out_of_range& e )
	{
		stringstream msg;
		msg << "out_of_range exception: str=" << str;
		LOG_ERROR( "unk", "sundry", msg.str().c_str() );
		LETS_ASSERT(false);
	}
	return retval;
}

unsigned long StringToULong( std::string str )
{
	unsigned long retval = 0L;
	try
	{
		retval = stoul( str, nullptr, 10 );
	}
	catch ( const std::invalid_argument& e )
	{
		stringstream msg;
		msg << "invalid_argument exception: str=" << str;
		LOG_ERROR( "unk", "sundry", msg.str().c_str() );
		LETS_ASSERT(false);
	}
	catch ( const std::out_of_range& e )
	{
		stringstream msg;
		msg << "out_of_range exception: str=" << str;
		LOG_ERROR( "unk", "sundry", msg.str().c_str() );
		LETS_ASSERT(false);
	}
	return retval;
}

float StringToFloat( std::string str )
{
	float retval = strtof( str.c_str(), nullptr );
	LETS_ASSERT(errno != ERANGE);
	return retval;
}

void RemoveBrackets( std::string & str )
{
	size_t len = str.length();
	if (len > 1)
	{
		if ((str.at(0) == '[') && (str.at(len-1) == ']'))
		{
			str = str.substr( 1, len-2 );
		}
	}
}


///----------------------------------------------------------------------
/// Logging section
///----------------------------------------------------------------------
void StrictlyAssert(const char* fileName, const int lineNumber, bool assertion)
{
	if (false == assertion)
	{
		cout << "Assert failed in file " << fileName << ", line " << lineNumber << endl;
		if (loggersInitialized)
		{
			// Note: The spdlog library uses a Python-style string format.
			// (See https://fmt.dev/5.0.0/syntax.html for details.)
			// But I haven't been able to get it to work
			// errorLogger->error( "Assertion failed in {0} at line {1.d}.", fileName, lineNumber);
			stringstream errorText;
			errorText << "Assertion failed in " << fileName << " at line " << lineNumber;
			errorLogger->error( errorText.str().c_str() );
		}
		while (true)
		{ }
	}
}

void LenientlyAssert(const char* fileName, const int lineNumber, bool assertion)
{
	if (false == assertion)
	{
		cout << "Assert failed in file " << fileName << ", line " << lineNumber << endl;
		if (loggersInitialized)
		{
//			cout << "Attempting to log" << endl;
			stringstream errorText;
			errorText << "Assertion failed in " << fileName << " at line " << lineNumber;
			errorLogger->error( errorText.str().c_str() );
		}
	}
}

// ##,123848.29,"program","thread","optional_subsystem",ERROR,"Can't find the camera",_func_,__FILE__,__LINE__,(varargs...)
void LogAnything(	const char* programName,
					const char* threadName,
					const char* subsystemName,
					const char* severityName,
					const char* message,
					const char* funcName,
					const char* srcFileName,
					int srcLineNumber,
					SeverityType enumSeverity,
					std::string optional )
{
	if (enumSeverity > g_suppressLoggingAtThisLevelAndBelow)
	{
		stringstream logLine;
//		std::chrono::system_clock::rep timeInSec = TimeSinceEpoch();
		logLine << "##," << NowAsYyyyMnDdHhMmSs() << "," << programName << "," << threadName << "," << subsystemName << ",";
		logLine << severityName << "," << message << "," << funcName << "," << srcFileName << "," << srcLineNumber;
		if (optional.size() > 0)
		{
			logLine << "," << optional;
		}
		logLine << endl;

		cout << logLine.str();
	}
}

void SetLogSuppressionSeverity( std::string severityToSuppress )
{
	cout << "SetLogSuppressionSeverity: severityToSuppress=" << severityToSuppress << endl;
	if (severityToSuppress.size() == 0)
	{
		g_suppressLoggingAtThisLevelAndBelow = 	SeverityType::Undefined;
		return;
	}
	std::transform( severityToSuppress.begin(), severityToSuppress.end(), severityToSuppress.begin(), (int(*)(int)) std::tolower );
	if (severityToSuppress == "trace")
	{
		cout << "SetLogSuppressionSeverity: g_suppressLoggingAtThisLevelAndBelow=SeverityType::Trace" << endl;
		g_suppressLoggingAtThisLevelAndBelow = SeverityType::Trace;
		return;
	}
	if (severityToSuppress == "debug")
	{
		g_suppressLoggingAtThisLevelAndBelow = SeverityType::Debug;
		return;
	}
	if (severityToSuppress == "info")
	{
		g_suppressLoggingAtThisLevelAndBelow = SeverityType::Info;
		return;
	}
	if (severityToSuppress == "warn")
	{
		g_suppressLoggingAtThisLevelAndBelow = SeverityType::Warn;
		return;
	}
	if (severityToSuppress == "error")
	{
		g_suppressLoggingAtThisLevelAndBelow = SeverityType::Error;
		return;
	}
	if (severityToSuppress == "fatal")
	{
		g_suppressLoggingAtThisLevelAndBelow = SeverityType::Fatal;
		return;
	}
	/// Interpret anything else as undefined.
	g_suppressLoggingAtThisLevelAndBelow = 	SeverityType::Undefined;
}

/* Maybe get variadic params working, if needed.
template<typename Arg>
std::string ConcatParameters(Arg param)
{
	std::string a = param;
	return a;
}

template<typename... Arguments>
std::string ConcatParameters(Arguments... parameters)
{

	//stringstream SundryArgumentSum;
	//int numParams = sizeof...(Arguments);
	//if (numParams > 0)
	//{
		//using commonType = std::common_type_t<Arguments...>;
		//auto tempArray = std::array<commonType, sizeof...(Arguments)>{std::forward<commonType>(parameters)...};
		//for (int i = 0; i < numParams; i++)
		//{
			//SundryArgumentSum << "," << tempArray[i];
		//}
	//}
	//return SundryArgumentSum.str();

	ConcatParameters2(parameters...);
	std::string a = "";
	return a;
}
*/

std::string ConcatParameters(const char * param)
{
	std::string a = param;
	return a;
}

std::string ConcatParameters()
{
	std::string a = "";
	return a;
}
	
bool InitializeAllLogs()
{
	auto max_size = 1048576 * 5;
	auto max_files = 3;
	try
	{
		errorLogger = spdlog::rotating_logger_mt( "es_errors", "logs/rotating.txt", max_size, max_files);
	}
	catch (const spdlog::spdlog_ex &ex)
	{
		cerr << "Log init failed" << ex.what() << endl;
	}
	/// Caution: The following line remains safe to use if all loggers are thread safe ("_mt" loggers).
	spdlog::flush_every(std::chrono::seconds(5));
	return true;
}


///----------------------------------------------------------------------
/// Envrionment variables section
/// Copied from user2100815's post in
/// https://stackoverflow.com/questions/5866134/how-to-read-linux-environment-variables-in-c
///----------------------------------------------------------------------
std::string GetEnv( const std::string & var )
{
     const char * val = std::getenv( var.c_str() );
     if ( val == nullptr ) { // invalid to assign nullptr to std::string
         return "";
     }
     else {
         return val;
     }
}

///----------------------------------------------------------------------
/// General section
///----------------------------------------------------------------------
void InitializeSundry()
{
	loggersInitialized = InitializeAllLogs();
}

bool FileExists( const std::string& name )
{
	struct stat buffer;
	return ( stat( name.c_str(), &buffer ) == 0 );
}

///----------------------------------------------------------------------
/// Ripped from the pages of Stack Overflow
/// https://stackoverflow.com/questions/5840148/how-can-i-get-a-files-size-in-c
///----------------------------------------------------------------------
long GetFileSize( const std::string & filepath )
{
    struct stat stat_buf;
    int rc = stat(filepath.c_str(), &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}
