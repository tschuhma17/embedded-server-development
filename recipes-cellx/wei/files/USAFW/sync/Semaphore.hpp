/* USAFW/sync/Semaphore.hpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#ifndef __USAFW_sync_Semaphore_hpp__
#define __USAFW_sync_Semaphore_hpp__

#include <cstdint>
#include <string>

#include "USAFW/details.h"
#include "USAFW/sizes.h"

#ifdef USAFW_SEMAPHORE_TYPE_POSIX
#include <semaphore.h>
#endif

#include "USAFW/debugging/Debuggable.hpp"
#include "USAFW/exceptions/LogicExceptions.hpp"
#include "USAFW/timing/timeout.h"

namespace USAFW {

#if 0
    class Semaphore : public Debuggable {
    public:
        // Constructor.  Takes a descriptive name, which will be abbreviated
        // as needed by platform memory limitations (via configuration headers).
        // A maximum count is expected; if this needs to be higher than 255,
        // revise this class to use uint32_t types.
        Semaphore(std::string zName, uint8_t maxCountP);

        // Destructor.
        ~Semaphore();

        // Blocking wait for the semaphore to become signaled, with no timeout.
        // THROWS
        //    FIXME
        void wait(void);

        // Wait for the semaphore to become signaled, with a timeout.
        // THROWS
        //    TimeoutExpiredException
        void timedWait(usafw_timeout_t timeoutP);

        // Checks to see if a wait would immediately succeed.  If it would, the
        // semaphore is waited on and consumed (the semaphore is locked), and the
        // function returns true; if it would not, the function returns false.
        //
        // NOTE: This function first checks the internal tracking count, returing
        // false immediately if it is zero.  If it is greater than zero, it 
        // attempts a timedWait operation with zero-length timeout.  If that fails,
        // the function returns false; if it succeeds, the function returns true.
        bool tryWait(void);

    protected:
        // Semaphore name.  Useful for debugging and performance monitoring.
        const char m_name[USAFW_SHORT_STRMAX];

        // Maximum signal count.  NOTE: 8-bit value for now.
        uint8_t m_max_count;

        // Tracking count for signaling.  0 means unsignaled.
        uint8_t m_count;


        // FOOTER GUARD REGION.
        // MUST BE LAST DATA MEMBER!
        DECLARE_GUARD_FOOTER
    };
#endif
    
    class Semaphore : public Debuggable{
    public:
        Semaphore(std::string zName, uint8_t maxCountP);
        ~Semaphore();

    protected:
        // Semaphore name.  Useful for debugging and performance monitoring.
        char m_name[USAFW_SHORT_STRMAX];     

        // Tracking count for signaling.  0 means unsignaled.
        uint8_t m_count;

#ifdef USAFW_SEMAPHORE_TYPE_POSIX
        // Actual underlying POSIX platform semaphore object.
        sem_t   m_semaphore;
#endif
    };

}

#endif

