/* src/USAFW/exceptions/SecurityExceptions.cpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#include "USAFW/exceptions/SecurityExceptions.hpp"

namespace USAFW{

    /* == SECURITY EXCEPTION MIDCLASS == */
    SecurityException::SecurityException(const char *fileP, const char *functionP, int lineP) :
        InstrumentedException(fileP, functionP, lineP)
    {
        ;
    }

    SecurityException::~SecurityException() {
        ;
    }

    std::string SecurityException::getTypeDesc(void) const {
        return std::string("SecurityException");
    }

    std::string SecurityException::getEventDesc(void) const {
        return std::string("A security exception has occurred.");
    }


    /* == GUARD REGION INTEGRITY CHECK ATTEMPT ON A NULL POINTER == */
    NullInspectionTargetException::NullInspectionTargetException(const char *fileP, const char *functionP, int lineP) :
        SecurityException(fileP, functionP, lineP)
    {
        ;
    }

    NullInspectionTargetException::~NullInspectionTargetException() {
        ;
    }

    std::string NullInspectionTargetException::getTypeDesc(void) const {
        return std::string("NullInspectionTargetException");
    }

    std::string NullInspectionTargetException::getEventDesc(void) const {
        return std::string("An integrity check was attempted against a NULL pointer.");
    }

    /* == GUARD REGION INTEGRITY CHECK FAILED== */
    IntegrityCheckFailedException::IntegrityCheckFailedException(const char *fileP, const char *functionP, int lineP) :
        SecurityException(fileP, functionP, lineP)
    {
        ;
    }

    IntegrityCheckFailedException::~IntegrityCheckFailedException() {
        ;
    }

    std::string IntegrityCheckFailedException::getTypeDesc(void) const {
        return std::string("IntegrityCheckFailedException");
    }

    std::string IntegrityCheckFailedException::getEventDesc(void) const {
        return std::string("A guard region memory integrity check failed, probably due to buffer overrun or underrun, or attack.");
    }

}


