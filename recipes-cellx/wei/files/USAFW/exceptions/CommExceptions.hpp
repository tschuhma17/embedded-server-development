/* USAFW/exceptions/CommExceptions.hpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#ifndef __CommExceptions_hpp__
#define __CommExceptions_hpp__

#include "USAFW/exceptions/InstrumentedException.hpp"

namespace USAFW {

    // The operation failed because the connection is already open.
    class CommunicationsException : public InstrumentedException {
    public:
        // Constructor
        CommunicationsException(const char *fileP, const char *functionP, int LineP);

        // Destructor
        virtual ~CommunicationsException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };

    // The operation failed because the connection is already open.
    class ConnectionAlreadyOpenException : public CommunicationsException {
    public:
        // Constructor
        ConnectionAlreadyOpenException(const char *fileP, const char *functionP, int LineP);

        // Destructor
        virtual ~ConnectionAlreadyOpenException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };


    // The operation failed because the connection is either not open, or has closed
    // unexpectedly.
    class ConnectionClosedException : public CommunicationsException {
    public:
        // Constructor
        ConnectionClosedException(const char *fileP, const char *functionP, int LineP);

        // Destructor
        virtual ~ConnectionClosedException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };


    // The connection could not be made.
    class ConnectionFailedException : public CommunicationsException {
    public:
        // Constructor
        ConnectionFailedException(const char *fileP, const char *functionP, int LineP);

        // Destructor
        virtual ~ConnectionFailedException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };


    // The network link is down (media unplugged, etc.).
    class LinkDownException : public CommunicationsException {
    public:
        // Constructor
        LinkDownException(const char *fileP, const char *functionP, int LineP);

        // Destructor
        virtual ~LinkDownException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };


    // The network link is down (media unplugged, etc.).
    class NameResolutionFailedException : public CommunicationsException {
    public:
        // Constructor
        NameResolutionFailedException(const char *fileP, const char *functionP, int LineP);

        // Destructor
        virtual ~NameResolutionFailedException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };


    // The link class in use does not support the transport type requested.
    // For example, TCPLink::open("udp://localhost:9000") will fail this way.
    class UnsupportedTransportException : public CommunicationsException {
    public:
        // Constructor
        UnsupportedTransportException(const char *fileP, const char *functionP, int LineP);

        // Destructor
        virtual ~UnsupportedTransportException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };

}

#endif
