/* USAFW/exceptions/LogicExceptions.hpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#ifndef __LogicExceptions_hpp__
#define __LogicExceptions_hpp__

#include "USAFW/exceptions/InstrumentedException.hpp"

namespace USAFW {

    // This exception is thrown when an operation in one thread realizes that the object
    // is currently being destroyed in another thread.  If thrown, it indicates a problem
    // in a higher-level shutdown and destruction sequence.
    class DestructionUnderwayException : public InstrumentedException {
    public:
        // Constructor
        DestructionUnderwayException(const char *fileP, const char *functionP, int LineP);

        // Destructor
        virtual ~DestructionUnderwayException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };

    // The parameter is not valid.  It may be out of range, incorrectly formatted, etc.
    class InvalidParameterException : public InstrumentedException {
    public:
        // Constructor
        InvalidParameterException(const char *fileP, const char *functionP, int lineP);

        // Destructor
        virtual ~InvalidParameterException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };


    // This exception is thrown when the specified resource, such as a hash table key or
    // thread, does not exist (or no longer exists).
    class NoSuchObjectException : public InstrumentedException {
    public:
        // Constructor
        NoSuchObjectException(const char *fileP, const char *functionP, int lineP);

        // Destructor
        virtual ~NoSuchObjectException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };

    // The specified thread is not joinable, or another thread is already waiting to
    // join it.  (Note that attempting to join yourself will throw WouldDeadlock.)
    class NotJoinableException : public InstrumentedException {
    public:
        // Constructor
        NotJoinableException(const char *fileP, const char *functionP, int lineP);

        // Destructor
        virtual ~NotJoinableException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };

    // This exception is thrown when a memory target passed to a function is NULL.
    class NullTargetPointerException : public InstrumentedException {
    public:
        // Constructor
        NullTargetPointerException(const char *fileP, const char *functionP, int LineP);

        // Destructor
        virtual ~NullTargetPointerException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };

    // This exception is thrown when a SoftFuse::assert check fails.
    class SoftFuseBlownException : public InstrumentedException {
    public:
        // Constructor
        SoftFuseBlownException(const char *fileP, const char *functionP, int LineP);

        // Destructor
        virtual ~SoftFuseBlownException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };

    // This exception is thrown when a method is called that has not yet been implemented.
    class UnimplementedException : public InstrumentedException{
    public:
        // Constructor
        UnimplementedException(const char *fileP, const char *functionP, int lineP);

        // Destructor
        virtual ~UnimplementedException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };

    // This exception is thrown when a method returns an unexpected return value or response.
    class UnknownResponseException : public InstrumentedException{
    public:
        // Constructor
        UnknownResponseException(const char *fileP, const char *functionP, int lineP);

        // Destructor
        virtual ~UnknownResponseException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };


    // This exception is thrown when a thread, semaphore, or mutex operation would cause
    // a deadlock.  In the case of trying to join a thread, it may also be thrown if
    // the caller is attempting to join itself.
    class WouldDeadlockException : public InstrumentedException{
    public:
        // Constructor
        WouldDeadlockException(const char *fileP, const char *functionP, int lineP);

        // Destructor
        virtual ~WouldDeadlockException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };



}

#endif
