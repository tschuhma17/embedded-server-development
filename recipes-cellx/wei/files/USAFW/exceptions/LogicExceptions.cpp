/* src/USAFW/exceptions/LogicExceptions.cpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#include "USAFW/exceptions/LogicExceptions.hpp"

namespace USAFW{

    /* == DESTRUCTION UNDERWAY == */
    DestructionUnderwayException::DestructionUnderwayException(const char *fileP, const char *functionP, int lineP) :
        InstrumentedException(fileP, functionP, lineP)
    {
        ;
    }

    DestructionUnderwayException::~DestructionUnderwayException() {
        ;
    }

    std::string DestructionUnderwayException::getTypeDesc(void) const {
        return std::string("DestructionUnderway");
    }

    std::string DestructionUnderwayException::getEventDesc(void) const {
        return std::string("An operation was attempted on an object which is currently being destroyed.");
    }

    /* == INVALID PARAMETER == */
    InvalidParameterException::InvalidParameterException(const char *fileP, const char *functionP, int lineP) :
        InstrumentedException(fileP, functionP, lineP)
    {
        ;
    }

    InvalidParameterException::~InvalidParameterException() {
        ;
    }

    std::string InvalidParameterException::getTypeDesc(void) const {
        return std::string("InvalidParameter");
    }

    std::string InvalidParameterException::getEventDesc(void) const {
        return std::string("The parameter is not valid.  It may be out of range, incorrectly formatted, etc.");
    }

    /* == NO SUCH OBJECT == */
    NoSuchObjectException::NoSuchObjectException(const char *fileP, const char *functionP, int lineP) :
        InstrumentedException(fileP, functionP, lineP)
    {
        ;
    }

    NoSuchObjectException::~NoSuchObjectException() {
        ;
    }

    std::string NoSuchObjectException::getTypeDesc(void) const {
        return std::string("NoSuchObject");
    }

    std::string NoSuchObjectException::getEventDesc(void) const {
        return std::string("The specified key, object, or resource does not exist (or no longer exists).");
    }

    /* == NO SUCH OBJECT == */
    NotJoinableException::NotJoinableException(const char *fileP, const char *functionP, int lineP) :
        InstrumentedException(fileP, functionP, lineP)
    {
        ;
    }

    NotJoinableException::~NotJoinableException() {
        ;
    }

    std::string NotJoinableException::getTypeDesc(void) const {
        return std::string("NotJoinable");
    }

    std::string NotJoinableException::getEventDesc(void) const {
        return std::string("The specified thread is not joinable, or another thread is already waiting to join it.");
    }

    /* == NULL TARGET POINTER == */
    NullTargetPointerException::NullTargetPointerException(const char *fileP, const char *functionP, int lineP) :
        InstrumentedException(fileP, functionP, lineP)
    {
        ;
    }

    NullTargetPointerException::~NullTargetPointerException() {
        ;
    }

    std::string NullTargetPointerException::getTypeDesc(void) const {
        return std::string("NullTargetPointer");
    }

    std::string NullTargetPointerException::getEventDesc(void) const {
        return std::string("A target buffer pointer was found to be NULL at runtime.");
    }

    /* == SOFT FUSE BLOWN == */
    SoftFuseBlownException::SoftFuseBlownException(const char *fileP, const char *functionP, int lineP) :
        InstrumentedException(fileP, functionP, lineP)
    {
        ;
    }

    SoftFuseBlownException::~SoftFuseBlownException() {
        ;
    }

    std::string SoftFuseBlownException::getTypeDesc(void) const {
        return std::string("SoftFuseBlown");
    }

    std::string SoftFuseBlownException::getEventDesc(void) const {
        return std::string("A soft fuse check failed, because the fuse has already been blown.");
    }


    /* == UNIMPLEMENTED == */
    UnimplementedException::UnimplementedException(const char *fileP, const char *functionP, int lineP) :
        InstrumentedException(fileP, functionP, lineP)
    {
        ;
    }

    UnimplementedException::~UnimplementedException() {
        ;
    }

    std::string UnimplementedException::getTypeDesc(void) const {
        return std::string("Unimplemented");
    }

    std::string UnimplementedException::getEventDesc(void) const {
        return std::string("This function has not yet been implemented.");
    }

    /* == UNKNOWN RESPONSE == */
    UnknownResponseException::UnknownResponseException(const char *fileP, const char *functionP, int lineP) :
        InstrumentedException(fileP, functionP, lineP)
    {
        ;
    }

    UnknownResponseException::~UnknownResponseException() {
        ;
    }

    std::string UnknownResponseException::getTypeDesc(void) const {
        return std::string("Unimplemented");
    }

    std::string UnknownResponseException::getEventDesc(void) const {
        return std::string("This function has not yet been implemented.");
    }

    /* == WOULD DEADLOCK == */
    WouldDeadlockException::WouldDeadlockException(const char *fileP, const char *functionP, int lineP) :
        InstrumentedException(fileP, functionP, lineP)
    {
        ;
    }

    WouldDeadlockException::~WouldDeadlockException() {
        ;
    }

    std::string WouldDeadlockException::getTypeDesc(void) const {
        return std::string("WouldDeadlock");
    }

    std::string WouldDeadlockException::getEventDesc(void) const {
        return std::string("The attempted operation would cause a deadlock.");
    }

}


