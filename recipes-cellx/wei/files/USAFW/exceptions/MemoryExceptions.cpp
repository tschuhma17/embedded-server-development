/* src/USAFW/exceptions/MemoryExceptions.cpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#include "USAFW/exceptions/MemoryExceptions.hpp"

namespace USAFW{

    /* == MEMORY EXCEPTION MIDCLASS == */
    MemoryException::MemoryException(const char *fileP, const char *functionP, int lineP) :
        InstrumentedException(fileP, functionP, lineP)
    {
        ;
    }

    MemoryException::~MemoryException() {
        ;
    }

    std::string MemoryException::getTypeDesc(void) const {
        return std::string("MemoryException");
    }

    std::string MemoryException::getEventDesc(void) const {
        return std::string("A memory exception has occurred.");
    }


    /* == OUT OF MEMORY (ALLOCATION FAILED) == */
    OutOfMemoryException::OutOfMemoryException(const char *fileP, const char *functionP, int lineP) :
        MemoryException(fileP, functionP, lineP)
    {
        ;
    }

    OutOfMemoryException::~OutOfMemoryException() {
        ;
    }

    std::string OutOfMemoryException::getTypeDesc(void) const {
        return std::string("OutOfMemory");
    }

    std::string OutOfMemoryException::getEventDesc(void) const {
        return std::string("Out of memory (allocation failed).");
    }


    /* == OPERATION WOULD HAVE OVERRUN THE TARGET BUFFER == */
    WouldOverrunException::WouldOverrunException(const char *fileP, const char *functionP, int lineP) :
        MemoryException(fileP, functionP, lineP)
    {
        ;
    }

    WouldOverrunException::~WouldOverrunException() {
        ;
    }

    std::string WouldOverrunException::getTypeDesc(void) const {
        return std::string("WouldOverrun");
    }

    std::string WouldOverrunException::getEventDesc(void) const {
        return std::string("The operation would have overrun the target buffer, so it was aborted.");
    }


    /* == OPERATION WOULD HAVE UNDERRUN THE TARGET BUFFER == */
    WouldUnderrunException::WouldUnderrunException(const char *fileP, const char *functionP, int lineP) :
        MemoryException(fileP, functionP, lineP)
    {
        ;
    }

    WouldUnderrunException::~WouldUnderrunException() {
        ;
    }

    std::string WouldUnderrunException::getTypeDesc(void) const {
        return std::string("WouldUnderrun");
    }

    std::string WouldUnderrunException::getEventDesc(void) const {
        return std::string("The operation would have underrun the target buffer, so it was aborted.");
    }

}
