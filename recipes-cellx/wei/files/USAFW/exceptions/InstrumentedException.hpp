/* include/USAFW/util/InstrumentedException.hpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#ifndef __InstrumentedException_hpp__
#define __InstrumentedException_hpp__

#include <exception>
#include <string>

namespace USAFW {

    // Utility macro for throwing instrumented exceptions.
    #define THROW(x) \
        throw x##Exception (__FILE__, __func__, __LINE__)

    // Utility macro for catching instrumented exceptions.
    #define CATCH(x, exP) \
        catch(x##Exception &exP)

    class InstrumentedException : public std::exception {
    public:
        // Constructor
        InstrumentedException(const char *fileP, const char *functionP, int Line);

        // Destructor
        ~InstrumentedException();

        virtual const char *what(void);

    protected:
        // File in which the exception was thrown.
        std::string     m_file_name;

        // Function in which the exception was thrown.
        std::string     m_function_name;

        // Line at which the exception was thrown.
        int             m_line_number;

        // Thread in which the exception was thrown.
        std::string     m_thread_name;
    };


}

#endif
