/* src/USAFW/exceptions/CommExceptions.cpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#include "USAFW/exceptions/CommExceptions.hpp"

namespace USAFW{

    /* == COMMUNICATIONS EXCEPTION MIDCLASS == */
    CommunicationsException::CommunicationsException(const char *fileP, const char *functionP, int lineP) :
        InstrumentedException(fileP, functionP, lineP)
    {
        ;
    }

    CommunicationsException::~CommunicationsException() {
        ;
    }

    std::string CommunicationsException::getTypeDesc(void) const {
        return std::string("Communications");
    }

    std::string CommunicationsException::getEventDesc(void) const {
        return std::string("General communications exception.");
    }


 
    /* == CONNECTION ALREADY OPEN == */
    ConnectionAlreadyOpenException::ConnectionAlreadyOpenException(const char *fileP, const char *functionP, int lineP) :
        CommunicationsException(fileP, functionP, lineP)
    {
        ;
    }

    ConnectionAlreadyOpenException::~ConnectionAlreadyOpenException() {
        ;
    }

    std::string ConnectionAlreadyOpenException::getTypeDesc(void) const {
        return std::string("ConnectionAlreadyOpen");
    }

    std::string ConnectionAlreadyOpenException::getEventDesc(void) const {
        return std::string("This object already has an open connection.");
    }


   /* == CONNECTION CLOSED == */
    ConnectionClosedException::ConnectionClosedException(const char *fileP, const char *functionP, int lineP) :
        CommunicationsException(fileP, functionP, lineP)
    {
        ;
    }

    ConnectionClosedException::~ConnectionClosedException() {
        ;
    }

    std::string ConnectionClosedException::getTypeDesc(void) const {
        return std::string("ConnectionClosed");
    }

    std::string ConnectionClosedException::getEventDesc(void) const {
        return std::string("The connection has been closed (or was never open).");
    }


    /* == CONNECTION FAILED == */
    ConnectionFailedException::ConnectionFailedException(const char *fileP, const char *functionP, int lineP) :
        CommunicationsException(fileP, functionP, lineP)
    {
        ;
    }

    ConnectionFailedException::~ConnectionFailedException() {
        ;
    }

    std::string ConnectionFailedException::getTypeDesc(void) const {
        return std::string("ConnectionFailed");
    }

    std::string ConnectionFailedException::getEventDesc(void) const {
        return std::string("The connection could not be established.");
    }


    /* == LINK DOWN == */
    LinkDownException::LinkDownException(const char *fileP, const char *functionP, int lineP) :
        CommunicationsException(fileP, functionP, lineP)
    {
        ;
    }

    LinkDownException::~LinkDownException() {
        ;
    }

    std::string LinkDownException::getTypeDesc(void) const {
        return std::string("LinkDown");
    }

    std::string LinkDownException::getEventDesc(void) const {
        return std::string("The communications link is down, or the media is not present.");
    }


    /* == NAME RESOLUTION FAILED == */
    NameResolutionFailedException::NameResolutionFailedException(const char *fileP, const char *functionP, int lineP) :
        CommunicationsException(fileP, functionP, lineP)
    {
        ;
    }

    NameResolutionFailedException::~NameResolutionFailedException() {
        ;
    }

    std::string NameResolutionFailedException::getTypeDesc(void) const {
        return std::string("NameResolutionFailed");
    }

    std::string NameResolutionFailedException::getEventDesc(void) const {
        return std::string("Name resolution failed.");
    }

    /* == UNSUPPORTED TRANSPORT == */
    UnsupportedTransportException::UnsupportedTransportException(const char *fileP, const char *functionP, int lineP) :
        CommunicationsException(fileP, functionP, lineP)
    {
        ;
    }

    UnsupportedTransportException::~UnsupportedTransportException() {
        ;
    }

    std::string UnsupportedTransportException::getTypeDesc(void) const {
        return std::string("UnsupportedTransport");
    }

    std::string UnsupportedTransportException::getEventDesc(void) const {
        return std::string("The link class does not support the transport specified in the URL.");
    }

}