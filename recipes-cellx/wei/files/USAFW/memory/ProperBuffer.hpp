/* USAFW/util/ProperBuffer.hpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#ifndef __ProperBuffer_hpp__
#define __ProperBuffer_hpp__

#include <cstdint>

#include "USAFW/memory/guarded_buffer.hpp"
#include "USAFW/util/Lockable.hpp"
#include "USAFW/util/Logger.hpp"

namespace USAFW {

    /** This mostly-virtual base class defines an interface that helps
     *  communications protocols be less concerned about how their actual data
     *  buffering is being done.
     * 
     *  Derived classes include:
     * 
     *  - StripBuffer       A flat, linear buffer that must be cleared after reading
     *                      before the read data area becomes available for reuse.
     *                      Reads are non-consuming operations.
     * 
     *  - RingBuffer        A simple, byte-oriented ring buffer, which copies data in
     *                      and out.  Read data becomes usable free storage immediately.
     *                      Reads are consuming operations.
     * 
     *  - StripRingBuffer   A ring of pointers to move-based strip buffers, used for
     *                      staging caller-allocated data blocks efficiently (that is,
     *                      without copies).
     *                      Reads are consuming operations.
     */
    class ProperBuffer : public Lockable {
    public:
        // Constructor
        ProperBuffer(Logger &loggerP, size_t bytesP);

        // Destructor
        virtual ~ProperBuffer();

        // Returns the total size allocated for the buffer's data storage.
        // This is NOT the number of bytes available!
        // 
        // Autolock: NO
        virtual size_t getBytesAllocated(void) const;

        // Returns the number of bytes written, but not yet read.
        // 
        // Autolock: YES
        virtual size_t getBytesPending(void) const;

        // Returns the number of bytes of storage remaining.
        //
        // NOTE: This function is non-const due to internal locking.
        // Autolock: YES
        virtual size_t getBytesFree(void);

        // Purges (drops) data in the buffer, setting the number of bytes
        // available to zero.  To avoid data leaks, the buffer is zeroed.
        virtual void purge(void);

        // Reads data from the ring buffer, returning the number of bytes
        // copied and consumed (that is, no longer available to read, but
        // made available to write).
        // 
        // Autolock: YES
        virtual size_t read(void *pDestR, size_t maxBytesP)=0;

        // Inserts data into the ring buffer.  ONLY a full insertion shall
        // work.  To ensure that enough space is available, lock the
        // buffer, check the free space, insert what you want, and unlock the
        // buffer (the internal lock is recursive and will not affect this).
        // 
        // Autolock: YES
        // Blocks:   NO
        //
        // THROWS:
        // - WouldOverrunException if the buffer does not have enough free
        //   space for the write.
        virtual void write(const void *pSrcP, size_t bytes)=0;

    protected:
        // Logger.
        Logger                  &m_logger;

        // Total allocated storage available for user data, in bytes.
        size_t                  m_bytes_allocated;

        // Number of filled (available for reading) bytes.
        size_t                  m_bytes_pending;

        // Actual data buffer, plus a guard region at either end.
        guarded_buffer_header_t *p_guarded_data;

        DECLARE_GUARD_FOOTER
    };

}


#endif
