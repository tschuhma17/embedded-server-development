/* USAFW/util/RingBuffer.hpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#ifndef __RingBuffer_hpp__
#define __RingBuffer_hpp__

#include <cstdint>

#include "USAFW/memory/ProperBuffer.hpp"

namespace USAFW {

    /** This class implements a simple, byte-oriented ring buffer. */
    class RingBuffer : public ProperBuffer {
    public:
        // Constructor
        RingBuffer(Logger &loggerP, size_t bytesP);

        // Destructor
        ~RingBuffer();

        // Inserts data into the ring buffer.  ONLY a full insertion shall
        // work.  To ensure that enough space is available, lock the
        // buffer, check the free space, insert what you want, and unlock the
        // buffer (the internal lock is recursive and will not affect this).
        // 
        // Autolock: YES
        // Blocks:   NO
        //
        // THROWS:
        // - WouldOverrunException if the buffer does not have enough free
        //   space for the write.
        virtual void write(const void *pSrcP, size_t bytes);

        // Reads data from the ring buffer, returning the number of bytes
        // copied and consumed (that is, no longer available to read, but
        // made available to write).
        // 
        // Autolock: YES
        virtual size_t read(void *pDestR, size_t maxBytesP);

    protected:
        // Index of next byte to READ OUT, assuming >0 bytes free.
        int m_head_index;

        // Index of next byte to WRITE IN, assuming >0 bytes available.
        int m_tail_index;
    };

}


#endif
