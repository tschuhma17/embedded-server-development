/* src/USAFW/util/RingBuffer.cpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#include <cstring>
#include <sstream>

#include "USAFW/exceptions/MemoryExceptions.hpp"
#include "USAFW/exceptions/SecurityExceptions.hpp"

#include "USAFW/memory/Guarded.hpp"
#include "USAFW/memory/RingBuffer.hpp"
#include "USAFW/util/Lockable.hpp"
#include "USAFW/util/math.hpp"

namespace USAFW {

    RingBuffer::RingBuffer(Logger &loggerP, size_t bytesP) :
        ProperBuffer(loggerP, bytesP),
        m_head_index(0),
        m_tail_index(0)
    {
        // Function check
        guardedBuffer_integrity_check(p_guarded_data);
    }

    RingBuffer::~RingBuffer()
    {
        // Ensure the object is not locked.
        ASSERT_UNLOCKED();
        m_bytes_allocated=0;
    }

    void RingBuffer::write(const void *pSrcP, size_t bytesP) {
        AUTOLOCK;
        
        // Not enough space.  Partial writes are not allowed; caller is responsible
        // for buffer splits.
        if (bytesP>(m_bytes_allocated-m_bytes_pending)) {
            m_logger.error("Not enough space for attempted write to ring buffer");
            THROW(WouldOverrun);
        }
        else {
            std::string msg("Trying to write ");
            msg+=std::to_string(bytesP);
            msg+=std::string(" bytes to buffer with ");
            msg+=std::to_string(m_bytes_allocated - m_bytes_pending);
            msg+=std::string(" bytes free");
            //m_logger.debug(msg);
        }

        // Pre-write check.
        // TODO: Make debug only
        guardedBuffer_integrity_check(p_guarded_data);

        // Copy the LEADING (offset, not wrapped) chunk of the buffer.
        int leading_bytes = MIN(m_bytes_allocated-m_tail_index, bytesP);
        //m_logger.debug("Writing "+std::to_string(leading_bytes)+" leading bytes to tail index "+std::to_string(m_tail_index));
        std::memcpy(p_guarded_data->p_data + m_tail_index,
                    pSrcP, leading_bytes);

        // Post-write check.
        // TODO: Make debug only
        guardedBuffer_integrity_check(p_guarded_data);

        // Copy the TRAILING (wrapped) chunk of the buffer.
        int trailing_bytes = bytesP-leading_bytes;
        if (trailing_bytes>0) {
			//std::string sent0Text( (char *)pSrcP, bytesP );
			//m_logger.debug("Q0 " + sent0Text);
			//std::string sent1Text( (char *)p_guarded_data->p_data + m_tail_index, leading_bytes );
			//m_logger.debug("Q1 " + sent1Text);

            m_logger.debug("Writing " + std::to_string(leading_bytes) + " leading bytes; writing " + std::to_string(trailing_bytes) + " trailing (wrapped) bytes");
            std::memcpy(p_guarded_data->p_data, (void *)((uint8_t *)pSrcP + leading_bytes), trailing_bytes);

			//std::string sent2Text( (char *)p_guarded_data->p_data, trailing_bytes );
			//m_logger.debug("Q2 " + sent2Text);
        }
        
        /// Check for buffer approaching buffer overflow
        if (m_tail_index >= m_head_index)
        {
			int spread = m_tail_index - m_head_index;
			if (spread > (m_bytes_allocated / 10))
			{
				m_logger.debug("ring buffer holds " + std::to_string(spread) + " bytes");
			}
		}

        // Post-write check.
        // TODO: Make debug only
        guardedBuffer_integrity_check(p_guarded_data);

        // Update index.
        m_tail_index = (m_tail_index+bytesP)%m_bytes_allocated;

        // Update unread bytes.
        m_bytes_pending += bytesP;

        //m_logger.debug("Wrote "+std::to_string(leading_bytes+trailing_bytes)+" bytes total.");
    }

    size_t RingBuffer::read(void *pDestR, size_t maxBytesP)
    {
        LOCK();

        size_t len = MIN(maxBytesP, m_bytes_pending);

        // Copy the LEADING (offset, not wrapped) chunk of the buffer.
        int leading_bytes = MIN(len, m_bytes_allocated-m_head_index);
        if (leading_bytes>0) {
            //m_logger.debug("Reading "+std::to_string(leading_bytes)+" leading bytes from head index "+std::to_string(m_head_index));
            std::memcpy(pDestR,
                        p_guarded_data->p_data + m_head_index,
                        leading_bytes);
        }

        // Copy the TRAILING (wrapped) chunk of the buffer.
        int trailing_bytes = MAX(0, len-leading_bytes);
        if (trailing_bytes>0) {
			//std::string sent1Text( (const char *)(p_guarded_data->p_data + m_head_index), leading_bytes );
			//m_logger.debug("Q1 " + sent1Text);
            m_logger.debug("Reading " + std::to_string(leading_bytes) + " leading bytes; reading " + std::to_string(trailing_bytes) + " trailing (wrapped) bytes");
            std::memcpy((void *)((uint8_t *)pDestR + leading_bytes), p_guarded_data->p_data, trailing_bytes);

			//std::string sent2Text( (char *)p_guarded_data->p_data, trailing_bytes );
			//m_logger.debug("Q2 " + sent2Text);
  			//std::string sent3Text( (char *)pDestR, len );
			//m_logger.debug("Q3 " + sent3Text);
       }

        // Update index.
        m_head_index = (m_head_index+len)%m_bytes_allocated;

        // Update unread bytes.
        m_bytes_pending -= len;

        // Post-read check.
        // TODO: Make debug only
        guardedBuffer_integrity_check(p_guarded_data);

        UNLOCK();
        //m_logger.debug("Read "+std::to_string(leading_bytes+trailing_bytes)+" bytes total.");
        return leading_bytes + trailing_bytes;
    }

}
