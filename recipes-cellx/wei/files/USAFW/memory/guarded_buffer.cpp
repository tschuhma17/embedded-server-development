/* src/USAFW/memory/guarded_buffer.cpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

/** Support for dynamically allocated guarded buffers of run-time determined
 *  size.  Where possible, dedicated classes or structs should be used instead,
 *  as memory layout is more predictable, but when sizes are truly dynamic,
 *  these structures and functions should serve.
 *
 *  While this functionality is C-like, note that this code is indeed C++, for
 *  two reasons: namespacing, and exception throws when integrity checks fail.
 */

#include <memory.h>
#include <stdlib.h>

#include "USAFW/exceptions/SecurityExceptions.hpp"
#include "USAFW/memory/guarded_buffer.hpp"

namespace USAFW {

    // Allocates a guarded buffer, as a block large enough to contain:
    // - guarded_buffer_header_t (including the header guard region)
    // - allocated payload data region
    // - the footer guard region
    guarded_buffer_t *guardedBuffer_new(size_t bytesP) {
        // Allocate storage and cast to header pointer.
        size_t actual_size=GUARDED_BUFFER_ACTUAL_SIZE(bytesP);
        uint8_t *p_raw_bytes = (uint8_t *)malloc(actual_size);
        guarded_buffer_t *p_buffer = reinterpret_cast<guarded_buffer_t*>(p_raw_bytes);

        // Set up metadata.
        p_buffer->m_bytes_allocated = bytesP;

        // Set up data pointers properly.
        p_buffer->p_data = p_raw_bytes + sizeof(guarded_buffer_t);
        p_buffer->p__footer_guard = reinterpret_cast<guard_region_t *> ( \
                                        p_raw_bytes + sizeof(guarded_buffer_t) + bytesP
                                    );

        // Initialize guard regions.
        for (int n=0; n<USAFW_GUARD_SIZE_BYTES; n++) {
            // Set sentinel values.  ZERO SHOULD NEVER BE USED.
            p_buffer->m__header_guard[n]=(2*USAFW_GUARD_SIZE_BYTES)-n;
            (*(p_buffer->p__footer_guard))[n]=(2*USAFW_GUARD_SIZE_BYTES)-n;
        }

        // Sanity check.
        // TODO - remove
        guardedBuffer_integrity_check(p_buffer);

        // Return the overall object;
        return p_buffer;
    }

    // Checks the header and footer guard integrity of a guarded buffer region.
    // THROWS
    // - IntegrityCheckFailedException if things have gone pear-shaped.
    void guardedBuffer_integrity_check(const volatile guarded_buffer_t *pSrcP) {
        if (NULL==pSrcP) {
            THROW(NullInspectionTarget);
        }
        for (int n=0; n<USAFW_GUARD_SIZE_BYTES; n++) {
            // Check sentinel values.  ZERO SHOULD NEVER BE USED.
            if (pSrcP->m__header_guard[n]!=(2*USAFW_GUARD_SIZE_BYTES)-n) {
                THROW(IntegrityCheckFailed);
            }
            if ((*(pSrcP->p__footer_guard))[n]!=(2*USAFW_GUARD_SIZE_BYTES)-n) {
                THROW(IntegrityCheckFailed);
            }
        }

    }
    
    // Security exfoliant.
    volatile uint32_t __guardedBufer_salt_scrub=0xdeadc0de;

    // Deallocates a guardded buffer, including the data region and both the
    // header and footer guard regions.
    void guardedBuffer_delete(guarded_buffer_t *pSrcP) {
        // Whack the guard buffers to prevent use-after-free and double-free
        // attacks.
        for (int n=0; n<USAFW_GUARD_SIZE_BYTES; n++) {
            // magic
            pSrcP->m__header_guard[n]=(((n+__guardedBufer_salt_scrub)>>(n%3))%256);
            // more magic
            (*(pSrcP->p__footer_guard))[n]=(((n+__guardedBufer_salt_scrub)>>(n%3))%256);
            __guardedBufer_salt_scrub+=n;
        }
        memset(pSrcP->p_data, 0, pSrcP->m_bytes_allocated);
        pSrcP->p_data=NULL;
        pSrcP->p__footer_guard=NULL;
        free(pSrcP);
    }

}

