/* include/USAFW/util/Guarded.hpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#ifndef __Guarded_hpp__
#define __Guarded_hpp__

#include <cstdint>
#include <exception>

#include "USAFW/sizes.h"

namespace USAFW {

    #define DECLARE_GUARD_FOOTER                                            \
        private:                                                            \
            volatile uint8_t m__footer_guard_region[USAFW_GUARD_SIZE_BYTES];

    /** This base class provides a "header" guard region at the beginning of
     *  any object whose class inherits it.  When used with its supporting
     *  macros, it will also provide "belt" and "footer" guard regions.
     *  These intermediate guard regions are handled as a linked-list.  The
     *  class includes integrity-check functions, and an optional system-level
     *  tracking list, which can be used to provide a list of objects that
     *  should be integrity checked by an security thread.  Note that the
     *  integrity check does NOT require the object to be locked, but does
     *  ensure this cannot happen during deletion.
     * 
     *  Note the following available alternatives:
     *  - If the object needs thread synchronization, consider Lockable.
     *  - If the object requires guard bytes, but does not need to be debuggable,
     *    consider using Guarded directly.  (Debugging implies Guarded.)
     * 
     *  CRITICAL
     *  --------
     *  To ensure correct memory layout:
     *  1. Guarded must be the ultimate base class.
     *  2. Multiple-inheritance must be either avoided or used with care (and,
     *     preferably, a debugger inspetion to ensure you are getting what you
     *     think you are getting, in terms of layout).
     *  3. You must use the GUARD_FOOTER macro at the VERY END of your
     *     intermediate and final classes to declare the required storage
     *     regions, or you will only get header protection.
     *  4. DERIVED OBJECT CONSTRUCTORS MUST CALL BEGIN_WATCH() AS THEIR FIRST
     *     ACTION (OR AS EARLY AS POSSIBLE).
     *  5. DERIVED OBJECT DESTRUCTORS MUST CALL END_OF_WATCH() AS THEIR FINAL
     *     ACTION (OR AS LATE AS POSSIBLE) TO PERFORM A FINAL INTEGRITY CHECK
     *     AND UNLINK THE FOOTER.
     *  6. Ensure you are using guard region sizes appropriate for your
     *     application, or memory and/or performance issues may result.
     *  7. Cryptographic, security-critical, and safety-critical objects should
     *     be integrity-checked before use whenever performance allows.
     */
    class Guarded {
        public:
            // Constructor
            Guarded();

            // Destructor.  Runs an integrity check prior to destruction.
            virtual ~Guarded();

            // Integrity checks the object.
            // THROWS:
            // - IntegrityCheckFailedException if any guard region is corrupt.
            void integrityCheck(void);

        protected:
            // Header guard region.  ABSOLUTELY MUST COME FIRST.
            volatile uint8_t    m__header_guard_region[USAFW_GUARD_SIZE_BYTES];

            //DECLARE_GUARD_FOOTER
    };

    // Computes the total effective size of a buffer of the specified number of bytes
    // including leading and trailing buffer regions.
    #define GUARDED_BUFFER_CALC_SIZE(size_bytes) (size_bytes+2*USAFW_GUARD_SIZE_BYTES)


    // Utility macro used when working with manually-allocated guarded buffers.
    // Converts a pointer to the buffer and a data byte index to a properly offset
    // data pointer.
    //
    // buffer should be of type uint8_t.
    #define GUARDED_INDEX(buffer, index) ((reinterpret_cast<uint8_t*>(buffer))+USAFW_GUARD_SIZE_BYTES+index)

    // Initialize a manually-managed guarded buffer.
    void initGuardedBuffer(void *pGuardedBufferP, size_t nBytesP);

    // Validate a manually-managed guarded buffer.
    void checkGuardedBuffer(void *pGuardedBufferP, size_t nBytesP);

};

#endif
