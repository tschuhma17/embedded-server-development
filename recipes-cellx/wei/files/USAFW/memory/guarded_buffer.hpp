/* include/USAFW/memory/guarded_buffer.h */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

/** Support for dynamically allocated guarded buffers of run-time determined
 *  size.  Where possible, dedicated classes or structs should be used instead,
 *  as memory layout is more predictable, but when sizes are truly dynamic,
 *  these structures and functions should serve.
 *
 *  While this functionality is C-like, note that this code is indeed C++, for
 *  two reasons: namespacing, and exception throws when integrity checks fail.
 */

#ifndef __USAFW_guarded_buffer_hpp__
#define __USAFW_guarded_buffer_hpp__

#include <cstddef>
#include <cstdint>

#include "USAFW/sizes.h"

namespace USAFW {

    // Returns the total size that should be allocated for a guarded buffer object,
    // for a given payload size in bytes.  This will include:
    // - The guarded buffer header, which includes the header guard region
    // - The payload data
    // - The footer guard region
    #define GUARDED_BUFFER_ACTUAL_SIZE(payloadSizeInBytes)              \
        (                                                               \
            sizeof(guarded_buffer_header_t) +                           \
            payloadSizeInBytes +                                        \
            USAFW_GUARD_SIZE_BYTES                                      \
        )

    // The elements used for guard regions.
    typedef volatile uint8_t (guard_byte_t);

    // The guard regions used to protect objects.  The use of guard_byte_t allows
    // the system to be configured to use volatile or non-volatile guard bytes to
    // protect from attacks executed on deeply-embedded coprocessors (e.g., Via)
    // that rely on temporary cache line conents to mask their presence.
    typedef guard_byte_t guard_region_t[USAFW_GUARD_SIZE_BYTES];

    // Used for dynamically-allocated data ONLY.  C++ classes should inherit from
    // GuardedBase instead where possible.
    typedef struct guarded_buffer_header_s {
        // PERFORMANCE CAUTION:
        // DO NOT ADD MEMBERS SMALLER THAN THE OPTIMAL ALIGNMENT GRANULARITY TO THIS
        // STRUCTURE.

        // Header guard region.  ABSOLUTELY MUST COME FIRST.
        guard_region_t      m__header_guard;

        // Size of the data block that immediately follows this buffer.
        size_t              m_bytes_allocated;

        // Amount of the buffer that is actually in use, in bytes.
        size_t              m_bytes_full;

        // Pointer to footer guard region.  Set by initializer and normally
        // points to data immediately after the data block.
        //
        // TODO:
        // Strictly speaking, this should ideally be replaced with pointer
        // arithmetic, probably via preprocessor macro, to avoid wasting the
        // pointer's space.  But more use cases are needed to evaluate that.
        guard_region_t      *p__footer_guard;

        // Guarded data pointer.  Set by initializer and normally points to data
        // immediately after the size of this pointer value itself.
        uint8_t             *p_data;
    } guarded_buffer_header_t;

    // This alias may help with future refinements.
    typedef guarded_buffer_header_t guarded_buffer_t;

    // Allocates a guarded buffer, as a block large enough to contain:
    // - guarded_buffer_header_t (including the header guard region)
    // - allocated payload data region
    // - the footer guard region
    guarded_buffer_t *guardedBuffer_new(size_t bytesP);

    // Checks the header and footer guard integrity of a guarded buffer region.
    // THROWS
    // - IntegrityCheckFailedException if things have gone pear-shaped.
    void guardedBuffer_integrity_check(const volatile guarded_buffer_t *pSrcP);
    
    // Deallocates a guardded buffer, including the data region and both the
    // header and footer guard regions.
    void guardedBuffer_delete(guarded_buffer_t *pSrcP);
}


#endif


