/* USAFW/util/Guarded.cpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#include "USAFW/exceptions/SecurityExceptions.hpp"

#include "USAFW/memory/Guarded.hpp"

namespace USAFW {

    Guarded::Guarded() {
        // Initialize the guard regions.
        //
        // Right now, this is done by filling the guard region with a linear
        // sequence.  Ideally, this would actually be a polynomial that is
        // randomly generated for each guarded object.
        for (int n=0; n<USAFW_GUARD_SIZE_BYTES; n++) {
            // Set sentinel values.  ZERO SHOULD NEVER BE USED.
            m__header_guard_region[n]=(2*USAFW_GUARD_SIZE_BYTES)-n;
        }
    }

    Guarded::~Guarded() {
        // Final integrity check.
        integrityCheck();
    }


    void Guarded::integrityCheck() {
        for (int n=0; n<USAFW_GUARD_SIZE_BYTES; n++) {
            // Set sentinel values.  ZERO SHOULD NEVER BE USED.
            if (m__header_guard_region[n]!=(2*USAFW_GUARD_SIZE_BYTES)-n) {
                THROW(IntegrityCheckFailed);
            }
        }
    }

#if 0
/* obsolete 
    void initGuardedBuffer(void *pBufferP, size_t nBytesP)
    {
        // Init HEADER.
        for (int n=0; n<USAFW_GUARD_SIZE_BYTES; n++) {        
            // Set sentinel values.  ZERO SHOULD NEVER BE USED.
            static_cast<uint8_t*>(pBufferP)[n]=(2*USAFW_GUARD_SIZE_BYTES)-n;
        }

        // Init FOOTER.
        for (int n=0; n<USAFW_GUARD_SIZE_BYTES; n++)
        { 
            // Set sentinel values.  ZERO SHOULD NEVER BE USED.
            static_cast<uint8_t*>(pBufferP)[USAFW_GUARD_SIZE_BYTES+nBytesP+n]=(2*USAFW_GUARD_SIZE_BYTES)-n;
        }
    }

    void checkGuardedBuffer(void *pBufferP, size_t nBytesP)
    {
        // Check HEADER.
        for (int n=0; n<USAFW_GUARD_SIZE_BYTES; n++) {        
            // Set sentinel values.  ZERO SHOULD NEVER BE USED.
            if (static_cast<const uint8_t*>(pBufferP)[n]!=(2*USAFW_GUARD_SIZE_BYTES)-n) {
                THROW(IntegrityCheckFailed);
            }
        }

        // Check FOOTER. 
        for (int n=0; n<USAFW_GUARD_SIZE_BYTES; n++) {        
            // Set sentinel values.  ZERO SHOULD NEVER BE USED.
            if (static_cast<const uint8_t*>(pBufferP)[USAFW_GUARD_SIZE_BYTES+nBytesP+n]!=(2*USAFW_GUARD_SIZE_BYTES)-n) {
                THROW(IntegrityCheckFailed);
            }
        }       
    }
*/
#endif

}
