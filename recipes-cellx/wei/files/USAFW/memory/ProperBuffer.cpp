/* src/USAFW/util/ProperBuffer.cpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#include "string.h"

#include "USAFW/util/Lockable.hpp"
#include "USAFW/memory/guarded_buffer.hpp"
#include "USAFW/memory/ProperBuffer.hpp"

namespace USAFW {

    ProperBuffer::ProperBuffer(Logger &loggerP, size_t bytesP) :
        m_logger(loggerP),
        m_bytes_allocated(bytesP),
        m_bytes_pending(0),
        p_guarded_data(guardedBuffer_new(bytesP))
    {
        guardedBuffer_integrity_check(p_guarded_data);
    }


    ProperBuffer::~ProperBuffer() {
        guardedBuffer_integrity_check(p_guarded_data);
        guardedBuffer_delete(p_guarded_data);
    }

    size_t ProperBuffer::getBytesAllocated(void) const {
        return m_bytes_allocated;
    }

    size_t ProperBuffer::getBytesPending(void) const {
        return m_bytes_pending;
    }

    size_t ProperBuffer::getBytesFree(void) {
        AUTOLOCK;
        size_t n;
        n = m_bytes_allocated - m_bytes_pending;
        return n;
    }

    void ProperBuffer::purge(void) {
        AUTOLOCK;
        memset(p_guarded_data->p_data, 0, p_guarded_data->m_bytes_allocated);
        p_guarded_data->m_bytes_full=0;
        m_bytes_pending=0;
    }

}