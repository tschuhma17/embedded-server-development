/* USAFW/SLED_POST.cpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

/** Power-On Self-Test for the USA Firmware Standard Library for Embedded
 *  Development (USAFW-SLED).
 * 
 *  TO USE:
 *      #include "USAFW/SLEDPOST.hpp"
 *      USAFW::SLED_POST();
 */

#include "USAFW/util/Lockable.hpp"
#include "USAFW/util/Logger.hpp"
#include "USAFW/exceptions/LogicExceptions.hpp"
#include "USAFW/exceptions/MemoryExceptions.hpp"
#include "USAFW/exceptions/SecurityExceptions.hpp"
#include "USAFW/memory/RingBuffer.hpp"
#include "USAFW/ProperComms/TCPLink.hpp"


namespace USAFW {

    void SLED_POST_TCPLink_on_exception(ProperCommBase *pLinkP, InstrumentedException &ex, void *userDataP) {
    }


    void SLED_POST_TCPLink_on_recv(ProperCommBase *pLinkP, void *userDataP) {
    }


    void SLED_POST_TCPLink_on_xmit(ProperCommBase *pLinkP, void *userDataP) {
    }


    void SLED_POST(void) {
        Logger oLogger;

        oLogger.debug("Start SLEDPost");

        // Suppress noise during POST.
        oLogger.set_min_stderr_level(Logger::log_level_fatal);
        oLogger.set_min_stdout_level(Logger::log_level_debug);

        oLogger.info("SLED test harness.");

        oLogger.debug("Testing locking primitives...");
        {
            Lockable lock;
            lock._lock(HERE());
            lock._unlock(HERE());
            {
                class AutolockTest : public Lockable{
                public:
                    void test(void) { AUTOLOCK; }
                };
                AutolockTest autolock_test_object;
                autolock_test_object.test();
            }
        }

        oLogger.debug("Testing buffers...");
        RingBuffer oRing(oLogger, 8);
        oRing.write("0123", 4);
        oRing.write("4567", 4);
        try {
            oRing.write("89AB", 4);
        } catch (USAFW::WouldOverrunException &ex) {
            // Dismiss, this is expected.
        }
        oRing.integrityCheck();

        // Test the TCP comms system.
        // FIXME:
        // This requires an external echo server; that should be replaced with a listener.
 
        TCPLink link(oLogger, "SLEDPost",
                        SLED_POST_TCPLink_on_exception, NULL,
                        SLED_POST_TCPLink_on_recv, NULL,
                        SLED_POST_TCPLink_on_xmit, NULL,
                        4096, 4096);

        oLogger.debug("End SLEDPost");
    }
}
