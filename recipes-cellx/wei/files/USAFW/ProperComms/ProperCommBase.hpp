/* USAFW/ProperComms/ProperCommBase.hpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#ifndef __ProperCommBase_hpp__
#define __ProperCommBase_hpp__

#include <cstdint>

#include "USAFW/details.h"
#include "USAFW/sizes.h"
#include "USAFW/exceptions/InstrumentedException.hpp"
#include "USAFW/memory/guarded_buffer.hpp"
#include "USAFW/memory/ProperBuffer.hpp"
#include "USAFW/timing/timeout.h"
#include "USAFW/util/Lockable.hpp"
#include "USAFW/util/Logger.hpp"

namespace USAFW {

    // Forward reference to allow callbacks to be prototyped.
    class ProperCommBase;

#if 0
    // Transmission buffer.
    class ProperCommsXmitBuffer : public Lockable {
        // True if and only if this buffer has not yet been transmitted.
        bool        m_buffer_pending;

        // Pending job serial number.  Increments, may wrap.  Reported to
        // transmission completion handler.
        uint32_t    m_xmit_job_serial_number;

        // Number of bytes in the buffer that are valid and should be transmitted.
        size_t      m_bytes_to_transmit;

        // Offset into the buffer for the next transmission attempt.  Initially zero.
        size_t      m_next_byte_offset;
 
        // Actual transmission buffer.  Assigned to its own address.
        // Size allocated depends on system configuration.
        uint8_t     *p_data;
    };


    // Receive buffer header.
    typedef struct propercomms_recv_buffer_s {
        // Receiver thread block serial number.  Increments, may wrap.
        uint32_t    m_recv_block_serial_number;

        // Number of bytes in the buffer that are valid and may be read out.
        size_t      m_bytes_received;

        // Actual reception buffer.  Assigned to its own address.
        // Size allocated depends on system configuration.
    } propercomms_recv_buffer_t;

/*
    // Channel exception handler type definition.  This handler runs in case
    // of an asynchronous channel exception, such as unexpected closure or
    // loss of physical media.
    typedef void (*propercomms_channel_exception_handler_t)(void *pUserDataP);

    // Transmission exception handler type definition.  This handler runs in
    // case of an asynchronous transmission exception, such as a timeout or
    // a failure in the lower-level communications library.
    typedef void (*propercomms_xmit_exception_handler_t)(void *pUserDataP);

    // Receiver exception handler type definition.  This handler runs in
    // case of an asynchronous reception exception, such as a buffer overflow
    // or a failure in the lower-level communications library.
    typedef void (*propercomms_recv_exception_handler_t)(void *pUserDataP);
    */

    // General-purpose exception handler for communications faults.
    typedef void (*propercomms_exception_handler_t)(class USAFW::ProperCommBase *pLinkP, USAFW::InstrumentedException& ex, void *pUserDataP);

    // Transmission job complete handler type definition.  This handler runs
    // when the transmission staging buffer has been fully transferred to the
    // communications layer.  Note that this does NOT mean all data has made
    // it to the remote target.
    typedef void (*propercomms_xmit_handler_t)(class USAFW::ProperCommBase *pLinkP, void *pUserDataP);

    // Data received handler type definition.  This handler runs when a block
    // of ANY size is received.  That is to say:
    // - The buffer may not be completely full
    // - The buffer may not end in a useful token, such as a line feed or carriage return
    // - The buffer may end in a carriage return even though a line feed is still pending
    typedef void (*propercomms_recv_handler_t)(class USAFW::ProperCommBase *pLinkP, void *pUserDataP);
#endif

    // Communications link exception handler type definition.  This handler
    // runs in case of an asynchronous channel exception, such as:
    // - unexpected closure
    // - loss of physical media
    // - receive buffer overflow
    // ...and more!
    typedef void (*propercomms_exception_handler_t)(class ProperCommBase *pLinkP, InstrumentedException& ex, void *pUserDataP);

    // Transmission complete handler type definition.  This handler runs when the
    // entire transmission buffer has been successfully transmitted.
    typedef void (*propercomms_xmit_handler_t)(class ProperCommBase *pLinkP, void *pUserDataP);

    // Data reception handler type definition.  This handler runs when a block of
    // of ANY size is received.  That is to say:
    // - The buffer may not be completely full
    // - The buffer may not end in a useful token, such as a line feed or carriage return
    // - The buffer may end in a carriage return even though a line feed is still pending
    typedef void (*propercomms_recv_handler_t)(class ProperCommBase *pLinkP, void *pUserDataP);

    /** Base class designed to provide as similar an interface as possible for
     *  devices which may offer multiple communication options, such as both
     *  TCP over Ethernet and RS232.
     * 
     *  The class runs two threads-- one for transmission, one for reception--
     *  enabling full-duplex operation.
     */
    class ProperCommBase : public Lockable {
    public:
/*
        // Constructor
        ProperCommBase( Logger &oLoggerP,
                        const char *channelNameP,
                        propercomms_channel_exception_handler_t channelExceptionHandlerP,
                        void *pChannelExceptionUserDataP,
                        propercomms_recv_exception_handler_t recvExceptionHandlerP,
                        void *pRecvExceptionUserDataP,
                        propercomms_xmit_exception_handler_t xmitExceptionHandlerP,
                        void *pXmitExceptionUserDataP,
                        size_t  recvBufSizeBytesP,
                        int     recvBufCountP,
                        size_t  xmitBufSizeBytesP,
                        int     xmitBufCountP);

        // Destructor
        ~ProperCommBase();
*/
        // Constructor
        ProperCommBase( Logger                          &oLoggerP,
                        const std::string               channelNameP,
                        propercomms_exception_handler_t exceptionHandlerP,
                        void                            *pExceptionUserDataP,
                        propercomms_recv_handler_t      onRecvHandlerP,
                        void                            *pOnRecvUserDataP,
                        propercomms_xmit_handler_t      onXmitHandlerP,
                        void                            *pOnXmitUserDataP,
                        size_t                          recvBufSizeBytesP,
                        size_t                          xmitBufSizeBytesP);

        // Destructor
        virtual ~ProperCommBase();

        // Closes the channel.  If the channel is already closed, the function
        // has no effect.  Note that this function does NOT wait until the
        // transmit buffer is empty, it will attempt to close the channel
        // immediately.  Closing the channel will result in the loss of any 
        // data currently in the receive buffer.  This function is synchronous.
        virtual void close(void)=0;

        // Returns true if and only if the link is open and connected, else false.
        inline bool isConnected(void) const { return m_connected; }

        // Attempts to connect to the server and port specified.  This function
        // is synchronous.  The connection will be built based on the transport,
        // target name, and target port, as appropriate for the link class in
        // use.  Examples:
        //
        // For TCPLink::connect: "tcp://192.168.100.1:80", "tcp://foo.com:444"
        // For SerialLink::connect: "serial:///dev/ttyS0&bps=9600&parity=N&databits=8&stopbits=1"
        // For I2C::connect: "i2c://gpio&sda=34&scl=35&mode=sync&bps=400000"
        virtual void open(const std::string targetP)=0;

        // Gets the number of bytes available in the transmission staging buffer.
        // This is the maximum number of bytes that should be sent at once, even
        // though the data block may be sent directly to the interface when
        // possible.
        virtual size_t getRecvBufferBytesFree(void);

        // Gets the number of bytes in the transmission staging buffer which have
        // not yet been passed to the low-level interface (e.g., devices driver).
        // Note that there may already be "bytes in flight" that have left this
        // buffer, but not yet been received by the remote.
        virtual size_t getRecvBufferBytesPending(void);

        // Gets the number of bytes available in the transmission staging buffer.
        // This is the maximum number of bytes that should be sent at once, even
        // though the data block may be sent directly to the interface when
        // possible.
        virtual size_t getXmitBufferBytesFree(void);

        // Gets the number of bytes in the transmission staging buffer which have
        // not yet been passed to the low-level interface (e.g., devices driver).
        // Note that there may already be "bytes in flight" that have left this
        // buffer, but not yet been received by the remote.
        virtual size_t getXmitBufferBytesPending(void);

        // Drops any content in the receiving buffer.  Normally used after:
        //
        // - initial connections to devices which do not restart their
        //   communications state machines
        // 
        // - resumption from suspend
        //
        // - baud rate changes
        virtual void purgeRx(void);

        // Synchronous non-blocking read.
        // Copies the amount of data requested, or the maximum amount of data
        // currently available, from the receiver staging buffer into the target
        // buffer provided, returning the number of bytes read.
        virtual size_t read(guarded_buffer_t &target, size_t maxBytesP)=0;

        // Asynchronous (non-blocking) send.
        // Copies the source buffer to a transmission staging buffer if it cannot be
        // sent immediately.  Returns the number of bytes staged into the buffer--
        // this will always match, as the function throws an exception if there is
        // not enough room to do this.
        //
        // THROWS
        //    BufferFullException
        //       The block requested was larger than the available free space in
        //       the transmission buffer.  NO bytes have been staged-- wait, then
        //       retry, or check getXmitBufferBytesFree and send a smaller block.
        //   
        //    ConnectionClosedException
        //       Nothing can be transmitted because the connection is closed.
        //       Either reopen it and try again, or abort the operation.
        virtual size_t write(const guarded_buffer_t &pSrcP, size_t bytesP)=0;

        // Synchronous (blocking) send.
        // Sends a block of data on the channel, and does not return until
        // the block is sent, the timeout expires, or an error occurs.
        //
        // If all goes well:
        // - bytesSentR will be set to bytesToSendP (the operation always
        //   tries to complete until an exception occurs or the timeout expires.)
        // - The function returns true.
        //
        // If the timeout expires:
        // - bytesSentR will be set to the number of bytes transmitted to the
        //   supporting system transmission buffers (which should be the number
        //   that make it to the other endpoint, unless something else goes wrong).
        // - The function throws XmitTimeoutExpiredException in the thread in
        //   which it was called.
        //
        // If another type of TRANSMISSION exception occurs:
        // - bytesSentR will be set to the number of bytes known to have been
        //   successfully transmitted to the supporting system transmission buffers
        //   before the error occurred.
        // - The registered transmission exception handler does NOT run.
        // - Instead, the exception is thrown in the same thread in which this
        //   function was called.
        // 
        // If a CHANNEL exception occurs:
        // - The registered channel exception handler runs.
        // - The function sets bytesSentR as it is best able, but this value should
        //   not be relied upon.
        // - The function returns false.
        //
        // THROWS
        //    BufferFullException
        //       The block requested was larger than the available free space in
        //       the transmission buffer.  NO bytes have been staged-- wait, then
        //       retry, or check getXmitBufferBytesFree and send a smaller block.
        //   
        //    ConnectionClosedException
        //       Nothing can be transmitted because the connection is closed.
        //       Either reopen it and try again, or abort the operation.
        //       bytesSentR should not be relied upon.
        //
        //    TimeoutExpiredException
        //       The total time specified has elapsed; any remaining data in the
        //       transmission buffer remains, and may be transmitted in the future.
        //       bytesSentR should not be relied upon.
        // 
        // THROWS:
        // - XmitTimeoutExpiredException if the timeout expires.
        // - ConnectionClosedException if the connection was closed unexpectedly.
        /*
        virtual bool sendSync(
            guarded_buffer_t    *pSrcP,
            size_t              bytesToSendP,
            usafw_timeout_t     timeoutP,
            size_t              &bytesSentR)=0;
        */

        // RECEIVE worker thread core.  Should be overridden; the default
        // implementation simply waits for a shutdown request.
        virtual void recvWorkerThreadCore(void);

        // RECEIVE worker thread core wrapper.
        static void *recvWorkerThreadCore_wrapper(void *pvThisP);

        // TRANSMIT worker thread core.  Should be overridden; the default
        // implementation simply waits for a shutdown request.
        virtual void xmitWorkerThreadCore(void);

        // TRANSMIT worker thread core wrapper.
        static void *xmitWorkerThreadCore_wrapper(void *pvThisP);

        // Starts the transmit and receive worker threads.  These threads must be
        // running to allow proper decoupling buffering and callback behavior.
        virtual void startWorkerThreads(void);

        // Signals all asynchronous background threads for this object to terminate,
        // normally in preparation for destruction.  This function does not block.
        // Call joinWorkerThreads after calling this function.    
        virtual void stopWorkerThreads(void);

        // Waits for all asynchronous background threads for this obect to terminate,
        // then "joins" them to the calling thread, deallocating them.  Call
        // stopWorkerThreads before calling this function.  This function blocks, and
        // does not take a timeout.
        virtual void joinWorkerThreads(void);

    protected:
        // Logger.
        Logger          &m_logger;

        // Name of the channel, mostly used for debugging.  Should be unique, but this is not enforced.
        char            m_channel_name[USAFW_SHORT_STRMAX];

        // True if and only if the channel is open with a connection established, else false.
        bool            m_connected;

        /* Asynchronous exception handler type definition.  This handler runs in case
         * of an asynchronous channel exception, such as unexpected closure or
         * loss of physical media.
         */
        propercomms_exception_handler_t m_async_exception_handler;

        // User data for the channel exception handler.
        void            *m_async_exception_user_data;
                
        // Data received handler type definition.  This handler runs when a block
        // of ANY size is received.  That is to say:
        // - The buffer may not be completely full
        // - The buffer may not end in a useful token, such as a line feed or carriage return
        // - The buffer may end in a carriage return even though a line feed is still pending
        propercomms_recv_handler_t m_on_recv_handler;

        // User data for the receive complete handler.
        void            *m_on_recv_user_data;

        // Transmission job complete handler type definition.  This handler runs
        // when a buffer is successfully transmitted.
        propercomms_xmit_handler_t m_on_xmit_handler;

        // User data for the transmission complete handler.
        void            *m_on_xmit_user_data;

        // This flag is set to false if the Receive worker thread may proceed,
        // and true if it should shut down when possible.
        bool            m_recv_shutdown_requested;

        // This flag is set when the Receive thread has shut down.
        bool            m_recv_shutdown_complete;

        // This flag is set to false if the Transmit worker thread may proceed,
        // and true if it should shut down when possible.
        bool            m_xmit_shutdown_requested;

        // This flag is set when the Transmit thread has shut down.
        bool            m_xmit_shutdown_complete;

#ifdef USAFW_THREAD_TYPE_POSIX
        // Receive worker thread.  Gradually acquires and stages incoming data
        // from the lower-level driver or interface.
        pthread_t       m_recv_thread;

        // Transmit worker thread.  Gradually unloads the transmission buffer
        // into the lower-level driver or interface.
        pthread_t       m_xmit_thread;
#else
    #error "ERROR: ProperCommBase needs to be extended to use a portable threading solution"
#endif
        
        // RECEIVING buffer-- IN to this object, FROM media.
        ProperBuffer    *p_recv_buffer;

        // TRANSMISSION buffer-- OUT from this object, TO media.
        ProperBuffer    *p_xmit_buffer;        

        
        /*
        // Size of each transmission buffer in bytes, not including the header.
        int     m_recv_buffer_size_bytes;

        // Size of each transmission buffer in bytes, not including the header.
        int     m_xmit_buffer_size_bytes;

        // Number of transmission buffers.
        int     m_xmit_buffer_count;

        // Number of transmission buffers pending transmission, including any current
        // buffer that has been partially transmitted.
        int     m_xmit_buffers_pending;

        // Index of the next transmission buffer to process, if and only if
        // m_xmit_buffers_pending is greater than zero.
        int     m_xmit_ring_head_index;

        // Index of the next transmission buffer available to use for staging,
        // if and only if m_xmit_buffers_pending < m_xmit_buffers_count.
        int     m_xmit_ring_tail_index;


        // Number of reception buffers.
        int     m_recv_buffer_count;

        // Number of receiving buffers pending user readout, including any current
        // buffer that has been partially filled.
        int     m_recv_buffers_pending;

        // Index of the next receiving buffer pending user readout, if and only if
        // m_recv_buffers_pending is greater than zero.
        int     m_recv_ring_head_index;

        // Index of the next receiving buffer available to use for staging incoming
        // data, if and only if m_recv_buffers_pending < m_recv_buffers_count.
        int     m_recv_ring_tail_index;

        // Transmission buffer bulk block.  Used to allocate and deallocate overall
        // transmission buffer storage.
        void *p_xmit_bulk_block;

        // Receive buffer bulk block.  Used to allocate and deallocate overall reception
        // buffer storage.
        void *p_recv_bulk_block;
        */
    };
}

#endif
