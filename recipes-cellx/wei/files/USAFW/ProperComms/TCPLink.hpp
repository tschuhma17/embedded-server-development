/* USAFW/ProperComms/TCPLink.hpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#ifndef __TCPLink_hpp__
#define __TCPLink_hpp__

#include <iostream>

#ifdef USAFW_SOCKET_STRATEGY_BOOST
#include <boost/array.hpp>
#include <boost/asio.hpp>
#endif

#include <string>
#include <vector>

#include "USAFW/util/Lockable.hpp"
#include "USAFW/ProperComms/ProperCommBase.hpp"

#ifdef USAFW_SOCKET_STRATEGY_BOOST
using boost::asio::ip::tcp;
#endif

namespace USAFW {

    class TCPLink : public ProperCommBase {
    public:
        // Constructor
        TCPLink(Logger                          &oLoggerP,
                const std::string               channelNameP,
                propercomms_exception_handler_t exceptionHandlerP,
                void                            *pExceptionUserDataP,
                propercomms_recv_handler_t      onRecvHandlerP,
                void                            *pOnRecvUserDataP,
                propercomms_xmit_handler_t      onXmitHandlerP,
                void                            *pOnXmitUserDataP,                
                size_t                          recvBufSizeBytesP,
                size_t                          xmitBufSizeBytesP);

        // Explicitly delete the copy constructor.
        TCPLink(TCPLink &src)=delete;

        // Destructor
        ~TCPLink();

        // Start the receive and transmit worker threads.
        void startWorkerThreads(void);

        // Shut down the receive and transmit worker threads.
        void stopWorkerThreads(void);
    
        // Closes the channel immediately.  This function is synchronous.
        // WARNING - ANY UNTRANSMITTED DATA WILL BE LOST
        // WARNING - ANY UNREAD RECEIVED DATA WILL BE LOST
        void close(void);

        // Specifies a server and port to connect to, and opens a connection.
        void open(const std::string serverNameP, int nPortP);

        // Specifies a server and port to connect to as a URI, of the form:
        //      tcp://fooserver:1234
        // THROWS
        //   ParsingFailedException
        //      The string was not a valid (or supported) URI.
        void open(const std::string uriP);

        // Synchronous non-blocking read.
        // Copies the amount of data requested, or the maximum amount of data
        // currently available, from the receiver staging buffer into the target
        // buffer provided, returning the number of bytes read.
        size_t read(guarded_buffer_t &targetP, size_t maxBytesP);

        // Synchronous non-blocking read.
        // Copies the amount of data requested, or the maximum amount of data
        // currently available, from the receiver staging buffer into the target
        // buffer provided, returning the number of bytes read.
        //
        // WARNING - the guarded_buffer variant of this function should be
        // preferred in almost all cases.
        size_t read(uint8_t *pTargetP, size_t maxBytesP);

        // RECEIVE worker thread core wrapper.
        static void *recvWorkerThreadCore_wrapper(void *pvThisP);

        // RECEIVE worker thread core.  Copies any available data from the socket
        // into the receiving buffer, presuming space is available.
        virtual void recvWorkerThreadCore(void);

        // TRANSMIT worker thread core wrapper.
        static void *xmitWorkerThreadCore_wrapper(void *pvThisP);

        // TRANSMIT worker thread core.  Copies any available data from the
        // transmission buffer to the socket, presuming the socket can accept
        // the write.
        virtual void xmitWorkerThreadCore(void);

        // Asynchronous (non-blocking) send.
        // Copies the source buffer to a transmission staging buffer if it cannot be
        // sent immediately.  Returns the number of bytes staged into the buffer--
        // this will always match, as the function throws an exception if there is
        // not enough room to do this.
        //
        // THROWS
        //    BufferFullException
        //       The block requested was larger than the available free space in
        //       the transmission buffer.  NO bytes have been staged-- wait, then
        //       retry, or check getXmitBufferBytesFree and send a smaller block.
        //   
        //    ConnectionClosedException
        //       Nothing can be transmitted because the connection is closed.
        //       Either reopen it and try again, or abort the operation.
        size_t write(const guarded_buffer_t &pSrcP, size_t bytesP);

        // Convenience wrapper for write that packages a C string for transport.
        // NOTE: The string is sent WITHOUT a trailing NULL.
        // For convenience, use this calling convention for literals:
        //
        //    write(LITSTR("Hi there"));
        //
        // ...which will expand out using sizeof instead of strlen.
        size_t write(const char *pSrcStrP, size_t bytesP);

        // Unguarded version of write.  Avoid.
        size_t write(void *pSrcP, size_t bytesP);

        // Signals all asynchronous background threads for this object to terminate,
        // normally in preparation for destruction.  This function does not block.
        // Call joinWorkerThreads after calling this function.    
        //virtual void stopWorkerThreads(void);

        // Waits for all asynchronous background threads for this obect to terminate,
        // then "joins" them to the calling thread, deallocating them.  Call
        // stopWorkerThreads before calling this function.  This function blocks, and
        // does not take a timeout.
        //virtual void joinWorkerThreads(void);

    protected:
        // Target server address.  May be either a resolvable string, or dot notation.
        std::string     m_remote_server_name;

        // Target TCP port.
        int             m_remote_server_port;

#ifdef USAFW_SOCKET_STRATEGY_BOOST
        // Boost asynchronous I/O context.
        boost::asio::io_context     m_boost_io_context;

        // Boost TCP socket
        tcp::socket                 *p_boost_socket;
#endif
#ifdef USAFW_SOCKET_STRATEGY_BERKELEY
        // Socket file descriptor.
        int             m_socket_fd;
#endif

    };

}

#endif