/* ProperCommBase.cpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#include <unistd.h>

#include <string>
#include <string.h>

#include "USAFW/exceptions/LogicExceptions.hpp"
#include "USAFW/memory/RingBuffer.hpp"
#include "USAFW/sync/Semaphore.hpp"
#include "USAFW/util/Logger.hpp"
#include "USAFW/util/math.hpp"
#include "USAFW/ProperComms/ProperCommBase.hpp"

namespace USAFW {

    ProperCommBase::ProperCommBase(
                        Logger                          &oLoggerP,
                        const std::string               channelNameP,
                        propercomms_exception_handler_t exceptionHandlerP,
                        void                            *pExceptionUserDataP,
                        propercomms_recv_handler_t      onRecvHandlerP,
                        void                            *pOnRecvUserDataP,
                        propercomms_xmit_handler_t      onXmitHandlerP,
                        void                            *pOnXmitUserDataP,
                        size_t                          recvBufSizeBytesP,
                        size_t                          xmitBufSizeBytesP) :
        m_logger(oLoggerP),
        m_connected(false),
        m_async_exception_handler(exceptionHandlerP),
        m_async_exception_user_data(pExceptionUserDataP),
        m_on_recv_handler(onRecvHandlerP),
        m_on_recv_user_data(pOnRecvUserDataP),
        m_on_xmit_handler(onXmitHandlerP),
        m_on_xmit_user_data(pOnXmitUserDataP),
        m_recv_shutdown_requested(false),
        m_recv_shutdown_complete(true),
        m_xmit_shutdown_requested(false),
        m_xmit_shutdown_complete(true),
        m_recv_thread(getInvalidPthread()),
        m_xmit_thread(getInvalidPthread()),
        p_recv_buffer(new RingBuffer(oLoggerP, recvBufSizeBytesP)),
        p_xmit_buffer(new RingBuffer(oLoggerP, recvBufSizeBytesP))
    {
        strncpy(m_channel_name, channelNameP.c_str(), MIN(USAFW_SHORT_STRMAX, channelNameP.length()));
    }

    
    ProperCommBase::~ProperCommBase() {
        BEGIN_LOCKABLE_DESTRUCTOR
        stopWorkerThreads();
        joinWorkerThreads();
        delete p_xmit_buffer;
        p_xmit_buffer=NULL;
        delete p_recv_buffer;
        p_recv_buffer=NULL;
        END_LOCKABLE_DESTRUCTOR
    }


    size_t ProperCommBase::getRecvBufferBytesFree(void) {
        LOCK();
        size_t ret = p_recv_buffer->getBytesFree();
        UNLOCK();
        return ret;
    }


    size_t ProperCommBase::getRecvBufferBytesPending(void) {
        LOCK();
        size_t ret = p_recv_buffer->getBytesPending();
        UNLOCK();
        return ret;
    }


    size_t ProperCommBase::getXmitBufferBytesFree(void) {
        LOCK();
        size_t ret = p_xmit_buffer->getBytesFree();
        UNLOCK();
        return ret;
    }


    size_t ProperCommBase::getXmitBufferBytesPending(void) {
        LOCK();
        size_t ret = p_xmit_buffer->getBytesPending();
        UNLOCK();
        return ret;
    }

    void ProperCommBase::purgeRx(void) {
        AUTOLOCK;
        p_recv_buffer->purge();
    }

    void ProperCommBase::recvWorkerThreadCore(void) {
        while(!m_recv_shutdown_requested) {
            // Just wait 250ms.
            usleep(250000);
        }
        m_recv_shutdown_complete = true;
    }

    void *ProperCommBase::recvWorkerThreadCore_wrapper(void *pvThisP) {
        static_cast<ProperCommBase*>(pvThisP)->recvWorkerThreadCore();
        return NULL;
    }


    void ProperCommBase::xmitWorkerThreadCore(void) {
        while(!m_xmit_shutdown_requested) {
            // Just wait 250ms.
            usleep(250000);
        }
        m_xmit_shutdown_complete = true;
    }

    void *ProperCommBase::xmitWorkerThreadCore_wrapper(void *pvThisP) {
        static_cast<ProperCommBase *>(pvThisP)->xmitWorkerThreadCore();
        return NULL;
    }


    void ProperCommBase::startWorkerThreads(void) {
        int err;
        
        pthread_attr_t thread_attributes;

        // Specify any special thread attributes needed.
        err=pthread_attr_init(&thread_attributes);

        // Start the RECEIVER worker thread.
        err=pthread_create(&m_recv_thread, &thread_attributes, ProperCommBase::recvWorkerThreadCore_wrapper, this);

        // Start the TRANSMIT worker thread.
        err=pthread_create(&m_recv_thread, &thread_attributes, ProperCommBase::xmitWorkerThreadCore_wrapper, this);
    }


    void ProperCommBase::stopWorkerThreads(void) {
        LOCK();
        m_recv_shutdown_requested=true;
        m_xmit_shutdown_requested=true;
        UNLOCK();
    }


    void ProperCommBase::joinWorkerThreads(void) {
        #ifdef USAFW_THREAD_TYPE_POSIX
            void    *retval;
            int     err;

            // Join the RECEIVING thread, if and only if it is running.
            if (pthread_equal(m_recv_thread, getInvalidPthread())==0) {
                err=pthread_join(m_recv_thread, &retval);
                switch(err) {
                    case 0:
                        // Success.
                        m_recv_thread=getInvalidPthread();
                        break;

                    case EDEADLK:
                        // EITHER:
                        //   A deadlock was detected
                        // OR:
                        //   The attempt is to join the calling thread.
                        THROW(WouldDeadlock);
                        break;

                    case EINVAL:
                        // Thread is either not joinable, or another thread is already waiting to join it.
                        THROW(NotJoinable);
                        break;

                    case ESRCH:
                        // The thread could not be found.
                        THROW(NoSuchObject);
                        break;

                    default:
                        // Unexpected return code.
                        THROW(UnknownResponse);
                }
            }

            // Join the TRANSMITTING thread, if and only if it is running.
            if (pthread_equal(m_xmit_thread, getInvalidPthread())==0) {
                err=pthread_join(m_xmit_thread, &retval);
                switch(err) {
                    case 0:
                        // Success.
                        m_xmit_thread=getInvalidPthread();
                        break;

                    case EDEADLK:
                        // EITHER:
                        //   A deadlock was detected
                        // OR:
                        //   The attempt is to join the calling thread.
                        THROW(WouldDeadlock);
                        break;

                    case EINVAL:
                        // Thread is either not joinable, or another thread is already waiting to join it.
                        THROW(NotJoinable);
                        break;

                    case ESRCH:
                        // The thread could not be found.
                        THROW(NoSuchObject);
                        break;

                    default:
                        // Unexpected return code.
                        THROW(UnknownResponse);
                }
            }

        #else
            #error "ERROR: ProperCommBase needs to be extended to use a portable threading solution"
        #endif

    }

}