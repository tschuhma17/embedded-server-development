/* src/USAFW/ProperComms/TCPLink.cpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <unistd.h>

#include <algorithm>
#include <string>

#include "USAFW/exceptions/CommExceptions.hpp"
#include "USAFW/exceptions/LogicExceptions.hpp"
#include "USAFW/exceptions/MemoryExceptions.hpp"
#include "USAFW/ProperComms/TCPLink.hpp"
#include "USAFW/util/math.hpp"

namespace USAFW {

    // Constructor
    TCPLink::TCPLink(
        Logger                          &oLoggerP,
        const std::string               channelNameP,
        propercomms_exception_handler_t exceptionHandlerP,
        void                            *pExceptionUserDataP,
        propercomms_xmit_handler_t      onXmitHandlerP,
        void                            *pOnXmitUserDataP,                
        propercomms_recv_handler_t      onRecvHandlerP,
        void                            *pOnRecvUserDataP,
        size_t                          xmitBufSizeBytesP,
        size_t                          recvBufSizeBytesP)
        : ProperCommBase(oLoggerP, channelNameP, exceptionHandlerP, pExceptionUserDataP,
                        onXmitHandlerP, pOnXmitUserDataP, onRecvHandlerP, pOnRecvUserDataP,
                        xmitBufSizeBytesP, recvBufSizeBytesP),
            m_remote_server_name("localhost"),
            m_remote_server_port(0),
            m_socket_fd(-1)
    {
        m_logger.info("TCPLink constructed");
        ;
    }


    TCPLink::~TCPLink() {
        // TODO - REVIEW
        if ((!m_xmit_shutdown_complete)||(!m_recv_shutdown_complete)) {
            close();
        }
        m_logger.info("TCPLink destroyed");
    }


    void TCPLink::startWorkerThreads(void) {
        int err;
        
        pthread_attr_t thread_attributes;

        // Specify any special thread attributes needed.
        err=pthread_attr_init(&thread_attributes);

        // Start the RECEIVER worker thread.
        err=pthread_create(&m_recv_thread, &thread_attributes, TCPLink::recvWorkerThreadCore_wrapper, this);

        // Start the TRANSMIT worker thread.
        err=pthread_create(&m_xmit_thread, &thread_attributes, TCPLink::xmitWorkerThreadCore_wrapper, this);
        m_logger.info("TCPLink started");
    }


    void TCPLink::stopWorkerThreads(void) {
        LOCK();
        m_recv_shutdown_requested = true;
        m_xmit_shutdown_requested = true;
        UNLOCK();

        while ((!m_xmit_shutdown_complete)||(!m_recv_shutdown_complete)) {
            // Wait for both comm threads to shut down.
            usleep(250000);
        }
        m_logger.info("TCPLink ended");
    }


    void TCPLink::close(void) {
        stopWorkerThreads();
        LOCK();
        if (m_socket_fd>0) {
            ::close(m_socket_fd);
            m_socket_fd=-1;
        }
        m_connected=false;
        UNLOCK();
    }



#ifdef USAFW_SOCKET_STRATEGY_BERKELEY
    void TCPLink::open(const std::string targetP) {
        AUTOLOCK;

        if (m_connected) {
            THROW(ConnectionAlreadyOpen);
        }

        // Verify that the TRANSPORT, if specified, is supported by this class.
        // The only supported transport is TCP.
        int transport_separator_index=targetP.find("://");
        if (std::string::npos!=transport_separator_index) {
            std::string transport=targetP.substr(0, transport_separator_index);
            // Convert to lower case
            std::transform(transport.begin(), transport.end(), transport.begin(), ::tolower);
            if (0!=transport.compare("tcp")) {
                THROW(UnsupportedTransport);
            }
        }

        // A TCP PORT is required.  This is expected to be specified after a colon.
        int port_separator_index=targetP.rfind(":");
        if (std::string::npos==port_separator_index) {
            THROW(InvalidParameter);
        }

        // NOTE: IPv6 addresses are expected to be within brackets, like this:
        //  http://[2001:db8:1f70::999:de8:7648:6e8]:100
        m_remote_server_name = targetP.substr(transport_separator_index+3, port_separator_index-(transport_separator_index+3));
        
        // Strictly speaking, this can be a decimal port number, or a service name.
        // In practice, nobody is likely to use the service name.
        std::string port_name = targetP.substr(port_separator_index+1, strnlen(targetP.c_str(), targetP.length()));

        // FIXME-- try to handle ASCII service names anyway (this will throw).
        m_remote_server_port = std::stoi(port_name);

        // TODO FIXME
        // DNS resolutions are complex, and require additional support.
        // At this time, this class only supports numerics and simple lookups.
        struct addrinfo*    addresses;
        struct addrinfo     hints;
        int                 error;

        memset(&hints, 0, sizeof(hints));

        // Get an IPv4  address.  (FIXME, consider AF_UNSPEC; review)
        hints.ai_family=AF_INET;

        // Specify a TCP connection.
        hints.ai_socktype=SOCK_STREAM;

        // Accept any protocol in the response.
        hints.ai_protocol=0;

        // Ensure that IPv6 addresses will only be returned on hosts that have
        // an IPv6 interface configured (likewise for IPv4).
        hints.ai_flags=AI_CANONNAME | AI_ALL | AI_ADDRCONFIG;

        // Resolve the hostname into a list of addresses, one of which will
        // be the canonical address.
        error=getaddrinfo(m_remote_server_name.c_str(), port_name.c_str(), &hints, &addresses);
        switch(error) {
            case 0:
                // Success.
                break;

            case EAI_SYSTEM:
            default:
                m_logger.error(std::string("TCPLink::open:getaddrinfo failed: ")+strerror(errno));
                THROW(NameResolutionFailed);
                break;
        }

        // Try to just use the first result.
        m_socket_fd=socket(addresses->ai_family, addresses->ai_socktype, addresses->ai_protocol);
        if (-1==m_socket_fd) {
            switch(errno) {
                case EAFNOSUPPORT:
                    m_logger.error("socket(): EAFNOSUPPORT");
                    break;

                case EMFILE:
                    m_logger.error("socket(): EMFILE");
                    break;
                    
                case ENFILE:
                    m_logger.error("socket(): ENFILE");
                    break;
                    
                case EPROTONOSUPPORT:
                    m_logger.error("socket(): EPROTONOSUPPORT");
                    break;
                    
                case EPROTOTYPE:
                    m_logger.error("socket(): EPROTOTYPE");
                    break;
                    
                case EACCES:
                    m_logger.error("socket(): EACCES");
                    break;
                    
                case ENOBUFS:
                    m_logger.error("socket(): ENOBUFS");
                    break;
                    
                case ENOMEM:
                    m_logger.error("socket(): ENOMEM");
                    break;
                    
                default:
                    m_logger.error("socket(): unknown errno");
            }
            THROW(ConnectionFailed);
        }

        switch(connect(m_socket_fd, addresses->ai_addr, addresses->ai_addrlen)) {
            case -1:
                // Failure
                switch(errno) {
                    case EADDRNOTAVAIL:
                        m_logger.error("socket(): EADDRNOTAVAIL");
                        break;
                        
                    case EAFNOSUPPORT:
                        m_logger.error("socket(): EAFNOSUPPORT");
                        break;
                        
                    case EALREADY:
                        m_logger.error("socket(): EALREADY");
                        break;
                        
                    case EBADF:
                        m_logger.error("socket(): EBADF");
                        break;
                        
                    case ECONNREFUSED:
                        m_logger.error("socket(): ECONNREFUSED");
                        break;
                        
                    case EINPROGRESS:
                        m_logger.error("socket(): EINPROGRESS");
                        break;
                        
                    case EINTR:
                        m_logger.error("socket(): EINTR");
                        break;
                        
                    case EISCONN:
                        m_logger.error("socket(): EISCONN");
                        break;
                        
                    case ENETUNREACH:
                        m_logger.error("socket(): ENETUNREACH");
                        break;
                        
                    case ENOTSOCK:
                        m_logger.error("socket(): ENOTSOCK");
                        break;
                        
                    case EPROTOTYPE:
                        m_logger.error("socket(): EPROTOTYPE");
                        break;
                        
                    case ETIMEDOUT:
                        m_logger.error("socket(): ETIMEDOUT");
                        break;
                        
                    case EIO:
                        m_logger.error("socket(): EIO");
                        break;
                        
                    case ELOOP:
                        m_logger.error("socket(): ELOOP");
                        break;
                        
                    case ENAMETOOLONG:
                        m_logger.error("socket(): ENAMETOOLONG");
                        break;
                        
                    case ENOENT:
                        m_logger.error("socket(): ENOENT");
                        break;

                    case ENOTDIR:
                        m_logger.error("socket(): ENOTDIR");
                        break;

                    case EACCES:
                        m_logger.error("socket(): EACCES");
                        break;

                    case EADDRINUSE:
                        m_logger.error("socket(): EADDRINUSE");
                        break;

                    case ECONNRESET:
                        m_logger.error("socket(): ECONNRESET");
                        break;

                    case EHOSTUNREACH:
                        m_logger.error("socket(): EHOSTUNREACH");
                        break;

                    case EINVAL:
                        m_logger.error("socket(): EINVAL");
                        break;

                    case ENETDOWN:
                        m_logger.error("socket(): ENETDOWN");
                        break;

                    case ENOBUFS:
                        m_logger.error("socket(): ENOBUFS");
                        break;

                    case EOPNOTSUPP:
                        m_logger.error("socket(): EOPNOTSUPP");
                        break;

                    default:
                        m_logger.error("socket(): unknown errno");
                }
                THROW(ConnectionFailed);
                break;

            case 0:
                // Success
                break;

            default:
                m_logger.error("connect(): unknown return code");
                THROW(ConnectionFailed);
        }

        m_connected=true;

        freeaddrinfo(addresses);

        startWorkerThreads();
    }
#endif

    size_t TCPLink::read(guarded_buffer_t &pTargetP, size_t maxBytesP) {
        AUTOLOCK;

        size_t  bytes_read=0;

        try {
            guardedBuffer_integrity_check(&pTargetP);
            bytes_read=p_recv_buffer->read(pTargetP.p_data, maxBytesP);
            guardedBuffer_integrity_check(&pTargetP);
        } catch (std::exception_ptr ex) {
            std::rethrow_exception(ex);
        }

        return bytes_read;
    }

    // WARNING - the guarded_buffer variant of this function should be
    // preferred in almost all cases.
    size_t TCPLink::read(uint8_t *pTargetP, size_t maxBytesP) {
        AUTOLOCK;

        size_t  bytes_read=0;

        try {
            // TODO - assert not null
            bytes_read=p_recv_buffer->read(pTargetP, maxBytesP);
        } catch (std::exception_ptr ex) {
            std::rethrow_exception(ex);
        }

        return bytes_read;
    }

    size_t TCPLink::write(const guarded_buffer_header_t &oSrcP, size_t bytesP) {
        AUTOLOCK;

        // Copy data into the staging buffer, so that we don't have to wait for
        // the actual device transmit buffer-- which may be very small-- to have
        // space available.  The background worker thread will then shovel it
        // out the wires as that becomes possible.
        //
        // First, ensure that enough space remains in the staging buffer.  The
        // caller is supposed to check first, so throw an exception if there
        // isn't.
        if (bytesP>getXmitBufferBytesFree()) {
            THROW(WouldOverrun);
        }

        guardedBuffer_integrity_check(&oSrcP);
        p_xmit_buffer->integrityCheck();
        p_xmit_buffer->write(oSrcP.p_data, oSrcP.m_bytes_full);
        p_xmit_buffer->integrityCheck();
        guardedBuffer_integrity_check(&oSrcP);
        
        return bytesP;
    }

    size_t TCPLink::write(const char *pSrcStrP, size_t bytesP) {
        AUTOLOCK;

        if (bytesP>getXmitBufferBytesFree()) {
            THROW(WouldOverrun);
        }

        p_xmit_buffer->integrityCheck();
        p_xmit_buffer->write(pSrcStrP, bytesP);
        p_xmit_buffer->integrityCheck();

        return bytesP;
    }

    size_t TCPLink::write(void *pSrcP, size_t bytesP) {
        AUTOLOCK;

        if (bytesP>getXmitBufferBytesFree()) {
            THROW(WouldOverrun);
        }
        p_xmit_buffer->integrityCheck();
        p_xmit_buffer->write(pSrcP, bytesP);
        p_xmit_buffer->integrityCheck();

        return bytesP;
    }

    // RECEIVE worker thread core wrapper.
    void *TCPLink::recvWorkerThreadCore_wrapper(void *pvThisP) {
        static_cast<TCPLink*>(pvThisP)->recvWorkerThreadCore();
        return NULL;
    }


    // RECEIVE worker thread core.  Copies any available data from the socket
    // into the receiving buffer, presuming space is available.
    void TCPLink::recvWorkerThreadCore(void) {
        // Work with up to 4K at a time.
        USAFW::guarded_buffer_t *p_staging_buffer = guardedBuffer_new(4096);
        guardedBuffer_integrity_check(p_staging_buffer);

        while(!m_recv_shutdown_requested) {          
            //std::cout << "recvWorkerThreadCore: TOP" << std::endl;
            int result;
            fd_set readfds;
            struct timeval tv_timeout;

            // If the socket is closed, kill time until it is reopened, or we are
            // ordered to shut down.
            if (!m_connected) {
                usleep(250000);
                continue;
            }

            // Just read from the one socket.
            FD_ZERO(&readfds);
            FD_SET(m_socket_fd, &readfds);

            // Wait up to a quarter second for the buffer to clear.
            tv_timeout.tv_sec=0;
            tv_timeout.tv_usec=250000;

            // Wait for traffic or timeout.
            result = select(m_socket_fd+1, &readfds, NULL, NULL, &tv_timeout);
            switch(result) {
                case 1:
                    // Data is ready to read.
                    if (FD_ISSET(m_socket_fd, &readfds)) {
                        // Read data, if available, up to what will fit in the main receiving buffer.
                        ssize_t bytes_read = ::recv(m_socket_fd, p_staging_buffer->p_data,
                                                    MIN(p_staging_buffer->m_bytes_allocated, p_recv_buffer->getBytesFree()),
                                                    MSG_DONTWAIT);
                        if (0==bytes_read) {
                            // Socket has closed.
                            // FIXME: Review other cases where this might return 0
                            AUTOLOCK;
                            m_connected = false;
                            // Should be redundant.
                            ::close(m_socket_fd);
                            m_socket_fd=-1;
                        } else if (bytes_read<0) {
                            // An error has occurred.
                            switch (errno) {
                                case EAGAIN:
#if (EAGAIN!=EWOULDBLOCK)
                                // (Some POSIX platforms define this differently, since strictly speaking it is
                                // not quite the same error, but we handle it the same way.  On Linux, these
                                // expand to the same value, which would cause an error in the switch statement.)
                                case EWOULDBLOCK:
#endif
                                    // No data was available.  Weird, but can happen.  Just continue.
                                    break;

                                case EBADF:
                                case EFAULT:
                                case EINVAL:
                                case ENOTSOCK:
                                    THROW(InvalidParameter);
                                    break;

                                case ENOMEM:
                                    THROW(OutOfMemory);

                                case ECONNREFUSED:
                                case ENOTCONN:
                                default:
                                    THROW(Communications);
                            }
                        } else {
                            AUTOLOCK;
                            // Valid read; post to buffer.
                            p_staging_buffer->m_bytes_full = bytes_read;
                            p_recv_buffer->write(p_staging_buffer->p_data, bytes_read);
                            m_on_recv_handler(this, m_on_recv_user_data);
                        }
                        
                    } else {
                        // Really should never happen...
                        m_logger.warning("TCPLink::recvWorkerThreadCore: recv fd mismatch?");
                    }
                    break;

                case 0:
                    // select() timeout expired.
                    break;

                case EBADF:
                    // Bad file descriptor.
                    // This really can only mean that the connection has been closed...
                    THROW(ConnectionClosed);
                    break;

                case EINTR:
                    // The function was interrupted before anything useful could happen.
                    // Just try again on the next loop.
                    m_logger.warning("TCPLink::recvWorkerThreadCore: select interrupted");
                    break;

                case EINVAL:
                    // Invalid parameter.
                    THROW(InvalidParameter);
                    break;

                default:
                    // Shouldn't happen... try to proceed
                    m_logger.error("TCPLink::recvWorkerThreadCore: unknown error");
                    break;
            } // END select() switch
        } // END while(!m_recv_shutdown_requested) 
        guardedBuffer_integrity_check(p_staging_buffer);
        guardedBuffer_delete(p_staging_buffer);
        m_recv_shutdown_complete = true;
    }


    // TRANSMIT worker thread core wrapper.
    void *TCPLink::xmitWorkerThreadCore_wrapper(void *pvThisP) {
        static_cast<TCPLink*>(pvThisP)->xmitWorkerThreadCore();
        return NULL;
    }


    // TRANSMIT worker thread core.  Copies any available data from the
    // transmission buffer to the socket, presuming the socket can accept
    // the write.
    void TCPLink::xmitWorkerThreadCore(void) {
        // Work with up to 4K at a time.
        USAFW::guarded_buffer_t *p_staging_buffer = guardedBuffer_new(4096);
        guardedBuffer_integrity_check(p_staging_buffer);

        // Allows staging buffer retransmission attempts without hitting the ring buffer more than once.
        bool retry_pending=false;

        while(!m_xmit_shutdown_requested) {          
            int result;
            fd_set writefds;
            struct timeval tv_timeout;

            if (p_xmit_buffer->getBytesPending()==0) {
                // Nothing to transmit.  Wait efficiently.
                // TODO FIXME - replace with a semaphore to reduce both latency and load
                // Sleep for 10ms.
                usleep(10000);

                // Go back to the top of the loop and check for shutdown requests.
                continue;
            }

            // If the socket is closed, kill time until it is reopened, or we are
            // ordered to shut down.
            if (!m_connected) {
                usleep(250000);
                continue;
            }

            // Read from the transmit buffer into the staging buffer, unless a retry is pending.
            if (!retry_pending) {
                p_staging_buffer->m_bytes_full = p_xmit_buffer->read(p_staging_buffer->p_data, MIN(p_xmit_buffer->getBytesPending(), p_staging_buffer->m_bytes_allocated));
            }

            // Just write to the one socket.
            FD_ZERO(&writefds);
            FD_SET(m_socket_fd, &writefds);

            // Wait up to a quarter second for the buffer to clear.
            tv_timeout.tv_sec=0;
            tv_timeout.tv_usec=250000;

            // Wait for traffic or timeout.
            result = select(m_socket_fd+1, NULL, &writefds, NULL, &tv_timeout);
            switch(result) {
                case 1:
                    // Transmit buffer is ready to send.
                    if (FD_ISSET(m_socket_fd, &writefds)) {
                        // Write data, if available, up to what will fit in the main receiving buffer.
                        ssize_t bytes_sent = ::send(m_socket_fd, p_staging_buffer->p_data,
                                                    p_staging_buffer->m_bytes_full,
                                                    MSG_DONTWAIT);
                        if (0==bytes_sent) {
                            // Socket has closed.
                            // FIXME: Review other cases where this might return 0
                            AUTOLOCK;
                            m_connected = false;
                            // Should be redundant.
                            ::close(m_socket_fd);
                            m_socket_fd=-1;
                        } else if (bytes_sent<0) {
                            // An error has occurred.
                            switch (errno) {
                                case EAGAIN:
#if (EAGAIN!=EWOULDBLOCK)
                                // (Some POSIX platforms define this differently, since strictly speaking it is
                                // not quite the same error, but we handle it the same way.  On Linux, these
                                // expand to the same value, which would cause an error in the switch statement.)
                                case EWOULDBLOCK:
#endif
                                    // No driver buffer space was available.  Weird, but can happen.
                                    // Flag the condition for retry and continue.
                                    retry_pending=true;
                                    break;

                                case EBADF:
                                case EFAULT:
                                case EINVAL:
                                case ENOTSOCK:
                                    THROW(InvalidParameter);
                                    break;

                                case ENOMEM:
                                    THROW(OutOfMemory);

                                case ECONNREFUSED:
                                case ENOTCONN:
                                default:
                                    THROW(Communications);
                            }
                        } else {
                            AUTOLOCK;
                            //std::string sentText( (const char *)p_staging_buffer->p_data, p_staging_buffer->m_bytes_full );
                            //m_logger.debug("QQ " + sentText);
                            // Valid write; clear the staging buffer.
                            retry_pending=false;
                            p_staging_buffer->m_bytes_full=0;
                        }
                        
                    } else {
                        // Really should never happen...
                        m_logger.warning("TCPLink::xmitWorkerThreadCore: recv fd mismatch?");
                    }
                    break;

                case 0:
                    // select() timeout expired.
                    break;

                case EBADF:
                    // Bad file descriptor.
                    // This really can only mean that the connection has been closed...
                    THROW(ConnectionClosed);
                    break;

                case EINTR:
                    // The function was interrupted before anything useful could happen.
                    // Just try again on the next loop.
                    m_logger.warning("TCPLink::xmitWorkerThreadCore: select interrupted");
                    break;

                case EINVAL:
                    // Invalid parameter.
                    THROW(InvalidParameter);
                    break;

                default:
                    // Shouldn't happen... try to proceed
                    m_logger.error("TCPLink::xmitWorkerThreadCore: unknown error");
                    break;
            } // END select() switch
        } // END while(!m_xmit_shutdown_requested) 
        guardedBuffer_integrity_check(p_staging_buffer);
        guardedBuffer_delete(p_staging_buffer);
        m_xmit_shutdown_complete = true;
    }


}
