/* USAFW/util/stringutils.hpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#ifndef __stringutils_hpp__
#define __stringutils_hpp__

#include <cstdint>
#include <cstring>
#include <string>

#include "USAFW/details.h"

namespace USAFW {

// This convenience macro allows proper string sizes for literal strings to be computed
// without human error.
#define LITSTR(x) x,(sizeof(x)-1)

    void _cstr_from_stdstr(char *targetP, std::string &srcP, size_t maxBytesP);

#ifdef USAFW_USE_STATIC_ASSERTS
    #ifdef USAFW_STATIC_ASSERT_TYPE_GCC
        // Copies as of the std::string in srcP as will fit into targetP, ensuring that
        // the buffer is null-terminated even if the source needs to be clipped.
        // targetByteSize should be the actual total size allocated, such that the
        // maximum number of prinitable single-byte characters that can be held in
        // targetP will be targetByteSize-1, since the final byte will hold the
        // terminating NULL.
        //
        // In your current build configuration, this variant should be protected with
        // compile-time static assertion checking.
        #define safe_cstr_from_stdstr(target,src,max_bytes)             \
            do {                                                        \
                _Static_assert(sizeof(target)>=max_bytes,               \
                    "safe_cstr_from_stdstr: target buffer size is too small for max size"); \
                _cstr_from_stdstr(target,src,max_bytes)
            } while (0)
    #else
        #error "Static asserts are enabled, but the implementation is unknown."
    #endif
#else
    // Copies as of the std::string in srcP as will fit into targetP, ensuring that
    // the buffer is null-terminated even if the source needs to be clipped.
    // targetByteSize should be the actual total size allocated, such that the
    // maximum number of prinitable single-byte characters that can be held in
    // targetP will be targetByteSize-1, since the final byte will hold the
    // terminating NULL.
    //
    // In your current build configuration, this variant IS NOT protected with
    // compile-time static assertion checking.
    #define safe_cstr_from_stdstr(target,src,max_bytes) \
        _safe_cstr_from_stdstr(target,src,max_bytes)
#endif
}

#endif
