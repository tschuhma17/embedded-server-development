/* include/USAFW/util/Lockable.hpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#ifndef __Lockable_hpp__
#define __Lockable_hpp__

#ifndef DISABLE_LOCK_DEBUGGING
#include <string>
#include <vector>
#include "include/USAFW/details.h"
#include "include/USAFW/debugging/CodePosition.hpp"
#include "include/USAFW/debugging/Debuggable.hpp"
#endif

// This utility macro should be used to lock Lockable-derived objects.  It.
// provides debugging context required to troubleshoot deadlocks and other
// problems, by tracking the file, function, line, and thread at which the
// object was locked, lock-contended, and unlocked.
//
// If USAFW_DISABLE_LOCK_DEBUGGING is defined, this macro passes directly
// through to a compact form of the supporting lock function, and position
// tracking / deadlock troubleshooting is not available.
#ifdef DISABLE_LOCK_DEBUGGING
#define LOCK() _lock()
#else
#define LOCK() _lock(HERE())
#endif

// This utility macro should be used to unlock Lockable-derived objects.  It
// provides debugging context required to troubleshoot deadlocks and other
// problems, by tracking the file, function, line, and thread at which the
// object was locked, lock-contended, and unlocked.
//
// If USAFW_DISABLE_LOCK_DEBUGGING is defined, this macro passes directly
// through to a compact form of the supporting unlock function, and position
// tracking / deadlock troubleshooting is not available.
#ifdef USAFW_DISABLE_LOCK_DEBUGGING
#define UNLOCK() _unlock()
#else
#define UNLOCK() _unlock(HERE())
#endif

// This utility macro is for use inside member functions of classes derived
// from Lockable.  It creates a local object whose construction locks the
// host object, and whose destruction unlocks it.  Because the unlock
// sequence is triggered by the object's destructor running when it goes out
// of scope, it is useful in situations where multiple points of control may
// TODO - FIXME
#define AUTOLOCK USAFW::Lockable::Autolock __autolock(*this, CodePosition(__FILE__,__func__,__LINE__));

// This macro throws an ObjectLockedException if the object is not, in fact,
// unlocked.  NOTE: This may be implemented with a "trylock" followed by an
// immediate unlock if the try succeeds.  Sequences using this function can
// result in a race condition; as such, this function is only useful as an
// assertion in defensive coding/debugging techniques.  It is particularly
// appropriate for destructors.
#define ASSERT_LOCKED() {/*"FIXME()"*/}

// This macro throws an ObjectNotLockedException if the object is not, in fact,
// locked.  NOTE: This may be implemented with a "trylock" followed by an
// immediate unlock if the try succeeds.  Sequences using this function can
// result in a race condition; as such, this function is only useful as an
// assertion in defensive coding/debugging techniques.
#define ASSERT_UNLOCKED() {/*"FIXME()"*/}

namespace USAFW {

    /** Utility class to make objects threadsafe.
     * 
     *  This class includes extensive debugging capabilities based on many
     *  years of experience troubleshooting multithreaded code.  When necessary,
     *  these features can be shut off for performance, but they are designed
     *  to be available even in a release build, when necessary.
    */
    class Lockable : public Debuggable {
    public:
        // Constructor.
        Lockable(void);

        // Destructor
        virtual ~Lockable();

        // Macro support locking function.  Do not call this directly-- use the
        // LOCK macro instead, which will provide debugging information in case
        // of a deadlock.  Note that this is a move constructor; it acquires the
        // temporary CodePosition object created by the LOCK() macro, moving it
        // into the m_last_lock_attempt latch when the attempt starts, and copying
        // it to the m_last_locker latch when (if) the attempt succeeds.
        //
        // If USAFW_DISABLE_LOCK_DEBUGGING is defined, then the function takes no
        // parameters, and these tracking latches are not available.
#ifdef USAFW_DISABLE_LOCK_DEBUGGING
        void _lock(void);
#else
        void _lock(const CodePosition &&oWhereP);
#endif

        // Macro support unlocking function.  Do not call this directly-- use the
        // UNLOCK macro instead, which will provide debugging information in case
        // of a deadlock.  Note that this is a move constructor; it acquires the
        // temporary CodePosition object created by the UNLOCK() macro, moving it
        // into the m_last_unlock_attempt latch when the attempt starts, and copying
        // it to the m_last_unlocker latch when (if) the attempt succeeds.
        //
        // If USAFW_DISABLE_LOCK_DEBUGGING is defined, then the function takes no
        // parameters, and these tracking latches are not available.
#ifdef USAFW_DISABLE_LOCK_DEBUGGING
        void _unlock(void);
#else
        void _unlock(const CodePosition &&oWhereP);
#endif

#ifndef USAFW_DISABLE_LOCK_DEBUGGING
        // Generates a human-readable debugging report about the state of the lockable
        // object; specifically:
        // - The file, function, line, and thread which last attempted to lock the object
        // - The file, function, line, and thread which last successfully locked the object
        // - The recursion depth of that lock, if recursive locking is enabled
        // - The file, function, line, and thread which last attempted to unlock the object
        // - The file, function, line, and thread which last successfully unlocked the object
        // - The total number of times the object has been locked since system startup.
        //
        // This function is not available if USAFW_DISABLE_LOCK_DEBUGGING is defined.

        std::string debugLockingState(void) const;

        // This class static method generates a human-readable list of all Lockable-derived
        // objects in the system which are currently locked.  The recommended usage is to
        // generate this report and sent it to STDERR when SIGUSR1 is received.  Developers
        // can then send SIGUSR1 to the program to generate a list of locked objects, and
        // thereby troubleshoot the deadlock.  The debug latches for each object will be
        // included in the report.
        //
        // This function is not available if USAFW_DISABLE_LOCK_DEBUGGING is defined.
        static std::string debugAllLockedObjects(void);
    #endif

        /** This helper class provides scoped, exception-safe automatic unlocking for a
         *  Lockable-derived object's methods.
         * 
         *  To use it, simply call:
         * 
         *      AUTOLOCK;
         * 
         *  At the beginning of your method.  The Autolock's constructor will lock your
         *  object, and its destructor will unlock it.
         * 
         *  To use it on an arbitrary Lockable object, instead do:
         *  
         *      Lockable::Autolock thing_lock(thing, HERE());
         * 
         *  The "HERE()" macro provides a CodePosition at that line, used for deadlock auditing.
         */
        class Autolock {
        public:
            // Constructor
            Autolock(USAFW::Lockable &whatP, CodePosition &&whereP);

            // Destructor
            virtual ~Autolock();
        
        protected:
            // The actual object projected.
            USAFW::Lockable        &m_what;
        };


        protected:
        // === CLASS DATA ===
    #ifndef USAFW_DISABLE_LOCK_DEBUGGING
    #ifdef USAFW_MUTEX_TYPE_POSIX
        // This lock is used for internal tracking operations only.  It allows the system
        // to keep track of lockable-derived objects, so that deadlock reports can be
        // automatically generated.
        static pthread_mutex_t                  m_lockable_system_lock;
    #endif
        // This class static list of lockable-derived objects is used to track Lockable-
        // derived objects, so that deadlock reports can be automatically generated.  It
        // is protected by a dedicated, class-static mutex.  Objects are added to the list
        // when constructed, and removed from it when destroyed.
        static std::vector<const Lockable *>    m_lockable_system_manifest;
    #endif

        // === INSTANCE DATA ===
    #ifndef USAFW_DISABLE_LOCK_DEBUGGING
        // File, function, line, and thread which last attempted to lock this object.
        CodePosition    m_last_lock_attempt;

        // File, function, line, and thread which last successfully locked this object.
        CodePosition    m_last_locked_at;

        // File, function, line, and thread which last attempted to unlock this object.
        CodePosition    m_last_unlock_attempt;

        // File, function, line, and thread which last successfully unlocked this object.
        CodePosition    m_last_unlocked_at;

        // The recursion depth, for recursive locks.
        unsigned int    m_lock_recursion_depth;

        // The total number of times the object has been unlocked.  Useful for profiling.
        unsigned int    m_total_cycle_count;

        // This "accounting lock" is used when pre-locking of pre-unlocking the object,
        // so that the debugging information can be reliably set even if a deadlock
        // occurs.  It is optimized out when lock debugging is disabled.
    #ifdef USAFW_MUTEX_TYPE_POSIX
        pthread_mutex_t m_accounting_lock;
    #endif
    #endif

        // This is the actual locking object used to make the object threadsafe.
    #ifdef USAFW_MUTEX_TYPE_POSIX
        pthread_mutex_t m_lock;
    #endif

        // ===LIFECYCLE STATE FLAGS AND OTHER NON-ALIGNED DATA===
        
        // True if and only if the destructor sequence is running.  This will cause
        // attempts to lock an object to throw DestructionUnderwayException, which
        // can help troubleshoot race conditions in shutdown sequences.
        volatile bool   m_destruction_underway;
    };

    // This macro should appear at the very beginning of every destructor for Lockable-derived classes.
    #define BEGIN_LOCKABLE_DESTRUCTOR {m_destruction_underway=true;}

    // This macro should appear the very end of every destructor for Lockable-derived classes.
    // It is currently reserved for future use, but should be present.
    #define END_LOCKABLE_DESTRUCTOR {;}
}

#endif
