/* include/USAFW/util/Logger.hpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#ifndef __Logger_hpp__
#define __Logger_hpp__

#include <cstdint>
#include <iostream>
#include <string>

namespace USAFW {

    class Logger {
    public:
        // Constructor
        Logger(void);

        // Destructor
        virtual ~Logger();

        enum logging_level {
            min_logging_level,
            log_level_debug,
            log_level_info,
            log_level_warning,
            log_level_error,
            log_level_fatal,
            max_logging_level
        };


        // Sets the minimum level to be reported to stdout.  Messages below this
        // level will not be reported to the console as standard output.
        inline void set_min_stdout_level(logging_level min_levelP) { m_min_stdout_level = min_levelP; }

        // Sets the maximum level to be reported to stdout.  Messages above this
        // level will not be reported to the console as standard output.  It is
        // important that the minimum level threshhold configured for stderr output
        // is the next level above the value used here, or important messages may be
        // dropped.
        inline void set_max_stdout_level(logging_level max_levelP) { m_max_stdout_level = max_levelP; }

        // Sets the minimum level to be reported to stderr.  Messages below this
        // level will not be reported to standard error.  It is important that the
        // this value is no more than one level higher than the maximum level
        // threshhold configured for stdout (console) output, or important messages
        // may be dropped.
        //
        // NOTE: There is no set_max_stderr_level.  That value is always
        // "max_logging_level".
        inline void set_min_stderr_level(logging_level min_levelP) { m_min_stderr_level = min_levelP; }
		static std::string NowAsYyyyMnDdHhMmSs(void);
        void log(logging_level levelP, const std::string &messageP) const;

        void debug(const std::string &messageP) const;
        void info(const std::string &messageP) const;
        void warning(const std::string &messageP) const;
        void error(const std::string &messageP) const;
        void fatal(const std::string &messageP) const;
    
    protected:
        int         m_min_stdout_level;
        int         m_max_stdout_level;
        int         m_min_stderr_level;
        int         m_max_stderr_level;
        uint64_t    m_zerotime;
    };
};

#endif
