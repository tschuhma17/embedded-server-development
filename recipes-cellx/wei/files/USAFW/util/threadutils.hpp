/* include/USAFW/util/threadutils.h */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#ifndef __USAFW_threadutils_h__
#define __USAFW_threadutils_h__

#include <string>

#include <pthread.h>

#include "include/USAFW/details.h"
#include "include/USAFW/sizes.h"

namespace USAFW {

    // Returns the name of the current thread.
    std::string getCurrentThreadName(void);

    // Returns the name of the current thread into the C string
    // buffer provided, up to the provided length maximum.
    void getCurrentThreadName(char *nameR, int strMaxP);

    // IMPORTANT
    // POSIX does not define a sentinel value for an invalid thread-- even zero.
    // Zero can actually come up, when the kernel thread ID counter wraps (which can happen early in
    // some implementations).
    // Generally speaking, there are two strategies:
    // 1. Set any member thread values to YOUR OWN thread ID, which you can easily check for.
    // 2. Start a thread that waits on a semaphore you will never set until shutdown.
    // 3. Use that value as your "invalid thread ID".
    // 
    // You MUST call USAFW_SLED_init() before calling this function.
    // Prior to calling that, the value of this thread object will be set to the thread the
    // compiler's static initialization preamble ran in.
    // You MUST call USAFW_SLED_shutdown() before your main() exits.
    pthread_t getInvalidPthread(void);

    // USAFW SLED INTERNAL USE ONLY
    void __startInvalidPthread(void);

    // USAFW SLED INTERNAL USE ONLY
    void __shutdownInvalidPthread(void);
}

#endif
