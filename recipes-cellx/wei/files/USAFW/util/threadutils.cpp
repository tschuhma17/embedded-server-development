/* src/threadutil.cpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#include "USAFW/details.h"

#include <string>

#ifdef USAFW_THREAD_TYPE_POSIX
#include <pthread.h>
#include <semaphore.h>
#endif

#include "USAFW/util/threadutils.hpp"

namespace USAFW {

    #ifdef USAFW_MUTEX_TYPE_POSIX

        std::string getCurrentThreadName(void) {
            char tmpstr[USAFW_SHORT_STRMAX];
            // FIXME: Error-check
            pthread_getname_np(pthread_self(), tmpstr, USAFW_SHORT_STRMAX-1);
            return std::string(tmpstr);
        }



        void getCurrentThreadName(char *nameR, int strMaxP) {
            pthread_getname_np(pthread_self(), nameR, strMaxP-1);
        }

        // Temporarily set this to preamble thread.
        // You MUST call USAFW_SLED_init(); before you can use
        // getInvalidPthread() !
        static pthread_t __s_invalid_pthread(pthread_self());

        // FIXME - Find a safe initialization sentinel for this.
        static sem_t __s_invalid_pthread_shutdown_semaphore;

        pthread_t getInvalidPthread(void) {
            return __s_invalid_pthread;
        }

        // USAFW SLED INTERNAL USE ONLY
        void* __invalidPthreadCore(void* pDataP) {
            // Wait, basically forever, until the SLED is shut down.
            sem_wait(&__s_invalid_pthread_shutdown_semaphore);

            return NULL;
        }

        // USAFW SLED INTERNAL USE ONLY
        void __startInvalidPthread(void) {
            // FIXME - check return values and throw exceptions
            sem_init(&__s_invalid_pthread_shutdown_semaphore, 0, 0);

            // FIXME - check return values and throw exceptions            
            pthread_create(&__s_invalid_pthread, 0, __invalidPthreadCore, NULL);
        }

        // USAFW SLED INTERNAL USE ONLY
        void __shutdownInvalidPthread(void) {
            // Signal the MacGuffin to shut down.
            sem_post(&__s_invalid_pthread_shutdown_semaphore);

            // Join the thread.
            void *retval;
            pthread_join(__s_invalid_pthread, &retval);

            // Next-safest value.
            __s_invalid_pthread = pthread_self();
        }

    #endif
}
