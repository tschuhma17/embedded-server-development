/* src/USAFW/util/Logger.cpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#include <chrono>
#include <sstream>

#include "USAFW/util/Logger.hpp"

using namespace std::chrono;

namespace USAFW {

    // Constructor
    Logger::Logger(void) :
        m_min_stdout_level(min_logging_level),
        m_max_stdout_level(log_level_warning),
        m_min_stderr_level(log_level_error),
        m_max_stderr_level(max_logging_level),
        m_zerotime(duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count())
    {
        info("Logger initialized at UNIX time T="+std::to_string(duration_cast<seconds>(system_clock::now().time_since_epoch()).count()));
    }

    // Destructor
    Logger::~Logger() {
        ;
    }

	std::string Logger::NowAsYyyyMnDdHhMmSs(void)
	{
		system_clock::time_point now = system_clock::now();
		timespec alsoNow;
		int rc = clock_gettime( CLOCK_REALTIME, &alsoNow );
		long mils = alsoNow.tv_nsec / 1000000L;
		time_t nowAsTime_t = system_clock::to_time_t( now );
		std::tm* t = std::gmtime( &nowAsTime_t );
		std::stringstream nowAsStrStream;
		nowAsStrStream << (1900 + t->tm_year) << "-";
		if (t->tm_mon < 9) nowAsStrStream << "0";
		nowAsStrStream << (1 + t->tm_mon) << "-";
		if (t->tm_mday < 10) nowAsStrStream << "0";
		nowAsStrStream << t->tm_mday << " ";
		if (t->tm_hour < 10) nowAsStrStream << "0";
		nowAsStrStream << t->tm_hour << ":";
		if (t->tm_min < 10) nowAsStrStream << "0";
		nowAsStrStream << t->tm_min << ":";
		if (t->tm_sec < 10) nowAsStrStream << "0";
		nowAsStrStream << t->tm_sec << ".";
		if (mils < 10) nowAsStrStream << "0";
		if (mils < 100) nowAsStrStream << "0";
		nowAsStrStream << mils;
		return nowAsStrStream.str();
	}

    void Logger::log(logging_level levelP, const std::string &messageP) const {
        if ((levelP<m_min_stdout_level)&&(levelP<m_min_stderr_level)) {
            return;
        }

        // ELAPSED time logging.
        //uint64_t elapsed_ms = (duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count()) - m_zerotime;
        //std::string full_message="T+" + std::to_string(elapsed_ms/1000.0) + ",";

        // ABSOLUTE (epoch) time logging.
        uint64_t now_ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
//        std::string full_message="T=" + std::to_string(now_ms/1000.0) + ",";
        std::string full_message="T=" + Logger::NowAsYyyyMnDdHhMmSs() + ",";

        switch(levelP) {
            case log_level_debug:
                full_message+="DEBUG";
                break;

            case log_level_info:
                full_message+="INFO";
                break;

            case log_level_warning:
                full_message+="WARNING";
                break;

            case log_level_error:
                full_message+="ERROR";
                break;

            case log_level_fatal:
                full_message+="FATAL";
                break;

            default:
                full_message+="UNKNOWN_LOG_LEVEL";
        }

        full_message+=","+messageP;

        if ((levelP>=m_min_stdout_level) && (levelP<=m_max_stdout_level)) {
            std::cout << full_message << std::endl;
        }
        if ((levelP>=m_min_stderr_level) && (levelP<=m_max_stderr_level)) {
            std::cerr << full_message << std::endl;
        }
    }

    void Logger::debug(const std::string &messageP) const {
        log(Logger::logging_level::log_level_debug, messageP);
    }

    void Logger::info(const std::string &messageP) const {
        log(Logger::logging_level::log_level_info, messageP);
    }

    void Logger::warning(const std::string &messageP) const {
        log(Logger::logging_level::log_level_warning, messageP);
    }

    void Logger::error(const std::string &messageP) const {
        log(Logger::logging_level::log_level_error, messageP);
    }

    void Logger::fatal(const std::string &messageP) const {
        log(Logger::logging_level::log_level_fatal, messageP);
    }

}
