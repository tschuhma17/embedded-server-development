/* MeasurementUnit.hpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#ifndef __MeasurementUnit_hpp__
#define __MeasurementUnit_hpp__

#include <string>

namespace USAFW {

    struct BaseUnit {
        std::string name;
        std::string abbreviation;
    };

    struct MetricPrefix {
        // Prefix name, e.g., "kilo".  Note that this is "" for (none).
        std::string prefix;

        // Prefix symbol, e.g., "k".  Note that all symbols are their closest low-
        // ASCII "basic typewriter" equivalents, e.g., "u" for "micro".
        // NOTE: This is "string" not "char" so that "" works for (none).
        std::string symbol;

        // Exponent value (10^n), e.g. 3 for "kilo".  May be positive or negative.
        int exponential;
    };

    extern MetricPrefix metric_prefix_none;
    extern MetricPrefix metric_prefix_centi;
    extern MetricPrefix metric_prefix_milli;
    extern MetricPrefix metric_prefix_micro;
    extern MetricPrefix metric_prefix_nano;
    extern MetricPrefix metric_prefix_pico;

    struct MeasurementUnit {
        const BaseUnit      &m_base_unit;
        const MetricPrefix  &m_metric_prefix;
    };

    extern MeasurementUnit meters;
    extern MeasurementUnit nanometers;

}

#endif
