/* src/USAFW/util/Lockable.cpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#include <pthread.h>

#include "USAFW/exceptions/LogicExceptions.hpp"
#include "USAFW/util/Lockable.hpp"

namespace USAFW {

    Lockable::Lockable() :
        m_last_lock_attempt(NOWHERE()),
        m_last_locked_at(NOWHERE()),
        m_last_unlock_attempt(NOWHERE()),
        m_last_unlocked_at(NOWHERE()),
        m_lock_recursion_depth(0),
        m_total_cycle_count(0),
        m_accounting_lock(PTHREAD_MUTEX_INITIALIZER),
        m_lock(PTHREAD_MUTEX_INITIALIZER),
        m_destruction_underway(false)
    {
        pthread_mutexattr_t attr;
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
        pthread_mutex_init(&m_accounting_lock, &attr);
        
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
        pthread_mutex_init(&m_lock, &attr);
    }

    
    Lockable::~Lockable() {
        pthread_mutex_destroy(&m_lock);
        pthread_mutex_destroy(&m_accounting_lock);
    }


#ifdef USAFW_DISABLE_LOCK_DEBUGGING
    Lockable::_lock(void) {
        pthread_mutex_trylock();
    }
#else
    void Lockable::_lock(const CodePosition &&oWhereP) {

#if 0 // THIS FUNCTIONALITY REQUIRES A DESIGN REVIEW
        // If the object is being destroyed, throw an exception.
        if(m_destruction_underway) {
            THROW(DestructionUnderway);
        }
#endif

        // Lock the accounting lock to set tracking data.
        pthread_mutex_lock(&m_accounting_lock);
        m_last_lock_attempt=oWhereP;
        pthread_mutex_unlock(&m_accounting_lock);

        pthread_mutex_lock(&m_lock);

        pthread_mutex_lock(&m_accounting_lock);
        if (m_last_locked_at==oWhereP) {
            m_lock_recursion_depth++;
        } else {
            m_last_locked_at=oWhereP;
            m_lock_recursion_depth=1;
        }
        pthread_mutex_unlock(&m_accounting_lock);
    }


    void Lockable::_unlock(const CodePosition &&oWhereP) {
        // Lock the accounting lock to set tracking data.
        pthread_mutex_lock(&m_accounting_lock);
        m_last_unlock_attempt=oWhereP;
        pthread_mutex_unlock(&m_accounting_lock);

        pthread_mutex_unlock(&m_lock);

        pthread_mutex_lock(&m_accounting_lock);
        m_last_unlocked_at=oWhereP;
        pthread_mutex_unlock(&m_accounting_lock);
    }
#endif


    Lockable::Autolock::Autolock(Lockable &whatP, CodePosition &&whereP):
        m_what(whatP)
    {
        m_what._lock(CodePosition(whereP));
    }

    Lockable::Autolock::~Autolock() {
        m_what._unlock(HERE());
    }

}
