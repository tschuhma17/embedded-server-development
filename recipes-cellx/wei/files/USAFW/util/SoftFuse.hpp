/* include/SoftFuse.hpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#ifndef __SoftFuse_hpp__
#define __SoftFuse_hpp__

#include <string>

namespace USAFW {

    /** This class implements a non-resettable fuse.  The fuse is good when created, and may be inspected with either the
     *  "test" function, which returns true if and only if the fuse has not been blown, or the "assert" function, which
     *  will throw a SoftFuseBlown exception if the fuse has been blown.  To blow the fuse, use the "blow" function.
     * 
     *  For troubleshooting purposes, fuses must be given names.
     */
    class SoftFuse {
    public:
        // Constructor
        SoftFuse(const std::string &nameP);

        // Destructor
        ~SoftFuse();

        /** Returns true if and only if the fuse has not been blown, else false.
         * 
         *  THROWS:
         *  (nothing)
         */
        bool check(void) const;

        /** Throws a SoftFuseBlownException if and only if the fuse has been blown.  The function has no other effect.
         *
         *  THROWS:
         *  - SoftFuseBlownException if and only if the fuse has been blown.
         */
         void assert(void) const;

        /** Explicitly delete assignment and copy constructor operators. */
        SoftFuse(const SoftFuse&) = delete;
        SoftFuse& operator= (const SoftFuse&) = delete;

    protected:
        // Fuse name, useful for troubleshooting.
        const std::string m_name;

        // Actual fuse state.
        // TODO: Consider std::call_once
        volatile bool m_blown;
    };

}

#endif
