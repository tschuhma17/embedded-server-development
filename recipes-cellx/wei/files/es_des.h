///----------------------------------------------------------------------
/// ES-DES (Embedded Server Data Exchange Specification)
/// This file is meant to be a place to define all of the strings that
/// can appear in data exchange messages. This is not done for language
/// translations. It is to prevent strings from being hardcoded
/// everywhere. Just doing our bit to prevent Ragnarok.
///----------------------------------------------------------------------
#ifndef _ES_DES_H
#define _ES_DES_H

#define ENUM_EDITABLE_FIXED						"enumEditableFixed"
#define ENUM_EDITABLE_CPD_ONLY					"enumEditableCPDOnly"
#define ENUM_EDITABLE_APP_ONLY					"enumEditableApplicationOnly"
#define ENUM_EDITABLE_CPD_APP					"enumEditableCPD_Application"
//#define ENUM_TYPE_UNKNOWN						"enumTypeUnknown"
#define ENUM_NOT_PERSIST						"enumNotPersist"
#define ENUM_PERSIST							"enumPersist"
#define ENUM_HARD_PERSIST						"enumHardPersist"
#define ENUM_REMOVE								"enumRemove"
#define ENUM_CIRCULAR							"enumCircular"
#define ENUM_RECTANGULAR						"enumRectangular"

#define _1MEG_L						(1000000L)
#define _1MEG_F						(1000000.0)
#define DEFAULT_CLEAR_KILLS_TIMEOUT	(5)
#define DEFAULT_JOG_HLDEC			(0)
#define DEFAULT_SAMPLE_PERIOD		(150)
#define DEFAULT_CAPTURE_DATA		(0)
#define DEFAULT_CAPTURE_TIME		(150)
//template <typename E> constexpr typename std::underlying_type<E>::type to_underlying(E e);

/// Thread names
#define MQTT_RECEIVER				"receivesFromMqtt"
#define COMMAND_INTERPRETER			"interpretsCommand"
#define DOLER_FOR_OPTICS			"dolerForOptics"
#define DOLER_FOR_INFO				"dolerForInfo"
#define DOLER_FOR_CXD				"dolerForCxD"
#define DOLER_FOR_CXPM				"dolerForCxPM"
#define DOLER_FOR_PUMP				"dolerForPump"
#define DOLER_FOR_RING_HOLDER		"dolerForRingHolder"
#define MQTT_SENDER					"sendsToMqtt"


/// Paths for data files
#define CONFIG_DIR					"/usr/share/cfgs"
#define LOG_DIR						"/usr/share/logs"
#define CPD_DIR						"/usr/share/cpds"
#define HW_EVENT_LOG				"hw_event.log"
#define	ALARMS_LOG					"alarms.log"
#define PLATES_DIR					"/usr/share/plates"
#define TIPS_DIR					"/usr/share/tips"
#define SERIAL_NUMBER_FILE			".sn"
#define ES_PARTS_JSON				"es_parts.json"
#define ES_HARDWARE_JSON			"es_hardware.json"
#define MOTION_SERVICES_CONFIG_JSON	"MotionServices-config.json"

/// Header strings for data files
#define HARDWARE_EVENT_LOG		 	"hardware_event_log"
#define ALARMS_LOG_JSON				"alarms_log_json"

/// Devices (The first parts of topics)
#define TOPIC_ES					"tbd/machine"
// TODO: uScope; Delete TOPIC_MICROSCOPE at a later time, when a new 
// type of microscope has been integrated.
#define TOPIC_MICROSCOPE			"tbd/microscope"
#define TRANSPORT_VERSION_VALUE		"1.0"

/// Channels (The second (last) parts of topics)
#define CHANNEL_COMMAND				"/command"
#define CHANNEL_ACK					"/ack"
#define CHANNEL_ALERT				"/alert"
#define CHANNEL_STATUS				"/status"
#define TEXT_COMMAND				"command"
#define TEXT_ACK					"ack"
#define TEXT_ALERT					"alert"
#define TEXT_STATUS					"status"
#define TEXT_PHOTO					"photo"

/// Call fields
#define MESSAGE_TYPE				"message_type"
#define TRANSPORT_VERSION			"transport_version"
#define COMMAND_GROUP_ID			"command_group_id"
#define COMMAND_ID					"command_id"
#define SESSION_ID					"session_id"
#define MACHINE_ID					"machine_id"
#define PAYLOAD						"payload"
#define STATUS						"status"
#define STATUS_CODE					"status_code"
#define TIMESTAMP					"timestamp"
#define FAILED_TO_PARSE				"failed_to_parse"
#define DETAILS						"details"
#define DEVICE_ENUM					"device_enum"
#define IS_CAUSED_HOLD_FLAG			"is_caused_hold_flag"

/// Status fields
// TODO: Flesh out the remaining values from the Data Exchange Spec.
#define STATUS_OK					200 /// indicates success
#define STATUS_STR_OK				"ok"
#define STATUS_SCHEDULED			202 /// A status update: the command has been scheduled
#define STATUS_IN_PROGRESS			2020 /// A status update: the command is being executed
#define STATUS_CMD_MSG_ERROR		300 /// Some (input) data of a command payload was not readable/usable, so command was aborted.
#define STATUS_CMD_MSG_DATA_MISSING	301 /// Some (input) data of a command payload was missing, so command was aborted.
#define STATUS_JSON_COMP_FAILED		302 /// Failed to compose a JSON message.
#define STATUS_REJECTED				400 /// Bad request a generic indicator that the request was malformed
#define STATUS_STR_REJECTED			"rejected"
#define STATUS_INCONSISTENCY		402	/// There is an inconsistency in the information provided	
#define STATUS_ID_MISMATCH			4021 /// There is an inconsistency between two referenced items that are assumed to be the same iten.	
#define STATUS_FORBIDDEN			403	/// The request was well formed, but the sender is not authorized to execute this command	
#define STATUS_MOTION_FORBIDDEN		4030 /// The parameters of a motion command were understood, and are outside the range of acceptable values for the device
#define STATUS_MOTION_OUT_OF_RANGE  4031 /// A motion parameter is outside hardware range.
#define STATUS_NOT_FOUND			404 /// The resource the operation is attempting to alter or retrieve is not found	
#define STATUS_TIP_NOT_FOUND		4040 /// Reading the skew of the tip failed because the tip was not present	
#define STATUS_CPD_NOT_FOUND		4041 /// An operation was attempted on a component property definition but now CPD with the provided CPD id was found	
#define STATUS_COMPONENT_NOT_FOUND	4042 /// An operation was attempted on a component but no component with the provided id was found	
#define STATUS_BARCODE_NOT_FOUND	4043 /// A plate move requested the plate’s barcode to be read but the barcode could not be read	
#define STATUS_INCORRECT_BARCODE	4044 /// detected	The barcode on the plate being moved did not match the expected barcode	
#define STATUS_BARCODE_ALREADY_EXISTS_IN_INVENTORY	4045
#define STATUS_NOT_ALLOWED			405 /// The command is not allowed against this target
#define STATUS_EJECT_FLUID_NOT_ALLOWED	4051 /// Ejecting ADP tips with fluid in them is not allowed
#define STATUS_OC_FL_LIGHT_FAILED	4061 /// The fluorescence light filter failed to change on/off state
#define STATUS_OC_SHUTTER_FAILED	4062 /// The shutter failed to change to the new state.
#define STATUS_OC_FL_SHUTTER_FAILED	4063 /// Both the fluorescence light filter and shutter failed to change to the requested states.
#define STATUS_OC_MAX_FAILED		4064 /// The max command failed.
#define STATUS_OC_FL_MAX_FAILED		4065 /// Fluorescence light failed failed to change on/off state *and* the max command failed.
#define STATUS_OC_FL_SHUTTER_MAX_FAILED	4066 /// The shutter failed to change to the new state *and* the max command failed.
#define STATUS_OC_FL_SHUTTER_LIGHT_MAX_FAILED	4067 /// The fluorescence light filter and shutter failed to change to the requested *and* the max command failed.
#define STATUS_INIT_FAILED			4071 /// Main entered time-out due to failure to progress through hardware init sequence.
#define STATUS_MOVEMENT_FAILED		4072 /// A move along an axis either timed out or stopped short of its destination.
#define STATUS_QUERY_FAILED         4073 /// A fetch of data from the ADP (or other) failed.
#define STATUS_AXIS_Y_FAULTED		4081
#define STATUS_AXIS_X_FAULTED		4082
#define STATUS_AXIS_XX_FAULTED		4083
#define STATUS_AXIS_YY_FAULTED		4084
#define STATUS_AXIS_Z_FAULTED		4085
#define STATUS_AXIS_ZZ_FAULTED		4086
#define STATUS_AXIS_ZZZ_FAULTED		4087
#define STATUS_AXIS_AR_FAULTED		4088
#define STATUS_PUMP_FAULTED			4089
#define STATUS_CONFLICT				4090 /// The requested operation cannot be completed because the specified resource is already in use
#define STATUS_CONTROLLED_SHUTDOWN	4091 /// Wes has been instructed to shut down.
#define STATUS_TIMEOUT_ERROR		4092 /// A motor action did not finish in the expected time.
#define STATUS_KILL_MOTION_Y		4101
#define STATUS_KILL_MOTION_X		4102
#define STATUS_KILL_MOTION_XX		4103
#define STATUS_KILL_MOTION_YY		4104
#define STATUS_KILL_MOTION_SH		4105
#define STATUS_KILL_MOTION_Z		4106
#define STATUS_KILL_MOTION_ZZ		4107
#define STATUS_KILL_MOTION_ZZZ		4108
#define STATUS_KILL_MOTION_ASP1		4109
#define STATUS_KILL_MOTION_DISP1	4110
#define STATUS_KILL_MOTION_DISP2	4111
#define STATUS_KILL_MOTION_DISP3	4112
#define STATUS_HARDWARE_CFG_ERROR	4113
#define STATUS_CAMERA_FAILED_GET_FRAME	4201
#define STATUS_DEVICE_ERROR			500  /// A generic alert code indicating that a failure occurred at the device
#define STATUS_SERIAL_PORT_ERROR	5011 /// A serial port operation failed.
#define STATUS_MATH_ERROR			501  /// A divide-by-zero or similar nono happened.
#define STATUS_PROG_0_ERROR			502  /// A prog0 command failed to find a force or erred in some other way.
#define STATUS_MEMORY_ERROR			503  /// Running out of memory.

#define ERROR						"error"
#define SUB_SUB_NULL				"ssnull"

/// Payload fields
#define Z_AXIS_SPEED				"z_axis_speed"
#define ZZ_AXIS_SPEED				"zz_axis_speed"
#define SLOW_DOWN_OFFSET_NM			"slow_down_offset_nm"
#define Z_NM						"z_nm"
#define ZZ_NM						"zz_nm"
#define ZZZ_NM						"zzz_nm"
//#define Z_STEP					"z_step"
#define Z_OFFSET_NM					"z_offset_nm"
//#define Z_OFFSET_STEP				"z_offset_step"
#define ZZ_OFFSET_NM				"zz_offset_nm"
#define ZZZ_OFFSET_NM				"zzz_offset_nm"
#define X_STEP_COUNT				"x_step_count"
#define X_STEP_INCREMENT_NM			"x_step_increment_nm"
#define Y_STEP_COUNT				"y_step_count"
#define Y_STEP_INCREMENT_NM			"y_step_increment_nm"
#define SCAN_MOVEMENT_DELAY_MS		"scan_movement_delay_ms"
#define CPD_ID						"cpd_id"
#define CPD_BODY					"cpd_body"
#define XY_SPEEDS					"xy_speeds"
#define MONTAGE_SPEEDS				"montage_speeds"

///----------------------------------------------------------------------
/// CPD fields
///----------------------------------------------------------------------
#define HEADER									"header"
#define HEADER_COMPONENT_TYPE					"component_type"
#define ENUM_COMPONENT_UNKNOWN					"enumComponentUnknown"
#define ENUM_COMPONENT_NONE						"enumComponentNone"
//#define ENUM_PLATE							"enumPlateType"
//#define ENUM_TIP								"enumTipType"
#define ENUM_CXD								"enumCxD"
#define ENUM_XYAXES								"enumXYAxes"
#define ENUM_XXAXIS								"enumXXAxis"
#define ENUM_YYAXIS								"enumYYAxis"
#define ENUM_ZAXIS								"enumZAxis"
#define ENUM_ZZAXIS								"enumZZAxis"
#define ENUM_ZZZAXIS							"enumZZZAxis"
#define ENUM_ASPIRATION_PUMP_1					"enumAspirationPump1"
#define ENUM_DISPENSE_PUMP_1					"enumDispensePump1"
#define ENUM_DISPENSE_PUMP_2					"enumDispensePump2"
#define ENUM_DISPENSE_PUMP_3					"enumDispensePump3"
#define ENUM_PICKING_PUMP_1						"enumPickingPump1"
#define ENUM_WHITE_LIGHT_SOURCE					"enumWhiteLightSource"
#define ENUM_FLUORESCENCE_LIGHT_FILTER			"enumFluorescenceLightFilter"
#define ENUM_ANNULAR_RING_HOLDER				"enumAnnularRingHolder"
#define ENUM_CAMERA								"enumCamera"
#define ENUM_OPTICAL_COLUMN						"enumOpticalColumn"
#define ENUM_INCUBATOR							"enumIncubator"
#define ENUM_CXPM								"enumCxPM"
#define ENUM_BARCODE_READER						"enumBarcodeReader"

#define FIELD_EDITABILITY_TYPE					"editability_type"
#define FIELD_PERSIST_TYPE						"persist_type"
#define FIELD_SETTING							"setting"
#define FIELD_DATA_TYPE							"data_type"
#define FIELD_SETTING							"setting"
#define FIELD_ES_MIN							"es_min_value"
#define FIELD_ES_MAX							"es_max_value"
#define FIELD_BASE_UNIT								"base_unit"
#define FIELD_SCALE_FACTOR							"scale_factor"
#define FIELD_POINTS_PAST_DECIMAL					"points_past_decimal"
#define FIELD_DISPLAY_DIRECTIVE						"display_directive"
#define FIELD_DISPLAY_MIN_VALUE						"display_min_value"
#define FIELD_DISPLAY_MAX_VALUE						"display_max_value"
#define ENUM_CALIBRATION_TYPE					"enumCalibrationType"
#define ENUM_WHITE_LIGHT_TYPE					"enumWhiteLightType"
#define ENUM_UNKNOWN							"enumUnknown"
#define ENUM_NONE								"enumNone"
#define ENUM_ZERO_ORDER_POLY					"enumZeroOrderPolynomial"
#define ENUM_FIRST_ORDER_POLY_FORCE_0_INTERCEPT	"enumFirstOrderPolynomialForceZeroIntercept"
#define ENUM_FIRST_ORDER_POLY					"enumFirstOrderPolynomial"
#define ENUM_SECOND_ORDER_POLY					"enumSecondOrderPolynomial"
#define ENUM_PUMP_TYPE							"enumPumpType"
#define ENUM_PERISTALTIC						"enumPeristaltic"
#define ENUM_SYRINGE							"enumSyringe"
#define ENUM_INCANDESCENT						"enumIncandescent"
#define ENUM_LED								"enumLED"
#define STRING									"string"
#define FLOAT									"float"
#define UINT32									"uint32_t"
#define INT32									"int32_t"
#define UINT16									"uint16_t"
#define INT16									"int16_t"
#define BOOLEAN									"boolean"

#define PROPERTIES_JSON				"properties_json"
#define COMPONENT					"component"
#define SETTING						"setting"
#define EDITABILITY_TYPE			"editability_type"
#define PERSIST_TYPE				"persist_type"
#define DATA_TYPE					"data_type"
#define ES_MIN_VALUE				"es_min_value"
#define ES_MAX_VALUE				"es_max_value"
#define BASE_UNIT					"base_unit"
#define SCALE_FACTOR				"scale_factor"
#define POINTS_PAST_DECIMAL			"points_past_decimal"
#define DISPLAY_DIRECTIVE			"display_directive"
#define DISPLAY_MIN_VALUE			"display_min_value"
#define DISPLAY_MAX_VALUE			"display_max_value"
#define COMPONENT_TYPE				"component_type"
#define CONTROLLER_VENDOR			"controller_vendor"
#define CONTROLLER_MODEL			"controller_model"
#define CONTROLLER_VERSION			"controller_version"
#define CONTROLLER_SERIAL_NUMBER	"controller_serial_number"
#define PROPERTY_NAME				"property_name"
#define PROPERTY_VALUE				"property_value"
#define DRIVER_VENDOR				"driver_vendor"
#define X_DRIVER_VENDOR				"x_driver_vendor"
#define Y_DRIVER_VENDOR				"y_driver_vendor"
#define DRIVER_MODEL				"driver_model"
#define X_DRIVER_MODEL				"x_driver_model"
#define Y_DRIVER_MODEL				"y_driver_model"
#define DRIVER_VERSION				"driver_version"
#define X_DRIVER_VERSION			"x_driver_version"
#define Y_DRIVER_VERSION			"y_driver_version"
#define DRIVER_SERIAL_NUMBER		"driver_serial_number"
#define X_DRIVER_SERIAL_NUMBER		"x_driver_serial_number"
#define Y_DRIVER_SERIAL_NUMBER		"y_driver_serial_number"
#define MOTOR_VENDOR				"motor_vendor"
#define X_MOTOR_VENDOR				"x_motor_vendor"
#define Y_MOTOR_VENDOR				"y_motor_vendor"
#define MOTOR_MODEL					"motor_model"
#define X_MOTOR_MODEL				"x_motor_model"
#define Y_MOTOR_MODEL				"y_motor_model"
#define MOTOR_VERSION				"motor_version"
#define X_MOTOR_VERSION				"x_motor_version"
#define Y_MOTOR_VERSION				"y_motor_version"
#define MOTOR_SERIAL_NUMBER			"motor_serial_number"
#define X_MOTOR_SERIAL_NUMBER		"x_motor_serial_number"
#define Y_MOTOR_SERIAL_NUMBER		"y_motor_serial_number"
#define MOTOR_TYPE					"motor_type"
#define X_MOTOR_TYPE				"x_motor_type"
#define Y_MOTOR_TYPE				"y_motor_type"
#define CPD_ID						"cpd_id"
#define CPD_FILE_NAME				"cpd_file_name"
#define VERSION						"version"
#define CPD_DESCRIPTION				"cpd_description"
#define SYNTAX_ERROR_STRING			"syntax_error_string"
#define XX_AXIS_RESOLUTION			"xx_travel/step"
#define XX_MAXIMUM_SPEED			"xx_maximum_speed"
#define XX_SET_SPEED				"xx_set_speed"
#define XX_ACCELERATION				"xx_acceleration"
#define XX_DECELERATION				"xx_deceleration"
#define XX_JOGGING_SPEED			"xx_jogging_speed"
#define XX_JOGGING_ACCELERATION		"xx_jogging_acceleration"
#define XX_JOGGING_DECELERATION		"xx_jogging_deceleration"
#define XX_DEFAULT_LOCATION_OFFSET			"xx_default_location_offset"
#define XX_Z_TIP_TO_TIP_WORKING_POSITION	"xx_z_tip_to_tip_working_position"
#define XX_ZZ_TIP_TO_TIP_WORKING_POSITION	"xx_zz_tip_to_tip_working_position"
#define XX_ZZZ_TIP_TO_TIP_WORKING_POSITION	"xx_zzz_tip_to_tip_working_position"
#define XX_Z_TIP_TO_TIP_SKEW_POSITION		"xx_z_tip_to_tip_skew_position"
#define XX_ZZ_TIP_TO_TIP_SKEW_POSITION		"xx_zz_tip_to_tip_skew_position"
#define XX_Z_TIP_TO_WASTE_1_POSITION		"xx_z_tip_to_waste_1_position"
#define XX_Z_TIP_TO_WASTE_2_POSITION		"xx_z_tip_to_waste_2_position"
#define XX_Z_TIP_TO_WASTE_3_POSITION		"xx_z_tip_to_waste_3_position"
#define XX_ZZ_TIP_TO_WASTE_1_POSITION		"xx_zz_tip_to_waste_1_position"
#define XX_ZZ_TIP_TO_WASTE_2_POSITION		"xx_zz_tip_to_waste_2_position"
#define XX_ZZ_TIP_TO_WASTE_3_POSITION		"xx_zz_tip_to_waste_3_position"
#define XX_ZZZ_TIP_TO_WASTE_1_POSITION		"xx_zzz_tip_to_waste_1_position"
#define XX_ZZZ_TIP_TO_WASTE_2_POSITION		"xx_zzz_tip_to_waste_2_position"
#define XX_ZZZ_TIP_TO_WASTE_3_POSITION		"xx_zzz_tip_to_waste_3_position"
#define XX_Z_TIP_TO_TIP_TRAY_RIGHT_EDGE		"xx_z_tip_to_tip_tray_right_edge"
#define XX_ZZ_TIP_TO_TIP_TRAY_RIGHT_EDGE	"xx_zz_tip_to_tip_tray_right_edge"
#define XX_Z_TIP_TO_KEY_ENTRY_POSITION		"xx_z_tip_to_key_entry_position"
#define XX_ZZ_TIP_TO_KEY_ENTRY_POSITION		"xx_zz_tip_to_key_entry_position"
#define XX_KEY_ENTRY_TO_KEY_WORK_POSITION	"xx_key_entry_to_key_work_position"
#define XX_Z_TIP_TO_KEY_WORK_POSITION		"xx_z_tip_to_key_work_position"
#define XX_ZZ_TIP_TO_KEY_WORK_POSITION		"xx_zz_tip_to_key_work_position"
#define YY_AXIS_RESOLUTION			"yy_axis_resolution"
#define YY_MAXIMUM_SPEED			"yy_maximum_speed"
#define YY_SET_SPEED				"yy_set_speed"
#define YY_ACCELERATION				"yy_acceleration"
#define YY_DECELERATION				"yy_deceleration"
#define YY_JOGGING_SPEED			"yy_jogging_speed"
#define YY_JOGGING_ACCELERATION		"yy_jogging_acceleration"
#define YY_JOGGING_DECELERATION		"yy_jogging_deceleration"
#define YY_DEFAULT_LOCATION_OFFSET	"yy_default_location_offset"
#define YY_KEY_TO_TIP_PATH			"yy_key_to_tip_path"
#define YY_TIP_TRAY_TOP_EDGE_TO_TIP_PATH	"yy_tip_tray_top_edge_to_tip_path"
#define YY_TIP_TRAY_CXPM_GRAB_POSITION		"yy_tip_tray_cxpm_grab_position"
#define Z_AXIS_RESOLUTION			"z_axis_resolution"
#define Z_MAXIMUM_SPEED				"z_maximum_speed"
#define Z_SET_SPEED					"z_set_speed"
#define Z_ACCELERATION				"z_acceleration"
#define Z_DECELERATION				"z_deceleration"
#define Z_JOGGING_SPEED				"z_jogging_speed"
#define Z_JOGGING_ACCELERATION		"z_jogging_acceleration"
#define Z_JOGGING_DECELERATION		"z_jogging_deceleration"
#define Z_DEFAULT_LOCATION_OFFSET	"z_default_location_offset"
#define Z_KEY_TO_TIP_PATH			"z_key_to_tip_path"
#define Z_TIP_TRAY_TOP_EDGE_TO_TIP_PATH		"z_tip_tray_top_edge_to_tip_path"
#define Z_TIP_TRAY_CXPM_GRAB_POSITION		"z_tip_tray_cxpm_grab_position"
#define ZZ_AXIS_RESOLUTION			"zz_axis_resolution"
#define ZZ_MAXIMUM_SPEED			"zz_maximum_speed"
#define ZZ_SET_SPEED				"zz_set_speed"
#define ZZ_ACCELERATION				"zz_acceleration"
#define ZZ_DECELERATION				"zz_deceleration"
#define ZZ_JOGGING_SPEED			"zz_jogging_speed"
#define ZZ_JOGGING_ACCELERATION		"zz_jogging_acceleration"
#define ZZ_JOGGING_DECELERATION		"zz_jogging_deceleration"
#define ZZ_DEFAULT_LOCATION_OFFSET	"zz_default_location_offset"
#define ZZ_KEY_TO_TIP_PATH			"zz_key_to_tip_path"
#define ZZ_TIP_TRAY_TOP_EDGE_TO_TIP_PATH	"zz_tip_tray_top_edge_to_tip_path"
#define ZZ_TIP_TRAY_CXPM_GRAB_POSITION		"zz_tip_tray_cxpm_grab_position"
#define ZZZ_AXIS_RESOLUTION			"zzz_axis_resolution"
#define ZZZ_MAXIMUM_SPEED			"zzz_maximum_speed"
#define ZZZ_SET_SPEED				"zzz_set_speed"
#define ZZZ_ACCELERATION			"zzz_acceleration"
#define ZZZ_DECELERATION			"zzz_deceleration"
#define ZZZ_JOGGING_SPEED			"zzz_jogging_speed"
#define ZZZ_JOGGING_ACCELERATION	"zzz_jogging_acceleration"
#define ZZZ_JOGGING_DECELERATION	"zzz_jogging_deceleration"
#define ZZZ_TOTAL_TRAVEL			"zzz_total_travel"
#define DISPENSE_TIP_1_VERSION_NUMBER			"dispense_tip_1_version_number"
#define DISPENSE_TIP_2_VERSION_NUMBER			"dispense_tip_2_version_number"
#define DISPENSE_TIP_3_VERSION_NUMBER			"dispense_tip_3_version_number"
#define ZZZ_TO_TOP_OF_XY_STAGE_HEIGHT			"zzz_to_top_of_xy_stage_height"
#define ZZZ_DISPENSE_HEIGHT_SAFETY_GAP_NM		"zzz_dispense_height_safety_gap_nm"
#define HOME_LOCATION				"home_location"
#define X_HOME_LOCATION				"x_home_location"
#define Y_HOME_LOCATION				"y_home_location"
#define HOMING_SPEED				"homing_speed"
#define X_HOMING_SPEED				"x_homing_speed"
#define Y_HOMING_SPEED				"y_homing_speed"
#define HOMING_ACCELERATION			"homing_acceleration"
#define X_HOMING_ACCELERATION		"x_homing_acceleration"
#define Y_HOMING_ACCELERATION		"y_homing_acceleration"
#define HOMING_DECELERATION			"homing_deceleration"
#define X_HOMING_DECELERATION		"x_homing_deceleration"
#define Y_HOMING_DECELERATION		"y_homing_deceleration"
#define AXIS_RESOLUTION							"axis_resolution"
#define MAX_ALLOWABLE_POSITION_ERROR			"max_allowable_position_error"
#define X_MAX_ALLOWABLE_POSITION_ERROR			"x_max_allowable_position_error"
#define Y_MAX_ALLOWABLE_POSITION_ERROR			"y_max_allowable_position_error"
#define END_OF_TRAVEL_LIMIT_POSITION_PLUS		"end_of_travel_limit_position_+"
#define X_END_OF_TRAVEL_LIMIT_POSITION_PLUS		"x_end_of_travel_limit_position_+"
#define Y_END_OF_TRAVEL_LIMIT_POSITION_PLUS		"y_end_of_travel_limit_position_+"
#define END_OF_TRAVEL_LIMIT_POSITION_MINUS		"end_of_travel_limit_position_-"
#define X_END_OF_TRAVEL_LIMIT_POSITION_MINUS	"x_end_of_travel_limit_position_-"
#define Y_END_OF_TRAVEL_LIMIT_POSITION_MINUS	"y_end_of_travel_limit_position_-"
#define SOFT_TRAVEL_LIMIT_POSITION_PLUS			"soft_travel_limit_position_+"
#define X_SOFT_TRAVEL_LIMIT_POSITION_PLUS		"x_soft_travel_limit_position_+"
#define Y_SOFT_TRAVEL_LIMIT_POSITION_PLUS		"y_soft_travel_limit_position_+"
#define SOFT_TRAVEL_LIMIT_POSITION_MINUS		"soft_travel_limit_position_-"
#define X_SOFT_TRAVEL_LIMIT_POSITION_MINUS		"x_soft_travel_limit_position_-"
#define Y_SOFT_TRAVEL_LIMIT_POSITION_MINUS		"y_soft_travel_limit_position_-"
#define X_AXIS_RESOLUTION						"x_axis_resolution"
#define Y_AXIS_RESOLUTION						"y_axis_resolution"
#define X_MAXIMUM_SPEED							"x_maximum_speed"
#define Y_MAXIMUM_SPEED							"y_maximum_speed"
#define X_SET_SPEED								"x_set_speed"
#define Y_SET_SPEED								"y_set_speed"
#define X_ACCELERATION							"x_acceleration"
#define Y_ACCELERATION							"y_acceleration"
#define X_DECELERATION							"x_deceleration"
#define Y_DECELERATION							"y_deceleration"
#define X_JOGGING_SPEED							"x_jogging_speed"
#define Y_JOGGING_SPEED							"y_jogging_speed"
#define Y_MONTAGE_SPEED							"y_montage_speed"
#define X_JOGGING_ACCELERATION					"x_jogging_acceleration"
#define Y_JOGGING_ACCELERATION					"y_jogging_acceleration"
#define Y_MONTAGE_ACCELERATION					"y_montage_acceleration"
#define X_JOGGING_DECELERATION					"x_jogging_deceleration"
#define Y_JOGGING_DECELERATION					"y_jogging_deceleration"
#define Y_MONTAGE_DECELERATION					"y_montage_deceleration"
#define X_DEFAULT_LOCATION_OFFSET				"x_default_location_offset"
#define Y_DEFAULT_LOCATION_OFFSET				"y_default_location_offset"
#define SCAN_MOVEMENT_DELAY_SLOW				"scan_movement_delay_slow"
#define SCAN_MOVEMENT_DELAY_MEDIUM				"scan_movement_delay_medium"
#define SCAN_MOVEMENT_DELAY_FAST				"scan_movement_delay_fast"
#define LAST_CHANGE_DATE			"last_change_date"
#define CALIBRATION_TYPE			"calibration_type"
#define X_CALIBRATION_TYPE			"x_calibration_type"
#define Y_CALIBRATION_TYPE			"y_calibration_type"
#define LAST_CALIBRATION_DATE		"last_calibration_date"
#define X_LAST_CALIBRATION_DATE		"x_last_calibration_date"
#define Y_LAST_CALIBRATION_DATE		"y_last_calibration_date"
#define CALIBRATION_FACTOR_1		"calibration_factor_1"
#define X_CALIBRATION_FACTOR_1		"x_calibration_factor_1"
#define Y_CALIBRATION_FACTOR_1		"y_calibration_factor_1"
#define CALIBRATION_FACTOR_2		"calibration_factor_2"
#define X_CALIBRATION_FACTOR_2		"x_calibration_factor_2"
#define Y_CALIBRATION_FACTOR_2		"y_calibration_factor_2"
#define CALIBRATION_FACTOR_3		"calibration_factor_3"
#define X_CALIBRATION_FACTOR_3		"x_calibration_factor_3"
#define Y_CALIBRATION_FACTOR_3		"y_calibration_factor_3"
#define NUMBER_OF_ROTATIONS			"number_of_rotations"
#define ROTATION_RADIUS				"rotation_radius"
#define ROTATION_PERIOD				"rotation_period"
#define CALIBRATION_COEFFICIENT_1	"calibration_coefficient_1"
#define CALIBRATION_COEFFICIENT_2	"calibration_coefficient_2"
#define CALIBRATION_COEFFICIENT_3	"calibration_coefficient_3"
#define FIRMWARE_VERSION_NUMBER		"firmware_version_number"
#define CONFIGURATION				"configuration"
#define SYSTEM_CONFIGURATION		"system_configuration"
#define CXR							"cxr"
#define XY_MOTION_AXES				"xy_motion_axes"
#define XX_MOTION_AXIS				"xx_motion_axis"
#define YY_MOTION_AXIS				"yy_motion_axis"
#define Z_MOTION_AXIS				"z_motion_axis"
#define ZZ_MOTION_AXIS				"zz_motion_axis"
#define ZZZ_MOTION_AXIS				"zzz_motion_axis"
#define ASPIRATION_PUMP				"aspiration_pump"
#define DISPENSE_PUMP_1				"dispense_pump_1"
#define DISPENSE_PUMP_2				"dispense_pump_2"
#define DISPENSE_PUMP_3				"dispense_pump_3"
#define PICKING_PUMP				"picking_pump"
#define PICKING_PUMP				"picking_pump"
#define PICKING_PUMP_CURRENT_VOLUME	"picking_pump_current_volume"
#define PICKING_PUMP_MAXIMUM_VOLUME "picking_pump_maximum_volume"
#define WHITE_LIGHT					"white_light"
#define FLUORESCENCE_LIGHT			"fluorescence_light"
#define ANNULAR_RING_HOLDER			"annular_ring_holder"
#define OPTICAL_COLUMN				"optical_column"
#define CAMERA						"camera"
#define INCUBATOR					"incubator"
#define CXPM						"cxpm"
#define BARCODE_READER				"barcode_reader"
#define TIP_TYPE					"tip_type"
#define TIP_AXIS					"tip_axis"
#define TIP_NUMBER					"tip_number"
#define X_NM						"x_nm"
#define XX_NM						"xx_nm"
#define Y_NM						"y_nm"
#define YY_NM						"yy_nm"
#define TIP_PICKUP_FORCE			"tip_pickup_force"
#define TIP_PICKUP_SPEED			"tip_pickup_speed"
#define TIP_SEATING_FORCE			"tip_seating_force"
#define TIP_SEATING_SPEED			"tip_seating_speed"
#define TIP_FIND_WELL_BOTTOM_FORCE	"tip_find_well_bottom_force"
#define TIP_FIND_WELL_BOTTOM_SPEED	"tip_find_well_bottom_speed"
#define TIP_INSERT_VERSION_NUMBER	"tip_insert_version_number"
#define Z_SAFE_HEIGHT				"z_safe_height"
#define ZZ_SAFE_HEIGHT				"zz_safe_height"
#define ZZZ_SAFE_HEIGHT				"zzz_safe_height"
#define RAISE_TIP_AT_END			"raise_tip_at_end"
#define Z_TO_TOP_OF_YY_STAGE_HEIGHT			"z_to_top_of_yy_stage_height"
#define ZZ_TO_TOP_OF_YY_STAGE_HEIGHT		"zz_to_top_of_yy_stage_height"
#define Z_TO_TOP_OF_SEATING_KEY_HEIGHT		"z_to_top_of_seating_key_height"
#define ZZ_TO_TOP_OF_SEATING_KEY_HEIGHT		"zz_to_top_of_seating_key_height"
#define Z_THROUGH_KEY_HEIGHT				"z_through_key_height"
#define ZZ_THROUGH_KEY_HEIGHT				"zz_through_key_height"
#define Z_TO_WASTE_1_HEIGHT			"z_to_waste_1_height"
#define ZZ_TO_WASTE_1_HEIGHT		"zz_to_waste_1_height"
#define ZZZ_TO_WASTE_1_HEIGHT		"zzz_to_waste_1_height"
#define Z_TO_WASTE_2_HEIGHT			"z_to_waste_2_height"
#define ZZ_TO_WASTE_2_HEIGHT		"zz_to_waste_2_height"
#define ZZZ_TO_WASTE_2_HEIGHT		"zzz_to_waste_2_height"
#define Z_TO_WASTE_3_HEIGHT			"z_to_waste_3_height"
#define ZZ_TO_WASTE_3_HEIGHT		"zz_to_waste_3_height"
#define ZZZ_TO_WASTE_3_HEIGHT		"zzz_to_waste_3_height"
#define Z_TO_TIP_SKEW_SENSOR_HEIGHT			"z_to_tip_skew_sensor_height"
#define ZZ_TO_TIP_SKEW_SENSOR_HEIGHT		"zz_to_tip_skew_sensor_height"
#define Z_TO_TOP_OF_XY_STAGE_HEIGHT			"z_to_top_of_xy_stage_height"
#define ZZ_TO_TOP_OF_XY_STAGE_HEIGHT		"zz_to_top_of_xy_stage_height"
#define Z_ASPIRATE_HEIGHT_FROM_WELL_BOTTOM	"z_aspirate_height_from_well_bottom"
#define ZZ_ASPIRATE_HEIGHT					"zz_aspirate_height"
#define Z_DISPENSE_HEIGHT_FROM_WELL_BOTTOM	"z_dispense_height_from_well_bottom"
#define Z_Y_OFFSET_FROM_XX_AXIS				"z_y_offset_from_xx_axis"
#define ZZ_Y_OFFSET_FROM_XX_AXIS			"zz_y_offset_from_xx_axis"
#define ZZZ_Y_OFFSET_FROM_XX_AXIS			"zzz_y_offset_from_xx_axis"
#define TIP_TOTAL_LENGTH					"tip_total_length"
#define TIP_WORKING_LENGTH					"tip_working_length"
#define TIP_SHOULDER_LENGTH					"tip_shoulder_length"
#define TIP_WORKING_LENGTH_NM				"tip_working_length_nm"
#define TIP_INNER_DIAMETER_AT_INSERTION_END	"tip_inner_diameter_at_insertion_end"
#define TIP_INNER_DIAMETER_AT_WORKING_END	"tip_inner_diameter_at_working_end"
#define TIP_MAX_MINIMAL_WORKING_VOLUME		"tip_max_minimal_working_volume"
#define CONTAINER_ROW_COUNT					"container_row_count"
#define CONTAINER_COLUMN_COUNT				"container_column_count"
#define CONTAINER_LENGTH					"container_length"
#define CONTAINER_WIDTH						"container_width"
#define CONTAINER_HEIGHT					"container_height"
#define CONTAINER_SEAT_TO_TIP_SEAT			"container_seat_to_tip_seat"
#define CONTAINER_SEAT_TO_TOP_OF_CONTAINER	"container_seat_to_top_of_container"
#define CONTAINER_SEAT_TO_TOP_OF_TIP		"container_seat_to_top_of_tip"
#define FIRST_TIP_X							"first_tip_x"
#define FIRST_TIP_Y							"first_tip_y"
#define INTER_TIP_X							"inter_tip_x"
#define INTER_TIP_Y							"inter_tip_y"
#define TIP_MAXIMUM_WORKING_VOLUME			"tip_maximum_working_volume"
#define TIP_RACK_ROW_COUNT					"tip_rack_row_count"
#define TIP_RACK_COLUMN_COUNT				"tip_rack_column_count"
#define TIP_RACK_LENGTH						"tip_rack_length"
#define TIP_RACK_WIDTH						"tip_rack_width"
#define TIP_RACK_HEIGHT						"tip_rack_height"
#define TIP_RACK_SEAT_TO_TIP_SEAT			"tip_rack_seat_to_tip_seat"
#define TIP_RACK_SEAT_TO_TOP_OF_TIP_RACK	"tip_rack_seat_to_top_of_tip_rack"
#define TIP_RACK_SEAT_TO_TOP_OF_TIP			"tip_rack_seat_to_top_of_tip"
#define USED_TIP_COUNT						"used_tip_count"
#define TIP_PICKUP_SLOWDOWN_OFFSET			"tip_pickup_slowdown_offset"
#define TIP_PICKUP_OVERSHOOT				"tip_pickup_overshoot"
#define TIP_SEATING_SLOWDOWN_OFFSET			"tip_seating_slowdown_offset"
#define TIP_SEATING_OVERSHOOT				"tip_seating_overshoot"
#define TIP_REMOVAL_SLOWDOWN_OFFSET			"tip_removal_slowdown_offset"
#define TIP_FIND_WELL_BOTTOM_OVERSHOOT		"tip_find_well_bottom_overshoot"
#define TIP_SKEW_SAFETY_GAP					"tip_skew_safety_gap"
#define TIP_WASTE_1_SAFETY_GAP				"tip_waste_1_safety_gap"
#define TIP_WASTE_2_SAFETY_GAP				"tip_waste_2_safety_gap"
#define TIP_WASTE_3_SAFETY_GAP				"tip_waste_3_safety_gap"
#define X_FIND_TIP_SPEED					"x_find_tip_speed"
#define Y_FIND_TIP_SPEED					"y_find_tip_speed"
#define X_FIND_TIP_ACCELERATION				"x_find_tip_acceleration"
#define Y_FIND_TIP_ACCELERATION				"y_find_tip_acceleration"
#define X_FIND_TIP_DECELERATION				"x_find_tip_deceleration"
#define Y_FIND_TIP_DECELERATION				"y_find_tip_deceleration"
#define X_FIND_TIP_SKEW_SPEED				"x_find_tip_skew_speed"
#define Y_FIND_TIP_SKEW_SPEED				"y_find_tip_skew_speed"
#define X_FIND_TIP_SKEW_ACCELERATION		"x_find_tip_skew_acceleration"
#define Y_FIND_TIP_SKEW_ACCELERATION		"y_find_tip_skew_acceleration"
#define X_FIND_TIP_SKEW_DECELERATION		"x_find_tip_skew_deceleration"
#define Y_FIND_TIP_SKEW_DECELERATION		"y_find_tip_skew_deceleration"
#define X_FIND_TIP_SKEW_TRAVEL_DISTANCE		"x_find_tip_skew_travel_distance"
#define Y_FIND_TIP_SKEW_TRAVEL_DISTANCE		"y_find_tip_skew_travel_distance"
#define X_SKEW_NM							"x_skew_nm"
#define Y_SKEW_NM							"y_skew_nm"
#define X_PLATE_1_RIGHT_EDGE_TO_OPTICAL_PATH				"x_plate_1_right_edge_to_optical_path"
#define Y_PLATE_1_TOP_EDGE_TO_OPTICAL_PATH					"y_plate_1_top_edge_to_optical_path"
#define X_PLATE_2_LEFT_EDGE_TO_OPTICAL_PATH					"x_plate_2_left_edge_to_optical_path"
#define Y_PLATE_2_TOP_EDGE_TO_OPTICAL_PATH					"y_plate_2_top_edge_to_optical_path"
#define X_PLATE_1_RIGHT_EDGE_TO_TIP_WORKING_POSITION		"x_plate_1_right_edge_to_tip_working_position"
#define Y_PLATE_1_TOP_EDGE_TO_TIP_WORKING_POSITION			"y_plate_1_top_edge_to_tip_working_position"
#define X_PLATE_2_LEFT_EDGE_TO_TIP_WORKING_POSITION			"x_plate_2_left_edge_to_tip_working_position"
#define Y_PLATE_2_TOP_EDGE_TO_TIP_WORKING_POSITION			"y_plate_2_top_edge_to_tip_working_position"
#define X_OPTICAL_SENSOR_TO_TIP_SKEW_POSITION					"x_optical_sensor_to_tip_skew_position"
#define Y_OPTICAL_SENSOR_TO_TIP_SKEW_POSITION					"y_optical_sensor_to_tip_skew_position"
#define MAX_COORDINATED_MOTION_SPEED							"max_coordinated_motion_speed"
#define MAX_COORDINATED_MOTION_ACCELERATION						"max_coordinated_motion_acceleration"
#define MAX_COORDINATED_MOTION_DECELERATION						"max_coordinated_motion_deceleration"
#define PUMP_VENDOR												"pump_vendor"
#define PUMP_MODEL												"pump_model"
#define PUMP_VERSION											"pump_version"
#define PUMP_SERIAL_NUMBER										"pump_serial_number"
#define PUMP_TYPE												"pump_type"
#define STANDARD_ASPIRATION_RATE								"standard_aspiration_rate"
#define STANDARD_DISPENSE_RATE									"standard_dispense_rate"
#define TUBING_TYPE												"tubing_type"
#define TUBING_INNER_DIAMETER									"tubing_inner_diameter"
#define TUBING_LAST_CHANGE_DATE									"tubing_last_change_date"
#define STEPS_PER_REVOLUTION									"steps_per_revolution"
#define VOLUME_PER_REVOLUTION									"volume_per_revolution"
#define MAXIMUM_REVOLUTIONS_PER_SECOND							"maximum_revolutions_per_second"
#define MAXIMUM_REVOLUTIONS_PER_HOUR							"maximum_revolutions_per_hour"
#define VELOCITY												"velocity"
#define ACCELERATION											"acceleration"
#define ACCELERATION_TIME										"acceleration_time"
#define DECELERATION											"deceleration"
#define DECELERATION_TIME										"deceleration_time"
#define STP														"stp"
#define STANDARD_FLOW_RATE										"standard_flow_rate"
#define RESERVOIR_VOLUME										"reservoir_volume"
#define CURRENT_SOLUTION_TYPE									"current_solution_type"
#define MAXIMUM_STROKE_SPEED									"maximum_stroke_speed"
#define MINIMUM_STROKE_SPEED									"minimum_stroke_speed"
#define LINEAR_DISPLACEMENT_PER_STEP							"linear_displacement_per_step"
#define SYRINGE_DIAMETER										"syringe_diameter"
#define SYRINGE_LENGTH											"syringe_length"
#define PUMP_SCALING_UNITS										"pump_scaling_units"
#define VENDOR													"vendor"
#define MODEL													"model"
#define SERIAL_NUMBER											"serial_number"
#define WHITE_LIGHT_SOURCE_TYPE									"white_light_source_type"
#define WHITE_LIGHT_SOURCE_SECONDS_USED							"white_light_source_seconds_used"
#define WHITE_LIGHT_SOURCE_LAST_CHANGED_DATE					"white_light_source_last_changed_date"
#define WHITE_LIGHT_ON_STATE									"white_light_on_state"
#define ANNULAR_RING_POSITION									"annular_ring_position"
#define REQUIRE_WHITE_LIGHT_SOURCE								"require_white_light_source"
#define FLUORESCENCE_FILTER_DISPLAY_NAME						"fluorescence_filter_display_name"
#define FLUORESCENCE_FILTER_EXCITATION_BAND						"fluorescence_filter_excitation_band"
#define FLUORESCENCE_FILTER_EMISSION_BAND						"fluorescence_filter_emission_band"
#define FLUORESCENCE_FILTER_SECONDS_USED						"fluorescence_filter_seconds_used"
#define FLUORESCENCE_FILTER_LAST_REPLACED_DATE					"fluorescence_filter_last_replaced_date"
#define FLUORESCENCE_FILTER_POSITION							"fluorescence_filter_position"
#define FLUORESCENCE_LIGHT_ON_STATE								"fluorescence_light_on_state"
#define RING_COUNT												"ring_count"
#define RING_BRIGHT_FIELD_POSITION								"ring_bright_field_position"
#define RING_FLUORESCENCE_POSITION								"ring_fluorescence_position"
#define DISPLAY_NAME_OBJECTIVE									"display_name_objective"
#define RING_POSITION_OBJECTIVE									"ring_position_objective"
#define AR_AXIS_RESOLUTION										"ar_axis_resolution"
#define AR_MAXIMUM_SPEED										"ar_maximum_speed"
#define AR_SET_SPEED											"ar_set_speed"
#define AR_ACCELERATION											"ar_acceleration"
#define AR_DECELERATION											"ar_deceleration"
#define AR_JOGGING_SPEED										"ar_jogging_speed"
#define AR_JOGGING_ACCELERATION									"ar_jogging_acceleration"
#define AR_JOGGING_DECELERATION									"ar_jogging_deceleration"
#define AR_DEFAULT_LOCATION_OFFSET								"ar_default_location_offset"
#define AR_INTER_RING_OFFSET									"ar_inter_ring_offset"
#define TURRET_POSITION_OBJECTIVE								"turret_position_objective"
#define TURRET_POSITION											"turret_position"
#define SAFE_MAGNIFICATION_CHANGE_HEIGHT						"safe_magnification_change_height"
#define FOCUS_TOTAL_TRAVEL										"focus_total_travel"
#define AUTO_FOCUS_START_OFFSET									"auto_focus_start_offset"
#define AUTO_FOCUS_INCREMENT									"auto_focus_increment"
#define AUTO_FOCUS_IMAGE_COUNT									"auto_focus_image_count"
#define FOCUS_POSITION											"focus_position"
#define PIXEL_COUNT_X											"pixel_count_x"
#define PIXEL_COUNT_Y											"pixel_count_y"
#define PIXEL_SIZE_X											"pixel_size_x"
#define PIXEL_SIZE_Y											"pixel_size_y"
#define SENSOR_WIDTH											"sensor_width"
#define SENSOR_HEIGHT											"sensor_height"
#define CAMERA_COLOR_DEPTH										"camera_color_depth"
#define SCALED_COLOR_DEPTH										"scaled_color_depth"
#define NUMBER_OF_COLOR_CHANNELS_USED							"number_of_color_channels_used"
#define GAIN													"gain"
#define EXPOSURE												"exposure"
#define BIN_FACTOR												"bin_factor"
#define ACTIVE_TURRET_POSITION									"active_turret_position"
#define CPD_CONTENTS											"cpd_contents"
#define CPD_TYPE												"cpd_type"
#define CPD_LIST												"cpd_list"
#define WELL_ROW												"well_row"
#define WELL_COLUMN												"well_column"
#define FROM_PRIMARY_LOCATION									"from_primary_location"
#define FROM_SUB_LOCATION										"from_sub_location"
#define FROM_SUB_SUB_LOCATION									"from_sub_sub_location"
#define TO_PRIMARY_LOCATION										"to_primary_location"
#define TO_SUB_LOCATION											"to_sub_location"
#define TO_SUB_SUB_LOCATION										"to_sub_sub_location"
#define READ_BARCODE											"read_barcode"
#define ACTIVE_FLUORESCENCE_FILTER_POSITION						"active_fluorescence_filter_position"
#define CURRENT_RING_POSITION									"current_ring_position"
#define ON_STATUS												"on_status"
#define ERROR_HEIGHT											"error_height"
#define WELL_BOTTOM_HEIGHT_NM									"well_bottom_height_nm"
#define HOME_OFFSET												"home_offset"
#define CLEAR_KILLS_TIMEOUT										"clear_kill_timout"
#define COORD_MOVE_ACCEL										"coord_move_accel"
#define COORD_MOVE_DECEL										"coord_move_decel"
#define COORD_MOVE_STP											"coord_move_stp"
#define COORD_MOVE_VEL											"coord_move_vel"
#define HOME_FINAL_VEL											"home_final_vel"
#define JOG_ACCEL												"jog_accel"
#define JOG_DECEL												"jog_decel"
#define JOG_VEL													"jog_vel"
#define JOG_HLDEC												"jog_hldec"
#define HOME_Y_INPUTS											"home_y_inputs"
#define HOME_X_INPUTS											"home_x_inputs"
#define HOME_YY_INPUTS											"home_yy_inputs"
#define HOME_XX_INPUTS											"home_xx_inputs"
#define HOME_SH_INPUTS											"home_sh_inputs"
#define HOME_Z_INPUTS											"home_z_inputs"
#define HOME_ZZ_INPUTS											"home_zz_inputs"
#define HOME_ZZZ_INPUTS											"home_zzz_inputs"
#define HOME_ASP_PUMP1_INPUTS									"home_asp_pump1_inputs"
#define HOME_DISP_PUMP1_INPUTS									"home_disp_pump1_inputs"
#define HOME_DISP_PUMP2_INPUTS									"home_disp_pump2_inputs"
#define HOME_DISP_PUMP3_INPUTS									"home_disp_pump3_inputs"
#define HOME_PICK_PUMP1_INPUTS									"home_pick_pump1_inputs"
#define HOME_WHITE_LIGHT_INPUTS									"home_white_light_inputs"
#define HOME_FLUORESCENCE_LIGHT_INPUTS							"home_fluorescence_light_inputs"
#define HOME_CAMERA_INPUTS										"home_camera_inputs"
#define HOME_OPTICAL_COLUMN_INPUTS								"home_optical_column_inputs"
#define HOME_INCUBATOR_INPUTS									"home_inclubator_inputs"
#define HOME_CXPM_INPUTS										"home_cxpm_inputs"
#define HOME_BARCODE_READER_INPUTS								"home_barcode_reader_inputs"
#define EXCESS_ERROR_Y											"excess_error_y"
#define EXCESS_ERROR_X											"excess_error_x"
#define EXCESS_ERROR_YY											"excess_error_yy"
#define EXCESS_ERROR_XX											"excess_error_xx"
#define EXCESS_ERROR_SH											"excess_error_sh"
#define EXCESS_ERROR_Z											"excess_error_z"
#define EXCESS_ERROR_ZZ											"excess_error_zz"
#define EXCESS_ERROR_ZZZ										"excess_error_zzz"
#define X_AXIS_ERROR_BAND										"x_axis_error_band"
#define Y_AXIS_ERROR_BAND										"y_axis_error_band"
#define XX_AXIS_ERROR_BAND										"xx_axis_error_band"
#define YY_AXIS_ERROR_BAND										"yy_axis_error_band"
#define AR_AXIS_ERROR_BAND										"ar_axis_error_band"
#define Z_AXIS_ERROR_BAND										"z_axis_error_band"
#define ZZ_AXIS_ERROR_BAND										"zz_axis_error_band"
#define ZZZ_AXIS_ERROR_BAND										"zzz_axis_error_band"
#define IN_POSITION_BAND_Y										"in_position_band_y"
#define IN_POSITION_BAND_X										"in_position_band_x"
#define IN_POSITION_BAND_YY										"in_position_band_yy"
#define IN_POSITION_BAND_XX										"in_position_band_xx"
#define IN_POSITION_BAND_SH										"in_position_band_sh"
#define IN_POSITION_BAND_Z										"in_position_band_z"
#define IN_POSITION_BAND_ZZ										"in_position_band_zz"
#define IN_POSITION_BAND_ZZZ									"in_position_band_zzz"

///----------------------------------------------------------------------
/// Call payload fields
///----------------------------------------------------------------------

/// Message types
#define DOES_MASTER_CLIENT_EXIST						"does_master_client_exist"							/* command-name */
#define REGISTER_AS_MASTER_CLIENT						"register_as_master_client"							/* command-name */
#define REGISTER_AS_OBSERVER_CLIENT						"register_as_observer_client"						/* command-name */
#define UNREGISTER_AS_CLIENT							"unregister_as_client"								/* command-name */
#define SYNCHRONIZE_TIME								"synchronize_time"									/* command-name */
#define IS_SYSTEM_BOOTING								"is_system_booting"									/* command-name */
#define GET_CXD_PROPERTIES								"get_cxd_properties"								/* command-name */ /* not-implemented */
#define GET_SYSTEM_SERIAL_NUMBER						"get_system_serial_number"							/* command-name */
#define APPEND_TO_CALIBRATION_LOG						"append_to_calibration_log"							/* command-name */ /* not-implemented */
#define GET_CALIBRATION_LOG								"get_calibration_log"								/* command-name */ /* not-implemented */
#define RESET_CALIBRATION_LOG							"reset_calibration_log"								/* command-name */ /* not-implemented */
#define APPEND_TO_MAINTENANCE_LOG						"append_to_maintenance_log"							/* command-name */ /* not-implemented */
#define GET_MAINTENANCE_LOG								"get_maintenance_log"								/* command-name */ /* not-implemented */
#define RESET_MAINTENANCE_LOG							"reset_maintenance_log"								/* command-name */ /* not-implemented */
#define GET_HARDWARE_EVENT_LOG							"get_hardware_event_log"							/* command-name */
#define RESET_HARDWARE_EVENT_LOG						"reset_hardware_event_log"							/* command-name */
#define GET_CURRENT_SYSTEM_CONFIGURATION				"get_current_system_configuration"					/* command-name */
#define GET_FIRMWARE_VERSION_NUMBER						"get_firmware_version_number"						/* command-name */
#define SET_SYSTEM_SERIAL_NUMBER						"set_system_serial_number"							/* command-name */
#define HOLD_OPERATION_STACK							"hold_operation_stack"								/* command-name */
#define HOLD_ALL_OPERATIONS								"hold_all_operations"								/* command-name */
#define RESUME_ALL_OPERATIONS							"resume_all_operations"								/* command-name */
#define KILL_PENDING_OPERATIONS							"kill_pending_operations"							/* command-name */
#define GET_ALARMS_LOG									"get_alarms_log"									/* command-name */
#define RESET_ALARMS_LOG								"reset_alarms_log"									/* command-name */
#define GET_CONNECTION_LOG								"get_connection_log"								/* command-name */ /* not-implemented */
#define RESET_CONNECTION_LOG							"reset_connection_log"								/* command-name */ /* not-implemented */
#define IS_ES_READY										"is_es_ready"										/* command-name */ /* not-implemented */
#define ADD_PLATE_TYPE_TO_DICTIONARY					"add_plate_type_to_dictionary"						/* command-name */ /* not-implemented */
#define REMOVE_PLATE_TYPE_FROM_DICTIONARY				"remove_plate_type_from_dictionary"					/* command-name */ /* not-implemented */
#define GET_PLATE_TYPE									"get_plate_type"									/* command-name */ /* not-implemented */
#define ADD_TIP_TYPE_TO_DICTIONARY						"add_tip_type_to_dictionary"						/* command-name */ /* not-implemented */
#define REMOVE_TIP_TYPE_FROM_DICTIONARY					"remove_tip_type_from_dictionary"					/* command-name */ /* not-implemented */
#define GET_TIP_TYPE									"get_tip_type"										/* command-name */ /* not-implemented */
#define UPLOAD_AND_APPLY_CPD							"upload_and_apply_cpd"								/* command-name */
#define UPLOAD_CPD										"upload_cpd"										/* command-name */
#define GET_CPD_LIST_FROM_TYPE							"get_cpd_list_from_type"							/* command-name */
#define GET_CPD_CONTENTS								"get_cpd_contents"									/* command-name */
#define DELETE_CPD										"delete_cpd"										/* command-name */
#define GET_COMPONENT_PROPERTIES						"get_component_properties"							/* command-name */ /* deferred */
#define SET_COMPONENT_PROPERTIES						"set_component_properties"							/* command-name */ /* deferred */
#define APPLY_CPD_TO_COMPONENT							"apply_cpd_to_component"							/* command-name */
#define MOVE_TO_XY_NM									"move_to_xy_nm"										/* command-name */
#define MOVE_TO_XY_RELATIVE_NM							"move_to_xy_relative_nm"							/* command-name */
#define GET_CURRENT_XY_NM								"get_current_xy_nm"									/* command-name */
#define MOVE_TO_X_LOW_LIMIT								"move_to_x_low_limit"								/* command-name */ /* not-implemented */
#define MOVE_TO_X_HIGH_LIMIT							"move_to_x_high_limit"								/* command-name */ /* not-implemented */
#define MOVE_X_TO_TIP_SKEW_TRIGGER						"move_x_to_tip_skew_trigger"						/* command-name */
#define MOVE_TO_Y_LOW_LIMIT								"move_to_y_low_limit"								/* command-name */ /* not-implemented */
#define MOVE_TO_Y_HIGH_LIMIT							"move_to_y_high_limit"								/* command-name */ /* not-implemented */
#define MOVE_Y_TO_TIP_SKEW_TRIGGER						"move_y_to_tip_skew_trigger"						/* command-name */
#define MOVE_XY_TO_OPTICAL_EYE_NM_A1_NOTATION			"move_XY_to_optical_eye_nm_A1_notation"				/* command-name */
#define MOVE_XY_TO_OPTICAL_EYE_NM_RC_NOTATION			"move_xy_to_optical_eye_nm_rc_notation"				/* command-name */
#define MOVE_XY_TO_TIP_WORKING_LOCATION_NM_A1_NOTATION	"move_XY_to_tip_working_location_nm_A1_notation"	/* command-name */
#define MOVE_XY_TO_TIP_WORKING_POSITION_NM_RC_NOTATION	"move_xy_to_tip_working_position_nm_rc_notation"	/* command-name */
#define SWIRL_XY_STAGE									"swirl_xy_stage"									/* command-name */ /* not-implemented */
#define GET_XY_PROPERTIES								"get_xy_properties"									/* command-name */
#define SET_XY_PROPERTIES								"set_xy_properties"									/* command-name */
#define MOVE_TO_XX_NM									"move_to_xx_nm"										/* command-name */
#define MOVE_TO_XX_RELATIVE_NM							"move_to_xx_relative_nm"							/* command-name */ /* not-implemented */
#define GET_CURRENT_XX_NM								"get_current_xx_nm"									/* command-name */
#define MOVE_TO_XX_LOW_LIMIT							"move_to_xx_low_limit"								/* command-name */ /* not-implemented */
#define MOVE_TO_XX_HIGH_LIMIT							"move_to_xx_high_limit"								/* command-name */ /* not-implemented */
#define GET_XX_PROPERTIES								"get_xx_properties"									/* command-name */
#define SET_XX_PROPERTIES								"set_xx_properties"									/* command-name */
#define MOVE_TO_YY_NM									"move_to_yy_nm"										/* command-name */
#define MOVE_TO_YY_RELATIVE_NM							"move_to_yy_relative_nm"							/* command-name */ /* not-implemented */
#define GET_CURRENT_YY_NM								"get_current_yy_nm"									/* command-name */
#define MOVE_TO_YY_LOW_LIMIT							"move_to_yy_low_limit"								/* command-name */ /* not-implemented */
#define MOVE_TO_YY_HIGH_LIMIT							"move_to_yy_high_limit"								/* command-name */ /* not-implemented */
#define GET_YY_PROPERTIES								"get_yy_properties"									/* command-name */
#define SET_YY_PROPERTIES								"set_yy_properties"									/* command-name */
#define MOVE_YY_TO_TIP_RACK_GRAB_LOCATION				"move_yy_to_tip_rack_grab_location"					/* command-name */
#define MOVE_TO_Z_NM									"move_to_z_nm"										/* command-name */
#define MOVE_TO_Z_RELATIVE_NM							"move_to_z_relative_nm"								/* command-name */
#define RETRACT_TO_Z_NM									"retract_to_z_nm"									/* command-name */
#define GET_CURRENT_Z_NM								"get_current_z_nm"									/* command-name */
#define MOVE_TO_Z_LOW_LIMIT								"move_to_z_low_limit"								/* command-name */ /* not-implemented */
#define MOVE_TO_Z_HIGH_LIMIT							"move_to_z_high_limit"								/* command-name */ /* not-implemented */
#define MOVE_Z_TO_SAFE_HEIGHT							"move_z_to_safe_height"								/* command-name */
#define GET_Z_PROPERTIES								"get_z_properties"									/* command-name */
#define SET_Z_PROPERTIES								"set_z_properties"									/* command-name */
#define MOVE_TO_Z_AXIS_FORCE_NEWTON						"move_to_z_axis_force_newton"						/* command-name */
#define MOVE_TO_Z_AXIS_SEATING_FORCE_NEWTON				"move_to_z_axis_seating_force_newton"				/* command-name */
#define MOVE_TO_Z_AXIS_WELL_BOTTOM_FORCE_NEWTON			"move_to_z_axis_well_bottom_force_newton"			/* command-name */
#define MOVE_TO_ZZ_NM									"move_to_zz_nm"										/* command-name */
#define MOVE_TO_ZZ_RELATIVE_NM							"move_to_zz_relative_nm"							/* command-name */
#define RETRACT_TO_ZZ_NM								"retract_to_zz_nm"									/* command-name */
#define GET_CURRENT_ZZ_NM								"get_current_zz_nm"									/* command-name */
#define MOVE_TO_ZZ_LOW_LIMIT							"move_to_zz_low_limit"								/* command-name */ /* not-implemented */
#define MOVE_TO_ZZ_HIGH_LIMIT							"move_to_zz_high_limit"								/* command-name */ /* not-implemented */
#define MOVE_ZZ_TO_SAFE_HEIGHT							"move_zz_to_safe_height"							/* command-name */
#define GET_ZZ_PROPERTIES								"get_zz_properties"									/* command-name */
#define SET_ZZ_PROPERTIES								"set_zz_properties"									/* command-name */
#define MOVE_TO_ZZ_AXIS_FORCE_NEWTON					"move_to_zz_axis_force_newton"						/* command-name */
#define MOVE_TO_ZZ_AXIS_SEATING_FORCE_NEWTON			"move_to_zz_axis_seating_force_newton"				/* command-name */
#define MOVE_TO_ZZ_AXIS_WELL_BOTTOM_FORCE_NEWTON		"move_to_zz_axis_well_bottom_force_newton"			/* command-name */
#define MOVE_TO_ZZZ_NM									"move_to_zzz_nm"									/* command-name */
#define MOVE_TO_ZZZ_RELATIVE_NM							"move_to_zzz_relative_nm"							/* command-name */
#define GET_CURRENT_ZZZ_NM								"get_current_zzz_nm"								/* command-name */
#define MOVE_TO_ZZZ_LOW_LIMIT							"move_to_zzz_low_limit"								/* command-name */ /* not-implemented */
#define MOVE_TO_ZZZ_HIGH_LIMIT							"move_to_zzz_high_limit"							/* command-name */ /* not-implemented */
#define MOVE_ZZZ_TO_SAFE_HEIGHT							"move_zzz_to_safe_height"							/* command-name */
#define GET_ZZZ_PROPERTIES								"get_zzz_properties"								/* command-name */
#define SET_ZZZ_PROPERTIES								"set_zzz_properties"								/* command-name */
#define ASPIRATE_ZZ_FLOW_RATE_TIME						"aspirate_zz_flow_rate_time"						/* command-name */
#define ASPIRATE_ZZ_FLOW_RATE_VOLUME					"aspirate_zz_flow_rate_volume"						/* command-name */
#define ASPIRATE_ZZ_VOLUME_TIME							"aspirate_zz_volume_time"							/* command-name */
#define ASPIRATION_PUMP_START_STOP						"aspiration_pump_start_stop"						/* command-name */
#define IS_ASPIRATION_PUMP_RUNNING						"is_aspiration_pump_running"						/* command-name */
#define GET_ASPIRATION_PUMP_PROPERTIES					"get_aspiration_pump_properties"					/* command-name */
#define SET_ASPIRATION_PUMP_PROPERTIES					"set_aspiration_pump_properties"					/* command-name */
#define DISPENSE_1_FLOW_RATE_TIME						"dispense_1_flow_rate_time"							/* command-name */
#define DISPENSE_1_FLOW_RATE_VOLUME						"dispense_1_flow_rate_volume"						/* command-name */
#define DISPENSE_1_VOLUME_TIME							"dispense_1_volume_time"							/* command-name */
#define DISPENSE_PUMP_1_START_STOP						"dispense_pump_1_start_stop"						/* command-name */
#define IS_DISPENSE_PUMP_1_RUNNING						"is_dispense_pump_1_running"						/* command-name */
#define GET_DISPENSE_PUMP_1_PROPERTIES					"get_dispense_pump_1_properties"					/* command-name */
#define SET_DISPENSE_PUMP_1_PROPERTIES					"set_dispense_pump_1_properties"					/* command-name */
#define DISPENSE_2_FLOW_RATE_TIME						"dispense_2_flow_rate_time"							/* command-name */
#define DISPENSE_2_FLOW_RATE_VOLUME						"dispense_2_flow_rate_volume"						/* command-name */
#define DISPENSE_2_VOLUME_TIME							"dispense_2_volume_time"							/* command-name */
#define DISPENSE_PUMP_2_START_STOP						"dispense_pump_2_start_stop"						/* command-name */
#define IS_DISPENSE_PUMP_2_RUNNING						"is_dispense_pump_2_running"						/* command-name */
#define GET_DISPENSE_PUMP_2_PROPERTIES					"get_dispense_pump_2_properties"					/* command-name */
#define SET_DISPENSE_PUMP_2_PROPERTIES					"set_dispense_pump_2_properties"					/* command-name */
#define DISPENSE_3_FLOW_RATE_TIME						"dispense_3_flow_rate_time"							/* command-name */
#define DISPENSE_3_FLOW_RATE_VOLUME						"dispense_3_flow_rate_volume"						/* command-name */
#define DISPENSE_3_VOLUME_TIME							"dispense_3_volume_time"							/* command-name */
#define DISPENSE_PUMP_3_START_STOP						"dispense_pump_3_start_stop"						/* command-name */
#define IS_DISPENSE_PUMP_3_RUNNING						"is_dispense_pump_3_running"						/* command-name */
#define GET_DISPENSE_PUMP_3_PROPERTIES					"get_dispense_pump_3_properties"					/* command-name */
#define SET_DISPENSE_PUMP_3_PROPERTIES					"set_dispense_pump_3_properties"					/* command-name */
#define PICKING_PUMP_FLOW_RATE_TIME						"picking_pump_flow_rate_time"						/* command-name */
#define PICKING_PUMP_FLOW_RATE_VOLUME					"picking_pump_flow_rate_volume"						/* command-name */
#define PICKING_PUMP_VOLUME_TIME						"picking_pump_volume_time"							/* command-name */
#define GET_PICKING_PUMP_CURRENT_VOLUME_NL				"get_picking_pump_current_volume_nl"				/* command-name */
#define GET_PICKING_PUMP_PROPERTIES						"get_picking_pump_properties"						/* command-name */
#define SET_PICKING_PUMP_PROPERTIES						"set_picking_pump_properties"						/* command-name */
#define GET_WHITE_LIGHT_SOURCE_PROPERTIES				"get_white_light_source_properties"					/* command-name */
#define SET_WHITE_LIGHT_SOURCE_PROPERTIES				"set_white_light_source_properties"					/* command-name */
#define GET_FLUORESCENCE_LIGHT_FILTER_PROPERTIES 		"get_fluorescence_light_filter_properties"			/* command-name */
#define SET_FLUORESCENCE_LIGHT_FILTER_PROPERTIES		"set_fluorescence_light_filter_properties"			/* command-name */
#define GET_ANNULAR_RING_HOLDER_PROPERTIES				"get_annular_ring_holder_properties"				/* command-name */
#define SET_ANNULAR_RING_HOLDER_PROPERTIES				"set_annular_ring_holder_properties"				/* command-name */
#define MOVE_TO_ANNULAR_RING_NM							"move_to_annular_ring_nm"							/* command-name */
#define MOVE_TO_FOCUS_STEP								"move_to_focus_step"								/* command-name */ /* deferred */
#define MOVE_TO_FOCUS_RELATIVE_STEP						"move_to_focus_relative_step"						/* command-name */ /* deferred */
#define GET_CURRENT_FOCUS_STEP							"get_current_focus_step"							/* command-name */ /* deferred */
#define MOVE_TO_FOCUS_NM								"move_to_focus_nm"									/* command-name */
#define MOVE_TO_FOCUS_RELATIVE_NM						"move_to_focus_relative_nm"							/* command-name */
#define GET_CURRENT_FOCUS_NM							"get_current_focus_nm"								/* command-name */
#define MOVE_TO_FOCUS_LOW_LIMIT							"move_to_focus_low_limit"							/* command-name */ /* not-implemented */
#define MOVE_TO_FOCUS_HIGH_LIMIT						"move_to_focus_high_limit"							/* command-name */ /* not-implemented */
#define ROTATE_TURRET_TO_POSITION						"rotate_turret_to_position"							/* command-name */ /* removed */
#define GET_OPTICAL_COLUMN_PROPERTIES					"get_optical_column_properties"						/* command-name */
#define SET_OPTICAL_COLUMN_PROPERTIES					"set_optical_column_properties"						/* command-name */
#define IS_CAMERA_IMAGE_AVAILABLE						"is_camera_image_available"							/* command-name */
#define GET_CAMERA_IMAGE								"get_camera_image"									/* command-name */
#define GET_CAMERA_PROPERTIES							"get_camera_properties"								/* command-name */
#define SET_CAMERA_PROPERTIES							"set_camera_properties"								/* command-name */
#define PLACE_PLATE_INTO_INCUBATOR						"place_plate_into_incubator"						/* command-name */ /* not-implemented */
#define RETRIEVE_PLATE_FROM_INCUBATOR					"retrieve_plate_from_incubator"						/* command-name */ /* not-implemented */
#define GET_INCUBATOR_PROPERTIES						"get_incubator_properties"							/* command-name */ /* not-implemented */
#define SET_INCUBATOR_PROPERTIES						"set_incubator_properties"							/* command-name */ /* not-implemented */
#define GET_CXD_PROPERTIES								"get_cxd_properties"								/* command-name */
#define SET_CXD_PROPERTIES								"set_cxd_properties"								/* command-name */
#define GET_CXPM_PROPERTIES								"get_cxpm_properties"								/* command-name */
#define SET_CXPM_PROPERTIES								"set_cxpm_properties"								/* command-name */
#define GET_BARCODE_READER_PROPERTIES					"get_barcode_reader_properties"						/* command-name */ /* not-implemented */
#define SET_BARCODE_READER_PROPERTIES					"set_barcode_reader_properties"						/* command-name */ /* not-implemented */
#define SET_DIGITAL_INPUT_ACTIVE_LEVEL					"set_digital_input_active_level"					/* command-name */ /* not-implemented */
#define GET_DIGITAL_INPUT_ACTIVE_LEVEL					"get_digital_input_active_level"					/* command-name */ /* not-implemented */
#define GET_DIGITAL_INPUT_STATE							"get_digital_input_state"							/* command-name */ /* not-implemented */
#define SET_DIGITAL_OUTPUT_ACTIVE_LEVEL					"set_digital_output_active_level"					/* command-name */ /* not-implemented */
#define GET_DIGITAL_OUTPUT_ACTIVE_LEVEL					"get_digital_output_active_level"					/* command-name */ /* not-implemented */
#define GET_DIGITAL_OUTPUT_STATE						"get_digital_output_state"							/* command-name */ /* not-implemented */
#define SET_DIGITAL_OUTPUT_STATE						"set_digital_output_state"							/* command-name */ /* not-implemented */
#define MOVE_ALL_TIPS_TO_SAFE_HEIGHT					"move_all_tips_to_safe_height"						/* command-name */ /* compound */
#define GET_TIP											"get_tip"											/* command-name */ /* compound */
#define MOVE_TIP_TO_WASTE_LOCATION_3					"move_tip_to_waste_location_3"						/* command-name */ /* compound */
#define MOVE_TIP_TO_WASTE_LOCATION_2					"move_tip_to_waste_location_2"						/* command-name */ /* compound */
#define MOVE_TIP_TO_WASTE_LOCATION_1					"move_tip_to_waste_location_1"						/* command-name */ /* compound */
#define SEAT_TIP										"seat_tip"											/* command-name */ /* compound */
#define GET_TIP_SKEW									"get_tip_skew"										/* command-name */ /* compound */
#define SET_WORKING_TIP									"set_working_tip"									/* command-name */ /* compound */
#define REMOVE_TIP										"remove_tip"										/* command-name */ /* compound */
#define COLLECT_MONTAGE_IMAGES							"collect_montage_images"							/* command-name */ /* compound */
#define MOVE_TO_Z_TIP_PICK_FORCE_NEWTON					"move_to_z_tip_pick_force_newton"					/* command-name */ /* compound */
#define MOVE_TO_Z_TIP_SEATING_FORCE_NEWTON				"move_to_z_tip_seating_force_newton"				/* command-name */ /* compound */
#define MOVE_TO_Z_TIP_WELL_BOTTOM_FORCE_NEWTON			"move_to_z_tip_well_bottom_force_newton"			/* command-name */ /* compound */
#define MOVE_TO_ZZ_TIP_PICK_FORCE_NEWTON				"move_to_zz_tip_pick_force_newton"					/* command-name */ /* compound */
#define MOVE_TO_ZZ_TIP_SEATING_FORCE_NEWTON				"move_to_zz_tip_seating_force_newton"				/* command-name */ /* compound */
#define MOVE_TO_ZZ_TIP_WELL_BOTTOM_FORCE_NEWTON			"move_to_zz_tip_well_bottom_force_newton"			/* command-name */ /* compound */
#define SET_OPTICAL_COLUMN_TURRET_POSITION				"set_optical_column_turret_position"				/* command-name */ /* compound */
#define GET_AUTO_FOCUS_IMAGE_STACK						"get_auto_focus_image_stack"						/* command-name */ /* compound */
#define CHANGE_ACTIVE_TIP_RACK							"change_active_tip_container"						/* command-name */ /* compound */ /* not-implemented */
#define ADD_PLATE_TO_DECK_INVENTORY						"add_plate_to_deck_inventory"						/* command-name */
#define REMOVE_PLATE_FROM_DECK_INVENTORY				"remove_plate_from_deck_inventory"					/* command-name */
#define MOVE_PLATE_IN_DECK_INVENTORY					"move_plate_in_deck_inventory"						/* command-name */
#define GET_PLATE_LIST_IN_LOCATION						"get_plate_list_in_location"						/* command-name */
#define FIND_PLATE_LOCATION_IN_INVENTORY				"find_plate_location_in_inventory"					/* command-name */
#define ADD_PLATE_LID_TO_DECK_INVENTORY					"add_plate_lid_to_deck_inventory"					/* command-name */ /* not-implemented */
#define REMOVE_PLATE_LID_FROM_DECK_INVENTORY			"remove_plate_lid_from_deck_inventory"				/* command-name */ /* not-implemented */
#define GET_PLATE_LID_LIST_IN_LOCATION					"get_plate_lid_list_in_location"					/* command-name */ /* not-implemented */
#define FIND_PLATE_LID_LOCATION_IN_INVENTORY			"find_plate_lid_location_in_inventory"				/* command-name */ /* not-implemented */
#define ADD_TIP_RACK_TO_DECK_INVENTORY					"add_tip_rack_to_deck_inventory"					/* command-name */
#define REMOVE_TIP_RACK_FROM_DECK_INVENTORY				"remove_tip_rack_from_deck_inventory"				/* command-name */
#define MOVE_TIP_RACK_IN_DECK_INVENTORY					"move_tip_rack_in_deck_inventory"					/* command-name */ /* not-implemented */
#define GET_TIP_RACK_LIST_IN_LOCATION					"get_tip_rack_list_in_location"						/* command-name */
#define FIND_TIP_RACK_LOCATION_IN_INVENTORY				"find_tip_rack_location_in_inventory"				/* command-name */
#define GET_TIP_COUNT_FOR_TIP_RACK						"get_tip_count_for_tip_rack"						/* command-name */
#define SET_TIP_COUNT_FOR_TIP_RACK						"set_tip_count_for_tip_rack"						/* command-name */
#define ADD_MEDIA_TO_DECK_INVENTORY						"add_media_to_deck_inventory"						/* command-name */ /* not-implemented */
#define REMOVE_MEDIA_AT_LOCATION_FROM_DECK_INVENTORY	"remove_media_at_location_from_deck_inventory"		/* command-name */ /* not-implemented */
#define GET_MEDIA_TYPE_LIST_IN_LOCATION					"get_media_type_list_in_location"					/* command-name */ /* not-implemented */
#define FIND_MEDIA_TYPE_LOCATIONS_IN_INVENTORY			"find_media_type_locations_in_inventory"			/* command-name */ /* not-implemented */
#define GET_MEDIA_VOLUME_UL_IN_LOCATION					"get_media_volume_ul_in_location"					/* command-name */ /* not-implemented */
#define SET_MEDIA_VOLUME_UL_IN_LOCATION					"set_media_volume_ul_in_location"					/* command-name */ /* not-implemented */
#define MOVE_PLATE										"move_plate"										/* command-name */
#define MOVE_PLATE_READ_BARCODE							"move_plate_read_barcode"							/* command-name */
#define DELID_PLATE										"delid_plate"										/* command-name */
#define RELID_PLATE										"relid_plate"										/* command-name */
#define MOVE_TIP_RACK									"move_tip_rack"										/* command-name */ /* not-implemented */
#define SET_HARDWARE_STATE								"set_hardware_state"								/* command-name */
#define GET_OPTICAL_COLUMN_EVENT_LOG					"get_optical_column_event_log"						/* command-name */
#define GET_FLUORESCENCE_LIGHT_FILTER_EVENT_LOG			"get_fluorescence_light_filter_event_log"			/* command-name */
#define SET_TURRET_POSITION								"set_turret_position"								/* command-name */
#define GET_OPTICAL_COLUMN_STATUS						"get_optical_column_status"							/* command-name */
#define GET_FLUORESCENCE_LIGHT_FILTER_STATUS			"get_fluorescence_light_filter_status"				/* command-name */
#define INIT_HARDWARE									"init_hardware"										/* command-name */
#define CHECK_HARDWARE									"check_hardware"									/* command-name */
#define INIT_HARDWARE_CONTROLLERS						"init_hardware_controllers"							/* command-name */
#define INIT_SERIAL_CONTROLLER							"init_serial_controller"							/* command-name */
#define HOME_SAFE_HARDWARE								"home_safe_hardware"								/* command-name */
#define HOME_UNSAFE_HARDWARE							"home_unsafe_hardware"								/* command-name */
#define INITIALIZE_PICKING_PUMP1						"initialize_picking_pump1"							/* command-name */
#define MOVE_HARDWARE_TO_ACTIVE_STATES					"move_hardware_to_active_states"					/* command-name */
#define SET_FLUORESCENCE_LIGHT_FILTER_ON				"set_fluorescence_light_filter_on"					/* command-name */
#define SET_FLUORESCENCE_LIGHT_FILTER_POSITION			"set_fluorescence_light_filter_position"			/* command-name */
#define SET_LOG_SUPPRESSION_LEVEL						"set_log_suppression_level"							/* command-name */
#define CLEAR_KILLS										"clear_kills"										/* command-name */
#define LOG_SUPPRESSION_LEVEL							"log_suppression_level"
#define GET_CXR_PROPERTIES								"get_cxr_properties"
#define Z_AXIS_PICKUP_FORCE								"z_axis_pickup_force"
#define ZZ_AXIS_PICKUP_FORCE							"zz_axis_pickup_force"
#define ZZZ_AXIS_PICKUP_FORCE							"zzz_axis_pickup_force"
#define Z_AXIS_SEATING_FORCE							"z_axis_seating_force"
#define ZZ_AXIS_SEATING_FORCE							"zz_axis_seating_force"
#define ZZZ_AXIS_SEATING_FORCE							"zzz_axis_seating_force"
#define X_TRIGGER_LOCATION_NM							"x_trigger_location_nm"
#define Y_TRIGGER_LOCATION_NM							"y_trigger_location_nm"
#define REQUESTING_CLIENT_GUID							"requesting_client_guid"
#define MASTER_CLIENT_EXISTS							"master_client_exists"
#define REGISTRATION_SUCCESSFUL							"registration_successful"
#define UNREGISTRATION_SUCCESSFUL						"unregistration_successful"
#define FLOW_RATE_UL_PER_S								"flow_rate_ul_per_s"
#define TIME_S											"time_s"
#define IS_NORMAL_DIRECTION								"is_normal_direction"
#define IS_ASPIRATE										"is_aspirate"
#define TIP_WORKING_VOLUME_NL							"tip_working_volume_nl"
#define VOLUME_UL										"volume_ul"
#define IS_TURN_ON										"is_turn_on"
#define FOCUS_NM										"focus_nm"
#define FOCUSPOS										"focuspos"
#define CURRENT_PICKING_PUMP_VOLUME_NL					"current_picking_pump_volume_nl"
#define FOCUS_OFFSET_NM									"focus_offset_nm"
#define IS_IMAGE_AVAILABLE								"is_image_available"
#define FILTER_INDEX									"filter_index"
#define IS_ON											"is_on"
#define X_PLATE_1_RIGHT_EDGE_TO_OPTICAL_PATH			"x_plate_1_right_edge_to_optical_path"
#define X_PLATE_2_LEFT_EDGE_TO_OPTICAL_PATH				"x_plate_2_left_edge_to_optical_path"
#define PLATE_PRIMARY_LOCATION							"plate_primary_location"
#define PLATE_SUB_LOCATION								"plate_sub_location"
#define PLATE_SUB_SUB_LOCATION							"plate_sub_sub_location"
#define LID_PRIMARY_LOCATION							"lid_primary_location"
#define LID_SUB_LOCATION								"lid_sub_location"
#define LID_SUB_SUB_LOCATION							"lid_sub_sub_location"
#define COMPONENT_ID									"component_id"
#define IS_PUMP_RUNNING									"is_pump_running"
#define IS_START_PUMP									"is_start_pump"
#define RETRACTION_HEIGHT_NM							"retraction_height_nm"
#define ANNULAR_RING_NM									"annular_ring_nm"
#define X_Z_TIP_SKEW_BASE_POSITION						"x_z_tip_skew_base_position"
#define X_ZZ_TIP_SKEW_BASE_POSITION						"x_zz_tip_skew_base_position"
#define Y_Z_TIP_SKEW_BASE_POSITION						"y_z_tip_skew_base_position"
#define Y_ZZ_TIP_SKEW_BASE_POSITION						"y_zz_tip_skew_base_position"
#define MONTAGE_SPEED									"montage_speed"
#define X_MONTAGE_SPEED_SLOW							"x_montage_speed_slow"
#define X_MONTAGE_SPEED_MEDIUM							"x_montage_speed_medium"
#define X_MONTAGE_SPEED_FAST							"x_montage_speed_fast"
#define Y_MONTAGE_SPEED_SLOW							"y_montage_speed_slow"
#define Y_MONTAGE_SPEED_MEDIUM							"y_montage_speed_medium"
#define Y_MONTAGE_SPEED_FAST							"y_montage_speed_fast"
#define X_MONTAGE_ACCELERATION_SLOW						"x_montage_acceleration_slow"
#define X_MONTAGE_ACCELERATION_MEDIUM					"x_montage_acceleration_medium"
#define X_MONTAGE_ACCELERATION_FAST						"x_montage_acceleration_fast"
#define Y_MONTAGE_ACCELERATION_SLOW						"y_montage_acceleration_slow"
#define Y_MONTAGE_ACCELERATION_MEDIUM					"y_montage_acceleration_medium"
#define Y_MONTAGE_ACCELERATION_FAST						"y_montage_acceleration_fast"
#define X_MONTAGE_DECELERATION_SLOW						"x_montage_deceleration_slow"
#define X_MONTAGE_DECELERATION_MEDIUM					"x_montage_deceleration_medium"
#define X_MONTAGE_DECELERATION_FAST						"x_montage_deceleration_fast"
#define Y_MONTAGE_DECELERATION_SLOW						"y_montage_deceleration_slow"
#define Y_MONTAGE_DECELERATION_MEDIUM					"y_montage_deceleration_medium"
#define Y_MONTAGE_DECELERATION_FAST						"y_montage_deceleration_fast"
/// UPLOAD_CPD payload strings
#define CPD_ID											"cpd_id"
#define MICROSCOPE_CPD_ID								"18"

/// partial commands
#define SET_Z_TO_SAFE_HEIGHT							"set_z_to_safe_height" // actually, move_z...
#define SET_ZZ_TO_SAFE_HEIGHT							"set_zz_to_safe_height"
#define SET_ZZZ_TO_SAFE_HEIGHT							"set_zzz_to_safe_height"

#define IMAGE											"image"
#define TAKE_IMAGE										"take_image"
#define UPLOAD_IMAGE									"upload_image"
#define DELAY											"delay"

#define SET_Z_AXIS_SPEED								"set_z_axis_speed"
#define SET_ZZ_AXIS_SPEED								"set_zz_axis_speed"

#define MOVE_TO_AUTO_FOCUS_RELATIVE_STEP				"move_to_auto_focus_relative_step"
#define FULFILL_MOVE_TO_ZZ_AXIS_SEATING_FORCE_NEWTON	"fulfill_move_to_zz_axis_seating_force_newton"

#define NEW_ACTIVE_TIP_RACK_ID							"new_active_tip_rack_id"
#define ROW_COUNT										"row_count"
#define COLUMN_COUNT									"column_count"
#define PLATE_LENGTH									"plate_length"
#define PLATE_WIDTH										"plate_width"
#define PLATE_HEIGHT									"plate_height"
#define FIRST_WELL_X									"first_well_x"
#define FIRST_WELL_Y									"first_well_y"
#define INTER_WELL_X									"inter_well_x"
#define INTER_WELL_Y									"inter_well_y"
#define WELL_VOLUME										"well_volume"
#define WORKING_VOLUME									"working_volume"
#define WELL_GEOMETRY									"well_geometry"
#define WELL_INNER_DIAMETER								"well_inner_diameter"
#define WELL_INNER_HEIGHT								"well_inner_height"
#define WELL_LENGTH										"well_length"
#define WELL_WIDTH										"well_width"
#define SWIRL_RADIUS									"swirl_radius"
#define SWIRL_VELOCITY									"swirl_velocity"
#define SWIRL_ACCELERATION								"swirl_acceleration"
#define PLATE_TYPE										"plate_type"
#define PLATE_POSITION									"plate_position"
#define WELL											"well"
#define X_OFFSET_NM										"x_offset_nm"
#define X_START_OFFSET_NM								"x_start_offset_nm"
#define Y_OFFSET_NM										"y_offset_nm"
#define Y_START_OFFSET_NM								"y_start_offset_nm"
#define X_INDEX											"x_index"
#define Y_INDEX											"y_index"
#define PHOTO_PATH_NAME									"photo_path_name"
#define READ_AND_SEND_PHOTO								"read_and_send_photo"
#define PLATE_ID										"plate_id"
#define ID												"id"
#define TIP_RACK_ID										"tip_rack_id"
#define PRIMARY_LOCATION								"primary_location"
#define SUB_LOCATION									"sub_location"
#define SUB_SUB_LOCATION								"sub_sub_location"
#define PLATE_LIST										"plate_list"
#define TIP_RACK_LIST									"tip_rack_list"

/// Unique symbols/keys for specific log lines
#define INTERMEDIATE_MOVE_KEY							"mv_int"
#define INTERMEDIATE_NIP_KEY							"mv_nip"
#define TERMINATE_MOVE_KEY								"mv_fin"

///----------------------------------------------------------------------
/// Fulfiller Commands
/// These extend the Data Exchange Specification to include some new
/// messages and to add new payload parameters to existing messages.
///
/// All DES messages are in JSON format, so these extensions are being
/// *documented* here for convenience's sake--not *declared* as
/// C++ functions.
///
/// Fulfiller Commands are only used for internal communication between
/// the CommandInterpreter thread and the CommandDoler thread.
///----------------------------------------------------------------------
/**
	SetZAxisSpeed (new)
Description		Change the speed of the ZAxis motor.
Type			Operational
Channel			Command
Control Flow	ES internal inter-thread control flow
Call Payload	{
					"message_type": "set_z_axis_speed",
					"transport_version": "1.0",
					"command_group_id": int,
					"command_id": int,
					"session_id": string,
					"machine_id": string,
					"payload": {
						"speed": long
					}
				}
Response		N/A (no response)
Complete Status	N/A (no status)

	SetZZAxisSpeed (new)
Description		Change the speed of the ZZAxis motor.
Type			Operational
Channel			Command
Control Flow	ES internal inter-thread control flow
Call Payload	{
					"message_type": "set_zz_axis_speed",
					"transport_version": "1.0",
					"command_group_id": int,
					"command_id": int,
					"session_id": string,
					"machine_id": string,
					"payload": {
						"speed": long
					}
				}
Response		N/A (no response)
Complete Status	N/A (no status)
 
	moveZToSafeHeight (extended)
Description: The normal DES version plus the following additions to the call payload:
			"z_safe_height": ulong

	moveZZToSafeHeight (extended)
Description: The normal DES version plus the following additions to the call payload:
			"zz_safe_height": ulong

	moveZZZToSafeHeight (extended)
Description: The normal DES version plus the following additions to the call payload:
			"zzz_safe_height": ulong

	moveToZUM (TBD)
Description: TBD.

	moveToZZUM (TBD)
Description: TBD.

**/
#endif 

