#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <cassert>
#include "main.hpp"
#include "es_des.h"
#include "enums.hpp"
#include "MqttReceiver.hpp"
#include "MsgQueue.hpp"
#include "Fault.hpp"
#include "MQTTClient.h"
#include "DesHelper.hpp"
#include "json.h"

using namespace std;

volatile MQTTClient_deliveryToken deliveredToken;

///----------------------------------------------------------------------
/// Delivered
///----------------------------------------------------------------------
static void Delivered(void *context, MQTTClient_deliveryToken dt)
{
//    cout << "Message with token value " << dt << " delivery confirmed" << endl;
    deliveredToken = dt;
}

///----------------------------------------------------------------------
/// MessageArrived
///----------------------------------------------------------------------
static int MessageArrived(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
    int i;
    char* payloadptr;
    /// outBoxQueues contains pointers to MqttReceiverThread properties, in order to make them
    /// available to this static function.
    outBoxMsgQueues *outBoxQueues = (outBoxMsgQueues *) context;
    bool messageParsed = false;
    bool ackHandled = false;
    bool callHandled = false;
    bool isaCommand = false;
    bool isaUploadCpdCommand = false;
    bool cpdIdIsForMicroscope = false;
    bool isaRegistrationCommand = false;
	bool commandIsAuthorized = false;
	bool senderIsMaster = false;
	bool senderIsAnObserver = false;
	bool doForwardToNextThread = false; 
    std::string messageType = "";
	std::string transportVersion = "";
	int commandGroupId = -1;
	int commandId = -1;
	std::string sessionId = "";
	std::string machineId = "";
    // TODO: Consider making desHelper a member of outBoxQueues so it doesn't have to be created
    // anew for every message.
    DesHelper desHelper(outBoxQueues->machineId, outBoxQueues->masterId);

//	cout << outBoxQueues->m_threadName << ": message arrived from " << topicName << endl;
//	cout << outBoxQueues->m_threadName << ": received payload is " << (const char*)message->payload << endl;

    /// The payload is JASON. convert it to an object so we can analyze it.
    json_object *jobj = desHelper.ParseJsonDesMessage( (const char*)message->payload, &messageParsed );

	if (messageParsed)
	{
		json_object *messageTypeObject = nullptr;
		json_bool found = json_object_object_get_ex(jobj, MESSAGE_TYPE, &messageTypeObject);
		
		if (found)
		{
			messageType = json_object_get_string(messageTypeObject);
			// cout << outBoxQueues->m_threadName << ": " << MESSAGE_TYPE << " found. it is " << messageType << endl;
		}
		else
		{
			LOG_WARN( outBoxQueues->m_threadName, "mqtt", "message_type not found" );
		}
	}
	else
	{
		/// Send an Ack with "rejected" status code.
		/// We can't fill out a proper Ack without parsing the incoming message, but something is better
		/// than nothing for debugging purposes.
		messageType = FAILED_TO_PARSE;
		/// Create the new parts of an ACK message. Some parts are new.
		/// Some parts are copied from the received message.
		///
		/// Ack needs a timestamp.
		std::chrono::system_clock::rep timeInSec = TimeSinceEpoch();
		/// Create a payload that tries to give the caller some indication of what went wrong.
		
		/// Pull a command_id and session_id out of thin air because we couldn't parse them from the message.
		/// Create an ACK message in json-object form.
		json_object *ack_jobj =
			desHelper.ComposeAck( messageType, 
								  0,					/// commandGroupId
								  0,					/// commandId,
								  ERROR,				/// sessionId,
								  STATUS_STR_REJECTED,	/// status,
								  STATUS_REJECTED,		/// statusCode,
								  timeInSec,
								  jobj );

		/// Create a topic (string) to be the destination.
		/// Acknowledgements always return to the '/ack' channel of the sender topic.
		std::string topic = topicName;
		std::string channelAck = TEXT_ACK;
		std::string ackTopicName = ReplaceTopicChannel(topicName, channelAck);
//		cout << outBoxQueues->m_threadName << ": (1) Pushing ack to " << ackTopicName << endl;

		/// Create a thread message containing the ack, its original "from" topic,
		/// and its new destination topic.
		std::shared_ptr<WorkMsg> ack =
			make_shared<WorkMsg>( json_object_to_json_string(ack_jobj),
								  topicName,
								  ackTopicName,
								  messageType.c_str(),
								  "",
								  false );
		
		/// Send the thread message.
		outBoxQueues->ackOutBox->PushMsg( ack );
		;
		/// EARLY RETURN!  EARLY RETURN!
		/// Don't fall through to end; avoid sending the message on to
		/// the command interpreter and don't send a status.
		return 1;
	}

	/// Did it come from a '/command' channel?
	std::string const topicNameStr = topicName;
	std::string::size_type matchLength = topicNameStr.find(CHANNEL_COMMAND);
	if (matchLength != std::string::npos)
	{
		isaCommand = true;
		/// Is the command for uploading Component Properties Definitions (CPD)?
		std::string uploadCpdMsgType = UPLOAD_CPD;
		isaUploadCpdCommand = (messageType == uploadCpdMsgType);
	}

    /// Acknowledge any command addressed to a local component.
    /// Acks go to MqttSenderThread.
    /// Don't acknowledge a command to the microscope. The microscope will
    /// Do that. The next block of code will ferry those microscope
    /// commands and acks back and forth.
	if (messageParsed && !ackHandled && isaCommand)
	{
		string whyInvalid = "";
		bool msgIsValid = desHelper.IsValidCommand(jobj, whyInvalid);
		
		/// Create the new parts of an ACK message. Some parts are new.
		/// Some parts are copied from the received message.
		///
		/// Ack needs a timestamp.
		std::chrono::system_clock::rep timeInSec = TimeSinceEpoch();
		/// Select the status.
		std::string status = msgIsValid ? STATUS_STR_OK : STATUS_STR_REJECTED;
		int commandId = 0;
		std::string sessionId = ERROR;
		/// Pull a command_id and session_id from the incoming command.
		msgIsValid &= desHelper.SplitCommand( jobj,
											  nullptr, /// messageType is already known.
											  &transportVersion,
											  &commandGroupId,
											  &commandId,
											  &sessionId,
											  &machineId );

		if (!msgIsValid)
		{
			LOG_DEBUG( outBoxQueues->m_threadName, "mqtt", whyInvalid.c_str() );
			/// Create an ACK message in json-object form.
			json_object *ack_jobj =
				desHelper.ComposeAck( messageType,
									  commandGroupId,
									  commandId,
									  sessionId,
									  STATUS_STR_REJECTED,	/// status,
									  STATUS_REJECTED,		/// statusCode,
									  timeInSec,
									  jobj );

			/// Create a topic (string) to be the destination.
			/// Acknowledgements always return to the '/ack' channel of the sender topic.
			std::string topic = topicName;
			std::string channelAck = TEXT_ACK;
			std::string ackTopicName = ReplaceTopicChannel(topicName, channelAck);
//			cout << outBoxQueues->m_threadName << ": (1) Pushing ack to " << ackTopicName << endl;

			/// Create a thread message containing the ack, its original "from" topic,
			/// and its new destination topic.
			std::shared_ptr<WorkMsg> ack =
				make_shared<WorkMsg>( json_object_to_json_string(ack_jobj),
									  topicName,
									  ackTopicName,
									  messageType.c_str(),
									  "",
									  false );
			
			/// Send the thread message.
			outBoxQueues->ackOutBox->PushMsg( ack );
			;
			/// EARLY RETURN!  EARLY RETURN!
			/// Don't fall through to end; avoid sending the message on to
			/// the command interpreter and don't send a status.
			return 1;
		}

		/// Create a status code.
		int statusCode = (msgIsValid) ? STATUS_OK : STATUS_REJECTED;
		/// Pull a payload from the incoming command.
		json_object *payloadObject = json_object_object_get( jobj, PAYLOAD );
		json_object *payloadObjectCopy = nullptr;
		if (payloadObject)
		{
			/// Make a deep copy of the payload for use in the outgoing ack object.
			i = json_object_deep_copy(payloadObject, &payloadObjectCopy, NULL);
			if (i != 0)
			{
				LOG_ERROR( outBoxQueues->m_threadName, "mqtt", "Failed to make deep copy" );
			}
		}
		else
		{
			LOG_WARN( outBoxQueues->m_threadName, "mqtt", "payload not found" );
			payloadObjectCopy = json_object_new_object();
		}
		
		/// Some commands are handled here.
		/// Deal with Registration commands here, so we can return an appropriate Ack.
		///
		/// DOES_MASTER_CLIENT_EXIST
		///
		if (isaCommand && (messageType == DOES_MASTER_CLIENT_EXIST))
		{
//			cout << outBoxQueues->m_threadName << ": command messageType is " << messageType << endl;
			isaRegistrationCommand = true;
			json_object * payloadObj = json_object_new_object();
			if (outBoxQueues->masterId == "")
			{
				/// No.
				int result = json_object_object_add( payloadObj, MASTER_CLIENT_EXISTS, json_object_new_boolean( false ) );
				LETS_ASSERT(result == 0);
			}
			else
			{
				/// Yes.
				int result = json_object_object_add( payloadObj, MASTER_CLIENT_EXISTS, json_object_new_boolean( true ) );
				LETS_ASSERT(result == 0);
			}
			
			/// Create a STATUS message in json-object form.
			json_object *statusObj =
				desHelper.ComposeStatus( commandGroupId, commandId, sessionId, messageType, machineId, statusCode, payloadObj );

			/// Create a thread message containing the status, its original "from" topic,
			/// and its new destination topic.
			std::string destinationTopic = TOPIC_ES;
			destinationTopic += CHANNEL_STATUS;
			std::shared_ptr<WorkMsg> statusWorkMsg =
				make_shared<WorkMsg> ( json_object_to_json_string(statusObj), // std::string payloadInJson
									   topicName, // std::string originTopic,
									   destinationTopic ); // std::string destinationTopic

			/// Send this to ackOutBox, which sends it to MqttSender.
			outBoxQueues->ackOutBox->PushMsg(statusWorkMsg);
			
			/// Now we free these structures: ack_jobj, jobj, and maybe
			/// payloadObjectCopy indirectly.
			int freed = 0;
			freed = json_object_put(statusObj);
//			if (freed != 1) cout << outBoxQueues->m_threadName << ": failed to free status_jobj" << endl;
	
			statusCode = STATUS_OK;
			/// Now let flow continue to allow the common code for acknowledgements to send the Ack.
		}
		
		///
		/// REGISTER_AS_MASTER_CLIENT
		///
		if (isaCommand && (messageType == REGISTER_AS_MASTER_CLIENT))
		{
			isaRegistrationCommand = true;
			json_object * payloadObj = json_object_new_object();
			int result = json_object_object_add( payloadObj, MACHINE_ID, json_object_new_string( outBoxQueues->machineId.c_str() ) );
			string clientGuid = desHelper.ExtractPayloadStringValue( jobj, REQUESTING_CLIENT_GUID );
			if (outBoxQueues->masterId == "")
			{
				/// OK.
				outBoxQueues->masterId = clientGuid;
				result = json_object_object_add( payloadObj, REGISTRATION_SUCCESSFUL, json_object_new_boolean( true ) );
				LETS_ASSERT(result == 0);
			}
			else
			{
				if (outBoxQueues->masterId != clientGuid)
				{
					/// Error: Not allowed to usurp mastership.
					statusCode = STATUS_FORBIDDEN;
					int result = json_object_object_add( payloadObj, REGISTRATION_SUCCESSFUL, json_object_new_boolean( false ) );
					LETS_ASSERT(result == 0);
				}
				else
				{
					/// Tell the client that it is (still) the master.
					result = json_object_object_add( payloadObj, REGISTRATION_SUCCESSFUL, json_object_new_boolean( true ) );
				}
			}

			/// Create a STATUS message in json-object form.
			json_object *statusObj =
				desHelper.ComposeStatus( commandGroupId, commandId, sessionId, messageType, machineId, statusCode, payloadObj );

			/// Create a thread message containing the status, its original "from" topic,
			/// and its new destination topic.
			std::string destinationTopic = TOPIC_ES;
			destinationTopic += CHANNEL_STATUS;
			std::shared_ptr<WorkMsg> statusWorkMsg =
				make_shared<WorkMsg> ( json_object_to_json_string(statusObj), // std::string payloadInJson
									   topicName, // std::string originTopic,
									   destinationTopic ); // std::string destinationTopic

			// cout << outBoxQueues->m_threadName << ": status message is " << statusWorkMsg->payload << endl;
			/// Send this to ackOutBox, which sends it to MqttSender.
			outBoxQueues->ackOutBox->PushMsg(statusWorkMsg);
			
			/// Now we free these structures: ack_jobj, jobj, and maybe
			/// payloadObjectCopy indirectly.
			int freed = 0;
			freed = json_object_put(statusObj);
//			if (freed != 1) cout << outBoxQueues->m_threadName << ": failed to free status_jobj" << endl;
	
			statusCode = STATUS_OK;
			/// Now let flow continue to allow the common code for acknowledgements to send the Ack.
		}
		
		///
		/// REGISTER_AS_OBSERVER_CLIENT
		///
		if (isaCommand && (messageType == REGISTER_AS_OBSERVER_CLIENT))
		{
			isaRegistrationCommand = true;
			json_object * payloadObj = json_object_new_object();
			int result = json_object_object_add( payloadObj, MACHINE_ID, json_object_new_string( outBoxQueues->machineId.c_str() ) );
			LETS_ASSERT(result == 0);
			string clientGuid = desHelper.ExtractPayloadStringValue( jobj, REQUESTING_CLIENT_GUID );
			if (outBoxQueues->masterId == clientGuid)
			{
				/// Error: Not allowed to be both master and observer
				statusCode = STATUS_FORBIDDEN;
				result = json_object_object_add( payloadObj, REGISTRATION_SUCCESSFUL, json_object_new_boolean( false ) );
				LETS_ASSERT(result == 0);
			}
			else
			{
				/// Search for sessionId among our list of observers.
				vector<string>::iterator it = find( outBoxQueues->observerIdVector.begin(),
													outBoxQueues->observerIdVector.end(),
													clientGuid );
				if (it == outBoxQueues->observerIdVector.end()) /// Not found.
				{
					/// OK. Add clientGuid to list of observers.
					outBoxQueues->observerIdVector.push_back( clientGuid );
					result = json_object_object_add( payloadObj, REGISTRATION_SUCCESSFUL, json_object_new_boolean( true ) );
					LETS_ASSERT(result == 0);
				}
				else
				{
					/// Tell the client that it is (still) an observer
					result = json_object_object_add( payloadObj, REGISTRATION_SUCCESSFUL, json_object_new_boolean( true ) );
					LETS_ASSERT(result == 0);
				}
			}

			/// Create a STATUS message in json-object form.
			json_object *statusObj =
				desHelper.ComposeStatus( commandGroupId, commandId, sessionId, messageType, machineId, statusCode, payloadObj );

			/// Create a thread message containing the status, its original "from" topic,
			/// and its new destination topic.
			std::string destinationTopic = TOPIC_ES;
			destinationTopic += CHANNEL_STATUS;
			std::shared_ptr<WorkMsg> statusWorkMsg =
				make_shared<WorkMsg> ( json_object_to_json_string(statusObj), // std::string payloadInJson
									   topicName, // std::string originTopic,
									   destinationTopic ); // std::string destinationTopic

			// cout << outBoxQueues->m_threadName << ": status message is " << statusWorkMsg->payload << endl;
			/// Send this to ackOutBox, which sends it to MqttSender.
			outBoxQueues->ackOutBox->PushMsg(statusWorkMsg);
			
			/// Now we free these structures: ack_jobj, jobj, and maybe
			/// payloadObjectCopy indirectly.
			int freed = 0;
			freed = json_object_put(statusObj);
//			if (freed != 1) cout << outBoxQueues->m_threadName << ": failed to free status_jobj" << endl;
	
			statusCode = STATUS_OK;
			/// Now let flow continue to allow the common code for acknowledgements to send the Ack.
		}
		
		///
		/// UNREGISTER_AS_CLIENT
		///
		if (isaCommand && (messageType == UNREGISTER_AS_CLIENT))
		{
			isaRegistrationCommand = true;
			json_object * payloadObj = json_object_new_object();
			string clientGuid = desHelper.ExtractPayloadStringValue( jobj, REQUESTING_CLIENT_GUID );
			if (outBoxQueues->masterId == clientGuid)
			{
				/// OK.
				outBoxQueues->masterId = "";
				int result = json_object_object_add( payloadObj, UNREGISTRATION_SUCCESSFUL, json_object_new_boolean( true ) );
				LETS_ASSERT(result == 0);
			}
			else
			{
				/// Search for sessionId among our list of observers.
				vector<string>::iterator it = find( outBoxQueues->observerIdVector.begin(),
													outBoxQueues->observerIdVector.end(),
													clientGuid );
				if (it != outBoxQueues->observerIdVector.end()) /// Found.
				{
					/// OK. Delete sessionId
					outBoxQueues->observerIdVector.erase( it );
					int result = json_object_object_add( payloadObj, UNREGISTRATION_SUCCESSFUL, json_object_new_boolean( true ) );
					LETS_ASSERT(result == 0);
				}
				else
				{
					int result = json_object_object_add( payloadObj, UNREGISTRATION_SUCCESSFUL, json_object_new_boolean( false ) );
					LETS_ASSERT(result == 0);
				}
			}

			/// Create a STATUS message in json-object form.
			json_object *statusObj =
				desHelper.ComposeStatus( commandGroupId, commandId, sessionId, messageType, machineId, statusCode, payloadObj );

			/// Create a thread message containing the status, its original "from" topic,
			/// and its new destination topic.
			std::string destinationTopic = TOPIC_ES;
			destinationTopic += CHANNEL_STATUS;
			std::shared_ptr<WorkMsg> statusWorkMsg =
				make_shared<WorkMsg> ( json_object_to_json_string(statusObj), // std::string payloadInJson
									   topicName, // std::string originTopic,
									   destinationTopic ); // std::string destinationTopic

			// cout << outBoxQueues->m_threadName << ": status message is " << statusWorkMsg->payload << endl;
			/// Send this to ackOutBox, which sends it to MqttSender.
			outBoxQueues->ackOutBox->PushMsg(statusWorkMsg);

			/// Now we free these structures: ack_jobj, jobj, and maybe
			/// payloadObjectCopy indirectly.
			int freed = 0;
			freed = json_object_put(statusObj);
//			if (freed != 1) cout << outBoxQueues->m_threadName << ": failed to free status_jobj" << endl;

			statusCode = STATUS_OK;
			/// Now let flow continue to allow the common code for acknowledgements to send the Ack.
		}
		
		///
		/// IS_SYSTEM_BOOTING
		///
		if (isaCommand && (messageType == IS_SYSTEM_BOOTING))
		{
			ComponentState EsState = g_EsHwState.load( std::memory_order_acquire );
			bool isSystemBooting = true;
			switch( EsState )
			{
				case ComponentState::Uninitialized :
					LOG_TRACE( outBoxQueues->m_threadName, "init", "state is Unitialized" );
					break;
				case ComponentState::Good :
					LOG_TRACE( outBoxQueues->m_threadName, "init", "state is Good" );
					isSystemBooting = false;
					break;
				case ComponentState::Faulty :
					LOG_TRACE( outBoxQueues->m_threadName, "init", "state is Faulty" );
					isSystemBooting = false;
					break;
				case ComponentState::Mocked :
					LOG_TRACE( outBoxQueues->m_threadName, "init", "state is Mocked" );
					isSystemBooting = false;
					break;
				default:
					LOG_TRACE( outBoxQueues->m_threadName, "init", "state is UNKNOWN" );
					break;
			}
			isaRegistrationCommand = true; /// Not really, but we want this command handled the same way as true Reg command--not forwarded to worker threads.
			json_object * payloadObj = json_object_new_object();
			int result = json_object_object_add( payloadObj, IS_SYSTEM_BOOTING, json_object_new_boolean( isSystemBooting ) );
			LETS_ASSERT(result == 0);

			/// Create a STATUS message in json-object form.
			json_object *statusObj =
				desHelper.ComposeStatus( commandGroupId, commandId, sessionId, messageType, machineId, statusCode, payloadObj );

			/// Create a thread message containing the status, its original "from" topic,
			/// and its new destination topic.
			std::string destinationTopic = TOPIC_ES;
			destinationTopic += CHANNEL_STATUS;
			std::shared_ptr<WorkMsg> statusWorkMsg =
				make_shared<WorkMsg> ( json_object_to_json_string(statusObj), /// payloadInJson
									   topicName, /// originTopic,
									   destinationTopic ); /// destinationTopic

			/// Send this to ackOutBox, which sends it to MqttSender.
			outBoxQueues->ackOutBox->PushMsg(statusWorkMsg);
			
			/// Now we free these structures: ack_jobj, jobj, and maybe
			/// payloadObjectCopy indirectly.
			int freed = 0;
			freed = json_object_put(statusObj);
			if (freed != 1) cout << outBoxQueues->m_threadName << ": failed to free status_jobj" << endl;
	
			statusCode = STATUS_OK;
			/// Now let flow continue to allow the common code for acknowledgements to send the Ack.
		}

		///
		/// SET_LOG_SUPPRESSION_LEVEL
		///
		if (isaCommand && (messageType == SET_LOG_SUPPRESSION_LEVEL))
		{
			json_object * payloadObj = json_object_object_get( jobj, PAYLOAD );
			if (payloadObj)
			{
				json_object * levelObj = json_object_object_get( payloadObj, LOG_SUPPRESSION_LEVEL );
				if (levelObj)
				{
					string level = json_object_get_string( levelObj );
					SetLogSuppressionSeverity( level );
				}
			}

			payloadObj = nullptr;
			payloadObj = json_object_new_object();
			int result = json_object_object_add( payloadObj, MACHINE_ID, json_object_new_string( outBoxQueues->machineId.c_str() ) );
			LETS_ASSERT(result == 0);

			/// Create a STATUS message in json-object form.
			json_object *statusObj = desHelper.ComposeStatus( commandGroupId, commandId, sessionId, messageType, machineId, statusCode, payloadObj );

			/// Create a thread message containing the status, its original "from" topic,
			/// and its new destination topic.
			std::string destinationTopic = TOPIC_ES;
			destinationTopic += CHANNEL_STATUS;
			std::shared_ptr<WorkMsg> statusWorkMsg = make_shared<WorkMsg> ( json_object_to_json_string(statusObj), topicName, destinationTopic );

			/// Send this to ackOutBox, which sends it to MqttSender.
			outBoxQueues->ackOutBox->PushMsg(statusWorkMsg);
			
			/// Now we free these structures: ack_jobj, jobj, and maybe
			/// payloadObjectCopy indirectly.
			int freed = 0;
			freed = json_object_put(statusObj);
	
			statusCode = STATUS_OK;
			/// Now let flow continue to allow the common code for acknowledgements to send the Ack.
		}
		
		///
		/// Inspect the sender's papers. Determine whether they are authorized to invode this command.
		///
		if (outBoxQueues->masterId == sessionId)
		{
			senderIsMaster = true;
			doForwardToNextThread = true;
//			cout << "Client is MASTER." << endl;
		}
		/// Search for sessionId among our list of observers.
		vector<string>::iterator it = find( outBoxQueues->observerIdVector.begin(),
											outBoxQueues->observerIdVector.end(),
											sessionId );
		if (it != outBoxQueues->observerIdVector.end()) /// Found.
		{
			senderIsAnObserver = true;
//			cout << "Client is objerver." << endl;

			if ( (messageType == DOES_MASTER_CLIENT_EXIST) ||
				 (messageType == REGISTER_AS_MASTER_CLIENT) ||
				 (messageType == REGISTER_AS_OBSERVER_CLIENT) ||
				 (messageType == UNREGISTER_AS_CLIENT) ||
				 (messageType == GET_CXD_PROPERTIES) ||
				 (messageType == GET_SYSTEM_SERIAL_NUMBER) ||
				 (messageType == GET_CALIBRATION_LOG) ||
				 (messageType == GET_MAINTENANCE_LOG) ||
				 (messageType == GET_HARDWARE_EVENT_LOG) ||
				 (messageType == GET_CURRENT_SYSTEM_CONFIGURATION) ||
				 (messageType == GET_FIRMWARE_VERSION_NUMBER) ||
				 (messageType == GET_ALARMS_LOG) ||
				 (messageType == GET_CONNECTION_LOG) ||
				 (messageType == IS_ES_READY) ||
				 (messageType == GET_CPD_LIST_FROM_TYPE) ||
				 (messageType == GET_CPD_CONTENTS) ||
				 (messageType == GET_COMPONENT_PROPERTIES) ||
				 (messageType == GET_CURRENT_XY_NM) ||
				 (messageType == GET_XY_PROPERTIES) ||
				 (messageType == GET_CURRENT_XX_NM) ||
				 (messageType == GET_XX_PROPERTIES) ||
				 (messageType == SET_XX_PROPERTIES) ||
				 (messageType == GET_CURRENT_YY_NM) ||
				 (messageType == GET_YY_PROPERTIES) ||
				 (messageType == GET_CURRENT_Z_NM) ||
				 (messageType == GET_Z_PROPERTIES) ||
				 (messageType == GET_CURRENT_ZZ_NM) ||
				 (messageType == GET_ZZ_PROPERTIES) ||
				 (messageType == SET_ZZ_PROPERTIES) ||
				 (messageType == GET_CURRENT_ZZZ_NM) ||
				 (messageType == GET_ZZZ_PROPERTIES) ||
				 (messageType == SET_ZZZ_PROPERTIES) ||
				 (messageType == GET_ASPIRATION_PUMP_PROPERTIES) ||
				 (messageType == GET_DISPENSE_PUMP_1_PROPERTIES) ||
				 (messageType == GET_DISPENSE_PUMP_2_PROPERTIES) ||
				 (messageType == SET_DISPENSE_PUMP_2_PROPERTIES) ||
				 (messageType == GET_DISPENSE_PUMP_3_PROPERTIES) ||
				 (messageType == GET_PICKING_PUMP_CURRENT_VOLUME_NL) ||
				 (messageType == GET_PICKING_PUMP_PROPERTIES) ||
				 (messageType == GET_WHITE_LIGHT_SOURCE_PROPERTIES) ||
				 (messageType == GET_FLUORESCENCE_LIGHT_FILTER_PROPERTIES) ||
				 (messageType == GET_ANNULAR_RING_HOLDER_PROPERTIES) ||
				 (messageType == GET_CURRENT_FOCUS_STEP) ||
				 (messageType == GET_CURRENT_FOCUS_NM) ||
				 (messageType == GET_OPTICAL_COLUMN_PROPERTIES) ||
				 (messageType == IS_CAMERA_IMAGE_AVAILABLE) ||
				 (messageType == GET_CAMERA_PROPERTIES) ||
				 (messageType == GET_INCUBATOR_PROPERTIES) ||
				 (messageType == GET_CXPM_PROPERTIES) ||
				 (messageType == GET_BARCODE_READER_PROPERTIES) ||
				 (messageType == GET_DIGITAL_INPUT_ACTIVE_LEVEL) ||
				 (messageType == GET_DIGITAL_INPUT_STATE) ||
				 (messageType == GET_DIGITAL_OUTPUT_ACTIVE_LEVEL) ||
				 (messageType == GET_DIGITAL_OUTPUT_STATE) ||
				 (messageType == GET_PLATE_LIST_IN_LOCATION) ||
				 (messageType == GET_PLATE_LID_LIST_IN_LOCATION) ||
				 (messageType == FIND_PLATE_LID_LOCATION_IN_INVENTORY) ||
				 (messageType == GET_TIP_RACK_LIST_IN_LOCATION) ||
				 (messageType == FIND_TIP_RACK_LOCATION_IN_INVENTORY) ||
				 (messageType == GET_TIP_COUNT_FOR_TIP_RACK) ||
				 (messageType == GET_MEDIA_TYPE_LIST_IN_LOCATION) ||
				 (messageType == FIND_MEDIA_TYPE_LOCATIONS_IN_INVENTORY) ||
				 (messageType == GET_MEDIA_VOLUME_UL_IN_LOCATION) )
			{
				doForwardToNextThread = true;
//				cout << "Observer is allowed to do this." << endl;
			}
			else
			{
				/// UNAUTHORIZED!
				statusCode = STATUS_FORBIDDEN;
				doForwardToNextThread = false;
				stringstream msg;
				msg << "Observer tried to perform " << messageType;
				LOG_WARN( outBoxQueues->m_threadName, "mqtt", msg.str().c_str() );
			}
		}
		if (!senderIsMaster && !senderIsAnObserver)
		{
//			cout << "Client is neither master nor observer." << endl;
			/// Give unregistered clients just enough support for them to decide
			/// whether or not to register.
			if ( (messageType == GET_SYSTEM_SERIAL_NUMBER) ||
				 (messageType == GET_FIRMWARE_VERSION_NUMBER) )
			{
				doForwardToNextThread = true;
			}
			else
			{
				doForwardToNextThread = false;
				if ( (messageType == DOES_MASTER_CLIENT_EXIST) ||
					 (messageType == REGISTER_AS_MASTER_CLIENT) ||
					 (messageType == REGISTER_AS_OBSERVER_CLIENT) ||
					 (messageType == UNREGISTER_AS_CLIENT) ||
					 (messageType == IS_SYSTEM_BOOTING) )
				{
					/// Do nothing here. The statusCode has already been updated.
				}
				else
				{
					statusCode = STATUS_FORBIDDEN;
					stringstream msg;
					msg << "Outsider tried to perform " << messageType;
					LOG_WARN( outBoxQueues->m_threadName, "mqtt", msg.str().c_str() );
				}
			}
		}
			
		/// Create an ACK message in json-object form.
		json_object *ack_jobj =
			desHelper.ComposeAck( messageType,
								  commandGroupId,
								  commandId,
								  sessionId,
								  status,
								  statusCode,
								  timeInSec,
								  payloadObjectCopy );
		/// Create a topic (string) to be the destination.
		/// Acknowledgements always return to the '/ack' channel of the sender topic.
		std::string topic = topicName;
		std::string channelAck = TEXT_ACK;
		std::string ackTopicName = ReplaceTopicChannel(topicName, channelAck);
//		cout << outBoxQueues->m_threadName << ": (2) Pushing ack to " << ackTopicName << endl;

		/// Create a thread message containing the ack, its original "from" topic,
		/// and its new destination topic.
		std::shared_ptr<WorkMsg> ack =
			make_shared<WorkMsg>( json_object_to_json_string(ack_jobj),
								  topicName,
								  ackTopicName,
								  messageType.c_str(),
								  "",
								  false );
//		cout << outBoxQueues->m_threadName << ": payload is " << json_object_to_json_string(ack_jobj) << endl;
//		cout << outBoxQueues->m_threadName << ": ack messageType is " << ack->msgType << endl;
		
		/// Send the thread message.
		outBoxQueues->ackOutBox->PushMsg( ack );
		
		/// Now we free these structures: ack_jobj, jobj, and maybe
		/// payloadObjectCopy indirectly.
		int freed = 0;
		freed = json_object_put(ack_jobj);
		if (freed != 1) cout << outBoxQueues->m_threadName << ": failed to free ack_jobj" << endl;
		freed = json_object_put(jobj);
		if (freed != 1) cout << outBoxQueues->m_threadName << ": failed to free jobj" << endl;
	}

	if (isaRegistrationCommand)
	{
		/// EARLY RETURN!  EARLY RETURN!
		/// Command was processed before we got here.
		/// Ack and Status were sent.
		/// Don't fall through to end; avoid sending the message on to
		/// the command interpreter.
		return 1;
	}

	if (DesHelper::IsAnImmediateCommand(messageType))
	{
//		cout << outBoxQueues->m_threadName << ": command messageType is " << messageType << " (2)" << endl;
		if (messageType == HOLD_ALL_OPERATIONS)
		{
			LOG_WARN( outBoxQueues->m_threadName, "immediate", "HOLD_ALL_OPERATIONS" );
			g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed );
			/// EARLY RETURN!  EARLY RETURN!
			/// Don't fall through to end; avoid sending the message on to
			/// the command interpreter and don't send a status.
			return 1;
		}
		if (messageType == HOLD_OPERATION_STACK)
		{
			LOG_WARN( outBoxQueues->m_threadName, "immediate", "HOLD_OPERATION_STACK" );
			g_EsHoldState.store( HoldType::HoldButFinishCurrentWork, std::memory_order_relaxed );
			/// EARLY RETURN!  EARLY RETURN!
			/// Don't fall through to end; avoid sending the message on to
			/// the command interpreter and don't send a status.
			return 1;
		}
		if (messageType == RESUME_ALL_OPERATIONS)
		{
			LOG_WARN( outBoxQueues->m_threadName, "immediate", "RESUME_ALL_OPERATIONS" );
			g_EsHoldState.store( HoldType::NoHold_RunFree, std::memory_order_relaxed );
			/// EARLY RETURN!  EARLY RETURN!
			/// Don't fall through to end; avoid sending the message on to
			/// the command interpreter and don't send a status.
			return 1;
		}
		if (messageType == KILL_PENDING_OPERATIONS)
		{
			LOG_WARN( outBoxQueues->m_threadName, "immediate", "KILL_PENDING_OPERATIONS" );
			if (g_EsHoldState.load(std::memory_order_acquire) != HoldType::NoHold_RunFree)
			{
				g_DoFlushQueues.store(true, std::memory_order_relaxed);
				ManageHoldStates(); /// Run this immediately instead of waiting for main to wake up.
			}
			else
			{
				// TODO: Find out how to handle an error state.
			}
			/// EARLY RETURN!  EARLY RETURN!
			/// Don't fall through to end; avoid sending the message on to
			/// the command interpreter and don't send a status.
			return 1;
		}
	}
	
	/// Handle any command addressed to a local component.
	/// Routine commands go to the CommandInterpreterThread.
	if (doForwardToNextThread)
	{
		std::shared_ptr<WorkMsg> command =
			make_shared<WorkMsg>( (const char *)message->payload, topicName, topicName );
		command->msgType = messageType;
//		cout << outBoxQueues->m_threadName << ": command messageType is " << command->msgType << " (1)" << endl;
		outBoxQueues->commandOutBox->PushMsg( command );
	}
	else
	{
		stringstream msg;
		msg << "Not forwarding to worker threads: " << messageType;
		LOG_DEBUG( outBoxQueues->m_threadName, "mqtt", msg.str().c_str() );
	}
    return 1;
} /// End of MessageArrived

///----------------------------------------------------------------------
/// ConnectionLost
///----------------------------------------------------------------------
static void ConnectionLost(void *context, char *cause)
{
	g_ConnectedToBroker.store( false, std::memory_order_relaxed );
    cout << "Connection lost" << endl;
    cout << "     cause: " << cause << endl;
}

///----------------------------------------------------------------------
/// MqttReceiverThread
/// Constructor. Perform one-time initializations
/// Makes the list of topics to which this thread will subscribe--
/// listOfTopics. See the Process() method for where it gets used.
///----------------------------------------------------------------------
MqttReceiverThread::MqttReceiverThread(const char* threadName) : m_thread(nullptr),
    m_brokerIpAddress("0.0.0.0"),
   	m_continueRunning(false)
{
	g_ConnectedToBroker.store( false, std::memory_order_relaxed );
	m_outBoxQueues.ackOutBox = nullptr;
	m_outBoxQueues.commandOutBox = nullptr;
	m_outBoxQueues.m_threadName = threadName;
    m_desHelper;
	
	string topic = TOPIC_ES;
	topic += CHANNEL_COMMAND;
	m_listOfTopics.push_front(topic);
	
	topic = TOPIC_ES;
	topic += CHANNEL_ALERT;
	m_listOfTopics.push_front(topic);
	
	m_outBoxQueues.masterId = "";
	
	// This whole block is temporary code to support a microscope type
	// that is not directly driven by the embedded server. Instead, the
	// microscope is controlled by fowarding MQTT calls to/from
	// another application.
	// TODO: uScope; Delete this block at a later time, when a new type
	// of microscope has been integrated.
	if (true)
	{
		topic = TOPIC_MICROSCOPE;
		topic += CHANNEL_ACK;
		m_listOfTopics.push_front(topic);
	
		topic = TOPIC_MICROSCOPE;
		topic += CHANNEL_ALERT;
		m_listOfTopics.push_front(topic);
	
		topic = TOPIC_MICROSCOPE;
		topic += CHANNEL_STATUS;
		m_listOfTopics.push_front(topic);
	}
}

///----------------------------------------------------------------------
/// ~MqttReceiverThread
///----------------------------------------------------------------------
MqttReceiverThread::~MqttReceiverThread()
{
	m_listOfTopics.clear();
}

///----------------------------------------------------------------------
/// CreateThread
///----------------------------------------------------------------------
bool MqttReceiverThread::CreateThread()
{
	bool success = false;
	if (!m_thread)
	{
		m_continueRunning = true;
		m_thread = std::unique_ptr<std::thread>(new thread(&MqttReceiverThread::Process, this));
		success = true;
	}
	return success;
}

///----------------------------------------------------------------------
/// ExitThread
///----------------------------------------------------------------------
void MqttReceiverThread::ExitThread()
{
	m_continueRunning = false;
	if (!m_thread)
	{
		cout << "MqttReceiver exited poorly" << endl;
		return;
	}

    m_thread->join();
    m_thread = nullptr;
    cout << "MqttReceiver exited OK" << endl;
}

///----------------------------------------------------------------------
/// AttachCommandOutBox
///----------------------------------------------------------------------
void MqttReceiverThread::AttachCommandOutBox(MsgQueue *q)
{
	LETS_ASSERT((!m_outBoxQueues.commandOutBox) && "AttachCommandOutBox called second time?");
    m_outBoxQueues.commandOutBox = q;
}
    
///----------------------------------------------------------------------
/// AttachAckOutBox
///----------------------------------------------------------------------
void MqttReceiverThread::AttachAckOutBox(MsgQueue *q)
{
	LETS_ASSERT((!m_outBoxQueues.ackOutBox) && "AttachAckOutBox called second time?");
	m_outBoxQueues.ackOutBox = q;
}

///----------------------------------------------------------------------
/// SetBrokerIpAddress
///----------------------------------------------------------------------
void MqttReceiverThread::SetBrokerIpAddress(std::string ipAddress)
{
	// TODO: Validate the address?
	m_brokerIpAddress = ipAddress;
}

///----------------------------------------------------------------------
/// SetMachineId
///----------------------------------------------------------------------
void MqttReceiverThread::SetMachineId(std::string id)
{
    m_outBoxQueues.machineId = id;
}

///----------------------------------------------------------------------
/// SetMasterId
///----------------------------------------------------------------------
void MqttReceiverThread::SetMasterId(std::string id)
{
    m_outBoxQueues.masterId = id;
}


///----------------------------------------------------------------------
/// Process
/// Main loop. Watches the inbox for work.
/// Depending on the nature of the incomming message, either ConnectionLost,
/// MessageArrived, or Delivered perform the work.
/// The are callback functions, lying outside of the MqttReceiverThread
/// class, so data must be passed into them via the m_outBoxQueues structure,
/// which is named "context" inside those functions. 
///----------------------------------------------------------------------
void MqttReceiverThread::Process()
{
    MQTTClient client;
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
    int mqttReturnCode;
    
//    cout << m_outBoxQueues.m_threadName << ": MqttReceiver is up and running" << endl;
    /// Record the time when this process started. This will be used by code in MessageArrived that handles IsSystemBooting.
	time( &(m_outBoxQueues.box_opening_time_seconds) );

    stringstream address;
    address <<  "tcp://" << m_brokerIpAddress << ":" << RECEIVER_BROKER_PORT;
    MQTTClient_create(
        &client,
        address.str().c_str(),
        RECEIVER_CLIENTID,
        MQTTCLIENT_PERSISTENCE_NONE,
        NULL);
    conn_opts.keepAliveInterval = RECEIVER_KEEP_ALIVE_INTERVAL;
    conn_opts.cleansession = RECEIVER_CLEAN_SESSION;
	MQTTClient_setCallbacks(client, (void *)&m_outBoxQueues, ConnectionLost, MessageArrived, Delivered);

	time_t last_hw_ping_time_seconds;
	time( &last_hw_ping_time_seconds );  // Get current time, in seconds since 00:00 hours, Jan 1, 1970 UTC.
	string payloadInJson = "{ \"message_type\": \"check_hardware\", \"transport_version\": \"1.0\", \"command_group_id\": 0, \"command_id\": 0, \"session_id\": NULL, \"machine_id\": NULL, \"payload\": NULL }";
	std::shared_ptr<WorkMsg> check_hw_command =
		make_shared<WorkMsg> ( payloadInJson,
							   "", 				/// originTopic,
							   "",				/// destinationTopic,
							   CHECK_HARDWARE,	/// messageType,
							   "", 				/// subMessageType
							   true); 			///  theEnd
  
	while (m_continueRunning && m_outBoxQueues.commandOutBox && m_outBoxQueues.ackOutBox)
	{
		if (g_ConnectedToBroker.load( std::memory_order_acquire ))
		{
		    // Fake some work. (The callbacks do most of the work.)
            std::this_thread::sleep_for( 250ms );
			time_t now_time_seconds;
			time( &now_time_seconds );
			int diffTime = 0;
			ComponentState EsState = g_EsHwState.load( std::memory_order_acquire );
			if ((EsState == ComponentState::Good) || (EsState == ComponentState::Faulty))
			{
				diffTime = difftime(now_time_seconds, last_hw_ping_time_seconds);
			}
			else
			{
				last_hw_ping_time_seconds = now_time_seconds;
			}
			if (diffTime > HW_PING_RATE_SECONDS)
			{
				LOG_TRACE( m_outBoxQueues.m_threadName, "health", "pinging HW" );
				MQTTClient_yield();
				last_hw_ping_time_seconds = now_time_seconds;
				/// Send a command to initiate hardware pings.
				m_outBoxQueues.commandOutBox->PushMsg( check_hw_command );
			}
	    }
	    else
	    {
			/// Attempt to connect to the broker and subscribe to topics.
			if ((mqttReturnCode = MQTTClient_connect(client, &conn_opts)) == MQTTCLIENT_SUCCESS)
			{
				g_ConnectedToBroker.store( true, std::memory_order_relaxed );
				LOG_INFO( m_outBoxQueues.m_threadName, "health", "Connected to broker" );
				for (const auto& topic : m_listOfTopics)
				{
					MQTTClient_subscribe(client, topic.c_str(), RECEIVER_QOS);
				}
			}
			else
			{
				g_ConnectedToBroker.store( false, std::memory_order_relaxed );
				stringstream msg;
				msg << "Failed to connect; return code is " << mqttReturnCode;
				LOG_ERROR( m_outBoxQueues.m_threadName, "health", msg.str().c_str() );
			}
		}
	}

	/// THE END. Clean up.
	if (g_ConnectedToBroker.load( std::memory_order_acquire ))
	{
		for (const auto& topic : m_listOfTopics)
		{
			MQTTClient_unsubscribe(client, topic.c_str());
		}
		MQTTClient_disconnect(client, TIMEOUT);
		MQTTClient_destroy(&client);
		cout << "MqttReceiver disconnected cleanly." << endl;
	}
}

void MqttReceiverThread::SendDisconnectAlert()
{
	stringstream topic_stream;
	topic_stream << TOPIC_ES << CHANNEL_ALERT;
	
	/// Send an alert to tell any connected CxSuite machines that they are being disconnected
	json_object * alertObj = m_desHelper.ComposeAlert( TEXT_ALERT,
														0, /// commandGroupId
														0, /// commandId
														topic_stream.str(), /// sessionId
														ComponentType::ComponentNone,
														STATUS_CONTROLLED_SHUTDOWN,
														false ); /// thisIsCausingHold

	bool sentOK = m_desHelper.SendAlert( alertObj, m_outBoxQueues.ackOutBox, topic_stream.str() );
	if (sentOK)
	{
		LOG_TRACE( m_outBoxQueues.m_threadName, "exit", "Sent alert OK" );
	}
	else
	{
		LOG_ERROR( m_outBoxQueues.m_threadName, "exit", "Failed to send alert" );
	}
}
