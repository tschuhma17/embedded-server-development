#include <iostream>
#include <thread>
#include <array>
#include <mutex>
#include "main.hpp"
#include "CommandInterpreter.hpp"
#include "CommandDoler.hpp"
#include "Components.hpp"
#include "MqttReceiver.hpp"
#include "MqttSender.hpp"
#include "MsgQueue.hpp"
#include "Fault.hpp"
#include "Sundry.hpp"
#include "sys/types.h"
#include "sys/stat.h"
#include "es_des.h"
#include <linux/rtc.h>
#include <time.h>
#include <sys/ioctl.h>
#include <array>

using namespace std;

/// NOTE: Let's standardize on one way to use atomic variables. According to the research described at
/// https://www.arangodb.com/2015/02/comparing-atomic-mutex-rwlocks/, the most efficient approach
/// for Linux ARM environments is to use Atomic Read “acquire” for getting, Atomic Set “relaxed” for setting.
/// See ManageHoldStates() for examples.

/// Allows any thread to look up the current XX location and compare it to the default location.
std::atomic<long> g_XxCurrentOffset;
/// Allows any thread to look up the XX default location.
std::atomic<long> g_XxDefaultLocationOffset;

/// Allows any thread to look up the current YY location and compare it to the default location.
std::atomic<long> g_YyCurrentOffset;
/// Allows any thread to look up the YY default location.
std::atomic<long> g_YyDefaultLocationOffset;

/// THE state variable for hold states that pause operations.
std::atomic<HoldType> g_EsHoldState;
std::atomic<HoldType> g_PreviousHoldState;

/// THE state variable managing a temporary state during which queues are flushed. It behaves like a trigger.
std::atomic<bool> g_DoFlushQueues;

/// THE state vaiables maintaining current records of the states of various hardware components.
std::atomic<ComponentState> g_EsHwState; /// Summarizes the states of all of the following hardware components.
// std::atomic<ComponentState> g_CxDState;
std::atomic<ComponentState> g_XYState;
std::atomic<ComponentState> g_XXState;
std::atomic<ComponentState> g_YYState;
std::atomic<ComponentState> g_ZState;
std::atomic<ComponentState> g_ZZState;
std::atomic<ComponentState> g_ZZZState;
std::atomic<ComponentState> g_AspirationPump1State;
std::atomic<ComponentState> g_DispensePump1State;
std::atomic<ComponentState> g_DispensePump2State;
std::atomic<ComponentState> g_DispensePump3State;
std::atomic<ComponentState> g_PickingPump1State;
std::atomic<ComponentState> g_WhiteLightSourceState;
std::atomic<ComponentState> g_FluorescenceLightFilterState;
std::atomic<ComponentState> g_AnnularRingHolderState;
std::atomic<ComponentState> g_CameraState;
std::atomic<ComponentState> g_OpticalColumnState;
std::atomic<ComponentState> g_IncubatorState;
std::atomic<ComponentState> g_CxPMState;
std::atomic<ComponentState> g_BarcodeReaderState;
std::string m_BrokerIpAddressConfiguration = "localhost";

/// THE list of components that are free to move first during initialization.
/// (They can move first, because there is no risk of them hitting other components.)
std::array<ComponentType, 15> m_SafeToHomeArray =
{
	ComponentType::ZAxis,
	ComponentType::ZZAxis,
	ComponentType::ZZZAxis,
	ComponentType::AspirationPump1,
	ComponentType::DispensePump1,
	ComponentType::DispensePump2,
	ComponentType::DispensePump3,
	ComponentType::PickingPump1,
	ComponentType::WhiteLightSource,
	ComponentType::FluorescenceLightFilter,
	ComponentType::AnnularRingHolder,
	ComponentType::Camera,
	ComponentType::OpticalColumn, /// Focus only
	ComponentType::Incubator,
	ComponentType::BarcodeReader
};

/// THE list of components that should wait to home after the other components are home.
std::array<ComponentType, 5> m_UnsafeToHomeArray =
{
	ComponentType::XYAxes,
	ComponentType::XXAxis,
	ComponentType::YYAxis,
	ComponentType::OpticalColumn, /// Turret only
	ComponentType::CxPM,
};

/// THE mutex to use for error logs, alert logs, and any other logs.
std::mutex g_logFileMutex;
/// THE mutex for accessing the camera.
std::mutex g_cameraMutex;
/// THE Camera list. (All CommandDoler threads have access, but only 2 should be using it. They should use g_cameraMutex for access and only one should initialize and destroy it.)
std::vector<Camera> g_CameraList;
std::atomic<bool> g_CameraIsInitialized;


/// Worker thread instances (private)
MqttReceiverThread receivesFromMqtt( MQTT_RECEIVER );
CommandInterpreterThread interpretsCommand( COMMAND_INTERPRETER );
CommandDolerThread dolerForOptics( DOLER_FOR_OPTICS );
CommandDolerThread dolerForInfo( DOLER_FOR_INFO );
CommandDolerThread dolerForCxD( DOLER_FOR_CXD );
CommandDolerThread dolerForCxPM( DOLER_FOR_CXPM );
CommandDolerThread dolerForPump( DOLER_FOR_PUMP );
CommandDolerThread dolerForRingHolder( DOLER_FOR_RING_HOLDER );
MqttSenderThread sendsToMqtt( MQTT_SENDER );
std::array<CommandDolerThread *, 6> DolerThreadArray =
{
	&dolerForOptics,
	&dolerForInfo,
	&dolerForCxD,
	&dolerForCxPM,
	&dolerForPump,
	&dolerForRingHolder,
};

/// Message queues between worker threads (private)
HoldableMsgQueue* jobSchedule = new HoldableMsgQueue("JobsQ");
HoldableMsgQueue* msgQueueForOptics = new HoldableMsgQueue("DolerOpticsQ");
HoldableMsgQueue* msgQueueForInfo = new HoldableMsgQueue("DolerInfoQ");
HoldableMsgQueue* msgQueueForCxD = new HoldableMsgQueue("DolerCxDQ");
HoldableMsgQueue* msgQueueForCxPM = new HoldableMsgQueue("DolerCxPMQ");
HoldableMsgQueue* msgQueueForPump = new HoldableMsgQueue("DolerPumpQ");
HoldableMsgQueue* msgQueueForRingHolder = new HoldableMsgQueue("DolerRingHolderQ");
MsgQueue* outboundMqtt = new MsgQueue("OutboundQ");
/// IMPORTANT: This array should be maintained so its size and order always matches those of DolerThreadArray.
std::array<HoldableMsgQueue *, 6> DolerQueueArray =
{
	msgQueueForOptics,
	msgQueueForInfo,
	msgQueueForCxD,
	msgQueueForCxPM,
	msgQueueForPump,
	msgQueueForRingHolder,
};

MotionServices g_motionServices;
uint32_t g_sequenceId;

///----------------------------------------------------------------------
/// AllCommandThreadsAreWaitingOnQueues() (private)
/// Returns true when MsgQueues feeding the CommandInterpreter thread and all CommandDoler
/// threads are waiting for messages--implying that all of those threads are idle.
///----------------------------------------------------------------------
bool AllCommandThreadsAreWaitingOnQueues()
{
	if (jobSchedule->IsWaitingOnQueue() == false) return false;
	for ( auto it = DolerQueueArray.begin(); it != DolerQueueArray.end(); ++it )
	{
		if ((*it)->IsWaitingOnQueue() == false) return false;
	}
	return true;
}

///----------------------------------------------------------------------
/// ManageHoldStates
/// A public function that implements hold states.
/// Called periodically by main(), this function checks the values of
/// global HOLD state variables and updates internal hold states
/// in the message queues accordingly.
///
/// Occasionally called by MqttReceiver when to speed up the handling
/// of a kill_pending_operations command.
///
/// Note that message queues could, theoretically, watch the global states
/// themselves. We are not doing that because all message queues share
/// the same implementation and only some of them need to implement holds.
/// The outboundMqtt queue is one that must not go on hold.
///----------------------------------------------------------------------
std::mutex mangeHoldStateMutex;
void ManageHoldStates()
{
	unique_lock<mutex> lock(mangeHoldStateMutex);
//	cout << "main: ManageHoldStates" << endl;
	HoldType holdState = g_EsHoldState.load( std::memory_order_acquire );
	HoldType previousHoldState = g_PreviousHoldState.load( std::memory_order_acquire );
	if (holdState != previousHoldState)
	{
		g_PreviousHoldState.store( holdState, std::memory_order_relaxed );
		if (holdState == HoldType::NoHold_RunFree)
		{
//			cout << "main: HoldState = RunFree" << endl;
			unique_lock<mutex> lock2(g_holdMutex);
			g_holdCVA.notify_all();
		}
		if (holdState == HoldType::HoldButFinishCurrentWork)
		{
			cout << "main: HoldState = HoldButFinishCurrentWork" << endl;
			bool doFlushQueues = g_DoFlushQueues.load(std::memory_order_acquire);
			if (doFlushQueues)
			{
//				cout << "main: flushing queues (1)" << endl;
				jobSchedule->DiscardAllMsgs();
				msgQueueForOptics->DiscardAllMsgs();
				for ( auto it = DolerQueueArray.begin(); it != DolerQueueArray.end(); ++it )
				{
					(*it)->DiscardAllMsgs();
				}
				; /// Do not discard the outboundMqtt queue.
				/// Reset the trigger.
				g_DoFlushQueues.store(false, std::memory_order_relaxed);
			}
		}
		if (holdState == HoldType::HoldUp_Freeze)
		{
//			cout << "main: HoldState = HoldUp_Freeze" << endl;
			bool doFlushQueues = g_DoFlushQueues.load(std::memory_order_acquire);
			if (doFlushQueues)
			{
				cout << "main: flushing queues (2)" << endl;
				jobSchedule->DiscardAllMsgs();
				for ( auto it = DolerQueueArray.begin(); it != DolerQueueArray.end(); ++it )
				{
					(*it)->DiscardAllMsgs();
				}
				; /// Do not discard the outboundMqtt queue.
				/// Reset the trigger.
				g_DoFlushQueues.store(false, std::memory_order_relaxed);
			}
		}
	}
}

///----------------------------------------------------------------------
/// Plans for future:
/// SummarizeHardwareStates
/// Sets EsState to a summary of hardware states. This means
/// EsState is Faulty if ANY hardware is faulty.
/// EsState is Mocked if ALL hardware is mocked.
/// EsState is Uninitialized if any hardware is Uninitialized.
/// else EsState is SettingsConfirmed if any hardware is SettingsConfirmed
/// else EsState is Homed1 if all of the following components are homed:
///		Z, ZZ, ZZZ, AnRH, all pumps, optical column focus
/// else EsState is Homed2 if all of the following components are homed:
///		XY, XX, YY, CxPM, optical column turret
/// EsState to Good if the previous conditions are not met.
///----------------------------------------------------------------------
std::mutex manageHardwareStateMutex;
ComponentState SummarizeHardwareStates()
{
	int numberOfActiveSafeToMoves = 0;
	int numberOfActiveUnsafeToMoves = 0;
	int numberOfHwStates = 0;
	unique_lock<mutex> lock(manageHardwareStateMutex);
//	cout << "main: SummarizeHardwareStates" << endl;
	ComponentState EsState = g_EsHwState.load( std::memory_order_acquire );
	std::array<unsigned int, (int) ComponentState::SizeOf> statesArray;
	statesArray.fill(0);
	
//	statesArray[(int) g_CxDState.load( std::memory_order_acquire )]++;

	ComponentState state = g_XYState.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveUnsafeToMoves++;
	numberOfHwStates++;

	state = g_XXState.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveUnsafeToMoves++;
	numberOfHwStates++;

	state = g_YYState.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveUnsafeToMoves++;
	numberOfHwStates++;

	state = g_ZState.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveSafeToMoves++;
	numberOfHwStates++;

	state = g_ZZState.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveSafeToMoves++;
	numberOfHwStates++;

	state = g_ZZZState.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveSafeToMoves++;
	numberOfHwStates++;

	state = g_AspirationPump1State.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveSafeToMoves++;
	numberOfHwStates++;

	state = g_DispensePump1State.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveSafeToMoves++;
	numberOfHwStates++;

	state = g_DispensePump2State.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveSafeToMoves++;
	numberOfHwStates++;

	state = g_DispensePump3State.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveSafeToMoves++;
	numberOfHwStates++;

	state = g_PickingPump1State.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveSafeToMoves++;
	numberOfHwStates++;

	state = g_WhiteLightSourceState.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveSafeToMoves++;
	numberOfHwStates++;

	state = g_FluorescenceLightFilterState.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveSafeToMoves++;
	numberOfHwStates++;

	state = g_AnnularRingHolderState.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveSafeToMoves++;
	numberOfHwStates++;

	state = g_CameraState.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveSafeToMoves++;
	numberOfHwStates++;

	state = g_OpticalColumnState.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked)
	{
		numberOfActiveSafeToMoves++;
		numberOfActiveUnsafeToMoves++;
	}
	numberOfHwStates++;

	state = g_IncubatorState.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveSafeToMoves++;
	numberOfHwStates++;

	state = g_CxPMState.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveUnsafeToMoves++;
	numberOfHwStates++;

	state = g_BarcodeReaderState.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked) numberOfActiveSafeToMoves++;
	numberOfHwStates++;
	
	if (statesArray[(int) ComponentState::Uninitialized] > 0)
	{
		EsState = ComponentState::Uninitialized;
		cout << "MAIN::SummarizeHardwareStates -- state is Uninitialized." << endl;
	}
	if ((EsState == ComponentState::Uninitialized) && (statesArray[(int) ComponentState::SettingsConfirmed] + statesArray[(int) ComponentState::Mocked] == numberOfHwStates))
	{
		EsState = ComponentState::SettingsConfirmed;
		cout << "MAIN::SummarizeHardwareStates -- state is SettingsConfirmed." << endl;
	}
	if ((EsState == ComponentState::SettingsConfirmed) && (statesArray[(int) ComponentState::Homed1] == numberOfActiveSafeToMoves))
	{
		EsState = ComponentState::Homed1;
		cout << "MAIN::SummarizeHardwareStates -- state is Homed1." << endl;
	}
	if ((EsState == ComponentState::Homed1) && (statesArray[(int) ComponentState::Homed2] == numberOfActiveUnsafeToMoves))
	{
		EsState = ComponentState::Homed2;
		cout << "MAIN::SummarizeHardwareStates -- state is Homed2." << endl;
	}
	if (statesArray[(int) ComponentState::Good] + statesArray[(int) ComponentState::Mocked] == numberOfHwStates)
	{
		if (statesArray[(int) ComponentState::Good] > 0)
		{
			EsState = ComponentState::Good;
//			LOG_TRACE( "main", "init", "state is Good" );
		}
		else
		{
			EsState = ComponentState::Mocked;
//			LOG_TRACE( "main", "init", "state is Mocked" );
		}
	}
	if (statesArray[(int) ComponentState::Faulty] > 0)
	{
		EsState = ComponentState::Faulty;
		cout << "MAIN::SummarizeHardwareStates -- state is Faulty." << endl;
	}
	// TODO: Any cases not covered?
	if ((EsState != ComponentState::Good) && (EsState != ComponentState::Mocked))
	{
		cout << "Good:" << statesArray[(int) ComponentState::Good] << ", Uninit:" << statesArray[(int) ComponentState::Uninitialized] << ", Mocked:" << statesArray[(int) ComponentState::Mocked] << ", SConf:" << statesArray[(int) ComponentState::SettingsConfirmed] << ", Homed1:" << statesArray[(int) ComponentState::Homed1]  << ", Homed2:" << statesArray[(int) ComponentState::Homed2] << ", Faulty:" << statesArray[(int) ComponentState::Faulty] << ", NumStates:" << numberOfHwStates << endl;
	}
	g_EsHwState.store( EsState, std::memory_order_relaxed );
	
	long numWorkMsgs = 0L;
	{
		unique_lock<mutex> lock( g_numWorkMsgsMutex );
		numWorkMsgs = g_numWorkMsgs;
	}
	if (numWorkMsgs > 5)
	{
		cout << "MAIN::SummarizeHardwareStates -- WARNING! Number of work messages is " << numWorkMsgs << endl;
	}

	return EsState;
}

/// A public function for time syncronization.
bool SynchronizeTime( unsigned long unixTime )
{
	struct tm ts; /// Identical to rtc_time structure?
	time_t time = (time_t) unixTime;
	ts = *localtime( &time );
	cout << "MAIN::SynchronizeTime -- New date/time = " << ts.tm_year << "," << ts.tm_mon << "," << ts.tm_mday << "," << ts.tm_hour << ":" << ts.tm_min << "," << ts.tm_sec << endl;
	int fileDescriptor;
	bool succeeded = false;
	/// If all of the doler threads are waiting on their queues, they aren't doing work
	/// that could be adversely affected by this clock change.
	if ( msgQueueForOptics->IsWaitingOnQueue() &&
		 /// We don't check msgQueueForInfo because we can safely assume taht it is in the middle of calling this function.
		 msgQueueForCxD->IsWaitingOnQueue() &&
		 msgQueueForCxPM->IsWaitingOnQueue() &&
		 msgQueueForPump->IsWaitingOnQueue() &&
		 msgQueueForRingHolder->IsWaitingOnQueue() )
	{
//		cout << "MAIN::SynchronizeTime -- entered safe zone" << endl;
		/// NOTE: I considered setting g_EsHoldState to HoldUp_Freeze during this block, but rejected
		/// the idea because this work is being done on the dolerForInfo thread, and changing the hold
		/// state would create unwelcome race conditions with main()'s inner wait loop. Let's just hope
		/// that any commands that come in while we are in this block are too late to be adversely
		/// affected by the clock change.

		/// Change the real-time-clock. (Best effort will have to do.)
		fileDescriptor = open("/dev/rtc", O_RDONLY);
		ioctl(fileDescriptor, RTC_SET_TIME, &ts);
		close(fileDescriptor);
		/// ... and change the system clock.
		const struct timeval tv = {mktime(&ts), 0};
		succeeded = (settimeofday(&tv, 0) == 0); /// Report on success.
		if (succeeded)
		{
//			cout << "MAIN::SynchronizeTime -- succeeded!" << endl;
		}
		else
		{
			cout << "MAIN::SynchronizeTime -- failed at settimeofday " << endl;
		}
	}
	else
	{
		cout << "MAIN() failed to enter safe zone." << endl;
	}
	return succeeded;
}

/// Read a configuration file that tells us which components are to be mocked. Arrange for
/// them to be mocked, by setting their global ComponentState to Mocked.
/// Set all other global ComponentStates to the given defaultState.
void ConfigureComponentsPerConfig( ComponentState defaultState )
{
	string filePath = CONFIG_DIR;
	filePath += "/";
	filePath += ES_PARTS_JSON;
	string fileContentsStr = "";
	
//	g_CxDState.store( defaultState, std::memory_order_relaxed );
	g_XYState.store( defaultState, std::memory_order_relaxed );
	g_XXState.store( defaultState, std::memory_order_relaxed );
	g_YYState.store( defaultState, std::memory_order_relaxed );
	g_ZState.store( defaultState, std::memory_order_relaxed );
	g_ZZState.store( defaultState, std::memory_order_relaxed );
	g_ZZZState.store( defaultState, std::memory_order_relaxed );
	g_AspirationPump1State.store( defaultState, std::memory_order_relaxed );
	g_DispensePump1State.store( defaultState, std::memory_order_relaxed );
	g_DispensePump2State.store( defaultState, std::memory_order_relaxed );
	g_DispensePump3State.store( defaultState, std::memory_order_relaxed );
	g_PickingPump1State.store( defaultState, std::memory_order_relaxed );
	g_WhiteLightSourceState.store( defaultState, std::memory_order_relaxed );
	g_FluorescenceLightFilterState.store( defaultState, std::memory_order_relaxed );
	g_AnnularRingHolderState.store( defaultState, std::memory_order_relaxed );
	g_CameraState.store( defaultState, std::memory_order_relaxed );
	g_OpticalColumnState.store( defaultState, std::memory_order_relaxed );
	g_IncubatorState.store( defaultState, std::memory_order_relaxed );
	g_CxPMState.store( defaultState, std::memory_order_relaxed );
	g_BarcodeReaderState.store( defaultState, std::memory_order_relaxed );

	if (FileExists( filePath )) {
		stringstream fileContents;
		fstream in( filePath.c_str(), std::ios::in );
		if (in.is_open())
		{
			fileContents << in.rdbuf();
		}
		in.close();
		fileContentsStr = fileContents.str();
		enum json_tokener_error jerr;
		json_object * fileContentsObj = json_tokener_parse_verbose( fileContentsStr.c_str(), &jerr );

		if (jerr == json_tokener_success)
		{
//			cout << "Main() parsed " << filePath << endl;
			if (json_object_get_type(fileContentsObj) == json_type_array)
			{
				size_t arrayLength = json_object_array_length( fileContentsObj );
				for (int i = 0; i < arrayLength; i++)
				{
					json_object * itemObj = json_object_array_get_idx( fileContentsObj, i );
					if (itemObj)
					{
						string componentName = json_object_get_string( json_object_object_get( itemObj, "component" ));

						bool toMock = false;
						json_object * mockObj = json_object_object_get( itemObj, "mock" );
						if (mockObj)
						{
							toMock = json_object_get_boolean( mockObj );
						}
						if (toMock && (defaultState != ComponentState::Mocked) )
						{
							if (componentName == ENUM_CXD)
							{
//								g_CxDState.store( ComponentState::Mocked, std::memory_order_relaxed );
								g_XYState.store( ComponentState::Mocked, std::memory_order_relaxed );
								g_XXState.store( ComponentState::Mocked, std::memory_order_relaxed );
								g_YYState.store( ComponentState::Mocked, std::memory_order_relaxed );
								g_ZState.store( ComponentState::Mocked, std::memory_order_relaxed );
								g_ZZState.store( ComponentState::Mocked, std::memory_order_relaxed );
								g_ZZZState.store( ComponentState::Mocked, std::memory_order_relaxed );
							}
							if (componentName == ENUM_XYAXES) g_XYState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_XXAXIS) g_XXState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_YYAXIS) g_YYState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_ZAXIS) g_ZState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_ZZAXIS) g_ZZState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_ZZZAXIS) g_ZZZState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_ASPIRATION_PUMP_1) g_AspirationPump1State.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_DISPENSE_PUMP_1) g_DispensePump1State.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_DISPENSE_PUMP_2) g_DispensePump2State.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_DISPENSE_PUMP_3) g_DispensePump3State.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_PICKING_PUMP_1) g_PickingPump1State.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_WHITE_LIGHT_SOURCE) g_WhiteLightSourceState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_FLUORESCENCE_LIGHT_FILTER) g_FluorescenceLightFilterState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_ANNULAR_RING_HOLDER) g_AnnularRingHolderState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_CAMERA) g_CameraState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_OPTICAL_COLUMN) g_OpticalColumnState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_INCUBATOR) g_IncubatorState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_CXPM) g_CxPMState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_BARCODE_READER) g_BarcodeReaderState.store( ComponentState::Mocked, std::memory_order_relaxed );
						}
						if (componentName == "mqtt_broker")
						{
							json_object * ipAddrObj = json_object_object_get( itemObj, "ip_address" );
							if (ipAddrObj)
							{
								m_BrokerIpAddressConfiguration = json_object_get_string( ipAddrObj );
								stringstream msg;
								msg << "BrokerIpAddress=" << m_BrokerIpAddressConfiguration;
								LOG_TRACE( "main", "init", msg.str().c_str() );
							}
						}
					}
				}
			}
		}
		else
		{
			cout << "Main() failed to parse " << filePath << endl;
		}
		json_object_put( fileContentsObj ); /// Clear allocated memory.
	}
	else
	{
		cout << "Main() didn't find " << filePath << endl;
	}
}


int main(int argc, char **argv)
{
	InitializeSundry();
	g_sequenceId = 0;
	g_XxCurrentOffset.store( 0L, std::memory_order_relaxed );
	g_XxDefaultLocationOffset.store( 0L, std::memory_order_relaxed );
	g_YyCurrentOffset.store( 0L, std::memory_order_relaxed );
	g_YyDefaultLocationOffset.store( 0L, std::memory_order_relaxed );
	g_EsHoldState.store( HoldType::NoHold_RunFree, std::memory_order_relaxed );
	g_PreviousHoldState.store( HoldType::NoHold_RunFree, std::memory_order_relaxed );
	g_DoFlushQueues.store( false, std::memory_order_relaxed );
	g_EsHwState.store( ComponentState::Uninitialized, std::memory_order_relaxed );
	g_CameraIsInitialized.store( false, std::memory_order_relaxed );
	ConfigureComponentsPerConfig( ComponentState::Uninitialized );
	{
		unique_lock<mutex> lock( g_numWorkMsgsMutex );
		g_numWorkMsgs = 0L;
	}

	// "喂!" doesn't print well.
	cout << " Wèi!" << endl;
	cout << " wes " << ES_FIRMWARE_VERSION << endl;

	/// Check the hardware for concurrency support.
	unsigned int hwcon = std::thread::hardware_concurrency();
//	cout << hwcon << " concurrent threads are supported." << endl;

	/// Make sure all needed files and directories are in place.
	/// These should fail when a directory already exists.
	int retval = mkdir( CPD_DIR, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
//	retval = mkdir( CONFIG_DIR, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ); // Now created (and populated) by the Yocto build.
	retval = mkdir( LOG_DIR, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
	retval = mkdir( PLATES_DIR, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
	retval = mkdir( TIPS_DIR, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
	
	/// Connect threads to queueus
	receivesFromMqtt.AttachCommandOutBox( jobSchedule );
	receivesFromMqtt.AttachAckOutBox( outboundMqtt );
	interpretsCommand.AttachInBox( jobSchedule );
	interpretsCommand.AttachOutBoxForOptics( msgQueueForOptics );
	interpretsCommand.AttachOutBoxForInfo( msgQueueForInfo );
	interpretsCommand.AttachOutBoxForCxD( msgQueueForCxD );
	interpretsCommand.AttachOutBoxForCxPM( msgQueueForCxPM );
	interpretsCommand.AttachOutBoxForPump( msgQueueForPump );
	interpretsCommand.AttachOutBoxForRingHolder( msgQueueForRingHolder );
	interpretsCommand.AttachStatusRouteBox( outboundMqtt );
	for (int i = 0; i < DolerThreadArray.size(); i++)
	{
		DolerThreadArray[i]->AttachInBox( DolerQueueArray[i] );
		DolerThreadArray[i]->AttachOutBox( outboundMqtt );
	}
	sendsToMqtt.AttachInBox( outboundMqtt );
	
	/// Set the static machineId.
	// TODO: Change this to read it from a file.
	uuid_t meGuid;
	uuid_generate_random( meGuid );
	char meGuidAsString[50] = {0};
	uuid_unparse_lower( meGuid, meGuidAsString );
	string machineId = meGuidAsString;
	
	/// Set the dynamic parts of MQTT configuration
	int count = 240; /// 4 minutes
	string brokerIpAddress = m_BrokerIpAddressConfiguration;
	if (brokerIpAddress == "localhost")
	{
		/// Keep poling until we detect the host's IP address.
		do {
			count--;
			this_thread::sleep_for(1s);
			brokerIpAddress = DetectHostIpAddress();
		} while ((brokerIpAddress == "") && (count > 0));
	}
	{
		stringstream msg;
		msg << "brokerIP_address=" << brokerIpAddress;
		LOG_DEBUG( "main", "main", msg.str().c_str() );
	}
	{
		stringstream msg;
		msg << "Count=" << (240-count) << " seconds waiting for IP address";
		LOG_DEBUG( "main", "main", msg.str().c_str() );
	}
	receivesFromMqtt.SetBrokerIpAddress( brokerIpAddress );
	sendsToMqtt.SetBrokerIpAddress( brokerIpAddress );
	receivesFromMqtt.SetMachineId(machineId);
	{
		stringstream msg;
		msg << "machineId=" << machineId;
		LOG_DEBUG( "main", "main", msg.str().c_str() );
	}
	interpretsCommand.SetMachineId( machineId );
	for ( auto it = DolerThreadArray.begin(); it != DolerThreadArray.end(); ++it )
	{
		(*it)->SetMachineId( machineId );
	}
	dolerForOptics.SetMachineId( machineId ); // TODO: This may be redundant. Confirm and delete.
	dolerForOptics.SetBrokerIpAddress( brokerIpAddress ); /// Do this before calling CreateThread().
	string masterId = ""; /// No master client at boot time.
	receivesFromMqtt.SetMasterId(masterId);
	
	/// Create and start worker threads
	receivesFromMqtt.CreateThread();
	interpretsCommand.CreateThread();
	for ( auto it = DolerThreadArray.begin(); it != DolerThreadArray.end(); ++it )
	{
		(*it)->CreateThread();
	}
	sendsToMqtt.CreateThread();

	/// Initialize the hardware.

	ComponentState esState = ComponentState::Uninitialized;
	ComponentState previousState = ComponentState::SizeOf; /// Initialize to invalid value.
	while ((esState != ComponentState::Good) && (esState != ComponentState::Mocked))
	{
		esState = SummarizeHardwareStates();
		if (esState == previousState)
		{
			/// Provide time for the Doler thread to do the previous work it was given.
			this_thread::sleep_for(500ms);
			cout << "Main(): Napping." << endl;
		}
		else
		{
			// TODO: Decide what to do when a component is Faulty. Other components should not initialize.
			// TODO: Some faults should put the ES on HOLD.
			this_thread::sleep_for(100ms);
			switch (esState)
			{
				case ComponentState::Uninitialized:
					cout << "Main(): esState=Uninitialized" << endl;
					interpretsCommand.InitializeHwComponents();
					break;
				case ComponentState::SettingsConfirmed:
					cout << "Main(): esState=SettingsConfirmed" << endl;
					interpretsCommand.HomeAllSafeHwComponents();
					break;
				case ComponentState::Homed1:
					cout << "Main(): esState=Homed1" << endl;
					interpretsCommand.HomeAllUnsafeHwComponents();
					break;
				case ComponentState::Homed2:
					cout << "Main(): esState=Homed2" << endl;
					interpretsCommand.RestoreAllHwComponentsToLastActiveStates();
					break;
				case ComponentState::Faulty:
					cout << "Main(): esState=Faulty. Wes is going into time-out." << endl;
					while (true)
					{
						this_thread::sleep_for(10s);
					}
					interpretsCommand.RestoreAllHwComponentsToLastActiveStates();
					break;
				default:
					break;
			}
		}
	}
	cout << "Main(): Done with initializations." << endl;

	while (true)
	{
		ManageHoldStates();
		esState = SummarizeHardwareStates();
		this_thread::sleep_for(1s);
	}

	receivesFromMqtt.ExitThread();
	interpretsCommand.ExitThread();
	for ( auto it = DolerThreadArray.begin(); it != DolerThreadArray.end(); ++it )
	{
		(*it)->ExitThread();
	}
	sendsToMqtt.ExitThread();

	return 0;
}
