#include <iostream>
#include <thread>
#include <array>
#include <mutex>
#include "main.hpp"
#include "CommandInterpreter.hpp"
#include "CommandDoler.hpp"
#include "Components.hpp"
#include "MqttHardwareIO.hpp"
#include "Fault.hpp"
#include "Sundry.hpp"
#include "sys/types.h"
#include "sys/stat.h"
#include "es_des.h"
#include <linux/rtc.h>
#include <time.h>
#include <sys/ioctl.h>
#include <array>

using namespace std;



int main(int argc, char **argv)
{
	cout << " Hello microscope!" << endl;


	/// Set the static machineId.
	// TODO: Change this to read it from a file.
	uuid_t meGuid;
	char meGuidAsString[50] = {0};
	uuid_generate_random( meGuid );
	uuid_unparse_lower( meGuid, meGuidAsString );
	string machineId = meGuidAsString;
	uuid_generate_random( meGuid );
	uuid_unparse_lower( meGuid, meGuidAsString );
	string sessionId = meGuidAsString;

	cerr << "Main is setting machineId " << machineId << endl;
//	interpretsCommand.SetMachineId( machineId );
//	dolerForOptics.SetMachineId( machineId );
	

	/// Set the dynamic parts of MQTT configuration
	int count = 240; /// 4 minutes
	string brokerIpAddress = "";
	while ((brokerIpAddress == "") && (count > 0))
	{
		count--;
		this_thread::sleep_for(1s);
		brokerIpAddress = DetectHostIpAddress();
	}
    cout << "Ip address = " << brokerIpAddress << endl;
    cout << "Count = " << count << endl;

	
	MqttHardwareIO mqttHardwareIO( brokerIpAddress );
	mqttHardwareIO.InitializeListOfTopics( ComponentType::OpticalColumn );
	
	bool connected = mqttHardwareIO.ConnectToBroker();
	string isConnected = connected ? "is connected" : "not connected";
	cout << isConnected << endl;
	
	isConnected = mqttHardwareIO.IsConnectedToBroker();
	isConnected = connected ? "is connected" : "not connected";
	cout << isConnected << endl;
	
	bool ackReceived = false;
	int ack = 0;
	json_object * jpayloadObj = nullptr;
	string messageType = "set_optical_column_turret_position";
	string desCallMessage = "{ \"message_type\": \"set_optical_column_turret_position\", \"transport_version\": \"1.0\", \"command_group_id\": 234, \"command_id\": 1234, \"session_id\": \"";
	desCallMessage += sessionId;
	desCallMessage +=  "\", \"machine_id\": \"";
	desCallMessage += machineId;
	desCallMessage += "\", \"payload\": { \"turret_position\": 1 } }";
	string topic = "tbd/microscope/command";
	int statusCode = mqttHardwareIO.CommandTheHardwareClient( messageType, desCallMessage, topic, ackReceived, ack, &jpayloadObj );

	cout << "ack = " << ack << endl;
	cout << "status_code = " << statusCode << endl;
	return 0;
}
