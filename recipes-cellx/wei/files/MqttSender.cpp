#include <iostream>
#include <cassert>
#include <chrono> // for measuring photo transfers
#include "es_des.h"
#include "MqttSender.hpp"
#include "Fault.hpp"
#include "Sundry.hpp"
#include "json.h"
#include "base64.h"

using namespace std;

//----------------------------------------------------------------------
// MqttSenderThread
//----------------------------------------------------------------------
MqttSenderThread::MqttSenderThread(const char* threadName) : m_thread(nullptr),
	m_threadName(threadName),
    m_brokerIpAddress("0.0.0.0"),
    m_connectedToBroker(false),
   	m_continueRunning(false)
{
	m_inBox = nullptr;
    m_client;
    m_conn_opts = MQTTClient_connectOptions_initializer;
    m_pubmsg = MQTTClient_message_initializer;
    m_token;
    m_mqttReturnCode;
    m_desHelper;
    m_address;
   	m_payloadBuffer = nullptr;

}

//----------------------------------------------------------------------
// ~MqttSenderThread
//----------------------------------------------------------------------
MqttSenderThread::~MqttSenderThread()
{
	m_inBox = nullptr;
	delete[] m_payloadBuffer;
}

//----------------------------------------------------------------------
// CreateThread
//----------------------------------------------------------------------
bool MqttSenderThread::CreateThread()
{
	bool success = false;
	if (!m_thread)
	{
		m_continueRunning = true;
		m_thread = std::unique_ptr<std::thread>(new thread(&MqttSenderThread::Process, this));
		success = true;
		cout << "MqttSender created!" << endl;
	}
	return success;
}

//----------------------------------------------------------------------
// ExitThread
//----------------------------------------------------------------------
void MqttSenderThread::ExitThread()
{
	m_continueRunning = false;
	/// Send a small message to the inbox, just to break out of listening long enough to exit.
	std::shared_ptr<WorkMsg> ender = make_shared<WorkMsg>( "", "", "", "end", "", true );
	m_inBox->PushMsg( ender );

	if (!m_thread)
	{
		cout << "MqttReceiver exited poorly" << endl;
		return;
	}

	int loopCount = 0;
	while ((m_inBox) && (loopCount++ > 100))
	{
		cout << "MqttSender... " << loopCount << endl;
		usleep(10000);
	}

    m_thread->join();
    m_thread = nullptr;
	cout << "MqttSender exited OK after " << loopCount << " loops." << endl;
}

//----------------------------------------------------------------------
// attachInBox
//----------------------------------------------------------------------
void MqttSenderThread::AttachInBox(MsgQueue *q)
{
	assert((!m_inBox) && "AttachInBox called second time?");
    m_inBox = q;
}

///----------------------------------------------------------------------
/// SetBrokerIpAddress
///----------------------------------------------------------------------
void MqttSenderThread::SetBrokerIpAddress(std::string ipAddress)
{
	m_brokerIpAddress = ipAddress;
}


///----------------------------------------------------------------------


volatile MQTTClient_deliveryToken deliveredtoken;

// TODO: Implement a context like MqttReceiver's so these callbacks have access to more info.
void delivered(void *context, MQTTClient_deliveryToken dt)
{
    cout << "MqttSender::delivered(): Message with token value " << dt << " delivery confirmed" << endl;;
    deliveredtoken = dt;
}

int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
    cout << "MqttSender::msgarrvd(): Message arrived" << endl;
    return 1;
}

void connlost(void *context, char *cause)
{
    cout << "MqttSender::connlost(): Connection lost! Cause=" << cause << "!" << endl;
}


//----------------------------------------------------------------------
// Process
//----------------------------------------------------------------------
void MqttSenderThread::Process()
{
//    MQTTClient client;
//    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
//    MQTTClient_message pubmsg = MQTTClient_message_initializer;
//    MQTTClient_deliveryToken token;
//    int mqttReturnCode;
//    DesHelper desHelper;

//    stringstream address;
    m_address <<  "tcp://" << m_brokerIpAddress << ":" << SENDER_BROKER_PORT;
    MQTTClient_create(
		&m_client,
		m_address.str().c_str(),
		SENDER_CLIENTID,
        MQTTCLIENT_PERSISTENCE_NONE,
        NULL);
    m_conn_opts.keepAliveInterval = SENDER_KEEP_ALIVE_INTERVAL;
    m_conn_opts.cleansession = SENDER_CLEAN_SESSION;
    string statusChannel = TOPIC_ES;
    statusChannel = statusChannel + CHANNEL_STATUS;


	MQTTClient_setCallbacks(m_client, NULL, connlost, msgarrvd, delivered);
			
	while (m_continueRunning && m_inBox)
	{
		if (m_continueRunning && m_connectedToBroker)
		{
			std::shared_ptr<WorkMsg> call = m_inBox->WaitAndPopMsg();
			if (false) // Temporarily disabled.
			{
				stringstream msg;
				msg << "Received: " << call->msgType;
				LOG_TRACE( m_threadName, "mqtt", msg.str().c_str() );
			}

			/// See ExitThread().
			if (call->msgType == "end") break;

			char* callPayloadCopy = nullptr;
			m_pubmsg.qos = SENDER_QOS;
			m_pubmsg.retained = 0;
			bool printTime = false;

			if ((call->msgType == COLLECT_MONTAGE_IMAGES) && (call->destination == statusChannel))
			{
				printTime = true;
				bool succeeded = PackMontagePictureIntoPayload( call->payload, call->blob, call->blobSize );
				if (!succeeded)
				{
					LOG_ERROR( m_threadName, "mqtt", "PackMontagePicture failed" );
				}
			}
			else if ((call->msgType == GET_CAMERA_IMAGE) && (call->destination == statusChannel))
			{
				bool succeeded = PackSinglePictureIntoPayload( call->payload, call->blob, call->blobSize );
				if (!succeeded)
				{
					LOG_ERROR( m_threadName, "mqtt", "PackSinglePicture failed" );
				}
			}
			else if ((call->msgType == GET_AUTO_FOCUS_IMAGE_STACK) && (call->destination == statusChannel))
			{
				bool succeeded = PackSinglePictureIntoPayload( call->payload, call->blob, call->blobSize );
				if (!succeeded)
				{
					LOG_ERROR( m_threadName, "mqtt", "PackAutoFocusPicture failed" );
				}
			}
			else
			{
				/// Handle generic case. Copy the payload from the call WorkMsg to the pubmsg.
				m_payloadBuffer = new char[call->payload.size() + 1];
				strcpy(m_payloadBuffer, call->payload.c_str());
				m_pubmsg.payload = (void *) m_payloadBuffer;
				m_pubmsg.payloadlen = (int)call->payload.size();
				if ((call->msgType == GET_FIRMWARE_VERSION_NUMBER) && (call->destination == statusChannel))
				{
					LOG_INFO( m_threadName, "mqtt", "GetFirmwareVersionNumber called" );
				}
			}
			if (false) // Temporarily disabled.
			{
				cout << m_threadName << ": outgoing payload is " << m_payloadBuffer << endl;
				cout << m_threadName << ": Publishing to topic " << call->destination << endl;
				cout << m_threadName << ": Publishing from topic " << call->origin << endl;
				cout << m_threadName << ": Message Type " << call->msgType << endl;
				cout << m_threadName << ": Sub-message Type " << call->subMsgType << endl;
			}

			/// Send to the MQTT broker that which was received from the inter-thread inBox
			MQTTClient_publishMessage(m_client, call->destination.c_str(), &m_pubmsg, &m_token);
			while (deliveredtoken != m_token) { }
			m_mqttReturnCode = MQTTClient_waitForCompletion(m_client, m_token, SENDER_TIMEOUT);
//			cout << m_threadName << ": PMessage with delivery token " << m_token << " delivered" << endl;

			delete[] m_payloadBuffer; /// clears memory allocated in the handlers for all message types.
			if (false)
			{
				stringstream msg;
				msg << "~WorkMsg()#blobSize=" << call->blobSize << "#blob=" << reinterpret_cast<void *>(call->blob) << endl;
				LOG_DEBUG( m_threadName, "-", msg.str().c_str() );
			}
			free( call->blob );
			call->blobSize = 0L;
			call.reset();
			call = nullptr;
			
			if (printTime)
			{
				/* Used for measuring duration of work. Delete this when it is no longer needed.
				auto time = std::chrono::system_clock::now();
				auto since_epoch = time.time_since_epoch();
				auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(since_epoch);
				long now = millis.count();
				cout << "BPACK-shipd " << now << endl;
				*/
			}
	    }
	    else
	    {
			/// Attempt to connect to the broker.
			if ((m_mqttReturnCode = MQTTClient_connect(m_client, &m_conn_opts)) == MQTTCLIENT_SUCCESS)
			{
				m_connectedToBroker = true;
//				cout << m_threadName << ": Connection succeeded; return code is " << m_mqttReturnCode << endl;
			}
			else
			{
				m_connectedToBroker = false;
				cout << m_threadName << ": Failed to connect, return code " << m_mqttReturnCode << endl;
				std::this_thread::sleep_for( 250ms );
			}
		}
    }
    
	cout << "MqttSender disconnecting..." << endl;
    /// THE END. Clean up.
    MQTTClient_disconnect( m_client, SENDER_TIMEOUT );
    MQTTClient_destroy( &m_client );
    m_inBox = nullptr;
	cout << "MqttSender disconnected cleanly" << endl;
}

bool MqttSenderThread::PackSinglePictureIntoPayload( const string & callPayload, const unsigned char * blob, const unsigned long blobSize )
{
	bool succeeded = false;
	/// Extract binary picture from workMsg.
	
	/// Compress the binary picture.
	
	/// Base64 encode the image
	std::string encodedPic = base64_encode( blob, blobSize);

	/// Send it out in a status message.
	bool messageParsed = false;
	json_object *jobj = m_desHelper.ParseJsonDesMessage( callPayload.c_str(), &messageParsed );
	if (messageParsed)
	{
		/// Create a JSON object { "image": "Base64-string..." }
		json_object * imageObj = json_object_new_object();
		int result = json_object_object_add( imageObj, IMAGE, json_object_new_string( encodedPic.c_str() ) );
		LETS_ASSERT(result == 0);

		/// Embed imageObj in the original "get_camera_image" command message as { (other fields...), "payload": { "image": "Base64-string..." } }
		/// This will replace any existing "payload" field.
		result = json_object_object_add( jobj, PAYLOAD, imageObj );
		LETS_ASSERT(result == 0);

		/// Covert from JSON object form into string form.
		const char * objAsString = json_object_to_json_string( jobj );
		size_t bufferSize = strlen( objAsString );
		m_payloadBuffer = new char[bufferSize + 1];
		strcpy(m_payloadBuffer, objAsString);
		m_pubmsg.payload = (void *) m_payloadBuffer;
		m_pubmsg.payloadlen = strlen( m_payloadBuffer );
		
		/// Free memory
		int freed = json_object_put( jobj );
		if (freed == 1)
		{
			succeeded = true;
		}
		else
		{
			succeeded = false;
			LOG_ERROR( m_threadName, "mem", "Failed to free jobj memory" );
		}
	}
	return succeeded;
}

bool MqttSenderThread::PackMontagePictureIntoPayload( const string & callPayload, const unsigned char * blob, const unsigned long blobSize )
{
	bool succeeded = false;
	cout << m_threadName << ": PackMontagePictureIntoPayload called" << endl;
	/// Extract binary picture from workMsg.
	
	/// Compress the binary picture.
	/* Used for measuring duration of work. Delete this when it is no longer needed.
	auto time = std::chrono::system_clock::now();
	auto since_epoch = time.time_since_epoch();
	auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(since_epoch);
	long now = millis.count();
	cout << "BPACK-start " << now << endl;
	*/
	
	/// Base64 encode the image
	std::string encodedPic = base64_encode( blob, blobSize);

	/* Used for measuring duration of work. Delete this when it is no longer needed.
	time = std::chrono::system_clock::now();
	since_epoch = time.time_since_epoch();
	millis = std::chrono::duration_cast<std::chrono::milliseconds>(since_epoch);
	now = millis.count();
	cout << "BPACK-converted " << now << endl;
	cout << "BPACK-bytes=" << encodedPic.length() << endl;
	*/

	/// Send it out in a status message.
	bool messageParsed = false;
	json_object *jobj = m_desHelper.ParseJsonDesMessage( callPayload.c_str(), &messageParsed );
	if (messageParsed)
	{
//		cout << m_threadName << ": Message parsed OK." << endl;
		/// We expect the incoming message to have a payload of { "x_index": X, "y_index": Y }
		json_object * payloadObj = json_object_object_get( jobj, PAYLOAD );
		if (payloadObj)
		{
//			cout << m_threadName << ": Payload found." << endl;
			/// Add this pair to the payload: "image": "Base64-string..." 
			int result = json_object_object_add( payloadObj, IMAGE, json_object_new_string( encodedPic.c_str() ) );
			LETS_ASSERT(result == 0);

			/// Covert from JSON object form into string form.
			const char * objAsString = json_object_to_json_string( jobj );
			size_t bufferSize = strlen( objAsString );
			m_payloadBuffer = new char[bufferSize + 1];
			strcpy(m_payloadBuffer, objAsString);
			m_pubmsg.payload = (void *) m_payloadBuffer;
			m_pubmsg.payloadlen = strlen( m_payloadBuffer );
			succeeded = true;
		}
		/// Free memory
		int freed = json_object_put( jobj );
		if (freed == 1)
		{
			succeeded = true;
		}
		else
		{
			succeeded = false;
			LOG_ERROR( m_threadName, "mem", "Failed to free jobj memory" );
		}
	}
	return succeeded;
}

