#ifndef _ES_CAMERA_RETIGA_HPP
#define _ES_CAMERA_RETIGA_HPP

#include <string>
#include <vector>

#include "PVCamera.hpp"
#include "es_camera.hpp"

class Camera_Retiga : public ES_CameraBase
{
public:
	Camera_Retiga();
	~Camera_Retiga();

	/// To be called at start of runtime to initialize drivers supplied by camera manufacturers.
	static int InitDriver( std::string &errorMsg );
	/// Get a version number.
	static int GetVersion( std::string &msg );

	/// Adds Camera_Retiga objects to cameraList and returns the number of cameras added.
	static int GetCameras( std::vector<ES_CameraBase *> &cameraList, std::string &msg );

	/// To be called at end of runtime to tear down drivers supplied by camera manufacturers.
	static bool TerminateDriver( std::vector<ES_CameraBase *> &cameraList, std::string &msg );

	/// Get a picture
	virtual bool GetFrame (unsigned short *bufferOut, std::string &msgOut);
	/// Get a string describing the most recent error.
	virtual int GetErrorState(std::string &msg);

	virtual unsigned short GetFrameWidth() { return m_pvCamera.GetFrameWidth(); }
	virtual unsigned short GetFrameHeight() { return m_pvCamera.GetFrameHeight(); }
	virtual unsigned int GetExposureTime() { return m_pvCamera.GetExposureTime(); }
	virtual unsigned int GetGain() { return m_pvCamera.GetGain(); }
	virtual unsigned int GetBinningX() { return m_pvCamera.GetBinningX(); }
	virtual unsigned int GetBinningY() { return m_pvCamera.GetBinningY(); }

	virtual void SetFrameWidth(const unsigned int &width);
	virtual void SetFrameHeight(const unsigned int &height);
	virtual void SetExposureTime(const unsigned long &expTime_us);
	virtual void SetGain (std::string &msg, const int &gainId);
	virtual void SetBinFactor (const int &binX, const int &binY);

	void Initialize (std::string &msg, char *camName, const short &camId);
	virtual void Release (std::string &msg);
	virtual size_t GetFrameSize( std::string &msg );

protected:
	PVCamera m_pvCamera;
};

#endif 
