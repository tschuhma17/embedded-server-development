#include <iostream>
#include <string.h>
#include "enums.hpp"
#include "es_des.h"

using namespace std;

///----------------------------------------------------------------------
/// ComponentType
///----------------------------------------------------------------------
ComponentType StringNameToComponentTypeEnum( const char * name )
{
	if (strcmp( name, ENUM_COMPONENT_UNKNOWN ) == 0) return ComponentType::ComponentUnknown;
	if (strcmp( name, ENUM_COMPONENT_NONE ) == 0) return ComponentType::ComponentNone;
	if (strcmp( name, ENUM_CXD ) == 0) return ComponentType::CxD;
	if (strcmp( name, ENUM_XYAXES ) == 0) return ComponentType::XYAxes;
	if (strcmp( name, ENUM_XXAXIS ) == 0) return ComponentType::XXAxis;
	if (strcmp( name, ENUM_YYAXIS ) == 0) return ComponentType::YYAxis;
	if (strcmp( name, ENUM_ZAXIS ) == 0) return ComponentType::ZAxis;
	if (strcmp( name, ENUM_ZZAXIS ) == 0) return ComponentType::ZZAxis;
	if (strcmp( name, ENUM_ZZZAXIS ) == 0) return ComponentType::ZZZAxis;
	if (strcmp( name, ENUM_ASPIRATION_PUMP_1 ) == 0) return ComponentType::AspirationPump1;
	if (strcmp( name, ENUM_DISPENSE_PUMP_1 ) == 0) return ComponentType::DispensePump1;
	if (strcmp( name, ENUM_DISPENSE_PUMP_2 ) == 0) return ComponentType::DispensePump2;
	if (strcmp( name, ENUM_DISPENSE_PUMP_3 ) == 0) return ComponentType::DispensePump3;
	if (strcmp( name, ENUM_PICKING_PUMP_1 ) == 0) return ComponentType::PickingPump1;
	if (strcmp( name, ENUM_WHITE_LIGHT_SOURCE ) == 0) return ComponentType::WhiteLightSource;
	if (strcmp( name, ENUM_FLUORESCENCE_LIGHT_FILTER ) == 0) return ComponentType::FluorescenceLightFilter;
	if (strcmp( name, ENUM_ANNULAR_RING_HOLDER ) == 0) return ComponentType::AnnularRingHolder;
	if (strcmp( name, ENUM_CAMERA ) == 0) return ComponentType::Camera;
	if (strcmp( name, ENUM_OPTICAL_COLUMN ) == 0) return ComponentType::OpticalColumn;
	if (strcmp( name, ENUM_INCUBATOR ) == 0) return ComponentType::Incubator;
	if (strcmp( name, ENUM_CXPM ) == 0) return ComponentType::CxPM;
	if (strcmp( name, ENUM_BARCODE_READER ) == 0) return ComponentType::BarcodeReader;
	cout << "IdentifyComponentType returning Unknown." << endl;
	return ComponentType::ComponentUnknown;
}

const char*  ComponentTypeEnumToStringName( ComponentType type )
{
	switch( type )
	{
		case ComponentType::ComponentUnknown :
			return ENUM_COMPONENT_UNKNOWN;
			break;
		case ComponentType::ComponentNone:
			return ENUM_COMPONENT_NONE;
			break;
		case ComponentType::CxD :
			return ENUM_CXD;
			break;
		case ComponentType::XYAxes :
			return ENUM_XYAXES;
			break;
		case ComponentType::XXAxis :
			return ENUM_XXAXIS;
			break;
		case ComponentType::YYAxis :
			return ENUM_YYAXIS;
			break;
		case ComponentType::ZAxis :
			return ENUM_ZAXIS;
			break;
		case ComponentType::ZZAxis :
			return ENUM_ZZAXIS;
			break;
		case ComponentType::ZZZAxis :
			return ENUM_ZZZAXIS;
			break;
		case ComponentType::AspirationPump1 :
			return ENUM_ASPIRATION_PUMP_1;
			break;
		case ComponentType::DispensePump1 :
			return ENUM_DISPENSE_PUMP_1;
			break;
		case ComponentType::DispensePump2 :
			return ENUM_DISPENSE_PUMP_2;
			break;
		case ComponentType::DispensePump3 :
			return ENUM_DISPENSE_PUMP_3;
			break;
		case ComponentType::PickingPump1 :
			return ENUM_PICKING_PUMP_1;
			break;
		case ComponentType::WhiteLightSource :
			return ENUM_WHITE_LIGHT_SOURCE;
			break;
		case ComponentType::FluorescenceLightFilter :
			return ENUM_FLUORESCENCE_LIGHT_FILTER;
			break;
		case ComponentType::AnnularRingHolder :
			return ENUM_ANNULAR_RING_HOLDER;
			break;
		case ComponentType::Camera :
			return ENUM_CAMERA;
			break;
		case ComponentType::OpticalColumn :
			return ENUM_OPTICAL_COLUMN;
			break;
		case ComponentType::Incubator :
			return ENUM_INCUBATOR;
			break;
		case ComponentType::CxPM :
			return ENUM_CXPM;
			break;
		case ComponentType::BarcodeReader :
			return ENUM_BARCODE_READER;
			break;
	}
	return ENUM_UNKNOWN;
}

std::ostream& operator << (std::ostream& os, const ComponentType& e)
{
	std::ostringstream ss;
	char eAsStr[3] = {0};
	ss << std::setw(2) << std::setfill('0') << ((int) e);
	return os << ss.str();
}
std::istream& operator >> (std::istream& is, ComponentType& e)
{
	char eAsStr[3] = {0};
	is.read(eAsStr, 2);
	e = (ComponentType) std::stoi(eAsStr, nullptr, 10);
//	std::cout << "ComponentType=" << e << std::endl;
	return is;
}

///----------------------------------------------------------------------
/// EditableType
///----------------------------------------------------------------------
const char* EditableTypeEnumToStringName( EditableType type )
{
	switch( type )
	{
		case EditableType::EditableFixed :
			return ENUM_EDITABLE_FIXED;
			break;
		case EditableType::EditableCPDOnly :
			return ENUM_EDITABLE_CPD_ONLY;
			break;
		case EditableType::EditableApplicationOnly :
			return ENUM_EDITABLE_APP_ONLY;
			break;
		case EditableType::EditableCPD_Application :
			return ENUM_EDITABLE_CPD_APP;
			break;
	}
	return ENUM_UNKNOWN;
}

EditableType StringNameToEditableTypeEnum( const char * name )
{
	if (name == ENUM_EDITABLE_FIXED) return EditableType::EditableFixed;
	if (name == ENUM_EDITABLE_CPD_ONLY) return EditableType::EditableCPDOnly;
	if (name == ENUM_EDITABLE_APP_ONLY) return EditableType::EditableApplicationOnly;
	if (name == ENUM_EDITABLE_CPD_APP) return EditableType::EditableCPD_Application;
	return EditableType::Unknown;
}

///----------------------------------------------------------------------
/// PersistType
///----------------------------------------------------------------------
const char* PersistTypeEnumToStringName( PersistType type )
{
	switch( type )
	{
		case PersistType::NotPersist :
			return ENUM_NOT_PERSIST;
			break;
		case PersistType::Persist :
			return ENUM_PERSIST;
			break;
		case PersistType::HardPersist :
			return ENUM_HARD_PERSIST;
			break;
		case PersistType::Remove :
			return ENUM_REMOVE;
			break;
	}
	return ENUM_UNKNOWN;
}
PersistType StringNameToPersistTypeEnum( const char * name )
{
	if (name == ENUM_NOT_PERSIST) return PersistType::NotPersist;
	if (name == ENUM_PERSIST) return PersistType::Persist;
	if (name == ENUM_HARD_PERSIST) return PersistType::HardPersist;
	if (name == ENUM_REMOVE) return PersistType::Remove;
	return PersistType::Unknown;
}

///----------------------------------------------------------------------
/// PersistType
///----------------------------------------------------------------------
const char* PumpTypeEnumToStringName( PumpType type )
{
	switch( type )
	{
		case PumpType::Peristaltic :
			return ENUM_PERISTALTIC;
			break;
		case PumpType::Syringe :
			return ENUM_SYRINGE;
			break;
	}
	return ENUM_UNKNOWN;
}

PumpType StringNameToPumpTypeEnum( const char * name )
{
	if (name == ENUM_PERISTALTIC) return PumpType::Peristaltic;
	if (name == ENUM_SYRINGE) return PumpType::Syringe;
	return PumpType::Unknown;
}


///----------------------------------------------------------------------
/// More generic
///----------------------------------------------------------------------
template<typename T>
std::ostream& operator << (typename std::enable_if<std::is_enum<T>::value, std::ostream>::type& stream, const T& e)
{
	return stream << static_cast<typename std::underlying_type<T>::type>(e);
}
template<typename T>
std::istream& operator >> (typename std::enable_if<std::is_enum<T>::value, std::istream>::type& stream, T& e)
{
	int i;
	stream >> i;
	e = (T)i;
	return stream;
}

HardwareType StringNameToHardwareTypeEnum( const char * name )
{
	if (strcmp( name, "ACR74T" ) == 0) return HardwareType::ACR74T;
	if (strcmp( name, "ACR74V" ) == 0) return HardwareType::ACR74V;
	if (strcmp( name, "CAVRO_ADP" ) == 0) return HardwareType::CAVRO_ADP;
	if (strcmp( name, "IX85" ) == 0) return HardwareType::IX85;
	if (strcmp( name, "LS620" ) == 0) return HardwareType::LS620;
	if (strcmp( name, "R1_R2_R3" ) == 0) return HardwareType::R1_R2_R3;
	if (strcmp( name, "daA3840-45um" ) == 0) return HardwareType::daA3840_45um;
	if (strcmp( name, "MATRIX_120" ) == 0) return HardwareType::MATRIX_120;
	return HardwareType::unknown;
}

const char*  HardwareTypeEnumToStringName( HardwareType type )
{
	switch( type )
	{
		case HardwareType::ACR74T :
			return "ACR74T";
			break;
		case HardwareType::ACR74V:
			return "ACR74V";
			break;
		case HardwareType::CAVRO_ADP :
			return "CAVRO_ADP";
			break;
		case HardwareType::IX85 :
			return "IX85";
			break;
		case HardwareType::LS620 :
			return "LS620";
			break;
		case HardwareType::R1_R2_R3 :
			return "R1_R2_R3";
			break;
		case HardwareType::daA3840_45um :
			return "daA3840-45um";
			break;
		case HardwareType::MATRIX_120 :
			return "MATRIX_120";
			break;
	}
	return "unknown";
}

SpeedType StringNameToMontageSpeedTypeEnum( const char * name )
{
	if (strcmp( name, "enumMontageSpeedUnknown" ) == 0) return SpeedType::Speed_undefined;
	if (strcmp( name, "enumMontageSpeedSlow" ) == 0) return SpeedType::Speed_slow;
	if (strcmp( name, "enumMontageSpeedMedium" ) == 0) return SpeedType::Speed_medium;
	if (strcmp( name, "enumMontageSpeedFast" ) == 0) return SpeedType::Speed_fast;
	return SpeedType::Speed_undefined;
}

const char* SpeedTypeEnumToMontageStringName( SpeedType type )
{
	switch( type )
	{
		case SpeedType::Speed_undefined :
			return "enumMontageSpeedUnknown";
			break;
		case SpeedType::Speed_slow:
			return "enumMontageSpeedSlow";
			break;
		case SpeedType::Speed_medium :
			return "enumMontageSpeedMedium";
			break;
		case SpeedType::Speed_fast :
			return "enumMontageSpeedFast";
			break;
	}
	return "enumMontageSpeedUnknown";
}
