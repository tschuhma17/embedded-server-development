///----------------------------------------------------------------------
/// Tiptype.hpp
/// Defines data structures used for handling tips.
///----------------------------------------------------------------------
#ifndef _TIP_TYPE_H
#define _TIP_TYPE_H

#include "json.h"
#include "es_des.h"
#include "Sundry.hpp"

///----------------------------------------------------------------------
/// TipType
/// Used for the following commands:
/// GetTip
/// MoveTipToWasteLocation3
/// MoveTipToWasteLocation2
/// MoveTipToWasteLocation1
/// SeatTip
/// GetTipSkew
/// RemoveTip
/// MoveToZAxisForceNewton
/// MoveToZAxisSeatingForceNewton
/// MoveToZZAxisForceNewton
/// MoveToZZAxisSeatingForceNewton
/// MoveToZTipPickForceNewton
/// MoveToZTipSeatingForceNewton
/// MoveToZTipWellBottomForceNewton,
/// MoveToZZTipPickForceNewton
/// MoveToZZTipSeatingForceNewton
/// MoveToZZTipWellBottomForceNewton
///----------------------------------------------------------------------
class TipType
{
public:
    /// Constructor
	TipType();
	~TipType();

	int tipTotalLength;
	int tipWorkingLength;
	int tipShoulderLength;
	int tipInnerDiameterAtInsertionEnd;
	int tipInnerDiameterAtWorkingEnd;
	int tipMaximumWorkingVolume;
	int tipRackRowCount;
	int tipRackColumnCount;
	int tipRackLength;
	int tipRackWidth;
	int tipRackHeight;
	int tipRackSeatToTipSeat;
	int tipRackSeatToTopOfTipRack;
	int tipRackSeatToTopOfTip;
	int firstTipX;
	int firstTipY;
	int interTipX;
	int interTipY;
	int usedTipCount;

	/// Method for Populating PlateType from a json_c object.
	bool PopulateFromJsonCObject( const char * threadName, json_object* jsonInObj );
	bool PopulateFromJsonCObject( json_object* jsonInObj );
	int GetInt( const char * threadName, bool * succeeded, json_object* jsonInObj, const char* name );
	int GetInt( bool * succeeded, json_object* jsonInObj, const char* name );
}; 

#endif 
