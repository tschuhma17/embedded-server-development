/* Transport.hpp */

#ifndef __Transport_hpp__
#define __Transport_hpp__

#include <vector>

#include "MotionAxis.hpp"

namespace USAFW {

    class MotionController;

    class Transport {
    public:
        // Constructor, single-axis transport.
        Transport(MotionController &motionControllerP, const std::string nameP, std::vector<MotionAxis *> *axesP);

        // Destructor
        virtual ~Transport();


        // == QUERIES ==

        // == COMMANDS ==

    protected:
        // Drops all axes from the transport.  Called by the destructor.
        virtual void disassemble(void);

        MotionController            &m_controller;

        const std::string           m_name;

        // Axes which are members of the transport.  This is an ordered vector.
        // This vector is constructed by the parser, but owned by this object.
        std::vector<MotionAxis*>    *p_axes;
    };

}

#endif
