///----------------------------------------------------------------------
/// ShellInvoker provides functions for making discrete calls to the
/// shell.
/// Written support calls to Python scripts, but other types of calls
/// can be supported some day.
///----------------------------------------------------------------------
#ifndef _SHELL_INVOKER_H
#define _SHELL_INVOKER_H

#include <cstdio> // for popen
#include <stdio.h>
#include <errno.h> // for popen
#include <string>
#include <cstring>
#include "Sundry.hpp"

/// These commands are handled by popen_hand.py script. Keep them in sync with popen_hand.py.
#define NO_OP				"nop"
#define RED_ON				"red_on"
#define GREEN_ON			"green_on"
#define BLUE_ON				"blue_on"
#define BRIGHT_FIELD_ON		"bf_on"
#define RED_OFF				"red_off"
#define GREEN_OFF			"green_off"
#define BLUE_OFF			"blue_off"
#define BRIGHT_FIELD_OFF	"bf_off"
#define HOME_Z				"home_z"
#define MOVE_Z_ABS			"move_z_abs"
#define HOME_T				"home_t"
#define MOVE_T_ABS			"move_t_abs"

class ShellInvoker
{
public:
    /// Constructor
    ShellInvoker();

    /// Destructor
    ~ShellInvoker();
	
	/// Calls popen_hand.py, which has predefined integer inputs and returns a short string of ASCII characters.
	static int InvokePopenHand( const std::string & cmd, const char * threadName, const char * subsystem, std::string & resultStr );

	/// Calls either rochacha.py or popen_snap.py. Both scripts return long strings of hexadecimal digits, which
	/// encode raw image data. InvokePopenSnap() converts them to binary data and returns them in *bufferPtrOut,
	/// which must be freed by the caller. InvokePopenSnap() also returns the size of the binary data in bufferSizeOut.
	static int InvokePopenSnap( bool mock, const char * threadName, size_t & bufferSizeOut, void ** bufferPtrOut );

private:
	/// Reads characters until it reads ':'. Interprets the characters as a number. Returns the number. Leaves
	/// fp's position at the next character after ':'.
	static int ReadNumber( FILE * fp, const char * threadName, int & number);

};

#endif 

