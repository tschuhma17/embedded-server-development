import base64
import numpy as np
import binascii
import sys

def make_row(width):
	row = bytearray()
	color = 0;
	z = 0
	x = 0
	color_as_bytes = color.to_bytes(2, 'little')
	while (x < width):
		row.append(color_as_bytes[0])
		x = x + 1
		z = z + 1
		if (z > 7):
			z = 0
			color = color + 1
			color_as_bytes = color.to_bytes(2, 'little')
		if color > 255:
			color = 0
			color_as_bytes = color.to_bytes(2, 'little')
	return row

def capture_image(width, height):
	row = make_row(width)
	image = bytearray()
	y = 0
	while (y < height):
		image.extend(row)
		y = y + 1
	return image

if __name__== '__main__':
	width = 2100
	height = 2100
	raw_image = capture_image(width, height)
	msgLength = str(2 * width * height)
	print(msgLength + ':')
	print(binascii.hexlify(raw_image).decode('ascii'))
