/* controllers/MotionController.cpp */

#include <algorithm>
#include "USAFW/exceptions/LogicExceptions.hpp"

#include "controllers/MotionController.hpp"
#include "exceptions/MotionException.hpp"
#include "MotionServices.hpp"

namespace USAFW {

    MotionController::MotionController(MotionServices &motionServicesP, const std::string nameP) :
        m_motion_services(motionServicesP),
        m_name(nameP)
    {
        ;
    }


    MotionController::~MotionController()
    {
        // Remove all axes from all transports.
        for(std::vector<MotionAxis *>::iterator iter = p_axes->begin();
            iter!=p_axes->end();
            ++iter)
        {
            (*iter)->leave();
        }

        for (std::vector<Transport *>::iterator iter = p_transports->begin();
             iter!=p_transports->end();
             ++iter)
        {
            delete *iter;
        }
        p_transports->clear();

        // Delete transport list.
        delete p_transports;
        p_transports=NULL;

        // Delete all axes.
        delete p_axes;
        p_axes=NULL;
    }

    void MotionController::init(std::vector<Transport*> *pTransportsP, std::vector<MotionAxis*> *pAxesP, int timeout_msP) {
        m_motion_services.getLogger().debug("Base MotionController initialization for \""+m_name+"\"");
        // TODO FIXME - protect against redundant calls (check for null?)
        p_transports    = pTransportsP;
        p_axes          = pAxesP;
    }

    void MotionController::shutdown(int timeout_msP)
    {
        m_motion_services.getLogger().debug("Base MotionController shutdown for \""+m_name+"\"");
	}


    void MotionController::preflightCheck(void) {
        // Check for a top-level Emergency Stop.
        m_motion_services.preflightCheck();
    }


    void MotionController::reboot(unsigned int timeout_msP) {
        THROW(Unimplemented); // TODO FIXME
    }

    /** Returns TRUE if the controller owns and operates the named axis, else false.
     */
    bool MotionController::hasAxis(const std::string nameP) const {
		std::string name = nameP;
		std::transform( name.begin(), name.end(), name.begin(), (int(*)(int)) std::tolower );

        for (std::vector<MotionAxis *>::iterator axis_iter = p_axes->begin();
                axis_iter!=p_axes->end();
                ++axis_iter)
        {
			std::string n = (*axis_iter)->getName();
			std::transform( n.begin(), n.end(), n.begin(), (int(*)(int)) std::tolower );
            if (n.compare(name)==0) {
                return true;
            }
        }
        return false;
    }

    /** Returns the axis requested, or throws NoSuchObjectException. */
    MotionAxis *MotionController::getAxis(const std::string nameP) const {
		std::string name = nameP;
		std::transform( name.begin(), name.end(), name.begin(), (int(*)(int)) std::tolower );
        for (std::vector<MotionAxis *>::iterator axis_iter = p_axes->begin();
                axis_iter!=p_axes->end();
                ++axis_iter)
        {
			std::string n = (*axis_iter)->getName();
			std::transform( n.begin(), n.end(), n.begin(), (int(*)(int)) std::tolower );
//			m_motion_services.getLogger().debug("Comparing \"" + name + "\" with \"" + n + "\"");
            if (n.compare(name)==0) {
                return *axis_iter;
            }
        }
        THROW(NoSuchObject);
    }

}

