/* include/controllers/ACR7000.hpp */

#ifndef __ACR7000_hpp__
#define __ACR7000_hpp__

/* C99/C11 and system headers */
#include <time.h>

/* C++11 headers */
#include <iostream>

/* USA Firmware SLED headers */
#include "USAFW/ProperComms/TCPLink.hpp"
#include "USAFW/timing/timeout.h"

/* Cell-X / Spartronics headers */
#include "controllers/MotionController.hpp"


namespace USAFW {

    // Number of distinct motor axes on the ACR7000 series.  Note that not
    // all models break all of these out as avaialble electrical connections.
    #define ACR7000_MAX_AXES                (8)

	// Number of distinct masters potentially running on a single ACR7000
	// series controller.
	#define ACR7000_MAX_MASTERS             (8)

	// Number of progN programs supported by an ACR7000 controller
	#define ACR7000_MAX_PROGRAMS            (8)

    // Minimum number of bytes in the shortest binary response packet from
    // the ACR7000 controller.
    #define ACR7000_MIN_RESPONSE (8)

    // Maximum number of bytes in the longest binary response packet from
    // the ACR7000 controller.
    // TODO FIXME - REVIEW DOCS AND CONFIRM
    #define ACR7000_MAX_RESPONSE (256)
    
    /// Masks for isolating master flags
	#define BIT_INDEX_ACCELERATING				(0)
	#define BIT_INDEX_DECLERATING				(1)
	#define BIT_INDEX_STOPPING					(2)
	#define BIT_INDEX_JERKING					(3)
	#define BIT_INDEX_IN_MOTION					(4)
	#define BIT_INDEX_KILL_ALL_MOVES			(10)
    #define MASK_IN_MOTION    					(1 << BIT_INDEX_IN_MOTION)

    /// Masks for isolating axis flags
	#define BIT_IPB								(0)
	#define MASK_IPB							(1 << BIT_IPB)
    
    /// Masks for isolating program flags
	#define BIT_INDEX_PROGRAM_RUNNING			(0)
	#define BIT_INDEX_PROGRAM_DWELLING			(1)
	#define BIT_INDEX_PROGRAM_INHIBITED			(2)
	#define BIT_INDEX_PROGRAM_PENDING			(3)
	#define BIT_INDEX_PROGRAM_TIMEOUT			(4)
    #define MASK_PROGRAM_RUNNING    			(1 << BIT_INDEX_PROGRAM_RUNNING)
	
	/// Masks for isolating secondary master flags
	#define BIT_INDEX_ENABLE_RAPID_MOVE_MODES	(5)
    
	/// Masks for isolating secondary master flags
	#define BIT_INDEX_FAST_IPB					(25)
	#define MASK_FAST_IPB						(1 << BIT_INDEX_FAST_IPB)
    
	/// Masks for isolating quaternary axis flags
	#define BIT_INDEX_DRIVE_ENABLE_OUT			(17)
	#define BIT_INDEX_KILL_ALL_MOTION_REQUEST	(19)
	#define BIT_INDEX_PHYS_DRIVE_ENABLE			(28)
	#define BIT_INDEX_DRIVE_FAULT_LATCHED		(29)
	#define BIT_INDEX_EXCESS_POSITION_ERROR		(31)
    #define MASK_DRIVE_ENABLE_OUT				(1 << BIT_INDEX_DRIVE_ENABLE_OUT) ///  B8465
    #define MASK_KILL_ALL_MOTION_REQUEST		(1 << BIT_INDEX_KILL_ALL_MOTION_REQUEST) ///  B8467
    #define MASK_PHYS_DRIVE_ENABLE				(1 << BIT_INDEX_PHYS_DRIVE_ENABLE) /// B8476

    // Masks for isolating servo-drive-2 flags
    #define MASK_ENCODER_READ_FAULT	(0x00100000) /// bit index 20, B10004 
    #define MASK_ENCODER_LOSS_FAULT	(0x00400000) /// bit index 22, B10006

    // Masks for isolating quinary axis flags
	#define BIT_INDEX_POS_EOT_LIMIT_HIT			(4)
	#define BIT_INDEX_NEG_EOT_LIMIT_HIT			(5)
	#define BIT_INDEX_FOUND_HOME				(6)
	#define BIT_INDEX_HOME_FAILED				(7)
	#define BIT_INDEX_POS_SOFT_LIMIT_HIT		(12)
	#define BIT_INDEX_NEG_SOFT_LIMIT_HIT		(13)
    #define MASK_POS_EOT_LIMIT_HIT				(1 << BIT_INDEX_POS_EOT_LIMIT_HIT) /// B16132
    #define MASK_NEG_EOT_LIMIT_HIT				(1 << BIT_INDEX_NEG_EOT_LIMIT_HIT) ///  B16133
    #define MASK_FOUND_HOME						(1 << BIT_INDEX_FOUND_HOME) /// B16134
    #define MASK_HOME_FAILED					(1 << BIT_INDEX_HOME_FAILED) /// B16135
    #define MASK_POS_SOFT_LIMIT_HIT				(1 << BIT_INDEX_POS_SOFT_LIMIT_HIT) /// B16140
    #define MASK_NEG_SOFT_LIMIT_HIT				(1 << BIT_INDEX_NEG_SOFT_LIMIT_HIT) /// B16141

	#define KILL_ALL_MOVES_MASTER_BIT			(522)
	#define KILL_ALL_MOTION_REQUEST_BIT			(8467)
	#define LATCHED_EXCESS_POSITION_ERROR_BIT	(8479)

    #define SETUP_ROUTINE        (0)
    #define CLEAR_KILLS_ROUTINE  (1)
	
	// Masks for sending binary move commands.

    class ACR7000 : public MotionController {
    public:
        /** Selects a binary conversion mode.  Mostly vestigial, from the days when
         *  these controllers were more frequently accessed via serial links.  For
         *  modern usage, use "binary_mode_0", which doesn't use any Kermit (yes,
         *  really) style escape sequences.
         */
        typedef enum binary_mode_e {
            // No Conversion (recommended for Ethernet)
            binary_mode_0,

            // Control Character Prefixing
            binary_mode_1,

            // No Conversion
            binary_mode_2,

            // Control Character Prefixing and High Bit Stripping
            binary_mode_3
        } binary_mode_t;


        /** This is an integer index into the ACR7000's memory, used to look up a "P-Value",
         *  or parameter/value, so called because this is either a configuration value, or a
         *  run-time status (that is, either a "parameter" for the controller to consider
         *  while operating, or a "value" describing the controller's current state which the
         *  controlling system may wish to evaluate).
         */
        typedef const uint16_t pvalue_index_t;


        /** This is the structure of the binary data packet request header sent
         *  to the ACR7000 to request a binary data packet.  This exact same
         *  header is then echoed back (originally intended, presumably, to allow
         *  dropping a response to an incorrectly received query, back when the
         *  interface was for serial usage, but now used for asynchronous
         *  sampling).
         */
        typedef struct binary_data_packet_s {
            // This is a sentinel value-- 0x00-- which indicates the start of a
            // binary data object.
            uint8_t     m_header_id;

            // Selects a general data grouping.
            uint8_t     m_group_code;

            // Selects a set of eight fields within the data group.
            uint8_t     m_group_index;

            // Selects which of the eight data fields in the indexed subgroup will
            // be used to compose the final data packet.
            uint8_t     m_isolation_mask;
        } binary_data_packet_t;

        /** Binary parameter access packet ID codes.  This are used to request a value
         *  for a specific parameter, and also appear in the response packets.
         */
        typedef enum packet_id_e {
            packet_id_get_long  = 0x88,
            packet_id_set_long  = 0x89,
            packet_id_get_float = 0x8A,
            packet_id_set_float = 0x8B
        } packet_id_t;


        /** This structure is used to request a signed 32-bit integer parameter value,
         *  or a 32-bit IEEE floating-point value, from the ACR7000 controller.
         */
        typedef struct binary_get_pvalue_request_s {
            // This is a sentinel value-- 0x00-- which indicates the start of a
            // binary data object.
            uint8_t     m_header_id;

            // This tells the controller what operation is being requested.
            // This must be set to packet_id_get_long or packet_id_get_float.
            uint8_t     m_packet_id;

            // Parameter value index/address, low order byte (addr&0xFF).
            uint8_t     m_pvalue_index_low;

            // Parameter value index/address, high order byte (addr>>8).
            uint8_t     m_pvalue_index_high;
        } binary_get_pvalue_request_t;


        /** This structure is used to receive the response to a get request for
         *  a signed 32-bit integer parameter value, or a 32-bit IEEE floating-
         *  point value, from the ACR7000 controller.
         */
        typedef struct binary_get_pvalue_response_s {
            // This is a sentinel value-- 0x00-- which indicates the start of a
            // binary data object.
            uint8_t     m_header_id;

            // This tells the controller what operation is being requested.
            // This must be set to packet_id_get_long or packet_id_get_float.
            uint8_t     m_packet_id;

            // Parameter value index/address, low order byte (addr&0xFF).
            uint8_t     m_pvalue_index_low;

            // Parameter value index/address, high order byte (addr>>8).
            uint8_t     m_pvalue_index_high;

            union {
                // For 32-bit signed integer ("LONG") parameter values, data
                // is received into this structure, low byte first.
                struct {
                    uint8_t m_long_foreign_bytes[4];
                } u_little_endian_long;

                // For 32-bit IEEE floating-point ("FP32") parameter values, data
                // is received into this structure, low byte first.
                struct {
                    uint8_t m_float_foreign_bytes[4];
                } u_little_endian_fp32;
            };
        } binary_get_pvalue_response_t;


        /** This structure is used to set a signed 32-bit integer parameter value,
         *  or a 32-bit IEEE floating-point value, on the ACR7000 controller.
         * 
         *  (NOTE: No response is expected from this request, so there is no corresponding
         *  response structure.)
         */
        typedef struct binary_set_pvalue_request_s {
            // This is a sentinel value-- 0x00-- which indicates the start of a
            // binary data object.
            uint8_t     m_header_id;

            // This tells the controller what operation is being requested.
            // This must be set to packet_id_get_long or packet_id_get_float.
            uint8_t     m_packet_id;

            // Parameter value index/address, low order byte (addr&0xFF).
            uint8_t     m_pvalue_index_low;

            // Parameter value index/address, high order byte (addr>>8).
            uint8_t     m_pvalue_index_high;

            union {
                // For 32-bit signed integer ("LONG") parameter values, data
                // is sent from this structure, low byte first.
                struct {
                    uint8_t m_long_foreign_bytes[4];
                } u_little_endian_long;

                // For 32-bit IEEE floating-point ("FP32") parameter values, data
                // is sent from this structure, low byte first.
                struct {
                    uint8_t m_float_foreign_bytes[4];
                } u_little_endian_fp32;
            };
        } binary_set_pvalue_request_t;


        /** These codes select a group of eight fields, which are then
         *  masked; this selects the data retrieved by the system.
         */
        typedef enum binary_data_group_code_e {
            binary_data_group_flag_params           = 0x10,
            binary_data_group_encoder_0_15_params   = 0x18,
            binary_data_group_DAC_params            = 0x19,
            binary_data_group_PLC_params            = 0x1A,
            binary_data_group_misc                  = 0x1B,
            binary_data_group_program_params        = 0x1C,
            binary_data_group_master_0_7_params     = 0x20,
            binary_data_group_master_8_15_params    = 0x28,
            binary_data_group_axis_0_7_params       = 0x30,
            binary_data_group_axis_8_15_params      = 0x38,
            binary_data_group_CMT_params            = 0x40,
            binary_data_group_logging_params        = 0x50,
            binary_data_group_encoder_16_23_params  = 0x60
        } binary_data_group_code_t;


        /** P-Value type.  Either 32-bit integer or a 32-bit float.
         */
        typedef enum pvalue_type_e {
            /** Defines pvalue_type_t==0 as invalid.  Used to help detect
             *  improper, corrupt, or uninitialized data.
             */
            pvalue_type_INVALID=0,

            /** This PValue latch holds a 32-bit signed integer in the
             *  embedded/control system's (THIS system's) native byte order.
             */
            pvalue_type_long,

            /** This PValue latch holds a 32-bit floating-point number in the
             *  embedded/control system's (THIS system's) native byte order.
             */
            pvalue_type_float,

            /** Total number of defined pvalue_type enumeration values.
             *  This number minus one is tha maximum valid value for a
             *  variable of type pvalue_type_t.
             */
            pvalue_type_COUNT
        } pvalue_type_t;


        /* === ACR7000 BITS AND PARAMETERS === */
        
        /* === P4096-P4375: Flag Parameters === */

        /** PValue addresses to retrieve Master Flag parameters for masters (0-7).
         *  Retrieved value type/mask: Long integer (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_master_flags[ACR7000_MAX_MASTERS] =
                        {4112, 4113, 4114, 4115, 4116, 4117, 4118, 4119}; 
		int AR7xT_first_bit_master_flags = 512;

        pvalue_index_t   AR7xT_pvaddrs_axis_flags[ACR7000_MAX_AXES] =
                        {4120, 4121, 4122, 4123, 4124, 4125, 4126, 4127}; 
		int AR7xT_first_bit_axis_flags = 768;

        pvalue_index_t   AR7xT_pvaddrs_program_flags[ACR7000_MAX_PROGRAMS] =
                        {4128, 4129, 4130, 4131, 4132, 4133, 4134, 4135};
		int AR7xT_first_bit_program_flags = 1024;
                        
        pvalue_index_t   AR7xT_pvaddrs_secondary_master_flags[ACR7000_MAX_MASTERS] =
                        {4160, 4161, 4162, 4163, 4164, 4165, 4166, 4167}; 
		int AR7xT_first_bit_secondary_master_flags = 2048;

        pvalue_index_t   AR7xT_pvaddrs_secondary_axis_flags[ACR7000_MAX_AXES] =
                        {4168, 4169, 4170, 4171, 4172, 4173, 4174, 4175}; 
		int AR7xT_first_bit_secondary_axis_flags = 2304;

        pvalue_index_t   AR7xT_pvaddrs_quaternary_axis_flags[ACR7000_MAX_AXES] =
                        {4360, 4361, 4362, 4363, 4364, 4365, 4366, 4367};
		int AR7xT_first_bit_quaternary_axis_flags = 8448;
 
        pvalue_index_t   AR7xV_pvaddrs_servo_drive_status_2_flags[ACR7000_MAX_PROGRAMS] =
                        {4408, 4409, 4410, 4411, 4412, 4413, 4414, 4415};
		int AR7xT_first_bit_servo_drive_status_2_flags = 9984;

        pvalue_index_t   AR7xT_pvaddrs_quinary_axis_flags[ACR7000_MAX_PROGRAMS] =
                        {4600, 4601, 4602, 4603, 4604, 4605, 4606, 4607};
		int AR7xT_first_bit_quinary_axis_flags = 6128;
 
        /* === P8192-P10047: Master Parameters === */
        /** PValue addresses to retrieve Master Flag parameters for masters (0-7).
         *  Retrieved value type/mask: Long integer (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_distance_into_move[ACR7000_MAX_PROGRAMS] =
                        {8192, 8448, 8704, 8960, 9216, 9472, 9728, 9984};

        pvalue_index_t   AR7xT_pvaddrs_distance_to_go[ACR7000_MAX_PROGRAMS] =
                        {8200, 8456, 8712, 8968, 9224, 9480, 9736, 9992};

        pvalue_index_t   AR7xT_pvaddrs_total_distance[ACR7000_MAX_PROGRAMS] =
                        {8203, 8459, 8715, 8971, 9227, 9483, 9739, 9995};

        /* === P12288-P14207: Axis Parameters === */

        // == Position Parameters ==

        /** PValue addresses to retrieve the current position for an axis (0-7).
         *  Retrieved value type/mask: Long integer (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_current_position[ACR7000_MAX_AXES] =
                        {12288, 12544, 12800, 13056, 13312, 13568, 13824, 14080}; 

        /** PValue addresses to retrieve the target position for an axis (0-7).
         *  Retrieved value type/mask: Long integer (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_target_position[ACR7000_MAX_AXES] =
                        {12289, 12545, 12801, 13057, 13313, 13569, 13825, 14081 };

        /** PValue addresses to retrieve the actual position for an axis (0-7).
         *  Retrieved value type/mask: Long integer (32-bit). 
         */
        pvalue_index_t    AR7xT_pvaddrs_axis_actual_position[ACR7000_MAX_AXES] =
                        {12290, 12546, 12802, 13058, 13314, 13570, 13826, 14082 };

        /** PValue addresses to retrieve the following error for an axis (0-7).
         *  Retrieved value type/mask: Long integer (32-bit). 
         */
        pvalue_index_t    AR7xT_pvaddrs_axis_following_error[ACR7000_MAX_AXES] =
                        {12291, 12547, 12803, 13059, 13315, 13571, 13827, 14083 };

        /** PValue addresses to retrieve hardware capture values for an axis (0-7).
         *  Retrieved value type/mask: Long integer (32-bit). 
         */
        pvalue_index_t    AR7xT_pvaddrs_axis_hardware_capture[ACR7000_MAX_AXES] =
                        {12292, 12548, 12804, 13060, 13316, 13572, 13828, 14084 };

        /** PValue addresses to retrieve software capture values for an axis (0-7).
         *  Retrieved value type/mask: Long integer (32-bit). 
         */
        pvalue_index_t    AR7xT_pvaddrs_axis_software_capture[ACR7000_MAX_AXES] =
                        {12293, 12549, 12805, 13061, 13317, 13573, 13829, 14085 };

        /** PValue addresses to retrieve primary setpoint values for an axis (0-7).
         *  Retrieved value type/mask: Long integer (32-bit). 
         */
        pvalue_index_t    AR7xT_pvaddrs_axis_primary_setpoint[ACR7000_MAX_AXES] =
                        {12294, 12550, 12806, 13062, 13318, 13574, 13830, 14086 };

        /** PValue addresses to retrieve secondary setpoint values for an axis (0-7).
         *  Retrieved value type/mask: Long integer (32-bit). 
         */
        pvalue_index_t    AR7xT_pvaddrs_axis_secondary_setpoint[ACR7000_MAX_AXES] =
                        {12295, 12551, 12807, 13063, 13319, 13575, 13831, 14087 };

        // == Offset Parameters ==

        // == Servo Parameters ==

        /** PValue addresses to retrieve Proportional Gain values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_PGAIN[ACR7000_MAX_AXES] =
                        {12304, 12560, 12816, 13072, 13328, 13584, 13840, 14096};

        /** PValue addresses to retrieve Integral Gain values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_IGAIN[ACR7000_MAX_AXES] =
                        {12305, 12561, 12817, 13073, 13329, 13585, 13841, 14097};

        /** PValue addresses to retrieve Integral Limit values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_ILIMIT[ACR7000_MAX_AXES] =
                        {12306, 12562, 12818, 13074, 13330, 13586, 13842, 14098};

        /** PValue addresses to retrieve Integral Delay values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_IDELAY[ACR7000_MAX_AXES] =
                        {12307, 12563, 12819, 13075, 13331, 13587, 13843, 14099};

        /** PValue addresses to retrieve Derivative Gain values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_DGAIN[ACR7000_MAX_AXES] =
                        {12308, 12564, 12820, 13076, 13332, 13588, 13844, 14100};

        /** PValue addresses to retrieve Derivative Width values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_DWIDTH[ACR7000_MAX_AXES] =
                        {12309, 12565, 12821, 13077, 13333, 13589, 13845, 14101};

        /** PValue addresses to retrieve Feedforward Velocity values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_FFVEL[ACR7000_MAX_AXES] =
                        {12310, 12566, 12822, 13078, 13334, 13590, 13846, 14102};

        /** PValue addresses to retrieve Feedforward Acceleration values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_FFACC[ACR7000_MAX_AXES] =
                        {12311, 12567, 12823, 13079, 13335, 13591, 13847, 14103};


        // == Monitor Parameters == 

        /** PValue addresses to retrieve proportional term values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_proportional_term[ACR7000_MAX_AXES] =
                        {12312, 12568, 12824, 13080, 13336, 13592, 13848, 14104};

        /** PValue addresses to retrieve integral term values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_integral_term[ACR7000_MAX_AXES] =
                        {12313, 12569, 12825, 13081, 13337, 13593, 13849, 14105};

        /** PValue addresses to retrieve derivative term values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_derivative_term[ACR7000_MAX_AXES] =
                        {12314, 12570, 12826, 13082, 13338, 13594, 13850, 14106};

        /** PValue addresses to retrieve velocity values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_velocity[ACR7000_MAX_AXES] =
                        {12315, 12571, 12827, 13083, 13339, 13595, 13851, 14107};

        /** PValue addresses to retrieve acceleration values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_acceleration[ACR7000_MAX_AXES] =
                        {12316, 12572, 12828, 13084, 13340, 13596, 13852, 14108};

        /** PValue addresses to retrieve summation point values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_summation_point[ACR7000_MAX_AXES] =
                        {12317, 12573, 12829, 13085, 13341, 13597, 13853, 14109};

        /** PValue addresses to retrieve filter output signal values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_filter_output_signal[ACR7000_MAX_AXES] =
                        {12318, 12574, 12830, 13086, 13342, 13598, 13854, 14110};

        /** PValue addresses to retrieve output signal values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_output_signal[ACR7000_MAX_AXES] =
                        {12319, 12575, 12831, 13087, 13343, 13599, 13855, 14111};

        // == Limit Parameters ==

        /** PValue addresses to retrieve Plus Excess Error values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_plus_excess_error[ACR7000_MAX_AXES] =
                        {12320, 12576, 12832, 13088, 13344, 13600, 13856, 14112};

        /** PValue addresses to retrieve Minus Excess Error values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_minus_excess_error[ACR7000_MAX_AXES] =
                        {12321, 12577, 12833, 13089, 13345, 13601, 13857, 14113};

        /** PValue addresses to retrieve Plus In Position values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_plus_in_position[ACR7000_MAX_AXES] =
                        {12322, 12578, 12834, 13090, 13346, 13602, 13858, 14114};

        /** PValue addresses to retrieve Minus In Position values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_minus_in_position[ACR7000_MAX_AXES] =
                        {12323, 12579, 12835, 13091, 13347, 13603, 13859, 14115};

        /** PValue addresses to retrieve Plus A Limit values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_plus_A_limit[ACR7000_MAX_AXES] =
                        {12324, 12580, 12836, 13092, 13348, 13604, 13860, 14116};

        /** PValue addresses to retrieve Minus A Limit values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_minus_A_limit[ACR7000_MAX_AXES] =
                        {12325, 12581, 12837, 13093, 13349, 13605, 13861, 14117};

        /** PValue addresses to retrieve Plus B Limit values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_plus_B_limit[ACR7000_MAX_AXES] =
                        {12326, 12582, 12838, 13094, 13350, 13606, 13862, 14118};

        /** PValue addresses to retrieve Minus B Limit values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_minus_B_limit[ACR7000_MAX_AXES] =
                        {12327, 12583, 12839, 13095, 13351, 13607, 13863, 14119};

        /** PValue addresses to retrieve Plus Torque Limit values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_plus_torque_limit[ACR7000_MAX_AXES] =
                        {12328, 12584, 12840, 13096, 13352, 13608, 13864, 14120};

        /** PValue addresses to retrieve Minus Torque Limit values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_minus_torque_limit[ACR7000_MAX_AXES] =
                        {12329, 12585, 12841, 13097, 13353, 13609, 13865, 14121};

        /** PValue addresses to retrieve Plus Torque Band values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_plus_torque_band[ACR7000_MAX_AXES] =
                        {12330, 12586, 12842, 13098, 13354, 13610, 13866, 14122};

        /** PValue addresses to retrieve Minus Torque Band values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_minus_torque_band[ACR7000_MAX_AXES] =
                        {12331, 12587, 12843, 13099, 13355, 13611, 13867, 14123};

        /** PValue addresses to retrieve Backlash Setting values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_backlash_setting[ACR7000_MAX_AXES] =
                        {12332, 12588, 12844, 13100, 13356, 13612, 13868, 14124};

        /** PValue addresses to retrieve Plus Jog Limit values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_plus_jog_limit[ACR7000_MAX_AXES] =
                        {12334, 12590, 12846, 13102, 13358, 13614, 13870, 14126};

        /** PValue addresses to retrieve Minus Jog Limit values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_minus_jog_limit[ACR7000_MAX_AXES] =
                        {12335, 12591, 12847, 13103, 13359, 13615, 13871, 14127};

        /** PValue addresses to retrieve FIXME values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_FIXME[ACR7000_MAX_AXES] =
                        {};


        // == Filter 0 Parameters ==
        // TODO
        /** PValue addresses to retrieve FIXME values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        //pvalue_index_t   AR7xT_pvaddrs_axis_FIXME[ACR7000_MAX_AXES] =
        //                {};

        // == Filter 1 Parameters ==
        // TODO

        // == Jog Parameters ==
        /** PValue addresses to retrieve current jog velocity for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_current_jog_vel[ACR7000_MAX_AXES] =
                        {12346, 12602, 12858, 13114, 13370, 13626, 13882, 14138};

        /** PValue addresses to retrieve FIXME for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_current_jog_acc[ACR7000_MAX_AXES] =
                        {12347, 12603, 12859, 13115, 13371, 13627, 13883, 14139};

        /** PValue addresses to retrieve the jog velocity setting for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_jog_vel_setting[ACR7000_MAX_AXES] =
                        {12348, 12604, 12860, 13116, 13372, 13628, 13884, 14140};

        /** PValue addresses to retrieve the jog acceleration setting for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_jog_acc_setting[ACR7000_MAX_AXES] =
                        {12349, 12605, 12861, 13117, 13373, 13629, 13885, 14141};

        /** PValue addresses to retrieve the jog deceleration setting for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_jog_dec_setting[ACR7000_MAX_AXES] =
                        {12350, 12606, 12862, 13118, 13374, 13630, 13886, 14142};

        /** PValue addresses to retrieve the jog "jerk" setting for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_jog_jrk_setting[ACR7000_MAX_AXES] =
                        {12351, 12607, 12863, 13119, 13375, 13631, 13887, 14143};


        // == Additional Servo Parameters ==

        /** PValue addresses to retrieve FBVEL Gain values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_FBVEL_gain[ACR7000_MAX_AXES] =
                        {12352, 12608, 12864, 13120, 13376, 13632, 13888, 14144};

        /** PValue addresses to retrieve FBVEL Term values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_FBVEL_term[ACR7000_MAX_AXES] =
                        {12353, 12609, 12865, 13121, 13377, 13633, 13889, 14145};

        /** PValue addresses to retrieve FFVEL Term values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_FFVEL_term[ACR7000_MAX_AXES] =
                        {12354, 12610, 12866, 13122, 13378, 13634, 13890, 14146};

        /** PValue addresses to retrieve FFACC Term values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_FFACC_term[ACR7000_MAX_AXES] =
                        {12355, 12611, 12867, 13123, 13379, 13635, 13891, 14147};

        /** PValue addresses to retrieve PPU Term values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        pvalue_index_t   AR7xT_pvaddrs_axis_PPU_term[ACR7000_MAX_AXES] =
                        {12375, 12631, 12887, 13143, 13399, 13655, 13911, 14167};

        /** PValue addresses to retrieve FIXME values for an axis (0-7).
         *  Retrieved value type/mask: Floating-point (32-bit). 
         */
        //pvalue_index_t   AR7xT_pvaddrs_axis_FIXME[ACR7000_MAX_AXES] =
        //                {};


        /** Control bit flags.
         *  Add (32*stepper_index) to this value.
         */
        typedef enum ACR7xT_control_bit_flag_e {
            ctrl__assert_new_config                 = 15616,
            ctrl__reset_params_to_factory_defaults  = 15617,
            ctrl__enable_auto_standby               = 15618,
            ctrl__assert_standby_current            = 15619
        } ACR7xT_control_bit_flag_t;


        /** Status bit flags.
         *  Add (32*stepper_index, starting at 0) to this value.
         */
        typedef enum ACR7xT_status_bit_flag_e {
            status__driver_chip_configured          = 15620,
            status__driver_chip_config_underway     = 15621,
            status__user_config_invalid_reverting   = 15622,
            status__change_to_stby_current_underway = 15623
        } ACR7xT_status_bit_flag_t;


        /** Instantaneous fault and warning bit flags.  These will clear if
         *  the operating condition causing the fault or warning clears.
         *  Add (32*stepper_index, starting at 0) to this value.
         */
        typedef enum ACR7xT_fault_and_warning_bit_flag_e {
            fault__motor_short_to_ground            = 15624,
            fault__over_temperature_warning         = 15625,
            fault__over_temperature_fault           = 15626,
            fault__stall_threshold_warning          = 15627,
            fault__under_voltage                    = 15628,
            fault__drive_at_standby_current         = 15629,
            fault__general_fault                    = 15630
        } ACR7xT_fault_and_warning_bit_flag_t;


        /** Latched fault and warning bit flags.  These are proper latches,
         *  and will NOT clear unless reset (the drive is disabled, then
         *  reenabled).
         *  Add (32*stepper_index, starting at 0) to this value.
         */
        typedef enum ACR7xT_latched_fault_warning_bit_flag_e {
            fault_latch__motor_short_to_ground      = 15640,
            fault_latch__over_temperature_warning   = 15641,
            fault_latch__over_temperature_fault     = 15642,
            fault_latch__stall_threshold_warning    = 15643,
            fault_latch__under_voltage              = 15644,
            fault_latch__not_used                   = 15645,
            fault_latch__general_fault              = 15646
        } ACR7xT_latched_fault_warning_bit_flag_t;


        /** Integrated stepper motor control and status parameters.
         *  NOTE: The parameter codes are "Pxxxx" where xxxx follows from this enum.
         *  Add (16*stepper_index, starting at 0) to the parameter.
         */
        typedef enum ACR7xT_stepper_parameter_e {
            /** Description: Full-scale current in Amperes.
             *  Value type: Floating-point status
             *  Default value: N/A
             *  Range: Value reported as a status
             */
            stepper_param__full_scale_current_amps  = 7936,

            /** Description: Product (actual hardware) maximum motor current in Amperes.
             *  Value type: Floating-point status
             *  Default value: N/A
             *  Range: Value reported as a status
             *  WARNING: Setting this value too high can result in hazardous thermal conditions.
             */
            stepper_param__product_max_motor_amps   = 7937,

            /** Description: User selected maximum motor current in Amperes.
             *  Value type: Floating-point
             *  Default value: 0.5
             *  Range: 0.0 to 4.0
             *  WARNING: Setting this value too high can result in hazardous thermal conditions.
             */
            stepper_param__user_max_motor_amps      = 7938,

            /** Description: Motor resistance in ohms.
             *  Value type: Floating-point
             *  Default value: 0.9
             *  Range: 0.1 to 15.0
             */
            stepper_param__motor_resistance_ohms    = 7939,

            /** Description: Motor inductance in millihenries.
             *  Value type: Floating-point
             *  Default value: 2.5
             *  Range: 0.1 to 40.0
             */
            stepper_param__motor_inductance_mH      = 7940,

            /** Description: Standby current percentage (of limits).
             *  Value type: Integer 
             *  Default value: 100
             *  Range: 3 to 100
             *  WARNING: If the effective standby current is too high,
             *  a thermal hazard can result even without movement.
             */
            stepper_param__standby_current_pct      = 7944,
            
            /** Description: Time over which current will adjust from full to standby,
             *               in milliseconds.
             *  Value type: Integer 
             *  Default value: 0
             *  Range: 0-5000
             */
            stepper_param__time_from_full_to_stby   = 7945,
            
            /** Description: Micro-steps per full step (2^N, specified as 2^N, not N).
             *  Value type: Integer 
             *  Default value: 256
             *  Range: {1, 2, 4, 8, 16, 32, 64, 128, 256}
             */
            stepper_param__microsteps_per_step      = 7946,
            
            /** Description: Configuration error code
             *  Value type: Integer value reported as a status
             *  Default value: N/A
             *  Range: The following values:
             *         0 - No error
             *         1 - Max current setting range error
             *         2 - Motor resistance range error
             *         3 - Motor inductance range error
             *         4 - Standby current range error
             *         5 - Standby time range error
             *         6 - Micro-stepping setting value error
             */
            stepper_param__configuration_error_Code = 7947,
 
            /** Description: Drive Register write value (for debugging)
             *  Value type: Integer?
             *  Default value: N/A
             *  Range: not specified?
             */
            stepper_param__drive_register_wrval     = 7948,

            /** Description: Drive Register read value (for debugging)
             *  Value type: Integer
             *  Default value: N/A
             *  Range: Value reported as a status
             */
            stepper_param__drive_register_rdval     = 7949,

            /** Description: Drive register tuning value (for debugging)
             *  Value type: Integer
             *  Default value: 73765
             *  Range: 0 to 131071 (0x1ffff)
             */
            stepper_param__drive_tuning_value       = 7950,

            /** Description: Drive register raw status read
             *  Value type: Integer
             *  Default value: N/A
             *  Range: 16 or 20 bit number
             */
            stepper_param__drive_raw_status_read    = 7951            
        } ACR7xT_stepper_parameter_t;


        /** Parameters used for servo drive configuration, normally set from the
         *  Configuration Wizard in Parker Motion Manager.
         *  NOTE: The parameter codes are "Pxxxx" where xxxx follows from this enum.
         *  FIXME: servo index offset unknown; these may be global?
         *  NOTE: These values are in the order in which they appear in the
         *        "ACR Programmer's Guide", and are not sorted by value.
         */
        typedef enum ACR7xV_servo_config_parameter_e {
            /** Description: Counts per revolution (rotary) or electrical pitch (linear).
             *  Value type: Integer
             *  Default value: 524288
             *  Range: 32 to 1073741823
             */
            servo_param__feedback_resolution        = 28674,

            /** Description: Continuous operating current in Amps (rms).
             *  Value type: Float
             *  Default value: 2.6
             *  Range: 0 to 200
             */
            servo_param__continuous_current_amps    = 28704,

            /** Description: Current derating percentage at rated speed.
             *  Value type: Integer?
             *  Default value: 10
             *  Range: 0 to 100
             */
            servo_param__cont_current_derating_pct  = 28705,

            /** Description: Maximum allowable current for the motor, Amps (rms).
             *  Value type: Float
             *  Default value: 7.8
             *  Range: 0 to 400
             */
            servo_param__peak_current_amps_rms      = 28706,

            /** Description: Maximum value of motor inductance, in millihenries.
             *  Value type: Float
             *  Default value: 5.68
             *  Range: 0 to 200
             */
            servo_param__motor_inductance_mH        = 28707,

            /** Description:
             *  Value type:
             *  Default value:
             *  Range:
             */
            servo_param__motor_inductance_factor    = 28708,

            /** Description:
             *  Value type:
             *  Default value:
             *  Range:
             */
            servo_param__motor_maximum_temp_degC    = 28709,

            /** Description:
             *  Value type:
             *  Default value:
             *  Range:
             */
            servo_param__winding_resistance_ohms    = 28710,

            /** Description: Speed of motor at maximum power.
             *  Value type: Float?
             *  Default value: 83.3
             *  Range: 0 to 400
             */
            servo_param__motor_rated_speed_rps      = 28711,

            /** Description: Motor pole count divided by 2.
             *  Value type: Integer
             *  Default value: 4
             *  Range: 1 to 200
             */
            servo_param__motor_pole_pairs           = 28675,

            /** Description:
             *  Value type:
             *  Default value:
             *  Range:
             */
            servo_param__motor_damping              = 28712,

            /** Description: Motor rotor inertia for rotary motors, in (kg*m^2)*(10^-6), or the
             *               forcer mass, in kilograms for linear motors.
             *  Value type: Integer
             *  Default value: 25
             *  Range: 0 to 1000000
             */
            servo_param__motor_inertia_or_mass      = 28713,

            /** Description: Electrical pitch of the magnets for use with linear motors.  Set to
             *               zero for rotary motors.
             *  Value type: Integer?
             *  Default value: 0
             *  Range: 0 for rotary motors, 0 to 300 for linear motors.
             */
            servo_param__linear_motor_pitch_mm      = 28715,

            /** Description: Maximum torque in Newtons available for rotary motors; maximum drive
             *               force in Newtons for linear motors.
             *  Value type: Float
             *  Default value: 4.67
             *  Range: 0 to 4000
             */
            servo_param__maximum_torque_or_force_N  = 28717,

            /** Description: Full scale torque in Newtons available for motor.  Maximum drive force
             *               in Newtons for linear motors.
             *  Value type: Float
             *  Default value: 4.67
             *  Range: 0 to 4000
             */
            servo_param__torque_scaling_Nm          = 28718,

            /** Description: Positive Encoder direction: 0 for CW, 1 for CCW.
             *  Value type: Boolean as an integer
             *  Default value: 1
             *  Range: 0 or 1
             *  WARNING: The reference from which this should be evaluated is unclear.
             */
            servo_param__encoder_polarity           = 28776,

            /** Description: Controls the logic sense of Hall effect sensors.  Set to 1 to invert
             *               these logic signals from sensors of this type.
             *  Value type: Boolean as an integer
             *  Default value: 0
             *  Range: 0 (normal) or 1 (inverted)
             */
            servo_param__invert_hall_effect_sensors = 28678,

            /** Description: Commutation with incremental encoders: 0 = Hall effect, 2 = DC brush mode.
             *  Value type: Integer
             *  Default value: 0 (Hall effect)
             *  Range: 0 to 4:
             *         0: Hall effect
             *         1: UNSPECIFIED, DO NOT USE
             *         2: DC brush mode
             *         3: UNSPECIFIED, DO NOT USE
             *         4: UNSPECIFIED, DO NOT USE
             */
            servo_param__hall_only_commutation      = 28775,

            /** Description: Motor ambient temperature used by the software motor thermal model.
             *  Value type: Integer
             *  Default value: 25
             *  Range: -50 to 250
             */
            servo_param__thermal_model_ambient_degC = 28719,

            /** Description: Motor thermal resistance in degrees C per watt.  This is defined as
             *               the temperature increase in the motor winding relative to the case
             *               temperature, per watt of power dissipated between the winding and the
             *               case.
             *  Value type: Float
             *  Default value: 1.4
             *  Range: 0 to 16
             */
            servo_param__motor_thermal_resistance   = 28720,

            /** Description: Motor thermal time constant.  This is the time for the motor to
             *               reach 63% of its final temperature, in minutes, given constant power.
             *  Value type: Float
             *  Default value: 20
             *  Range: 0 to 7200
             */
            servo_param__motor_thermal_tc           = 28721,

            /** Description: Winding thermal time constant.  This is the time for the winding to
             *               reach 63% of its final temperature rise above the rest of the motor,
             *               in minutes, given constant power.
             *  Value type: Float
             *  Default value: 0.7 
             *  Range: 0 to 3600
             */
            servo_param__winding_thermal_tc         = 28722,

            /** Description: Thermal switch checking.  0 to enable, 1 to disable.
             *  Value type: Boolean as integer
             *  Default value: 1
             *  Range: 0 (check thermal switches) or 1 (do not check thermal switches).
             */
            servo_param__disable_thermal_switches   = 28679,

            /** Description: Maximum velocity of rotary motor in revolutions per second; maxium
             *               velocity of linear motors in meters per second.
             *  Value type: Float
             *  Default value: 83.3
             *  Range: 0 to 250
             */
            servo_param__motor_velocity_limit       = 28716,

            /** Description: Encoder commutation offset in counts.  1.0 equals 180 degrees.
             *  Value type: Float
             *  Default value: -0.45
             *  Range: -1.0 to 1.0
             */
            servo_param__encoder_commutation_offset = 28725,            

            /** Description: Number of valid bits for serial encoder.  Single and multi-turn
             *               total.
             *  Value type: Integer
             *  Default value: 35
             *  Range: 0 to 214783647
             */
            servo_param__serial_encoder_valid_bits  = 28769,

            /** Description: Feedback type.  1 for incremental, 5 for BiSS (ed: binary spread-spectrum?)
             *  Value type: Integer
             *  Default value: 5
             *  Range: 0 to 10, but only 1 (incremental) and 5 (BiSS) are described.
             */
            servo_param__feedback_type              = 28771,

            /** Description: Number of supported multi turns for a serial encoder.
             *  Value type: Integer
             *  Default value: 65535
             *  Range: 1 to 214783647
             */
            servo_param__serial_enc_valid_turns     = 28772,

            /** Description: Number of single turn bits in BiSS frame
             *  Value type: Integer
             *  Default value: 21
             *  Range: 0 to 64
             */
            servo_param__biss_single_turn_bits      = 28800,

            // TODO FIXME - enums 28801 and up (ACR Programmer's Guide P337)
        } ACR7xV_servo_config_parameter_t;


        typedef enum pvalue_query_type_e {
            // The value should be queried once, and only once.  After the value is
            // is received and latched, the query type in the latched value will be
            // changed to pvalue_query_complete. 
            pvalue_query_one_shot,

            // The value should be queried at regular intervals (specified by an
            // associated parameter).
            pvalue_query_recurring,

            // The value has been queried once as per pvalue_query_one_shot, a value
            // has been received, and no further queries should be made unless
            // explicitly requested.
            pvalue_query_complete
        } pvalue_query_type_t;


        /** This structure specifies the rate at which a particular P-value should be
         *  queried from the ACR7000 (including a one-shot query) and contains the
         *  timestamped state data received as a response to those requests.
         */
        typedef struct pvalue_value_request_latch_s {
            /** The P-value (aka parameter, flag, etc.) index. */
            uint16_t            m_pvalue_index;

            /** The type of the P-Value register being queried (integer, floating-
             *  point).
             */
            pvalue_type_t       m_pvalue_type;

            /** Query type (one-shot, recurring, or complete/none). */
            pvalue_query_type_t m_query_type;

            /** The interval between queries, specified as a RELATIVE timespec (that is,
             *  an interval with no epoch, rather than a wall-clock or monotonic time).
             *  For a one-shot query, this should be zero.
             */
            struct timespec     m_interval;

            // True if and only if at least one request has been sent.
            bool                m_request_sent;

            // True if and only if a response has been received at least once.
            bool                m_latch_valid;

            // The time at which the last request was sent to the ACR7000 controller
            // by the embedded system.  This value comes from the system's monotonic
            // clock, which is guaranteed never to be corrected on the fly-- see:
            // https://pubs.opengroup.org/onlinepubs/9699919799/functions/clock_getres.html
            // You MUST check that m_latch_valid is true before using this value.
            struct timespec     m_request_timestamp;

            // The time at which the value was last received from the ACR7000 controller
            // by the embedded system.  This value comes from the system's monotonic
            // clock, which is guaranteed never to be corrected on the fly-- see:
            // https://pubs.opengroup.org/onlinepubs/9699919799/functions/clock_getres.html
            // You MUST check that m_latch_valid is true before using this value.
            struct timespec     m_response_timestamp;

            // The value of the flag/latch.  This is a union; the appropriate data type
            // must be used for the flag/latch index in question.  You MUST check that
            // m_latch_valid is true before using this value, and check pvalue_type
            union {
                // A signed 32-bit integer value.  Make sure this is the correct type for
                // flag/value index you are trying to work with.
                int32_t         m_value_int32;

                // A 32-bit floating-point value.  Make sure this is the correct type for
                // flag/value index you are trying to work with.
                float           m_value_float32;
            };
        } pvalue_request_latch_t;

        /** This structure is used to send move commands that affect 2 slave axes.
		 *  The controller supports up to 15 slave axes, but I don't expect we need to
		 *  use that much wasted space. - MDG
		 *  This structure does not contain space for Arc Radius Scaling, so always_noconv
		 *  set the Arc_Radius_Scaling bit to 0.
         */
        typedef struct binary_move_command_request_s {
            // This is a sentinel value-- 0x00-- which indicates the start of a
            // binary data object.
            uint8_t     m_header_id;

            // Header codes 0-7.
            uint8_t     m_header_code_0;
            uint8_t     m_header_code_1;
            uint8_t     m_header_code_2;
            uint8_t     m_header_code_3;
            uint8_t     m_header_code_4;
            uint8_t     m_header_code_5;
            uint8_t     m_header_code_6;
            uint8_t     m_header_code_7;
            uint8_t     m_padding_0;
            uint8_t     m_padding_1;
            uint8_t     m_padding_2;
			
			struct {
				uint8_t m_float_foreign_bytes[4];
			} u_vel;

			struct {
				uint8_t m_float_foreign_bytes[4];
			} u_fvel;

			struct {
				uint8_t m_float_foreign_bytes[4];
			} u_acc_dec;

            union {
                // For 32-bit signed integer ("LONG") parameter values, data
                // is received into this structure, low byte first.
                struct {
                    uint8_t m_long_foreign_bytes[4];
                } u_NURB_Spline_for_slave_0_long;

                // For 32-bit IEEE floating-point ("FP32") parameter values, data
                // is received into this structure, low byte first.
                struct {
                    uint8_t m_float_foreign_bytes[4];
                } u_NURB_Spline_for_slave_0_fp32;
            };

            union {
                // For 32-bit signed integer ("LONG") parameter values, data
                // is received into this structure, low byte first.
                struct {
                    uint8_t m_long_foreign_bytes[4];
                } u_NURB_Spline_for_slave_1_long;

                // For 32-bit IEEE floating-point ("FP32") parameter values, data
                // is received into this structure, low byte first.
                struct {
                    uint8_t m_float_foreign_bytes[4];
                } u_NURB_Spline_for_slave_1_fp32;
            };

            union {
                // For 32-bit signed integer ("LONG") parameter values, data
                // is received into this structure, low byte first.
                struct {
                    uint8_t m_long_foreign_bytes[4];
                } u_primary_center_long;

                // For 32-bit IEEE floating-point ("FP32") parameter values, data
                // is received into this structure, low byte first.
                struct {
                    uint8_t m_float_foreign_bytes[4];
                } u_primary_center_fp32;
            };

            union {
                // For 32-bit signed integer ("LONG") parameter values, data
                // is received into this structure, low byte first.
                struct {
                    uint8_t m_long_foreign_bytes[4];
                } u_secondary_center_long;

                // For 32-bit IEEE floating-point ("FP32") parameter values, data
                // is received into this structure, low byte first.
                struct {
                    uint8_t m_float_foreign_bytes[4];
                } u_secondary_center_fp32;
            };

        } binary_move_command_request_t;


        // Constructor
        ACR7000(MotionServices &motionServicesP, const std::string &nameP, const std::string &network_addressP);

        // Destructor
        virtual ~ACR7000();

        /** Initialize the controller, providing a list of transports and axes.
         *  For the ACR7000, two TCP connections (ASCII and binary) are created at this point.
         * 
         *  THROWS:
         *  - TimeoutExpiredException if the timeout expires.  The actual delay may be longer than the timeout.
         * 
         *  - ConnectionFailedException if either or both of the connections cannot be made.
         */
        virtual void init(std::vector<Transport*> *transportsP, std::vector<MotionAxis*> *axesP, int timeout_msP);
        virtual void shutdown(int timeout_msP);

        virtual void energize(int timeout_msP);
        virtual void deenergize(int timeout_msP);

        /** Enables a motor power channel.  Normally this is used indirectly, by using MotionAxis::enable().
         */
        virtual void enableChannel(int channelIndexP);
        virtual void disableChannel(int channelIndexP);

        void reboot(unsigned int timeout_msP);

		// Sends an ASCII command in the form of "ACC # DEC # STP # VEL #"
		size_t setVelAccelDecelStop( float velocity, float acceleration, float deceleration, float stop );

		// Sends an ASCII command in the form of "JRK #"
		void setJrk( float jerk );

		// Sends an ASCII command in the form of "HLDEC #"
		void setHwLimitDecel( std::string axisName, float hwLimitDecel );

		// Sends a reset command to set the axis position to 0 (without moving it).
		void resetAxis( std::string axisNameP );

		// Sets the in-position-band (IPB) for an axis.
		void setIPB(int axisIndex, double inPositionBandP);

        // Moves the controller to an absolute position.
        // The move is accomplished by sending a command to the ASCII command channel.
        void absoluteMove( std::string axisNameP, double positionP );

        // Concurrently moves two controller axes to absolute positions at their configured speeds.
        // This is not a coordinated move. The two axes will not necessarily finish their moves at the same time.
        // Both need to be watched to confirm successful completion.
        // The move is accomplished by sending a command to the ASCII command channel.
        void concurrentAbsoluteMove( std::string axisAlphaNameP, double positionAlphaP, std::string axisBetaNameP, double positionBetaP );

		void doubleToString( double D, std::string & str );
		/// Commands a swirl using the BHI
//		void swirl( std::string axisAlphaNameP, double swirlSpeedP, double swirlRadiusP, double swirlCountP );
		/// Commands a swirl using the ASCII interface
		void swirl( std::string axisANameP,
					std::string axisBNameP,
					double startAP,
					double startBP,
					double centerAP,
					double centerBP,
					int swirlCountP );

        // Requests a signed 32-bit integer P-Value from the controller.
        // The retrieved integer is converted to the embedded system's native byte order.
        // The function name and implementation follows from the ACR Programmer's Guide (p.243).
        // This is an asynchronous function-- the request is queued to be sent immediately,
        // but the caller must check the latches for a response using inspect().
        void binaryGetLong(pvalue_index_t pvalueIndexP);

        // Requests a 32-bit floating-point P-Value from the controller.
        // The function name and implementation follows from the ACR Programmer's Guide.
        // This is an asynchronous function-- the request is queued to be sent immediately,
        // but the caller must check the latches for a response using inspect().
        void binaryGetFloat(pvalue_index_t pvalueIndexP);

        // Sets a signed 32-bit integer P-Value on the controller.
        // The retrieved integer is converted to the embedded system's native byte order.
        // The function name and implementation follows from the ACR Programmer's Guide (p.243).
        // This is an asynchronous function-- the order is queued to be sent immediately,
        // but the caller must query the latches to verify the operation has taken effect.
        void binarySetLong(pvalue_index_t pvalueIndexP, int32_t valueP);

        // Sets a 32-bit floating-point P-Value on the controller.
        // The function name and implementation follows from the ACR Programmer's Guide.
        // This is an asynchronous function-- the order is queued to be sent immediately,
        // but the caller must query the latches to verify the operation has taken effect.
        void binarySetFloat(pvalue_index_t pvalueIndexP, float valueP);

        // Begins, or changes the interval of, background monitoring for a given P-value on the
        // binary communications channel.  Note that the interval, specified in milliseconds,
        // is the minimum amount of time that will elapse between requests.  If a large number
        // of parameters are being monitored, this timing is not guaranteed.
        //
        // CAUTION!
        // Because the monitoring thread sleeps for 250ms if it has no other work to do,
        // the first inquiry for a particular PValue can take up to 250ms.  Subsequent
        // inquiries should happen closer to the desired interval time.
        void monitor(pvalue_index_t pvalueIndexP, pvalue_type_t pvalueTypeP, uint32_t interval_msP);

        // Cancels monitoring of a given P-value.
        void cancelMonitoring(pvalue_index_t pvalueIndexP);

        // Inspects the last received response for a monitored 32-bit floating-point
        // (FP32) value.  Returns the value, and also assigns the monotonic timestamp
        // at which the value was received to whenR.
        //
        // EXCEPTIONS THROWN:
        // - PValueNotMonitoredException
        //      If the value is not being monitored.
        // 
        // - PValuePendingException
        //      If the value is being monitored, but no response has yet been received.
        //
        // - PValueWrongFormatException
        //      If the value is being monitored as a long, not a float, and you should
        //      have used inspectPValueLong instead.
        float inspectPValueFloat(pvalue_index_t pvalueIndexP, struct timespec &whenR);

        // Inspects the last received response for a monitored 32-bit signed integer
        // (LONG) value.
        //
        // EXCEPTIONS THROWN:
        // - PValueNotMonitoredException
        //      If the value is not being monitored.
        // 
        // - PValuePendingException
        //      If the value is being monitored, but no response has yet been received.
        //
        // - PValueWrongDataTypeException
        //      If the value is being monitored as a float, not a long, and you should
        //      have used inspectPValueFloat instead.
        int32_t inspectPValueLong(pvalue_index_t pvalueIndexP, struct timespec &whenR);

        /** Orders the controller to run a program, specified by index.
         *  The program begins running in the background immediately.
         *  If listenP is true, the "lrun" modifier is used to cause the program's
         *  PRINT output to go the ASCII receive buffer, where it will make its way
         *  into the log, etc.
         */
        void runProgram(int indexP, bool listenP);
		
		/** Use these in place of runProgram when you want to log movement progress
		 *  during the running of the program.
		 */
		void launchProgram(int indexP, bool listenP);
		bool programIsRunning(int indexP);
		float getProgramStatus(int indexP);

		/// For accessing individual bits of Master-Flags
		void clearMasterFlag( int bitIndex, int masterId );
		void setMasterFlag( int bitIndex, int masterId );
		bool getMasterFlag( int bitIndex, int masterId );
		
		/// Internal support functions for accessing individual bits of Flags
		void clearOneFlag( int firstBit, int bitIndex, int axisNumber );
		void setOneFlag( int firstBit, int bitIndex, int axisNumber );
		bool getOneFlag( pvalue_index_t * pValueAddressArray, int firstBit, int bitIndex, int axisNumber );
		
		/// For accessing individual bits of Axis Flags
		void clearAxisFlag( int bitIndex, int axisNumber );
		void setAxisFlag( int bitIndex, int axisNumber );
		bool getAxisFlag( int bitIndex, int axisNumber );
		
		/// For accessing individual bits of Secondary-Axis-Flags
		void clearSecondaryAxisFlag( int bitIndex, int axisNumber );
		void setSecondaryAxisFlag( int bitIndex, int axisNumber );
		bool getSecondaryAxisFlag( int bitIndex, int axisNumber );
		
		/// For accessing individual bits of Quaternary-Axis-Flags
		void clearQuaternaryAxisFlag( int bitIndex, int axisNumber );
		void setQuaternaryAxisFlag( int bitIndex, int axisNumber );
		bool getQuaternaryAxisFlag( int bitIndex, int axisNumber );
		bool getKillAllMotionRequestFlag( std::string axisNameP );

		/// For accessing individual bits of all Quaternary-Axis-Flags associated with a master.
//		void clearBitsOnMaster( int bitIndex, int masterId );
		void clearQuaternaryAxisFlagsOfMaster( int bitIndex, int masterId );
		void setQuaternaryAxisFlagsOfMaster( int bitIndex, int masterId );
		bool QuaternaryAxisFlagsOfMasterAreClear( int bitIndex, int masterId );
		
		/// For sending an ASCII query to print a value so it gets logged.
		void printActualPositionViaAscii( int axisNumber );

		//void getClearBitsOfMaster( int bitIndex, int masterId, bool & 
        // MDG: Temp method for test purposes.
        void purgeRx(void);

    protected:
        // Static wrapper for ASCII TCP channel exception handler.
        static void onAsciiTCPException_wrapper(ProperCommBase *pLinkP, InstrumentedException &exP, void *pUserDataP);

        // ASCII TCP channel exception handler.
        void onAsciiTCPException(InstrumentedException &exP);

        // Static wrapper for ASCII TCP channel On Receive handler.
        static void onAsciiTCPRecv_wrapper(ProperCommBase *pLinkP, void *pUserDataP);

        // ASCII TCP channel On Receive handler.
        void onAsciiTCPRecv(void);

        // Static wrapper for ASCII TCP channel On Transmission handler.
        static void onAsciiTCPXmit_wrapper(ProperCommBase *pLinkP, void *pUserDataP);

        // ASCII TCP channel On Transmission handler.
        void onAsciiTCPXmit(void);

        // Static wrapper for binary TCP channel exception handler.
        static void onBinaryTCPException_wrapper(ProperCommBase *pLinkP, InstrumentedException &exP, void *pUserDataP);

        // Binary TCP channel exception handler.
        void onBinaryTCPException(InstrumentedException &exP);

        // Static wrapper for binary TCP channel On Receive handler.
        static void onBinaryTCPRecv_wrapper(ProperCommBase *pLinkP, void *pUserDataP);

        // Binary TCP channel On Receive handler.
        void onBinaryTCPRecv(void);

        // Static wrapper for binary TCP channel On Transmission handler.
        static void onBinaryTCPXmit_wrapper(ProperCommBase *pLinkP, void *pUserDataP);

        // Binary TCP channel On Transmission handler.
        void onBinaryTCPXmit(void);

        // Sends an asynchronous request for a data packet to the ACR7000.
        // Data packets may return multiple fields at once, as opposed to requests for
        // individual field values.  The asynchronous response copies the header for the
        // packet, allowing the parser to latch it correctly.
        void requestPacket(void);

        // Check to see if enough data has been received for a binary data packet,
        // peek at it if it has to see if it seems to be of a known type, and if
        // everything looks good, parse the packet.
        void parsePendingBinaryRecv(void);

        // Process a multi-parameter binary data packet response from the parser's
        // staging buffer.
        // 
        // The object must be locked prior to making this call!
        void processBinaryDataPacket(void);

        // Process a 32-bit Long Integer PValue request response from the parser's
        // staging buffer.
        // 
        // The object must be locked prior to making this call!
        void processBinaryGetLongResponse(void);

        // Process a 32-bit IEEE Floating-Point PValue request response from the parser's
        // staging buffer.
        // 
        // The object must be locked prior to making this call!
        void processBinaryGetIEEEResponse(void);

        // Static wrapper for pvalue polling thread.
        static void *pvaluePollingThreadWrapper(void *pDataP);

        // PValue polling thread core.
        void pvaluePollingThreadCore(void);
        
        // === CLASS STATIC DATA ===


        // === DATA ===

        // Controller network address.
        const std::string           m_network_address;

        // Controller's ASCII communications TCP port number.
        int                         m_ascii_tcp_port_number;

        // Controller's binary communications TCP port number
        int                         m_binary_tcp_port_number;

        // TCP connection for ASCII communication.
        TCPLink                     m_ascii_link;

        // TCP connection for binary communication.
        TCPLink                     m_binary_link;

        // P-value request latches.  This list stores the most recent requests made
        // for any given P-value, and the most recent responses received, along with
        // timestamps for both the request and the response.
        std::vector<pvalue_request_latch_t*> m_pvalue_request_latches;

        // Flag to indicate that the polling thread should shut down.
        bool                m_pvalue_polling_thread_want_shutdown;

        // Flag to indicate that the polling thread has in fact stopped.
        bool                m_pvalue_polling_thread_running;

        // Worker thread for monitoring binary channel (P-Value query/catch).
        pthread_t           m_pvalue_polling_thread;

        // Receive parser staging buffer.
        uint8_t             m_parser_staging_buf[ACR7000_MAX_RESPONSE];
        int                 m_parser_bytes_staged;
    };

}

#endif

