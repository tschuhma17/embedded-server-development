/* src/controllers/ACR7000.cpp */

#include <iostream>

#include <sstream>
#include <string>
#include <vector>

#include <unistd.h>

#include "USAFW/exceptions/LogicExceptions.hpp"
#include "USAFW/ProperComms/TCPLink.hpp"
#include "USAFW/util/math.hpp"
#include "USAFW/util/stringutils.hpp"

#include "MotionServices.hpp"
#include "exceptions/ControllerException.hpp"
#include "controllers/ACR7000.hpp"

#define ACR7000_DEFAULT_ASCII_PORT  (5002)
#define ACR7000_DEFAULT_BINARY_PORT (5006)
#define MAX_PROGRAM_WAIT_FOR_START_LOOPS (50)
#define MAX_PROGRAM_WAIT_FOR_END_LOOPS   (50000)

namespace USAFW {

    // Constructor
    ACR7000::ACR7000(MotionServices &motionServicesP, const std::string &nameP, const std::string &networkAddressP) :
        MotionController(motionServicesP, nameP),
        m_network_address(networkAddressP),
        m_ascii_tcp_port_number(ACR7000_DEFAULT_ASCII_PORT),
        m_binary_tcp_port_number(ACR7000_DEFAULT_BINARY_PORT),
        m_ascii_link(motionServicesP.getLogger(),
                        nameP+"_ascii_tcp",
                        ACR7000::onAsciiTCPException_wrapper, this,
                        ACR7000::onAsciiTCPRecv_wrapper, this,
                        ACR7000::onAsciiTCPXmit_wrapper, this,
                        65536,
                        65536),
        m_binary_link(motionServicesP.getLogger(),
                        nameP+"_binary_tcp",
                        ACR7000::onBinaryTCPException_wrapper, this,
                        ACR7000::onBinaryTCPRecv_wrapper, this,
                        ACR7000::onBinaryTCPXmit_wrapper, this,
                        65536,
                        65536),
        m_pvalue_polling_thread_running(false),
        m_pvalue_polling_thread_want_shutdown(false),
        m_pvalue_polling_thread(getInvalidPthread()),
        m_parser_staging_buf{0},
        m_parser_bytes_staged(0)
    {
        m_motion_services.getLogger().info("Constructing ACR7000 instance for controller \""+nameP+"\"...");
    }

    // Destructor
    ACR7000::~ACR7000() {
    }


    void ACR7000::init(std::vector<Transport*> *pTransportsP, std::vector<MotionAxis*> *pAxesP, int timeout_msP)
    {
        MotionController::init(pTransportsP, pAxesP, timeout_msP);

        try {
            // Create the ASCII connection to the unit.
            m_motion_services.getLogger().info("Initializing ASCII TCP communication with ACR7000 controller \""+m_name+"\"...");
            std::string full_url;
            full_url = "tcp://" + m_network_address + ":" + std::to_string(m_ascii_tcp_port_number);
            m_ascii_link.open(full_url);
            m_motion_services.getLogger().debug("...connected.");
        } catch(...) {
            m_motion_services.getLogger().fatal("Unable to connect to ACR7000 controller (ASCII)\""
                +m_name+"\" (" + m_network_address + ":" + std::to_string(m_ascii_tcp_port_number) + ")");
            exit(-1);
        }

        try {
            m_motion_services.getLogger().info("Initializing binary TCP communication with ACR7000 controller \""+m_name+"\"...");
            std::string full_url;
            full_url = "tcp://" + m_network_address + ":" + std::to_string(m_binary_tcp_port_number);
            m_binary_link.open(full_url);
            m_motion_services.getLogger().debug("...connected.");
        } catch(...) {
            m_motion_services.getLogger().fatal("Unable to connect to ACR7000 controller (binary)\""
                +m_name+"\" (" + m_network_address + ":" + std::to_string(m_binary_tcp_port_number) + ")");
            exit(-1);
        }
      
        // Clear cruft and sync up.
        m_ascii_link.write(LITSTR("\r\n\r\n\r\n"));
        // (nothing to send to clear the binary interface)
        // Wait 250ms.
        usleep(250000);

        // Just discard the receiving buffers.
        m_ascii_link.purgeRx();
        m_binary_link.purgeRx();

        // Set the default context to Program 0.
        m_ascii_link.write(LITSTR("PROG 0\r\n"));

        // Clear the "Associated slave kill all motion requests".
        // This was MADDENING to figure out.  What that message means, near as I can
        // tell, is that another motor with the same master has a "kill all motion"
        // request active.  ALL OF THOSE are turned ON *WHENEVER ANY TCP CONNECTION
        // IS DROPPED*.
        //
        // So, they all have to be cleared.
/*
        // Clear Kill All Motion Request for Quaternary Axis 0
        m_ascii_link.write(LITSTR("CLR 8467\r\n"));

        // Clear Kill All Motion Request for Quaternary Axis 1
        m_ascii_link.write(LITSTR("CLR 8499\r\n"));

        // Clear Kill All Motion Request for Quaternary Axis 2
        m_ascii_link.write(LITSTR("CLR 8531\r\n"));

        // Clear Kill All Motion Request for Quaternary Axis 3
        m_ascii_link.write(LITSTR("CLR 8563\r\n"));

        // Clear Kill All Motion Request for Quaternary Axis 4
        m_ascii_link.write(LITSTR("CLR 8595\r\n"));

        // Clear Kill All Motion Request for Quaternary Axis 5
        m_ascii_link.write(LITSTR("CLR 8627\r\n"));

        // Clear Kill All Motion Request for Quaternary Axis 6
        m_ascii_link.write(LITSTR("CLR 8659\r\n"));

        // Clear Kill All Motion Request for Quaternary Axis 7
        m_ascii_link.write(LITSTR("CLR 8691\r\n"));

        // Likewise, clear the "Kill All Moves" flags.

        // Clear Kill All Moves flag for Master 0.
        m_ascii_link.write(LITSTR("CLR 522\r\n"));

        // Clear Kill All Moves flag for Master 1.
        m_ascii_link.write(LITSTR("CLR 554\r\n"));

        // Clear Kill All Moves flag for Master 2.
        m_ascii_link.write(LITSTR("CLR 586\r\n"));

        // Clear Kill All Moves flag for Master 3.
        m_ascii_link.write(LITSTR("CLR 618\r\n"));

        // Clear Kill All Moves flag for Master 4.
        m_ascii_link.write(LITSTR("CLR 650\r\n"));

        // Clear Kill All Moves flag for Master 5.
        m_ascii_link.write(LITSTR("CLR 682\r\n"));

        // Clear Kill All Moves flag for Master 6.
        m_ascii_link.write(LITSTR("CLR 714\r\n"));

        // Clear Kill All Moves flag for Master 7.
        m_ascii_link.write(LITSTR("CLR 746\r\n"));

        sleep(1);
*/
        // Enable all drives.  This apparently must be done by name.  Note that this
        // is distinct from the axis channel enablement, which must be done by index
        // (see enableChannel).
        int axis_index=0;
        for (std::vector<MotionAxis *>::iterator axis_iter = p_axes->begin();
                axis_iter!=p_axes->end();
                axis_iter++, axis_index++)
        {
/*
            std::string cmd = "DRIVE ON "+(*axis_iter)->getName()+"\r\n";
            m_ascii_link.write(cmd.c_str(), strlen(cmd.c_str()));
*/
            // Monitor current position for all axes.
//            monitor(AR7xT_pvaddrs_axis_current_position[axis_index], pvalue_type_long, 100);
            monitor(AR7xT_pvaddrs_axis_actual_position[axis_index], pvalue_type_long, 100);

            // Monitor current velocity for all axes.
            monitor(AR7xT_pvaddrs_axis_velocity[axis_index], pvalue_type_float, 100);

            // Monitor master flags for all axes.
            monitor(AR7xT_pvaddrs_axis_master_flags[(*axis_iter)->getMasterIndex()], pvalue_type_long, 100);

            // Monitor axis flags for all axes.
            monitor(AR7xT_pvaddrs_axis_flags[axis_index], pvalue_type_long, 100);

			// Monitor program flags for the controller. By convention, only 0 is used.
			monitor(AR7xT_pvaddrs_program_flags[0], pvalue_type_long, 100);

            // Monitor quaternary axis flags for all axes.
            monitor(AR7xT_pvaddrs_quaternary_axis_flags[axis_index], pvalue_type_long, 100);

            // Monitor servo-drive-status-2 flags, but only for controllers of type ACR7xV.
			// We don't know what kind of controller we are talking to within the scope of
			// this function, so we don't know whether this axis is for steppers or servos.
			// We'll try montitoring for these values and hope that stepper motors gracefully
			// fail to send us any data.
			monitor(AR7xV_pvaddrs_servo_drive_status_2_flags[axis_index], pvalue_type_long, 100);

            // Monitor quinary axis flags for all axes.
            monitor(AR7xT_pvaddrs_quinary_axis_flags[axis_index], pvalue_type_long, 100);

            // Monitor PPU values for all axes.
            monitor(AR7xT_pvaddrs_axis_PPU_term[axis_index], pvalue_type_float, 100);
        }

        // Start pvalue monitoring on the binary channel.
        pthread_attr_t attrs;
        pthread_attr_init(&attrs);
        pthread_create(&m_pvalue_polling_thread, &attrs, pvaluePollingThreadWrapper, this);

		usleep(10000);
		/// Collect values PPU static values once. Then stop poling for them.
        m_motion_services.getLogger().debug("Collecting PPU values");
        for (std::vector<MotionAxis *>::iterator axis_iter = p_axes->begin();
                axis_iter!=p_axes->end();
                axis_iter++, axis_index++)
        {
			double ppu = 0.0;
			while (ppu == 0.0)
			{
				try {
					ppu = (*axis_iter)->getPPU();
				} catch (PValuePendingException &ex) {
					ppu = 0.0;
					m_motion_services.getLogger().debug("awaiting PPU values");
					usleep(10000);
				}
			}
            m_motion_services.getLogger().info("PPU of axis " + (*axis_iter)->getName() + " is " + std::to_string(ppu));
		}

		usleep(10000);
        for (std::vector<MotionAxis *>::iterator axis_iter = p_axes->begin();
                axis_iter!=p_axes->end();
                axis_iter++, axis_index++)
        {
            // PPU values for all axes.
            cancelMonitoring(AR7xT_pvaddrs_axis_PPU_term[axis_index]);
		}
		
    }

    void ACR7000::shutdown(int timeout_msP)
    {
        int axis_index=0;
        for (std::vector<MotionAxis *>::iterator axis_iter = p_axes->begin();
                axis_iter!=p_axes->end();
                axis_iter++, axis_index++)
        {
            m_motion_services.getLogger().debug("Shutting down DRIVE " + (*axis_iter)->getName());
            std::string cmd = "DRIVE OFF "+(*axis_iter)->getName()+"\r\n";
            m_ascii_link.write(cmd.c_str(), strlen(cmd.c_str()));

            // current position for all axes.
//            cancelMonitoring(AR7xT_pvaddrs_axis_current_position[axis_index]);
            cancelMonitoring(AR7xT_pvaddrs_axis_actual_position[axis_index]);

            // current velocity for all axes.
            cancelMonitoring(AR7xT_pvaddrs_axis_velocity[axis_index]);

            // master flags for all axes.
            cancelMonitoring(AR7xT_pvaddrs_axis_master_flags[(*axis_iter)->getMasterIndex()]);

            // axis flags for all axes.
            cancelMonitoring(AR7xT_pvaddrs_axis_flags[axis_index]);

			// Monitor program flags for the controller. By convention, only 0 is used.
			cancelMonitoring(AR7xT_pvaddrs_program_flags[0]);

            // quaternary axis flags for all axes.
            cancelMonitoring(AR7xT_pvaddrs_quaternary_axis_flags[axis_index]);

            // PPU values for all axes.
            cancelMonitoring(AR7xT_pvaddrs_axis_PPU_term[axis_index]);
        }
        
        m_pvalue_polling_thread_want_shutdown = true;
        void * retval;
        pthread_join(m_pvalue_polling_thread, &retval);
	}

    // Energize all drives.  This apparently must be done by name.  Note that this
    // is distinct from the axis channel enablement, which must be done by index
    // (see enableChannel).
    void ACR7000::energize(int timeout_msP)
    {
        int axis_index=0;
        for (std::vector<MotionAxis *>::iterator axis_iter = p_axes->begin();
                axis_iter!=p_axes->end();
                axis_iter++, axis_index++)
        {
            // Monitor master flags for all axes.
            monitor(AR7xT_pvaddrs_axis_master_flags[axis_index], pvalue_type_long, 100);
            // Monitor quaternary axis flags for all axes.
            monitor(AR7xT_pvaddrs_quaternary_axis_flags[axis_index], pvalue_type_long, 100);

			struct timespec when;
			long masterFlags = inspectPValueLong(AR7xT_pvaddrs_axis_master_flags[axis_index], when);
			long qFlags = inspectPValueLong(AR7xT_pvaddrs_quaternary_axis_flags[axis_index], when);
			m_motion_services.getLogger().info("ACR7000::energize masterFlags=" + std::to_string(masterFlags));
			m_motion_services.getLogger().info("ACR7000::energize quaternaryFlags=" + std::to_string(qFlags));

			bool isMoving = ((masterFlags & MASK_IN_MOTION) == MASK_IN_MOTION);
			bool isEnabled = ((qFlags & MASK_DRIVE_ENABLE_OUT) == MASK_DRIVE_ENABLE_OUT);
			bool isPhysicalDriveEnabled = ((qFlags & MASK_PHYS_DRIVE_ENABLE) == MASK_PHYS_DRIVE_ENABLE);
			
			if (isMoving)
			{
				m_motion_services.getLogger().info("ACR7000::energize is moving");
			}
			if (isEnabled)
			{
				m_motion_services.getLogger().info("ACR7000::energize is enabled");
			}
			if (isPhysicalDriveEnabled)
			{
				m_motion_services.getLogger().info("ACR7000::energize is 'physically' enabled");
			}

			// Check enabled state and command 'DRIVE ON' if not already on.
			if (isMoving || isEnabled || isPhysicalDriveEnabled)
			{
				m_motion_services.getLogger().info("ACR7000::energize skipping because drive is moving or enabled");
			}
			else
			{
				m_motion_services.getLogger().info("Sending 'DRIVE ON "+(*axis_iter)->getName()+"'");
				std::string cmd = "DRIVE ON "+(*axis_iter)->getName()+"\r\n";
				m_ascii_link.write(cmd.c_str(), strlen(cmd.c_str()));
			}

			// Check enabled state to confirm it is on.
			long loopCount = 0L;
			while(loopCount < 100)
			{
                qFlags = inspectPValueLong(AR7xT_pvaddrs_quaternary_axis_flags[axis_index], when);
                if ((qFlags & MASK_DRIVE_ENABLE_OUT) == MASK_DRIVE_ENABLE_OUT)
                {
					m_motion_services.getLogger().info("drive enabled");
					break;
				}
				usleep(100000);
				loopCount++;
			}
			if (loopCount >= 100)
			{
				m_motion_services.getLogger().debug("drive not enabled; timed out");
			}
        }
	}

    void ACR7000::deenergize(int timeout_msP)
    {
        m_ascii_link.write(LITSTR("PROG 0\r\n"));

        int axis_index=0;
        for (std::vector<MotionAxis *>::iterator axis_iter = p_axes->begin();
                axis_iter!=p_axes->end();
                axis_iter++, axis_index++)
        {
            m_motion_services.getLogger().info("Sending 'DRIVE OFF "+(*axis_iter)->getName()+"'");
            std::string cmd = "DRIVE OFF "+(*axis_iter)->getName()+"\r\n";
            m_ascii_link.write(cmd.c_str(), strlen(cmd.c_str()));
        }
	}

	// Sends an ASCII command in the form of "ACC # DEC # STP # VEL #"
	size_t ACR7000::setVelAccelDecelStop( float velocity, float acceleration, float deceleration, float stop )
	{
        std::string command("\r\nACC " + std::to_string(acceleration) +
							" DEC " + std::to_string(deceleration) + " STP " + std::to_string(stop) +
							" VEL " + std::to_string(velocity) + "\r\n");
        return m_ascii_link.write(command.c_str(), strlen(command.c_str()));
	}

	// Sends an ASCII command in the form of "JRK #"
	void ACR7000::setJrk( float jerk )
	{
        std::string command("\r\nJRK " + std::to_string(jerk) + "\r\n");
        m_ascii_link.write(command.c_str(), strlen(command.c_str()));
	}

	// Sends an ASCII command in the form of "HLDEC #"
	void ACR7000::setHwLimitDecel( std::string axisName, float hwLimitDecel ) {
        std::string command("HlDEC " + axisName  + std::to_string(hwLimitDecel) + "\r\n");
        m_ascii_link.write(command.c_str(), strlen(command.c_str()));
	}

	// Sends a reset command to set the axis position to 0 (without moving it).
	void ACR7000::resetAxis( std::string axisNameP )
	{
		std::string command( "RES " + axisNameP + "\r\n" );
        m_ascii_link.write(command.c_str(), strlen(command.c_str()));
	}

	// Sets the in-position-band (IPB) for an axis.
    void ACR7000::setIPB(int axisIndex, double inPositionBandP) {
        std::ostringstream ipb;
        ipb.precision(5);
        ipb << std::fixed << inPositionBandP;

		/// Example command: 'AXIS0 IPB (0.50000)'
        std::string command("AXIS" + std::to_string(axisIndex) + " IPB (" + ipb.str() + ")\r\n");
        m_ascii_link.write(command.c_str(), strlen(command.c_str()));
    }

    // Moves the controller to an absolute position.
    // The move is accomplished by sending a command to the ASCII command channel.
    void ACR7000::absoluteMove(std::string axisNameP, double positionP) {

        std::ostringstream out;
        out.precision(5);
        out << std::fixed << positionP;

		// Let's assume that setVelAccelDecelStop always gets called before this, so there is no need to send "PROG0" again.
        // std::string command("\r\nPROG0\r\n"+axisNameP + out.str() + "\r\n");
        std::string command(axisNameP + out.str() + "\r\n");
        m_ascii_link.write(command.c_str(), strlen(command.c_str()));
    }

	// Concurrently moves two controller axes to absolute positions at their configured speeds.
	// This is not a coordinated move. The two axes will not necessarily finish their moves at the same time.
	// Both need to be watched to confirm successful completion.
	// The move is accomplished by sending a command to the ASCII command channel.
	void ACR7000::concurrentAbsoluteMove(std::string axisAlphaNameP, double positionAlphaP, std::string axisBetaNameP, double positionBetaP) {
        m_motion_services.getLogger().info(std::string("ACR7000::concurrentAbsoluteMove() called"));
        std::ostringstream outAlpha;
        outAlpha.precision(5);
        outAlpha << std::fixed << positionAlphaP;

        std::ostringstream outBeta;
        outBeta.precision(5);
        outBeta << std::fixed << positionBetaP;

		// Let's assume that setVelAccelDecelStop always gets called before this, so there is no need to send "PROG0" again.
        // std::string command("\r\nPROG0\r\n" + axisAlphaNameP + outAlpha.str() + " " + axisBetaNameP + outBeta.str() +"\r\n");
        std::string command(axisAlphaNameP + outAlpha.str() + " " + axisBetaNameP + outBeta.str() +"\r\n");
        m_ascii_link.write(command.c_str(), strlen(command.c_str()));
	}

	/// Use the Binary Host Interface to command an "arc move".
	/// Per PMM_Users_Guide, Binary Host Interface--Binary Move Command:
	// A binary move consists of a variable length header followed by a number of four-byte data fields.
	// The bit-mapped information in the header determines the number of data fields and their content.
	// All data fields are sent low order byte first.
	///
	// There are two versions defined for Header Code 0 based on Secondary Master Flag Bit Index 5, Enable Rapid Move Modes.
	/// (I will assume, for now, that Rapid Move Modes is enabled. I'll confirm that later. -MDG)
	// --------------------------------------------------------------------------
	// Header Code 0 (assuming that Enable_Rapid_Move_Modes flag is disabled (cleared)
	// Data Field	Data Type					Description
	// Bit 0		FVEL Lockout				Forces FVEL to zero for this move
	// Bit 1		FOV Lockout					Forces FOV to 1.0 for this move
	// Bit 2		STP Ramp Activate			Sets STP equal to DEC, else STP 0
	// Bit 3		Code 3 Present				Header contains "Header Code 3"
	// Bit 4		Velocity Data Present		Packet contains master VEL
	// Bit 5		Acceleration Data Present	Packet contains master ACC/DEC
	// Bit 6		Counter Dir					Count down if set, else up
	// Bit 7		Counter Mode				Master move counter enable
	// --------------------------------------------------------------------------
	// Header Code 0 (assuming that Enable_Rapid_Move_Modes flag is enabled (set)
	// Data Field	Data Type					Description
	// Bit 0		Move Mode Bit 1				Selects the move mode for this move along with Header Code 0 Bit 2.
	// Bit 1		FOV/ROV Lockout				Forces FOV or ROV to 1.0 for this move
	// Bit 2		Move Mode Bit 0				Selects the move mode for this move along with Header Code Bit 0.
	// Bit 3		Code 3 Present				Header contains "Header Code 3"
	// Bit 4		Velocity Data Present		Packet contains master VEL
	// Bit 5		Acceleration Data Present	Packet contains master ACC/DEC
	// Bit 6		Counter Dir					Count down if set, else up
	// Bit 7		Counter Mode				Master move counter enable
	// --------------------------------------------------------------------------
	// Header Code 1
	// Data Field	Data Type					Description
	// Bit 0-2		Master Bits 0-2				Master for this move packet
	// Bit 3		Interrupt Select			Interrupt host when move starts
	// Bit 4		Arc Direction				CCW if set, else CW
	// Bit 5		Arc Mode					Packet contains center points or Spline Knot present
	// Bit 6		Arc Plane Bit 0				Primary and secondary axis or NURB Mode
	// Bit 7		Arc Plane Bit 1				For binary arc move commands or SPLINE Mode
	// --------------------------------------------------------------------------
	// Header Code 2
	// Data Field	Data Type					Description
	// Bit 0-7		Slave 0-7					Present Slave target positions to be contained in this move packet
	// --------------------------------------------------------------------------
	// Header Code 3
	// Data Field	Data Type					Description
	// Bit 0	Incremental Target				Target positions are incremental
	// Bit 1	Incremental Center				Center points are incremental
	// Bit 2	Floating Point Data				Targets and centers are IEEE32
	// Bit 3	Arc Radius Scaling				Packet contains radius scaling / NURB/Spline
	// Bit 4	FVEL Data Present				Packet contains master FVEL
	// Bit 5	Block Skip Check				Sets the master Block Skip Check
	// Bit 6	NURB or Spline					Move data packet for NURB or Spline Interpolation
	// Bit 7	Extended Codes					Extended codes 4,5,6 and 7 are present. This bit should be set if DBCB is used
	// --------------------------------------------------------------------------
	// Header Code 4
	// Data Field	Data Type					Description
	// Bit 0-2		Reserved					Reserved
	// Bit 3		Master Bit 3				Master for this move packet
	// Bit 4-7		Reserved					Reserved
	// --------------------------------------------------------------------------
	// Header Code 5
	// Data Field	Data Type					Description
	// Bit 0-7		Reserved					Reserved
	// --------------------------------------------------------------------------
	// Header Code 6
	// Data Field	Data Type					Description
	// Bit 0-7		Slave 8-15 Present			Slave target positions to be contained in this move packet
	// --------------------------------------------------------------------------
	// Header Code 7
	// Data Field	Data Type					Description
	// Bit 0-7		Reserved					Reserved
//	void ACR7000::swirl( std::string axisAlphaNameP, double swirlSpeedP, double swirlRadiusP, double swirlCountP ) {
		// Header Code 0
		// Assume we want Move-Mode-0 (Feed Continuous) Move Mode Bits = 00
		// Assume FOV/ROV Lockout = 0
		// Assume we need header code 3. Code 3 Present = 1.
		// Let's make Velocity Data Present = 1. We'll send Master VEL in data 00.
		// Let's make Acceleration Data Present = 1. We'll send Master ACC/DEC in data 02.
		// Header Code 1
		// Master Bit can only be 0 now, but for future expanstion, maybe it should be passed in? Master Bits = 000
		// Assume arc direction can be arbitrarily fixed (CW). Arc Direction = 0
		// Header Code 2
		// At launch, we only support Y,X as the slaves. Maybe these should be passed in? Save 0 Present = 1; Slave 1 Present = 1
		// All other Save # Present = 0. Hense, Header Code 2 Bits = 00000011.
		// Header Code 3
		// Assume we want to send "Slave 0 Target or NURB/Spline control point" values as floats. Floating Point Data = 1
		// Do we need FVEL? Let's try to get away with FVEL Data Present = 0.
		// Assume we don't need extended codes. Extended Codes = 0.  Maybe this implies no Codes 4-7.
/*
1c 05 08 00
00 88 0a 11		overhead?
04	Header ID
b8	Code 0	1011 1000	Counter-Mode(enabled) Counter-Dir(up) Acceleration-Data-Present(true) Velocity-Data-Present(true) -- Code-3-Present(true)  Move-Mode-3(Continuous) FOV/ROV-Lockout(false)
30	Code 1	0011 0000	1st,2nd-Slave-Axes(0,1) Arc-Mode(true) Arc-Direction(CCW) -- Interrup-Select(false) Master(0)
03	Code 2	0000 0011	Slaves4-7(absent) || Slave3(absent) Slave2(absent) Slave1(present) Slave0(present)
96	Code 3	1001 0110	Extended-Codes(present) NURB-or-Spline(false) Block-Skip-Check(false) || Arc-Radius-Scaling(false) Floating-Point-Data(true) Incremental-Center(true) Incremental-Target(false)
00	Code 4
00	Code 5
00	Code 6
00	Code 7
00 00 00 padding?
00 00 c8 42	= 100	VEL
00 00 00 00 = 0		FVEL
00 00 c8 42 = 100	ACC/DEC
00 00 20 41 = 10	Slave0 END
00 00 00 00 = 0		Slave1 END
00 00 a0 40 = 5		Slave0? Center
00 00 a0 40 = 5		Slave1? Center

00 88 0b 11		overhead?


1c 05 08 00
04 b8 30 03
96 00 00 00
00 00 00 00
00 00 c8 42
00 00 00 00
00 00 c8 42
00 00 20 41
00 00 00 00
00 00 a0 40
00 00 a0 40


1c 05 08 00
04 b8 30 03
96 00 00 00
00 00 00 00
00 00 c8 42
00 00 00 00
00 00 c8 42
00 00 20 41
00 00 00 00
00 00 a0 40
00 00 a0 40

00 88 00 18		overhead?


00001348  00 88 00 10                                        ....
    00002680  00 88 00 10 00 00 00 00                            ........ 

1c 05 08 00

04	Header ID
b8	Code 0	1011 1000	Counter-Mode(enabled) Counter-Dir(up) Acceleration-Data-Present(true) Velocity-Data-Present(true) -- Code-3-Present(true)  Move-Mode-3(Continuous) FOV/ROV-Lockout(false)
20	Code 1	0010 0000	1st,2nd-Slave-Axes(0,1) Arc-Mode(true) Arc-Direction(CW) -- Interrup-Select(false) Master(0)
03	Code 2	0000 0011	Slaves4-7(absent) || Slave3(absent) Slave2(absent) Slave1(present) Slave0(present)
96	Code 3	1001 0110	Extended-Codes(present) NURB-or-Spline(false) Block-Skip-Check(false) || Arc-Radius-Scaling(false) Floating-Point-Data(true) Incremental-Center(true) Incremental-Target(false)
00	Code 4
00	Code 5
00	Code 6
00	Code 7
00 00 00	padding?
00 00 c8 42	= 100	VEL
00 00 00 00 = 0		FVEL
00 00 c8 42 = 100	ACC/DEC
00 00 20 41 = 10	Slave0 END
00 00 00 00 = 0		Slave1 END
00 00 a0 40 = 5		Slave0 Center
00 00 40 40 = 3		Slave1 Center

00 88 00 18


00001468  00 88 00 10                                        ....
    00002868  00 88 00 10 00 00 00 00                            ........ 
0000146C  1c 05 08 00                                        ....
00001470  04 b8 20 03 96 00 00 00  00 00 00 00 00 00 c8 42   .. ..... .......B
00001480  00 00 00 00 00 00 c8 42  00 00 20 41 00 00 00 00   .......B .. A....
00001490  00 00 a0 40 00 00 40 40                            ...@..@@ 
00001498  00 88 00 18                                        ....

00001770  00 88 00 10                                        ....
    00002E20  00 88 00 10 00 00 00 00                            ........ 
00001774  1c 05 08 00                                        ....
00001778  04 b8 20 03 96 00 00 00  00 00 00 00 00 00 c8 42   .. ..... .......B
00001788  00 00 00 00 00 00 c8 42  00 00 20 41 00 00 00 00   .......B .. A....
00001798  00 00 a0 40 00 00 40 40                            ...@..@@ 
000017A0  00 88 04 1b                                        ....

00001A7C  00 88 00 10                                        ....
    000033E0  00 88 00 10 00 00 00 00                            ........ 
00001A80  1c 05 08 00                                        ....
00001A84  04 b8 20 03 96 00 00 00  00 00 00 00 00 00 c8 42   .. ..... .......B
00001A94  00 00 00 00 00 00 c8 42  00 00 20 41 00 00 00 00   .......B .. A....
00001AA4  00 00 a0 40 00 00 40 40                            ...@..@@ 
00001AAC  00 88 00 18                                        ....

*/
//	}

	// TODO: Use this in all functions that are currently doing this conversion.
	void ACR7000::doubleToString( double D, std::string & str )
	{
		std::ostringstream out;
        out.precision(5);
        out << std::fixed << D;
		str = out.str();
	}

	// Concurrently moves two controller axes in a circles swirlCount times.
	// This is a coordinated move.
	// The caller must ensure that the axes are already at the startA and startB positions.
	// The caller must set the velocity and accelerations before calling this.
	// This function starts the move by sending a command to the ASCII command channel.
	// The caller should watch the progress of the move afterward.
	void ACR7000::swirl( std::string axisANameP,
						 std::string axisBNameP,
						 double startAP,
						 double startBP,
						 double centerAP,
						 double centerBP,
						 int swirlCountP )
	{
        m_motion_services.getLogger().info(std::string("ACR7000::swirl() called"));
		std::string startAStr;
		std::string startBStr;
		std::string centerAStr;
		std::string centerBStr;
		doubleToString( startAP, startAStr );
		doubleToString( startBP, startBStr );
		doubleToString( centerAP, centerAStr );
		doubleToString( centerBP, centerBStr );
		std::stringstream oneLoop;
		oneLoop << "CIRCW " << axisANameP << " (" << startAStr << "," << centerAStr << ") ";
		oneLoop << axisBNameP << " (" << startBStr << "," << centerBStr << ")\r\n";
		std::string oneLoopStr = oneLoop.str();

		int loopCount = 0;
		do {
			try {
				m_ascii_link.write(oneLoopStr.c_str(), strlen(oneLoopStr.c_str()));
				loopCount++;
			} catch (std::exception e) {
				m_motion_services.getLogger().info("Unknown exception while swirling");
				usleep(1000);
			}
		} while (loopCount < swirlCountP);
	}


    void ACR7000::binaryGetFloat(pvalue_index_t pvalueIndexP) {
        binary_get_pvalue_request_t request;

        // Header ID: NULL
        request.m_header_id=0x00;

        // Packet ID: 0x8A Binary Get IEEE
        request.m_packet_id=0x8A;

        // PValue index low-order byte
        request.m_pvalue_index_low = pvalueIndexP & 0xFF;

        // PValue index high-order byte
        request.m_pvalue_index_high = (pvalueIndexP>>8) & 0xFF;

        m_binary_link.write(&request, sizeof(binary_get_pvalue_request_t));
    }


    void ACR7000::binaryGetLong(pvalue_index_t pvalueIndexP) {
        binary_get_pvalue_request_t request;

        // Header ID: NULL
        request.m_header_id=0x00;

        // Packet ID: 0x88 Binary Get Long
        request.m_packet_id=packet_id_get_long;

        // PValue index low-order byte
        request.m_pvalue_index_low = pvalueIndexP & 0xFF;

        // PValue index high-order byte
        request.m_pvalue_index_high = (pvalueIndexP>>8) & 0xFF;

        m_binary_link.write(&request, sizeof(binary_get_pvalue_request_t));
    }

    void ACR7000::binarySetFloat(pvalue_index_t pvalueIndexP, float valueP) {
        binary_set_pvalue_request_t request;

        // Header ID: NULL
        request.m_header_id=0x00;

        // Packet ID: 0x8A Binary Get IEEE
        request.m_packet_id=packet_id_set_float;

        // PValue index low-order byte
        request.m_pvalue_index_low = pvalueIndexP & 0xFF;

        // PValue index high-order byte
        request.m_pvalue_index_high = (pvalueIndexP>>8) & 0xFF;

        // Set value bytes.
        uint8_t *float_bytes = reinterpret_cast<uint8_t *>(&valueP);
        request.u_little_endian_fp32.m_float_foreign_bytes[0] = float_bytes[0];
        request.u_little_endian_fp32.m_float_foreign_bytes[1] = float_bytes[1];
        request.u_little_endian_fp32.m_float_foreign_bytes[2] = float_bytes[2];
        request.u_little_endian_fp32.m_float_foreign_bytes[3] = float_bytes[3];

        m_binary_link.write(&request, sizeof(binary_set_pvalue_request_t));
    }


    void ACR7000::binarySetLong(pvalue_index_t pvalueIndexP, int32_t valueP) {
        binary_set_pvalue_request_t request;

        // Header ID: NULL
        request.m_header_id=0x00;

        // Packet ID: 0x88 Binary Set Long
        request.m_packet_id=packet_id_set_long;

        // PValue index low-order byte
        request.m_pvalue_index_low = pvalueIndexP & 0xFF;

        // PValue index high-order byte
        request.m_pvalue_index_high = (pvalueIndexP>>8) & 0xFF;

        // Set value bytes.
        request.u_little_endian_long.m_long_foreign_bytes[0] = valueP       & 0xFF;
        request.u_little_endian_long.m_long_foreign_bytes[1] = (valueP>>8)  & 0xFF;
        request.u_little_endian_long.m_long_foreign_bytes[2] = (valueP>>16) & 0xFF;
        request.u_little_endian_long.m_long_foreign_bytes[3] = (valueP>>24) & 0xFF;

        m_binary_link.write(&request, sizeof(binary_set_pvalue_request_t));
    }

    void ACR7000::monitor(pvalue_index_t pvalueIndexP, pvalue_type_t pvalueTypeP, uint32_t interval_msP) {
        bool found_it=false;
        for (   std::vector<pvalue_request_latch_t*>::iterator iter = m_pvalue_request_latches.begin();
                iter!=m_pvalue_request_latches.end();
                ++iter)
        {
            // Look for a PValue index in a request that matches this response.
            if (pvalueIndexP == (*iter)->m_pvalue_index) {
                // Found it; update the interval.
                (*iter)->m_interval.tv_sec  = (interval_msP/1000);
                (*iter)->m_interval.tv_nsec = (interval_msP%1000)*1000*1000;

                // Re-enable the query in case it was cancelled.
                (*iter)->m_query_type       = pvalue_query_recurring;
                found_it=true;
                break;
            }
        }

        if (found_it) {
			m_motion_services.getLogger().info("ACR7000::monitor already monitoring " + std::to_string(pvalueIndexP));
		}
		else {
            // Build a new PValue latch record and initialize both the request
            // and response fields.
            pvalue_request_latch_t *latch = new pvalue_request_latch_t();
            latch->m_interval.tv_sec            = (interval_msP/1000);
            latch->m_interval.tv_nsec           = (interval_msP%1000)*1000*1000;
            latch->m_latch_valid                = false;
            latch->m_pvalue_index               = pvalueIndexP;
            latch->m_pvalue_type                = pvalueTypeP;
            latch->m_query_type                 = pvalue_query_recurring;
            latch->m_request_sent               = false;
            latch->m_request_timestamp.tv_sec   = 0;
            latch->m_request_timestamp.tv_nsec  = 0;
            latch->m_response_timestamp.tv_sec  = 0;
            latch->m_response_timestamp.tv_nsec = 0;
            switch (pvalueTypeP) {
                case pvalue_type_long:
                    latch->m_value_int32        = 0;
                    break;

                case pvalue_type_float:
                    latch->m_value_float32      = 0.0;
                    break;

                default:
                    THROW(InvalidParameter);
            }
            m_pvalue_request_latches.push_back(latch);
        }
    }

    // Cancels monitoring of a given P-value.
    void ACR7000::cancelMonitoring(pvalue_index_t pvalueIndexP) {
        for (   std::vector<pvalue_request_latch_t*>::iterator iter = m_pvalue_request_latches.begin();
                iter!=m_pvalue_request_latches.end();
                ++iter)
        {
            // Look for a PValue index in a request that matches this response.
            if (pvalueIndexP == (*iter)->m_pvalue_index) {
                // Found it; disable the request.  Arguably could do a delete instead.
                (*iter)->m_query_type = pvalue_query_complete;
                break;
            }
        }
        // NOTE: If the pvalue index was not found, the function has no effect.
    }


    float ACR7000::inspectPValueFloat(pvalue_index_t pvalueIndexP, struct timespec &whenR) {
        for (   std::vector<pvalue_request_latch_t*>::iterator iter = m_pvalue_request_latches.begin();
                iter!=m_pvalue_request_latches.end();
                ++iter)
        {
            // Look for a PValue index in a request that matches this response.
            if (pvalueIndexP == (*iter)->m_pvalue_index) {
                // Found the latch; make sure the data format matches.
                if ((*iter)->m_pvalue_type != pvalue_type_float) {
                    THROW(PValueWrongDataType);
                }
                
                // See if a response is available yet.
                if (!((*iter)->m_latch_valid)) {
                    // No data yet available.
                    THROW(PValuePending);
                }
                // Found it; copy the timestamp and return the latched value.
                whenR = (*iter)->m_response_timestamp;
                return (*iter)->m_value_float32;
            }
        }
        // Didn't find a matching latch.
        THROW(PValueNotMonitored);
        return 0.0;
    }


    int32_t ACR7000::inspectPValueLong(pvalue_index_t pvalueIndexP, struct timespec &whenR) {
        for (   std::vector<pvalue_request_latch_t*>::iterator iter = m_pvalue_request_latches.begin();
                iter!=m_pvalue_request_latches.end();
                ++iter)
        {
            // Look for a PValue index in a request that matches this response.
            if (pvalueIndexP == (*iter)->m_pvalue_index) {
                // Found the latch; make sure the data format matches.
                if ((*iter)->m_pvalue_type != pvalue_type_long) {
                    THROW(PValueWrongDataType);
                }
                
                // See if a response is available yet.
                if (!((*iter)->m_latch_valid)) {
                    // No data yet available.
                    THROW(PValuePending);
                }
                // Found it; copy the timestamp and return the latched value.
                whenR = (*iter)->m_response_timestamp;
                return (*iter)->m_value_int32;
            }
        }
        // Didn't find a matching latch.
        THROW(PValueNotMonitored);
        return 0;
    }

    void ACR7000::runProgram(int indexP, bool listenP) {
		m_motion_services.getLogger().info("ACR7000::runProgram start");
		float runStatus = 0.0;
		struct timespec when;
		/// Print a comment
        std::string command=": REM : ACR7000_runProgram\r\nPROG0\r\n";
        m_ascii_link.write(command.c_str(), strlen(command.c_str()));

		/// Set P0 to the number of the program to run.
        command="P0="+std::to_string(indexP)+"\r\n";
        m_ascii_link.write(command.c_str(), strlen(command.c_str()));

		/// Run the program
        if (listenP) {
            command="lrun\r\n";
        } else {
            command="run\r\n";
        }
        m_ascii_link.write(command.c_str(), strlen(command.c_str()));

		int loopCount = 0;
		bool isRunning = false;
		while(!isRunning)
		{
			long programFlags = inspectPValueLong(AR7xT_pvaddrs_program_flags[0], when);
			m_motion_services.getLogger().info("ACR7000::programFlags = " + std::to_string(programFlags) + "(not running)");
			isRunning = ((programFlags & MASK_PROGRAM_RUNNING) == MASK_PROGRAM_RUNNING);
			usleep(100000);
			if (loopCount++ > MAX_PROGRAM_WAIT_FOR_START_LOOPS)
			{
				m_motion_services.getLogger().error("ACR7000::runProgram timed out waiting for program start");
				isRunning = true; // Break out of loop.
			}
		}
		while(isRunning)
		{
			long programFlags = inspectPValueLong(AR7xT_pvaddrs_program_flags[0], when);
			m_motion_services.getLogger().info("ACR7000::programFlags = " + std::to_string(programFlags) + "(running)");
			isRunning = ((programFlags & MASK_PROGRAM_RUNNING) == MASK_PROGRAM_RUNNING);
			if (indexP != SETUP_ROUTINE) /// Setup does not report status
			{
				runStatus = inspectPValueFloat((pvalue_index_t)indexP, when);
				isRunning &= (runStatus == 0.0);
			}
			usleep(100000);
			if (loopCount++ > MAX_PROGRAM_WAIT_FOR_END_LOOPS)
			{
				m_motion_services.getLogger().error("ACR7000::runProgram timed out waiting for program end");
				isRunning = false; // Break out of loop.
			}
		}

		if (indexP == SETUP_ROUTINE) /// Setup does not report status
		{
			m_motion_services.getLogger().info("ACR7000::runProgram() done.");
		}
		else
		{
			runStatus = inspectPValueFloat((pvalue_index_t)indexP, when);
			m_motion_services.getLogger().info("ACR7000::runProgram done. P" + std::to_string(indexP) + "=" + std::to_string(runStatus));
		}
    }

    void ACR7000::launchProgram(int indexP, bool listenP) {
		m_motion_services.getLogger().info("ACR7000::launchProgram");
		/// Print a comment
        std::string command=": REM : ACR7000_runProgram\r\nPROG0\r\n";
        m_ascii_link.write(command.c_str(), strlen(command.c_str()));

		/// Set P0 to the number of the program to run.
        command="P0="+std::to_string(indexP)+"\r\n";
        m_ascii_link.write(command.c_str(), strlen(command.c_str()));

		/// Run the program
        if (listenP) {
            command="lrun\r\n";
        } else {
            command="run\r\n";
        }
        m_ascii_link.write(command.c_str(), strlen(command.c_str()));
	}

    bool ACR7000::programIsRunning(int indexP) {
		struct timespec when;
		long programFlags = inspectPValueLong(AR7xT_pvaddrs_program_flags[0], when);
		return ((programFlags & MASK_PROGRAM_RUNNING) == MASK_PROGRAM_RUNNING);
	}

	float ACR7000::getProgramStatus(int indexP) {
		struct timespec when;
		float status = 0.0;
		if (indexP != SETUP_ROUTINE) /// Setup does not report status
		{
			status = inspectPValueFloat((pvalue_index_t)indexP, when);
		}
		return status;
	}


	void ACR7000::clearOneFlag( int firstBit, int bitIndex, int axisNumber ) {
		int axisBitNumber0 = firstBit + (32 * axisNumber);
		int bitNumber = axisBitNumber0 + bitIndex;
		/// Use the ASCII interface to cleare the bit to get some debug printout
		std::string clearCommand = "CLR " + std::to_string(bitNumber) + " : PRINT BIT " + std::to_string(bitNumber) + "\r\n";
        m_ascii_link.write(clearCommand.c_str(), strlen(clearCommand.c_str()));
		usleep(10000);
	}

	void ACR7000::setOneFlag( int firstBit, int bitIndex, int axisNumber ) {
		int axisBitNumber0 = firstBit + (32 * axisNumber);
		int bitNumber = axisBitNumber0 + bitIndex;
		/// Use the ASCII interface to cleare the bit to get some debug printout
		std::string setCommand = "SET " + std::to_string(bitNumber) + " : PRINT BIT " + std::to_string(bitNumber) + "\r\n";
        m_ascii_link.write(setCommand.c_str(), strlen(setCommand.c_str()));
		usleep(10000);
	}

	bool ACR7000::getOneFlag( pvalue_index_t * pValueAddressArray, int firstBit, int bitIndex, int axisNumber ) {
		int axisBitNumber0 = firstBit + (32 * axisNumber);
		int bitNumber = axisBitNumber0 + bitIndex;
		/// Use the ASCII interface to get some debug printout
		std::string printCommand = "PRINT BIT " + std::to_string(bitNumber) + "\r\n";
        m_ascii_link.write(printCommand.c_str(), strlen(printCommand.c_str()));
		usleep(100);
		/// Use the binary host interace to get a value to return.
		struct timespec when;
		long setOfFlags = inspectPValueLong(pValueAddressArray[axisNumber], when);
		long mask = 1L << bitIndex;
		bool isSet = ((setOfFlags & mask) == mask);
		if (isSet)
		{
			m_motion_services.getLogger().info("axis=" + std::to_string(axisNumber) + "|bit=" + std::to_string(bitIndex) + "is SET!" );
			m_motion_services.getLogger().info("Flags=" + std::to_string(setOfFlags) + "|mask=" + std::to_string(mask) );
		}
		return isSet;
	}

	void ACR7000::clearMasterFlag( int bitIndex, int masterId ) {
		clearOneFlag( AR7xT_first_bit_master_flags, bitIndex, masterId );
	}
	void ACR7000::setMasterFlag( int bitIndex, int masterId ) {
		setOneFlag( AR7xT_first_bit_master_flags, bitIndex, masterId );
	}
	bool ACR7000::getMasterFlag( int bitIndex, int masterId ) {
		return getOneFlag( AR7xT_pvaddrs_axis_master_flags, AR7xT_first_bit_master_flags, bitIndex, masterId );
	}

	void ACR7000::clearAxisFlag( int bitIndex, int axisNumber ) {
		clearOneFlag( AR7xT_first_bit_axis_flags, bitIndex, axisNumber );
	}
	void ACR7000::setAxisFlag( int bitIndex, int axisNumber ) {
		setOneFlag( AR7xT_first_bit_axis_flags, bitIndex, axisNumber );
	}
	bool ACR7000::getAxisFlag( int bitIndex, int axisNumber ){
		return getOneFlag( AR7xT_pvaddrs_axis_flags, AR7xT_first_bit_axis_flags, bitIndex, axisNumber );
	}

	void ACR7000::clearSecondaryAxisFlag( int bitIndex, int axisNumber ) {
		clearOneFlag( AR7xT_first_bit_secondary_axis_flags, bitIndex, axisNumber );
	}
	void ACR7000::setSecondaryAxisFlag( int bitIndex, int axisNumber ) {
		setOneFlag( AR7xT_first_bit_secondary_axis_flags, bitIndex, axisNumber );
	}
	bool ACR7000::getSecondaryAxisFlag( int bitIndex, int axisNumber ) {
		return getOneFlag( AR7xT_pvaddrs_secondary_axis_flags, AR7xT_first_bit_secondary_axis_flags, bitIndex, axisNumber );
	}

	void ACR7000::clearQuaternaryAxisFlag( int bitIndex, int axisNumber ) {
		clearOneFlag( AR7xT_first_bit_quaternary_axis_flags, bitIndex, axisNumber );
	}
	void ACR7000::setQuaternaryAxisFlag( int bitIndex, int axisNumber ) {
		setOneFlag( AR7xT_first_bit_quaternary_axis_flags, bitIndex, axisNumber );
	}
	bool ACR7000::getQuaternaryAxisFlag( int bitIndex, int axisNumber ) {
		return getOneFlag( AR7xT_pvaddrs_quaternary_axis_flags, AR7xT_first_bit_quaternary_axis_flags, bitIndex, axisNumber );
	}

	/// Get the quaternary axis flags for the channel_index implied by the given axisNameP.
	/// Return true if the MASK_KILL_ALL_MOTION_REQUEST bit is set.
	bool ACR7000::getKillAllMotionRequestFlag( std::string axisNameP )
	{
		struct timespec when;
		int channelIndex = getAxis( axisNameP )->getChannelIndex();
		long QuatAxisFlags = inspectPValueLong(AR7xT_pvaddrs_quaternary_axis_flags[channelIndex], when);
		return ((QuatAxisFlags & MASK_KILL_ALL_MOTION_REQUEST) == MASK_KILL_ALL_MOTION_REQUEST);
	}

/*
	void ACR7000::clearBitsOnMaster( int bitIndex, int masterId ) {
		int axis_index;
		std::vector<MotionAxis *>::iterator axis_iter;
        for (axis_iter = p_axes->begin(), axis_index = 0;
                axis_iter!=p_axes->end();
                axis_iter++, axis_index++)
        {
			if ((*axis_iter)->getMasterIndex() == masterId)
			{
				bool ACR7000::clearQuaternaryAxisFlag( int bitIndex, int axisNumber ) {
				int modedBitIndex = bitIndex + (axis_index * 32);
				std::string clearCommand = "CLR " + std::to_string(modedBitIndex) + "\r\n";
//				m_motion_services.getLogger().info("clearBitsOnMaster: " + clearCommand);
				m_ascii_link.write(clearCommand.c_str(), strlen(clearCommand.c_str()));
				usleep(1000);
			}
		}
	}
*/

	void ACR7000::clearQuaternaryAxisFlagsOfMaster( int bitIndex, int masterId ) {
		int axis_index;
		std::vector<MotionAxis *>::iterator axis_iter;
        for (axis_iter = p_axes->begin(), axis_index = 0;
                axis_iter!=p_axes->end();
                axis_iter++, axis_index++)
        {
			if ((*axis_iter)->getMasterIndex() == masterId)
			{
				clearQuaternaryAxisFlag( bitIndex, axis_index );
			}
		}
	}

	void ACR7000::setQuaternaryAxisFlagsOfMaster( int bitIndex, int masterId ) {
		int axis_index;
		std::vector<MotionAxis *>::iterator axis_iter;
        for (axis_iter = p_axes->begin(), axis_index = 0;
                axis_iter!=p_axes->end();
                axis_iter++, axis_index++)
        {
			if ((*axis_iter)->getMasterIndex() == masterId)
			{
				setQuaternaryAxisFlag( bitIndex, axis_index );
			}
		}
	}
	
	bool ACR7000::QuaternaryAxisFlagsOfMasterAreClear( int bitIndex, int masterId ) {
		bool anySet = false;
		int axis_index;
		std::vector<MotionAxis *>::iterator axis_iter;
        for (axis_iter = p_axes->begin(), axis_index = 0;
                axis_iter!=p_axes->end();
                axis_iter++, axis_index++)
        {
			if ((*axis_iter)->getMasterIndex() == masterId)
			{
				bool oneSet = getQuaternaryAxisFlag( bitIndex, axis_index );
				if (oneSet)
				{
					m_motion_services.getLogger().info("axis=" + std::to_string(axis_index) + "|bit=" + std::to_string(bitIndex) + "is SET!" );
				}
				anySet |= oneSet;
			}
		}
		return (!anySet);
	}

	void ACR7000::printActualPositionViaAscii( int axisNumber ) {
		std::string command = "PRINT P" + std::to_string(AR7xT_pvaddrs_axis_actual_position[axisNumber]) + "\r\n";
        m_ascii_link.write(command.c_str(), strlen(command.c_str()));
	}

    void ACR7000::enableChannel(int channelIndexP) {
        std::string command="\r\nPROG 0\r\nAXIS"+std::to_string(channelIndexP)+" ON\r\n";
        m_ascii_link.write(command.c_str(), strlen(command.c_str()));
    }

    void ACR7000::disableChannel(int channelIndexP) {
        std::string command="\r\nPROG 0\r\nAXIS"+std::to_string(channelIndexP)+" OFF\r\n";
        m_ascii_link.write(command.c_str(), strlen(command.c_str()));
    }

    // Static wrapper for ASCII TCP channel exception handler.
    void ACR7000::onAsciiTCPException_wrapper(ProperCommBase *pLinkP, InstrumentedException &exP, void *pUserDataP) {
        ACR7000 *pThis = reinterpret_cast<ACR7000 *>(pUserDataP);
        pThis->onAsciiTCPException(exP);
    }

    // ASCII TCP channel exception handler.
    void ACR7000::onAsciiTCPException(InstrumentedException &exP){
        
    }

    // Static wrapper for ASCII TCP channel On Receive handler.
    void ACR7000::onAsciiTCPRecv_wrapper(ProperCommBase *pLinkP, void *pUserDataP) {
        ACR7000 *pThis = reinterpret_cast<ACR7000 *>(pUserDataP);
        pThis->onAsciiTCPRecv();
    }

    // ASCII TCP channel On Receive handler.
    void ACR7000::onAsciiTCPRecv(void) {
        m_motion_services.getLogger().info("ACR7000::onAsciiTCPRecv");

        // Feedback on the ASCII channel is currently used only for debugging.
        char ascii_in[257];
        int bytes_read=m_ascii_link.read((uint8_t*)ascii_in, 256);
        ascii_in[bytes_read]=0;

        std::stringstream ss;
        ss << (const char*)ascii_in;
        for (std::string line; std::getline(ss, line);)
        {
            m_motion_services.getLogger().info(std::string("ACR7000::onAsciiTCPRecv: ")+line);
        }
        

    }

    // Static wrapper for ASCII TCP channel On Transmission handler.
    void ACR7000::onAsciiTCPXmit_wrapper(ProperCommBase *pLinkP, void *pUserDataP) {
        ACR7000 *pThis = reinterpret_cast<ACR7000 *>(pUserDataP);
        pThis->onAsciiTCPXmit();
    }

    // ASCII TCP channel On Transmission handler.
    void ACR7000::onAsciiTCPXmit(void) {
        m_motion_services.getLogger().info("ACR7000::onAsciiTCPXmit");
    }

    // Static wrapper for binary TCP channel exception handler.
    void ACR7000::onBinaryTCPException_wrapper(ProperCommBase *pLinkP, InstrumentedException &exP, void *pUserDataP) {
        ACR7000 *pThis = reinterpret_cast<ACR7000 *>(pUserDataP);
        pThis->onBinaryTCPException(exP);
    }

    // Binary TCP channel exception handler.
    void ACR7000::onBinaryTCPException(InstrumentedException &exP) {
        m_motion_services.getLogger().error(std::string("ACR7000::onBinaryTCPException: ")+exP.what());
    }

    // Static wrapper for binary TCP channel On Receive handler.
    void ACR7000::onBinaryTCPRecv_wrapper(ProperCommBase *pLinkP, void *pUserDataP) {
        ACR7000 *pThis = reinterpret_cast<ACR7000 *>(pUserDataP);
        pThis->onBinaryTCPRecv();
    }

    // Binary TCP channel On Receive handler.
     void ACR7000::onBinaryTCPRecv(void) {
        //m_motion_services.getLogger().info("ACR7000::onBinaryTCPRecv");
        parsePendingBinaryRecv();
    }

    // Static wrapper for binary TCP channel On Transmission handler.
    void ACR7000::onBinaryTCPXmit_wrapper(ProperCommBase *pLinkP, void *pUserDataP) {
        ACR7000 *pThis = reinterpret_cast<ACR7000 *>(pUserDataP);
        pThis->onBinaryTCPXmit();
    }

    // Binary TCP channel On Transmission handler.
    void ACR7000::onBinaryTCPXmit(void) {
        m_motion_services.getLogger().info("ACR7000::onBinaryTCPXmit");
    }


    void *ACR7000::pvaluePollingThreadWrapper(void *pDataP) {
        ACR7000 *pThis = (ACR7000*)pDataP;
        // FIXME
        //ASSERT(pThis);
        pThis->pvaluePollingThreadCore();
        return NULL;
    }


    void ACR7000::parsePendingBinaryRecv(void) {
        // This function parses pending traffic from the ACR7000, provided
        // enough data has been received for that to be possible.  It
        // should be run by the TCPLink object's "on receive" handler.
        //
        // - IMPORTANT -
        // Binary traffic packets from the ACR7000 start with a Header ID.
        // For MOST commands, this is a NULL.  This is a holdover from the
        // serial communication protocol that the original devices had, and
        // is really unfortunate, given that a NULL is a valid byte for most
        // other fields in the packet.
        //
        // Worse, there are some commands that use a different Header ID.
        // ***AT THIS TIME, THESE ARE UNSUPPORTED.***
        //
        // Luckily, we can take advantage of the fact that most of the
        // traffic we are concerned with are responses to requests, and
        // these responses echo back both the code for the request type,
        // and the PValue index/address the request was for.
        //
        // Because of possible synchronization errors due to the lack of
        // a CRC or checksum in the packet header, we have to consider the
        // possibility of getting a response for all possible command
        // types, regardless of whether or not we're sending those requests
        // or not, in case the controller gets confused.  Some of these
        // commands can be quite long, which could be a problem if the
        // parser loses sync.
        //
        // Meanwhile, the shortest response packets in the references
        // appear to be eight bytes in length.  So, there's no point in
        // trying to check anything until there are at least eight bytes
        // in the receive buffer, with the first bytes equal to NULL.
        //
        // If the parser has desynchronized-- failed-- then the approach
        // is to just discard bytes until something that appears to be a
        // valid packet is available.

        int     bytes_pending   = m_binary_link.getRecvBufferBytesPending();

        // True if enough data for a minimum response is available, but the
        // actual packet type is known, and not enough data is yet available
        // for THAT.
        bool    wait            = false;

        // Number of bits set in a mask.
        int     bits_set;

        // To keep this logic simpler, always abort if we've had to chew enough
        // data that we would have to maintain more state between calls.
        while ( (!wait) && (bytes_pending>=ACR7000_MIN_RESPONSE) )
        {
            LOCK();
            // Ensure the Header ID is staged.
            if (0==m_parser_bytes_staged) {
                // Advance until we get a NULL as the potential "Header ID" field.
                m_parser_bytes_staged=m_binary_link.read(&(m_parser_staging_buf[0]), 1);
                if (1!=m_parser_bytes_staged) {
                    // FIXME, create and use a logic exception
                    THROW(NoSuchObject);
                } else {
                    bytes_pending--;
                }
                if (m_parser_staging_buf[0]!=0x00) {
                    // Continue trying to resync
                    m_parser_bytes_staged=0;
                    continue;
                }
            }

            // Ensure the Packet ID is staged.
            if (1==m_parser_bytes_staged) {
                m_parser_bytes_staged+=m_binary_link.read(&(m_parser_staging_buf[1]), 1);
                if (2!=m_parser_bytes_staged) {
                    // FIXME, create and use a logic exception
                    THROW(NoSuchObject);
                } else {
                    bytes_pending--;
                }
            }

            if (2<=m_parser_bytes_staged) {
                // VALIDATE the Packet ID and read remaining data if available.
                switch (m_parser_staging_buf[1]) {
                    /* Binary Data Packet Group Codes*/
                    case 0x10:  // Flag parameters
                    case 0x18:  // Encoder parameters
                    case 0x19:  // DAC parameters
                    case 0x1A:  // PLC parameters
                    case 0x1B:  // Miscellaneous
                    case 0x1C:  // Program parameters
                    case 0x20:  // Master parameters
                    case 0x28:  // Master parameters
                    case 0x30:  // Axis parameters
                    case 0x38:  // Axis parameters
                    case 0x40:  // CMT parameters
                    case 0x50:  // Logging parameters
                    case 0x60:  // Encoder parameters
                        // PARSE BINARY DATA PACKET
                        // Load the Group Index and the Isolation Mask, if not yet loaded.
                        if (2==m_parser_bytes_staged) {
                            // (These are within the minimum packet size, so should be available).
                            m_parser_bytes_staged+=m_binary_link.read(&(m_parser_staging_buf[2]), 2);
                        }

                        if (4!=m_parser_bytes_staged) {
                            // FIXME, create and use a logic exception
                            THROW(NoSuchObject);
                        } else {
                            bytes_pending-=2;
                        }

                        // Count the number of bits set in the Isolation Mask to know
                        // how many 32-bit values will need to be read.
                        // TODO FIXME - optimize using a nibble table
                        bits_set =
                                    ((m_parser_staging_buf[3])    &1)
                                +   ((m_parser_staging_buf[3]>>1) &1)
                                +   ((m_parser_staging_buf[3]>>2) &1)
                                +   ((m_parser_staging_buf[3]>>3) &1)
                                +   ((m_parser_staging_buf[3]>>4) &1)
                                +   ((m_parser_staging_buf[3]>>5) &1)
                                +   ((m_parser_staging_buf[3]>>6) &1)
                                +   ((m_parser_staging_buf[3]>>7) &1);

                        // Read the appropriate number of data bytes, if and only if they are available.
                        // Otherwise, we will fall through back to here.
                        if (bytes_pending>=(4*bits_set)) {
                            m_parser_bytes_staged+=m_binary_link.read(&(m_parser_staging_buf[4]), (4*bits_set));
                            if ((4+(4*bits_set))!=m_parser_bytes_staged) {
                                // FIXME, create and use a logic exception
                                THROW(NoSuchObject);
                            } else {
                                bytes_pending-=(4*bits_set);
                            }

                            // Binary data packet fully receieved.  Process it.
                            processBinaryDataPacket();

                            // Reset parser for next data.
                            m_parser_bytes_staged=0;
                        } else {
                            // Not enough data available yet; wait for next call.
                            wait=true;
                        }
                        break; // End of Data Packet logic

                    case 0x88:  // Binary Get Long (32-bit integer)
                        // We expect a parameter index (copied from the request) and a value.
                        // Read these data bytes if and only if they are available.
                        if (2==m_parser_bytes_staged) {
                            // (These are within the minimum packet size, so should be available).
                            m_parser_bytes_staged+=m_binary_link.read(&(m_parser_staging_buf[2]), 6);
                        }

                        if (8!=m_parser_bytes_staged) {
                            // FIXME, create and use a logic exception
                            THROW(NoSuchObject);
                        } else {
                            bytes_pending-=6;
                        }

                        // Binary Get Long response fully received, process it.
                        processBinaryGetLongResponse();

                        // Reset parser for next data.
                        m_parser_bytes_staged=0;
                        break; // End of Binary Get Long logic
                    
                    case 0x89:  // Binary Set Long (32-bit integer)
                        // SHOULD NEVER OCCUR, THIS REQUEST HAS NO RESPONSE
                        m_motion_services.getLogger().warning("WARNING, unexpected ACR7000 response Packet ID: Binary Set Long\n");
                        // Resynchronize.
                        m_parser_bytes_staged=0;
                        break;
                        
                    case 0x8A:  // Binary Get IEEE (32-bit floating point)
                        // We expect a parameter index (copied from the request) and a value.
                        // Read these data bytes if and only if they are available.
                        if (2==m_parser_bytes_staged) {
                            // (These are within the minimum packet size, so should be available).
                            m_parser_bytes_staged+=m_binary_link.read(&(m_parser_staging_buf[2]), 6);
                        }

                        if (8!=m_parser_bytes_staged) {
                            // FIXME, create and use a logic exception
                            THROW(NoSuchObject);
                        } else {
                            bytes_pending-=6;
                        }

                        // Binary Get Long response fully received, process it.
                        processBinaryGetIEEEResponse();

                        // Reset parser for next data.
                        m_parser_bytes_staged=0;
                        break; // End of Binary Get IEEE logic

                    case 0x8B:  // Binary Set IEEE (32-bit floating point)
                        // SHOULD NEVER OCCUR, THIS REQUEST HAS NO RESPONSE
                        m_motion_services.getLogger().warning("WARNING, unexpected ACR7000 response Packet ID: Binary Set IEEE\n");
                        // Resynchronize.
                        m_parser_bytes_staged=0;
                        break;


                    /* THE FOLLOWING RESPONSE TYPES ARE CURRENTLY UNSUPPORTED, BUT THE PARSER
                     * *SHOULD* RECOVER IF THEY ARE ENCOUNTERED.
                     */
                    case 0x90:  // Binary Peek
                        // TODO FIXME - IMPLEMENT PROPER PARSING FOR THIS RESPONSE TYPE
                        m_motion_services.getLogger().warning("WARNING, unsupported ACR7000 response Packet ID: Binary Peek\n");
                        // Resynchronize-- poorly, since the rest of this packet will not be chewed
                        m_parser_bytes_staged=0;
                        break;

                    case 0x91:  // Binary Poke
                        // SHOULD NEVER OCCUR, THIS REQUEST HAS NO RESPONSE
                        // TODO FIXME - IMPLEMENT PROPER PARSING FOR THIS RESPONSE TYPE
                        m_motion_services.getLogger().warning("WARNING, unexpected ACR7000 response Packet ID: Binary Poke\n");
                        m_motion_services.getLogger().warning("WARNING, unsupported ACR7000 response Packet ID: Binary Poke\n");
                        // Resynchronize-- poorly, since the rest of this packet will not be chewed
                        m_parser_bytes_staged=0;
                        break;

                    case 0x92:  // Binary Address (convert a program number and parameter code to an actual address in the ACR7000's memory)
                        // TODO FIXME - IMPLEMENT PROPER PARSING FOR THIS RESPONSE TYPE
                        m_motion_services.getLogger().warning("WARNING, unsupported ACR7000 response Packet ID: Binary Address\n");
                        // Resynchronize-- poorly, since the rest of this packet will not be chewed
                        m_parser_bytes_staged=0;
                        break;

                    case 0x93:  // Binary Parameter Address (convert a parameter index to an actual address in the ACR7000's memory)
                        // TODO FIXME - IMPLEMENT PROPER PARSING FOR THIS RESPONSE TYPE
                        m_motion_services.getLogger().warning("WARNING, unsupported ACR7000 response Packet ID: Binary Parameter Address\n");
                        // Resynchronize-- poorly, since the rest of this packet will not be chewed
                        m_parser_bytes_staged=0;
                        break;

                    case 0x94:  // Binary Mask
                        // SHOULD NEVER OCCUR, THIS REQUEST HAS NO RESPONSE
                        // TODO FIXME - IMPLEMENT PROPER PARSING FOR THIS RESPONSE TYPE
                        m_motion_services.getLogger().warning("WARNING, unexpected ACR7000 response Packet ID: Binary Mask\n");
                        m_motion_services.getLogger().warning("WARNING, unsupported ACR7000 response Packet ID: Binary Mask\n");
                        // Resynchronize-- poorly, since the rest of this packet will not be chewed
                        m_parser_bytes_staged=0;
                        break;

                    case 0x95:  // Binary Parameter Mask
                        // SHOULD NEVER OCCUR, THIS REQUEST HAS NO RESPONSE
                        // TODO FIXME - IMPLEMENT PROPER PARSING FOR THIS RESPONSE TYPE
                        m_motion_services.getLogger().warning("WARNING, unexpected ACR7000 response Packet ID: Binary Parameter Mask\n");
                        m_motion_services.getLogger().warning("WARNING, unsupported ACR7000 response Packet ID: Binary Parameter Mask\n");
                        // Resynchronize-- poorly, since the rest of this packet will not be chewed
                        m_parser_bytes_staged=0;
                        break;

                    case 0x96:  // Binary Parameter Address (convert a parameter index to an actual address in the ACR7000's memory)
                        // TODO FIXME - IMPLEMENT PROPER PARSING FOR THIS RESPONSE TYPE
                        m_motion_services.getLogger().warning("WARNING, unsupported ACR7000 response Packet ID: Binary Parameter Address\n");
                        // Resynchronize-- poorly, since the rest of this packet will not be chewed
                        m_parser_bytes_staged=0;
                        break;

                    default:
                        // Probably we are desynchronized, and just encountered a NULL in the datastream.
                        // Continue trying to resynchronize to a known HeaderID:PacketID combination.
                        m_parser_bytes_staged=0;
                        break;
                } // end Packet ID switch for Header ID==0x00
            } // end (2<=m_parser_bytes_staged)

            // See what's left
            bytes_pending = m_binary_link.getRecvBufferBytesPending();
            UNLOCK();
        } // Continue if enough data remains
        // End parsing for this pass (presumably a call from an OnReceive handler).
    }

    void ACR7000::processBinaryDataPacket(void) {
        // ASSERT_LOCKED();
        // TODO FIXME
    }

    void ACR7000::processBinaryGetLongResponse(void) {
        // ASSERT_LOCKED();
        binary_get_pvalue_response_t *p_response = reinterpret_cast<binary_get_pvalue_response_t*>(&m_parser_staging_buf);

        uint16_t pvalue_index = (p_response->m_pvalue_index_high<<8) + (p_response->m_pvalue_index_low);

        bool found_it=false;

        // Find the request entry.
        for (   std::vector<pvalue_request_latch_t*>::iterator iter = m_pvalue_request_latches.begin();
                iter!=m_pvalue_request_latches.end();
                ++iter)
        {
            // Look for a PValue index in a request that matches this response.
            if ((*iter)->m_pvalue_index == pvalue_index) {
                // Update the value.
                (*iter)->m_value_int32 =
                        (((int32_t)p_response->u_little_endian_long.m_long_foreign_bytes[3])<<24)
                    +   (((int32_t)p_response->u_little_endian_long.m_long_foreign_bytes[2])<<16)
                    +   (((int32_t)p_response->u_little_endian_long.m_long_foreign_bytes[1])<<8)
                    +   ((int32_t)p_response->u_little_endian_long.m_long_foreign_bytes[0]);

                struct timespec monotonic_now;
                // Get the current time on the monotonic clock.  Monotonic clocks cannot jump
                // forward or backwards in time, even if NTP or some other mechanism changes
                // the system "wall clock" time.
                clock_gettime(CLOCK_MONOTONIC, &monotonic_now);

                // Update the timestamp.
                (*iter)->m_response_timestamp = monotonic_now;

                // Validate the latch.
                (*iter)->m_latch_valid = true;

                found_it=true;
                break;
            }
        }

        if (!found_it) {
            m_motion_services.getLogger().warning("DROPPED an unexpected Binary Get Long response packet for PValue index "+std::to_string(pvalue_index));
        }
    }

    void ACR7000::processBinaryGetIEEEResponse(void) {
        // ASSERT_LOCKED();
        binary_get_pvalue_response_t *p_response = reinterpret_cast<binary_get_pvalue_response_t*>(&m_parser_staging_buf);

        uint16_t pvalue_index = (p_response->m_pvalue_index_high<<8) + (p_response->m_pvalue_index_low);

        bool found_it=false;

        // Find the request entry.
        for (   std::vector<pvalue_request_latch_t*>::iterator iter = m_pvalue_request_latches.begin();
                iter!=m_pvalue_request_latches.end();
                ++iter)
        {
            // Look for a PValue index in a request that matches this response.
            if ((*iter)->m_pvalue_index == pvalue_index) {

                // Update the value.
                float *p_float_hack = reinterpret_cast<float *>(&(p_response->u_little_endian_fp32));
                (*iter)->m_value_float32 = *p_float_hack;

                struct timespec monotonic_now;
                // Get the current time on the monotonic clock.  Monotonic clocks cannot jump
                // forward or backwards in time, even if NTP or some other mechanism changes
                // the system "wall clock" time.
                clock_gettime(CLOCK_MONOTONIC, &monotonic_now);

                // Update the timestamp.
                (*iter)->m_response_timestamp = monotonic_now;

                // Validate the latch.
                (*iter)->m_latch_valid = true;

                found_it=true;
                break;
            }
        }

        if (!found_it) {
            m_motion_services.getLogger().warning("DROPPED an unexpected Binary Get IEEE response packet for PValue index "+std::to_string(pvalue_index));
        }
    }


    /** This function polls the ACR7000 controller for parameter values ("PValues")
     *  by index.  This is done by walking the list of monitored parameters and
     *  transmitting explicit requests for their current values.  
    */
    void ACR7000::pvaluePollingThreadCore(void) {
        
        LOCK();
        m_pvalue_polling_thread_running=true;
        UNLOCK();

        while (!m_pvalue_polling_thread_want_shutdown) {
        //m_motion_services.getLogger().info("ACR7000::pvaluePollingThreadCore: TOP");
            // Figure out the maximum amount of time this thread can sleep before another
            // request will need to be sent.
            //
            // CAUTION!
            // If no other, more frequent tasks are pending, this thread sleeps for 1/4
            // second (250ms) before checking for pending requests.  This means that the
            // initially inquiry for a recurring rqeuest can take up to 1/4 second.  If
            // this becomes an issue, the sleep call at the bottom of this loop should
            // be replaced with a pthread_cond_timedwait, or something like that, with a
            // kernel object that can be signalled when a new PValue monitoring request
            // is added to the vector.
            int sleep_usec=250000;

            struct timespec monotonic_now;
    
            //LOCK();

            // Get the current time on the monotonic clock.  Monotonic clocks cannot jump
            // forward or backwards in time, even if NTP or some other mechanism changes
            // the system "wall clock" time.
            clock_gettime(CLOCK_MONOTONIC, &monotonic_now);

            for (std::vector<pvalue_request_latch_t*>::iterator iter = m_pvalue_request_latches.begin();
                    iter!=m_pvalue_request_latches.end();
                    iter++)
            {
                bool send_query=false;

                switch ((*iter)->m_query_type) {
                    case pvalue_query_complete:
                        // Either a one-shot that is already done, or disabled; do nothing.
                        break;

                    case pvalue_query_one_shot:
                        // A one-shot that has not yet been sent; send the query.
                        send_query=true;
                        (*iter)->m_query_type=pvalue_query_complete;
                        break;

                    case pvalue_query_recurring:
                        // See if enough time has elapsed yet that this should be sent-- or, if no request has
                        // yet been sent, send the first one.
                        if  (!((*iter)->m_request_sent)) {
                            send_query=true;
                            uint32_t interval_usec =
                                            ((*iter)->m_interval.tv_sec  * 1000000)
                                        +   ((*iter)->m_interval.tv_nsec * 1000);
                            sleep_usec = MIN(sleep_usec, interval_usec);
                        } else {
                            struct timespec age;
                            age.tv_sec  = monotonic_now.tv_sec  - (*iter)->m_request_timestamp.tv_sec;
                            age.tv_nsec = monotonic_now.tv_nsec - (*iter)->m_request_timestamp.tv_nsec;
                            
                            if  (       (age.tv_sec     > (*iter)->m_interval.tv_sec)
                                    ||  (age.tv_nsec    > (*iter)->m_interval.tv_nsec)
                                )
                            {
                                send_query=true;
                            } else {
                                struct timespec remaining;

                                remaining.tv_sec    = (*iter)->m_interval.tv_sec - age.tv_sec;
                                remaining.tv_nsec   = (*iter)->m_interval.tv_nsec -age.tv_nsec;

                                uint32_t remaining_usec =
                                                (remaining.tv_sec * 1000000)
                                            +   (remaining.tv_nsec * 1000);

                                sleep_usec = MIN(sleep_usec, remaining_usec);
                            }
                        }
                        break;

                    default:
                        THROW(InvalidParameter);
                }

                if (send_query) {
                    // Send the request.
                    switch ((*iter)->m_pvalue_type) {
                        case pvalue_type_float:
                            binaryGetFloat((*iter)->m_pvalue_index);
                            break;

                        case pvalue_type_long:
                            binaryGetLong((*iter)->m_pvalue_index);
                            break;

                        default:
                            THROW(InvalidParameter);
                    }
                    (*iter)->m_request_sent=true;
                    (*iter)->m_request_timestamp=monotonic_now;
                }
            } // end latch vector iteration
            //UNLOCK();

            // Sleep for the minimum amount of time before the next request needs to be sent.
            usleep(sleep_usec);
        }

        LOCK();
        m_pvalue_polling_thread_running=false;
        m_pvalue_polling_thread_want_shutdown=false;
        UNLOCK();
    }


    void ACR7000::purgeRx(void) {
		m_ascii_link.purgeRx();
		m_binary_link.purgeRx();
	}

    void ACR7000::reboot(unsigned int timeout_msP) {
        THROW(Unimplemented);
    }

}
