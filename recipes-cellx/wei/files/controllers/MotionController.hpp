/* MotionController.h */

#ifndef __MotionController_h__
#define __MotionController_h__

#include <string>
#include <vector>

#include "USAFW/util/Lockable.hpp"

namespace USAFW {

    // Forward references
    class MotionServices;
    class Transport;
    class MotionAxis;

    /* Base class for motion controllers.  Should contain only functions which
    * all supported motion controllers implement.
    */
    class MotionController : public Lockable {
    public:
        // Constructor.
        MotionController(MotionServices &motionServicesP, const std::string nameP);

        // Destructor.
        virtual ~MotionController();

        /** Initialize the controller, providing a list of transports and axes.
         */
        virtual void init(std::vector<Transport*> *pTransportsP, std::vector<MotionAxis*> *pAxesP, int timeout_ms_P);
        virtual void shutdown(int timeout_msP);

        virtual void energize(int timeout_ms_P)=0;
        virtual void deenergize(int timeout_ms_P)=0;


        /** Enables a motor power channel.  Normally this is used indirectly, by using MotionAxis::enable().
         */
        virtual void enableChannel(int channelIndexP)=0;
        virtual void disableChannel(int channelIndexP)=0;

        /** Order the controller to reboot.  
         *
         * THROWS:
         * OperationTimedOutException if any of the following occur:
         *
         * - The controller does not respond to the reboot command (if and only if
         *   a response is expected from this model)
         *
         * - The controller does not respond to a basic inquiry after the reboot
         *   completes.
         */
        virtual void reboot(unsigned int timeout_msP)=0;

        /** Returns TRUE if the controller owns and operates the named axis, else false.
         */
        bool hasAxis(const std::string nameP) const;

        /** Returns the axis requested, or throws NoSuchObjectException. */
        MotionAxis *getAxis(const std::string nameP) const;

    protected:
        /** Checks that the MotionServices instance, and the controller itself, are in acceptable condition for an active
         *  command to execute.  Active commands are moves, jogs, motor-enables, and other work that can cause the system
         *  to change positions, power levels, etc.
         * 
         *  THROWS:
         *  - EmergencyStopException if this, or any other, controller has been emergency stopped.  Emergency stops
         *    propogate throughout the entire system, and cannot be cleared without a restart.
         */
        virtual void preflightCheck(void);

        // ==DATA==

        // Link back to the master MotionServices object.
        MotionServices                  &m_motion_services;

        // Controller name.
        const std::string               m_name;

        // List of transports served by this controller.  This list is constructed by the parser,
        // but owned by this object.
        std::vector<Transport *>        *p_transports;

        // List of axes served by this controller.  This list is constructed by the parser,
        // but owned by this object.
        std::vector<MotionAxis *>       *p_axes;
    };

}

#endif
