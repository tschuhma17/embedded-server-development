/* include/USAFW/util/CodePosition.hpp */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

#ifndef __CodePosition__
#define __CodePosition__

#include "USAFW/sizes.h"
#include "USAFW/util/threadutils.hpp"

namespace USAFW {

    class CodePosition{
    public:
        // Constructor
        CodePosition(const char *fileP, const char *functionP, int lineP);

        // Constructor with explicit thread name specifier (normally automatically
        // determined in the constructor logic).
        CodePosition(const char *fileP, const char *functionP, int lineP, const char *zExplicitThreadNameP);

        // Equality check
        virtual bool operator==(const CodePosition &rhs) const;

    #ifdef USAFW_DISALLOW_HEAP
    protected:
        // Destructor
        ~CodePosition();

        // Explicitly delete the allocator to disallow heap usage when requested.
        void *operator new (size_t size)=delete;
    #else
    public:
        // Destructor
        ~CodePosition();
    #endif

    protected:
        char        m_filename      [USAFW_SHORT_STRMAX];
        char        m_function_name [USAFW_SHORT_STRMAX];
        char        m_thread_name   [USAFW_SHORT_STRMAX];
        int         m_line;
    };

    // This macro creates a CodePosition that represents the calling file, function,
    // line, and thread.
    #define HERE() CodePosition(__FILE__, __func__, __LINE__)

    // This is the appropriate static initializer for a CodePosition that should not
    // yet refer to a specific location and thread.
    #define NOWHERE() CodePosition("(NOWHERE)", "(NOWHERE)", -1, "(NOBODY)")
}

#endif
