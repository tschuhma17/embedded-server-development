/* include/USAFW/details.h */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

/** This file derives non-memory-size details, like feature options, from
 *  the high-level platform selections in include/USAFW_TARGET_CONFIG.h.
 */

#ifndef __USAFW_details_h__
#define __USAFW_details_h__

// Include project-specific user defines.
#include "USAFW_TARGET_CONFIG.h"

#ifdef USAFW_TARGET_LINUX
#define USAFW_MUTEX_TYPE_POSIX
#define USAFW_MUTEX_STRATEGY_RECURSIVE
#define USAFW_SEMAPHORE_TYPE_POSIX
#define USAFW_SOCKET_STRATEGY_BERKELEY
#define USAFW_THREAD_TYPE_POSIX
#endif


// Enable additional safety features when using GCC
#ifdef __GNUC__
#define USAFW_ENABLE_STATIC_ASSERTS
#define USAFW_STATIC_ASSERT_TYPE_GCC
#endif

#endif
