/* include/USAFW/sizes.h */

/** USA FIRMWARE STANDARD LIBRARY FOR EMBEDDED DEVELOPMENT (SLED)
 *  Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

/** This file derives memory size details, such as string buffer sizes, from
 *  the high-level platform selections in include/USAFW_TARGET_CONFIG.h.
 */

#ifndef __USAFW_SIZES_H__
#define __USAFW_SIZES_H__

// Include project-specific user defines.
#include "USAFW_TARGET_CONFIG.h"

#ifdef USAFW_TARGET_TINY
#ifdef USAFW_SHORT_STRMAX
#error ERROR: More than one USAFW_TARGET_* define seems to be set, or a subordinate define was predefined.
#endif
#define USAFW_DEBUG_STRMAX      (16)
#define USAFW_SHORT_STRMAX      (16)
#define USAFW_STRMAX            (16)
#define USAFW_LONG_STRMAX       (32)
#define USAFW_GUARD_SIZE_BYTES  (4)
#endif

#ifdef USAFW_TARGET_SMALL
#ifdef USAFW_SHORT_STRMAX
#error ERROR: More than one USAFW_TARGET_* define seems to be set, or a subordinate define was predefined.
#endif
#define USAFW_DEBUG_STRMAX      (40)
#define USAFW_SHORT_STRMAX      (40)
#define USAFW_STRMAX            (80)
#define USAFW_LONG_STRMAX       (120)
#define USAFW_GUARD_SIZE_BYTES  (8)
#endif

#ifdef USAFW_TARGET_COMPACT
#ifdef USAFW_SHORT_STRMAX
#error ERROR: More than one USAFW_TARGET_* define seems to be set, or a subordinate define was predefined.
#endif
#define USAFW_DEBUG_STRMAX      (40)
#define USAFW_SHORT_STRMAX      (80)
#define USAFW_STRMAX            (80)
#define USAFW_LONG_STRMAX       (120)
#define USAFW_GUARD_SIZE_BYTES  (8)
#endif

#ifdef USAFW_TARGET_NORMAL
#ifdef USAFW_SHORT_STRMAX
#error ERROR: More than one USAFW_TARGET_* define seems to be set, or a subordinate define was predefined.
#endif
#define USAFW_DEBUG_STRMAX      (256)
#define USAFW_SHORT_STRMAX      (80)
#define USAFW_STRMAX            (160)
#define USAFW_LONG_STRMAX       (256)
#define USAFW_GUARD_SIZE_BYTES  (16)
#endif

#ifdef USAFW_TARGET_LARGE
#ifdef USAFW_SHORT_STRMAX
#error ERROR: More than one USAFW_TARGET_* define seems to be set, or a subordinate define was predefined.
#endif
#define USAFW_DEBUG_STRMAX      (256)
#define USAFW_SHORT_STRMAX      (80)
#define USAFW_STRMAX            (256)
#define USAFW_LONG_STRMAX       (4096)
#define USAFW_GUARD_SIZE_BYTES  (32)
#endif

#endif
