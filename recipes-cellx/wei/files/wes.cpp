#include <iostream>
#include <thread>
#include <array>
#include <mutex>
#include "main.hpp"
#include "CommandInterpreter.hpp"
#include "CommandDoler.hpp"
#include "Components.hpp"
#include "MqttReceiver.hpp"
#include "MqttSender.hpp"
#include "MsgQueue.hpp"
#include "Fault.hpp"
#include "Sundry.hpp"
#include "ShellInvoker.hpp"
#include "USAFW/SLED.hpp"
#include "USAFW/util/Logger.hpp"
#include "MotionServices.hpp"
#include "exceptions/MotionException.hpp"
#include "Tecan_ADP.hpp"
#include "sys/types.h"
#include "sys/stat.h"
#include "es_des.h"
#include <linux/rtc.h>
#include <time.h>
#include <sys/ioctl.h>
#include <array>
#include <unistd.h>
//#include <signal.h>

//#define USE_POPEN
//#define USE_PYLON
//#define USE_PYWRAP

#ifdef USE_PYLON
#include <pylon/PylonIncludes.h>
/// Namespace for using pylon objects.
using namespace Pylon;
/// Namespace for using GenApi objects.
using namespace GenApi;
#endif // USE_PYLON

#ifdef USE_PYWRAP
#include "pylon_wrapper.hpp"
#endif // USE_PYWRAP

using namespace std;
using namespace USAFW;

/// NOTE: Let's standardize on one way to use atomic variables. According to the research described at
/// https://www.arangodb.com/2015/02/comparing-atomic-mutex-rwlocks/, the most efficient approach
/// for Linux ARM environments is to use Atomic Read “acquire” for getting, Atomic Set “relaxed” for setting.
/// See ManageHoldStates() for examples.

/// Allows any thread to look up the current XX location and compare it to the default location.
std::atomic<long> g_XxCurrentOffset;
/// Allows any thread to look up the XX default location.
std::atomic<long> g_XxDefaultLocationOffset;

/// Allows any thread to look up the current YY location and compare it to the default location.
std::atomic<long> g_YyCurrentOffset;
/// Allows any thread to look up the YY default location.
std::atomic<long> g_YyDefaultLocationOffset;

/// Allows any thread to look up the current picking pump volume.
std::atomic<float> g_PickingPumpVolume;
std::atomic<float> g_PickingPumpMaximumVolume;

/// THE state variable for hold states that pause operations.
std::atomic<HoldType> g_EsHoldState;
std::atomic<HoldType> g_PreviousHoldState;

/// THE state variable managing a temporary state during which queues are flushed. It behaves like a trigger.
std::atomic<bool> g_DoFlushQueues;

/// The means by which main() knows whether MqttReceiver is connected to a broker.
std::atomic<bool> g_ConnectedToBroker;

/// THE state vaiables maintaining current records of the states of various hardware components.
std::atomic<ComponentState> g_EsHwState; /// Summarizes the states of all of the following hardware components.
// std::atomic<ComponentState> g_CxDState;
std::atomic<ComponentState> g_MotionServicesLibState; /// For use in tracking initialization. This is never mocked.
std::atomic<ComponentState> g_XYState;
std::atomic<ComponentState> g_XXState;
std::atomic<ComponentState> g_YYState;
std::atomic<ComponentState> g_ZState;
std::atomic<ComponentState> g_ZZState;
std::atomic<ComponentState> g_ZZZState;
std::atomic<ComponentState> g_AspirationPump1State;
std::atomic<ComponentState> g_DispensePump1State;
std::atomic<ComponentState> g_DispensePump2State;
std::atomic<ComponentState> g_DispensePump3State;
std::atomic<ComponentState> g_PickingPump1State;
std::atomic<ComponentState> g_WhiteLightSourceState;
std::atomic<ComponentState> g_FluorescenceLightFilterState;
std::atomic<ComponentState> g_AnnularRingHolderState;
std::atomic<ComponentState> g_CameraState;
std::atomic<ComponentState> g_OpticalColumnState;
std::atomic<ComponentState> g_IncubatorState;
std::atomic<ComponentState> g_CxPMState;
std::atomic<ComponentState> g_BarcodeReaderState;
std::string m_BrokerIpAddressConfiguration = "localhost";
std::string m_SerialPortDevice_ADP = "/dev/ttyUSB0";
std::string g_SerialPortDevice_MICRO = "/dev/ttyACM0"; /// This is a guess. Micro and Camera might be swapped.
std::string g_SerialPortDevice_CAMERA = "/dev/ttyACM1";

/// Start switching to arrays for maintaining component property states
std::array<HardwareType, (int)ComponentType::FINAL_COMPONENT_TYPE_ENTRY> g_ComponentHardwareTypeArray{};

volatile sig_atomic_t g_signalReceived = 0;

void signal_handler( int signo, siginfo_t *sinfo, void *context )
{
	g_signalReceived = 1;
}

/// THE list of components that are free to move first during initialization.
/// (They can move first, because there is no risk of them hitting other components.)
/** Not used. Kept for documenting purposes.
std::array<ComponentType, 15> m_SafeToHomeArray =
{
	ComponentType::ZAxis,
	ComponentType::ZZAxis,
	ComponentType::ZZZAxis,
	ComponentType::AspirationPump1,
	ComponentType::DispensePump1,
	ComponentType::DispensePump2,
	ComponentType::DispensePump3,
	ComponentType::PickingPump1,
	ComponentType::WhiteLightSource,
	ComponentType::FluorescenceLightFilter,
	ComponentType::AnnularRingHolder,
	ComponentType::Camera,
	ComponentType::OpticalColumn, /// Focus only
	ComponentType::Incubator,
	ComponentType::BarcodeReader
};
**/

/// THE list of components that should wait to home after the other components are home.
/** Not used. Kept for documenting purposes.
std::array<ComponentType, 5> m_UnsafeToHomeArray =
{
	ComponentType::XYAxes,
	ComponentType::XXAxis,
	ComponentType::YYAxis,
	ComponentType::OpticalColumn, /// Turret only
	ComponentType::CxPM,
};
**/

/// THE mutex to use for error logs, alert logs, and any other logs.
std::mutex g_logFileMutex;
/// THE mutex for accessing the camera.
std::mutex g_cameraMutex;
/// THE Camera list. (All CommandDoler threads have access, but only 2 should be using it. They should use g_cameraMutex for access and only one should initialize and destroy it.)
std::vector<ES_CameraBase *> g_CameraList;
std::atomic<bool> g_CameraIsInitialized;
/// THE mutex for keeping the annular ring holder from homing while any Z axes are homing
std::mutex g_controllerBMutex;
/// THE mutex for controlling access to the mn::CppLinuxSerial::SerialPort object maintained by one Doler thread.
std::mutex g_SerialPortMutex;
/// THE mutex for controlling access to the Parker controllers during hardware checks.
std::mutex g_HardwareCheckMutex;

/// Worker thread instances (private)
MqttReceiverThread receivesFromMqtt( MQTT_RECEIVER );
CommandInterpreterThread interpretsCommand( COMMAND_INTERPRETER );
CommandDolerThread dolerForOptics( DOLER_FOR_OPTICS );
CommandDolerThread dolerForInfo( DOLER_FOR_INFO );
CommandDolerThread dolerForCxD( DOLER_FOR_CXD );
CommandDolerThread dolerForCxPM( DOLER_FOR_CXPM );
CommandDolerThread dolerForPump( DOLER_FOR_PUMP );
CommandDolerThread dolerForRingHolder( DOLER_FOR_RING_HOLDER );
MqttSenderThread sendsToMqtt( MQTT_SENDER );
std::array<CommandDolerThread *, 6> DolerThreadArray =
{
	&dolerForOptics,
	&dolerForInfo,
	&dolerForCxD,
	&dolerForCxPM,
	&dolerForPump,
	&dolerForRingHolder,
};

/// Message queues between worker threads (private)
HoldableMsgQueue* jobSchedule = new HoldableMsgQueue("JobsQ");
HoldableMsgQueue* msgQueueForOptics = new HoldableMsgQueue("DolerOpticsQ");
HoldableMsgQueue* msgQueueForInfo = new HoldableMsgQueue("DolerInfoQ");
HoldableMsgQueue* msgQueueForCxD = new HoldableMsgQueue("DolerCxDQ");
HoldableMsgQueue* msgQueueForCxPM = new HoldableMsgQueue("DolerCxPMQ");
HoldableMsgQueue* msgQueueForPump = new HoldableMsgQueue("DolerPumpQ");
HoldableMsgQueue* msgQueueForRingHolder = new HoldableMsgQueue("DolerRingHolderQ");
MsgQueue* outboundMqtt = new MsgQueue("OutboundQ");
/// IMPORTANT: This array should be maintained so its size and order always matches those of DolerThreadArray.
std::array<HoldableMsgQueue *, 6> DolerQueueArray =
{
	msgQueueForOptics,
	msgQueueForInfo,
	msgQueueForCxD,
	msgQueueForCxPM,
	msgQueueForPump,
	msgQueueForRingHolder,
};

MotionServices * g_motionServices_ptr = nullptr;

/* My current assumption is that the motion services object will be shared between CxD, Pump,
 *  and AnRH threads, thus requiring a mutex. This situation may change. */
std::mutex g_motionServicesMutex;

uint32_t g_sequenceId;

///----------------------------------------------------------------------
/// AllCommandThreadsAreWaitingOnQueues() (private)
/// Returns true when MsgQueues feeding the CommandInterpreter thread and all CommandDoler
/// threads are waiting for messages--implying that all of those threads are idle.
///----------------------------------------------------------------------
bool AllCommandThreadsAreWaitingOnQueues()
{
	if (jobSchedule->IsWaitingOnQueue() == false) return false;
	for ( auto it = DolerQueueArray.begin(); it != DolerQueueArray.end(); ++it )
	{
		if ((*it)->IsWaitingOnQueue() == false) return false;
	}
	return true;
}

string ComponentStateToString( ComponentState state )
{
	if (state == ComponentState::Uninitialized) return "Uninitialized";
	if (state == ComponentState::SettingsConfirmed) return "SettingsConfirmed";
	if (state == ComponentState::Homed1) return "Homed1";
	if (state == ComponentState::Homed2) return "Homed2";
	if (state == ComponentState::Homed3) return "Homed3";
	if (state == ComponentState::Good) return "Good";
	if (state == ComponentState::Faulty) return "Faulty";
	if (state == ComponentState::Mocked) return "Mocked";
	if (state == ComponentState::SizeOf) return "SizeOf";
	return "ERROR in ComponentStateToString";
}

///----------------------------------------------------------------------
/// ManageHoldStates
/// A public function that implements hold states.
/// Called periodically by main(), this function checks the values of
/// global HOLD state variables and updates internal hold states
/// in the message queues accordingly.
///
/// Occasionally called by MqttReceiver when to speed up the handling
/// of a kill_pending_operations command.
///
/// Note that message queues could, theoretically, watch the global states
/// themselves. We are not doing that because all message queues share
/// the same implementation and only some of them need to implement holds.
/// The outboundMqtt queue is one that must not go on hold.
///----------------------------------------------------------------------
std::mutex mangeHoldStateMutex;
void ManageHoldStates()
{
	unique_lock<mutex> lock(mangeHoldStateMutex);
//	cout << "main: ManageHoldStates" << endl;
	HoldType holdState = g_EsHoldState.load( std::memory_order_acquire );
	HoldType previousHoldState = g_PreviousHoldState.load( std::memory_order_acquire );
	if (holdState != previousHoldState)
	{
		g_PreviousHoldState.store( holdState, std::memory_order_relaxed );
		if (holdState == HoldType::NoHold_RunFree)
		{
//			cout << "main: HoldState = RunFree" << endl;
			unique_lock<mutex> lock2(g_holdMutex);
			g_holdCVA.notify_all();
		}
		if (holdState == HoldType::HoldButFinishCurrentWork)
		{
			cout << "main: HoldState = HoldButFinishCurrentWork" << endl;
			bool doFlushQueues = g_DoFlushQueues.load(std::memory_order_acquire);
			if (doFlushQueues)
			{
				cout << "main: flushing queues (1)" << endl;
				jobSchedule->DiscardAllMsgs();
				msgQueueForOptics->DiscardAllMsgs();
				for ( auto it = DolerQueueArray.begin(); it != DolerQueueArray.end(); ++it )
				{
					(*it)->DiscardAllMsgs();
				}
				; /// Do not discard the outboundMqtt queue.
				/// Reset the trigger.
				g_DoFlushQueues.store(false, std::memory_order_relaxed);
			}
		}
		if (holdState == HoldType::HoldUp_Freeze)
		{
			cout << "main: HoldState = HoldUp_Freeze" << endl;
			bool doFlushQueues = g_DoFlushQueues.load(std::memory_order_acquire);
			if (doFlushQueues)
			{
				cout << "main: flushing queues (2)" << endl;
				jobSchedule->DiscardAllMsgs();
				for ( auto it = DolerQueueArray.begin(); it != DolerQueueArray.end(); ++it )
				{
					(*it)->DiscardAllMsgs();
				}
				; /// Do not discard the outboundMqtt queue.
				/// Reset the trigger.
				g_DoFlushQueues.store(false, std::memory_order_relaxed);
			}
		}
	}
}

void RecordCompState(
	std::string componentName,
	ComponentState state,
	std::array<unsigned int, (int) ComponentState::SizeOf> & statesArray_out,
	int & numberOfActiveXsafeToMoves_out,
	int & numberOfHwStates_out,
	bool printTheStatus )
{
	statesArray_out[(int) state]++;
	if (state != ComponentState::Mocked)
	{
		numberOfActiveXsafeToMoves_out++;
	}
	numberOfHwStates_out++;
	if ((state != ComponentState::Mocked) && (state != ComponentState::Good) && printTheStatus)
	{
		cout << componentName << " state is " << ComponentStateToString( state ) << endl;
	}
}

///----------------------------------------------------------------------
/// Plans for future:
/// SummarizeHardwareStates
/// Sets EsState to a summary of hardware states. This means
/// EsState is Faulty if ANY hardware is faulty.
/// EsState is Mocked if ALL hardware is mocked.
/// EsState is Uninitialized if any hardware is Uninitialized.
/// else EsState is SettingsConfirmed if any hardware is SettingsConfirmed
/// else EsState is Homed1 if all of the following components are homed:
///		Z, ZZ, ZZZ, AnRH, all aspirate and dispense pumps, optical column focus
/// else EsState is Homed2 if all of the following components are homed:
///		XY, XX, YY, CxPM, optical column turret
/// else EsState is Homed3 if PickingPump1 is initialized.
/// EsState to Good if the previous conditions are not met.
///----------------------------------------------------------------------
std::mutex manageHardwareStateMutex;
ComponentState SummarizeHardwareStates( bool printTheStatus )
{
	int numberOfActiveSafeToMoves = 0;
	int numberOfActiveUnsafeToMoves = 0;
	int numberOfHwStates = 0;
	unique_lock<mutex> lock(manageHardwareStateMutex);
	ComponentState EsState = g_EsHwState.load( std::memory_order_acquire );
	std::array<unsigned int, (int) ComponentState::SizeOf> statesArray;
	statesArray.fill(0);
	
//	statesArray[(int) g_CxDState.load( std::memory_order_acquire )]++;

	/// g_MotionServicesLibState is not tracked

	RecordCompState( "XY", g_XYState.load( std::memory_order_acquire ), statesArray, numberOfActiveUnsafeToMoves, numberOfHwStates, printTheStatus );
	RecordCompState( "XX", g_XXState.load( std::memory_order_acquire ), statesArray, numberOfActiveUnsafeToMoves, numberOfHwStates, printTheStatus );
	RecordCompState( "YY", g_YYState.load( std::memory_order_acquire ), statesArray, numberOfActiveUnsafeToMoves, numberOfHwStates, printTheStatus );
	RecordCompState( "Z", g_ZState.load( std::memory_order_acquire ), statesArray, numberOfActiveSafeToMoves, numberOfHwStates, printTheStatus );
	RecordCompState( "ZZ", g_ZZState.load( std::memory_order_acquire ), statesArray, numberOfActiveSafeToMoves, numberOfHwStates, printTheStatus );
	RecordCompState( "ZZZ", g_ZZZState.load( std::memory_order_acquire ), statesArray, numberOfActiveSafeToMoves, numberOfHwStates, printTheStatus );
	RecordCompState( "Asp pump 1", g_AspirationPump1State.load( std::memory_order_acquire ), statesArray, numberOfActiveSafeToMoves, numberOfHwStates, printTheStatus );
	RecordCompState( "Disp pump 1", g_DispensePump1State.load( std::memory_order_acquire ), statesArray, numberOfActiveSafeToMoves, numberOfHwStates, printTheStatus );
	RecordCompState( "Disp pump 2", g_DispensePump2State.load( std::memory_order_acquire ), statesArray, numberOfActiveSafeToMoves, numberOfHwStates, printTheStatus );
	RecordCompState( "Disp pump 3", g_DispensePump3State.load( std::memory_order_acquire ), statesArray, numberOfActiveSafeToMoves, numberOfHwStates, printTheStatus );
	RecordCompState( "Picking pump 1", g_PickingPump1State.load( std::memory_order_acquire ), statesArray, numberOfActiveSafeToMoves, numberOfHwStates, printTheStatus );
	RecordCompState( "White light", g_WhiteLightSourceState.load( std::memory_order_acquire ), statesArray, numberOfActiveSafeToMoves, numberOfHwStates, printTheStatus );
	RecordCompState( "Fluorescence light", g_FluorescenceLightFilterState.load( std::memory_order_acquire ), statesArray, numberOfActiveSafeToMoves, numberOfHwStates, printTheStatus );
	RecordCompState( "Annular ring holder", g_AnnularRingHolderState.load( std::memory_order_acquire ), statesArray, numberOfActiveSafeToMoves, numberOfHwStates, printTheStatus );
	RecordCompState( "Camera", g_CameraState.load( std::memory_order_acquire ), statesArray, numberOfActiveSafeToMoves, numberOfHwStates, printTheStatus );

	ComponentState state = g_OpticalColumnState.load( std::memory_order_acquire );
	statesArray[(int) state]++;
	if (state != ComponentState::Mocked)
	{
		numberOfActiveSafeToMoves++;
		numberOfActiveUnsafeToMoves++;
	}
	numberOfHwStates++;
	if ((state != ComponentState::Mocked) && (state != ComponentState::Good) && printTheStatus)
	{
		cout << "Optical column is " << ComponentStateToString( state ) << endl;
	}

	RecordCompState( "Incubator", g_IncubatorState.load( std::memory_order_acquire ), statesArray, numberOfActiveSafeToMoves, numberOfHwStates, printTheStatus );
	RecordCompState( "CxPM", g_CxPMState.load( std::memory_order_acquire ), statesArray, numberOfActiveUnsafeToMoves, numberOfHwStates, printTheStatus );
	RecordCompState( "Barcode reader", g_BarcodeReaderState.load( std::memory_order_acquire ), statesArray, numberOfActiveSafeToMoves, numberOfHwStates, printTheStatus );
	
	if (statesArray[(int) ComponentState::Uninitialized] > 0)
	{
		EsState = ComponentState::Uninitialized;
		cout << "MAIN::SummarizeHardwareStates -- state is Uninitialized." << endl;
	}
	if ((EsState == ComponentState::Uninitialized) && (statesArray[(int) ComponentState::SettingsConfirmed] + statesArray[(int) ComponentState::Mocked] == numberOfHwStates))
	{
		EsState = ComponentState::SettingsConfirmed;
		if (printTheStatus)
		{
			cout << "MAIN::SummarizeHardwareStates -- state is SettingsConfirmed." << endl;
		}
	}
	if ((EsState == ComponentState::SettingsConfirmed) && (statesArray[(int) ComponentState::Homed1] == numberOfActiveSafeToMoves))
	{
		EsState = ComponentState::Homed1;
		if (printTheStatus)
		{
			cout << "MAIN::SummarizeHardwareStates -- state is Homed1." << endl;
		}
	}
	if ((EsState == ComponentState::Homed1) && (statesArray[(int) ComponentState::Homed2] + statesArray[(int) ComponentState::Mocked] == numberOfHwStates))
	{
		EsState = ComponentState::Homed2;
		if (printTheStatus)
		{
			cout << "MAIN::SummarizeHardwareStates -- state is Homed2." << endl;
		}
	}
	if ((EsState == ComponentState::Homed2) && (statesArray[(int) ComponentState::Homed3] + statesArray[(int) ComponentState::Mocked] == numberOfHwStates))
	{
		EsState = ComponentState::Homed3;
		if (printTheStatus)
		{
			cout << "MAIN::SummarizeHardwareStates -- state is Homed3." << endl;
		}
	}
	if (statesArray[(int) ComponentState::Good] + statesArray[(int) ComponentState::Mocked] == numberOfHwStates)
	{
		if (statesArray[(int) ComponentState::Good] > 0)
		{
			EsState = ComponentState::Good;
//			LOG_TRACE( "main", "init", "state is Good" );
		}
		else
		{
			EsState = ComponentState::Mocked;
//			LOG_TRACE( "main", "init", "state is Mocked" );
		}
	}
	if (statesArray[(int) ComponentState::Faulty] > 0)
	{
		EsState = ComponentState::Faulty;
		cout << "MAIN::SummarizeHardwareStates -- state is Faulty." << endl;
	}
	// TODO: Any cases not covered?
	if ((EsState != ComponentState::Good) && (EsState != ComponentState::Mocked) && printTheStatus)
	{
		cout << "Good:" << statesArray[(int) ComponentState::Good] << ", Uninit:" << statesArray[(int) ComponentState::Uninitialized] << ", Mocked:" << statesArray[(int) ComponentState::Mocked];
		cout << ", SConf:" << statesArray[(int) ComponentState::SettingsConfirmed] << ", Homed1:" << statesArray[(int) ComponentState::Homed1] << ", Homed2:" << statesArray[(int) ComponentState::Homed2];
		cout << ", Homed3:" << statesArray[(int) ComponentState::Homed3] << ", Faulty:" << statesArray[(int) ComponentState::Faulty] << ", NumStates:" << numberOfHwStates << endl;
	}
	g_EsHwState.store( EsState, std::memory_order_relaxed );
	
	long numWorkMsgs = 0L;
	{
		unique_lock<mutex> lock( g_numWorkMsgsMutex );
		numWorkMsgs = g_numWorkMsgs;
	}
	if (numWorkMsgs > 5)
	{
		cout << "MAIN::SummarizeHardwareStates -- WARNING! Number of work messages is " << numWorkMsgs << endl;
	}

	return EsState;
}

/// A public function for time syncronization.
bool SynchronizeTime( unsigned long unixTime )
{
	struct tm ts; /// Identical to rtc_time structure?
	time_t currentTime;
	time( &currentTime );
	time_t requestedTime = (time_t) unixTime;
	ts = *localtime( &requestedTime );
	cout << "MAIN::SynchronizeTime -- New date/time = " << ts.tm_year << "," << ts.tm_mon << "," << ts.tm_mday << "," << ts.tm_hour << ":" << ts.tm_min << "," << ts.tm_sec << endl;
	int fileDescriptor;
	bool succeeded = false;
	long deltaTime_sec = (long)requestedTime - (long)currentTime;
	stringstream msg;
	msg << "delta time is " << deltaTime_sec << " seconds.";
	LOG_TRACE( "main", "init", msg.str().c_str() );

	/// Try not to set the time every time we are asked.
	if (Abs(deltaTime_sec) < 2) // TODO: Consider using a smaller number.
	{
		return true;	// EARLY RETURN!!	EARLY RETURN!!
	}

	/// If all of the doler threads are waiting on their queues, they aren't doing work
	/// that could be adversely affected by this clock change.
	if ( msgQueueForOptics->IsWaitingOnQueue() &&
		 /// We don't check msgQueueForInfo because we can safely assume taht it is in the middle of calling this function.
		 msgQueueForCxD->IsWaitingOnQueue() &&
		 msgQueueForCxPM->IsWaitingOnQueue() &&
		 msgQueueForPump->IsWaitingOnQueue() &&
		 msgQueueForRingHolder->IsWaitingOnQueue() )
	{
//		cout << "MAIN::SynchronizeTime -- entered safe zone" << endl;
		/// NOTE: I considered setting g_EsHoldState to HoldUp_Freeze during this block, but rejected
		/// the idea because this work is being done on the dolerForInfo thread, and changing the hold
		/// state would create unwelcome race conditions with main()'s inner wait loop. Let's just hope
		/// that any commands that come in while we are in this block are too late to be adversely
		/// affected by the clock change.

		/// Change the real-time-clock. (Best effort will have to do.)
		fileDescriptor = open("/dev/rtc", O_RDONLY);
		ioctl(fileDescriptor, RTC_SET_TIME, &ts);
		close(fileDescriptor);
		/// ... and change the system clock.
		const struct timeval tv = {mktime(&ts), 0};
		succeeded = (settimeofday(&tv, 0) == 0); /// Report on success.
		if (succeeded)
		{
			LOG_TRACE( "main", "init", "succeeded" );
		}
		else
		{
			LOG_ERROR( "main", "init", "failed at settimeofday" );
		}
	}
	else
	{
		cout << "MAIN() failed to enter safe zone." << endl;
	}
	return succeeded;
}

/// Read a configuration file that tells us which components are to be mocked. Arrange for
/// them to be mocked, by setting their global ComponentState to Mocked.
/// Set all other global ComponentStates to the given defaultState.
void ConfigureComponentsPerConfig( ComponentState defaultState )
{
	string filePath = CONFIG_DIR;
	filePath += "/";
	filePath += ES_PARTS_JSON;
	string fileContentsStr = "";
	
//	g_CxDState.store( defaultState, std::memory_order_relaxed );
	/// g_MotionServicesLibState is not included here, because it is always
	/// initialized to ComponentState::Uninitialized in main()
	g_XYState.store( defaultState, std::memory_order_relaxed );
	g_XXState.store( defaultState, std::memory_order_relaxed );
	g_YYState.store( defaultState, std::memory_order_relaxed );
	g_ZState.store( defaultState, std::memory_order_relaxed );
	g_ZZState.store( defaultState, std::memory_order_relaxed );
	g_ZZZState.store( defaultState, std::memory_order_relaxed );
	g_AspirationPump1State.store( defaultState, std::memory_order_relaxed );
	g_DispensePump1State.store( defaultState, std::memory_order_relaxed );
	g_DispensePump2State.store( defaultState, std::memory_order_relaxed );
	g_DispensePump3State.store( defaultState, std::memory_order_relaxed );
	g_PickingPump1State.store( defaultState, std::memory_order_relaxed );
	g_WhiteLightSourceState.store( defaultState, std::memory_order_relaxed );
	g_FluorescenceLightFilterState.store( defaultState, std::memory_order_relaxed );
	g_AnnularRingHolderState.store( defaultState, std::memory_order_relaxed );
	g_CameraState.store( defaultState, std::memory_order_relaxed );
	g_OpticalColumnState.store( defaultState, std::memory_order_relaxed );
	g_IncubatorState.store( defaultState, std::memory_order_relaxed );
	g_CxPMState.store( defaultState, std::memory_order_relaxed );
	g_BarcodeReaderState.store( defaultState, std::memory_order_relaxed );

	if (FileExists( filePath )) {
		stringstream fileContents;
		fstream in( filePath.c_str(), std::ios::in );
		if (in.is_open())
		{
			fileContents << in.rdbuf();
		}
		in.close();
		fileContentsStr = fileContents.str();
		enum json_tokener_error jerr;
		json_object * fileContentsObj = json_tokener_parse_verbose( fileContentsStr.c_str(), &jerr );

		if (jerr == json_tokener_success)
		{
//			cout << "Main() parsed " << filePath << endl;
			if (json_object_get_type(fileContentsObj) == json_type_array)
			{
				size_t arrayLength = json_object_array_length( fileContentsObj );
				for (int i = 0; i < arrayLength; i++)
				{
					json_object * itemObj = json_object_array_get_idx( fileContentsObj, i );
					if (itemObj)
					{
						string componentName = json_object_get_string( json_object_object_get( itemObj, "component" ));

						bool toMock = false;
						json_object * mockObj = json_object_object_get( itemObj, "mock" );
						if (mockObj)
						{
							toMock = json_object_get_boolean( mockObj );
						}
						if (toMock && (defaultState != ComponentState::Mocked) )
						{
							if (componentName == ENUM_CXD)
							{
//								g_CxDState.store( ComponentState::Mocked, std::memory_order_relaxed );
								g_XYState.store( ComponentState::Mocked, std::memory_order_relaxed );
								LOG_TRACE( "main", "init", "XY mocked" );
								g_XXState.store( ComponentState::Mocked, std::memory_order_relaxed );
								g_YYState.store( ComponentState::Mocked, std::memory_order_relaxed );
								g_ZState.store( ComponentState::Mocked, std::memory_order_relaxed );
								LOG_TRACE( "main", "init", "Z mocked" );
								g_ZZState.store( ComponentState::Mocked, std::memory_order_relaxed );
								g_ZZZState.store( ComponentState::Mocked, std::memory_order_relaxed );
							}
							if (componentName == ENUM_XYAXES)
							{
								LOG_TRACE( "main", "init", "XY mocked" );
								g_XYState.store( ComponentState::Mocked, std::memory_order_relaxed );
							}
							if (componentName == ENUM_XXAXIS) g_XXState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_YYAXIS) g_YYState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_ZAXIS)
							{
								LOG_TRACE( "main", "init", "Z mocked" );
								g_ZState.store( ComponentState::Mocked, std::memory_order_relaxed );
							}
							if (componentName == ENUM_ZZAXIS) g_ZZState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_ZZZAXIS) g_ZZZState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_ASPIRATION_PUMP_1) g_AspirationPump1State.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_DISPENSE_PUMP_1) g_DispensePump1State.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_DISPENSE_PUMP_2) g_DispensePump2State.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_DISPENSE_PUMP_3) g_DispensePump3State.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_PICKING_PUMP_1) g_PickingPump1State.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_WHITE_LIGHT_SOURCE) g_WhiteLightSourceState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_FLUORESCENCE_LIGHT_FILTER) g_FluorescenceLightFilterState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_ANNULAR_RING_HOLDER) g_AnnularRingHolderState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_CAMERA) g_CameraState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_OPTICAL_COLUMN) g_OpticalColumnState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_INCUBATOR) g_IncubatorState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_CXPM) g_CxPMState.store( ComponentState::Mocked, std::memory_order_relaxed );
							if (componentName == ENUM_BARCODE_READER) g_BarcodeReaderState.store( ComponentState::Mocked, std::memory_order_relaxed );
						}
						if (componentName == "mqtt_broker")
						{
							json_object * ipAddrObj = json_object_object_get( itemObj, "ip_address" );
							if (ipAddrObj)
							{
								m_BrokerIpAddressConfiguration = json_object_get_string( ipAddrObj );
								stringstream msg;
								msg << "BrokerIpAddress=" << m_BrokerIpAddressConfiguration;
								LOG_TRACE( "main", "init", msg.str().c_str() );
							}
						}
						if (componentName == "serial_port_device")
						{
							json_object * ipAddrObj = json_object_object_get( itemObj, "device_file_name" );
							if (ipAddrObj)
							{
								m_SerialPortDevice_ADP = json_object_get_string( ipAddrObj );
								stringstream msg;
								msg << "Serial port file name =" << m_SerialPortDevice_ADP;
								LOG_TRACE( "main", "init", msg.str().c_str() );
							}
						}
					}
				}
			}
		}
		else
		{
			stringstream msg;
			msg << "BAD JSON in " << filePath;
			LOG_ERROR( "main", "init", msg.str().c_str() );
		}
		json_object_put( fileContentsObj ); /// Clear allocated memory.
	}
	else
	{
		stringstream msg;
		msg << "missing " << filePath;
		LOG_ERROR( "main", "init", msg.str().c_str() );
	}
}

/// Read a configuration file that tells us what vendor/model was installed for each component.
/// Record the data for branching to correct hardware-specific control software, as needed.
void ConfigureHardwarePerModelType( void )
{
	string filePath = CONFIG_DIR;
	filePath += "/";
	filePath += ES_HARDWARE_JSON;
	string fileContentsStr = "";
	/// Initialize all records of vendor/model to HardwareType::unknown. We will overwrite
	/// them later.
	for (int i = 0; i < (int)ComponentType::FINAL_COMPONENT_TYPE_ENTRY; i++)
	{
		g_ComponentHardwareTypeArray[i] = HardwareType::unknown;
	}

	if (FileExists( filePath )) {
		stringstream fileContents;
		fstream in( filePath.c_str(), std::ios::in );
		if (in.is_open())
		{
			fileContents << in.rdbuf();
		}
		in.close();
		fileContentsStr = fileContents.str();
		enum json_tokener_error jerr;
		json_object * fileContentsObj = json_tokener_parse_verbose( fileContentsStr.c_str(), &jerr );

		if (jerr == json_tokener_success)
		{
			cout << "Main() parsed " << filePath << endl;
			if (json_object_get_type(fileContentsObj) == json_type_array)
			{
				size_t arrayLength = json_object_array_length( fileContentsObj );
				cout << "es_hardware array length = " << arrayLength << endl;
				for (int i = 0; i < arrayLength; i++)
				{
					json_object * itemObj = json_object_array_get_idx( fileContentsObj, i );
					if (itemObj)
					{
						json_object * componentObj = json_object_object_get( itemObj, "component" );
						json_object * modelObj = json_object_object_get( itemObj, "model" );
						/// Process array elements that have "component" in them. Silently ignore others.
						if (componentObj)
						{
							string componentName = json_object_get_string( componentObj );
							cout << "Handling es_hardware component" << componentName << endl;
							ComponentType componentType = StringNameToComponentTypeEnum( componentName.c_str() );
							/// Process array elements that have "model" in them. Print a warning otherwise.
							if (modelObj)
							{
								string modelName = json_object_get_string( modelObj );
								HardwareType hardwareType = StringNameToHardwareTypeEnum( modelName.c_str() );
								stringstream msg;
								msg << "Component #" << i << " is " << componentName.c_str() << " model " << modelName.c_str() << " being set to hardware type " << (int)hardwareType;
								g_ComponentHardwareTypeArray[(int)componentType] = hardwareType;
							}
							else
							{
								stringstream msg;
								msg << "Component #" << i << " is being configured as 'unknown' due to lack of component-name or model";
								LOG_WARN( "main", "init", msg.str().c_str() );
								g_ComponentHardwareTypeArray[(int)componentType] = HardwareType::unknown;
							}
						}
					}
				}
			}
		}
		else
		{
			stringstream msg;
			msg << "BAD JSON in " << filePath;
			LOG_ERROR( "main", "init", msg.str().c_str() );
		}
		json_object_put( fileContentsObj ); /// Clear allocated memory.
	}
	else
	{
		stringstream msg;
		msg << "missing " << filePath << "| Configuring per hardware configuration of SN 1";
		LOG_WARN( "main", "init", msg.str().c_str() );
		g_ComponentHardwareTypeArray[(int)ComponentType::XYAxes] = HardwareType::ACR74V;
		g_ComponentHardwareTypeArray[(int)ComponentType::XXAxis] = HardwareType::ACR74V;
		g_ComponentHardwareTypeArray[(int)ComponentType::YYAxis] = HardwareType::ACR74V;
		g_ComponentHardwareTypeArray[(int)ComponentType::ZAxis] = HardwareType::ACR74T;
		g_ComponentHardwareTypeArray[(int)ComponentType::ZZAxis] = HardwareType::ACR74T;
		g_ComponentHardwareTypeArray[(int)ComponentType::ZZZAxis] = HardwareType::ACR74T;
		g_ComponentHardwareTypeArray[(int)ComponentType::AspirationPump1] = HardwareType::ACR74T;
		g_ComponentHardwareTypeArray[(int)ComponentType::DispensePump1] = HardwareType::ACR74T;
		g_ComponentHardwareTypeArray[(int)ComponentType::DispensePump2] = HardwareType::ACR74T;
		g_ComponentHardwareTypeArray[(int)ComponentType::DispensePump3] = HardwareType::ACR74T;
		g_ComponentHardwareTypeArray[(int)ComponentType::PickingPump1] = HardwareType::CAVRO_ADP;
		g_ComponentHardwareTypeArray[(int)ComponentType::WhiteLightSource] = HardwareType::IX85;
		g_ComponentHardwareTypeArray[(int)ComponentType::FluorescenceLightFilter] = HardwareType::IX85;
		g_ComponentHardwareTypeArray[(int)ComponentType::AnnularRingHolder] = HardwareType::ACR74T;
		g_ComponentHardwareTypeArray[(int)ComponentType::Camera] = HardwareType::R1_R2_R3;
		g_ComponentHardwareTypeArray[(int)ComponentType::OpticalColumn] = HardwareType::IX85;
		g_ComponentHardwareTypeArray[(int)ComponentType::Incubator] = HardwareType::unknown;
		g_ComponentHardwareTypeArray[(int)ComponentType::CxPM] = HardwareType::unknown;
		g_ComponentHardwareTypeArray[(int)ComponentType::BarcodeReader] = HardwareType::MATRIX_120;
	}
}

ComponentState ComponentStateAccordingToMotionServicesConfig( json_object * jobj, const char * compName )
{
	LOG_TRACE( "main", "init", "-" );
	json_object * controllersObj = json_object_object_get( jobj, "controllers" );
	bool compNameFound = false;
	if (controllersObj)
	{
		size_t controllersLength = json_object_array_length( controllersObj );
		size_t ii;
		for (ii = 0; ((ii < controllersLength) && (!compNameFound)); ii++)
		{
			json_object * controllerObj = json_object_array_get_idx( controllersObj, ii );
			if (controllersObj)
			{
				json_object * axesObj = json_object_object_get( controllerObj, "axes" );
				if (axesObj)
				{
					size_t axesLength = json_object_array_length( axesObj );
					size_t aa;
					for (aa = 0; ((aa < axesLength) && (!compNameFound)); aa++)
					{
						json_object * axisObj = json_object_array_get_idx( axesObj, aa );
						if (axesObj)
						{
							json_object * nameObj = json_object_object_get( axisObj, "name" );
							if (nameObj)
							{
								string nameStr = json_object_get_string( nameObj );
								if (nameStr == compName)
								{
									compNameFound = true;
									stringstream msg;
									msg << "found axis " << compName;
									LOG_DEBUG( "main", "init", msg.str().c_str() );
								}
							}
						}
					}
				}
			}
		}
	}
	if (compNameFound)
	{
		return ComponentState::Uninitialized;
	}
	stringstream msg;
	msg << "Did not find axis " << compName;
	LOG_WARN( "main", "init", msg.str().c_str() );
	return ComponentState::Mocked;
}

void MockComponentsNotSupportedByMotionServicesConfig( string msConfigfilePath )
{
	LOG_TRACE( "main", "init", "-" );
	stringstream fileContents;
	fstream in( msConfigfilePath.c_str(), std::ios::in );
	if (in.is_open())
	{
		fileContents << in.rdbuf();
	}
	in.close();
	string fileContentsStr = fileContents.str();
	enum json_tokener_error jerr;
	json_object * fileContentsObj = json_tokener_parse_verbose( fileContentsStr.c_str(), &jerr );

	if (jerr == json_tokener_success)
	{
		if (g_XYState.load( std::memory_order_acquire ) != ComponentState::Mocked)
		{
			ComponentState newStateY = ComponentStateAccordingToMotionServicesConfig( fileContentsObj, "Y" );
			ComponentState newStateX = ComponentStateAccordingToMotionServicesConfig( fileContentsObj, "X" );
			if ((newStateY == ComponentState::Mocked) || (newStateX == ComponentState::Mocked))
			{
				LOG_TRACE( "main", "init", "changing XY to mocked" );
				g_XYState.store( ComponentState::Mocked, std::memory_order_relaxed );
			}
		}
		if (g_XXState.load( std::memory_order_acquire ) != ComponentState::Mocked)
		{
			ComponentState newState = ComponentStateAccordingToMotionServicesConfig( fileContentsObj, "XX" );
			if (newState == ComponentState::Mocked)
			{
				LOG_TRACE( "main", "init", "changing XX to mocked" );
				g_XXState.store( ComponentState::Mocked, std::memory_order_relaxed );
			}
		}
		if (g_YYState.load( std::memory_order_acquire ) != ComponentState::Mocked)
		{
			ComponentState newState = ComponentStateAccordingToMotionServicesConfig( fileContentsObj, "YY" );
			if (newState == ComponentState::Mocked)
			{
				LOG_TRACE( "main", "init", "changing YY to mocked" );
				g_YYState.store( ComponentState::Mocked, std::memory_order_relaxed );
			}
		}
		if (g_ZState.load( std::memory_order_acquire ) != ComponentState::Mocked)
		{
			ComponentState newState = ComponentStateAccordingToMotionServicesConfig( fileContentsObj, "Z" );
			if (newState == ComponentState::Mocked)
			{
				LOG_TRACE( "main", "init", "changing Z to mocked" );
				g_ZState.store( ComponentState::Mocked, std::memory_order_relaxed );
			}
		}
		if (g_ZZState.load( std::memory_order_acquire ) != ComponentState::Mocked)
		{
			ComponentState newState = ComponentStateAccordingToMotionServicesConfig( fileContentsObj, "ZZ" );
			if (newState == ComponentState::Mocked)
			{
				LOG_TRACE( "main", "init", "changing ZZ to mocked" );
				g_ZZState.store( ComponentState::Mocked, std::memory_order_relaxed );
			}
		}
		if (g_ZZZState.load( std::memory_order_acquire ) != ComponentState::Mocked)
		{
			ComponentState newState = ComponentStateAccordingToMotionServicesConfig( fileContentsObj, "ZZZ" );
			if (newState == ComponentState::Mocked)
			{
				LOG_TRACE( "main", "init", "changing ZZZ to mocked" );
				g_ZZZState.store( ComponentState::Mocked, std::memory_order_relaxed );
			}
		}
		if (g_AspirationPump1State.load( std::memory_order_acquire ) != ComponentState::Mocked)
		{
			ComponentState newState = ComponentStateAccordingToMotionServicesConfig( fileContentsObj, "PN" );
			if (newState == ComponentState::Mocked)
			{
				LOG_TRACE( "main", "init", "changing PN to mocked" );
				g_AspirationPump1State.store( ComponentState::Mocked, std::memory_order_relaxed );
			}
		}
		if (g_DispensePump1State.load( std::memory_order_acquire ) != ComponentState::Mocked)
		{
			ComponentState newState = ComponentStateAccordingToMotionServicesConfig( fileContentsObj, "PP" );
			if (newState == ComponentState::Mocked)
			{
				LOG_TRACE( "main", "init", "changing PP to mocked" );
				g_DispensePump1State.store( ComponentState::Mocked, std::memory_order_relaxed );
			}
		}
		if (g_DispensePump2State.load( std::memory_order_acquire ) != ComponentState::Mocked)
		{
			ComponentState newState = ComponentStateAccordingToMotionServicesConfig( fileContentsObj, "PPP" );
			if (newState == ComponentState::Mocked)
			{
				LOG_TRACE( "main", "init", "changing PPP to mocked" );
				g_DispensePump2State.store( ComponentState::Mocked, std::memory_order_relaxed );
			}
		}
		if (g_DispensePump3State.load( std::memory_order_acquire ) != ComponentState::Mocked)
		{
			ComponentState newState = ComponentStateAccordingToMotionServicesConfig( fileContentsObj, "PPPP" );
			if (newState == ComponentState::Mocked)
			{
				LOG_TRACE( "main", "init", "changing PPPP to mocked" );
				g_DispensePump3State.store( ComponentState::Mocked, std::memory_order_relaxed );
			}
		}
		if (g_AnnularRingHolderState.load( std::memory_order_acquire ) != ComponentState::Mocked)
		{
			ComponentState newState = ComponentStateAccordingToMotionServicesConfig( fileContentsObj, "SH" );
			if (newState == ComponentState::Mocked)
			{
				LOG_TRACE( "main", "init", "changing SH to mocked" );
				g_PickingPump1State.store( ComponentState::Mocked, std::memory_order_relaxed );
			}
		}
		json_object_put( fileContentsObj ); /// Free memory.
	}
	else
	{
		stringstream msg;
		msg << "BAD JSON in " << msConfigfilePath;
		LOG_ERROR( "main", "init", msg.str().c_str() );
	}
}

///
/// Initialize the MotionServices library.
/// It is mocked IFF all the components that rely on it are mocked.
///
void InitMotionServicesIfNeeded( bool & failed )
{
	failed = false;
	ComponentState motionServicesLibState = g_MotionServicesLibState.load( std::memory_order_acquire );
	if (motionServicesLibState == ComponentState::Uninitialized)
	{
		bool AllMotionServicesAreMocked =
			( (g_XYState.load( std::memory_order_acquire ) == ComponentState::Mocked) &&
			  (g_XXState.load( std::memory_order_acquire ) == ComponentState::Mocked) &&
			  (g_YYState.load( std::memory_order_acquire ) == ComponentState::Mocked) &&
			  (g_ZState.load( std::memory_order_acquire ) == ComponentState::Mocked) &&
			  (g_ZZState.load( std::memory_order_acquire ) == ComponentState::Mocked) &&
			  (g_ZZZState.load( std::memory_order_acquire ) == ComponentState::Mocked) &&
			  (g_AspirationPump1State.load( std::memory_order_acquire ) == ComponentState::Mocked) &&
			  (g_DispensePump1State.load( std::memory_order_acquire ) == ComponentState::Mocked) &&
			  (g_DispensePump2State.load( std::memory_order_acquire ) == ComponentState::Mocked) &&
			  (g_DispensePump3State.load( std::memory_order_acquire ) == ComponentState::Mocked) &&
			  (g_AnnularRingHolderState.load( std::memory_order_acquire ) == ComponentState::Mocked) );

		/// If at least one component depends on the MotionServices AND is configured on,
		/// then try to initialize g_motionServices
		if (AllMotionServicesAreMocked)
		{
			LOG_TRACE( "main", "init", "Not Initializing MotionServices (not needed)" );
		}
		else
		{
			LOG_TRACE( "main", "init", "Initializing MotionServices" );
			string msConfigfilePath = CONFIG_DIR;
			msConfigfilePath += "/";
			msConfigfilePath += MOTION_SERVICES_CONFIG_JSON;
			if (FileExists( msConfigfilePath ))
			{
				MockComponentsNotSupportedByMotionServicesConfig( msConfigfilePath );
				this_thread::sleep_for(100ms);
				/// Initialize the MotionServices library.
				try {
					// TODO: Modify init() to return an error condition instead of exiting the whole application. Set failed on error.
					g_motionServices_ptr->init( msConfigfilePath, INITIALIZATION_TIMEOUT_MS );
					usleep(10000);
					motionServicesLibState = ComponentState::Good;
					g_MotionServicesLibState.store( motionServicesLibState, std::memory_order_relaxed );
					LOG_TRACE( "main", "init", "MotionServices initialized" );
				} catch (...) {
					LOG_ERROR( "main", "init", "Unable to initialize motion services. Check service and controllers." );
					motionServicesLibState = ComponentState::Faulty;
					g_MotionServicesLibState.store( motionServicesLibState, std::memory_order_relaxed );
					exit(-1);
				}
			}
			else
			{
				/// If the configuration file is missing, treat it like an override of the es_parts.json file
				LOG_WARN( "main", "init", "MotionServices config file missing" );
				motionServicesLibState = ComponentState::Faulty;
				g_MotionServicesLibState.store( motionServicesLibState, std::memory_order_relaxed );
				g_XYState.store( ComponentState::Mocked, std::memory_order_relaxed );
				g_XXState.store( ComponentState::Mocked, std::memory_order_relaxed );
				g_YYState.store( ComponentState::Mocked, std::memory_order_relaxed );
				g_ZState.store( ComponentState::Mocked, std::memory_order_relaxed );
				g_ZZState.store( ComponentState::Mocked, std::memory_order_relaxed );
				g_ZZZState.store( ComponentState::Mocked, std::memory_order_relaxed );
				g_AspirationPump1State.store( ComponentState::Mocked, std::memory_order_relaxed );
				g_DispensePump1State.store( ComponentState::Mocked, std::memory_order_relaxed );
				g_DispensePump2State.store( ComponentState::Mocked, std::memory_order_relaxed );
				g_DispensePump3State.store( ComponentState::Mocked, std::memory_order_relaxed );
				g_AnnularRingHolderState.store( ComponentState::Mocked, std::memory_order_relaxed );
			}
		}
	}
}

#ifdef USE_POPEN //	TODO: Deprecate and remove
int testPopenSnap()
{
	cout << "Moshimoshi" << endl;
	size_t imageSize = 0;
	void * buffer = nullptr;
	int status = ShellInvoker::InvokePopenSnap( true, "main", imageSize, &buffer );
	cout << "invokation done. imageSize = " << imageSize << endl;
	char * img = (char *) buffer;

	cout << "picture snapped" << endl;
	constexpr auto width = 2100u, height = 2100u;

	stringstream header;
	header << "P6\n" << width << ' ' << height << "\n255\n";
	string headerStr = header.str();
	int headerLength = headerStr.length();
	cout << "headerLength = " << headerLength << endl;
	char * headerCStr = new char[headerLength + 1]();
	strcpy( headerCStr, headerStr.c_str() );
	cout << headerCStr << endl;

	FILE * outFile = fopen( "popen_snap.ppm", "wb" );
	fwrite( (void *) headerCStr, 1, headerLength, outFile );

	char * buf = new char[4]();
	for (int i = 0; i < imageSize; i++)
	{
		buf[0] = img[i];
		buf[1] = img[i];
		buf[2] = img[i];
		fwrite( buf, 1, 3, outFile );
	}
	fclose( outFile );

	delete[] buf;
	delete[] headerCStr;
	cout << "picture written" << endl;
	free( buffer );
	cout << "buffer freed" << endl;
	return STATUS_OK;
}

int testPopenHand()
{
	string resultStr = "";
	int status = ShellInvoker::InvokePopenHand( NO_OP, "main", resultStr );
	status = ShellInvoker::InvokePopenHand( RED_ON, "main", resultStr );
	sleep( 2 );
	status = ShellInvoker::InvokePopenHand( RED_OFF, "main", resultStr );
	sleep( 2 );
	status = ShellInvoker::InvokePopenHand( GREEN_ON, "main", resultStr );
	sleep( 2 );
	status = ShellInvoker::InvokePopenHand( GREEN_OFF, "main", resultStr );
	sleep( 2 );
	status = ShellInvoker::InvokePopenHand( BLUE_ON, "main", resultStr );
	sleep( 2 );
	status = ShellInvoker::InvokePopenHand( BLUE_OFF, "main", resultStr );
	sleep( 2 );
	status = ShellInvoker::InvokePopenHand( BRIGHT_FIELD_ON, "main", resultStr );
	sleep( 2 );
	status = ShellInvoker::InvokePopenHand( BRIGHT_FIELD_OFF, "main", resultStr );
	sleep( 2 );
	cout << "InvokePopenHand() returned " << status << "and '" << resultStr << "'" << endl;
	return status;
}
#endif // USE_POPEN

#ifdef USE_PYLON
int testPylon()
{
	cout << "Testing pylon" << endl;
    /// The exit code of the sample application.
    int exitCode = 0;

    /// Before using any pylon methods, the pylon runtime must be initialized.
    PylonInitialize();

    try
    {
        /// Create an instant camera object with the camera device found first.
        CInstantCamera camera( CTlFactory::GetInstance().CreateFirstDevice() );
        
        INodeMap& nodemap = camera.GetNodeMap();
        /// Open the camera for accessing the parameters.
        camera.Open();

        CEnumParameter(nodemap, "TestImageSelector").SetValue("Off");
        CEnumParameter(nodemap, "ImageFileMode").SetValue("On");
        CStringParameter(nodemap, "ImageFilename").SetValue("/home/mark/Pictures/P5300045.png");
        CIntegerParameter width( nodemap, "Width" );
        CIntegerParameter height( nodemap, "Height" );
        cout << "Width            : " << width.GetValue() << endl;
        cout << "Height           : " << height.GetValue() << endl;

        /// Print the model name of the camera.
        cout << "Using device " << camera.GetDeviceInfo().GetModelName() << endl;

        /// The parameter MaxNumBuffer can be used to control the count of buffers
        /// allocated for grabbing. The default value of this parameter is 10.
        camera.MaxNumBuffer = 5;

        /// Start the grabbing of c_countOfImagesToGrab images.
        /// The camera device is parameterized with a default configuration which
        /// sets up free-running continuous acquisition.
        camera.StartGrabbing( c_countOfImagesToGrab );

        /// This smart pointer will receive the grab result data.
        CGrabResultPtr ptrGrabResult;

        /// Camera.StopGrabbing() is called automatically by the RetrieveResult() method
        /// when c_countOfImagesToGrab images have been retrieved.
        while (camera.IsGrabbing())
        {
            /// Wait for an image and then retrieve it. A timeout of 5000 ms is used.
            camera.RetrieveResult( 5000, ptrGrabResult, TimeoutHandling_ThrowException );

            /// Image grabbed successfully?
            if (ptrGrabResult->GrabSucceeded())
            {
                /// Access the image data.
                cout << "SizeX: " << ptrGrabResult->GetWidth() << endl;
                cout << "SizeY: " << ptrGrabResult->GetHeight() << endl;
                const uint8_t* pImageBuffer = (uint8_t*) ptrGrabResult->GetBuffer();
                cout << "Gray value of first pixel: " << (uint32_t) pImageBuffer[0] << endl << endl;
            }
            else
            {
                cout << "Error: " << std::hex << ptrGrabResult->GetErrorCode() << std::dec << " " << ptrGrabResult->GetErrorDescription() << endl;
            }
        }
    }
    catch (const GenericException& e)
    {
        /// Error handling.
        cerr << "An exception occurred." << endl
            << e.GetDescription() << endl;
        exitCode = 1;
    }

    /// Comment the following two lines to disable waiting on exit.
//    cerr << endl << "Press enter to exit." << endl;
//    while (cin.get() != '\n');

    /// Releases all pylon resources.
    PylonTerminate();

    return exitCode;
}
#endif /// USE_PYLON

int main(int argc, char **argv)
{
	struct sigaction act;
	memset( &act, 0, sizeof( struct sigaction ) );
	sigemptyset( &act.sa_mask );
	act.sa_sigaction = signal_handler;
	act.sa_flags = SA_SIGINFO;
	if (-1 == sigaction( SIGTERM, &act, NULL ))
	{
		LOG_ERROR( "main", "signals", "Failed to set handler for SIGTERM" );
		exit( EXIT_FAILURE );
	}
#ifdef USE_POPEN //	TODO: Deprecate and remove
	int testResult = testPopenSnap();
//	int testResult = testPopenHand();
	cout << "testResults=" << testResult << endl;
	sleep(2);
	cout << "exiting now."<< endl;
	return testResult;
#endif // USE_POPEN

#ifdef USE_PYLON
	int pylonResult = testPylon();
	sleep(10);
	cout << "exiting now."<< endl;
	return testResult;
#endif // USE_PYLON

#ifdef USE_PYWRAP
	PylonWrapper * pywrap = new PylonWrapper();
	cout << "pywrap version " << pywrap->GetVersion() << endl;
	if (pywrap->IsForReal())
	{
		cout << "pywrap implemented" << endl;
	}
	else
	{
		cout << "pywrap is mocked" << endl;
	}
	int width = pywrap->GetWidth();
	cout << "width = " << width << endl;
	delete pywrap;
	return 0;
#endif // USE_PYWRAP


    /// Initialize the USA Firmware Standard Library for Embedded Development (USAFW-SLED).
    SLED_init();

	Logger g_logger;
	MotionServices motionServices( g_logger );
	g_motionServices_ptr = &motionServices;

	g_ConnectedToBroker.store( false, std::memory_order_relaxed );
	InitializeSundry();
	g_sequenceId = 0;
	g_XxCurrentOffset.store( 0L, std::memory_order_relaxed );
	g_XxDefaultLocationOffset.store( 0L, std::memory_order_relaxed );
	g_YyCurrentOffset.store( 0L, std::memory_order_relaxed );
	g_YyDefaultLocationOffset.store( 0L, std::memory_order_relaxed );
	g_EsHoldState.store( HoldType::NoHold_RunFree, std::memory_order_relaxed );
	g_PreviousHoldState.store( HoldType::NoHold_RunFree, std::memory_order_relaxed );
	g_DoFlushQueues.store( false, std::memory_order_relaxed );
	g_EsHwState.store( ComponentState::Uninitialized, std::memory_order_relaxed );
	g_CameraIsInitialized.store( false, std::memory_order_relaxed );
	g_MotionServicesLibState.store( ComponentState::Uninitialized, std::memory_order_relaxed );
	ConfigureComponentsPerConfig( ComponentState::Uninitialized );
	ConfigureHardwarePerModelType();
	{
		unique_lock<mutex> lock( g_numWorkMsgsMutex );
		g_numWorkMsgs = 0L;
	}
	/// Make a single serial port and TecanADP object
	/// IFF they are needed. (The ADP and white light source need them.)
	cout << "here! "<< endl; usleep(10);
	mn::CppLinuxSerial::SerialPort * serialPortAdpPtr = nullptr;
	if ((g_PickingPump1State.load( std::memory_order_acquire ) !=  ComponentState::Mocked) ||
		(g_WhiteLightSourceState.load( std::memory_order_acquire ) !=  ComponentState::Mocked))
	{
		serialPortAdpPtr = new mn::CppLinuxSerial::SerialPort( m_SerialPortDevice_ADP,
															mn::CppLinuxSerial::BaudRate::B_9600,
															mn::CppLinuxSerial::NumDataBits::EIGHT,
															mn::CppLinuxSerial::Parity::NONE,
															mn::CppLinuxSerial::NumStopBits::ONE,
															mn::CppLinuxSerial::HardwareFlowControl::OFF,
															mn::CppLinuxSerial::SoftwareFlowControl::OFF );
	}
	TecanADP & adp = TecanADP::getInstance( serialPortAdpPtr );
	/// If that succeeded
	if (serialPortAdpPtr)
	{
		/// The doler for pumps does initialization and aspirate/dispense
		dolerForPump.m_serialPort = serialPortAdpPtr;
		dolerForPump.m_tecanADP = & adp;
		/// The doler for CxD ejects ADP tips as a part of Remove Tip
		dolerForCxD.m_serialPort = serialPortAdpPtr;
		dolerForCxD.m_tecanADP = & adp;
		cout << "SERIAL PORT OPENED." << endl;
	} /// else they remain null pointers.
	else
	{
		cout << "NULL SERIAL PORT. Probably mocking the picking pump" << endl;
	}
	cout << "and here! "<< endl; usleep(10);

	// "喂!" doesn't print well.
	cout << " Wèi!" << endl;
	cout << " wes " << ES_FIRMWARE_VERSION << endl;

	/// Check the hardware for concurrency support.
	unsigned int hwcon = std::thread::hardware_concurrency();

	/// Make sure all needed files and directories are in place.
	/// These should fail when a directory already exists.
	int retval = mkdir( CPD_DIR, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
	retval = mkdir( LOG_DIR, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
	retval = mkdir( PLATES_DIR, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
	retval = mkdir( TIPS_DIR, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
	
	/// Connect threads to queueus
	receivesFromMqtt.AttachCommandOutBox( jobSchedule );
	receivesFromMqtt.AttachAckOutBox( outboundMqtt );
	interpretsCommand.AttachInBox( jobSchedule );
	interpretsCommand.AttachOutBoxForOptics( msgQueueForOptics );
	interpretsCommand.AttachOutBoxForInfo( msgQueueForInfo );
	interpretsCommand.AttachOutBoxForCxD( msgQueueForCxD );
	interpretsCommand.AttachOutBoxForCxPM( msgQueueForCxPM );
	interpretsCommand.AttachOutBoxForPump( msgQueueForPump );
	interpretsCommand.AttachOutBoxForRingHolder( msgQueueForRingHolder );
	interpretsCommand.AttachStatusRouteBox( outboundMqtt );
	for (int i = 0; i < DolerThreadArray.size(); i++)
	{
		DolerThreadArray[i]->AttachInBox( DolerQueueArray[i] );
		DolerThreadArray[i]->AttachOutBox( outboundMqtt );
	}
	sendsToMqtt.AttachInBox( outboundMqtt );
	
	/// Set the static machineId.
	// TODO: Change this to read it from a file.
	uuid_t meGuid;
	uuid_generate_random( meGuid );
	char meGuidAsString[50] = {0};
	uuid_unparse_lower( meGuid, meGuidAsString );
	string machineId = meGuidAsString;
	
	///
	/// Set the dynamic parts of MQTT configuration
	///
	int count = 240; /// 4 minutes
	string brokerIpAddress = m_BrokerIpAddressConfiguration;
	if (brokerIpAddress == "localhost")
	{
		/// Keep poling until we detect the host's IP address.
		do {
			count--;
			this_thread::sleep_for(1s);
			brokerIpAddress = DetectHostIpAddress();
		} while ((brokerIpAddress == "") && (count > 0));
	}
	{
		stringstream msg;
		msg << "brokerIP_address=" << brokerIpAddress;
		LOG_DEBUG( "main", "main", msg.str().c_str() );
	}
	{
		stringstream msg;
		msg << "Count=" << (240-count) << " seconds waiting for IP address";
		LOG_DEBUG( "main", "main", msg.str().c_str() );
	}
	receivesFromMqtt.SetBrokerIpAddress( brokerIpAddress );
	sendsToMqtt.SetBrokerIpAddress( brokerIpAddress );
	receivesFromMqtt.SetMachineId(machineId);
	{
		stringstream msg;
		msg << "machineId=" << machineId;
		LOG_DEBUG( "main", "main", msg.str().c_str() );
	}
	interpretsCommand.SetMachineId( machineId );
	for ( auto it = DolerThreadArray.begin(); it != DolerThreadArray.end(); ++it )
	{
		(*it)->SetMachineId( machineId );
	}
	dolerForOptics.SetMachineId( machineId ); // TODO: This may be redundant. Confirm and delete.
	dolerForOptics.SetBrokerIpAddress( brokerIpAddress ); /// Do this before calling CreateThread().
	string masterId = ""; /// No master client at boot time.
	receivesFromMqtt.SetMasterId(masterId);
	
	///
	/// Initialize the MotionServices library.
	///
	bool failedToInitMS = false;
	InitMotionServicesIfNeeded( failedToInitMS );
	// TODO: Exit nicely if failedToInitMS

	///
	/// Create and start all worker threads
	///
	receivesFromMqtt.CreateThread();
	interpretsCommand.CreateThread();
	/// Wait until receivesFromMqtt confirms it has connected to the broker.
	/// Then we can be sure that the doler for optics can connect too, during CreateThread().
	do
	{
		cout << "Waiting to connect to MQTT broker" << endl;
		this_thread::sleep_for(100ms);
	} 	while (!g_ConnectedToBroker.load( std::memory_order_acquire ));

	for ( auto it = DolerThreadArray.begin(); it != DolerThreadArray.end(); ++it )
	{
		(*it)->CreateThread();
	}
	sendsToMqtt.CreateThread();
	
	///
	/// A short loop
	/// Wait for all threads to report that they are either initialized or mocked.
	///
	ComponentState esState = ComponentState::Uninitialized;
	ComponentState previousState = ComponentState::SizeOf; /// Initialize to invalid value.
	count = 0;
	while ((esState != ComponentState::Good) && (esState != ComponentState::Mocked) && (g_signalReceived == 0))
	{
		bool printStatus = (count % 20 == 0);
		esState = SummarizeHardwareStates( printStatus );
		if (esState == previousState)
		{
			/// Provide time for the Doler thread to do the previous work it was given.
			this_thread::sleep_for(500ms);
			if (printStatus)
			{
				cout << "Main(): Napping " << ComponentStateToString( esState ) << " count= " << count << endl;
			}
			count++;
			if (count > 600)
			{
				cout << "Main(): esState=Faulty. Wes is going into time-out." << endl;
				g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed );
				interpretsCommand.SendAlertMessage(
					0, /// commandGroupId
					0, /// commandId
					"", /// sessionId
					ComponentType::ComponentNone,
					STATUS_INIT_FAILED,
					true ); ///  thisIsCausingHold
				while (g_signalReceived == 0)
				{
					this_thread::sleep_for(100ms);
				}
			}
		}
		else
		{
			count = 0;
			// TODO: Decide what to do when a component is Faulty. Other components should not initialize.
			// TODO: Some faults should put the ES on HOLD.
			previousState = esState;
			this_thread::sleep_for(100ms);
			switch (esState)
			{
				case ComponentState::Uninitialized:
					cout << "Main(): esState=Uninitialized" << endl;
					interpretsCommand.InitializeHwComponents();
					break;
				case ComponentState::SettingsConfirmed:
					cout << "Main(): esState=SettingsConfirmed" << endl;
					interpretsCommand.HomeAllSafeHwComponents(); /// Z, ZZ, ZZZ, AspirationPump1, DispensePump1, DispensePump2, DispensePump3, PickingPump1,
																 /// WhiteLightSource, FluorescenceLightFilter, AnnularRingHolder, Camera,
																 /// OpticalColumn(focus), Incubator, and BarcodeReader
					break;
				case ComponentState::Homed1:
					cout << "Main(): esState=Homed1" << endl;
					interpretsCommand.HomeAllUnsafeHwComponents(); /// XY, XX, YY, OpticalColumn(turret), and CxPM.
					break;
				case ComponentState::Homed2:
					cout << "Main(): esState=Homed2" << endl;
					interpretsCommand.HomePickingPump1(); /// PPInit
					break;
				case ComponentState::Homed3:
					cout << "Main(): esState=Homed3" << endl;
					interpretsCommand.RestoreAllHwComponentsToLastActiveStates(); /// lights and camera
					break;
				case ComponentState::Faulty:
					cout << "Main(): esState=Faulty. Wes is going into time-out." << endl;
					g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed );
					interpretsCommand.SendAlertMessage(
						0, /// commandGroupId
						0, /// commandId
						"", /// sessionId
						ComponentType::ComponentNone,
						STATUS_INIT_FAILED,
						true ); ///  thisIsCausingHold
					while (g_signalReceived == 0)
					{
						this_thread::sleep_for(100ms);
					}
					interpretsCommand.RestoreAllHwComponentsToLastActiveStates();
					break;
				default:
					break;
			}
		}
	}
	cout << "Main(): Done with initializations." << endl;

	///
	/// MAIN loop
	///
	while (g_signalReceived == 0)
	{
		ManageHoldStates();
		esState = SummarizeHardwareStates( false );

		// TODO: Figure out a path to reconnect with the broker if (!g_ConnectedToBroker.load( std::memory_order_acquire ));
		this_thread::sleep_for(1s);

		// TODO: Delete this temp code
		string endPath = "/tmp/end";
		if (FileExists( endPath ))
		{
			if (unlink(endPath.c_str()) != 0)
			{
				cout << "Failed to delete " << endPath << endl;
			}
			g_signalReceived = 2;
		}

		// TODO: Delete this temp code
		string clearPath = "/tmp/clear";
		if (FileExists( clearPath ))
		{
			if (unlink(clearPath.c_str()) != 0)
			{
				cout << "Failed to delete " << clearPath << endl;
			}
			interpretsCommand.ClearKills();
		}

	}

	///
	/// Shut down all worker threads
	///
	cout << "Shutting down receivesFromMqtt" << endl;
	receivesFromMqtt.SendDisconnectAlert();
	this_thread::sleep_for(100ms);
	receivesFromMqtt.ExitThread();
	cout << "Shutting down interpretsCommand" << endl;
	interpretsCommand.ExitThread();
	int threadNum = 0;
	for ( auto it = DolerThreadArray.begin(); it != DolerThreadArray.end(); ++it )
	{
		cout << "Shutting down doler thread" << threadNum << endl;
		(*it)->ExitThread();
		threadNum++;
	}
	cout << "Shutting down sendsToMqtt" << endl;
	sendsToMqtt.ExitThread();

	/// Close down the serial port.
	if (serialPortAdpPtr)
	{
		serialPortAdpPtr->Close();
		delete serialPortAdpPtr;
	}

	///
	/// Investigate potential memory loss
	///
	long numWorkMsgs = 0L;
	{
		unique_lock<mutex> lock( g_numWorkMsgsMutex );
		numWorkMsgs = g_numWorkMsgs;
	}
	cout << "MAIN::main -- DEBUG: Number of work messages is " << numWorkMsgs << endl;

	if (numWorkMsgs > 0L)
	{
		cout << "MAIN::main -- DEBUG: jobSchedule queue still contains " << jobSchedule->GetQueueSize() << " work messages." << endl;
		cout << "MAIN::main -- DEBUG: msgQueueForOptics still contains " << msgQueueForOptics->GetQueueSize() << " work messages." << endl;
		cout << "MAIN::main -- DEBUG: msgQueueForInfo still contains " << msgQueueForInfo->GetQueueSize() << " work messages." << endl;
		cout << "MAIN::main -- DEBUG: msgQueueForCxD still contains " << msgQueueForCxD->GetQueueSize() << " work messages." << endl;
		cout << "MAIN::main -- DEBUG: msgQueueForCxPM still contains " << msgQueueForCxPM->GetQueueSize() << " work messages." << endl;
		cout << "MAIN::main -- DEBUG: msgQueueForPump still contains " << msgQueueForPump->GetQueueSize() << " work messages." << endl;
		cout << "MAIN::main -- DEBUG: msgQueueForRingHolder still contains " << msgQueueForRingHolder->GetQueueSize() << " work messages." << endl;
		cout << "MAIN::main -- DEBUG: outboundMqtt still contains " << outboundMqtt->GetQueueSize() << " work messages." << endl;
	}

	for (int i=0; i<6; i++)
	{
		DolerThreadArray[i] = nullptr;
	}

	/// Un-init MotionServices.
	if  (g_MotionServicesLibState.load( std::memory_order_acquire ) != ComponentState::Uninitialized)
	{
		motionServices.shutdown(5000);
		g_MotionServicesLibState.store( ComponentState::Uninitialized, std::memory_order_relaxed );
		usleep(1000000);
	}
	g_motionServices_ptr = nullptr;

    /// Shut down the USA Firmware Standard Library for Embedded Development (USAFW-SLED).
	cout << "Shutting down SLED..." << endl;
    SLED_shutdown();
	cout << "Done." << endl;

	return 0;
}
