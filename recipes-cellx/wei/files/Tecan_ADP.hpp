
///----------------------------------------------------------------------
/// Tecan_ADP
/// A module for controlling the Tecan Air Displacement Pipettor (ADP)
///----------------------------------------------------------------------
#ifndef _TECAN_ADP_H
#define _TECAN_ADP_H

#include "CppLinuxSerial/SerialPort.hpp"
#include "CppLinuxSerial/Exception.hpp"
#include <vector>
#include <string.h>

///----------------------------------------------------------------------
/// TecanADP
/// Provides low level commands to the Tecan Air Displacement Pipettor
///
/// See ClassTecanADP(Code) of SerialMediatorTecanADP_001.xlsm for
/// the model on which this class is based.
///
/// This class is implemented as a singleton, because it controls
/// access to a single serial port object.
///----------------------------------------------------------------------

// #define gcSERIAL_CHECK_INTERVAL			(1)		// Seconds
// #define gcCOMMAND_COMPLETION_TIMEOUT	(10)	// Seconds

#define cCOMMAND_START_CODE				(0xff)
#define cCOMMAND_START_DELIMITER		(2)		// STX
#define cCOMMAND_END_DELIMITER			(3)		// ETX
#define cPARAMETER_DELIMITER			","
#define cPARAMETER_1_PLACEHOLDER		"##1"
#define cPARAMETER_2_PLACEHOLDER		"##2"
#define cRESPONSE_DATA_START_POSITION	(4)
#define cRESPONSE_DATA_TAIL_LENGTH		(2)
#define cSTATUS_BYTE_LOCATION			(4)
// #define cSTANDARD_WAIT_USECONDS			(10000)

// #define cSPEED_FORMAT_STRING					"0.000"
// #define cVOLUME_FORMAT_STRING					"0.000"

#define cmCOMMAND_RESET_PUMP					"!"
#define cmCOMMAND_GET_PUMP_PART_NUMBER			"&0"
#define cmCOMMAND_GET_PUMP_FIRMWARE_VERSION		"&1"
#define cmCOMMAND_GET_PUMP_SERIAL_NUMBER		"&2"

#define cmCOMMAND_QUERY_CURRENT_VOLUME_UL		"?3"
#define cmCOMMAND_QUERY_MAXIMUM_VOLUME_UL		"?17"
#define cmCOMMAND_QUERY_START_SPEED_UL_PER_SEC	"?18"
#define cmCOMMAND_QUERY_TOP_SPEED_UL_PER_SEC	"?19"
#define cmCOMMAND_QUERY_CUTOFF_SPEED_UL_PER_SEC	"?20"
#define cmCOMMAND_QUERY_TIP_PRESENCE			"?31"

#define cmCOMMAND_QUERY_STATUS					"Q1"

#define cmCOMMAND_EJECT_TIP_AT_INITIALIZE_ONLY	"E0R"
#define cmCOMMAND_EJECT_TIP						"E1R"
#define cmCOMMAND_INITIALIZE_PLUNGER			"WR"
#define cmCOMMAND_CLOSE							",1R"
#define cmCOMMAND_SET_TOP_SPEED_UL_PER_SEC		"V"
#define cmCOMMAND_SET_START_SPEED_UL_PER_SEC	"v"
#define cmCOMMAND_SET_CUTOFF_SPEED_UL_PER_SEC	"c"

#define cmCOMMAND_MOVE_ABSOLUTE_POSITION_UL		"A"
#define cmRESERVED_MAXIMUM_VOLUME_NL			"10000"		// Volume less than the maximum volume to hold as "safety" reserve
#define cmEXPULSION_CHASE_VOLUME_NL				(0)			// Volume to be aspirated after full expulsion to ensure full contents are expulsed upon next expulsion
// #define cmEXPULSION_CHASE_VOLUME_NL = 50000	 'Volume to be aspirated after full expulsion to ensure full contents are expulsed upon next expulsion

#define cmDEVICE_IS_NOT_INITIALIZED_STRING		"G"

#define cmERROR_CODE_NO_ERROR					 (0)
#define cmERROR_CODE_INITIALIZATION_ERROR		 (1)
#define cmERROR_CODE_INVALID_COMMAND			 (2)
#define cmERROR_CODE_INVALID_OPERAND			 (3)
#define cmERROR_CODE_PRESSURE_SENSOR_PROBLEM	 (4)
#define cmERROR_CODE_OVER_PRESSURE				 (5)
#define cmERROR_CODE_LIQUID_LEVEL_DETECT_FAILURE (6)
#define cmERROR_CODE_DEVICE_NOT_INITIALIZED		 (7)
#define cmERROR_CODE_TIP_EJECT_FAILURE			 (8)
#define cmERROR_CODE_PLUNGER_OVERLOAD			 (9)
#define cmERROR_CODE_TIP_LOST_OR_NOT_PRESENT	 (10)
#define cmERROR_CODE_PLUNGER_MOVE_NOT_ALLOWED	 (11)
#define cmERROR_CODE_EXTENDED_ERROR				 (12)
#define cmERROR_CODE_NVMEM_ACCESS_FAILURE		 (13)
#define cmERROR_CODE_COMMAND_BUFFER_PROBLEM		 (14)
#define cmERROR_CODE_COMMAND_BUFFER_OVERFLOW	 (15)

enum class EventType {
	None,
	CommandSent,
	ResponseReceived,
	ParsedResponse,
	BusinessLogicError,
	DeviceError,
	Comm,
	ResponseChecksumError,
};

enum class CommEventType {
	None,
	CommOpened,
	CommClosed,
	CommFailedToOpen,
	CommFailedToClose,
	CommSendSuccess,
	CommSendFailure,
	CommReceiveSuccess,
	CommReceiveFailure,
	CommChecksumError,
	CommTimeoutError,
};

enum class ADPClassErrorCodes {
	NoError,
	CommandStringError,
	DeviceBusyTimeout,
	ParameterLimitsExceeded,
	TopSpeedTooSmall,
	StartSpeedTooBig,
	CutoffSpeedTooBig,
	PumpWorkingVolumeViolation,
	TipWorkingVolumeViolation,
};

struct EventRecord {
	EventType m_Type;
	CommEventType m_Code;
	ADPClassErrorCodes m_Error;
	std::string m_Text; /// For holding full response messages (complete with 0xff and checksum)
	std::string m_Payload; /// For holding optional string daya pulled out of m_Text.
	EventRecord();
	EventRecord( EventType type, CommEventType code, ADPClassErrorCodes error, const char * text, mn::CppLinuxSerial::Exception & e );
};

/// This is a work-around. I couldn't figure out how to pass a vector<string> object by reference.
/// But I can pass it by reference if it is in a structure.
struct BoxOfErrors {
	std::vector<std::string> errors;
};


class TecanADP
{
public:
	static TecanADP & getInstance( mn::CppLinuxSerial::SerialPort * serialPort )
	{
		static TecanADP instance(serialPort);
		return instance;
	}
	/// Destructor
	~TecanADP();

	static const int CommonLongLoopCount;
	static const long StandardWait_uS;
	static const float TopSpeedMin_uL;
	static const float TopSpeedMax_uL;
	static const float StartSpeedMin_uL;
	static const float StartSpeedMax_uL;
	static const float CutOffSpeedMin_uL;
	static const float CutOffSpeedMax_uL;
	int InitializeDevice( const char * threadName_in, int & stdError_out );
	int EjectTip( const char * threadName_in, int & stdError_out );
	int EjectTipAtInitialization( const char * threadName_in, int & stdError_out );
	int MoveAbsolutePosition( const char * threadName_in, float position_uL_in, int & stdError_out );
	int IsDeviceInitialized( const char * threadName_in, bool & isBusy, bool & hasErrors, bool & isInitialized_out );
	/// A more generic command relying on "Q1". Returns is-busy and a list of all standard or extended errors as strings.
	int GetBusy( const char * threadName_in, bool & isBusy, BoxOfErrors & box );
	int WaitForNotBusy( const char * threadName_in, const char * functionName );
	int SetTopSpeed( const char * threadName_in, float topSpeed_uL_per_sec_in, int & stdError_out );
	int SetStartSpeed( const char * threadName_in, float startSpeed_uL_per_sec_in, int & stdError_out );
	int SetCutoffSpeed( const char * threadName_in, float cutOffSpeed_uL_per_sec_in, int & stdError_out );
	int GetSpeed( const char * threadName_in, long & topSpeed_NlPerSec_out );
	int GetPumpPartNumber( const char * threadName_in, std::string & pumpPartNumber_out );
	int GetPumpFirmwareVersion( const char * threadName_in, std::string & pumpFirmwareVersion_out );
	int GetPumpSerialNumber( const char * threadName_in, std::string & pumpSerialNumber_out );
	int GetPumpCurrentVolume ( const char * threadName_in, std::string & pumpCurrentVolume_out );
	int GetPumpMaximumVolume ( const char * threadName_in, std::string & pumpMaximumVolume_out );
    int GetPumpStartSpeed( const char * threadName_in, std::string & pumpStartSpeed_out );
    int GetPumpTopSpeed( const char * threadName_in, std::string & pumpTopSpeed_out );
    int GetPumpCutoffSpeed( const char * threadName_in, std::string & pumpCutoffSpeed_out );

	EventRecord ExecuteCommand( const char * threadName_in, const std::string baseCommand_in );
	int IsTipPresent( const char * threadName_in, bool & tipIsPresent_out );
	int ClosePort( const char * threadName_in );

//private:
	std::string BuildCommand( const std::string & baseCommand );
	EventRecord SendMessageToDevice( const char * threadName_in, std::string message_in );
	EventRecord ReadMessageFromDevice( const char * threadName_in );
	char GetChecksum( std::string str );
//	std::string ExtractResponsePayload( const std::string & responseString );
	void printInHex(std::string str);
	int ExtractBusyAndStdError( const char * threadName_in, const std::string response_in, bool & busy_out, int & stdError_out );
	int ExtractExtendedErrors( const char * threadName_in, const std::string response_in, std::string & extendedEerrorList_out );
	int ExtractPayload( const char * threadName_in, const std::string response_in, std::string & payload_out );
	std::string ADPClassErrorCodesToString( ADPClassErrorCodes code );
	std::string StandardErrorCodesToString( int stdError );

	/// Constructors
	TecanADP();
	TecanADP( mn::CppLinuxSerial::SerialPort * serialPort );
	TecanADP( const TecanADP& ) = delete;
	TecanADP& operator=( const TecanADP& ) = delete;

	/// For communicating with the TecanADP and ...
	mn::CppLinuxSerial::SerialPort * m_serialPort;
	bool m_isInitialized;
	bool m_tipIsPresent;
	long m_topSpeed;
	long m_startSpeed;
	long m_cutOffSpeed;
	int m_commandSequence;
	int m_pumpID;
	EventRecord m_lastError;
};

extern std::vector<std::string> extendedErrorCodes;
#endif
