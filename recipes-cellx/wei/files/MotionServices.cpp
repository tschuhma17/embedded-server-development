/* src/MotionServices.cpp */

#include <fstream>
#include <string>
#include <sstream>

#include "thirdparty/json11/json11.hpp"

#include "USAFW/exceptions/LogicExceptions.hpp"

#include "controllers/ACR7000.hpp"
#include "exceptions/MotionException.hpp"
#include "MotionServices.hpp"

using namespace json11;

namespace USAFW {

    MotionServices::MotionServices(Logger &loggerP) :
        m_logger(loggerP)
    {
        ;
    }

    MotionServices::~MotionServices() {
        ;
    }

    void MotionServices::init(std::string config_file_pathP, unsigned int timeout_msec_P) {
        // Load configuration file.
        std::ifstream config_file(config_file_pathP);
        std::stringstream config_buffer;
        config_buffer << config_file.rdbuf();

        m_logger.info("Reading configuration data from " + config_file_pathP);

        // Parse configuration file.
        std::string err;
        const auto config_json=Json::parse(config_buffer.str(), err);

        for (auto &controller_json : config_json["controllers"].array_items() ) {
            ACR7000 *pController=NULL;

            // VALIDATE

            if(controller_json["axes"].is_null()) {
                m_logger.fatal("Required configuration value missing: controllers[]::axes");
                THROW(BadConfiguration);
            }

            if(controller_json["name"].is_null()) {
                m_logger.fatal("Required configuration value missing: controllers[]::name");
                THROW(BadConfiguration);
            }

            if(controller_json["transports"].is_null()) {
                m_logger.fatal("Required configuration value missing: controllers[]::transports");
                THROW(BadConfiguration);
            }

            if(controller_json["type"].is_null()) {
                m_logger.fatal("Required configuration value missing: controllers[]::type");
                THROW(BadConfiguration);
            }

            // EXECUTE

            if (controller_json["type"].string_value().compare("ACR7000")==0) {
                if(controller_json["ipv4addr"].is_null()) {
                    m_logger.fatal("Required configuration value missing: controllers[]::ipv4addr");
                    THROW(BadConfiguration);
                }

                pController = new ACR7000(*this, controller_json["name"].string_value(),
                                            controller_json["ipv4addr"].string_value());
            }

            std::vector<MotionAxis *> *p_axes = new std::vector<MotionAxis *>();
            for (auto &axis_json : controller_json["axes"].array_items() ) {
                // VALIDATE
                if(axis_json["name"].is_null()) {
                    m_logger.fatal("Required configuration value missing: axes[]::name");
                    THROW(BadConfiguration);
                }

                if(axis_json["index"].is_null()) {
                    m_logger.fatal("Required configuration value missing: axes[]::index");
                    THROW(BadConfiguration);
                }

                // EXECUTE
                // TODO: Add "master" to the configuration and use it here, in place of 0.
                MotionAxis *p_axis = new MotionAxis(*pController, axis_json["name"].string_value(), axis_json["index"].int_value(), 0);

                // Add the axis to the controller-specific list.
                m_logger.info("MotionServices::init() Pushing axis '" + axis_json["name"].string_value() + "'");
                p_axes->push_back(p_axis);
            }

            std::vector<Transport *> *p_transports = new std::vector<Transport *>();
            for (auto &transport_json : controller_json["transports"].array_items() ) {
                // VALIDATE
                if(transport_json["name"].is_null()) {
                    m_logger.fatal("Required configuration value missing: transports[]::name");
                    THROW(BadConfiguration);
                }

                if(transport_json["axes"].is_null()) {
                    m_logger.fatal("Required configuration value missing: transports[]::axes");
                    THROW(BadConfiguration);
                }

                // EXECUTE
                std::vector<MotionAxis *> *p_transport_axes = new std::vector<MotionAxis *>();

                for (auto &transport_axis_json : transport_json["axes"].array_items() ) {
                    // VALIDATE
                    if(transport_axis_json["name"].is_null()) {
                        m_logger.fatal("Required configuration value missing: transports[]::axes[]::name");
                        THROW(BadConfiguration);
                    }

                    // Find the axis that matches the name.
                    bool found=false;
                    for (auto axes_iter = p_axes->begin();
                            ((found==false)&&(axes_iter != p_axes->end()));
                            ++axes_iter)
                    {
                        if ((*axes_iter)->getName().compare(transport_axis_json["name"].string_value())==0) {
							m_logger.info("MotionServices::init() Pushing transport axis '" + transport_axis_json["name"].string_value() + "' for axis '" + (*axes_iter)->getName() + "'");
                            p_transport_axes->push_back(*axes_iter);
                            found=true;
                        }
                    }
                    if (!found) {
                        m_logger.fatal("Transport " + transport_json["name"].string_value() + " specified an unknown axis, \"" +
                            transport_axis_json["name"].string_value() +"\"");
                        THROW(BadConfiguration);
                    }
                }
                Transport *pTransport = new Transport(*pController, transport_json["name"].string_value(), p_transport_axes);
				m_logger.info("Pushing transport '" + transport_json["name"].string_value() + "'");
                p_transports->push_back(pTransport);
            }

            pController->init(p_transports, p_axes, DEFAULT_CONTROLLER_TIMEOUT_MS);

            m_controllers.push_back(pController);
        }
    }

    void MotionServices::shutdown(unsigned int msec_timeout_P)
    {
        for (std::vector<MotionController *>::iterator controller_iter = m_controllers.begin();
                controller_iter!=m_controllers.end();
                ++controller_iter)
        {
            (*controller_iter)->shutdown(msec_timeout_P);
        }
	}

    void MotionServices::energize(unsigned int msec_timeout_P)
    {
        for (std::vector<MotionController *>::iterator controller_iter = m_controllers.begin();
                controller_iter!=m_controllers.end();
                ++controller_iter)
        {
            (*controller_iter)->energize(msec_timeout_P);
        }
	}

    void MotionServices::deenergize(unsigned int msec_timeout_P)
    {
        for (std::vector<MotionController *>::iterator controller_iter = m_controllers.begin();
                controller_iter!=m_controllers.end();
                ++controller_iter)
        {
            (*controller_iter)->deenergize(msec_timeout_P);
        }
	}

/*
 * WIP. Hold off implementing this for now. 
    void MotionServices::enable(unsigned int msec_timeout_P)
    {
        for (std::vector<MotionController *>::iterator controller_iter = m_controllers.begin();
                controller_iter!=m_controllers.end();
                ++controller_iter)
        {
            (*controller_iter)->enableChannel(msec_timeout_P);
        }
	}

    void MotionServices::disable(unsigned int msec_timeout_P)
*/
    MotionAxis &MotionServices::getAxis(const std::string &nameP) {
        // Walk the controller list.  Search each controller for the named axis.
        for (std::vector<MotionController *>::iterator controller_iter = m_controllers.begin();
                controller_iter!=m_controllers.end();
                ++controller_iter)
        {
            if ((*controller_iter)->hasAxis(nameP)) {
				m_logger.info("MotionServices::getAxis() Found axis: " + nameP);
                return *(*controller_iter)->getAxis(nameP);
            }
        }

        // TODO FIXME - Convert to top-level exception
        m_logger.fatal("MotionServices::getAxis() No such axis: " + nameP);
        THROW(NoSuchObject);
    }

    void MotionServices::preflightCheck(void) {
        THROW(Unimplemented);
    }

    MotionController *MotionServices::getMotionControllerOfAxis(const std::string &nameP) {
        // Walk the controller list.  Search each controller for the named axis.
        for (std::vector<MotionController *>::iterator controller_iter = m_controllers.begin();
                controller_iter!=m_controllers.end();
                ++controller_iter)
        {
            if ((*controller_iter)->hasAxis(nameP)) {
				m_logger.info("getMotionControllerOfAxis() Found axis: " + nameP);
                return (*controller_iter);
            }
        }

        // TODO FIXME - Convert to top-level exception
        m_logger.fatal("getMotionControllerOfAxis() No such axis: " + nameP);
        THROW(NoSuchObject);
	}

	// Gets the controllerID of the first MotionController found that contains a MotionAxis of the given name.
	int MotionServices::getMotionControllerIdOfAxis(const std::string &nameP)
	{
		int id = 0;
        // Walk the controller list.  Search each controller for the named axis.
        for (std::vector<MotionController *>::iterator controller_iter = m_controllers.begin();
                controller_iter!=m_controllers.end();
                ++controller_iter)
        {
            if ((*controller_iter)->hasAxis(nameP)) {
				m_logger.info("getMotionControllerOfAxis() Found axis: " + nameP);
                return id;
            }
			id++;
        }

        // TODO FIXME - Convert to top-level exception
        m_logger.fatal("getMotionControllerIdOfAxis() No such axis: " + nameP);
        THROW(NoSuchObject);
	}
}
