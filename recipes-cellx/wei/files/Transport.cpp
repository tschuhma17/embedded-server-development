/* src/Transport.cpp */

#include "controllers/MotionController.hpp"
#include "Transport.hpp"

namespace USAFW {

    Transport::Transport(MotionController &motionControllerP, const std::string nameP, std::vector<MotionAxis *> *axesP) :
        m_controller(motionControllerP),
        m_name(nameP),
        p_axes(axesP)
    {
        ;
    }

    Transport::~Transport() {
        // Remove all axes from the transport.
        disassemble();

        // This object is now empty.
        delete p_axes;
        p_axes=NULL;
    }


    void Transport::disassemble(void) {
        for (auto iter = p_axes->begin();
                iter != p_axes->end();
                ++iter)
        {
            (*iter)->leave();
        }

        p_axes->clear();
    }

}