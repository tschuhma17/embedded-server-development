import base64
import numpy as np
import binascii
import sys
import time
from ledboard import LEDBoard
from motorboard import MotorBoard

led = LEDBoard()
motors = MotorBoard()
# We don't include lumascope_api.py because it has dependencies on PylonCamera, which we won't have.
# Of the lumascope_api.py functions that we use, some are simply passed to led or motors.
# Other functions may do extra work at the api level. These need to be re-implemented here.

def format_and_write_output(msg):
	msgLength = str(len(msg))
	print(msgLength + ':' + msg)

def log(msg):
	print('')
	'''
	with open('/usr/share/logs/popen_hand.log', 'w') as f:
		f.write(msg)
		f.close()
	'''

def handle1(cmdString):
	cmd = cmdString
	if cmd == 'nop':
		format_and_write_output('nop')
	elif cmd == 'red_on':
		led.led_on(led.color2ch('Red'), 100)
		format_and_write_output('ok')
	elif cmd == 'green_on':
		led.led_on(led.color2ch('Green'), 100)
		format_and_write_output('ok')
	elif cmd == 'blue_on':
		led.led_on(led.color2ch('Blue'), 100)
		format_and_write_output('ok')
	elif cmd == 'bf_on':
		led.led_on(led.color2ch('BF'), 100)
		format_and_write_output('ok')
	elif cmd == 'red_off':
		led.led_off(led.color2ch('Red'))
		format_and_write_output('ok')
	elif cmd == 'green_off':
		led.led_off(led.color2ch('Green'))
		format_and_write_output('ok')
	elif cmd == 'blue_off':
		led.led_off(led.color2ch('Blue'))
		format_and_write_output('ok')
	elif cmd == 'bf_off':
		led.led_off(led.color2ch('BF'))
		format_and_write_output('ok')
	elif cmd == 'home_z':
		motors.move_abs_pos('Z', 0)
		waitOnMove('Z', 0)
		motors.zhome()
		format_and_write_output('ok')
	elif cmd == 'get_z':
		position_um = motors.current_pos('Z')
		format_and_write_output(str(position_um))
	elif cmd == 'home_t':
		motors.move_abs_pos('T', 0)
		waitOnMove('T', 0)
		motors.thome()
		format_and_write_output('ok')
	elif cmd == 'get_t':
		position_deg = motors.current_pos('T')
		format_and_write_output(str(position_deg))
	else:
		log('ERROR! ' + cmdString + ' undedined')
		format_and_write_output('unknown command')

def waitOnMove(axis, goal_um):
	not_there = True
	timeOutSeconds = 30
	while(not_there):
		time.sleep(1)
		pos = motors.current_pos(axis)
		# print('pos =', pos)
		timeOutSeconds = timeOutSeconds - 1
		if timeOutSeconds < 1:
			not_there = False
		if (goal_um - 2) < pos < (goal_um + 2):
			not_there = False

def handle2(cmdString, dataString):
	cmd = cmdString
	# print('data =', dataString)
	if cmd == 'move_z_abs':
		position_um = int(dataString)
		current_pos = motors.current_pos('Z')
		if current_pos != position_um:
			# print('data =', str(position_um))
			motors.move_abs_pos('Z', position_um)
			waitOnMove('Z', position_um)
		format_and_write_output('ok')
	elif cmd == 'move_t_abs':
		angle_deg = int(dataString)
		current_pos = motors.current_pos('T')
		if current_pos != angle_deg:
			# print('data =', str(position_um))
			motors.move_abs_pos('T', angle_deg)
			waitOnMove('T', angle_deg)
		format_and_write_output('ok')
	else:
		log('ERROR! ' + cmdString + ' undedined')
		format_and_write_output('unknown command')


if __name__== '__main__':
	# print('argv=', str(sys.argv))
	if len(sys.argv) > 2:
		handle2( sys.argv[1], sys.argv[2] )
	elif len(sys.argv) > 1:
		handle1(sys.argv[1])
	else:
		log('ERROR! missing command')
		format_and_write_output('missing command')
