#ifndef _PVCam_Helper_h_
#define _PVCam_Helper_h_

#include <master.h>
#include <pvcam.h>

#include <string>
#include <stdarg.h>

#define PVCAM_ERROR_CHECK(x, y)\
{\
	if (!x)\
	{\
		char tmp[ERROR_MSG_LEN];\
		int error_code = pl_error_code();\
		pl_error_message (error_code, tmp);\
		y += format("|Error[%i]:%s", error_code, tmp);\
		{\
			printf("%i-%s\n", error_code, tmp);\
		}\
	}\
}

#define LOG(x)\
{\
}
/* Defer work until we decide on the new logging scheme. - MDG
	SYSTEMTIME st;\
	GetLocalTime(&st);\
	printf("%02i:%02i:02%i.%03i %s\n", st.wHour, st.wMinute, st.wSecond, st.wMilliseconds, x);\
}
*/
#endif
