///----------------------------------------------------------------------
/// MotionServices
/// This class a stub.
/// It will be replaced with a working version from USA Firmware.
///----------------------------------------------------------------------
#ifndef _MOTION_SERVICES_H
#define _MOTION_SERVICES_H

#define millimeters -3		/// meaning 10^-3
#define micrometers -6		/// meaning 10^-6
#define nanometers -9		/// meaning 10^-9

///----------------------------------------------------------------------
/// Axis
///----------------------------------------------------------------------
class Axis
{
public:
	/// Constructors
	Axis();
	Axis( const Axis& ) = delete;
	Axis& operator=( const Axis& ) = delete;
	/// Destructor
	~Axis();

	void AbsoluteMove( double target_position, long scale, unsigned long asyncTimeout_ms, uint32_t sequence_id, uint32_t op_sn ) noexcept(false);
	double GetAbsolutePosition( long scale, unsigned long blockingCommandTimeout_ms ) noexcept(false);
private:
};

///----------------------------------------------------------------------
/// MotionServices
///----------------------------------------------------------------------
class MotionServices
{
public:
	/// Constructors
	MotionServices();
	MotionServices( const MotionServices& ) = delete;
	MotionServices& operator=( const MotionServices& ) = delete;
	/// Destructor
	~MotionServices();

	void Init(unsigned long timeout_ms) noexcept(false);
	Axis& GetAxis(const char* axis_name);
private:
	Axis m_allInOne;
};


#endif 
