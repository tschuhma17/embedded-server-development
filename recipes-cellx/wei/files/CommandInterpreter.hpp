///----------------------------------------------------------------------
/// CommandInterpreterThread takes raw JSON commands from the job-
/// schedule queueu and reinterprets them into *workable* commands.
/// It then sends workable commands to the Task That Gets Things Done
/// via the synchronous command queue.
/// 1. Compound commands are made workable by decomposing them into
///    a series of simple commands.
/// 2. Plate-mover commands are made workable by using local knowledge
///    of the plate-mover's location to translate motion into local
///    coordinates.
/// 3. Tip-finder commands are similarly handled, using local knowledge
///    of the tip-finder location(s).
///----------------------------------------------------------------------
#ifndef _CMD_INTERPTR_THREAD_H
#define _CMD_INTERPTR_THREAD_H

#include <thread>
#include <string>
#include "MsgQueue.hpp"
#include "es_des.h"
#include "DesHelper.hpp"
#include "Inventory.hpp"
#include "Components.hpp"

class CommandInterpreterThread
{
public:
    /// Constructor
    CommandInterpreterThread(const char* threadName);

    /// Destructor
    ~CommandInterpreterThread();

    /// Called once to create the worker thread
    /// @return True if thread is created. False otherwise. 
    bool CreateThread();

    /// Called once a program exit to exit the worker thread
    void ExitThread();
    
    void AttachInBox(MsgQueue *q);
    
    void AttachOutBox1(MsgQueue *q);

    void AttachOutBoxForOptics(MsgQueue *q);
	void AttachOutBoxForInfo(MsgQueue *q);
	void AttachOutBoxForCxD(MsgQueue *q);
	void AttachOutBoxForCxPM(MsgQueue *q);
	void AttachOutBoxForPump(MsgQueue *q);
	void AttachOutBoxForRingHolder(MsgQueue *q);

    void AttachStatusRouteBox(MsgQueue *q);

    void SetMachineId(std::string machineId);

	/// Sends component properties to all hardware components but performs no moves.
	bool InitializeHwComponents();

	/// First, turns off lights.
	/// Then moves Z, ZZ, ZZZ, AspirationPump1, DispensePump1, DispensePump2, DispensePump3, PickingPump1,
	/// WhiteLightSource, FluorescenceLightFilter, AnnularRingHolder, Camera, OpticalColumn(focus), Incubator,
	/// and BarcodeReader to home positions. (Only affects components that support such moves.)
	bool HomeAllSafeHwComponents();
	
	/// Moves XY, XX, YY, OpticalColumn(turret), and CxPM to home positions.
	bool HomeAllUnsafeHwComponents();
	
	/// Moves the picking pump tip to the eject location, ejects tip, initializes the pump.
	bool HomePickingPump1();
	
	/// Moves FluorescenceLightFilter(filter position), AnnularRingHolder, and OpticalColumn(turret) to their active positions.
	/// Then, sets white and fluorescence lights to their active states.
	bool RestoreAllHwComponentsToLastActiveStates();
	
	/// Calls the prog0 ClrKills command on all Parker controllers.
	bool ClearKills();
	
	/// Sends an alert message.
	bool SendAlertMessage(
		int commandGroupId,
		int commandId,
		std::string sessionId,
		ComponentType componentType,
		int statusCode,
		bool thisIsCausingHold
	);

	bool ParametersAreReady();

private:
    CommandInterpreterThread(const CommandInterpreterThread&) = delete;
    CommandInterpreterThread& operator=(const CommandInterpreterThread&) = delete;

    /// Entry point for the worker thread
    void Process();

    std::unique_ptr<std::thread> m_thread;
	MsgQueue * m_inBox;
	MsgQueue * m_outBoxForOptics;
	MsgQueue * m_outBoxForInfo;
	MsgQueue * m_outBoxForCxD;
	MsgQueue * m_outBoxForCxPM;
	MsgQueue * m_outBoxForPump;
	MsgQueue * m_outBoxForRingHolder;

	MsgQueue * m_statusRouteBox;
    const char * m_threadName;
    DesHelper * m_desHelper;

	ComponentProperties * m_parametersCxD;
    ComponentProperties * m_parametersAxesXY;
    ComponentProperties * m_parametersAxisXX;
    ComponentProperties * m_parametersAxisYY;
    ComponentProperties * m_parametersAxisZ;
    ComponentProperties * m_parametersAxisZZ;
    ComponentProperties * m_parametersAxisZZZ;
    ComponentProperties * m_parametersAspirationPump1;
    ComponentProperties * m_parametersDispensePump1;
    ComponentProperties * m_parametersDispensePump2;
    ComponentProperties * m_parametersDispensePump3;
    ComponentProperties * m_parametersPickingPump1;
    ComponentProperties * m_parametersWhiteLightSource;
    ComponentProperties * m_parametersFluorescenceLightFilter;
    ComponentProperties * m_parametersAnnularRingHolder;
    ComponentProperties * m_parametersOpticalColumn;
    ComponentProperties * m_parametersCamera;
	ComponentProperties * m_parametersIncubator;
	ComponentProperties * m_parametersCxPM;
	ComponentProperties * m_parametersBarcodeReader;
    bool m_parmetersAreInitialized;
    bool m_continueRunning;
    
    void InitializeWorkingParameters( void );
	/// Methods for handling incomming commands.
	
	bool MeteOut_CheckHardware( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveAllTipsToSafeHeight( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveZZZToSafeHeight( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_GetTip( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveTipToWasteLocation3( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveTipToWasteLocation2( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveTipToWasteLocation1( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_SeatTip( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_GetTipSkew( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_SetWorkingTip( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_RemoveTip( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_SetOpticalColumnTurretPosition( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );

	bool MeteOut_MoveToZLowLimit( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveToZHighLimit( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveZToSafeHeight( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveToZAxisForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveToZTipPickForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveToZTipSeatingForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveToZTipWellBottomForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveToZAxisSeatingForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveToZAxisWellBottomForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );

	bool GetProperties( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg, ComponentProperties * p );
	bool SetProperties( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg, ComponentProperties * p );

	bool MeteOut_MoveToZZLowLimit( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveToZZHighLimit( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveZZToSafeHeight( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveToZZAxisForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveToZZTipPickForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveToZZTipSeatingForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveToZZTipWellBottomForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveToZZAxisSeatingForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveToZZAxisWellBottomForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );

	bool MeteOut_moveToXYNm( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveXToTipSkewTrigger( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveYToTipSkewTrigger( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveXYTo_withPlatePositionDetails( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_SwirlXYStage( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveYYToTipRackGrabLocation( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_MoveSingleAxis( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg, MsgQueue * outBox, const char * axisName, ComponentProperties * props );
	bool MeteOut_RetractSingleAxis( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg, MsgQueue * outBox, const char * axisName, ComponentProperties * props );
	bool MeteOut_Aspirate( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_AspirateStartStop( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_Dispense( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg, int pumpNumber );
	bool MeteOut_DispenseStartStop( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg, int pumpNumber );
	bool MeteOut_Picking( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_GetAutoFocusImageStack( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_ChangeActiveTipRack( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_CollectMontageImages( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool MeteOut_GetCameraImage( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
    bool StoreCpd( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg, bool updateToo );
    bool GetCpdListFromType( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
    bool GetCpdContents( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
    bool DeleteCpd( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool ApplyCpdToComponent( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	int ApplyCpdFileToComponent( json_object *payloadObj, std::string & cpdFilePath, std::string & enumComponentTypeStr );
	bool GetFirmwareVersionNumber( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool SetFirmwareVersionNumber( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool GetCurrentSystemConfiguration( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool GetSystemSerialNumber( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	bool SetSystemSerialNumber( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg );
	json_object *GetCurrentComponentConfiguration( ComponentType type );

	void SetHardwareStateIfNeeded( ComponentType compType, ComponentProperties * oldProps, ComponentProperties * newProps, const char * propName, MsgQueue * destinationQueue );
	void SetTurretStateIfNeeded( ComponentType compType, ComponentProperties * oldProps, ComponentProperties * newProps, const char * propName, MsgQueue * destinationQueue );
	void SetANRPositionStateIfNeeded( ComponentType compType, ComponentProperties * oldProps, ComponentProperties * newProps, const char * propName, MsgQueue * destinationQueue );
	bool SetHardwareState( ComponentType componentType, std::string propertyName, std::string dataType, std::string propertyValue, MsgQueue * destinationQueue, json_object *payloadObj );
};

#endif 

