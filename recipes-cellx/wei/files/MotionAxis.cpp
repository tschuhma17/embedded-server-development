/* MotionAxis.cpp */

#include <string>
#include <unistd.h>

#include "controllers/ACR7000.hpp"
#include "MotionAxis.hpp"

namespace USAFW {

    MotionAxis::MotionAxis(ACR7000 &controllerP, const std::string nameP, int channelIndexP, int masterP) :
        m_controller(controllerP),
        m_name(nameP),
        m_channel_index(channelIndexP),
        m_master_index(masterP),
        m_ppu(0.0)
    {
        ;
    }

    MotionAxis::~MotionAxis() {
        ;
    }

    void MotionAxis::join(Transport &transportP) {
        p_transport = &transportP;
    }

    void MotionAxis::leave(void) {
        p_transport = NULL;
    }

    void MotionAxis::enable(void) {
        m_controller.enableChannel(m_channel_index);
    }

    void MotionAxis::disable(void) {
        m_controller.disableChannel(m_channel_index);
    }

    long MotionAxis::getMasterFlags(void) {
        struct timespec when;
        return m_controller.inspectPValueLong(m_controller.AR7xT_pvaddrs_axis_master_flags[m_master_index], when);
	}

    long MotionAxis::getAxisFlags(void) {
        struct timespec when;
        return m_controller.inspectPValueLong(m_controller.AR7xT_pvaddrs_axis_flags[m_channel_index], when);
	}

    long MotionAxis::getQuaternaryAxisFlags(void) {
        struct timespec when;
        return m_controller.inspectPValueLong(m_controller.AR7xT_pvaddrs_quaternary_axis_flags[m_channel_index], when);
	}

    long MotionAxis::getQuinaryAxisFlags(void) {
        struct timespec when;
        return m_controller.inspectPValueLong(m_controller.AR7xT_pvaddrs_quinary_axis_flags[m_channel_index], when);
	}

    long MotionAxis::getAbsolutePosition(void) {
        struct timespec when;
        return m_controller.inspectPValueLong(m_controller.AR7xT_pvaddrs_axis_actual_position[m_channel_index], when);
    }

    long MotionAxis::getCurrentPosition(void) {
        struct timespec when;
        return m_controller.inspectPValueLong(m_controller.AR7xT_pvaddrs_axis_current_position[m_channel_index], when);
    }

    long MotionAxis::getActualPosition(void) {
        struct timespec when;
        return m_controller.inspectPValueLong(m_controller.AR7xT_pvaddrs_axis_actual_position[m_channel_index], when);
    }

    double MotionAxis::getCurrentVelocity(void) {
        struct timespec when;
        return (double) m_controller.inspectPValueFloat(m_controller.AR7xT_pvaddrs_axis_velocity[m_channel_index], when);
    }

    double MotionAxis::getPPU(void) {
        struct timespec when;
        m_ppu = (double) m_controller.inspectPValueFloat(m_controller.AR7xT_pvaddrs_axis_PPU_term[m_channel_index], when);
        return m_ppu;
	}

    void MotionAxis::absoluteMove(double positionP) {
        m_controller.absoluteMove(m_name, positionP);
    }

    int MotionAxis::getMasterIndex(void) {
		return m_master_index;
	}

	int MotionAxis::getChannelIndex(void) {
		return m_channel_index;
	}


	double MotionAxis::getCachedPPU(void) {
		return m_ppu;
	}

	/// Send a Kill All Motion Request (KAMR)
	/// This will cause the following effects in the controller:
	/// 1. attempt to bring the axis to a controlled stop
	/// 2. Stop any jog, cam, gear, or ballscrew motion on the axis by clearing the flag that enable those functions on the axis. 
	/// 3. Set the Kill-All-MOVES flag for the Master that is assigned to that axis. This will stop and prevent any coordinated motion. 
	/// 4. Set the Kill-All-MOTION Request flag for any other axes on that same Master. 
    bool MotionAxis::killAllMotion(void) {
		/// Set bit 8467, which is bit-index 19 of the Quaternary Axis Flags [P4360, P4361, P4362, P4363...]
		long quaternaryAxisFlags = getQuaternaryAxisFlags();
		quaternaryAxisFlags = quaternaryAxisFlags | MASK_KILL_ALL_MOTION_REQUEST;
        m_controller.binarySetLong(m_controller.AR7xT_pvaddrs_quaternary_axis_flags[m_channel_index], quaternaryAxisFlags);
		usleep(1000);
		return true;
	}

	/// Clear all the kill bits set by a Kill All Motion Request (KAMR)
	/// This will be done by directly manipulating bits--NOT by running the ClrKills prog0 command.
	/// Perform the following actions on the controller:
	/// 1. Clear the Kill_All_MOTION_REQUEST flag for this axis and any others on the same Master. 
	/// 2. Clear the Kill_All_MOVES flag for the Master assigned to this axis.
	/// 3. Clear the LATCHED_EXCESS_POSITION_ERROR flag for this axis and any others on the same Master. 
	/// (This function is modeled on ClearKills_C() by Tom Adams.)
	bool MotionAxis::clearKills(void) {
		/// Clear the KILL_ALL_MOTION_REQUEST bits of all axes controlled by this master.
		m_controller.clearQuaternaryAxisFlagsOfMaster( BIT_INDEX_KILL_ALL_MOTION_REQUEST, m_master_index );
		/// Clear the Kill-All-MOVES flag of this master
		m_controller.clearMasterFlag( BIT_INDEX_KILL_ALL_MOVES, m_master_index ); 	/// (522)
		/// For good measure, clear the LATCHED_EXCESS_POSITION_ERROR bits of all axes controlled by this master.
		m_controller.clearQuaternaryAxisFlagsOfMaster( BIT_INDEX_EXCESS_POSITION_ERROR, m_master_index );
		return true;
	}

	/// Returns true if this axis's master flags' KILL-ALL-MOVES is clear AND
	/// all the axes that share this master have clear KILL_ALL_MOTION_REQUEST
	/// flags AND all the axes that share this master have clear EXCESS_POSITION_ERROR flags.
	/// (This function is modeled on ClearKills_C() by Tom Adams.)
	bool MotionAxis::killsAreCleared(void) {
		bool allClear = true;
		allClear &= (!m_controller.getMasterFlag( BIT_INDEX_KILL_ALL_MOVES, m_master_index ));
		allClear &= m_controller.QuaternaryAxisFlagsOfMasterAreClear( BIT_INDEX_KILL_ALL_MOTION_REQUEST, m_master_index );
		allClear &= m_controller.QuaternaryAxisFlagsOfMasterAreClear( BIT_INDEX_EXCESS_POSITION_ERROR, m_master_index );
		return allClear;
	}

	bool MotionAxis::isInMotion(void) {
		long masterFlags = getMasterFlags();
		return ((masterFlags & MASK_IN_MOTION) == MASK_IN_MOTION);
	}

	bool MotionAxis::isInPositionBand(void) {
		long axisFlags0 = getAxisFlags();
		return !((axisFlags0 & MASK_IPB) == MASK_IPB);
	}
}

