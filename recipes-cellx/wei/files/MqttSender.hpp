///----------------------------------------------------------------------
/// MqttSender (Message Queue Telemetry Transport Receiver)
/// This file implements a thread, which performs the following jobs.
/// 1. Connect to an MQTT broker for ES messages per the "Embedded
///    Server Data Exchange Specification (ES-DES).
/// 2. Receive inter-thread messages from other threads. These will
///    mostly be ack and status messages.
/// 3. Send the messages to the appropriate MQTT topics.
/// 4. Interthread messages come in via shared pointers, and this is the
///    last task to use them, so it will work to ensure that the
///    messages get deleted from memory.
///----------------------------------------------------------------------
#ifndef _MQTT_SENDER_THREAD_H
#define _MQTT_SENDER_THREAD_H


#include <thread>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <list>
#include "MsgQueue.hpp"
#include "DesHelper.hpp"
#include "MQTTClient.h"

#define SENDER_CLIENTID         "ES-MQTT-sender-task"
#define SENDER_BROKER_PORT      "1883"
#define SENDER_KEEP_ALIVE_INTERVAL 120
#define SENDER_CLEAN_SESSION    1
#define SENDER_QOS              2
#define SENDER_TIMEOUT          10000L

class MqttSenderThread
{
public:
    /// Constructor
    MqttSenderThread(const char* threadName);

    /// Destructor
    ~MqttSenderThread();

    /// Called once to create the worker thread
    /// @return True if thread is created. False otherwise. 
    bool CreateThread();

    /// Called once a program exit to exit the worker thread
    void ExitThread();
    
    void AttachInBox(MsgQueue *q);

    /// Called once to enable connecting to the MQTT broker.
    void SetBrokerIpAddress(std::string ipAddress);

private:
    MqttSenderThread(const MqttSenderThread&) = delete;
    MqttSenderThread& operator=(const MqttSenderThread&) = delete;

    /// Entry point for the worker thread
    void Process();

    std::unique_ptr<std::thread> m_thread;
	MsgQueue *m_inBox;
    const char* m_threadName;
    bool m_continueRunning;

	/// For MQTT communications
    std::string m_brokerIpAddress;
    bool m_connectedToBroker;
    std::list<std::string> m_listOfTopics;
    MQTTClient m_client;
    MQTTClient_connectOptions m_conn_opts = MQTTClient_connectOptions_initializer;
    MQTTClient_message m_pubmsg = MQTTClient_message_initializer;
    MQTTClient_deliveryToken m_token;
    int m_mqttReturnCode;
    DesHelper m_desHelper;
    std::stringstream m_address;
	char* m_payloadBuffer;
    
    /// For handling special commands
    bool PackSinglePictureIntoPayload( const std::string & callPayload, const unsigned char * blob, const unsigned long blobSize );
    bool PackMontagePictureIntoPayload( const std::string & callPayload, const unsigned char * blob, const unsigned long blobSize );
};

#endif 

