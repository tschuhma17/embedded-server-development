#include <iostream>
#include <sstream>
#include <cassert>
#include <list>
#include <stdio.h> // for fwrite()
#include <math.h> // for M_PI
#include <cstdio> // for popen
#include "main.hpp"
#include "es_des.h"
#include "es_controller.h"
#include "enums.hpp"
#include "CommandDoler.hpp"
#include "MsgQueue.hpp"
#include "Fault.hpp"
#include "Sundry.hpp"
#include "ShellInvoker.hpp"
#include "USAFW/util/math.hpp"
#include "exceptions/ControllerException.hpp"
#include "exceptions/MotionException.hpp"

using namespace std;
using namespace USAFW;

#define MAX_ALLOWABLE_VELOCITY_ERROR (0.001)

///----------------------------------------------------------------------
/// CommandDolerThread
///----------------------------------------------------------------------
CommandDolerThread::CommandDolerThread(const char* threadName) :
	m_thread(nullptr),
	m_threadName(threadName),
	m_inBox(nullptr),
	m_outBox(nullptr),
	m_blobPtr(nullptr),
	m_blobSizePtr(nullptr),
	m_mqttHardwareIO(nullptr),
   	m_continueRunning(false),
	m_serialPort(nullptr),
	m_tecanADP(nullptr)
{
	m_desHelper = new DesHelper();
}

///----------------------------------------------------------------------
/// ~CommandDolerThread
///----------------------------------------------------------------------
CommandDolerThread::~CommandDolerThread()
{
	ExitThread();
}

///----------------------------------------------------------------------
/// CreateThread
///----------------------------------------------------------------------
bool CommandDolerThread::CreateThread()
{
	bool success = false;
	if (!m_thread)
	{
	   	m_continueRunning = true;
		m_thread = std::unique_ptr<std::thread>(new thread(&CommandDolerThread::Process, this));
		success = true;

		if (m_threadName == DOLER_FOR_OPTICS)
		{
			m_mqttHardwareIO = new MqttHardwareIO( m_brokerIpAddress ); /// Assume caller called SetBrokerIpAddress() before CreateThread().
			m_mqttHardwareIO->InitializeListOfTopics( ComponentType::OpticalColumn );
			m_mqttHardwareIO->ConnectToBroker();
			/// initialize cache values in the appropriate maps in case there are reads before writes.
			m_states_long[ACTIVE_FLUORESCENCE_FILTER_POSITION] = 0L;
			m_states_long[ANNULAR_RING_POSITION] = 0L;
			m_states_ulong[FOCUS_NM] = 0L;
			m_states_ulong[ACTIVE_TURRET_POSITION] = 0L;
			m_states_ulong[Y_SET_SPEED] = 0L;
			m_states_ulong[Y_ACCELERATION] = 0L;
			m_states_ulong[Y_DECELERATION] = 0L;
			m_states_ulong[Y_JOGGING_SPEED] = 0L;
			m_states_ulong[Y_JOGGING_ACCELERATION] = 0L;
			m_states_ulong[Y_JOGGING_DECELERATION] = 0L;
			m_states_ulong[GAIN] = 0L;
			m_states_ulong[EXPOSURE] = 0L;
			m_states_ulong[BIN_FACTOR] = 0L;
			m_states_bool[WHITE_LIGHT_ON_STATE] = false;
			m_states_bool[FLUORESCENCE_LIGHT_ON_STATE] = false;
		}
		if (m_threadName == DOLER_FOR_CXD)
		{
			/// initialize cache values in the appropriate maps in case there are reads before writes.
			m_states_long[X_NM] = 0L;
			m_states_long[Y_NM] = 0L;
			m_states_long[XX_NM] = 0L;
			m_states_long[YY_NM] = 0L;
		}
		if (m_threadName == DOLER_FOR_PUMP)
		{
			/// initialize cache values in the appropriate maps in case there are reads before writes.
			m_states_bool[IS_ASPIRATION_PUMP_RUNNING] = false;
			m_states_bool[IS_DISPENSE_PUMP_1_RUNNING] = false;
			m_states_bool[IS_DISPENSE_PUMP_2_RUNNING] = false;
			m_states_bool[IS_DISPENSE_PUMP_3_RUNNING] = false;
			float initPickingPumpCurrentVolume = 0.0;
			m_states_float[PICKING_PUMP_CURRENT_VOLUME] = initPickingPumpCurrentVolume;
			g_PickingPumpVolume.store( initPickingPumpCurrentVolume, std::memory_order_relaxed ); /// for users outside the FOR-PUMP thread.
			float initPickingPumpMaxVolume = 1100.0;
			m_states_float[PICKING_PUMP_MAXIMUM_VOLUME] = initPickingPumpMaxVolume; /// Will be used by Mock code or overwritten with real value.
			g_PickingPumpMaximumVolume.store( initPickingPumpMaxVolume, std::memory_order_relaxed ); /// for users outside the FOR-PUMP thread.
		}
	}
	return success;
}
///----------------------------------------------------------------------
/// ExitThread
///----------------------------------------------------------------------
void CommandDolerThread::ExitThread()
{
	if (m_threadName == DOLER_FOR_CXD)
	{
		unique_lock<mutex> lock( g_controllerBMutex ); /// Avoid letting the CxD thread perform shutdowns at the same time as the Pump thread.
		if (m_continueRunning) /// Guard against doing this action multiple times.
		{
			if (g_XYState.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				Implement_disableAxis( "Y" ); // "AXIS 0 OFF"
				usleep(100000);
				Implement_disableAxis( "X" );
				usleep(100000);
			}
			if (g_XXState.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				Implement_disableAxis( "XX" );
				usleep(100000);
			}
			if (g_YYState.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				Implement_disableAxis( "YY" );
				usleep(100000);
			}
			if (g_AnnularRingHolderState.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				Implement_disableAxis( "SH" );
				usleep(100000);
			}
			if (g_ZState.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				Implement_disableAxis( "Z" );
				usleep(100000);
			}
			if (g_ZZState.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				Implement_disableAxis( "ZZ" );
				usleep(100000);
			}
			if (g_ZZZState.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				Implement_disableAxis( "ZZZ" );
				usleep(100000);
			}
		}
	}
	if (m_threadName == DOLER_FOR_PUMP)
	{
		unique_lock<mutex> lock( g_controllerBMutex ); /// Avoid letting the CxD thread perform shutdowns at the same time as the Pump thread.
		if (m_continueRunning) /// Guard against doing this action multiple times.
		{
			if (g_AspirationPump1State.load( std::memory_order_acquire ) == ComponentState::SettingsConfirmed)
			{
				Implement_disableAxis( "PN" );
				usleep(100000);
			}
			if (g_DispensePump1State.load( std::memory_order_acquire ) == ComponentState::SettingsConfirmed)
			{
				Implement_disableAxis( "PP" );
				usleep(100000);
			}
			if (g_DispensePump2State.load( std::memory_order_acquire ) == ComponentState::SettingsConfirmed)
			{
				Implement_disableAxis( "PPP" );
				usleep(100000);
			}
			if (g_DispensePump3State.load( std::memory_order_acquire ) == ComponentState::SettingsConfirmed)
			{
				Implement_disableAxis( "PPPP" );
				usleep(100000);
			}
		}
	}

	
	if (m_threadName == DOLER_FOR_OPTICS)
	{
		if (m_continueRunning) /// Guard against doing this action multiple times.
		{
			string camera_release_msg = "";
			ReleasePVCamComms( camera_release_msg );
			delete m_serialPort;
		}
	}

   	m_continueRunning = false;
	/// Send a small message to the inbox, just to break out of listening long enough to exit.
	std::shared_ptr<WorkMsg> ender = make_shared<WorkMsg>( "", "", "", "end", "", true );
	m_inBox->PushMsg( ender );

	if (!m_thread)
		return;
		
    m_thread->join();
    m_thread = nullptr;
}

///----------------------------------------------------------------------
/// attachInBox
///----------------------------------------------------------------------
void CommandDolerThread::AttachInBox(MsgQueue *q)
{
	assert((!m_inBox) && "AttachInBox called second time?");
    m_inBox = q;
}
    
///----------------------------------------------------------------------
/// attachOutBox
///----------------------------------------------------------------------
void CommandDolerThread::AttachOutBox(MsgQueue *q)
{
	assert((!m_outBox) && "AttachOutBox called second time?");
	m_outBox = q;
}

///----------------------------------------------------------------------
/// SetMachineId
///----------------------------------------------------------------------
void CommandDolerThread::SetMachineId(std::string machineId)
{
	LETS_ASSERT(this->m_desHelper);
	this->m_desHelper->SetMachineId( machineId );
}

///----------------------------------------------------------------------
/// SetBrokerIpAddress
///----------------------------------------------------------------------
void CommandDolerThread::SetBrokerIpAddress( std::string ipAddress )
{
	m_brokerIpAddress = ipAddress;
}

///----------------------------------------------------------------------
// TODO: Rename this to make it generic.
/// InitPVCamComms
///----------------------------------------------------------------------
bool CommandDolerThread::InitPVCamComms (std::string &msg)
{
	bool succeeded = true;

	if (m_threadName != DOLER_FOR_OPTICS)
	{
		return false;
	}

	HardwareType cameraType = g_ComponentHardwareTypeArray[(int)ComponentType::Camera];
	unique_lock<mutex> lock( g_cameraMutex );

	if (!g_CameraIsInitialized.load( std::memory_order_acquire ))
	{
		int result = -1; /// Initialize to a value indicating failure.
		string errorMsg = "Unsupported Camera";
		if (cameraType == HardwareType::R1_R2_R3)
		{
			result = Camera_Retiga::InitDriver( errorMsg );
		}
		// TODO: Add support for HardwareType::daA3840_45um.
		//
		if (result == 0)
		{
			succeeded &= true;
			g_CameraIsInitialized.store( true, std::memory_order_relaxed );
		}
		else
		{
			succeeded &= false;
			LOG_ERROR( m_threadName, "camera", errorMsg.c_str() );
		}
	}
	
	if (!succeeded)
	{
		LOG_ERROR( m_threadName, "camera", "Initializing function bailing out" );
		return false;
	}

	/// Continue with the rest no matter how many other threads have done this.
	/// It just initializes CommandDolerThread class attrubutes. 
	string versionMsg = "";
	int versionNumber = -1;
	if (cameraType == HardwareType::R1_R2_R3)
	{
		versionNumber = Camera_Retiga::GetVersion( versionMsg );
	}
	if (cameraType == HardwareType::daA3840_45um)
	{
//		versionNumber = Camera_Basler::GetVersion( versionMsg );
	}
	LOG_TRACE( m_threadName, "camera", versionMsg.c_str() );

	/// Populate g_CameraList with one ES_CameraBase object for each camera found.
	/// ES_CameraBase is an abstract class. The objects returned are either Camera_Retiga
	/// or Camera_Basler objects, depending on which class's static GetCameras() function
	/// is used.
	int numCameras = 0;
	string cameraMsg = "";
	if (cameraType == HardwareType::R1_R2_R3)
	{
		numCameras = Camera_Retiga::GetCameras( g_CameraList, cameraMsg );
	}
	if (cameraType == HardwareType::daA3840_45um)
	{
//		numCameras = Camera_Basler::GetCameras( g_CameraList, cameraMsg );
	}
	if (numCameras == 0)
	{
		LOG_ERROR( m_threadName, "camera", cameraMsg.c_str() );
		succeeded &= false;
	}
	else
	{
		LOG_TRACE( m_threadName, "camera", cameraMsg.c_str() );
	}
	return succeeded;
}

///----------------------------------------------------------------------
// TODO: Rename this to make it generic.
/// ReleasePVCamComms
///----------------------------------------------------------------------
bool CommandDolerThread::ReleasePVCamComms (std::string &msg)
{
	bool succeeded = false;

	if (!g_CameraIsInitialized.load( std::memory_order_acquire ))
	{
		return false;
	}

	HardwareType cameraType = g_ComponentHardwareTypeArray[(int)ComponentType::Camera];
	unique_lock<mutex> lock( g_cameraMutex );

	string terminationMsg = "";
	if (cameraType == HardwareType::R1_R2_R3)
	{
		succeeded = Camera_Retiga::TerminateDriver( g_CameraList, terminationMsg );
	}
	if (cameraType == HardwareType::daA3840_45um)
	{
//		numCameras = Camera_Basler::TerminateDriver( g_CameraList, terminationMsg );
	}

	if (succeeded)
	{
		LOG_TRACE( m_threadName, "camera", terminationMsg.c_str() );
	}
	else
	{
		LOG_ERROR( m_threadName, "camera", terminationMsg.c_str() );
	}

	return succeeded;
}

/// Performs the work of a single command.
/// It also extracts some data from class attribute m_commandMessageInObj to return to the caller for the caller's use.
/// So DoWork is doing more than one thing, which isn't clean,
/// but the extracted data is sometimes helpful for performing a command, so it is tolerated.
int CommandDolerThread::DoWork( std::string taskAtHand, int *commandId, int *commandGroupId, string *sessionId )
{
	/// From the json_object, pull out the fields of interest to us.
	std::string messageType = "";
	std::string machineId = "";
	bool messageParsed = m_desHelper->SplitCommand( m_commandMessageInObj,
													&messageType,
													nullptr, // &transportVersion,
													commandGroupId,
													commandId,
													sessionId,
													&machineId );
	LETS_ASSERT( messageParsed );

	/// DOLE OUT THIS TASK TO APPROPRIATE HARDWARE.
	if (taskAtHand == CHECK_HARDWARE)
	{
		return DoleOut_CheckHardware();
	}
	if (taskAtHand == SYNCHRONIZE_TIME)
	{
		return Fulfill_SynchronizeTime();
	}
	if (taskAtHand == MOVE_TO_XY_NM)
	{
		return DoleOut_moveToXYNm();
	}
	if (taskAtHand == MOVE_TO_XY_RELATIVE_NM)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_CURRENT_XY_NM)
	{
		return DoleOut_getCurrentXYNm();
	}
	if (taskAtHand == MOVE_TO_X_LOW_LIMIT)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_TO_X_HIGH_LIMIT)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_TO_Y_LOW_LIMIT)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_TO_Y_HIGH_LIMIT)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_XY_TO_OPTICAL_EYE_NM_A1_NOTATION)
	{
		return DoleOut_MoveXYToSpecialNotation( true, /// for Well
												true ); /// use A1 Notation
	}
	if (taskAtHand == MOVE_XY_TO_OPTICAL_EYE_NM_RC_NOTATION)
	{
		return DoleOut_MoveXYToSpecialNotation( true, /// for Well
												false ); /// use RC Notation
	}
	if (taskAtHand == MOVE_XY_TO_TIP_WORKING_LOCATION_NM_A1_NOTATION)
	{
		return DoleOut_MoveXYToSpecialNotation( false, /// for Tip
												true ); /// use A1 Notation
	}
	if (taskAtHand == MOVE_XY_TO_TIP_WORKING_POSITION_NM_RC_NOTATION)
	{
		return DoleOut_MoveXYToSpecialNotation( false, /// for Tip
												false ); /// use RC Notation
	}
	if (taskAtHand == SWIRL_XY_STAGE)
	{
		return DoleOut_swirlXYStage();
	}
	if (taskAtHand == GET_XY_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_XY_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_TO_XX_NM)
	{
		return DoleOut_moveToXXNm();
	}
	if (taskAtHand == MOVE_TO_XX_RELATIVE_NM)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_CURRENT_XX_NM)
	{
		return DoleOut_getCurrentXXNm();
	}
	if (taskAtHand == MOVE_TO_XX_LOW_LIMIT)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_TO_XX_HIGH_LIMIT)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_XX_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_XX_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_TO_YY_NM)
	{
		return DoleOut_moveToYYNm();
	}
	if (taskAtHand == MOVE_TO_YY_RELATIVE_NM)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_CURRENT_YY_NM)
	{
		return DoleOut_getCurrentYYNm();
	}
	if (taskAtHand == MOVE_TO_YY_LOW_LIMIT)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_TO_YY_HIGH_LIMIT)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_YY_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_YY_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_YY_TO_TIP_RACK_GRAB_LOCATION)
	{
		return DoleOut_MoveYYToTipRackGrabLocation();
	}
	
	/// All of the Zs
	if (taskAtHand == MOVE_TO_Z_NM)
	{
		return DoleOut_moveToZNm();
	}
	if (taskAtHand == MOVE_TO_Z_RELATIVE_NM)
	{
		return DoleOut_moveToZRelativeNm();
	}
	if (taskAtHand == RETRACT_TO_Z_NM)
	{
		/// Call moveToZNm because the interpreter already *replaced* speed, accel, and decel with retraction versions.
		/// If we ever need to make retract_to_z_nm a part of a compound command, this implementation will have to change.
		return DoleOut_retractToZNm();
	}
	if (taskAtHand == GET_CURRENT_Z_NM)
	{
		return DoleOut_getCurrentZNm();
	}
	if (taskAtHand == MOVE_TO_Z_LOW_LIMIT)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_TO_Z_HIGH_LIMIT)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_Z_TO_SAFE_HEIGHT)
	{
		return DoleOut_MoveZToSafeHeight();
	}
	if (taskAtHand == GET_Z_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_Z_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}

	if (taskAtHand == MOVE_TO_Z_AXIS_FORCE_NEWTON)
	{
		return DoleOut_MoveToZAxisForceNewton();
	}
	if (taskAtHand == MOVE_TO_Z_AXIS_SEATING_FORCE_NEWTON)
	{
		return DoleOut_MoveToZAxisSeatingForceNewton();
	}
	if (taskAtHand == MOVE_TO_Z_AXIS_WELL_BOTTOM_FORCE_NEWTON)
	{
        LOG_TRACE( m_threadName, "z", "-" );
 		return DoleOut_MoveToZAxisWellBottomForceNewton();
	}
	if (taskAtHand == MOVE_TO_Z_TIP_PICK_FORCE_NEWTON)
	{
		return DoleOut_MoveToZTipPickForceNewton();
	}
	if (taskAtHand == MOVE_TO_Z_TIP_SEATING_FORCE_NEWTON)
	{
		return DoleOut_MoveToZTipSeatingForceNewton();
	}
	if (taskAtHand == MOVE_TO_Z_TIP_WELL_BOTTOM_FORCE_NEWTON)
	{
		return DoleOut_MoveToZTipWellBottomForceNewton();
	}
	
	/// ZZs
	if (taskAtHand == MOVE_TO_ZZ_NM)
	{
		return DoleOut_moveToZZNm();
	}
	if (taskAtHand == MOVE_TO_ZZ_RELATIVE_NM)
	{
		return DoleOut_moveToZZRelativeNm();
	}
	if (taskAtHand == RETRACT_TO_ZZ_NM)
	{
		/// Call moveToZNm because the interpreter already *replaced* speed, accel, and decel with retraction versions.
		/// If we ever need to make retract_to_zz_nm a part of a compound command, this implementation will have to change.
		return DoleOut_retractToZZNm();
	}
	if (taskAtHand == GET_CURRENT_ZZ_NM)
	{
		return DoleOut_getCurrentZZNm();
	}
	if (taskAtHand == MOVE_TO_ZZ_LOW_LIMIT)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_TO_ZZ_HIGH_LIMIT)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_ZZ_TO_SAFE_HEIGHT)
	{
		return DoleOut_MoveZZToSafeHeight();
	}
	if (taskAtHand == GET_ZZ_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_ZZ_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}

	if (taskAtHand == MOVE_TO_ZZ_AXIS_FORCE_NEWTON)
	{
		return DoleOut_MoveToZZAxisForceNewton();
	}
	if (taskAtHand == MOVE_TO_ZZ_AXIS_SEATING_FORCE_NEWTON)
	{
		return DoleOut_MoveToZZAxisSeatingForceNewton();
	}
	if (taskAtHand == MOVE_TO_ZZ_AXIS_WELL_BOTTOM_FORCE_NEWTON)
	{
        LOG_TRACE( m_threadName, "z", "-" );
		return DoleOut_MoveToZZAxisWellBottomForceNewton();
	}
	if (taskAtHand == MOVE_TO_ZZ_TIP_PICK_FORCE_NEWTON)
	{
		return DoleOut_MoveToZZTipPickForceNewton();
	}
	if (taskAtHand == MOVE_TO_ZZ_TIP_SEATING_FORCE_NEWTON)
	{
		return DoleOut_MoveToZZTipSeatingForceNewton();
	}
	if (taskAtHand == MOVE_TO_ZZ_TIP_WELL_BOTTOM_FORCE_NEWTON)
	{
		return DoleOut_MoveToZZTipWellBottomForceNewton();
	}

	if (taskAtHand == MOVE_TO_ZZZ_NM)
	{
		return DoleOut_moveToZZZNm();
	}
	if (taskAtHand == MOVE_TO_ZZZ_RELATIVE_NM)
	{
		return DoleOut_moveToZZZRelativeNm();
	}
	if (taskAtHand == GET_CURRENT_ZZZ_NM)
	{
		return DoleOut_getCurrentZZZNm();
	}
	if (taskAtHand == MOVE_TO_ZZZ_LOW_LIMIT)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_TO_ZZZ_HIGH_LIMIT)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_ZZZ_TO_SAFE_HEIGHT)
	{
		return DoleOut_MoveZZZToSafeHeight();
	}
	if (taskAtHand == GET_ZZZ_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_ZZZ_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}

	if (taskAtHand == ASPIRATE_ZZ_FLOW_RATE_TIME)
	{
		return DoleOut_MoveStepperPump( & g_AspirationPump1State, /// compState
										true,		/// calcVolume
										false );	/// calcRate
	}
	if (taskAtHand == ASPIRATE_ZZ_FLOW_RATE_VOLUME)
	{
		return DoleOut_MoveStepperPump( & g_AspirationPump1State, /// compState
										false,		/// calcVolume
										false );	/// calcRate
	}
	if (taskAtHand == ASPIRATE_ZZ_VOLUME_TIME)
	{
		return DoleOut_MoveStepperPump( & g_AspirationPump1State, /// compState
										false,		/// calcVolume
										true );		/// calcRate
	}
	if (taskAtHand == ASPIRATION_PUMP_START_STOP)
	{
		return DoleOut_StartStopStepperPump( & g_AspirationPump1State,
											 IS_ASPIRATION_PUMP_RUNNING );
	}
	if (taskAtHand == IS_ASPIRATION_PUMP_RUNNING)
	{
		return DoleOut_IsPumpRunning( IS_ASPIRATION_PUMP_RUNNING );
	}
	if (taskAtHand == GET_ASPIRATION_PUMP_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_ASPIRATION_PUMP_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == DISPENSE_1_FLOW_RATE_TIME)
	{
		return DoleOut_MoveStepperPump( & g_DispensePump1State, /// compState
										true,		/// calcVolume
										false );	/// calcRate
	}
	if (taskAtHand == DISPENSE_1_FLOW_RATE_VOLUME)
	{
		return DoleOut_MoveStepperPump( & g_DispensePump1State, /// compState
										false,		/// calcVolume
										false );	/// calcRate
	}
	if (taskAtHand == DISPENSE_1_VOLUME_TIME)
	{
		return DoleOut_MoveStepperPump( & g_DispensePump1State, /// compState
										false,		/// calcVolume
										true );		/// calcRate
	}
	if (taskAtHand == DISPENSE_PUMP_1_START_STOP)
	{
		return DoleOut_StartStopStepperPump( & g_DispensePump1State,
											 IS_DISPENSE_PUMP_1_RUNNING );
	}
	if (taskAtHand == IS_DISPENSE_PUMP_1_RUNNING)
	{
		return DoleOut_IsPumpRunning( IS_DISPENSE_PUMP_1_RUNNING );
	}
	if (taskAtHand == GET_DISPENSE_PUMP_1_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_DISPENSE_PUMP_1_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == DISPENSE_2_FLOW_RATE_TIME)
	{
		return DoleOut_MoveStepperPump( & g_DispensePump2State, /// compState
										true,		/// calcVolume
										false );	/// calcRate
	}
	if (taskAtHand == DISPENSE_2_FLOW_RATE_VOLUME)
	{
		return DoleOut_MoveStepperPump( & g_DispensePump2State, /// compState
										false,		/// calcVolume
										false );	/// calcRate
	}
	if (taskAtHand == DISPENSE_2_VOLUME_TIME)
	{
		return DoleOut_MoveStepperPump( & g_DispensePump2State, /// compState
										false,		/// calcVolume
										true );		/// calcRate
	}
	if (taskAtHand == DISPENSE_PUMP_2_START_STOP)
	{
		return DoleOut_StartStopStepperPump( & g_DispensePump2State,
											 IS_DISPENSE_PUMP_2_RUNNING );
	}
	if (taskAtHand == IS_DISPENSE_PUMP_2_RUNNING)
	{
		return DoleOut_IsPumpRunning( IS_DISPENSE_PUMP_2_RUNNING );
	}
	if (taskAtHand == GET_DISPENSE_PUMP_2_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_DISPENSE_PUMP_2_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == DISPENSE_3_FLOW_RATE_TIME)
	{
		return DoleOut_MoveStepperPump( & g_DispensePump3State, /// compState
										true,		/// calcVolume
										false );	/// calcRate
	}
	if (taskAtHand == DISPENSE_3_FLOW_RATE_VOLUME)
	{
		return DoleOut_MoveStepperPump( & g_DispensePump3State, /// compState
										false,		/// calcVolume
										false );	/// calcRate
	}
	if (taskAtHand == DISPENSE_3_VOLUME_TIME)
	{
		return DoleOut_MoveStepperPump( & g_DispensePump3State, /// compState
										false,		/// calcVolume
										true );		/// calcRate
	}
	if (taskAtHand == DISPENSE_PUMP_3_START_STOP)
	{
		return DoleOut_StartStopStepperPump( & g_DispensePump3State,
											 IS_DISPENSE_PUMP_3_RUNNING );
	}
	if (taskAtHand == IS_DISPENSE_PUMP_3_RUNNING)
	{
		return DoleOut_IsPumpRunning( IS_DISPENSE_PUMP_3_RUNNING );
	}
	if (taskAtHand == GET_DISPENSE_PUMP_3_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_DISPENSE_PUMP_3_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == PICKING_PUMP_FLOW_RATE_TIME)
	{
		return DoleOut_MovePickingPump( & g_PickingPump1State, /// compState
										true,		/// calcVolume
										false );	/// calcRate
	}
	if (taskAtHand == PICKING_PUMP_FLOW_RATE_VOLUME)
	{
		return DoleOut_MovePickingPump( & g_PickingPump1State, /// compState
										false,		/// calcVolume
										false );	/// calcRate
	}
	if (taskAtHand == PICKING_PUMP_VOLUME_TIME)
	{
		return DoleOut_MovePickingPump( & g_PickingPump1State, /// compState
										false,		/// calcVolume
										true );		/// calcRate
	}
	if (taskAtHand == GET_PICKING_PUMP_CURRENT_VOLUME_NL)
	{
		return DoleOut_GetPickingPumpCurrentVolumeNl();
	}
	if (taskAtHand == GET_PICKING_PUMP_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_PICKING_PUMP_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_WHITE_LIGHT_SOURCE_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_WHITE_LIGHT_SOURCE_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_FLUORESCENCE_LIGHT_FILTER_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_FLUORESCENCE_LIGHT_FILTER_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_ANNULAR_RING_HOLDER_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_ANNULAR_RING_HOLDER_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_TO_ANNULAR_RING_NM)
	{
		return DoleOut_MoveToAnnularRingPosition();
	}

	if (taskAtHand == MOVE_TO_FOCUS_NM)
	{
		return DoleOut_moveToFocusNM();
	}
	if (taskAtHand == MOVE_TO_FOCUS_RELATIVE_NM)
	{
		return DoleOut_moveToFocusRelativeNM();
	}
	if (taskAtHand == GET_CURRENT_FOCUS_NM)
	{
		return DoleOut_getCurrentFocusNM();
	}
	if (taskAtHand == MOVE_TO_FOCUS_LOW_LIMIT)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_TO_FOCUS_HIGH_LIMIT)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_OPTICAL_COLUMN_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_OPTICAL_COLUMN_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == IS_CAMERA_IMAGE_AVAILABLE)
	{
		return DoleOut_isCameraImageAvailable();
	}
	if (taskAtHand == GET_CAMERA_IMAGE)
	{
		return DoleOut_getCameraImage();
	}
	if (taskAtHand == GET_CAMERA_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_CAMERA_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == PLACE_PLATE_INTO_INCUBATOR)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == RETRIEVE_PLATE_FROM_INCUBATOR)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_INCUBATOR_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_INCUBATOR_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_CXPM_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_CXPM_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_BARCODE_READER_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_BARCODE_READER_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_DIGITAL_INPUT_ACTIVE_LEVEL)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_DIGITAL_INPUT_ACTIVE_LEVEL)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_DIGITAL_INPUT_STATE)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_DIGITAL_OUTPUT_ACTIVE_LEVEL)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_DIGITAL_OUTPUT_ACTIVE_LEVEL)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_DIGITAL_OUTPUT_STATE)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_DIGITAL_OUTPUT_STATE)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == COLLECT_MONTAGE_IMAGES)
	{
		return DoleOut_CollectMontageImages( *commandId, *commandGroupId, *sessionId, machineId );
	}
	if (taskAtHand == ADD_PLATE_TO_DECK_INVENTORY)
	{
		return DoleOut_AddPlateToDeckInventory();
	}
	if (taskAtHand == REMOVE_PLATE_FROM_DECK_INVENTORY)
	{
		return DoleOut_RemovePlateFromDeckInventory();
	}
	if (taskAtHand == MOVE_PLATE_IN_DECK_INVENTORY)
	{
		return DoleOut_MovePlateInDeckInventory();
	}
	if (taskAtHand == GET_PLATE_LIST_IN_LOCATION)
	{
		return DoleOut_GetPlateListInLocation();
	}
	if (taskAtHand == FIND_PLATE_LOCATION_IN_INVENTORY)
	{
		return DoleOut_FindPlateLocationInInventory();
	}
	if (taskAtHand == ADD_PLATE_LID_TO_DECK_INVENTORY)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == REMOVE_PLATE_LID_FROM_DECK_INVENTORY)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_PLATE_LID_LIST_IN_LOCATION)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == FIND_PLATE_LID_LOCATION_IN_INVENTORY)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == ADD_TIP_RACK_TO_DECK_INVENTORY)
	{
		return DoleOut_AddTipRackToDeckInventory();
	}
	if (taskAtHand == REMOVE_TIP_RACK_FROM_DECK_INVENTORY)
	{
		return DoleOut_RemoveTipRackFromDeckInventory();
	}
	if (taskAtHand == MOVE_TIP_RACK_IN_DECK_INVENTORY)
	{
		return DoleOut_MoveTipRackInDeckInventory();
	}
	if (taskAtHand == GET_TIP_RACK_LIST_IN_LOCATION)
	{
		return DoleOut_GetTipRackListInLocation();
	}
	if (taskAtHand == FIND_TIP_RACK_LOCATION_IN_INVENTORY)
	{
		return DoleOut_FindTipRackLocationInInventory();
	}
	if (taskAtHand == GET_TIP_COUNT_FOR_TIP_RACK)
	{
		return DoleOut_GetTipCountForTipRack();
	}
	if (taskAtHand == SET_TIP_COUNT_FOR_TIP_RACK)
	{
		return DoleOut_SetTipCountForTipRack();
	}
	if (taskAtHand == ADD_MEDIA_TO_DECK_INVENTORY)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == REMOVE_MEDIA_AT_LOCATION_FROM_DECK_INVENTORY)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_MEDIA_TYPE_LIST_IN_LOCATION)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == FIND_MEDIA_TYPE_LOCATIONS_IN_INVENTORY)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_MEDIA_VOLUME_UL_IN_LOCATION)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_MEDIA_VOLUME_UL_IN_LOCATION)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_PLATE)
	{
		return DoleOut_MovePlate( false );
	}
	if (taskAtHand == MOVE_PLATE_READ_BARCODE)
	{
		return DoleOut_MovePlate( true );
	}
	if (taskAtHand == DELID_PLATE)
	{
		return DoleOut_DelidPlate();
	}
	if (taskAtHand == RELID_PLATE)
	{
		return DoleOut_RelidPlate();
	}
	if (taskAtHand == MOVE_TIP_RACK)
	{
		return DoleOut_MoveTipRack();
	}
	if (taskAtHand == GET_CXR_PROPERTIES)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_SYSTEM_SERIAL_NUMBER)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == APPEND_TO_CALIBRATION_LOG)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_CALIBRATION_LOG)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == RESET_CALIBRATION_LOG)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == APPEND_TO_MAINTENANCE_LOG)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_MAINTENANCE_LOG)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == RESET_MAINTENANCE_LOG)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_HARDWARE_EVENT_LOG)
	{
		return Fulfill_GetHardwareEventLog();
	}
	if (taskAtHand == RESET_HARDWARE_EVENT_LOG)
	{
		return Fulfill_ResetHardwareEventLog();
	}
	if (taskAtHand == HOLD_OPERATION_STACK)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == HOLD_ALL_OPERATIONS)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_ALARMS_LOG)
	{
		return Fulfill_GetAlarmsLog();
	}
	if (taskAtHand == RESET_ALARMS_LOG)
	{
		return Fulfill_ResetAlarmsLog();
	}
	if (taskAtHand == GET_CONNECTION_LOG)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == RESET_CONNECTION_LOG)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == ADD_PLATE_TYPE_TO_DICTIONARY)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == REMOVE_PLATE_TYPE_FROM_DICTIONARY)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_PLATE_TYPE)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == ADD_TIP_TYPE_TO_DICTIONARY)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == REMOVE_TIP_TYPE_FROM_DICTIONARY)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == GET_TIP_TYPE)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == SET_ZZ_AXIS_SPEED)
	{
		return DoleOut_setZZAxisSpeed();
	}
	if (taskAtHand == SET_Z_AXIS_SPEED)
	{
		return DoleOut_setZAxisSpeed();
	}
	if (taskAtHand == MOVE_X_TO_TIP_SKEW_TRIGGER)
	{
		return DoleOut_moveXToTipSkewTrigger( true );
	}
	if (taskAtHand == MOVE_Y_TO_TIP_SKEW_TRIGGER)
	{
		return DoleOut_moveYToTipSkewTrigger( true );
	}
	if (taskAtHand == GET_TIP)
	{
		return DoleOut_GetTip();
	}
	if (taskAtHand == MOVE_TIP_TO_WASTE_LOCATION_3)
	{
		return DoleOut_MoveTipToWasteLocation3();
	}
	if (taskAtHand == MOVE_TIP_TO_WASTE_LOCATION_2)
	{
		return DoleOut_MoveTipToWasteLocation2();
	}
	if (taskAtHand == MOVE_TIP_TO_WASTE_LOCATION_1)
	{
		return DoleOut_MoveTipToWasteLocation1();
	}
	if (taskAtHand == GET_TIP_SKEW)
	{
		return DoleOut_GetTipSkew();
	}
	if (taskAtHand == SEAT_TIP)
	{
		return DoleOut_SeatTip();
	}
	if (taskAtHand == SET_WORKING_TIP)
	{
		return DoleOut_SetWorkingTip();
	}
	if (taskAtHand == REMOVE_TIP)
	{
		return DoleOut_RemoveTip();
	}
	if (taskAtHand == SET_OPTICAL_COLUMN_TURRET_POSITION)
	{
		return DoleOut_SetOpticalColumnTurretPosition();
	}
	if (taskAtHand == GET_AUTO_FOCUS_IMAGE_STACK)
	{
		return DoleOut_GetAutoFocusImageStack( *commandId, *commandGroupId, *sessionId, machineId );
	}
	if (taskAtHand == CHANGE_ACTIVE_TIP_RACK)
	{
		return DoleOut_notImplemented( taskAtHand );
	}
	if (taskAtHand == MOVE_ALL_TIPS_TO_SAFE_HEIGHT)
	{
		return DoleOut_MoveAllTipsToSafeHeight();
	}
	if (taskAtHand == SET_HARDWARE_STATE)
	{
		return DoleOut_SetHardwareState();
	}
	if (taskAtHand == INIT_HARDWARE_CONTROLLERS)
	{
		return DoleOut_InitHardwareControllers();
	}
	if (taskAtHand == INIT_SERIAL_CONTROLLER)
	{
		return DoleOut_InitSerialControllers();
	}
	if (taskAtHand == HOME_SAFE_HARDWARE)
	{
		return DoleOut_HomeSafeHardware();
	}
	if (taskAtHand == HOME_UNSAFE_HARDWARE)
	{
		return DoleOut_HomeUnsafeHardware();
	}
	if (taskAtHand == INITIALIZE_PICKING_PUMP1)
	{
		return DoleOut_HomeInitializePickingPump();
	}
	if (taskAtHand == MOVE_HARDWARE_TO_ACTIVE_STATES)
	{
		return DoleOut_MoveHardwareToActiveStates();
	}
	if (taskAtHand == CLEAR_KILLS)
	{
		return DoleOut_ClearKills();
	}
	return DoleOut_notImplemented( taskAtHand );
}

///----------------------------------------------------------------------
/// FreezeWorkInProgress
/// Put thread into wait state while HoldAllOperations is in place.
/// Sprinkle calls to this in long-running work functions. Doing so will
/// empower them to pause while HoldAllOperations is active.
///
/// This function remains a NOP when HoldOperationStack is active.
///----------------------------------------------------------------------
void CommandDolerThread::FreezeWorkInProgress( void )
{
	bool doLog = true;
	while (g_EsHoldState.load(std::memory_order_acquire) == HoldType::HoldUp_Freeze)
	{
		if (doLog)
		{
			LOG_DEBUG( m_threadName, "hold", "HOLD-UP FREEZE" );
			doLog = false;
		}
		unique_lock<mutex> lock2(g_holdMutex);
		g_holdCVA.wait(lock2);
	}
}

///----------------------------------------------------------------------
/// ConfirmThreadIdentity
/// Compare threadName with m_threadName. If they differ, print an error
/// and return false.
/// Return true if they match.
///----------------------------------------------------------------------
bool CommandDolerThread::ConfirmThreadIdentity( const char * threadName, const char * command )
{
	if (strcmp(m_threadName, threadName) != 0)
	{
		cout << "ERROR! Command " << command << " being handled by thread " << m_threadName << " instead of " << threadName << endl;
		return false;
	}
	return true;
}

///----------------------------------------------------------------------
/// InitializeWorkingData
/// If log files don't already exist, create them.
/// Each log file is treated like a json array with the closing bracket
/// left out to make it easy to append entries. The closing bracket will
/// be appended to any outgoing copy.
///----------------------------------------------------------------------
void CommandDolerThread::InitializeWorkingData()
{
	if (m_threadName == DOLER_FOR_INFO)
	{
		Implement_ResetHardwareEventLog();
		Implement_ResetAlarmsLog();
	}

	/// The CXPM thread handles all commands dealing with plates and tips
	if (m_threadName == DOLER_FOR_CXPM)
	{
		m_plateInventory = &g_plateInventory;
		m_plateInventory->InitializeWithDefaultValues();
		m_plateInventory->ReadFromFiles( PLATES_DIR );
		m_tipInventory = &g_tipInventory;
		m_tipInventory->InitializeWithDefaultValues();
		m_tipInventory->ReadFromFiles( TIPS_DIR );
	}
	else
	{
		m_plateInventory = &g_plateInventory; // TODO: Replace with something lightweight.
		m_tipInventory = &g_tipInventory; // TODO: Replace with something lightweight.
	}
}


bool CommandDolerThread::WriteToLogFile( string & logFilePath, string & content )
{
	unique_lock<mutex> lock( g_logFileMutex );
	bool succeeded = false;
	std::fstream out( logFilePath.c_str(), std::ios::app );
	{
		if (out.is_open())
		out << content;
		out.close();
		succeeded = true;
	}
	return succeeded;
}

///----------------------------------------------------------------------
/// Process
///----------------------------------------------------------------------
void CommandDolerThread::Process()
{
	bool isPartOfCompoundCommand = false;
	int statusCode = STATUS_DEVICE_ERROR;
	InitializeWorkingData();
	string taskAtHand = "";

	while (m_continueRunning && m_inBox && m_outBox)
	{
		std::shared_ptr<WorkMsg> workMsg = m_inBox->WaitAndPopMsg();
		if (false)
		{
			stringstream msg;
			msg << "Received: " << workMsg->msgType;
			LOG_TRACE( m_threadName, "mqtt", msg.str().c_str() );
		}

		/// See ExitThread().
		if (workMsg->msgType == "end") break;

		; // TODO: Clear m_currentWorkMsgType so it can be an informative status variable for use by other threads. Maybe make it std::atomic<> too.
		if (workMsg->msgType != CHECK_HARDWARE)
		{
//			cout << m_threadName << ": received workMsg." << endl;
		}
		
		/// CommandInterpreter sometimes forwards status messages through here.
		std::string channel = ExtractChannel(workMsg->destination);
		if (channel == TEXT_STATUS)
		{
//			cout << "Command Doler is forwarding a status" << endl;
			m_outBox->PushMsg(workMsg);
			/// Do nothing else on this loop.
		}
		else
		{
			if (workMsg->msgType != CHECK_HARDWARE)
			{
				/// Check for other threads sharing this workMsg. This can be bad.
				/// Take corrective action if you see this happen.
				if (!workMsg.unique())
				{
					stringstream msg;
					msg << "WARNING! WARNING! The work message is not unique in command " << taskAtHand;
					LOG_WARN( m_threadName, "-", msg.str().c_str() );
				}
			}
			taskAtHand = workMsg->msgType;
			m_statusPayloadOutObj = json_object_new_object();
			/// Transform the JSON message string into a json_object.
			statusCode = STATUS_OK; /// ... until it is not.
			m_currentWorkDestination = workMsg->destination;
			m_currentWorkOrigin = workMsg->origin;
			m_currentWorkMsgType = workMsg->msgType;

			/// Capture references to the workMsg's blob, for the rare instances when a command
			/// needs to send binary data to the next thread. One example would be an unprocessed image
			/// sent to the MqttSender, allowing that thread to take on the work of packaging the blob
			/// for adding to a JSON payload.
			m_blobPtr = &workMsg->blob;
			m_blobSizePtr = &workMsg->blobSize;

			bool messageParsed = false;
			if (workMsg->msgType != CHECK_HARDWARE)
			{
				stringstream msg;
				// msg << "received payload is " << workMsg->payload;
				msg << "received message_type is " << workMsg->msgType;
				LOG_TRACE( m_threadName, "-", msg.str().c_str() );
			}
			m_commandMessageInObj = m_desHelper->ParseJsonDesMessage( workMsg->payload.c_str(), &messageParsed );
			LETS_ASSERT( messageParsed );

			/// Get the command ID of the top command. This will be returnd by DoWork.
			/// In the case of a compound command, all the sub-commands are given the
			/// same ID, so we really only use the last one we get.
			int commandId = 0;
			int commandGroupId = 0;
			string sessionId = "";
			m_doSendStatus = true;
			int newStatusCode = DoWork( taskAtHand, &commandId, &commandGroupId, &sessionId );
			if (workMsg->msgType != CHECK_HARDWARE)
			{
//				cout << m_threadName << ": DoWork is done." << endl;
			}
			if (newStatusCode != STATUS_OK)
			{
				statusCode = newStatusCode;
			}
			m_blobPtr = nullptr;
			m_blobSizePtr = nullptr;

			if (m_doSendStatus)
			{
//				cout << m_threadName << "Command Doler is replacing payload with null" << endl;
				/// Recycle the workMsg, turning it into a status message.
				json_object * payloadToGoObj = nullptr;
				json_object_deep_copy( m_statusPayloadOutObj, &payloadToGoObj, nullptr );
				workMsg->payload = m_desHelper->ComposeStatusString( commandGroupId,
																	 commandId,
																	 sessionId,
																	 workMsg->msgType,
																	 "", /// machineId
																	 statusCode,
																	 payloadToGoObj );
				/// destination topic becomes "/status".
				workMsg->destination = ReplaceTopicChannel(workMsg->destination, TEXT_STATUS);
				m_outBox->PushMsg(workMsg);
//				cout << m_threadName << ": m_outBox->PushMsg(workMsg) is done." << endl;
			}
			else
			{
				workMsg.reset();
			}

			if (m_statusPayloadOutObj)
			{
				int result = json_object_put( m_statusPayloadOutObj );
				if (result != 1)
				{
					cout << m_threadName << ": json_object_put( m_statusPayloadOutObj ) failed. Result is  " << result << endl;
				}
			}
			if (m_commandMessageInObj)
			{
				int result = json_object_put( m_commandMessageInObj );
				if (result != 1)
				{
					cout << m_threadName << ": json_object_put( m_commandMessageInObj ) failed. Result is " << result << endl;
				}
			}
		}
	}
}

/// Changes the embedded server's internal clock.
int CommandDolerThread::Fulfill_SynchronizeTime()
{
	if (!ConfirmThreadIdentity( DOLER_FOR_INFO, SYNCHRONIZE_TIME )) return STATUS_DEVICE_ERROR;
	unsigned long unixTime = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, TIMESTAMP );
	cout << m_threadName << ": Fulfill_SynchronizeTime() received " << unixTime << endl;
	if (SynchronizeTime( unixTime ))
	{
		stringstream msg;
		msg << "synchronized to " << unixTime;
		LOG_TRACE( m_threadName, "time", msg.str().c_str() ); 
	}
	else
	{
		stringstream msg;
		msg << "did not synchronize to " << unixTime;
		LOG_WARN( m_threadName, "time", msg.str().c_str() ); 
	}
	return STATUS_OK;
}

int CommandDolerThread::Fulfill_GetHardwareEventLog()
{
	string filePath = LOG_DIR;
	filePath += "/";
	filePath += HW_EVENT_LOG;
	int status = STATUS_OK;
	
	stringstream fileContents;
	{
		unique_lock<mutex> lock( g_logFileMutex );
		fstream in( filePath.c_str(), std::ios::in );
		if (in.is_open())
		{
			fileContents << in.rdbuf();
		}
		in.close();
	}
	/// Note: We expect the file to start with a '[' and NOT end with a ']'.
	/// A log empty of content will contain "[".

	// TODO: I hope this section can become superfluous by collecting all optical column log events via the ping command, instead. -MDG
	/// Some hardware components keep their own log files. Here, we will gather them and append them to fileContents.
	string aLog = "";
	if (DoleOut_GetOpticalColumnEventLog( aLog ) == STATUS_OK)
	{
		RemoveBrackets( aLog ); /// Remove the brackets bounding a JSON list in string format.
		/// If both strings contain content, we need to add a comma between them when we concatenate them.
		if ((fileContents.str().length() > 1) && (aLog.length() > 0))
		{
			fileContents << ",";
			fileContents << aLog;
		}
		else
		{
			fileContents << aLog; /// Safe to NOT add ',' because at least one of these is empty.
		}
	}
	// TODO: I hope this section can become superfluous by collecting all fluorescence light filter log events via the ping command, instead. -MDG
	aLog = "";
	if (DoleOut_GetFluorescenceLightFilterEventLog( aLog ) == STATUS_OK)
	{
		RemoveBrackets( aLog ); /// Remove the brackets bounding a JSON list in string format.
		/// If both strings contain content, we need to add a comma between them when we concatenate them.
		if ((fileContents.str().length() > 1) && (aLog.length() > 0))
		{
			fileContents << ",";
			fileContents << aLog;
		}
		else
		{
			fileContents << aLog; /// Safe to NOT add ',' because at least one of these is empty.
		}
	}
	
	fileContents << "]"; /// Add closing bracket to make it a valid JSON array.
	bool succeeded = false;
	/// Convert the file string into a JSON object. We expect it to wind up being an
	/// array of hardware-event objects.
	
	json_object * arrayOfHwEventsOjb = m_desHelper->ParseJsonDesMessage( fileContents.str().c_str(), &succeeded );
	if (!succeeded)
	{
		cout << m_threadName << ": ERROR! failed to convert " << fileContents.str() << " to a JSON object." << endl;
		return STATUS_DEVICE_ERROR;
	}

    int result = json_object_object_add( m_statusPayloadOutObj,
										 HARDWARE_EVENT_LOG,
										 arrayOfHwEventsOjb );
	return status;
}

int CommandDolerThread::OpticalColumnStatusCode_to_ESStatusCode( int opticalColumnStatusCode )
{
	int status = STATUS_OK;
	return status; // TODO: Implement a real conversion!!!
}

int CommandDolerThread::FluorescencLightFilterStatusCode_to_ESStatusCode( int fluorescencLightFilterStatusCode )
{
	return fluorescencLightFilterStatusCode; // TODO: Implement a real conversion!!!
}

/// Called by Fulfill_GetHardwareEventLog().
int CommandDolerThread::DoleOut_GetOpticalColumnEventLog( string & jsonLogStr ) // TODO: Deprecate and remove. Otherwise, fix the memory handling.
{
	if (!m_mqttHardwareIO)
	{
		cout << m_threadName << "ERROR: m_mqttHardwareIO is null." << endl;
		return STATUS_DEVICE_ERROR; // EARLY RETURN!	EARLY RETURN!
	}

	json_object * commandMessageOut = NULL;
	/// Make a temporary copy of m_commandMessageInObj so we can re-use much of it for our outgoing message
	/// without risk of breaking its use in sending status to MqttSender after we are done here.
	int rc = json_object_deep_copy( m_commandMessageInObj, &commandMessageOut, NULL );
	FreezeWorkInProgress();
	bool succeeded = true;
	bool ackReceived = false;
	int ack = 0;
	json_object * jpayloadObj = nullptr;
	stringstream streamtopic;
	streamtopic << TOPIC_MICROSCOPE << CHANNEL_COMMAND;
	string topic = streamtopic.str();

	/// Change a few parts of commandMessageOut.
	string messageType = GET_OPTICAL_COLUMN_EVENT_LOG;
	json_object * focusPayloadObj = json_object_new_object();
	rc = json_object_object_add( commandMessageOut, MESSAGE_TYPE, json_object_new_string( GET_OPTICAL_COLUMN_EVENT_LOG ) );
	succeeded &= (rc == 0);

	string desCallMessage = json_object_to_json_string( commandMessageOut );
	json_object_put( commandMessageOut ); /// Free memory.
	FreezeWorkInProgress();
	int opticalColumnStatusCode = m_mqttHardwareIO->CommandTheHardwareClient( messageType, desCallMessage, topic, ackReceived, ack, &jpayloadObj );
	succeeded &= ackReceived;
	succeeded &= (ack == STATUS_OK);
	succeeded &= (jpayloadObj != NULL);

		json_object * hwEventLogObj = json_object_object_get( jpayloadObj, HARDWARE_EVENT_LOG );
		if (hwEventLogObj)
		{
			jsonLogStr = json_object_get_string( hwEventLogObj );
		}
		else
		{
			succeeded = false;
		}

	return succeeded ? STATUS_OK : STATUS_DEVICE_ERROR;
}

/// Called by Fulfill_GetHardwareEventLog().
int CommandDolerThread::DoleOut_GetFluorescenceLightFilterEventLog( string & jsonLogStr ) // TODO: Deprecate and remove. Otherwise, fix the memory handling.
{
	if (!m_mqttHardwareIO)
	{
		cout << m_threadName << "ERROR: m_mqttHardwareIO is null." << endl;
		return STATUS_DEVICE_ERROR; // EARLY RETURN!	EARLY RETURN!
	}
// TODO: Modify all this:
	json_object * commandMessageOut = NULL;
	/// Make a temporary copy of m_commandMessageInObj so we can re-use much of it for our outgoing message
	/// without risk of breaking its use in sending status to MqttSender after we are done here.
	int rc = json_object_deep_copy( m_commandMessageInObj, &commandMessageOut, NULL );
	FreezeWorkInProgress();
	bool succeeded = true;
	bool ackReceived = false;
	int ack = 0;
	json_object * jpayloadObj = nullptr;
	stringstream streamtopic;
	streamtopic << TOPIC_MICROSCOPE << CHANNEL_COMMAND;
	string topic = streamtopic.str();

	/// Change a few parts of commandMessageOut.
	string messageType = GET_FLUORESCENCE_LIGHT_FILTER_EVENT_LOG;
	json_object * focusPayloadObj = json_object_new_object();
	rc = json_object_object_add( commandMessageOut, MESSAGE_TYPE, json_object_new_string( GET_FLUORESCENCE_LIGHT_FILTER_EVENT_LOG ) );
	succeeded &= (rc == 0);

	string desCallMessage = json_object_to_json_string( commandMessageOut );
	json_object_put( commandMessageOut ); /// Free memory.
	FreezeWorkInProgress();
	int opticalColumnStatusCode = m_mqttHardwareIO->CommandTheHardwareClient( messageType, desCallMessage, topic, ackReceived, ack, &jpayloadObj );
	succeeded &= ackReceived;
	succeeded &= (ack == STATUS_OK);
	succeeded &= (jpayloadObj != NULL);

		json_object * hwEventLogObj = json_object_object_get( jpayloadObj, HARDWARE_EVENT_LOG );
		if (hwEventLogObj)
		{
			jsonLogStr = json_object_get_string( hwEventLogObj );
		}
		else
		{
			succeeded = false;
		}

	return succeeded ? STATUS_OK : STATUS_DEVICE_ERROR;
}

int CommandDolerThread::Fulfill_ResetHardwareEventLog()
{
	m_doSendStatus = false;  /// No status message for reset_hardware_event_log.
	return Implement_ResetHardwareEventLog();
}

int CommandDolerThread::Implement_ResetHardwareEventLog()
{
	int status = STATUS_OK;
	string logFilePath = "";
	{
		stringstream wip;
		wip << LOG_DIR << "/" << HW_EVENT_LOG;
		logFilePath = wip.str();
	}
	if (FileExists( logFilePath )) {
		unique_lock<mutex> lock( g_logFileMutex );
		if( remove( logFilePath.c_str() ) != 0 )
		{
			stringstream msg;
			msg << "Failed to delete " << logFilePath;
			LOG_ERROR( m_threadName, "reset", msg.str().c_str() );
			return STATUS_DEVICE_ERROR;
		}
	}
	/// Create a new hardware event log
	if (!FileExists( logFilePath ))
	{
		stringstream wip;
		std::chrono::system_clock::rep timeInSec = TimeSinceEpoch();
		wip << "[{\"timestamp\": " << timeInSec << ", \"user_id\": 0, \"event_type\": 0, \"action\": 0, \"description\": \"hardware event log opened\"}";
		string logStartString = wip.str();
		WriteToLogFile( logFilePath, logStartString );
	}
	return status;
}

int CommandDolerThread::Fulfill_GetAlarmsLog()
{
	int status = STATUS_OK;
	string filePath = LOG_DIR;
	filePath += "/";
	filePath += ALARMS_LOG;
	
	stringstream fileContents;
	{
		unique_lock<mutex> lock( g_logFileMutex );
		fstream in( filePath.c_str(), std::ios::in );
		if (in.is_open())
		{
			fileContents << in.rdbuf();
		}
		in.close();
	}
	fileContents << "]"; /// Add closing bracket to make it a valid JSON array.
	bool succeeded = false;
	/// Convert the file string into a JSON object. We expect it to wind up being an
	/// array of alarm objects.
	json_object * arrayOfAlarmsOjb = m_desHelper->ParseJsonDesMessage( fileContents.str().c_str(), &succeeded );
	if (!succeeded)
	{
		cout << m_threadName << ": ERROR! failed to convert " << fileContents.str() << " to a JSON object." << endl;
		return STATUS_DEVICE_ERROR;
	}

    int result = json_object_object_add( m_statusPayloadOutObj,
										 HARDWARE_EVENT_LOG,
										 arrayOfAlarmsOjb );
	return status;
}

int CommandDolerThread::Fulfill_ResetAlarmsLog()
{
	m_doSendStatus = false;  /// No status message for reset_alarms_log.
	return Implement_ResetAlarmsLog();
}

int CommandDolerThread::Implement_ResetAlarmsLog()
{
	int status = STATUS_OK;
	string logFilePath = "";
	{
		stringstream wip;
		wip << LOG_DIR << "/" << ALARMS_LOG;
		logFilePath = wip.str();
	}
	if (FileExists( logFilePath )) {
		unique_lock<mutex> lock( g_logFileMutex );
		if( remove( logFilePath.c_str() ) != 0 )
		{
			stringstream msg;
			msg << "Failed to delete " << logFilePath;
			LOG_ERROR( m_threadName, "reset", msg.str().c_str() );
			return STATUS_DEVICE_ERROR;
		}
	}
	/// Create a new alarms log.
	if (!FileExists( logFilePath ))
	{
		stringstream wip;
		std::chrono::system_clock::rep timeInSec = TimeSinceEpoch();
		wip << "[{\"timestamp\": " << timeInSec << ", \"user_id\": 0, \"alarm_type\": 0, \"action\": 0, \"description\": \"alarm log opened\"}";
		string logStartString = wip.str();
		WriteToLogFile( logFilePath, logStartString );
	}
	return status;
}

/// Ping hardware or otherwise check its health.
int CommandDolerThread::DoleOut_CheckHardware()
{
	int status = STATUS_OK;
	m_doSendStatus = false; /// Because this is an internal command, nobody outside the ES expects a status.
	if (m_threadName == DOLER_FOR_OPTICS)
	{
//		cout << m_threadName << ": CheckHardeware called for Optics." << endl;
		ComponentState opticalColumnState = g_OpticalColumnState.load( std::memory_order_acquire );
		if (opticalColumnState != ComponentState::Mocked)
		{
			// if (DoleOut_GetOpticalColumnStatus() == STATUS_DEVICE_ERROR)
			if (false) // TODO: Restore the call to DoleOut_GetOpticalColumnStatus after it is allotted budget.
			{
				g_OpticalColumnState.store( ComponentState::Faulty, std::memory_order_relaxed );
			}
			else
			{
				g_OpticalColumnState.store( ComponentState::Good, std::memory_order_relaxed );
			}
		}
		ComponentState fluorescenceLightFilterState = g_FluorescenceLightFilterState.load( std::memory_order_acquire );
		if (fluorescenceLightFilterState != ComponentState::Mocked)
		{
			// if (DoleOut_FluorescenceLightFilterStatus() == STATUS_DEVICE_ERROR)
			if (false) // TODO: Restore the call to DoleOut_FluorescenceLightFilterStatus after it is allotted budget.
			{
				g_FluorescenceLightFilterState.store( ComponentState::Faulty, std::memory_order_relaxed );
			}
			else
			{
				g_FluorescenceLightFilterState.store( ComponentState::Good, std::memory_order_relaxed );
			}
		}
		ComponentState cameraState = g_CameraState.load( std::memory_order_acquire );
		if (cameraState != ComponentState::Mocked)
		{
			unique_lock<mutex> lock( g_cameraMutex );
			if (g_CameraList.size() > 0)
			{
				string camMsg = "";
				int errorCode = g_CameraList[0]->GetErrorState( camMsg );
				if (errorCode == 0)
				{
					g_CameraState.store( ComponentState::Good, std::memory_order_relaxed );
				}
				else
				{
					g_CameraState.store( ComponentState::Faulty, std::memory_order_relaxed );
					stringstream logMsg;
					logMsg << "errorCode=" << errorCode;
					LOG_WARN( m_threadName, "camera", logMsg.str().c_str() );
				}
			}
			else
			{
				LOG_WARN( m_threadName, "camera", "Camera was never found" );
			}
		}
		/// Test for limited memory.
		/// One of the biggest use of memory is for images. Our test should be a size
		/// bigger than a typical image to help us identify a problem before it happens
		/// somewhere else.
		void * testBuffer = malloc( 8000000 );
		bool mallocFailed = (testBuffer == NULL);
		free( testBuffer );
		if (mallocFailed)
		{
			LOG_ERROR( m_threadName, "hardware-check", "RUNNING LOW ON MEMORY. HOLDING..." );
			g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed ); /// Cause a hold.
		}
	}
	if (m_threadName == DOLER_FOR_INFO)
	{
	}
	if (m_threadName == DOLER_FOR_CXD)
	{
		if (true)
		{
			unique_lock<mutex> lock( g_HardwareCheckMutex );
			int status = DoleOut_ConfirmNoKillAllMotionRequests( A_CONTROLLER );
			if ((STATUS_KILL_MOTION_Y <= status) && (status <= STATUS_KILL_MOTION_YY))
			{
				stringstream topic_stream;
				topic_stream << TOPIC_ES << CHANNEL_ALERT;
				switch (status)
				{
					case STATUS_KILL_MOTION_Y:
						if (g_XYState.load( std::memory_order_acquire ) == ComponentState::Good)
						{
							g_XYState.store( ComponentState::Faulty, std::memory_order_relaxed );
							LOG_ERROR( m_threadName, "health", "Y axis kill-all-motion-flag" );
							/// Send an alert.
							json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT, 0, 0, topic_stream.str(),
																				ComponentType::XYAxes,
																				status,
																				true ); /// thisIsCausingHold
							bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
						}
						break;
					case STATUS_KILL_MOTION_X:
						if (g_XYState.load( std::memory_order_acquire ) == ComponentState::Good)
						{
							g_XYState.store( ComponentState::Faulty, std::memory_order_relaxed );
							LOG_ERROR( m_threadName, "health", "X axis kill-all-motion-flag" );
							/// Send an alert.
							json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT, 0, 0, topic_stream.str(),
																				ComponentType::XYAxes,
																				status,
																				true ); /// thisIsCausingHold
							bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
						}
						break;
					case STATUS_KILL_MOTION_XX:
						if (g_XXState.load( std::memory_order_acquire ) == ComponentState::Good)
						{
							g_XXState.store( ComponentState::Faulty, std::memory_order_relaxed );
							LOG_ERROR( m_threadName, "health", "XX axis kill-all-motion-flag" );
							/// Send an alert.
							json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT, 0, 0, topic_stream.str(),
																				ComponentType::XXAxis,
																				status,
																				true ); /// thisIsCausingHold
							bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
						}
						break;
					case STATUS_KILL_MOTION_YY:
						if (g_YYState.load( std::memory_order_acquire ) == ComponentState::Good)
						{
							g_YYState.store( ComponentState::Faulty, std::memory_order_relaxed );
							LOG_ERROR( m_threadName, "health", "YY axis kill-all-motion-flag" );
							/// Send an alert.
							json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT, 0, 0, topic_stream.str(),
																				ComponentType::YYAxis,
																				status,
																				true ); /// thisIsCausingHold
							bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
						}
				}
			}
		}

		if (true)
		{
			unique_lock<mutex> lock( g_HardwareCheckMutex );
			int status = DoleOut_ConfirmNoKillAllMotionRequests( B_CONTROLLER );
			if ((STATUS_KILL_MOTION_SH <= status) && (status <= STATUS_KILL_MOTION_ZZZ))
			{
				stringstream topic_stream;
				topic_stream << TOPIC_ES << CHANNEL_ALERT;
				switch (status)
				{
					case STATUS_KILL_MOTION_SH:
						if (g_AnnularRingHolderState.load( std::memory_order_acquire ) == ComponentState::Good)
						{
							g_AnnularRingHolderState.store( ComponentState::Faulty, std::memory_order_relaxed );
							LOG_ERROR( m_threadName, "health", "SH axis kill-all-motion-flag" );
							/// Send an alert.
							json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT, 0, 0, topic_stream.str(),
																				ComponentType::AnnularRingHolder,
																				status,
																				true ); /// thisIsCausingHold
							bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
						}
						break;
					case STATUS_KILL_MOTION_Z:
						if (g_ZState.load( std::memory_order_acquire ) == ComponentState::Good)
						{
							g_ZState.store( ComponentState::Faulty, std::memory_order_relaxed );
							LOG_ERROR( m_threadName, "health", "Z axis kill-all-motion-flag" );
							/// Send an alert.
							json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT, 0, 0, topic_stream.str(),
																				ComponentType::ZAxis,
																				status,
																				true ); /// thisIsCausingHold
							bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
						}
						break;
					case STATUS_KILL_MOTION_ZZ:
						if (g_ZZState.load( std::memory_order_acquire ) == ComponentState::Good)
						{
							g_ZZState.store( ComponentState::Faulty, std::memory_order_relaxed );
							LOG_ERROR( m_threadName, "health", "ZZ axis kill-all-motion-flag" );
							/// Send an alert.
							json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT, 0, 0, topic_stream.str(),
																				ComponentType::ZZAxis,
																				status,
																				true ); /// thisIsCausingHold
							bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
						}
						break;
					case STATUS_KILL_MOTION_ZZZ:
						if (g_ZZZState.load( std::memory_order_acquire ) == ComponentState::Good)
						{
							g_ZZZState.store( ComponentState::Faulty, std::memory_order_relaxed );
							LOG_ERROR( m_threadName, "health", "ZZZ axis kill-all-motion-flag" );
							/// Send an alert.
							json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT, 0, 0, topic_stream.str(),
																				ComponentType::ZZZAxis,
																				status,
																				true ); /// thisIsCausingHold
							bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
						}
				}
			}
		}
	}
	if (m_threadName == DOLER_FOR_CXPM)
	{
	}
	if (m_threadName == DOLER_FOR_PUMP)
	{
		unique_lock<mutex> lock( g_HardwareCheckMutex );
		int status = DoleOut_ConfirmNoKillAllMotionRequests( C_CONTROLLER );
		if ((STATUS_KILL_MOTION_ASP1 <= status) && (status <= STATUS_KILL_MOTION_DISP3))
		{
			stringstream topic_stream;
			topic_stream << TOPIC_ES << CHANNEL_ALERT;
			switch (status)
			{
				case STATUS_KILL_MOTION_ASP1:
					if (g_AspirationPump1State.load( std::memory_order_acquire ) == ComponentState::Good)
					{
						g_AspirationPump1State.store( ComponentState::Faulty, std::memory_order_relaxed );
						LOG_ERROR( m_threadName, "health", "Aspiration Pump 1 kill-all-motion-flag" );
						/// Send an alert.
						json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT, 0, 0, topic_stream.str(),
																			ComponentType::AspirationPump1,
																			status,
																			true ); /// thisIsCausingHold
						bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
					}
					break;
				case STATUS_KILL_MOTION_DISP1:
					if (g_DispensePump1State.load( std::memory_order_acquire ) == ComponentState::Good)
					{
						g_DispensePump1State.store( ComponentState::Faulty, std::memory_order_relaxed );
						LOG_ERROR( m_threadName, "health", "Dispense Pump 1 kill-all-motion-flag" );
						/// Send an alert.
						json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT, 0, 0, topic_stream.str(),
																			ComponentType::DispensePump1,
																			status,
																			true ); /// thisIsCausingHold
						bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
					}
					break;
				case STATUS_KILL_MOTION_DISP2:
					if (g_DispensePump2State.load( std::memory_order_acquire ) == ComponentState::Good)
					{
						g_DispensePump2State.store( ComponentState::Faulty, std::memory_order_relaxed );
						LOG_ERROR( m_threadName, "health", "Dispense Pump 2 kill-all-motion-flag" );
						/// Send an alert.
						json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT, 0, 0, topic_stream.str(),
																			ComponentType::DispensePump2,
																			status,
																			true ); /// thisIsCausingHold
						bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
					}
					break;
				case STATUS_KILL_MOTION_DISP3:
					if (g_DispensePump3State.load( std::memory_order_acquire ) == ComponentState::Good)
					{
						g_DispensePump3State.store( ComponentState::Faulty, std::memory_order_relaxed );
						LOG_ERROR( m_threadName, "health", "Dispense Pump 3 kill-all-motion-flag" );
						/// Send an alert.
						json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT, 0, 0, topic_stream.str(),
																			ComponentType::DispensePump3,
																			status,
																			true ); /// thisIsCausingHold
						bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
					}
			}
		}
	}
	if (m_threadName == DOLER_FOR_RING_HOLDER)
	{
	}

	return status;
}

int CommandDolerThread::DoleOut_GetOpticalColumnStatus()
{
	if (!m_mqttHardwareIO)
	{
		cout << m_threadName << "ERROR: m_mqttHardwareIO is null." << endl;
		return STATUS_DEVICE_ERROR; // EARLY RETURN!	EARLY RETURN!
	}
	json_object * commandMessageOutOjb = NULL;

	/// Prepare variables that have one-time initialization.
	bool succeeded = true;
	stringstream streamtopic;
	streamtopic << TOPIC_MICROSCOPE << CHANNEL_COMMAND;
	string topic = streamtopic.str();
	bool ackReceived = false;
	int ack = 0;
	json_object * jCommandPayloadObj = json_object_new_object();
	string messageType = GET_OPTICAL_COLUMN_STATUS;
	/// Re-use most of the templateMessageOjb (now commandMessageOutOjb), but change the payload and message_type.

	commandMessageOutOjb = m_desHelper->ComposeCommand( GET_OPTICAL_COLUMN_STATUS,	/// std::string messageType
														0,							/// commandGroupId
														0,							/// commandId
														"",							/// std::string sessionId
														jCommandPayloadObj );		/// json_object * payload
	string desCallMessage = json_object_to_json_string( commandMessageOutOjb );
	json_object_put( commandMessageOutOjb ); /// Free memory
//	cout << m_threadName << " outgoing command: " << desCallMessage << endl;
//	cout << m_threadName << " outgoing topic: " << topic << endl;
//	cout << m_threadName << " messageType: " << messageType << endl;
	json_object * jStatusPayloadObj = nullptr;
	int opticalColumnStatusCode = m_mqttHardwareIO->CommandTheHardwareClient( messageType, desCallMessage, topic, ackReceived, ack, & jStatusPayloadObj );
	succeeded &= ackReceived;
	succeeded &= (ack == STATUS_OK);
	succeeded &= (OpticalColumnStatusCode_to_ESStatusCode( opticalColumnStatusCode ) == STATUS_OK);
	if (succeeded)
	{
		/// Pull log data out of the returned payload and stuff it in the hardware event log.
		unsigned long coded_status = 0L;
		if (jStatusPayloadObj)
		{
			json_object * payloadField = json_object_object_get( jStatusPayloadObj, STATUS );
			if (payloadField)
			{
				coded_status = json_object_get_uint64( payloadField );
			}
			else
			{
				cout << m_threadName << " ERROR: CommandDolerThread::DoleOut_GetOpticalColumnStatus() got status message but could not find STATUS field." << endl;
			}
			payloadField = json_object_object_get( jStatusPayloadObj, DETAILS );
			if (payloadField)
			{
				string detailsStr = json_object_get_string( payloadField );
				if (detailsStr.length() > 0)
				{
//					cout << m_threadName << " CommandDolerThread::DoleOut_GetOpticalColumnStatus() got status. Details are " << detailsStr << endl;
					detailsStr.insert( 0, ",\n{" ); /// Prepend a comma, line feed, and opening bracket.
					detailsStr.insert( detailsStr.end(), '}' );
//					cout << m_threadName << " DoleOut_GetOpticalColumnStatus received log details: " << detailsStr << endl;
					/// Stuff it in the file.
					stringstream wip;
					wip << LOG_DIR << "/" << HW_EVENT_LOG;
					string logFilePath = wip.str();
					WriteToLogFile( logFilePath, detailsStr );
				}
			}
			else
			{
				cout << m_threadName << " ERROR: CommandDolerThread::DoleOut_GetOpticalColumnStatus() got status message but could not find DETAILS field." << endl;
			}
			int freed = json_object_put( jStatusPayloadObj );
			if (freed != 1) cout << m_threadName << " ERROR: CommandDolerThread::DoleOut_GetOpticalColumnStatus() failed to free memory in jStatusPayloadObj." << endl;
			
		}
		return STATUS_OK;	// EARLY RETURN!	EARLY RETURN!
	}
	else
	{
		cout << m_threadName << " ERROR: CommandDolerThread::DoleOut_GetOpticalColumnStatus() Failed to receive ack or status messages." << endl;
	}

	return STATUS_DEVICE_ERROR;
}

int CommandDolerThread::DoleOut_FluorescenceLightFilterStatus()
{
	if (!m_mqttHardwareIO)
	{
		cout << m_threadName << "ERROR: m_mqttHardwareIO is null." << endl;
		return STATUS_DEVICE_ERROR; // EARLY RETURN!	EARLY RETURN!
	}
	json_object * commandMessageOutOjb = NULL;

	/// Prepare variables that have one-time initialization.
	bool succeeded = true;
	stringstream streamtopic;
	streamtopic << TOPIC_MICROSCOPE << CHANNEL_COMMAND;
	string topic = streamtopic.str();
	bool ackReceived = false;
	int ack = 0;
	json_object * jCommandPayloadObj = json_object_new_object();
	string messageType = GET_FLUORESCENCE_LIGHT_FILTER_STATUS;
	/// Re-use most of the templateMessageOjb (now commandMessageOutOjb), but change the payload and message_type.

	commandMessageOutOjb = m_desHelper->ComposeCommand( GET_FLUORESCENCE_LIGHT_FILTER_STATUS,	/// std::string messageType
														0,										/// commandGroupId
														0,										/// commandId
														"",										/// std::string sessionId
														jCommandPayloadObj );					/// json_object * payload
	string desCallMessage = json_object_to_json_string( commandMessageOutOjb );
	json_object_put( commandMessageOutOjb ); /// Free memory
//	cout << m_threadName << " outgoing command: " << desCallMessage << endl;
//	cout << m_threadName << " outgoing topic: " << topic << endl;
//	cout << m_threadName << " messageType: " << messageType << endl;
	json_object * jStatusPayloadObj = nullptr;
	int fluorescencLightFilterStatusCode = m_mqttHardwareIO->CommandTheHardwareClient( messageType, desCallMessage, topic, ackReceived, ack, & jStatusPayloadObj );
	succeeded &= ackReceived;
	succeeded &= (ack == STATUS_OK);
	succeeded &= (FluorescencLightFilterStatusCode_to_ESStatusCode( fluorescencLightFilterStatusCode ) == STATUS_OK);
	if (succeeded)
	{
		/// Pull log data out of the returned payload and stuff it in the hardware event log.
		unsigned long coded_status = 0L;
		if (jStatusPayloadObj)
		{
			json_object * payloadField = json_object_object_get( jStatusPayloadObj, STATUS );
			if (payloadField)
			{
				coded_status = json_object_get_uint64( payloadField );
			}
			else
			{
				cout << m_threadName << " ERROR: CommandDolerThread::DoleOut_FluorescenceLightFilterStatus() got status message but could not find STATUS field." << endl;
			}
			payloadField = json_object_object_get( jStatusPayloadObj, DETAILS );
			if (payloadField)
			{
				string detailsStr = json_object_get_string( payloadField );
				if (detailsStr.length() > 0)
				{
//					cout << m_threadName << " CommandDolerThread::DoleOut_FluorescenceLightFilterStatus() got status. Details are " << detailsStr << endl;
					detailsStr.insert( 0, ",\n{" ); /// Prepend a comma, line feed, and opening bracket.
					detailsStr.insert( detailsStr.end(), '}' );
//					cout << m_threadName << " CommandDolerThread::DoleOut_FluorescenceLightFilterStatus() received log details: " << detailsStr << endl;
					/// Stuff it in the file.
					stringstream wip;
					wip << LOG_DIR << "/" << HW_EVENT_LOG;
					string logFilePath = wip.str();
					WriteToLogFile( logFilePath, detailsStr );
				}
			}
			else
			{
				cout << m_threadName << " ERROR: CommandDolerThread::DoleOut_FluorescenceLightFilterStatus() got status message but could not find DETAILS field." << endl;
			}
			int freed = json_object_put( jStatusPayloadObj );
			if (freed != 1) cout << m_threadName << " ERROR: CommandDolerThread::DoleOut_FluorescenceLightFilterStatus() failed to free memory in jStatusPayloadObj." << endl;
			
		}
		return STATUS_OK;	// EARLY RETURN!!	EARLY RETURN!!
	}
	else
	{
		cout << m_threadName << " ERROR: CommandDolerThread::DoleOut_FluorescenceLightFilterStatus() Failed to receive ack or status messages." << endl;
	}

	return STATUS_DEVICE_ERROR;
}


/// Moves the camara to 1 or more locations. At each location, it
/// 1. takes a picture (not implemented yet)
/// 2. saves the picture to file (not implemented yet)
/// 3. sends a nearly complete status message to MqttSender with the filename in the payload.
///    Upon reciept, MqttSender will read the file and send it to the client. Maybe as the payload of the status message.
int CommandDolerThread::DoleOut_CollectMontageImages( int commandId, int commandGroupId, string sessionId, string machineId )
{
	if (m_threadName != DOLER_FOR_CXD)
	{
		cout << m_threadName << " : DoleOut_CollectMontageImages run on wrong thread. It should be on the CxD thread." << endl;
		return STATUS_DEVICE_ERROR;
	}
	unique_lock<mutex> lock( g_cameraMutex ); /// Please ensure this is done in every function that access the camera.

	/// Parse the payload to get 4 ulong and 3 float values.
	unsigned long pixel_count_x = 0L;
	unsigned long pixel_count_y = 0L;
	long X_start_nm = 0L;
	long Y_start_nm = 0L;
	unsigned long X_stepCount = 0L;
	unsigned long Y_stepCount = 0L;
	unsigned long bin_factor = 0L;
	unsigned long camera_color_depth = 0L;
	unsigned long scaled_color_depth = 0L;
	float X_stepIncrementNm = 0.0;
	float Y_stepIncrementNm = 0.0;
	float scanMovementDelayMs = 0.0; // TODO: Delete if new changes to collect_montage_images don't ever use it.
	long velocity = 0L;
	long acceleration = 0L;
	long deceleration = 0L;
	long stp = 0L;
	long axisResolutionNm = 300L;
	ComponentState xyState = g_XYState.load( std::memory_order_acquire );

	bool succeeded =  m_desHelper->ExtractCreateMontageParameters( m_threadName,
																   m_commandMessageInObj,
																   &pixel_count_x,
																   &pixel_count_y,
																   &X_stepCount,
																   &Y_stepCount,
																   &X_start_nm,
																   &Y_start_nm,
																   &bin_factor,
																   &camera_color_depth,
																   &scaled_color_depth,
																   &X_stepIncrementNm,
																   &Y_stepIncrementNm,
																   &scanMovementDelayMs,
																   &velocity,
																   &acceleration,
																   &deceleration,
																   &stp,
																   &axisResolutionNm );
	
	long xPosition_nm = X_start_nm;  /// For tracking current position in terms of calculations from inputs.
	long yPosition_nm = Y_start_nm;  /// For tracking current position in terms of calculations from inputs.
	long xReal_nm = xPosition_nm; /// Returned from hardware after each move.
	long yReal_nm = yPosition_nm; /// Returned from hardware after each move.
	string messageType = COLLECT_MONTAGE_IMAGES;
	string statusDestinationTopic = ReplaceTopicChannel( m_currentWorkDestination, TEXT_STATUS );

	PlateType pt = this->m_desHelper->ExtractPayloadPlateType( m_commandMessageInObj );

	int currentStatus = (succeeded) ? STATUS_OK : STATUS_DEVICE_ERROR;

	/// Move to the start position
	if (xyState == ComponentState::Good)
	{
		stringstream msg;
		msg << "startX=" << xReal_nm << "|startY=" << yReal_nm << "|vel=" << velocity << "acc=" << acceleration << "|dec=" << deceleration;
		LOG_TRACE( m_threadName, "xy", msg.str().c_str() );
		currentStatus = Implement_MoveTwoAxesConcurrently(	"X", xReal_nm, axisResolutionNm,
															"Y", yReal_nm, axisResolutionNm,
															velocity, acceleration, deceleration, stp );
	}
	else if (xyState == ComponentState::Mocked)
	{
		std::this_thread::sleep_for(200ms); // Fake work
	}
	else
	{
		LOG_ERROR( m_threadName, "xy", "xyStatus neither mocked nor good" );
	}
	m_states_long[X_NM] = xReal_nm;
	m_states_long[Y_NM] = yReal_nm;
	if (false)
	{
		stringstream msg;
		msg << "(Move to start position) X_nm=" << xReal_nm << " Y_nm=" << yReal_nm;
		LOG_TRACE( m_threadName, "xy", msg.str().c_str() );
	}

	if (currentStatus == STATUS_MOVEMENT_FAILED)
	{
		/// Send an alert.
		stringstream topic_stream;
		topic_stream << TOPIC_ES << CHANNEL_ALERT;

		/// Collect fields from the command's header for use in the alert.
		std::string messageType = "";
		int commandGroupId = 0;
		int commandId = 0;
		std::string sessionId = "";
		std::string machineId = "";
		bool succeeded = m_desHelper->SplitCommand( m_commandMessageInObj,
													&messageType,
													nullptr, // &transportVersion,
													&commandGroupId,
													&commandId,
													&sessionId,
													&machineId );

		json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
															commandGroupId,
															commandId,
															sessionId, // Or topic_stream.str()?
															ComponentType::XYAxes,
															STATUS_AXIS_X_FAULTED,
															false ); /// thisIsCausingHold
		bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
	}
	
	/// Pull out the montage speeds and accelerations and switch to using those.
	/// (Overwriting the velocity, and other variables that we used for the first move.)
	succeeded &= m_desHelper->ExtractMontageSpeeds(	m_threadName,
													m_commandMessageInObj,
													"xy",
													velocity,
													acceleration,
													deceleration,
													stp );
	
	/// Set Jerk Parameter (S-curve)
	/// Note that this approach differs from how we set speed and acclerations. We are setting JRK
	/// once at the start of montage and restoring it at the end.
	/// It is tempting to do the same for ACC, DEC, STP, and VEL. But we won't. It's better start doing that
	/// in the future, when we switch to using simple X and Y moves. At that time, we may start using
	/// different speeds, accels, and jerks for X and Y. So they will have to be sent to the controller
	/// at different intervals throughout the montage.
	if (xyState != ComponentState::Mocked)
	{
		try {
			ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllerOfAxis( "X" );
			/// Convert values to millimeters.
			double accel = (double)acceleration/_1MEG_F;
			double vel = (double)velocity/_1MEG_F;
			pController->setJrk( (float)(accel * accel / vel) );
			usleep(10000);
		} catch (...) {
			currentStatus = STATUS_MOVEMENT_FAILED;
			LOG_ERROR( m_threadName, "xy", "unknown EXCEPTION while setting JRK!" );
		}
	}

	/// Sleep after each move to reduce sloshing.
	if (xyState == ComponentState::Mocked)
	{
		stringstream msg;
		msg << "Skipping scan-movement delay of " << scanMovementDelayMs << " ms";
		LOG_TRACE( m_threadName, "xy", msg.str().c_str() );
	}
	else
	{
		stringstream msg;
		msg << "Starting scan-movement delay of " << scanMovementDelayMs << " ms";
		LOG_TRACE( m_threadName, "xy", msg.str().c_str() );
		usleep( scanMovementDelayMs * 1000L );
		LOG_TRACE( m_threadName, "xy", "End of scan-movement delay" );
	}

	/// Perform a sequence of small moves and picture-taking.
	for (unsigned long x = 0; x < X_stepCount; x++) // TODO: Spawn a thread to perform this loop.
	{
		for (unsigned long y = 0; y < Y_stepCount; y++)
		{
			if (currentStatus == STATUS_OK)
			{
				FreezeWorkInProgress();

				/// Take picture and upload it.
				json_object* payloadObj = json_object_new_object();
				long payloadY = (x % 2 == 0) ? y : (Y_stepCount -y -1);
				AddToPayloadXY( payloadObj, x, payloadY ); /// Report coordinates that match the snaking pattern.
				currentStatus = SendPictureInStatusMessage( payloadObj,
															pixel_count_x,
															pixel_count_y,
															bin_factor,
															scaled_color_depth,
															camera_color_depth,
															commandId,
															commandGroupId,
															messageType,
															sessionId,
															machineId,
															m_currentWorkOrigin,
															statusDestinationTopic );
				if (currentStatus == STATUS_OK)
				{
					cout << m_threadName << ": DoleOut_CollectMontageImages(" << x << "," << y << ") SNAP!" << " command ID=" << commandId << endl;
				}
				else
				{
					cout << m_threadName << ": ERROR!  DoleOut_CollectMontageImages(" << x << "," << y << ") failed a picture" << endl;
				}
				
				/// Calculate moves for the next iteration.
				/// This is a little cumbersome. In a 2x2 montage, we seek coordinates like (0,0), (0,dY), (dX,dY), (dX,0)
				if (y+1 < Y_stepCount) /// Don't change Y on the last iteration, because X will change *instead*.
				{
					if (x % 2 == 0)
					{
						yPosition_nm += Y_stepIncrementNm;
					}
					else
					{
						yPosition_nm -= Y_stepIncrementNm;
					}
				}
				else
				{
					xPosition_nm -= X_stepIncrementNm;
				}

				FreezeWorkInProgress();

				/// Move to current xPosition_nm, yPosition_nm coordinates.
				xReal_nm = xPosition_nm;
				yReal_nm = yPosition_nm;
				if (xyState == ComponentState::Good)
				{
					/*
					stringstream msg;
					msg << "Move from " << m_states_long[X_NM] << "," << m_states_long[Y_NM] << " to " << xReal_nm << "," << yReal_nm;
					LOG_TRACE( m_threadName, "xy", msg.str().c_str() );
					*/
					stringstream msg;
					msg << "newX=" << xReal_nm << "|newY=" << yReal_nm << "|vel=" << velocity << "acc=" << acceleration << "|dec=" << deceleration;
					LOG_TRACE( m_threadName, "xy", msg.str().c_str() );

					/// This will update x,y to the new values, which we will record in m_states_long.
					/// In case they accumulate small errors, we will NOT use these values to calculate
					/// the next positions. xPosition_nm and yPosition_nm will be used for that.
					currentStatus = Implement_MoveTwoAxesConcurrently(	"X", xReal_nm, axisResolutionNm,
																		"Y", yReal_nm, axisResolutionNm,
																		velocity, acceleration, deceleration, stp );
				}
				else if (xyState == ComponentState::Mocked)
				{
					std::this_thread::sleep_for(200ms); // Fake work
				}
				else
				{
					LOG_ERROR( m_threadName, "xy", "xyStatus neither mocked nor good" );
				}
				m_states_long[X_NM] = xReal_nm;
				m_states_long[Y_NM] = yReal_nm;
				{
					stringstream msg;
					msg << "X_nm=" << xReal_nm << " Y_nm=" << yReal_nm;
					LOG_TRACE( m_threadName, "xy", msg.str().c_str() );
				}
			}
			/// Sleep after each move to reduce sloshing.
			if (xyState == ComponentState::Mocked)
			{
				stringstream msg;
				msg << "Skipping scan-movement delay of " << scanMovementDelayMs << " ms";
				LOG_TRACE( m_threadName, "xy", msg.str().c_str() );
			}
			else
			{
				stringstream msg;
				msg << "Starting scan-movement delay of " << scanMovementDelayMs << " ms";
				LOG_TRACE( m_threadName, "xy", msg.str().c_str() );
				usleep( scanMovementDelayMs * 1000L );
				LOG_TRACE( m_threadName, "xy", "End of scan-movement delay" );
			}
			if (g_signalReceived != 0)
			{
				x = X_stepCount; /// Break out of very long-running loop to allow shutdown.
				y = Y_stepCount;
			}
		}
	}

	if (currentStatus == STATUS_MOVEMENT_FAILED)
	{
		/// Send an alert.
		stringstream topic_stream;
		topic_stream << TOPIC_ES << CHANNEL_ALERT;

		/// Collect fields from the command's header for use in the alert.
		std::string messageType = "";
		int commandGroupId = 0;
		int commandId = 0;
		std::string sessionId = "";
		std::string machineId = "";
		bool succeeded = m_desHelper->SplitCommand( m_commandMessageInObj,
													&messageType,
													nullptr, // &transportVersion,
													&commandGroupId,
													&commandId,
													&sessionId,
													&machineId );

		json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
															commandGroupId,
															commandId,
															sessionId, // Or topic_stream.str()?
															ComponentType::XYAxes,
															STATUS_AXIS_X_FAULTED,
															false ); /// thisIsCausingHold
		bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
	}
	if (xyState != ComponentState::Mocked)
	{
		try {
			ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllerOfAxis( "X" );
			pController->setJrk( 0.0 );
			usleep(10000);
		} catch (...) {
			currentStatus = STATUS_MOVEMENT_FAILED;
			LOG_ERROR( m_threadName, "xy", "unknown EXCEPTION while restoring JRK!" );
		}
	}
	return currentStatus;
}

///----------------------------------------------------------------------
/// DoleOut_AddPlateToDeckInventory
///	Add plate location details to the plate inventory.
/// If a plate_id is found in inventory, ...?
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_AddPlateToDeckInventory()
{
	int status = STATUS_OK;
	/// Get the payload object.
	json_object * payloadObj = nullptr;
	if (!json_object_object_get_ex( m_commandMessageInObj, PAYLOAD, &payloadObj ))
	{
		LOG_ERROR( m_threadName, "plates", "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}
	/// Get the plate_id
	string plateId = "";
	if (status == STATUS_OK)
	{
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, PLATE_ID, plateId ))
		{
			LOG_ERROR( m_threadName, "plates", "Plate ID is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
	}
	/// Check for plate_id already in inventory.
	if (status == STATUS_OK)
	{
		bool found = false;
		string temp = m_plateInventory->GetLocation( plateId );
		found |= (temp.length() != 0);
		temp = m_plateInventory->GetSubLocation( plateId );
		found |= (temp.length() != 0);
		temp = m_plateInventory->GetSubSubLocation( plateId );
		found |= (temp.length() != 0);
		if (found)
		{
			LOG_ERROR( m_threadName, "plates", "Barcode already in inventory" );
			status = STATUS_BARCODE_ALREADY_EXISTS_IN_INVENTORY;
		}
	}
	/// Get the location information from the payload. (Plates don't deal with counts.)
	string primaryLocation = "";
	string subLocation = "";
	string subSubLocation = "";
	if (status == STATUS_OK)
	{
		/// For plates, always expect ID, Primary and Sub-locations.
//		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, PLATE_ID, plateId ))
//		{
//			LOG_ERROR( m_threadName, "plates", "Plate ID is missing" );
//			status = STATUS_CMD_MSG_DATA_MISSING;
//		}
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, PRIMARY_LOCATION, primaryLocation ))
		{
			LOG_ERROR( m_threadName, "plates", "Primary location is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, SUB_LOCATION, subLocation ))
		{
			LOG_ERROR( m_threadName, "plates", "Sublocation is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		/// For plates, expect sub-sub-locations to be either a value of 1+ or empty.
		json_object * obj = nullptr;
		if (json_object_object_get_ex( payloadObj, SUB_SUB_LOCATION, &obj ))
		{
			subSubLocation = json_object_get_string( obj );
		}
		else
		{
			LOG_WARN( m_threadName, "plates", "sub_sub_location key is missing" );
		}
		if (subSubLocation.length() == 0)
		{
			subSubLocation = SUB_SUB_NULL; /// Assign a value that means "empty".
		}
	}
	/// Add the location information to inventory.
	if ( status == STATUS_OK )
	{
		bool succeeded = ( m_plateInventory->SetLocation(plateId, primaryLocation) &&
						   m_plateInventory->SetSubLocation(plateId, subLocation) &&
						   m_plateInventory->SetSubSubLocation(plateId, subSubLocation) );
		if (!succeeded)
		{
			LOG_ERROR( m_threadName, "plates", "Failed to update RAM" );
			status = STATUS_CMD_MSG_ERROR;
		}
	}
	/// Write the modified inventory to disk.
	if ( status == STATUS_OK )
	{
		if ( m_plateInventory->StoreToFiles( PLATES_DIR ))
		{
			LOG_TRACE( m_threadName, "plates", "Saved" );
		}
		else
		{
			LOG_ERROR( m_threadName, "plates", "Failed to write to file" );
			status = STATUS_DEVICE_ERROR;
		}
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_RemovePlateFromDeckInventory
/// If a plate_id is found in inventory, delete its old locations.
/// If plate_id is not found in the inventory, return STATUS_BARCODE_NOT_FOUND.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_RemovePlateFromDeckInventory()
{
	int status = STATUS_OK;
	/// Get the payload object.
	json_object * payloadObj = nullptr;
	if (!json_object_object_get_ex( m_commandMessageInObj, PAYLOAD, &payloadObj ))
	{
		LOG_ERROR( m_threadName, "plates", "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string(payloadObj);
		cout << payloadStr << endl;
	}
	/// Get the plate_id from the payload.
	string plateId = "";
	if (status == STATUS_OK)
	{
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, PLATE_ID, plateId ))
		{
			LOG_ERROR( m_threadName, "plates", "Plate ID is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
	}
	/// Attempt to delete plate_id from inventory. (Plates don't deal with counts.)
	if (status == STATUS_OK)
	{
		bool succeeded = ( m_plateInventory->DeleteLocation(plateId) &&
						   m_plateInventory->DeleteSubLocation(plateId) &&
						   m_plateInventory->DeleteSubSubLocation(plateId) );
		if (!succeeded)
		{
			LOG_ERROR( m_threadName, "plates", "Barcode not found" );
			status = STATUS_BARCODE_NOT_FOUND;
		}
	}
	/// Write the modified inventory to disk.
	if (status == STATUS_OK)
	{
		if (!m_plateInventory->StoreToFiles( PLATES_DIR ))
		{
			LOG_ERROR( m_threadName, "plates", "Failed to write to file" );
			status = STATUS_DEVICE_ERROR;
		}
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_MovePlateInDeckInventory
/// If a plate_id is found in inventory, delete its old locations and
/// add its new locations.
/// If any error happens, try to leave the data unchanged.
/// If a given plate_id is not already in the inventory, return
/// STATUS_BARCODE_NOT_FOUND.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MovePlateInDeckInventory()
{
	int status = STATUS_OK;
	/// Get the payload object.
	json_object * payloadObj = nullptr;
	if (!json_object_object_get_ex( m_commandMessageInObj, PAYLOAD, &payloadObj ))
	{
		LOG_ERROR( m_threadName, "plates", "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string(payloadObj);
		cout << payloadStr << endl;
	}
	/// Get the plate_id and location info from the payload.
	string plateId = "";
	string primaryLocation = "";
	string subLocation = "";
	string subSubLocation = "";
	if (status == STATUS_OK)
	{
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, PLATE_ID, plateId ))
		{
			LOG_ERROR( m_threadName, "plates", "Plate ID is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, PRIMARY_LOCATION, primaryLocation ))
		{
			LOG_ERROR( m_threadName, "plates", "Primary location is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, SUB_LOCATION, subLocation ))
		{
			LOG_ERROR( m_threadName, "plates", "Sublocation is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		/// For plates, expect sub-sub-locations to be either a value of "1+" or empty.
		json_object * obj = nullptr;
		if (json_object_object_get_ex( payloadObj, SUB_SUB_LOCATION, &obj ))
		{
			subSubLocation = json_object_get_string( obj );
		}
		else
		{
			LOG_WARN( m_threadName, "plates", "sub_sub_location key is missing" );
		}
		if (subSubLocation.length() == 0)
		{
			subSubLocation = SUB_SUB_NULL; /// Assign a value that means "empty".
		}
	}
	/// Delete the old locations from inventory. (Plates don't deal with counts.)
	if (status == STATUS_OK)
	{
		bool succeeded = ( m_plateInventory->DeleteLocation(plateId) &&
						   m_plateInventory->DeleteSubLocation(plateId) &&
						   m_plateInventory->DeleteSubSubLocation(plateId) );
		if (!succeeded)
		{
			LOG_WARN( m_threadName, "plates", "Barcode not found" );
			status = STATUS_BARCODE_NOT_FOUND;
		}
	}
	if (status != STATUS_OK)
	{
		return status;		/// EARLY RETURN!	EARLY RETURN!
	}

	/// Add the new locations to inventory. (Plates don't deal with counts.)
	if ( status == STATUS_OK )
	{
		/// ADD data to the inventory.
		bool succeeded = ( m_plateInventory->SetLocation(plateId, primaryLocation) &&
						   m_plateInventory->SetSubLocation(plateId, subLocation) &&
						   m_plateInventory->SetSubSubLocation(plateId, subSubLocation) );
		if (!succeeded)
		{
			LOG_ERROR( m_threadName, "plates", "Failed to update RAM" );
			status = STATUS_CMD_MSG_ERROR;
		}
	}
	/// Serialize the fle file as atomically as we can.
	if ( status == STATUS_OK )
	{
		/// Save to file.
		if (m_plateInventory->StoreToFiles( PLATES_DIR ))
		{
			LOG_TRACE( m_threadName, "plates", "Saved" );
		}
		else
		{
			LOG_ERROR( m_threadName, "plates", "Failed to write to file" );
			status = STATUS_DEVICE_ERROR;
		}
	}
	if ( status != STATUS_OK )
	{
		/// Restore from file to recover from any partial changes.
		m_plateInventory->ReadFromFiles( PLATES_DIR );
		LOG_WARN( m_threadName, "plates", "Change failed|Rolled back OK" );
		status = STATUS_DEVICE_ERROR;
	}
	return status;
}


/// Retrieve a list of plate IDs and their full locations.
/// The returned list is a subset of the full plate inventory.
/// The command arrives with 3 input parameters: primary_location,
/// sub_location, and sub_sub_location. These parameters set boundaries
/// on what plate IDs to included in the status.
/// When an input is set to a non-null value, it restricts the output list to only those IDs
/// that match its value.
/// When an input value is null, no restriction is applied.
int CommandDolerThread::DoleOut_GetPlateListInLocation()
{
	int status = STATUS_OK;
	/// Get the payload object.
	json_object * payloadObj = nullptr;
	if (!json_object_object_get_ex( m_commandMessageInObj, PAYLOAD, &payloadObj ))
	{
		LOG_ERROR( m_threadName, "plates", "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}
	/// Extract the input parameters.
	string primaryLocation = "";
	string subLocation = "";
	string subSubLocation = "";
	LOG_TRACE( m_threadName, "plates", "-" );
	if (status == STATUS_OK)
	{
		/// Get primary location as a raw string.
		json_object * primeLocObj = json_object_object_get( payloadObj, PRIMARY_LOCATION );
		if (primeLocObj)
		{
			primaryLocation = json_object_get_string( primeLocObj );
		}
		else
		{
			LOG_ERROR( m_threadName, "plates", "Primary location is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		/// Get subLocation as a raw string.
		json_object * subLocObj = json_object_object_get( payloadObj, SUB_LOCATION );
		if (subLocObj)
		{
			subLocation = json_object_get_string( subLocObj );
		}
		else
		{
			LOG_ERROR( m_threadName, "plates", "Sublocation is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		/// Get subSubLocation as a raw string.
		json_object * subSubLocObj = json_object_object_get( payloadObj, SUB_SUB_LOCATION );
		if (subSubLocObj)
		{
			subSubLocation = json_object_get_string( subSubLocObj );
		}
		else
		{
			LOG_ERROR( m_threadName, "plates", "Sublocation is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
	}
	if (status != STATUS_OK)
	{
		return status;		/// EARLY RETURN!	EARLY RETURN!
	}

	if (status == STATUS_OK)
	{
		/// Do the searching and filtering that defines the list.
		list<string> plateIds = m_plateInventory->GetFilteredIdList( primaryLocation, subLocation, subSubLocation );

		/// For each ID in the list, pack it and its locations into a JSON list.
		json_object * arrayObj = json_object_new_array();
		int arrayInitialSize = json_object_array_length( arrayObj );
		{
			stringstream msg;
			msg << "JSON array length starts at " << arrayInitialSize;
			LOG_TRACE( m_threadName, "plates", msg.str().c_str() );
		}
		int arrayIdx = 0;
		for (list<string>::iterator it = plateIds.begin(); it != plateIds.end(); ++it)
		{
			json_object * plateObj = json_object_new_object();
			string plateId = *it;
			int result = json_object_object_add( plateObj, ID, json_object_new_string( plateId.c_str() ) );
			if (result != 0)
			{
				stringstream msg;
				msg << "Failed to add plate ID to array at index=" << arrayIdx;
				LOG_ERROR( m_threadName, "plates", msg.str().c_str() );
				status = STATUS_DEVICE_ERROR;
			}
			string location = m_plateInventory->GetLocation( plateId );
			result = json_object_object_add( plateObj, PRIMARY_LOCATION, json_object_new_string( location.c_str() ) );
			if (result != 0)
			{
				stringstream msg;
				msg << "Failed to add primary location to array at index=" << arrayIdx;
				LOG_ERROR( m_threadName, "plates", msg.str().c_str() );
				status = STATUS_DEVICE_ERROR;
			}
			string subLocation = m_plateInventory->GetSubLocation( plateId );
			result = json_object_object_add( plateObj, SUB_LOCATION, json_object_new_string( subLocation.c_str() ) );
			if (result != 0)
			{
				stringstream msg;
				msg << "Failed to add sub_location to array at index=" << arrayIdx;
				LOG_ERROR( m_threadName, "plates", msg.str().c_str() );
				status = STATUS_DEVICE_ERROR;
			}
			string subSubLocation = m_plateInventory->GetSubSubLocation( plateId );
			if (subSubLocation.compare( SUB_SUB_NULL ) == 0)
			{
				/// Replace the internal representation of null with the external value "".
				subSubLocation = "";
			}
			result = json_object_object_add( plateObj, SUB_SUB_LOCATION, json_object_new_string( subSubLocation.c_str() ) );
			if (result != 0)
			{
				stringstream msg;
				msg << "Failed to add sub_sub_location to array at index=" << arrayIdx;
				LOG_ERROR( m_threadName, "plates", msg.str().c_str() );
				status = STATUS_DEVICE_ERROR;
			}
			
			if (arrayIdx < arrayInitialSize)
			{
				json_object_array_put_idx(arrayObj, arrayIdx, plateObj);
				arrayIdx++;
			}
			else
			{
				json_object_array_add(arrayObj, plateObj);
				arrayIdx++;
			}
		}
		int result = json_object_object_add( m_statusPayloadOutObj, PLATE_LIST, arrayObj );
		if (result != 0)
		{
			LOG_ERROR( m_threadName, "plates", "Failed to plate_list to payload" );
			status = STATUS_DEVICE_ERROR;
		}
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_FindPlateLocationInInventory
/// Returns the location of the plate specified by its plate_id
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_FindPlateLocationInInventory()
{
	int status = STATUS_OK;
	/// Get payload object
	json_object * payloadObj = nullptr;
	if (!json_object_object_get_ex( m_commandMessageInObj, PAYLOAD, &payloadObj ))
	{
		LOG_ERROR( m_threadName, "plates", "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}
	/// Get plate_id from payload.
	string plateId = "";
	if (status == STATUS_OK)
	{
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, PLATE_ID, plateId ))
		{
			LOG_ERROR( m_threadName, "plates", "Plate ID is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
	}
	/// Attempt to get location info from the inventory. (Plates don't deal with counts.)
	/// If not all location parts are found, issue a STATUS_BARCODE_NOT_FOUND error.
	string primaryLocation = "";
	string subLocation = "";
	string subSubLocation = "";
	if (status == STATUS_OK)
	{
		primaryLocation = m_plateInventory->GetLocation(plateId);
		subLocation = m_plateInventory->GetSubLocation(plateId);
		subSubLocation = m_plateInventory->GetSubSubLocation(plateId);
		if ((primaryLocation.length() == 0) ||
			 (subLocation.length() == 0) ||
			 (subSubLocation.length() == 0))
		{
			LOG_WARN( m_threadName, "plates", "Barcode not found." );
			status = STATUS_BARCODE_NOT_FOUND;
		}
	}
	/// Pack the location parts into the payload.
	if (status == STATUS_OK)
	{
		int result = json_object_object_add( m_statusPayloadOutObj, PRIMARY_LOCATION, json_object_new_string( primaryLocation.c_str() ) );
		if (result != 0)
		{
			LOG_ERROR( m_threadName, "plates", "Failed to add primary_location to status message." );
			status = STATUS_CMD_MSG_ERROR;
		}

		result = json_object_object_add( m_statusPayloadOutObj, SUB_LOCATION, json_object_new_string( subLocation.c_str() ) );
		if (result != 0)
		{
			LOG_ERROR( m_threadName, "plates", "Failed to add sub_location to status message." );
			status = STATUS_CMD_MSG_ERROR;
		}

		if (subSubLocation.compare( SUB_SUB_NULL ) == 0)
		{
			/// Replace the internal representation of null with the external value "".
			subSubLocation = "";
		}
		result = json_object_object_add( m_statusPayloadOutObj, SUB_SUB_LOCATION, json_object_new_string( subSubLocation.c_str() ) );
		if (result != 0)
		{
			LOG_ERROR( m_threadName, "plates", "Failed to add sub_sub_location to status message." );
			status = STATUS_CMD_MSG_ERROR;
		}
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_AddTipRackToDeckInventory
///	Add tip rack location details to the tip inventory.
/// If a tip_rack_id is found in inventory, ...?
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_AddTipRackToDeckInventory()
{
	int status = STATUS_OK;
	char axisName[] = "tips";
	/// Get the payload object.
	json_object * payloadObj = nullptr;
	if (!json_object_object_get_ex( m_commandMessageInObj, PAYLOAD, &payloadObj ))
	{
		LOG_ERROR( m_threadName, axisName, "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}
	/// Get the tip_rack_id
	string id = "";
	if (status == STATUS_OK)
	{
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, TIP_RACK_ID, id ))
		{
			LOG_ERROR( m_threadName, axisName, "ID is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
	}
	/// Check for tip_rack_id already in inventory.
	if (status == STATUS_OK)
	{
		bool found = false;
		string temp = m_tipInventory->GetLocation( id );
		cout << "temp1=" << temp << endl;
		found |= (temp.length() != 0);
		temp = m_tipInventory->GetSubLocation( id );
		cout << "temp2=" << temp << endl;
		found |= (temp.length() != 0);
		temp = m_tipInventory->GetSubSubLocation( id );
		cout << "temp3=" << temp << endl;
		found |= (temp.length() != 0);
		int unitCount = m_tipInventory->GetUnitCount( id );
		// TODO: Make functions that explicitly test the presense of values.
		if (found)
		{
			stringstream msg;
			msg << "barcode=" << id << "|already in inventory|";
			LOG_ERROR( m_threadName, axisName, msg.str().c_str() );
			status = STATUS_BARCODE_ALREADY_EXISTS_IN_INVENTORY;
		}
	}
	/// Get the location information from the payload.
	string primaryLocation = "";
	string subLocation = "";
	string subSubLocation = "";
	long usedTipCount = 0L;
	if (status == STATUS_OK)
	{
		/// For tips, always expect ID, Primary, Sub-locations, and count.
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, PRIMARY_LOCATION, primaryLocation ))
		{
			LOG_ERROR( m_threadName, axisName, "Primary location is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, SUB_LOCATION, subLocation ))
		{
			LOG_ERROR( m_threadName, axisName, "Sublocation is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		/// For plates, expect sub-sub-locations to be either a value of 1+ or empty.
		json_object * obj = nullptr;
		if (json_object_object_get_ex( payloadObj, SUB_SUB_LOCATION, &obj ))
		{
			subSubLocation = json_object_get_string( obj );
		}
		else
		{
			LOG_WARN( m_threadName, axisName, "sub_sub_location key is missing" );
		}
		if (subSubLocation.length() == 0)
		{
			subSubLocation = SUB_SUB_NULL; /// Assign a value that means "empty".
		}
		if (! m_desHelper->ExtractLongValue( m_threadName, payloadObj, USED_TIP_COUNT, usedTipCount ))
		{
			LOG_ERROR( m_threadName, axisName, "Sublocation is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
	}
	/// Add the location information to inventory.
	if ( status == STATUS_OK )
	{
		bool succeeded = ( m_tipInventory->SetLocation(id, primaryLocation) &&
						   m_tipInventory->SetSubLocation(id, subLocation) &&
						   m_tipInventory->SetSubSubLocation(id, subSubLocation) &&
						   m_tipInventory->SetUnitCount(id, usedTipCount ) );
		if (!succeeded)
		{
			LOG_ERROR( m_threadName, axisName, "Failed to update RAM" );
			status = STATUS_CMD_MSG_ERROR;
		}
	}
	/// Write the modified inventory to disk.
	if ( status == STATUS_OK )
	{
		if ( m_tipInventory->StoreToFiles( TIPS_DIR ))
		{
			LOG_TRACE( m_threadName, axisName, "Saved" );
		}
		else
		{
			LOG_ERROR( m_threadName, axisName, "Failed to write to file" );
			status = STATUS_DEVICE_ERROR;
		}
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_RemoveTipRackFromDeckInventory
/// If a tip_rack_id is found in inventory, delete its old locations.
/// If tip_rack_id is not found in the inventory, return STATUS_BARCODE_NOT_FOUND.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_RemoveTipRackFromDeckInventory()
{
	int status = STATUS_OK;
	char axisName[] = "tips";
	/// Get the payload object.
	json_object * payloadObj = nullptr;
	if (!json_object_object_get_ex( m_commandMessageInObj, PAYLOAD, &payloadObj ))
	{
		LOG_ERROR( m_threadName, axisName, "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}
	/// Get the id string from the payload.
	string id = "";
	if (status == STATUS_OK)
	{
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, TIP_RACK_ID, id ))
		{
			LOG_ERROR( m_threadName, axisName, "ID is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
	}
	/// Attempt to delete plate_id from inventory. (Plates don't deal with counts.)
	if (status == STATUS_OK)
	{
		bool succeeded = ( m_tipInventory->DeleteLocation( id ) &&
						   m_tipInventory->DeleteSubLocation( id ) &&
						   m_tipInventory->DeleteSubSubLocation( id ) &&
						   m_tipInventory->DeleteUnitCount( id ) );
		if (!succeeded)
		{
			LOG_ERROR( m_threadName, axisName, "Barcode not found" );
			status = STATUS_BARCODE_NOT_FOUND;
		}
	}
	/// Write the modified inventory to disk.
	if (status == STATUS_OK)
	{
		if (!m_tipInventory->StoreToFiles( TIPS_DIR ))
		{
			LOG_ERROR( m_threadName, axisName, "Failed to write to file" );
			status = STATUS_DEVICE_ERROR;
		}
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_MoveTipRackInDeckInventory
/// Logically changes the specified tip rack’s location within the deck inventory
///
/// The ES locates the current location for tip_rack_id, removes it from there,
/// and adds it to the specified location.
///
/// If a given tip_rack_id is not already in the inventory, return
/// STATUS_BARCODE_NOT_FOUND.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveTipRackInDeckInventory()
{
	int status = STATUS_OK;
	char axisName[] = "tips";
	/// Get the payload object.
	json_object * payloadObj = nullptr;
	if (!json_object_object_get_ex( m_commandMessageInObj, PAYLOAD, &payloadObj ))
	{
		LOG_ERROR( m_threadName, axisName, "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}
	/// Get the tip_rack_id and location info from the payload.
	string id = "";
	string primaryLocation = "";
	string subLocation = "";
	string subSubLocation = "";
	if (status == STATUS_OK)
	{
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, TIP_RACK_ID, id ))
		{
			LOG_ERROR( m_threadName, axisName, "ID is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, PRIMARY_LOCATION, primaryLocation ))
		{
			LOG_ERROR( m_threadName, axisName, "Primary location is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, SUB_LOCATION, subLocation ))
		{
			LOG_ERROR( m_threadName, axisName, "Sublocation is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		/// For tips, expect sub-sub-locations to be either a value of "1+" or empty.
		json_object * obj = nullptr;
		if (json_object_object_get_ex( payloadObj, SUB_SUB_LOCATION, &obj ))
		{
			subSubLocation = json_object_get_string( obj );
		}
		else
		{
			LOG_WARN( m_threadName, axisName, "sub_sub_location key is missing" );
		}
		if (subSubLocation.length() == 0)
		{
			subSubLocation = SUB_SUB_NULL; /// Assign a value that means "empty".
		}
	}
	/// Delete the old locations from inventory. No need to affect the
	/// unit count because that won't need to change.
	if (status == STATUS_OK)
	{
		bool succeeded = ( m_tipInventory->DeleteLocation(id) &&
						   m_tipInventory->DeleteSubLocation(id) &&
						   m_tipInventory->DeleteSubSubLocation(id) );
		if (!succeeded)
		{
			LOG_ERROR( m_threadName, axisName, "Barcode not found" );
			status = STATUS_BARCODE_NOT_FOUND;
		}
	}
	if (status != STATUS_OK)
	{
		return status;		/// EARLY RETURN!	EARLY RETURN!
	}

	/// Add the new locations to inventory. No need to affect the
	/// unit count because that won't need to change.
	if ( status == STATUS_OK )
	{
		/// ADD data to the inventory.
		bool succeeded = ( m_tipInventory->SetLocation(id, primaryLocation) &&
						   m_tipInventory->SetSubLocation(id, subLocation) &&
						   m_tipInventory->SetSubSubLocation(id, subSubLocation) );
		if (!succeeded)
		{
			LOG_ERROR( m_threadName, axisName, "Failed to update RAM" );
			status = STATUS_CMD_MSG_ERROR;
		}
	}
	/// Serialize the fle file as atomically as we can.
	if ( status == STATUS_OK )
	{
		/// Save to file.
		if (m_tipInventory->StoreToFiles( TIPS_DIR ))
		{
			LOG_TRACE( m_threadName, axisName, "Saved" );
		}
		else
		{
			LOG_ERROR( m_threadName, axisName, "Failed to write to file" );
			status = STATUS_DEVICE_ERROR;
		}
	}
	if (status != STATUS_OK)
	{
		/// Restore from file to recover from any partial changes.
		m_tipInventory->ReadFromFiles( TIPS_DIR );
		LOG_WARN( m_threadName, axisName, "Change failed|Rolled back OK" );
		status = STATUS_DEVICE_ERROR;
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_GetTipRackListInLocation
/// Retrieves a list of tip rack ids, used tip counts, and their full locations in the specified location.
///
/// If the primary location, sub location, or sub sub location are empty, that element matches all values.
///
/// Example 1, If primary location = active, sub location = 0, and sub sub location = "" the return list
/// contains the tip rack id, type, used tip count, and full location in the active tip rack rack 0.
///
/// Example 2, If primary location = extra, sub location = "", and sub sub location = "" the return list
/// contains the tip rack ids, types, used tip counts, and full locations in all extra tip rack racks.
///
/// Example 3, If primary location, sub location, and sub sub location are all empty, the return list contains
/// all tip rack on the deck including the tip rack in the active tip rack and all tip rack in the extra tip racks.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_GetTipRackListInLocation()
{
	int status = STATUS_OK;
	char axisName[] = "tips";
	/// Get the payload object.
	json_object * payloadObj = nullptr;
	if (!json_object_object_get_ex( m_commandMessageInObj, PAYLOAD, &payloadObj ))
	{
		LOG_ERROR( m_threadName, axisName, "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}
	/// Extract the input parameters.
	string primaryLocation = "";
	string subLocation = "";
	string subSubLocation = "";
	LOG_TRACE( m_threadName, axisName, "-" );
	if (status == STATUS_OK)
	{
		/// Get primary location as a raw string.
		json_object * primeLocObj = json_object_object_get( payloadObj, PRIMARY_LOCATION );
		if (primeLocObj)
		{
			primaryLocation = json_object_get_string( primeLocObj );
		}
		else
		{
			LOG_ERROR( m_threadName, axisName, "Primary location is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		/// Get subLocation as a raw string.
		json_object * subLocObj = json_object_object_get( payloadObj, SUB_LOCATION );
		if (subLocObj)
		{
			subLocation = json_object_get_string( subLocObj );
		}
		else
		{
			LOG_ERROR( m_threadName, axisName, "Sublocation is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		/// Get subSubLocation as a raw string.
		json_object * subSubLocObj = json_object_object_get( payloadObj, SUB_SUB_LOCATION );
		if (subSubLocObj)
		{
			subSubLocation = json_object_get_string( subSubLocObj );
		}
		else
		{
			LOG_ERROR( m_threadName, axisName, "Sublocation is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
	}
	if (status != STATUS_OK)
	{
		return status;		/// EARLY RETURN!	EARLY RETURN!
	}

	if (status == STATUS_OK)
	{
		/// Do the searching and filtering that defines the list.
		list<string> idList = m_tipInventory->GetFilteredIdList( primaryLocation, subLocation, subSubLocation );

		/// For each ID in the list, pack it and its locations into a JSON list.
		json_object * arrayObj = json_object_new_array();
		int arrayInitialSize = json_object_array_length( arrayObj );
		{
			stringstream msg;
			msg << "JSON array length starts at " << arrayInitialSize;
			LOG_TRACE( m_threadName, axisName, msg.str().c_str());
		}
		int arrayIdx = 0;
		for (list<string>::iterator it = idList.begin(); it != idList.end(); ++it)
		{
			json_object * tipObj = json_object_new_object();
			string id = *it;

			int result = json_object_object_add( tipObj, TIP_RACK_ID, json_object_new_string( id.c_str() ) );
			if (result != 0)
			{
				stringstream msg;
				msg << "failed to add ID to array at index " << arrayIdx;
				LOG_ERROR( m_threadName, axisName, msg.str().c_str());
				status = STATUS_DEVICE_ERROR;
			}

			int used_tip_count = m_tipInventory->GetUnitCount( id );
			result = json_object_object_add( tipObj, USED_TIP_COUNT, json_object_new_int( used_tip_count ) );
			if (result != 0)
			{
				stringstream msg;
				msg << "failed to add " << USED_TIP_COUNT << " to array at index " << arrayIdx;
				LOG_ERROR( m_threadName, axisName, msg.str().c_str());
				status = STATUS_DEVICE_ERROR;
			}

			string location = m_tipInventory->GetLocation( id );
			result = json_object_object_add( tipObj, PRIMARY_LOCATION, json_object_new_string( location.c_str() ) );
			if (result != 0)
			{
				stringstream msg;
				msg << "failed to add " << PRIMARY_LOCATION << " to array at index " << arrayIdx;
				LOG_ERROR( m_threadName, axisName, msg.str().c_str());
				status = STATUS_DEVICE_ERROR;
			}

			string subLocation = m_tipInventory->GetSubLocation( id );
			result = json_object_object_add( tipObj, SUB_LOCATION, json_object_new_string( subLocation.c_str() ) );
			if (result != 0)
			{
				stringstream msg;
				msg << "failed to add " << SUB_LOCATION << " to array at index " << arrayIdx;
				LOG_ERROR( m_threadName, axisName, msg.str().c_str());
				status = STATUS_DEVICE_ERROR;
			}
			string subSubLocation = m_tipInventory->GetSubSubLocation( id );
			if (subSubLocation.compare( SUB_SUB_NULL ) == 0)
			{
				/// Replace the internal representation of null with the external value "".
				subSubLocation = "";
			}
			result = json_object_object_add( tipObj, SUB_SUB_LOCATION, json_object_new_string( subSubLocation.c_str() ) );
			if (result != 0)
			{
				stringstream msg;
				msg << "failed to add " << SUB_SUB_LOCATION << " to array at index " << arrayIdx;
				LOG_ERROR( m_threadName, axisName, msg.str().c_str());
				status = STATUS_DEVICE_ERROR;
			}
			
			if (arrayIdx < arrayInitialSize)
			{
				json_object_array_put_idx(arrayObj, arrayIdx, tipObj);
				arrayIdx++;
			}
			else
			{
				json_object_array_add(arrayObj, tipObj);
				arrayIdx++;
			}
		}
		int result = json_object_object_add( m_statusPayloadOutObj, TIP_RACK_LIST, arrayObj );
		if (result != 0)
		{
			LOG_ERROR( m_threadName, axisName, "Failed to list to payload" );
			status = STATUS_DEVICE_ERROR;
		}
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_FindTipRackLocationInInventory
/// Returns the location of the specified tip rack.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_FindTipRackLocationInInventory()
{
	int status = STATUS_OK;
	char axisName[] = "tips";
	/// Get payload object
	json_object * payloadObj = nullptr;
	if (!json_object_object_get_ex( m_commandMessageInObj, PAYLOAD, &payloadObj ))
	{
		LOG_ERROR( m_threadName, axisName, "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}
	/// Get id from payload.
	string id = "";
	if (status == STATUS_OK)
	{
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, TIP_RACK_ID, id ))
		{
			LOG_ERROR( m_threadName, axisName, "ID is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
	}
	/// Attempt to get location info from the inventory.
	/// If not all location parts are found, issue a STATUS_BARCODE_NOT_FOUND error.
	string primaryLocation = "";
	string subLocation = "";
	string subSubLocation = "";
	if (status == STATUS_OK)
	{
		primaryLocation = m_tipInventory->GetLocation( id );
		subLocation = m_tipInventory->GetSubLocation( id );
		subSubLocation = m_tipInventory->GetSubSubLocation( id );
		if ((primaryLocation.length() == 0) ||
			 (subLocation.length() == 0) ||
			 (subSubLocation.length() == 0))
		{
			LOG_WARN( m_threadName, axisName, "Barcode not found." );
			status = STATUS_BARCODE_NOT_FOUND;
		}
	}
	/// Pack the location parts into the payload.
	if (status == STATUS_OK)
	{
		int result = json_object_object_add( m_statusPayloadOutObj, PRIMARY_LOCATION, json_object_new_string( primaryLocation.c_str() ) );
		if (result != 0)
		{
			LOG_ERROR( m_threadName, axisName, "Failed to add primary_location to status message." );
			status = STATUS_CMD_MSG_ERROR;
		}

		result = json_object_object_add( m_statusPayloadOutObj, SUB_LOCATION, json_object_new_string( subLocation.c_str() ) );
		if (result != 0)
		{
			LOG_ERROR( m_threadName, axisName, "Failed to add sub_location to status message." );
			status = STATUS_CMD_MSG_ERROR;
		}

		if (subSubLocation.compare( SUB_SUB_NULL ) == 0)
		{
			/// Replace the internal representation of null with the external value "".
			subSubLocation = "";
		}
		result = json_object_object_add( m_statusPayloadOutObj, SUB_SUB_LOCATION, json_object_new_string( subSubLocation.c_str() ) );
		if (result != 0)
		{
			LOG_ERROR( m_threadName, axisName, "Failed to add sub_sub_location to status message." );
			status = STATUS_CMD_MSG_ERROR;
		}
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_GetTipCountForTipRack
/// Returns the used tip count of the specified tip rack.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_GetTipCountForTipRack()
{
	int status = STATUS_OK;
	char axisName[] = "tips";
	/// Get the payload object.
	json_object * payloadObj = nullptr;
	if (!json_object_object_get_ex( m_commandMessageInObj, PAYLOAD, &payloadObj ))
	{
		LOG_ERROR( m_threadName, axisName, "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}
	/// Get the id string from the payload.
	string id = "";
	if (status == STATUS_OK)
	{
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, TIP_RACK_ID, id ))
		{
			LOG_ERROR( m_threadName, axisName, "ID is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
	}
	/// Validate the id
	if (status == STATUS_OK)
	{
		string primaryLocation = "";
		string subLocation = "";
		string subSubLocation = "";
		primaryLocation = m_tipInventory->GetLocation( id );
		subLocation = m_tipInventory->GetSubLocation( id );
		subSubLocation = m_tipInventory->GetSubSubLocation( id );
		if ((primaryLocation.length() == 0) ||
			 (subLocation.length() == 0) ||
			 (subSubLocation.length() == 0))
		{
			LOG_WARN( m_threadName, axisName, "Barcode not found." );
			status = STATUS_BARCODE_NOT_FOUND;
		}
	}

	/// Pack tipCount into the status payload.
	if (status == STATUS_OK)
	{
		long tipCount = (long) m_tipInventory->GetUnitCount( id );
		int result = json_object_object_add( m_statusPayloadOutObj, USED_TIP_COUNT, json_object_new_int64( tipCount ) );
		if (result != 0)
		{
			LOG_ERROR( m_threadName, axisName, "Failed to add tip_count to payload" );
			status = STATUS_DEVICE_ERROR;
		}
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_SetTipCountForTipRack
/// Sets the tip count for the specified tip rack.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_SetTipCountForTipRack()
{
	int status = STATUS_OK;
	char axisName[] = "tips";
	/// Get the payload object.
	json_object * payloadObj = nullptr;
	if (!json_object_object_get_ex( m_commandMessageInObj, PAYLOAD, &payloadObj ))
	{
		LOG_ERROR( m_threadName, axisName, "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}
	/// Get the id and tip_count from the payload.
	string id = "";
	long tipCount = 0L;
	string plateId = "";
	if (status == STATUS_OK)
	{
		if (! m_desHelper->ExtractStringValue( m_threadName, payloadObj, TIP_RACK_ID, id ))
		{
			LOG_ERROR( m_threadName, axisName, "ID is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		if (! m_desHelper->ExtractLongValue( m_threadName, payloadObj, USED_TIP_COUNT, tipCount ))
		{
			LOG_ERROR( m_threadName, axisName, "Used tip count is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
	}
	/// Validate the id.
	if (status == STATUS_OK)
	{
		string primaryLocation = "";
		string subLocation = "";
		string subSubLocation = "";
		primaryLocation = m_tipInventory->GetLocation( id );
		subLocation = m_tipInventory->GetSubLocation( id );
		subSubLocation = m_tipInventory->GetSubSubLocation( id );
		if ((primaryLocation.length() == 0) ||
			 (subLocation.length() == 0) ||
			 (subSubLocation.length() == 0))
		{
			LOG_WARN( m_threadName, axisName, "Barcode not found." );
			status = STATUS_BARCODE_NOT_FOUND;
		}
	}
	/// Set the unit count in inventory.
	if (status == STATUS_OK)
	{
		if (!m_tipInventory->SetUnitCount( id, tipCount ))
		{
			LOG_ERROR( m_threadName, axisName, "Failed to update RAM" );
			status = STATUS_CMD_MSG_ERROR;
		}
	}
	/// Serialize the inventory to file.
	if (status == STATUS_OK)
	{
		if (!m_tipInventory->StoreToFiles( TIPS_DIR ))
		{
			LOG_ERROR( m_threadName, axisName, "Failed writing to files!" );
			status = STATUS_DEVICE_ERROR;
		}
	}
	return status;
}

int CommandDolerThread::DoleOut_GetAutoFocusImageStack( int commandId, int commandGroupId, std::string sessionId, std::string machineId )
{
	int status = STATUS_OK;
	if (m_threadName != DOLER_FOR_OPTICS)
	{
		cout << m_threadName << " : DoleOut_GetAutoFocusImageStack run on wrong thread. It should be on the OPTICS thread." << endl;
		return STATUS_DEVICE_ERROR;
	}
	unique_lock<mutex> lock( g_cameraMutex ); /// Please ensure this is done in every function that access the camera.

	/// Parse the payload to get 5 ulong values.
	unsigned long auto_focus_start_offset_nm = this->m_desHelper->ExtractPayloadLongValue( m_commandMessageInObj, AUTO_FOCUS_START_OFFSET );
	unsigned long auto_focus_image_count = this->m_desHelper->ExtractPayloadLongValue( m_commandMessageInObj, AUTO_FOCUS_IMAGE_COUNT );
	unsigned long auto_focus_increment_nm = this->m_desHelper->ExtractPayloadLongValue( m_commandMessageInObj, AUTO_FOCUS_INCREMENT );
	unsigned long pixel_count_x = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, PIXEL_COUNT_X );
	unsigned long pixel_count_y = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, PIXEL_COUNT_Y );
	unsigned long bin_factor = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, BIN_FACTOR );
	unsigned long camera_color_depth = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, CAMERA_COLOR_DEPTH );
	unsigned long scaled_color_depth = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, SCALED_COLOR_DEPTH );

	/// Move the optical column focus to auto_focus_start_offset_nm position
	unsigned long autoFocusPosition_nm = auto_focus_start_offset_nm;
	int currentStatus = Implement_moveToFocusNM( m_commandMessageInObj, autoFocusPosition_nm );
	if (currentStatus != STATUS_OK)
	{
		cout << m_threadName << ": ERROR!  DoleOut_GetAutoFocusImageStack() failed a picture" << endl;
	}
	
	/// Snap a series of pictures
	json_object* payloadObj = nullptr;
	string messageType = GET_AUTO_FOCUS_IMAGE_STACK;
	string statusDestinationTopic = ReplaceTopicChannel( m_currentWorkDestination, TEXT_STATUS );
	for (unsigned int i = 0; i < auto_focus_image_count; i++) // TODO: Spawn a thread to perform this loop.
	{
		FreezeWorkInProgress();
		
		currentStatus = SendPictureInStatusMessage( payloadObj,
													pixel_count_x,
													pixel_count_y,
													bin_factor,
													scaled_color_depth,
													camera_color_depth,
													commandId,
													commandGroupId,
													messageType,
													sessionId,
													machineId,
													m_currentWorkOrigin,
													statusDestinationTopic );
		if (currentStatus == STATUS_OK)
		{
			LOG_DEBUG( m_threadName, "camera", "SNAP!" );
		}
		else
		{
			currentStatus = STATUS_DEVICE_ERROR;
			json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
															    commandGroupId,
															    commandId,
															    sessionId,
															    ComponentType::Camera,
															    STATUS_DEVICE_ERROR,
															    false );
			bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, m_currentWorkOrigin );
			LOG_ERROR( m_threadName, "camera", "Failed taking a picture" );
			if (!sentOK)
			{
				LOG_ERROR( m_threadName, "camera", "Failed to send alert message" );
			}
			i = auto_focus_image_count; /// End work.
		}

		autoFocusPosition_nm += auto_focus_increment_nm;
		currentStatus = Implement_moveToFocusNM( m_commandMessageInObj, autoFocusPosition_nm );
		if (currentStatus != STATUS_OK)
		{
			LOG_ERROR( m_threadName, "optical_clumn", "Failed a focus move" );
			// TODO: Send an OC-related alert?
		}
	}

	return status;
}

/// Used by DoleOut_CollectMontageImages to prepare a status payload with x and y values.
/// MqttSender will encode an image and add it to this payload before sending it out to the broker.
bool CommandDolerThread::AddToPayloadXY( json_object* payloadObj, int x, int y )
{
	bool succeeded = true;
	if (json_object_is_type(payloadObj, json_type_object))
	{
		int result = json_object_object_add( payloadObj, X_INDEX, json_object_new_int( x ) );
		succeeded &= (result == 0);
		result = json_object_object_add( payloadObj, Y_INDEX, json_object_new_int( y ) );
		succeeded &= (result == 0);
	}
	else
	{
		succeeded = false;
		cout << m_threadName << ": AddToPayloadXY given wrong type of json_object" << endl;
	}
	return succeeded;
}

/// Used by DoleOut_CollectMontageImages and DoleOut_GetAutoFocusImageStack.
/// Send a status message containing a JSON message with all the normal body text. Message_type is "status".
/// Include a payload supplied by the caller.
/// Add raw picture data to the WorkMsg->blob. The MqttSender thread will convert that into Base64 text and
///    add it to the payload before sending it on.
int CommandDolerThread::SendPictureInStatusMessage( json_object* payloadObj,
													unsigned long pixel_count_x,
													unsigned long pixel_count_y,
													unsigned long bin_factor,
													unsigned long scaled_color_depth,
													unsigned long camera_color_depth,
													int commandId,
													int commandGroupId,
													std::string & messageType,
													std::string & sessionId,
													std::string & machineId,
													std::string & originTopic,
													std::string & statusDestinationTopic )
{
	/// CAUTION! Please do not lock g_cameraMutex here. It is being locked in the methods that call this method.
	bool succeeded = true;
	int status = STATUS_OK;

	/// Embed the payload into a full MQTT status message.
	string mqttPayload = m_desHelper->ComposeStatusString( commandGroupId,
															commandId,
															sessionId,
															STATUS,
															machineId,
															STATUS_OK,
															payloadObj );

	/// Create a WorkMsg object for sending it on to the MqttSender thread.
	std::shared_ptr<WorkMsg> statusMsg =
		make_shared<WorkMsg>( mqttPayload,
							  originTopic, // std::string originTopic,
							  statusDestinationTopic, // std::string destinationTopic,
							  messageType.c_str(), // const char* messageType,
							  READ_AND_SEND_PHOTO, // std::string subMsgType
							  false );
	/// Test for limited memory.
	/// One of the biggest use of memory is for images. Our test should be a size
	/// bigger than a typical image to help us identify a problem before it happens
	/// somewhere else.
	void * testBuffer = malloc( 8000000 );
	bool mallocFailed = (testBuffer == NULL);
	free( testBuffer );
	if (mallocFailed)
	{
		LOG_ERROR( m_threadName, "camera", "RUNNING LOW ON MEMORY. HOLDING..." );
		g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed ); /// Cause a hold.
		return STATUS_DEVICE_ERROR;		/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState cameraState = g_CameraState.load( std::memory_order_acquire );
	if (cameraState == ComponentState::Mocked)
	{
		/// Allocate space for the image. It will go out in this status message and
		/// be deallocated by the MqttSender thread.
		/// If possible, use the mock image file size as the blob size.
		string canned_photo_path = "";
		if ((pixel_count_x == 1920) && (pixel_count_y == 1460)) /// Size typical of Retiga camera
		{
			canned_photo_path = "/usr/share/pics/Rochacha.raw";
		}
		else if ((pixel_count_x == 2100) && (pixel_count_y == 2100)) /// Size typical of Basler camera
		{
			canned_photo_path = "/usr/share/pics/bf_2100x2.raw";
		}
		else
		{
			stringstream msg;
			msg << "Using a 1920x1460 picture when a " << pixel_count_x << "x" << pixel_count_y << " picture was requsted";
			LOG_WARN( m_threadName, "camera", msg.str().c_str() );
			canned_photo_path = "/usr/share/pics/Rochacha.raw";
		}
		
		FILE * file = fopen( canned_photo_path.c_str(), "rb" );
		if (file == nullptr)
		{
			statusMsg->blobSize = pixel_count_x * pixel_count_y;
		}
		else
		{
			/// Measure the file size
			fseek( file, 0, SEEK_END );
			statusMsg->blobSize = ftell( file );
			fclose( file );
		}

		statusMsg->blob = (unsigned char *) malloc( statusMsg->blobSize );
		if (statusMsg->blob == nullptr) // error condition
		{
			statusMsg->blobSize = 0L;
			cout << m_threadName << ": ERROR! Failed to allocate memory for image." << endl;
			return STATUS_MEMORY_ERROR;		/// EARLY RETURN!	EARLY RETURN!
		}
		
		/// Capture a picture and stuff it in the blob.
		succeeded &= MockCameraCapture( statusMsg->blob, statusMsg->blobSize );
	}
	else
	{
		/// Picture-snapping code starts here. This should stay identical to the code in DoleOut_getCameraImage().
		/// Before we allocate memory for the blob, free old memory if there is any.
		if (statusMsg->blobSize > 0)
		{
			cout << m_threadName << ": ERROR! blobSize is " << *m_blobSizePtr << " when it should be 0" << endl;
			free( statusMsg->blob );
		}

		/// Calculate size of the buffer for sending to the applciation.
		unsigned long scaledImageSize_bytes = CalculateImageBufferSize( pixel_count_x, pixel_count_y, bin_factor, scaled_color_depth );
		
		/// Allocate space for the outgoing image.
		/// This space will be inside the current WorkMsg. It will go out in the status message and
		/// be deallocated by the MqttSender thread.
		statusMsg->blobSize = scaledImageSize_bytes;
		statusMsg->blob = (unsigned char *) malloc( scaledImageSize_bytes );
		if (statusMsg->blob == nullptr) // error condition
		{
			statusMsg->blobSize = 0L;
			cout << m_threadName << ": ERROR! Failed to allocate memory for image." << endl;
			return STATUS_MEMORY_ERROR;		/// EARLY RETURN!	EARLY RETURN!
		}
		
		/// Calculate size of the buffer for receiving from the camera.
		unsigned long cameraImageSize_bytes = CalculateImageBufferSize( pixel_count_x, pixel_count_y, bin_factor, camera_color_depth );

		FreezeWorkInProgress();

// TODO: Delete this block.
//		if (bin_factor > 1L)
//		{
//			g_CameraList[0]->SetBinFactor( (int) bin_factor, (int) bin_factor );
//		}
		string getFrameMsg = "";
		
		if (1 <= camera_color_depth && camera_color_depth <= 8)
		{
			unsigned char *blobPtr = (unsigned char *) statusMsg->blob;
			succeeded = false; /// Unsupported case.
		}
		else if (8 < camera_color_depth && camera_color_depth <= 16 && scaled_color_depth <= camera_color_depth)
		{
			/// assume (binary) layout:  ppDD DDDD DDdd dddd  where p=padded 0s, D=desired bits, d=undesired bits
			/// or ppDD DDDD Dddd dddd
			unsigned short unwanted_bits = camera_color_depth - scaled_color_depth; /// eg 6 or 7
			unsigned short * exposureFrame = (unsigned short *) malloc( cameraImageSize_bytes );
			succeeded &= g_CameraList[0]->GetFrame( exposureFrame, getFrameMsg );
			cout << m_threadName << getFrameMsg << endl;
			if (succeeded)
			{
				unsigned char *blobPtr = (unsigned char *) statusMsg->blob;
				for (long i = 0L; i < scaledImageSize_bytes; i++)
				{
					/// Scale down the color range of each pixel, while copying it to the output buffer.
					unsigned short pixel = exposureFrame[i] >> unwanted_bits;
					blobPtr[i] = (unsigned char) (pixel & 0x00FF);
				}
				free( exposureFrame );
			}
			else
			{
				free( exposureFrame );
				LOG_ERROR( m_threadName, CAMERA, "Failed to grab picture" );
				status = STATUS_CAMERA_FAILED_GET_FRAME;
			}
		}
		else
		{
			succeeded = false; /// Unsupported case.
			stringstream msg;
			msg << "Failed to change color depth;camera_color_depth=" << camera_color_depth << ";scaled_color_depth=" << scaled_color_depth;
			LOG_ERROR( m_threadName, CAMERA, msg.str().c_str() );
			status = STATUS_DEVICE_ERROR;
		}

		/// Picture-snapping code ends here.
	}

	if (succeeded)
	{
		m_outBox->PushMsg(statusMsg);
	}
	else
	{
		cout << m_threadName << ": SendPictureInStatusMessage() failed somehow." << endl;
		/// statusMsg does not copy to another thread and instead gets deleted as soon as it goes out of scope?
	}

	return status;
}

///----------------------------------------------------------------------
/// DoleOut_moveToXYNm
/// Moves XY to x,y.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_moveToXYNm()
{
	/// Extract parameters from payload.
	long x_nm = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, X_NM );
	long y_nm = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, Y_NM );

	/// This status needs no payload.
	return Implement_moveToXYNm( x_nm, y_nm ); /// Assume that speeds are already packed in the payload.
}

int CommandDolerThread::Implement_moveToXYNm(long x_nm, long y_nm)
{
	int status = STATUS_OK;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "xy", "MoveToXYNm sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState XYState = g_XYState.load( std::memory_order_acquire );

	if (XYState == ComponentState::Mocked)
	{
		m_states_long[X_NM] = x_nm;
		m_states_long[Y_NM] = y_nm;
		status = STATUS_OK;
	}
	else if (XYState == ComponentState::Good)
	{
		long velocity = 0L;
		long acceleration = 0L;
		long deceleration = 0L;
		long stp = 0L;
		long axisResolution_nm = 0L;
		long x_soft_travel_limit_plus = 0L;
		long x_soft_travel_limit_minus = 0L;
		long y_soft_travel_limit_plus = 0L;
		long y_soft_travel_limit_minus = 0L;
		if (!m_desHelper->ExtractXYSpeeds( m_threadName, m_commandMessageInObj, "xy",
										 velocity, acceleration, deceleration, stp, axisResolution_nm,
										 x_soft_travel_limit_plus, x_soft_travel_limit_minus,
										 y_soft_travel_limit_plus, y_soft_travel_limit_minus ))
		{
			LOG_ERROR( m_threadName, "xy", "Missing speeds" );
			return STATUS_NOT_FOUND;			/// EARLY RETURN!	EARLY RETURN!
		}

		if(true)
		{
			stringstream msg;
			msg << "x_nm=" << x_nm << " y_nm=" << y_nm << " vel=" << velocity << " acc=" << acceleration << " dec=" << deceleration << " stp=" << stp;
			LOG_TRACE( m_threadName, "XYAxes", msg.str().c_str() );
		}
		if ((x_soft_travel_limit_minus > x_nm) || (x_nm > x_soft_travel_limit_plus))
		{
			stringstream msg;
			msg << "X move is out of (soft-limit) bounds|minus=" << x_soft_travel_limit_minus << "|X=" << x_nm << "|plus=" << x_soft_travel_limit_plus;
			LOG_ERROR( m_threadName, "x", msg.str().c_str() );
			return STATUS_MOTION_FORBIDDEN;			/// EARLY RETURN!	EARLY RETURN!
		}
		if ((y_soft_travel_limit_minus > y_nm) || (y_nm > y_soft_travel_limit_plus))
		{
			stringstream msg;
			msg << "Y move is out of (soft-limit) bounds|minus=" << y_soft_travel_limit_minus << "|Y=" << y_nm << "|plus=" << y_soft_travel_limit_plus;
			LOG_ERROR( m_threadName, "y", msg.str().c_str() );
			return STATUS_MOTION_FORBIDDEN;			/// EARLY RETURN!	EARLY RETURN!
		}
		FreezeWorkInProgress();
		status = Implement_MoveTwoAxesConcurrently( "X", x_nm, axisResolution_nm,
													"Y", y_nm, axisResolution_nm,
													velocity, acceleration, deceleration, stp );
		m_states_long[X_NM] = x_nm;
		m_states_long[Y_NM] = y_nm;
		if (true)
		{
			stringstream msg;
			msg << "X_nm=" << x_nm << " Y_nm=" << y_nm;
			LOG_TRACE( m_threadName, "xy", msg.str().c_str() );
		}
		if (status == STATUS_MOVEMENT_FAILED)
		{
			/// Send an alert.
			stringstream topic_stream;
			topic_stream << TOPIC_ES << CHANNEL_ALERT;

			/// Collect fields from the command's header for use in the alert.
			std::string messageType = "";
			int commandGroupId = 0;
			int commandId = 0;
			std::string sessionId = "";
			std::string machineId = "";
			bool succeeded = m_desHelper->SplitCommand( m_commandMessageInObj,
														&messageType,
														nullptr, // &transportVersion,
														&commandGroupId,
														&commandId,
														&sessionId,
														&machineId );

			json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
																commandGroupId,
																commandId,
																sessionId, // Or topic_stream.str()?
																ComponentType::XYAxes,
																STATUS_AXIS_X_FAULTED,
																false ); /// thisIsCausingHold
			bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "xy", "Wrong XY state" );
	}

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_getCurrentXYNm
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_getCurrentXYNm()
{
	int status = STATUS_OK;
	long x_nm = 0L;
	long y_nm = 0L;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "xy", "GetCurrentXYNM sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState XYState = g_XYState.load( std::memory_order_acquire );

	if (XYState == ComponentState::Mocked)
	{
		x_nm = m_states_long[X_NM];
		y_nm = m_states_long[Y_NM];
	}
	else if (XYState == ComponentState::Good)
	{
		status = Implement_GetAxisPosition( "x", x_nm ); /// Temporarily means "xStatus"
		if (x_nm != m_states_long[X_NM])
		{
			stringstream msg;
			msg << "X measured-pos=" << x_nm << "|recorded-pos=" << m_states_long[X_NM];
			LOG_WARN( m_threadName, "x", msg.str().c_str() );
			m_states_long[X_NM] = x_nm;
		}
		int yStatus = Implement_GetAxisPosition( "y", y_nm );
		if (y_nm != m_states_long[Y_NM])
		{
			stringstream msg;
			msg << "Y measured-pos=" << y_nm << "|recorded-pos=" << m_states_long[Y_NM];
			LOG_WARN( m_threadName, "y", msg.str().c_str() );
			m_states_long[Y_NM] = y_nm;
		}
		/// We collected different statuses for x and y. Now make sure that any failures
		/// are preserved in the one true status.
		if (status == STATUS_OK)
		{
			status = yStatus;
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "xy", "Wrong XY state" );
	}

	/// Populate the status payload
    int resultX = json_object_object_add( m_statusPayloadOutObj, X_NM, json_object_new_int64( x_nm ) );
    int resultY = json_object_object_add( m_statusPayloadOutObj, Y_NM, json_object_new_int64( y_nm ) );
    if (status == STATUS_OK)
    {
		if ((resultX != 0) || (resultY != 0))
		{
			status = STATUS_CMD_MSG_ERROR;
		}
	}
    return status;
}

///----------------------------------------------------------------------
/// DoleOut_swirlXYStage
/// Moves XY in a circular motion 1 or more times
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_swirlXYStage()
{
	int status = STATUS_OK;
	bool succeeded = true;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "xy", "DoleOut_swirlXYStage sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string(payloadObj);
		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "xy", "Failed to find payload" );
		return STATUS_CMD_MSG_DATA_MISSING;			/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState XYState = g_XYState.load( std::memory_order_acquire );

	if (XYState == ComponentState::Mocked)
	{
		status = STATUS_OK;
		/// No need to update m_states_long here, because swirling ends where it starts.
	}
	else if (XYState == ComponentState::Good)
	{
		unsigned long numberOfRotations = 0;
		long acceleration = 0L;
		long deceleration = 0L;
		long stp = 0L;
		long axisResolution_nm = 0L;
		long x_soft_travel_limit_plus = 0L;
		long x_soft_travel_limit_minus = 0L;
		long y_soft_travel_limit_plus = 0L;
		long y_soft_travel_limit_minus = 0L;
		long velocity_notUsed = 0L;
		succeeded &= m_desHelper->ExtractULongValue( m_threadName, payloadObj, NUMBER_OF_ROTATIONS, numberOfRotations );

		succeeded &= m_desHelper->ExtractXYSpeeds( m_threadName, m_commandMessageInObj, "xy",
										 velocity_notUsed, acceleration, deceleration, stp, axisResolution_nm,
										 x_soft_travel_limit_plus, x_soft_travel_limit_minus,
										 y_soft_travel_limit_plus, y_soft_travel_limit_minus );

		PlateType plateType = this->m_desHelper->ExtractPayloadPlateType( m_commandMessageInObj );
		float rotationRadius_nm = plateType.swirlRadius;
		float velocity_nmPerSec = plateType.swirlVelocity;	/// This overrides the one set_speed from ExtractXYSpeeds.
		acceleration = plateType.swirlAcceleration; 		/// This overrides the one acceleration from ExtractXYSpeeds.

		if (!succeeded)
		{
			LOG_ERROR( m_threadName, "xy", "Failed to find needed payload contents" );
			return STATUS_CMD_MSG_DATA_MISSING;			/// EARLY RETURN!	EARLY RETURN!
		}

		/// A. Move the plate to 0X,0Y.
		long original_x_nm = m_states_long[X_NM];
		long original_y_nm = m_states_long[Y_NM];
		long x_nm = 0L;
		long y_nm = 0L;
		status = Implement_moveToXYNm( x_nm, y_nm );
		if ((status == STATUS_MOVEMENT_FAILED) || (status == STATUS_MATH_ERROR))
		{
			/// Assume that Implement_moveToXYNm() already sent out an alert.
			return status;	// 	EARLY RETURN!!	EARLY RETURN!!
		}

		/// B. Swirl the plate in a circle.
		long centerX_nm = x_nm; /// This implies that the soft-limit check for X will be (+/- radius).
		long centerY_nm = y_nm + (long) rotationRadius_nm; /// This implies that the soft-limit check for Y will be (+ diameter/-0)
		long diameter_nm = (long) (2.0 * rotationRadius_nm);
		if(true)
		{
			stringstream msg;
			msg << "numberOfRotations=" << numberOfRotations << "|rotationRadius=" << rotationRadius_nm << "|vel=" << velocity_nmPerSec << "|acc=" << acceleration << "|dec=" << deceleration << "|stp=" << stp;
			LOG_TRACE( m_threadName, "xy", msg.str().c_str() );
		}
		if ((x_soft_travel_limit_minus > (x_nm - (long)rotationRadius_nm)) || ((x_nm + (long)rotationRadius_nm) > x_soft_travel_limit_plus))
		{
			stringstream msg;
			msg << "[X plus/minus diameter] is out of (soft-limit) bounds|minus=" << x_soft_travel_limit_minus << "|X=" << x_nm << "|plus=" << x_soft_travel_limit_plus;
			LOG_ERROR( m_threadName, "x", msg.str().c_str() );
			return STATUS_MOTION_FORBIDDEN;			/// EARLY RETURN!	EARLY RETURN!
		}
		if ((y_soft_travel_limit_minus > y_nm) || ((y_nm + diameter_nm) > y_soft_travel_limit_plus))
		{
			stringstream msg;
			msg << "Y move is out of (soft-limit) bounds|minus=" << y_soft_travel_limit_minus << "|Y=" << y_nm << "|plus=" << y_soft_travel_limit_plus;
			LOG_ERROR( m_threadName, "y", msg.str().c_str() );
			return STATUS_MOTION_FORBIDDEN;			/// EARLY RETURN!	EARLY RETURN!
		}
		FreezeWorkInProgress();
		if (status == STATUS_OK)
		{
			status = Implement_SwirlXYStage( "X", x_nm, centerX_nm, axisResolution_nm,
											 "Y", y_nm, centerY_nm, axisResolution_nm,
											 rotationRadius_nm, numberOfRotations,
											 velocity_nmPerSec, acceleration, deceleration, stp );
			m_states_long[X_NM] = x_nm;
			m_states_long[Y_NM] = y_nm;
			if (true)
			{
				stringstream msg;
				msg << "After swirling, X_nm=" << x_nm << " Y_nm=" << y_nm;
				LOG_TRACE( m_threadName, "xy", msg.str().c_str() );
			}
		}
		if ((status == STATUS_MOVEMENT_FAILED) || (status == STATUS_MATH_ERROR))
		{
			/// Send an alert.
			stringstream topic_stream;
			topic_stream << TOPIC_ES << CHANNEL_ALERT;

			/// Collect fields from the command's header for use in the alert.
			std::string messageType = "";
			int commandGroupId = 0;
			int commandId = 0;
			std::string sessionId = "";
			std::string machineId = "";
			bool succeeded = m_desHelper->SplitCommand( m_commandMessageInObj,
														&messageType,
														nullptr, // &transportVersion,
														&commandGroupId,
														&commandId,
														&sessionId,
														&machineId );

			json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
																commandGroupId,
																commandId,
																sessionId, // Or topic_stream.str()?
																ComponentType::XYAxes,
																STATUS_AXIS_X_FAULTED,
																false ); /// thisIsCausingHold
			bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
			return status;	// 	EARLY RETURN!!	EARLY RETURN!!
		}

		/// C. Move the plate back to original position.
		if (status == STATUS_OK)
		{
			status = Implement_moveToXYNm( original_x_nm, original_y_nm );
		}

	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "xy", "Wrong XY state" );
	}

	/// This status needs no payload.
	return status;
}

int CommandDolerThread::Implement_SwirlXYStage( std::string axisNameAStr, long & startA_nm, long centerA_nm, long axisResolutionA_nm,
												std::string axisNameBStr, long & startB_nm, long centerB_nm, long axisResolutionB_nm,
												float rotationRadius_nm, unsigned long numberOfRotations,
												float & velocity_nmPerSec, long & acceleration_nmPerSecPerSec, long & deceleration_nmPerSecPerSec, long & stp_nmPerSecPerSec )
{
	int status = STATUS_OK;
	const char * axisNameA = axisNameAStr.c_str();
	const char * axisNameB = axisNameBStr.c_str();
	MotionAxis &axisA = g_motionServices_ptr->getAxis(axisNameAStr);
	MotionAxis &axisB = g_motionServices_ptr->getAxis(axisNameBStr);
	string twoAxis = axisNameAStr + axisNameBStr;
	
	/// Sanity-check the numbers we'll be working with.
	if (isnan( startA_nm ) || isnan( startB_nm ) || isnan( velocity_nmPerSec ))
	{
		stringstream msg;
		msg << "One of these values is NAN: " << startA_nm << "|" << startB_nm << "|" << velocity_nmPerSec;
		LOG_ERROR( m_threadName, twoAxis.c_str(), msg.str().c_str() );
		return STATUS_MATH_ERROR;	//	EARLY RETURN!	EARLY RETURN!
	}

	///
	/// Dont confirm that we are already at the start position. Trust the caller to do that.
	/// But do confirm that 1+ rotations were requested.
	///
	if (numberOfRotations == 0)
	{
		return STATUS_OK;	//	EARLY RETURN!	EARLY RETURN!
	}

	///
	/// Send the command that starts motion.
	///
	try {
		ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllerOfAxis( axisNameAStr );
		/// Convert values to millimeters. TODO: Try setting deceleration to 0.
		pController->setVelAccelDecelStop( (float)velocity_nmPerSec/_1MEG_F, (float)acceleration_nmPerSecPerSec/_1MEG_F, (float)deceleration_nmPerSecPerSec/_1MEG_F, (float)stp_nmPerSecPerSec/_1MEG_F );
		usleep(500000);
		pController->swirl( axisA.getName(),
							axisB.getName(),
							(float)startA_nm/_1MEG_F,
							(float)startB_nm/_1MEG_F,
							(float)centerA_nm/_1MEG_F,
							(float)centerB_nm/_1MEG_F,
							numberOfRotations );
		usleep(500000);
    } catch (...) {
		status = STATUS_MOVEMENT_FAILED;
		LOG_ERROR( m_threadName, twoAxis.c_str(), "unknown EXCEPTION!" );
		return status;	//	EARLY RETURN!	EARLY RETURN!
	}

	// Temporary debug.
	{
		stringstream msg;
		msg << axisNameA << "_PPU=" << axisA.getCachedPPU() << " " << axisNameB << "_PPU=" << axisB.getCachedPPU();
		LOG_DEBUG( m_threadName, twoAxis.c_str(), msg.str().c_str() );
	}

	///
	/// Wait for motion to start
	///
	int loopCount = 0;
	bool isInMotionA = false;
	bool isInMotionB = false;
	do
	{
		try {
			isInMotionA = axisA.isInMotion();
			isInMotionB = axisB.isInMotion();
		}  catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, twoAxis.c_str(), "masterFlags: STILL PENDING" );
		}
		if (loopCount++ > MAX_LOOPS_AWAITING_MOTION_START)
		{
			status = STATUS_MOVEMENT_FAILED;
			LOG_ERROR( m_threadName, twoAxis.c_str(), "timed out waiting for motion to start" );
			return status;	//	EARLY RETURN!	EARLY RETURN!
		}
		usleep(10000);
	} while (!isInMotionA && !isInMotionB);

	LOG_TRACE( m_threadName, twoAxis.c_str(), "Motion detected" );

	///
	/// Wait for motion to stop (according to in-motion and velocity)
	///
	float positionA_mm = 0.0;
	float positionB_mm = 0.0;
	int strikeCount = 0;
	bool hasVelocityA = false;
	bool hasVelocityB = false;
	float velocityA = 0.0;
	float velocityB = 0.0;
	loopCount = 0;
	while (isInMotionA || isInMotionB || hasVelocityA || hasVelocityB) {
		try {
			isInMotionA = axisA.isInMotion();
			isInMotionB = axisB.isInMotion();
			velocityA = axisA.getCurrentVelocity();
			hasVelocityA = (Abs( velocityA ) > MAX_ALLOWABLE_VELOCITY_ERROR);
			velocityB = axisB.getCurrentVelocity();
			hasVelocityB = (Abs( velocityB ) > MAX_ALLOWABLE_VELOCITY_ERROR);
			// Get position info.
			positionA_mm = (float)axisA.getActualPosition() / axisA.getCachedPPU();
			positionB_mm = (float)axisB.getActualPosition() / axisB.getCachedPPU();
		} catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, twoAxis.c_str(), "position: STILL PENDING" );
		}

		if (loopCount++ % 10 == 0)
		{
			/// Movement is on course.
			stringstream msgA;
			msgA << axisNameA << "_vel=" << velocityA << "|" << axisNameA << "_pos(mm)=" << positionA_mm;
			LOG_TRACE( m_threadName, axisNameA, msgA.str().c_str() );
			stringstream msgB;
			msgB << axisNameB << "_vel=" << velocityB << "|" << axisNameB << "_pos(mm)=" << positionB_mm;
			LOG_TRACE( m_threadName, twoAxis.c_str(), msgB.str().c_str() );
		}
		if (loopCount > MAX_LOOPS_AWAITING_MOTION_END)
		{
			status = STATUS_MOVEMENT_FAILED;
			LOG_ERROR( m_threadName, twoAxis.c_str(), "timed out waiting for motion to end" );
			return status;	//	EARLY RETURN!	EARLY RETURN!
		}
		usleep(10000);
	}

	///
	/// Wait for motion to stop (according to in-position-band)
	///
	strikeCount = 0;
	bool inPositionBandA = false;
	bool inPositionBandB = false;
	do {
		try {
			positionA_mm = (float)axisA.getActualPosition() / axisA.getCachedPPU();
			positionB_mm = (float)axisB.getActualPosition() / axisB.getCachedPPU();
			inPositionBandA = axisA.isInPositionBand();
			inPositionBandB = axisB.isInPositionBand();
		} catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, twoAxis.c_str(), "position: STILL PENDING" );
		}

		if (strikeCount++ >= MAX_STRIKES)
		{
			status = STATUS_MOVEMENT_FAILED;
			return status;	//	EARLY RETURN!	EARLY RETURN!
		}

		stringstream msg;
		msg << axisNameA << "_pos(mm)=" << positionA_mm << "|" << axisNameB << "_pos(mm)=" << positionB_mm;
		if ((!inPositionBandA) || (!inPositionBandB))
		{
			LOG_TRACE( m_threadName, twoAxis.c_str(), msg.str().c_str() );
		}
		else
		{
			/// Final resting position.
			LOG_INFO( m_threadName, twoAxis.c_str(), msg.str().c_str() );
		}
		usleep(10000);
	} while ((!inPositionBandA) || (!inPositionBandB));

	return status;
}


///----------------------------------------------------------------------
/// DoleOut_moveToXXNm
/// Moves XX axis to xx_nm
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_moveToXXNm()
{
	/// Extract parameters from payload.
	long xx_nm = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, XX_NM );

	/// This status needs no payload.
	return Implement_moveToXXNm( xx_nm ); /// Assume that speeds are already packed in the payload.
}


int CommandDolerThread::Implement_moveToXXNm(long xx_nm)
{
	int status = STATUS_OK;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "xx", "MoveToXXNm sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState XXState = g_XXState.load( std::memory_order_acquire );

	if (XXState == ComponentState::Mocked)
	{
		m_states_long[XX_NM] = xx_nm;
		status = STATUS_OK;
		float xx_mm = (float)xx_nm / _1MEG_F;
		stringstream msg;
		msg << "Mocking XX move to " << xx_mm;
		LOG_TRACE( m_threadName, "xx", msg.str().c_str() );
	}
	/// Allow movement when state is Home3 for the sake of initializing the picking pump.
	else if ((XXState == ComponentState::Good) || (XXState == ComponentState::Homed3))
	{
		/// Extract parameters from payload.
		long velocity = 0L;
		long acceleration = 0L;
		long deceleration = 0L;
		long stp = 0L;
		long axisResolution_nm = 0L;
		long softTravelLimitPlus_nm = 0L;
		long softTravelLimitMinus_nm = 0L;
		if (!m_desHelper->ExtractSpeedsAndLimits( m_threadName, m_commandMessageInObj, "xx",
										 velocity, acceleration, deceleration, stp, axisResolution_nm,
										 softTravelLimitPlus_nm, softTravelLimitMinus_nm ))
		{
			return STATUS_NOT_FOUND;			/// EARLY RETURN!	EARLY RETURN!
		}
		if ((softTravelLimitMinus_nm > xx_nm) || (xx_nm > softTravelLimitPlus_nm))
		{
			stringstream msg;
			msg << "XX move is out of (soft-limit) bounds|minus=" << softTravelLimitMinus_nm << "|XX=" << xx_nm << "|plus=" << softTravelLimitPlus_nm;
			LOG_ERROR( m_threadName, "xx", msg.str().c_str() );
			return STATUS_MOTION_FORBIDDEN;			/// EARLY RETURN!	EARLY RETURN!
		}
		FreezeWorkInProgress();
		status = Implement_MoveOneAxis( "XX", xx_nm, axisResolution_nm,
										velocity, acceleration, deceleration, stp );
		m_states_long[XX_NM] = xx_nm;
		if (status == STATUS_MOVEMENT_FAILED)
		{
			/// Send an alert.
			stringstream topic_stream;
			topic_stream << TOPIC_ES << CHANNEL_ALERT;

			/// Collect fields from the command's header for use in the alert.
			std::string messageType = "";
			int commandGroupId = 0;
			int commandId = 0;
			std::string sessionId = "";
			std::string machineId = "";
			bool succeeded = m_desHelper->SplitCommand( m_commandMessageInObj,
														&messageType,
														nullptr, // &transportVersion,
														&commandGroupId,
														&commandId,
														&sessionId,
														&machineId );

			json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
																commandGroupId,
																commandId,
																sessionId,
																ComponentType::XXAxis,
																STATUS_AXIS_XX_FAULTED,
																false ); /// thisIsCausingHold

			bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "xx", "Wrong XX state" );
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_getCurrentXXNm
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_getCurrentXXNm()
{
	int status = STATUS_OK;
	long xx_nm = 0L;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "xx", "GetCurrentXXNM sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState XXState = g_XXState.load( std::memory_order_acquire );

	if (XXState == ComponentState::Mocked)
	{
		xx_nm = m_states_long[XX_NM];
	}
	/// Allow movement when state is Home3 for the sake of initializing the picking pump.
	else if ((XXState == ComponentState::Good) || (XXState == ComponentState::Homed3))
	{
		status = Implement_GetAxisPosition( "xx", xx_nm );
		if (xx_nm != m_states_long[XX_NM])
		{
			stringstream msg;
			msg << "XX measured-pos=" << xx_nm << "|recorded-pos=" << m_states_long[XX_NM];
			LOG_WARN( m_threadName, "xx", msg.str().c_str() );
			m_states_long[XX_NM] = xx_nm;
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "xx", "Wrong XX state" );
	}

	/// Populate the status payload
    int result = json_object_object_add( m_statusPayloadOutObj, XX_NM, json_object_new_int64( xx_nm ) );
	if ((status == STATUS_OK) && (result != 0))
	{
		/// populating the payload failed and nothing else did
		status = STATUS_CMD_MSG_ERROR;
	}
	return status;
}


///----------------------------------------------------------------------
/// DoleOut_moveToYYNm
/// Moves YY axis to yy_nm
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_moveToYYNm()
{
	/// Extract parameters from payload.
	long yy_nm = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, YY_NM );

	/// This status needs no payload.
	return Implement_moveToYYNm( yy_nm ); /// Assume that speeds are already packed in the payload.
}


int CommandDolerThread::Implement_moveToYYNm(long yy_nm)
{
	int status = STATUS_OK;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "yy", "MoveToYYNm sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState YYState = g_YYState.load( std::memory_order_acquire );

	if (YYState == ComponentState::Mocked)
	{
		m_states_long[YY_NM] = yy_nm;
		status = STATUS_OK;
		float yy_mm = (float)yy_nm / _1MEG_F;
		stringstream msg;
		msg << "Mocking YY move to " << yy_mm;
		LOG_TRACE( m_threadName, "yy", msg.str().c_str() );
	}
	/// Allow movement when state is Home3 for the sake of initializing the picking pump.
	else if ((YYState == ComponentState::Good) || (YYState == ComponentState::Homed3))
	{
		/// Extract parameters from payload.
		long velocity = 0L;
		long acceleration = 0L;
		long deceleration = 0L;
		long stp = 0L;
		long axisResolution_nm = 0L;
		long softTravelLimitPlus_nm = 0L;
		long softTravelLimitMinus_nm = 0L;
		if (!m_desHelper->ExtractSpeedsAndLimits( m_threadName, m_commandMessageInObj, "yy",
										 velocity, acceleration, deceleration, stp, axisResolution_nm,
										 softTravelLimitPlus_nm, softTravelLimitMinus_nm ))
		{
			return STATUS_NOT_FOUND;			/// EARLY RETURN!	EARLY RETURN!
		}
		if ((softTravelLimitMinus_nm > yy_nm) || (yy_nm > softTravelLimitPlus_nm))
		{
			stringstream msg;
			msg << "YY move is out of (soft-limit) bounds|minus=" << softTravelLimitMinus_nm << "|YY=" << yy_nm << "|plus=" << softTravelLimitPlus_nm;
			LOG_ERROR( m_threadName, "yy", msg.str().c_str() );
			return STATUS_MOTION_FORBIDDEN;			/// EARLY RETURN!	EARLY RETURN!
		}
		FreezeWorkInProgress();
		status = Implement_MoveOneAxis( "YY", yy_nm, axisResolution_nm,
										velocity, acceleration, deceleration, stp );
		m_states_long[YY_NM] = yy_nm;
		if (status == STATUS_MOVEMENT_FAILED)
		{
			/// Send an alert.
			stringstream topic_stream;
			topic_stream << TOPIC_ES << CHANNEL_ALERT;

			/// Collect fields from the command's header for use in the alert.
			std::string messageType = "";
			int commandGroupId = 0;
			int commandId = 0;
			std::string sessionId = "";
			std::string machineId = "";
			bool succeeded = m_desHelper->SplitCommand( m_commandMessageInObj,
														&messageType,
														nullptr, // &transportVersion,
														&commandGroupId,
														&commandId,
														&sessionId,
														&machineId );

			json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
																commandGroupId,
																commandId,
																sessionId, // Or topic_stream.str()?
																ComponentType::YYAxis,
																STATUS_AXIS_YY_FAULTED,
																false ); /// thisIsCausingHold
			bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "yy", "Wrong YY state" );
	}
	return status;
}


///----------------------------------------------------------------------
/// DoleOut_getCurrentYYNm
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_getCurrentYYNm()
{
	int status = STATUS_OK;
	long yy_nm = 0L;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "yy", "GetCurrentYYNM sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState YYState = g_YYState.load( std::memory_order_acquire );

	if (YYState == ComponentState::Mocked)
	{
		yy_nm = m_states_long[YY_NM];
	}
	/// Allow movement when state is Home3 for the sake of initializing the picking pump.
	else if ((YYState == ComponentState::Good) || (YYState == ComponentState::Homed3))
	{
		status = Implement_GetAxisPosition( "yy", yy_nm );
		if (yy_nm != m_states_long[YY_NM])
		{
			stringstream msg;
			msg << "YY measured-pos=" << yy_nm << "|recorded-pos=" << m_states_long[YY_NM];
			LOG_WARN( m_threadName, "yy", msg.str().c_str() );
			m_states_long[YY_NM] = yy_nm;
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "yy", "Wrong YY state" );
	}

	/// Populate the status payload
    int result = json_object_object_add( m_statusPayloadOutObj, YY_NM, json_object_new_int64( yy_nm ) );
	if ((status == STATUS_OK) && (result != 0))
	{
		/// populating the payload failed and nothing else did
		status = STATUS_CMD_MSG_ERROR;
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_MoveYYToTipRackGrabLocation
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveYYToTipRackGrabLocation()
{
	int status = STATUS_OK;
	long yy_tip_tray_cxpm_grab_location = 0L;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "yy", "MoveYYToTipRackGrabLocation( sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		// TODO: Delete this temporary debug.
		string payloadStr = json_object_to_json_string( payloadObj );
		cout << payloadStr << endl;

		/// Extract payload contents.
		if (m_desHelper->ExtractLongValue( m_threadName, payloadObj, YY_TIP_TRAY_CXPM_GRAB_POSITION, yy_tip_tray_cxpm_grab_location ))
		{
			status = Implement_moveToYYNm(yy_tip_tray_cxpm_grab_location);
		}
		else
		{
			/// DesHelper::ExtractLongValue should have logged the error.
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
	}
	else
	{
		LOG_ERROR( m_threadName, "yy", "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}

	/// This status needs no payload.
	return status;
}

///
/// XY Axis Motion
///

int CommandDolerThread::DoleOut_MoveXYToSpecialNotation( bool forWell, bool useA1Notation )
{
	LOG_TRACE( m_threadName, "xy", "-" );
	long x_nm = 0L;
	long y_nm = 0L;
	long velocity = 0L;
	long acceleration = 0L;
	long deceleration = 0L;
	long stp = 0L;
	long axisResolutionNm = 300L;

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string(payloadObj);
		cout << payloadStr << endl;
	}

	bool succeeded = m_desHelper->ExtractNmNotations(   m_threadName,
														m_commandMessageInObj,
														forWell,
														useA1Notation,
														x_nm,
														y_nm,
														velocity,
														acceleration,
														deceleration,
														stp,
														axisResolutionNm );

	int status = (succeeded) ? STATUS_OK : STATUS_DEVICE_ERROR;

	ComponentState XYState = g_XYState.load( std::memory_order_acquire );
	if (XYState == ComponentState::Mocked)
	{
		m_states_long[X_NM] = x_nm;
		m_states_long[Y_NM] = y_nm;
		status = STATUS_OK;
	}
	else if (XYState == ComponentState::Good)
	{
		if (succeeded)
		{
			FreezeWorkInProgress();
			status = Implement_MoveTwoAxesConcurrently( "X", x_nm, axisResolutionNm,
														"Y", y_nm, axisResolutionNm,
														velocity, acceleration, deceleration, stp );
			m_states_long[X_NM] = x_nm;
			m_states_long[Y_NM] = y_nm;
			{
				stringstream msg;
				msg << "X_nm=" << x_nm << " Y_nm=" << y_nm;
				LOG_TRACE( m_threadName, "xy", msg.str().c_str() );
			}
			if (status == STATUS_MOVEMENT_FAILED)
			{
				/// Send an alert.
				stringstream topic_stream;
				topic_stream << TOPIC_ES << CHANNEL_ALERT;

				/// Collect fields from the command's header for use in the alert.
				std::string messageType = "";
				int commandGroupId = 0;
				int commandId = 0;
				std::string sessionId = "";
				std::string machineId = "";
				bool succeeded = m_desHelper->SplitCommand( m_commandMessageInObj,
															&messageType,
															nullptr, // &transportVersion,
															&commandGroupId,
															&commandId,
															&sessionId,
															&machineId );

				json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
																	commandGroupId,
																	commandId,
																	sessionId, // Or topic_stream.str()?
																	ComponentType::XYAxes,
																	STATUS_AXIS_X_FAULTED,
																	false ); /// thisIsCausingHold
				bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
			}
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "xy", "Wrong XY state" );
	}

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_moveXToTipSkewTrigger
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_moveXToTipSkewTrigger( bool packStatusPayload )
{
	int status = STATUS_OK;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "x", "DoleOut_moveXToTipSkewTrigger() sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		// TODO: Delete this temporary debug.
		string payloadStr = json_object_to_json_string( payloadObj );
//		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "x", "Payload is missing" );
		return STATUS_CMD_MSG_DATA_MISSING;			/// EARLY RETURN!	EARLY RETURN!
	}

	bool succeeded = true;
	long x_optical_sensor_to_tip_skew_position = 0L;
	long x_find_tip_skew_travel_distance = 0L;
	unsigned long x_find_tip_skew_speed = 0L;
	unsigned long x_find_tip_skew_acceleration = 0L;
	unsigned long x_find_tip_skew_deceleration = 0L;
	long x_trigger_location_nm = 0L;

	/// Extract values from payload
	if (payloadObj)
	{
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, X_OPTICAL_SENSOR_TO_TIP_SKEW_POSITION, x_optical_sensor_to_tip_skew_position );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, X_FIND_TIP_SKEW_TRAVEL_DISTANCE, x_find_tip_skew_travel_distance );
		succeeded &= m_desHelper->ExtractULongValue( m_threadName, payloadObj, X_FIND_TIP_SKEW_SPEED, x_find_tip_skew_speed );
		succeeded &= m_desHelper->ExtractULongValue( m_threadName, payloadObj, X_FIND_TIP_SKEW_ACCELERATION, x_find_tip_skew_acceleration );
		succeeded &= m_desHelper->ExtractULongValue( m_threadName, payloadObj, X_FIND_TIP_SKEW_DECELERATION, x_find_tip_skew_deceleration );

		if (!succeeded)
		{
			LOG_ERROR( m_threadName, "x", "Failed to some payload content" );
			return STATUS_CMD_MSG_DATA_MISSING;			/// EARLY RETURN!	EARLY RETURN!
		}
	}
	if (status == STATUS_OK)
	{
		status = Implement_moveXYToTipSkewTrigger( "x", x_optical_sensor_to_tip_skew_position,
													x_find_tip_skew_travel_distance, x_find_tip_skew_speed,
													x_find_tip_skew_acceleration, x_find_tip_skew_deceleration,
													NEEDLE_X_MAX_POS_SN, 10000000L,
													FIND_NEEDLE_X_POS_ROUTINE, NEEDLE_X_POS_SN,
													FIND_NEEDLE_X_STATUS_SN, x_trigger_location_nm );
	}

	/// Enable this option if this is being called directly from CommandDolerThread::DoWork()
	/// Disable it called indirectly from another DoleOut_ function.
	if ( packStatusPayload )
	{
		/// Populate the status payload
		int result = json_object_object_add( m_statusPayloadOutObj, X_TRIGGER_LOCATION_NM, json_object_new_int64( x_trigger_location_nm ) );
		if ((status == STATUS_OK) && (result != 0))
		{
			/// populating the payload failed and nothing else did
			status = STATUS_CMD_MSG_ERROR;
		}
	}
	
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_moveYToTipSkewTrigger
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_moveYToTipSkewTrigger( bool packStatusPayload )
{
	int status = STATUS_OK;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "y", "DoleOut_moveYToTipSkewTrigger() sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		// TODO: Delete this temporary debug.
		string payloadStr = json_object_to_json_string( payloadObj );
//		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "y", "Payload is missing" );
		return STATUS_CMD_MSG_DATA_MISSING;			/// EARLY RETURN!	EARLY RETURN!
	}

	bool succeeded = true;
	long y_optical_sensor_to_tip_skew_position = 0L;
	long y_find_tip_skew_travel_distance = 0L;
	unsigned long y_find_tip_skew_speed = 0L;
	unsigned long y_find_tip_skew_acceleration = 0L;
	unsigned long y_find_tip_skew_deceleration = 0L;
	long y_trigger_location_nm = 0L;

	/// Extract values from payload
	if (payloadObj)
	{
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Y_OPTICAL_SENSOR_TO_TIP_SKEW_POSITION, y_optical_sensor_to_tip_skew_position );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Y_FIND_TIP_SKEW_TRAVEL_DISTANCE, y_find_tip_skew_travel_distance );
		succeeded &= m_desHelper->ExtractULongValue( m_threadName, payloadObj, Y_FIND_TIP_SKEW_SPEED, y_find_tip_skew_speed );
		succeeded &= m_desHelper->ExtractULongValue( m_threadName, payloadObj, Y_FIND_TIP_SKEW_ACCELERATION, y_find_tip_skew_acceleration );
		succeeded &= m_desHelper->ExtractULongValue( m_threadName, payloadObj, Y_FIND_TIP_SKEW_DECELERATION, y_find_tip_skew_deceleration );

		if (!succeeded)
		{
			LOG_ERROR( m_threadName, "y", "Failed to some payload content" );
			return STATUS_CMD_MSG_DATA_MISSING;			/// EARLY RETURN!	EARLY RETURN!
		}
	}
	if (status == STATUS_OK)
	{
		status = Implement_moveXYToTipSkewTrigger( "y", y_optical_sensor_to_tip_skew_position,
													y_find_tip_skew_travel_distance, y_find_tip_skew_speed,
													y_find_tip_skew_acceleration, y_find_tip_skew_deceleration,
													NEEDLE_Y_MAX_POS_SN, 10000000L,
													FIND_NEEDLE_Y_POS_ROUTINE, NEEDLE_Y_POS_SN,
													FIND_NEEDLE_Y_STATUS_SN, y_trigger_location_nm );
	}

	/// Enable this option if this is being called directly from CommandDolerThread::DoWork()
	/// Disable it called indirectly from another DoleOut_ function.
	if ( packStatusPayload )
	{
		/// Populate the status payload
		int result = json_object_object_add( m_statusPayloadOutObj, Y_TRIGGER_LOCATION_NM, json_object_new_int64( y_trigger_location_nm ) );
		if ((status == STATUS_OK) && (result != 0))
		{
			/// populating the payload failed and nothing else did
			status = STATUS_CMD_MSG_ERROR;
		}
	}
	
	return status;
}

int CommandDolerThread::Implement_moveXYToTipSkewTrigger( const string axisNameStr_in,
														  const long opticalSensorToTipSkewPosition_in,
														  const long findTipSkewTravelDistance_in,
														  const unsigned long findTipSkewSpeed_in,
														  const unsigned long findTipSkewAcceleration_in,
														  const unsigned long findTipSkewDeceleration_in,
														  const unsigned int maxPosSn_in,		// eg NEEDLE_Y_MAX_POS_SN or NEEDLE_X_MAX_POS_SN
														  const unsigned long maxPosValNm_in,	// eg 10,000,000
														  const int routineId_in, 				// eg FIND_NEEDLE_Y_POS_ROUTINE
														  const unsigned int positionSn_in, 	// eg NEEDLE_Y_POS_SN or NEEDLE_X_POS_SN
														  const unsigned int findNeedleStatusSn_in, // eg FIND_NEEDLE_Y_STATUS_SN or FIND_NEEDLE_X_STATUS_SN
														  long & triggerLocation_nm_out )
{
	int status = STATUS_OK;
	ComponentState XYState = g_XYState.load( std::memory_order_acquire );
	string axisName_NM = axisNameStr_in;
 	std::transform( axisName_NM.begin(), axisName_NM.end(), axisName_NM.begin(), (int(*)(int)) std::tolower ); /// Change to lower case
	axisName_NM = axisName_NM + "_nm";
	struct timespec when;
	
	FreezeWorkInProgress();
	if (XYState == ComponentState::Mocked)
	{
		triggerLocation_nm_out = m_states_long[axisName_NM];
	}
	else if (XYState == ComponentState::Good)
	{
		try {
			ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at( A_CONTROLLER );
			pController->binarySetFloat( COORD_MOVE_ACCEL_SN, (float) findTipSkewAcceleration_in/_1MEG_F );
			pController->binarySetFloat( COORD_MOVE_DECEL_SN, (float) findTipSkewDeceleration_in/_1MEG_F );
			pController->binarySetFloat( COORD_MOVE_STP_SN, (float) findTipSkewDeceleration_in/_1MEG_F );
			pController->binarySetFloat( COORD_MOVE_VEL_SN, (float) findTipSkewSpeed_in/_1MEG_F );
			pController->binarySetFloat( maxPosSn_in, (float) maxPosValNm_in/_1MEG_F );
			usleep(10000);
		} catch (std::exception &ex) {
			LOG_FATAL( m_threadName, axisNameStr_in.c_str(), "Exception while setting input values" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}

		float needlePos = 0.0;
		float findNeedleStatus = 0.0;
		if ((XYState != ComponentState::Mocked) && (status == STATUS_OK))
		{
			string error_msg = "";
			try {
				ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at( A_CONTROLLER );
				pController->monitor( positionSn_in, ACR7000::pvalue_type_t::pvalue_type_float, 100 );
				pController->monitor( findNeedleStatusSn_in, ACR7000::pvalue_type_t::pvalue_type_float, 100 );
				pController->monitor( routineId_in, ACR7000::pvalue_type_t::pvalue_type_float, 100 );
				usleep( 500000 );
				pController->runProgram( routineId_in, true );
				usleep( 50000 );
				pController->cancelMonitoring( routineId_in );
				needlePos = pController->inspectPValueFloat((ACR7000::pvalue_index_t)positionSn_in, when);
				findNeedleStatus = pController->inspectPValueFloat((ACR7000::pvalue_index_t)findNeedleStatusSn_in, when);
				pController->cancelMonitoring( positionSn_in );
				pController->cancelMonitoring( findNeedleStatusSn_in );
			} catch (MotionException &x) {
				error_msg = x.getEventDesc();
				status = STATUS_TIP_NOT_FOUND;
			} catch (std::exception &ex) {
				error_msg = "Setting P0 failed";
				status = STATUS_TIP_NOT_FOUND;
			}
			if (true)
			{
				stringstream msg;
				msg << "findNeedleStatus=" << findNeedleStatus;
				LOG_DEBUG( m_threadName, axisNameStr_in.c_str(), msg.str().c_str() );
			}
			if (status == STATUS_OK)
			{
				m_states_long[axisName_NM] = (long)(needlePos * _1MEG_F); /// Record where we stopped moving.
				triggerLocation_nm_out = m_states_long[axisName_NM];
				if (findNeedleStatus == 1.0)
				{
					error_msg = "Prog_0 reported failure";
					triggerLocation_nm_out = 0; /// We never triggered, so return a blatantly wrong value.
					status = STATUS_TIP_NOT_FOUND;
					stringstream msg;
					LOG_ERROR( m_threadName, axisNameStr_in.c_str(), "DECTION FAILED" );
				}
				if (findNeedleStatus == 0.0)
				{
					error_msg = "Prog_0 reported still running";
					triggerLocation_nm_out = 0; /// We never triggered, so return a blatantly wrong value.
					status = STATUS_TIP_NOT_FOUND;
					stringstream msg;
					LOG_ERROR( m_threadName, axisNameStr_in.c_str(), "DECTION NOT REPORTED" );
				}
			}
			else
			{
				LOG_FATAL(  m_threadName, axisNameStr_in.c_str(), error_msg.c_str() );
				status = STATUS_TIP_NOT_FOUND;
			}
		}
		if (true)
		{
			stringstream msg;
			msg << axisNameStr_in << "needlePos=" << needlePos;
			LOG_TRACE( m_threadName, axisNameStr_in.c_str(), msg.str().c_str() );
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR(  m_threadName, axisNameStr_in.c_str(), "XY not in good state" );
	}
	return status;
}


///----------------------------------------------------------------------
/// GetTip
///
/// Retrieves the specified tip on the Z tip holder.
/// Does not update tip count for container.
/// A. MoveAllTipsToSafeHeight()
/// B. a-MoveToXXStep(<Offset to tip number for tip type and tip_axis>)
/// C. a-MoveToYYStep(<Offset to tip number for tip type>)
/// D. If (tip_axis = Z) Then MoveToZTipPickForceNewton(<tip_type>) End If
/// E. If (tip_axis = ZZ) Then MoveToZZTipPickForceNewton(<tip_type>) End If
/// F.	MoveAllTipsToSafeHeight()
///
/// Note that step F. is not explicitly implemented. Functions
/// DoleOut_MoveToZTipPickForceNewton and DoleOut_MoveToZZTipPickForceNewton
/// move either Z or ZZ back to safe height before they return.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_GetTip()
{
	bool succeeded = true;
	int status = STATUS_OK;

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string(payloadObj);
		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "CxD", "FAILED to find payload object" );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	TipType tipType;
	ComponentType tipAxis = ComponentType::ComponentUnknown;
	unsigned long tipNumber = 1L;
	succeeded &= m_desHelper->ExtractTipParameters( m_threadName, m_commandMessageInObj, tipType, tipAxis, tipNumber );

	long z_safeHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_SAFE_HEIGHT, z_safeHeight_nm );
	long zz_safeHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_SAFE_HEIGHT, zz_safeHeight_nm );
	long zzz_safeHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZZ_SAFE_HEIGHT, zzz_safeHeight_nm );
	long yy_tipTrayTopEdgeToTipPath = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, YY_TIP_TRAY_TOP_EDGE_TO_TIP_PATH, yy_tipTrayTopEdgeToTipPath );
	
	if (!succeeded) LOG_ERROR( m_threadName, "temp", "Not succeeding" );

	long xx_TipToTipTrayRightEdge = 0L;
	long __TopOfYYStageHeight_nm = 0L;
	long __y_offset_from_xx_axis = 0L;
	if (tipAxis == ComponentType::ZAxis)
	{
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, XX_Z_TIP_TO_TIP_TRAY_RIGHT_EDGE, xx_TipToTipTrayRightEdge );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_TO_TOP_OF_YY_STAGE_HEIGHT, __TopOfYYStageHeight_nm );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_Y_OFFSET_FROM_XX_AXIS, __y_offset_from_xx_axis );
	}
	else if (tipAxis == ComponentType::ZZAxis)
	{
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, XX_ZZ_TIP_TO_TIP_TRAY_RIGHT_EDGE, xx_TipToTipTrayRightEdge );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_TO_TOP_OF_YY_STAGE_HEIGHT, __TopOfYYStageHeight_nm );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_Y_OFFSET_FROM_XX_AXIS, __y_offset_from_xx_axis );
	}
	else
	{
		stringstream msg;
		msg << "TipAxis(" << ComponentTypeEnumToStringName( tipAxis ) << ") is invalid";
		LOG_ERROR( m_threadName, "CxD", msg.str().c_str() );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}
	if (!succeeded)
	{
		LOG_ERROR( m_threadName, "temp", "Not succeeding" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}

	/// Move Z,Z, and ZZZ to safe height
	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "temp", "Moving Z to safety" );
		status = Implement_moveToZNm( z_safeHeight_nm );
	}
	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "temp", "Moving ZZ to safety" );
		status = Implement_moveToZZNm( zz_safeHeight_nm );
	}
	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "temp", "Moving ZZZ to safety" );
		status = Implement_moveToZZZNm( zzz_safeHeight_nm );
	}
	
	/// Move to XX_nm
	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "temp", "Moving XX to position" );
		/// XXOffest = XXAxis.xx_z[z]_tip_to_tip_tray_right_edge - TipType.first_tip_x - (modulus((<tipnumber> - 1)/TipType.tip_rack_column_count) * TipType.inter_tip_x)
		long xx_nm = xx_TipToTipTrayRightEdge - tipType.firstTipX - (((tipNumber - 1) % tipType.tipRackColumnCount) * tipType.interTipX);
		status = Implement_moveToXXNm( xx_nm );
	}
	
	/// Move to YY_nm
	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "temp", "Moving YY to position" );
		/// YYOffest = YYAxis.yy_tip_tray_top_edge_to_tip_path + TipType.first_tip_y + (truncate((<tipnumber> - 1)/TipType.tip_rack_column_count) * TipType.inter_tip_y) + <tipaxis>.z[z]_y_offset_from_xx_axis
		long yy_nm = yy_tipTrayTopEdgeToTipPath + tipType.firstTipY + (((tipNumber - 1) / tipType.tipRackColumnCount) * tipType.interTipY) + __y_offset_from_xx_axis;
		status = Implement_moveToYYNm( yy_nm );
	}

	if (tipAxis == ComponentType::ZAxis)
	{
		if (status == STATUS_OK)
		{
			/// Perform MoveToZTipPickForceNewton and move-to-safe-height.
			status = DoleOut_MoveToZTipPickForceNewton();
		}
		else
		{
			LOG_WARN( m_threadName, "z", "Returning Z to safety AFTER ERROR" );
			int stat = Implement_moveToZNm( z_safeHeight_nm ); /// Stat is local so the failing status will be returned.
		}
	}
	else if (tipAxis == ComponentType::ZZAxis)
	{
		if (status == STATUS_OK)
		{
			/// Perform MoveToZZTipPickForceNewton and move-to-safe-height.
			status = DoleOut_MoveToZZTipPickForceNewton();
		}
		else
		{
			LOG_WARN( m_threadName, "zz", "Returning ZZ to safety AFTER ERROR" );
			int stat = Implement_moveToZZNm( zz_safeHeight_nm ); /// Stat is local so the failing status will be returned.
		}
	}
	
	/// This status needs no payload.
	return status;
}

int CommandDolerThread::DoleOut_MoveTipToWasteLocation3()
{
	int status = STATUS_OK;
	/// Get a pointer to the payload object.
	ComponentType tip_axis = ComponentType::ComponentUnknown;
	string axisNameStr = "_z_";
	long z_safeHeight_nm = 0L;
	long zz_safeHeight_nm = 0L;
	long zzz_safeHeight_nm = 0L;
	long xx___z_tip_to_waste_3_position = 0L;
	long __z_to_waste_3_height = 0L;
	long tip_waste_3_safety_gap = 0L;
	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		// TODO: Remove this temp debug
		string payloadStr = json_object_to_json_string( payloadObj );
		cout << payloadStr;
		
		/// Read the tip_axis value from the payload to learn which axis to draw from.
		json_object * tipAxisObj = json_object_object_get( payloadObj, TIP_AXIS );
		if (tipAxisObj)
		{
			string tipAxisStr = json_object_get_string( tipAxisObj );
			tip_axis = StringNameToComponentTypeEnum( tipAxisStr.c_str() );

			if (tip_axis == ComponentType::ZAxis)
			{
				axisNameStr = "z";
				xx___z_tip_to_waste_3_position = json_object_get_int64( json_object_object_get( payloadObj, XX_Z_TIP_TO_WASTE_3_POSITION ) );
				__z_to_waste_3_height = json_object_get_int64( json_object_object_get( payloadObj, Z_TO_WASTE_3_HEIGHT ) );
				tip_waste_3_safety_gap = json_object_get_int64( json_object_object_get( payloadObj, TIP_WASTE_3_SAFETY_GAP ) );
			}
			else if (tip_axis == ComponentType::ZZAxis)
			{
				axisNameStr = "zz";
				xx___z_tip_to_waste_3_position = json_object_get_int64( json_object_object_get( payloadObj, XX_ZZ_TIP_TO_WASTE_3_POSITION ) );
				__z_to_waste_3_height = json_object_get_int64( json_object_object_get( payloadObj, ZZ_TO_WASTE_3_HEIGHT ) );
				tip_waste_3_safety_gap = json_object_get_int64( json_object_object_get( payloadObj, TIP_WASTE_3_SAFETY_GAP ) );
			}
			else if (tip_axis == ComponentType::ZZZAxis)
			{
				axisNameStr = "zzz";
				xx___z_tip_to_waste_3_position = json_object_get_int64( json_object_object_get( payloadObj, XX_ZZZ_TIP_TO_WASTE_3_POSITION ) );
				__z_to_waste_3_height = json_object_get_int64( json_object_object_get( payloadObj, ZZZ_TO_WASTE_3_HEIGHT ) );
				tip_waste_3_safety_gap = json_object_get_int64( json_object_object_get( payloadObj, TIP_WASTE_3_SAFETY_GAP ) );
			}
			else
			{
				stringstream msg;
				msg << "Invalid tip_axis|value=" << ComponentTypeEnumToStringName( tip_axis );
				status = STATUS_DEVICE_ERROR;
				LOG_ERROR( m_threadName, axisNameStr.c_str(), msg.str().c_str() );
			}
			cout << endl;
			cout << "tip_axis = " << ComponentTypeEnumToStringName( tip_axis ) << endl;
		}
		else
		{
			status = STATUS_NOT_FOUND;
			LOG_ERROR( m_threadName, axisNameStr.c_str(), "Failed to find tip_axis" );
		}
		
		/// Add the data needed by MoveAllTipsToSafeHeight.
		bool succeeded = true;
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_SAFE_HEIGHT, z_safeHeight_nm );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_SAFE_HEIGHT, zz_safeHeight_nm );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZZ_SAFE_HEIGHT, zzz_safeHeight_nm );
		if (!succeeded) LOG_ERROR( m_threadName, "move_tip", "Not succeeding" );
	}
	else
	{
		status = STATUS_NOT_FOUND;
		LOG_ERROR( m_threadName, axisNameStr.c_str(), "Failed to find payload" );
	}

	bool succeeded = false;
	TipType tipType = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );

	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "z", "Moving z to safe height" );
		status = Implement_moveToZNm( z_safeHeight_nm );

		LOG_TRACE( m_threadName, "zz", "Moving zz to safe height" );
		status = Implement_moveToZZNm( zz_safeHeight_nm );

		LOG_TRACE( m_threadName, "zzz", "Moving zzz to safe height" );
		status = Implement_moveToZZZNm( zzz_safeHeight_nm );
	}	

	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, axisNameStr.c_str(), "Moving XX to Z_waste_3_position" );
		status = Implement_moveToXXNm( xx___z_tip_to_waste_3_position );

		if (status == STATUS_OK)
		{
			if (tip_axis == ComponentType::ZAxis)
			{
				long z_target_pos = __z_to_waste_3_height + tipType.tipWorkingLength + tip_waste_3_safety_gap;
				LOG_TRACE( m_threadName, axisNameStr.c_str(), "Moving z to waste_3_position" );
				status = Implement_moveToZNm( z_target_pos );
			}
			if (tip_axis == ComponentType::ZZAxis)
			{
				long zz_target_pos = __z_to_waste_3_height + tipType.tipWorkingLength + tip_waste_3_safety_gap;
				LOG_TRACE( m_threadName, axisNameStr.c_str(), "Moving zz to waste_3_position" );
				status = Implement_moveToZZNm( zz_target_pos );
			}
			if (tip_axis == ComponentType::ZZZAxis)
			{
				long zzz_target_pos = __z_to_waste_3_height + tip_waste_3_safety_gap;
				LOG_TRACE( m_threadName, axisNameStr.c_str(), "Moving zzz to waste_3_position" );
				status = Implement_moveToZZZNm( zzz_target_pos );
			}
		}
	}

	/// This status needs no payload.
	return status;
}

int CommandDolerThread::DoleOut_MoveTipToWasteLocation2()
{
	int status = STATUS_OK;
	/// Get a pointer to the payload object.
	ComponentType tip_axis = ComponentType::ComponentUnknown;
	string axisNameStr = "_z_";
	long z_safeHeight_nm = 0L;
	long zz_safeHeight_nm = 0L;
	long zzz_safeHeight_nm = 0L;
	long xx___z_tip_to_waste_2_position = 0L;
	long __z_to_waste_2_height = 0L;
	long tip_waste_2_safety_gap = 0L;
	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		// TODO: Remove this temp debug
		string payloadStr = json_object_to_json_string( payloadObj );
		cout << payloadStr;

		/// Read the tip_axis value from the payload to learn which axis to draw from.
		json_object * tipAxisObj = json_object_object_get( payloadObj, TIP_AXIS );
		if (tipAxisObj)
		{
			string tipAxisStr = json_object_get_string( tipAxisObj );
			tip_axis = StringNameToComponentTypeEnum( tipAxisStr.c_str() );

			if (tip_axis == ComponentType::ZAxis)
			{
				axisNameStr = "z";
				xx___z_tip_to_waste_2_position = json_object_get_int64( json_object_object_get( payloadObj, XX_Z_TIP_TO_WASTE_2_POSITION ) );
				__z_to_waste_2_height = json_object_get_int64( json_object_object_get( payloadObj, Z_TO_WASTE_2_HEIGHT ) );
				tip_waste_2_safety_gap = json_object_get_int64( json_object_object_get( payloadObj, TIP_WASTE_2_SAFETY_GAP ) );
			}
			else if (tip_axis == ComponentType::ZZAxis)
			{
				axisNameStr = "zz";
				xx___z_tip_to_waste_2_position = json_object_get_int64( json_object_object_get( payloadObj, XX_ZZ_TIP_TO_WASTE_2_POSITION ) );
				__z_to_waste_2_height = json_object_get_int64( json_object_object_get( payloadObj, ZZ_TO_WASTE_2_HEIGHT ) );
				tip_waste_2_safety_gap = json_object_get_int64( json_object_object_get( payloadObj, TIP_WASTE_2_SAFETY_GAP ) );
			}
			else if (tip_axis == ComponentType::ZZZAxis)
			{
				axisNameStr = "zzz";
				xx___z_tip_to_waste_2_position = json_object_get_int64( json_object_object_get( payloadObj, XX_ZZZ_TIP_TO_WASTE_2_POSITION ) );
				__z_to_waste_2_height = json_object_get_int64( json_object_object_get( payloadObj, ZZZ_TO_WASTE_2_HEIGHT ) );
				tip_waste_2_safety_gap = json_object_get_int64( json_object_object_get( payloadObj, TIP_WASTE_2_SAFETY_GAP ) );
			}
			else
			{
				stringstream msg;
				msg << "Invalid tip_axis|value=" << ComponentTypeEnumToStringName( tip_axis );
				status = STATUS_DEVICE_ERROR;
				LOG_ERROR( m_threadName, axisNameStr.c_str(), msg.str().c_str() );
			}
			cout << endl;
			cout << "tip_axis = " << ComponentTypeEnumToStringName( tip_axis ) << endl;
		}
		else
		{
			status = STATUS_NOT_FOUND;
			LOG_ERROR( m_threadName, axisNameStr.c_str(), "Failed to find tip_axis" );
		}
		
		/// Add the data needed by MoveAllTipsToSafeHeight.
		bool succeeded = true;
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_SAFE_HEIGHT, z_safeHeight_nm );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_SAFE_HEIGHT, zz_safeHeight_nm );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZZ_SAFE_HEIGHT, zzz_safeHeight_nm );
		if (!succeeded) LOG_ERROR( m_threadName, "move_tip", "Not succeeding" );
	}
	else
	{
		status = STATUS_NOT_FOUND;
		LOG_ERROR( m_threadName, axisNameStr.c_str(), "Failed to find payload" );
	}

	bool succeeded = false;
	TipType tipType = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );

	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "z", "Moving z to safe height" );
		status = Implement_moveToZNm( z_safeHeight_nm );

		LOG_TRACE( m_threadName, "zz", "Moving zz to safe height" );
		status = Implement_moveToZZNm( zz_safeHeight_nm );

		LOG_TRACE( m_threadName, "zzz", "Moving zzz to safe height" );
		status = Implement_moveToZZZNm( zzz_safeHeight_nm );
	}	

	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, axisNameStr.c_str(), "Moving XX to Z_waste_2_position" );
		status = Implement_moveToXXNm( xx___z_tip_to_waste_2_position );

		if (status == STATUS_OK)
		{
			if (tip_axis == ComponentType::ZAxis)
			{
				long z_target_pos = __z_to_waste_2_height + tipType.tipWorkingLength + tip_waste_2_safety_gap;
				LOG_TRACE( m_threadName, axisNameStr.c_str(), "Moving z to waste_2_position" );
				status = Implement_moveToZNm( z_target_pos );
			}
			if (tip_axis == ComponentType::ZZAxis)
			{
				long zz_target_pos = __z_to_waste_2_height + tipType.tipWorkingLength + tip_waste_2_safety_gap;
				LOG_TRACE( m_threadName, axisNameStr.c_str(), "Moving zz to waste_2_position" );
				status = Implement_moveToZZNm( zz_target_pos );
			}
			if (tip_axis == ComponentType::ZZZAxis)
			{
				long zzz_target_pos = __z_to_waste_2_height + tip_waste_2_safety_gap;
				LOG_TRACE( m_threadName, axisNameStr.c_str(), "Moving zzz to waste_2_position" );
				status = Implement_moveToZZZNm( zzz_target_pos );
			}
		}
	}

	/// This status needs no payload.
	return status;
}

int CommandDolerThread::DoleOut_MoveTipToWasteLocation1()
{
	int status = STATUS_OK;
	/// Get a pointer to the payload object.
	ComponentType tip_axis = ComponentType::ComponentUnknown;
	string axisNameStr = "_z_";
	long z_safeHeight_nm = 0L;
	long zz_safeHeight_nm = 0L;
	long zzz_safeHeight_nm = 0L;
	long xx___z_tip_to_waste_1_position = 0L;
	long __z_to_waste_1_height = 0L;
	long tip_waste_1_safety_gap = 0L;
	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		// TODO: Remove this temp debug
		string payloadStr = json_object_to_json_string( payloadObj );
		cout << payloadStr;

		/// Read the tip_axis value from the payload to learn which axis to draw from.
		json_object * tipAxisObj = json_object_object_get( payloadObj, TIP_AXIS );
		if (tipAxisObj)
		{
			string tipAxisStr = json_object_get_string( tipAxisObj );
			tip_axis = StringNameToComponentTypeEnum( tipAxisStr.c_str() );

			cout << endl;
			cout << "tip_axis = " << ComponentTypeEnumToStringName( tip_axis ) << endl;

			if (tip_axis == ComponentType::ZAxis)
			{
				axisNameStr = "z";
				xx___z_tip_to_waste_1_position = json_object_get_int64( json_object_object_get( payloadObj, XX_Z_TIP_TO_WASTE_1_POSITION ) );
				__z_to_waste_1_height = json_object_get_int64( json_object_object_get( payloadObj, Z_TO_WASTE_1_HEIGHT ) );
				tip_waste_1_safety_gap = json_object_get_int64( json_object_object_get( payloadObj, TIP_WASTE_1_SAFETY_GAP ) );
			}
			else if (tip_axis == ComponentType::ZZAxis)
			{
				axisNameStr = "zz";
				xx___z_tip_to_waste_1_position = json_object_get_int64( json_object_object_get( payloadObj, XX_ZZ_TIP_TO_WASTE_1_POSITION ) );
				__z_to_waste_1_height = json_object_get_int64( json_object_object_get( payloadObj, ZZ_TO_WASTE_1_HEIGHT ) );
				tip_waste_1_safety_gap = json_object_get_int64( json_object_object_get( payloadObj, TIP_WASTE_1_SAFETY_GAP ) );
			}
			else if (tip_axis == ComponentType::ZZZAxis)
			{
				axisNameStr = "zzz";
				xx___z_tip_to_waste_1_position = json_object_get_int64( json_object_object_get( payloadObj, XX_ZZZ_TIP_TO_WASTE_1_POSITION ) );
				__z_to_waste_1_height = json_object_get_int64( json_object_object_get( payloadObj, ZZZ_TO_WASTE_1_HEIGHT ) );
				tip_waste_1_safety_gap = json_object_get_int64( json_object_object_get( payloadObj, TIP_WASTE_1_SAFETY_GAP ) );
			}
			else
			{
				stringstream msg;
				msg << "Invalid tip_axis|value=" << ComponentTypeEnumToStringName( tip_axis );
				status = STATUS_DEVICE_ERROR;
				LOG_ERROR( m_threadName, axisNameStr.c_str(), msg.str().c_str() );
			}
		}
		else
		{
			status = STATUS_NOT_FOUND;
			LOG_ERROR( m_threadName, axisNameStr.c_str(), "Failed to find tip_axis" );
		}

		/// Add the data needed by MoveAllTipsToSafeHeight.
		bool succeeded = true;
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_SAFE_HEIGHT, z_safeHeight_nm );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_SAFE_HEIGHT, zz_safeHeight_nm );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZZ_SAFE_HEIGHT, zzz_safeHeight_nm );
		if (!succeeded) LOG_ERROR( m_threadName, "move_tip", "Not succeeding" );
	}
	else
	{
		status = STATUS_NOT_FOUND;
		LOG_ERROR( m_threadName, axisNameStr.c_str(), "Failed to find payload" );
	}

	bool succeeded = false;
	TipType tipType = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );

	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "z", "Moving z to safe height" );
		status = Implement_moveToZNm( z_safeHeight_nm );

		LOG_TRACE( m_threadName, "zz", "Moving zz to safe height" );
		status = Implement_moveToZZNm( zz_safeHeight_nm );

		LOG_TRACE( m_threadName, "zzz", "Moving zzz to safe height" );
		status = Implement_moveToZZZNm( zzz_safeHeight_nm );
	}	

	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, axisNameStr.c_str(), "Moving XX to Z_waste_1_position" );
		status = Implement_moveToXXNm( xx___z_tip_to_waste_1_position );

		if (status == STATUS_OK)
		{
			if (tip_axis == ComponentType::ZAxis)
			{
				long z_target_pos = __z_to_waste_1_height + tipType.tipWorkingLength + tip_waste_1_safety_gap;
				LOG_TRACE( m_threadName, axisNameStr.c_str(), "Moving z to waste_1_position" );
				status = Implement_moveToZNm( z_target_pos );
			}
			else if (tip_axis == ComponentType::ZZAxis)
			{
				long zz_target_pos = __z_to_waste_1_height + tipType.tipWorkingLength + tip_waste_1_safety_gap;
				LOG_TRACE( m_threadName, axisNameStr.c_str(), "Moving zz to waste_1_position" );
				status = Implement_moveToZZNm( zz_target_pos );
			}
			else if (tip_axis == ComponentType::ZZZAxis)
			{
				long zzz_target_pos = __z_to_waste_1_height + tip_waste_1_safety_gap;
				LOG_TRACE( m_threadName, axisNameStr.c_str(), "Moving zzz to waste_1_position" );
				status = Implement_moveToZZZNm( zzz_target_pos );
			}
			else
			{
				stringstream msg;
				msg << "tip_axis=" << tip_axis << ", which is wrong.";
				LOG_ERROR( m_threadName, axisNameStr.c_str(), msg.str().c_str() );
			}
		}
		else
		{
			LOG_ERROR( m_threadName, axisNameStr.c_str(), "XX move erred." );
		}
	}
	else
	{
		LOG_ERROR( m_threadName, axisNameStr.c_str(), "Move Z,ZZ,ZZZ to safe height failed." );
	}

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_SeatTip
/// Moves the XX axis to seat the specified tip
/// A. MoveAllTipsToSafeHeight()
/// B. a-MoveToYYNM(<YYAxis.yy_key_to_tip_path + <tip_axis> y offset to xx axis>)
/// C. If (<tip_axis> = Z) Then 
///		a. a-MoveToXXNM(XXAxis.xx_z_tip_to_key_work_position)
///		b. MoveToZTipSeatingForceNewton(<tip_type>) 
///		c. End If
/// D. If (<tip_axis> = ZZ) Then 
///		a. a-MoveToXXNM(XXAxis.xx_zz_tip_to_key_work_position)
///		b. MoveToZZTipSeatingForceNewton(<tip_type>) 
///		c. End If
/// E. MoveAllTipsToSafeHeight()
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_SeatTip()
{
	bool succeeded = true;
	int status = STATUS_OK;

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string(payloadObj);
		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "CxD", "FAILED to find payload object" );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	TipType tipType = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );
	if (!succeeded)
	{
		status = STATUS_CMD_MSG_DATA_MISSING;
	}
	ComponentType tip_axis = ComponentType::ComponentUnknown;
	
	json_object * tipAxisObj = json_object_object_get( payloadObj, TIP_AXIS );
	if (tipAxisObj)
	{
		string tipAxisStr = json_object_get_string( tipAxisObj );
		tip_axis = StringNameToComponentTypeEnum( tipAxisStr.c_str() );
	}
	else
	{
		succeeded = false;
	}
	
	long z_safeHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_SAFE_HEIGHT, z_safeHeight_nm );
	long zz_safeHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_SAFE_HEIGHT, zz_safeHeight_nm );
	long zzz_safeHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZZ_SAFE_HEIGHT, zzz_safeHeight_nm );
	long yy_keyToTipPath = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, YY_KEY_TO_TIP_PATH, yy_keyToTipPath );

	long xx__tipToKeyWorkPosition = 0L;
	long __yOffsetFromXXAxis = 0L;
	if (tip_axis == ComponentType::ZAxis)
	{
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, XX_Z_TIP_TO_KEY_WORK_POSITION, xx__tipToKeyWorkPosition );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_Y_OFFSET_FROM_XX_AXIS, __yOffsetFromXXAxis );
	}
	else if (tip_axis == ComponentType::ZZAxis)
	{
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, XX_ZZ_TIP_TO_KEY_WORK_POSITION, xx__tipToKeyWorkPosition );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_Y_OFFSET_FROM_XX_AXIS, __yOffsetFromXXAxis );
	}
	else
	{
		stringstream msg;
		msg << "TipAxis(" << ComponentTypeEnumToStringName( tip_axis ) << ") is invalid";
		LOG_ERROR( m_threadName, "CxD", msg.str().c_str() );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}
	if (!succeeded)
	{
		LOG_ERROR( m_threadName, "temp", "Not succeeding" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}

	/// Move Z,Z, and ZZZ to safe height
	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "temp", "Moving Z to safety" );
		status = Implement_moveToZNm( z_safeHeight_nm );
	}
	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "temp", "Moving ZZ to safety" );
		status = Implement_moveToZZNm( zz_safeHeight_nm );
	}
	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "temp", "Moving ZZZ to safety" );
		status = Implement_moveToZZZNm( zzz_safeHeight_nm );
	}

	/// Move to YY_nm
	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "temp", "Moving YY to position" );
		/// YYOffest = YYAxis.yy_key_to_tip_path + <tipaxis>.z[z]_y_offset_from_xx_axis
		long yy_nm = yy_keyToTipPath + __yOffsetFromXXAxis;
		status = Implement_moveToYYNm( yy_nm );
	}

	/// Move to XX_nm
	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "temp", "Moving XX to position" );
		/// XXOffest = XXAxis.xx_z[z]_tip_to_key_work_position
		status = Implement_moveToXXNm( xx__tipToKeyWorkPosition );
	}
	
	if (tip_axis == ComponentType::ZAxis)
	{
		if (status == STATUS_OK)
		{
			/// Perform DoleOut_MoveToZTipSeatingForceNewton and move-to-safe-height.
			status = DoleOut_MoveToZTipSeatingForceNewton();
		}
		else
		{
			LOG_WARN( m_threadName, "z", "Returning Z to safety AFTER ERROR" );
			int stat = Implement_moveToZNm( z_safeHeight_nm ); /// Stat is local so the failing status will be returned.
		}
	}
	else if (tip_axis == ComponentType::ZZAxis)
	{
		if (status == STATUS_OK)
		{
			/// Perform DoleOut_MoveToZZTipSeatingForceNewton and move-to-safe-height.
			status = DoleOut_MoveToZZTipSeatingForceNewton();
		}
		else
		{
			LOG_WARN( m_threadName, "zz", "Returning ZZ to safety AFTER ERROR" );
			int stat = Implement_moveToZZNm( zz_safeHeight_nm ); /// Stat is local so the failing status will be returned.
		}
	}

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_GetTipSkew
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_GetTipSkew()
{
	int status = STATUS_OK;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "skew", "DoleOut_GetTipSkew() sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		// TODO: Delete this temporary debug.
		string payloadStr = json_object_to_json_string( payloadObj );
//		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "skew", "Payload is missing" );
		return STATUS_CMD_MSG_DATA_MISSING;			/// EARLY RETURN!	EARLY RETURN!
	}

	bool succeeded = true;
	ComponentType tipAxis = ComponentType::ComponentUnknown;
	long z_safe_height_nm = 0L;
	long zz_safe_height_nm = 0L;
	long zzz_safe_height_nm = 0L;
	long x_optical_sensor_to_tip_skew_position = 0L;
	long y_optical_sensor_to_tip_skew_position = 0L;
	long offset_from_center_nm = 2000000L; /// 2mm.
	long tip_skew_safety_gap = 0L;
	long xx_z__tip_to_tip_skew_position = 0L;
	long z__to_tip_skew_sensor_height = 0L;
	long __y_offset_from_xx_axis = 0L;
	long x__tip_skew_base_position = 0L;
	long y__tip_skew_base_position = 0L;
	struct timespec when;

	/// Get a pointer to the payload object.
	if (payloadObj)
	{
		/// Read the tip_axis value from the payload to learn which axis to draw from.
		json_object * tipAxisObj = json_object_object_get( payloadObj, TIP_AXIS );
		if (tipAxisObj)
		{
			string tipAxisStr = json_object_get_string( tipAxisObj );
			tipAxis = StringNameToComponentTypeEnum( tipAxisStr.c_str() );
			succeeded = ((tipAxis == ComponentType::ZAxis) || (tipAxis == ComponentType::ZZAxis));
		}
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, X_OPTICAL_SENSOR_TO_TIP_SKEW_POSITION, x_optical_sensor_to_tip_skew_position );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Y_OPTICAL_SENSOR_TO_TIP_SKEW_POSITION, y_optical_sensor_to_tip_skew_position );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_SKEW_SAFETY_GAP, tip_skew_safety_gap );

		if (tipAxis == ComponentType::ZAxis)
		{
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, XX_Z_TIP_TO_TIP_SKEW_POSITION, xx_z__tip_to_tip_skew_position );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_TO_TIP_SKEW_SENSOR_HEIGHT, z__to_tip_skew_sensor_height );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_Y_OFFSET_FROM_XX_AXIS, __y_offset_from_xx_axis );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, X_Z_TIP_SKEW_BASE_POSITION, x__tip_skew_base_position );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Y_Z_TIP_SKEW_BASE_POSITION, y__tip_skew_base_position );
			stringstream msg;
			msg << "Using X_Z_TIP_SKEW_BASE_POSITION=" << x__tip_skew_base_position << "|Y_Z_TIP_SKEW_BASE_POSITION=" << y__tip_skew_base_position;
			LOG_TRACE( m_threadName, "skew", msg.str().c_str() );
		}
		else if (tipAxis == ComponentType::ZZAxis)
		{
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, XX_ZZ_TIP_TO_TIP_SKEW_POSITION, xx_z__tip_to_tip_skew_position );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_TO_TIP_SKEW_SENSOR_HEIGHT, z__to_tip_skew_sensor_height );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_Y_OFFSET_FROM_XX_AXIS, __y_offset_from_xx_axis );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, X_ZZ_TIP_SKEW_BASE_POSITION, x__tip_skew_base_position );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Y_ZZ_TIP_SKEW_BASE_POSITION, y__tip_skew_base_position );
			stringstream msg;
			msg << "Using X_ZZ_TIP_SKEW_BASE_POSITION=" << x__tip_skew_base_position << "|Y_ZZ_TIP_SKEW_BASE_POSITION=" << y__tip_skew_base_position;
			LOG_TRACE( m_threadName, "skew", msg.str().c_str() );
		}
		else
		{
			stringstream msg;
			msg << "TipAxis(" << ComponentTypeEnumToStringName( tipAxis ) << ") is invalid";
			LOG_ERROR( m_threadName, "skew", msg.str().c_str() );
			return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
		}

		if (!succeeded)
		{
			LOG_ERROR( m_threadName, "skew", "Failed to some payload content" );
			return STATUS_CMD_MSG_DATA_MISSING;			/// EARLY RETURN!	EARLY RETURN!
		}
	}
	TipType tipType = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );

	/// A. MoveAllTipsToSafeHeight()
	status = DoleOut_MoveAllTipsToSafeHeight();

	/// B. MoveToXYNM(XYAxis.x_optical_sensor_to_tip_skew_position, XYAxis.y_optical_sensor_to_tip_skew_position + <tip_axis>_y_offset_to_xx_axis)
	if (status == STATUS_OK)
	{
		/// Move to X-start-pos
		status = Implement_moveToXYNm( x_optical_sensor_to_tip_skew_position + offset_from_center_nm,
									   y_optical_sensor_to_tip_skew_position + __y_offset_from_xx_axis );
	}
	
	/// E/F a. MoveToXXNM(XXAxis.xx_z(z)_tip_to_tip_skew_position)
	if (status == STATUS_OK)
	{
		status = Implement_moveToXXNm( xx_z__tip_to_tip_skew_position );
	}
	
	/// E/F b. MoveToZ(Z)NM(Z(Z)Axis.z(z)_to_tip_skew_sensor_height + tip_working_length + Z(Z)Axis.tip_skew_safety_gap)
	if (status == STATUS_OK)
	{
		if (tipAxis == ComponentType::ZAxis)
		{
			status = Implement_moveToZNm( z__to_tip_skew_sensor_height + tipType.tipWorkingLength + tip_skew_safety_gap );
		}
		if (tipAxis == ComponentType::ZZAxis)
		{
			status = Implement_moveToZZNm( z__to_tip_skew_sensor_height + tipType.tipWorkingLength + tip_skew_safety_gap );
		}
	}

	/// G. <x_skew_nm> = MoveXToTipSkewTrigger (this is a prog0 command)
	if (status == STATUS_OK)
	{
		status = DoleOut_moveXToTipSkewTrigger( false );
	}
	long x_position_measured_nm = 0L;
	if (status == STATUS_OK)
	{
		x_position_measured_nm = m_states_long[X_NM];
	}

	/// H. MoveToXYNM(XYAxis.x_optical_sensor_to_tip_skew_position, XYAxis.y_optical_sensor_to_tip_skew_position + <tip_axis>_y_offset_to_xx_axis)
	if (status == STATUS_OK)
	{
		/// Return to X-start-pos
		status = Implement_moveToXYNm( x_optical_sensor_to_tip_skew_position + offset_from_center_nm,
									   y_optical_sensor_to_tip_skew_position + __y_offset_from_xx_axis );
	}
	if (status == STATUS_OK)
	{
		/// Move to Y-start-pos
		status = Implement_moveToXYNm( x_optical_sensor_to_tip_skew_position,
									   y_optical_sensor_to_tip_skew_position + __y_offset_from_xx_axis + offset_from_center_nm );
	}

	/// D. SetYTo(XYAxes.y_find_tip_skew_speed, XYAxes.y_find_tip_skew_acceleration, XYAxes.y_find_tip_skew_deceleration)
	if (status == STATUS_OK)
	{
		status = DoleOut_moveYToTipSkewTrigger( false );
	}
	long y_position_measured_nm = 0L;
	if (status == STATUS_OK)
	{
		y_position_measured_nm = m_states_long[Y_NM];
	}

	/// L. MoveAllTipsToSafeHeight()
	if (status == STATUS_OK)
	{
		status = CommandDolerThread::DoleOut_MoveAllTipsToSafeHeight();
	}
	else
	{
		LOG_WARN( m_threadName, "skew", "Returning to safe heights AFTER ERROR" );
		int stat = CommandDolerThread::DoleOut_MoveAllTipsToSafeHeight();
	}

	/// Populate the status payload
	long x_skew_nm = 0L;
	if (true)
	{
		x_skew_nm = x__tip_skew_base_position - x_position_measured_nm;
//		x_skew_nm = 0L; // TODO: REMOVE THIS TEMPORARY CHANGE!
		stringstream msg;
		msg << "Measured X_nm position at sensor detection = " << x_position_measured_nm;
		msg << "|x_skew_nm=" << x_skew_nm;
		LOG_DEBUG( m_threadName, "x", msg.str().c_str() );
	}
	int result = json_object_object_add( m_statusPayloadOutObj, X_SKEW_NM, json_object_new_int64( x_skew_nm ) );
	if ((status == STATUS_OK) && (result != 0))
	{
		/// populating the payload failed and nothing else did
		status = STATUS_CMD_MSG_ERROR;
	}

	long y_skew_nm = 0L;
	if (true)
	{
		y_skew_nm = y_position_measured_nm - y__tip_skew_base_position;
//		y_skew_nm = 0L; // TODO: REMOVE THIS TEMPORARY CHANGE!
		stringstream msg;
		msg << "Measured Y_nm position at sensor detection = " << y_position_measured_nm;
		msg << "|y_skew_nm=" << y_skew_nm;
		LOG_DEBUG( m_threadName, "y", msg.str().c_str() );
	}
	result = json_object_object_add( m_statusPayloadOutObj, Y_SKEW_NM, json_object_new_int64( y_skew_nm ) );
	if ((status == STATUS_OK) && (result != 0))
	{
		/// populating the payload failed and nothing else did
		status = STATUS_CMD_MSG_ERROR;
	}
	
	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_SetWorkingTip
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_SetWorkingTip()
{
	int status = STATUS_OK;
	long xx_z___tip_to_tip_working_position = 0L;
	ComponentType tipAxis = ComponentType::ComponentUnknown;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "xx", "SetWorkingTip sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState XXState = g_XXState.load( std::memory_order_acquire );

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		/// Read the tip_axis value from the payload to learn which axis to draw from.
		json_object * tipAxisObj = json_object_object_get( payloadObj, TIP_AXIS );
		if (tipAxisObj)
		{
			string tipAxisStr = json_object_get_string( tipAxisObj );
			tipAxis = StringNameToComponentTypeEnum( tipAxisStr.c_str() );
			if ((tipAxis == ComponentType::ZAxis) || (tipAxis == ComponentType::ZZAxis) || (tipAxis == ComponentType::ZZZAxis))
			{ }
			else
			{
				status = STATUS_CMD_MSG_ERROR;
				stringstream msg;
				msg << "Invalid tipType=" << ComponentTypeEnumToStringName( tipAxis );
				LOG_ERROR( m_threadName, "xx", msg.str().c_str() );
			}
		}
		else
		{
			status = STATUS_CMD_MSG_DATA_MISSING;
			LOG_ERROR( m_threadName, "xx", "Failed to find tipAxis" );
		}
	}
	else
	{
		status = STATUS_CMD_MSG_DATA_MISSING;
		LOG_ERROR( m_threadName, "xx", "Failed to find payload" );
	}
	
	if (status == STATUS_OK)
	{
		if (tipAxis == ComponentType::ZAxis)
		{
			xx_z___tip_to_tip_working_position = json_object_get_int64( json_object_object_get( payloadObj, XX_Z_TIP_TO_TIP_WORKING_POSITION ) );
		}
		else if (tipAxis == ComponentType::ZZAxis)
		{
			xx_z___tip_to_tip_working_position = json_object_get_int64( json_object_object_get( payloadObj, XX_ZZ_TIP_TO_TIP_WORKING_POSITION ) );
		}
		else if (tipAxis == ComponentType::ZZZAxis)
		{
			xx_z___tip_to_tip_working_position = json_object_get_int64( json_object_object_get( payloadObj, XX_ZZZ_TIP_TO_TIP_WORKING_POSITION ) );
		}
		else
		{
			status = STATUS_CMD_MSG_ERROR;
			LOG_ERROR( m_threadName, "xx", "Failed to find a tip_to_tip_working_position" );
		}
	}

	if (status == STATUS_OK)
	{
		if (XXState == ComponentState::Mocked)
		{
			m_states_long[XX_NM] = xx_z___tip_to_tip_working_position;
		}
		else if (XXState == ComponentState::Good)
		{
// TODO: Revisit this. Commented out at Nate's request. Temporary? Or for ever?
//			status = CommandDolerThread::DoleOut_MoveAllTipsToSafeHeight();
			if (status == STATUS_OK)
			{
				status = Implement_moveToXXNm(xx_z___tip_to_tip_working_position);
			}
		}
		else
		{
			status = STATUS_DEVICE_ERROR;
			LOG_ERROR( m_threadName, "xx", "Wrong XX state" );
		}
	}

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// RemoveTip
///
/// Removes the specified tip from its tip axis
/// A. MoveAllTipsToSafeHeight() at set_speed
/// B. a-MoveToYYNM(<YYAxis.yy_key_to_tip_path + <tip_axis< y offset to xx axis>>)
/// C. If (<tip_axis> = Z) Then 
/// 	a.	a-MoveToXXNM(XXAxis.xx_z_tip_to_key_entry_position) at set_speed
///		b.	MoveToZNM(ZAxis.z_to_top_of_seating_key_height)
/// 	c.	ADPDevice.EjectTip()
/// D. If (<tip_axis> = ZZ) Then 
/// 	a.	a-MoveToXXNM(XXAxis.xx_zz_tip_to_key_entry_position) at set_speed
/// 	b.	MoveToZZNM(ZZAxis.zz_through_key_height - (tip_total_length - tip_working_length) - ZZaxis.tip_removal_slowdown_offset) at set_speed
/// 	c.	MoveToXXNM(XXAxis,xx_zz_tip_to_key_work_position) at set_speed
/// 	d.	MoveToZZNM(ZZAxis.zz_to_top_of_seating_key_height) ZZAxis.tip_seating_speed
/// E. MoveAllTipsToSafeHeight()
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_RemoveTip()
{
	bool succeeded = true;
	int status = STATUS_OK;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string(payloadObj);
		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "CxD", "FAILED to find payload object" );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	TipType tipType = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );
	/// Read the tip_axis value from the payload to learn which axis to draw from.
	ComponentType tipAxis = ComponentType::ComponentUnknown;
	json_object * tipAxisObj = json_object_object_get( payloadObj, TIP_AXIS );
	if (tipAxisObj)
	{
		string tipAxisStr = json_object_get_string( tipAxisObj );
		tipAxis = StringNameToComponentTypeEnum( tipAxisStr.c_str() );
	}

	long z_safeHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_SAFE_HEIGHT, z_safeHeight_nm );
	long zz_safeHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_SAFE_HEIGHT, zz_safeHeight_nm );
	long zzz_safeHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZZ_SAFE_HEIGHT, zzz_safeHeight_nm );
	long yy_keyToTipPath = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, YY_KEY_TO_TIP_PATH, yy_keyToTipPath );
	long tip_removal_slowdown_offset = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_REMOVAL_SLOWDOWN_OFFSET, tip_removal_slowdown_offset );

	long xx__tipToKeyEntryPosition = 0L;
	long xx__tipToKeyWorkPosition = 0L;
	long __throughKeyHeight = 0L;
	long __toTopOfSeatingKeyHeight = 0L;
	long __yOffsetFromXXAxis = 0L;
	long __tipSeatingSpeed = 0L;
	string pumpVendor = "";
	string pumpModel = "";

	if (tipAxis == ComponentType::ZAxis)
	{
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, XX_Z_TIP_TO_KEY_ENTRY_POSITION, xx__tipToKeyEntryPosition );
//		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, XX_Z_TIP_TO_KEY_WORK_POSITION, xx__tipToKeyWorkPosition );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_THROUGH_KEY_HEIGHT, __throughKeyHeight );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_TO_TOP_OF_SEATING_KEY_HEIGHT, __toTopOfSeatingKeyHeight );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_Y_OFFSET_FROM_XX_AXIS, __yOffsetFromXXAxis );
		succeeded &= m_desHelper->ExtractStringValue( m_threadName, payloadObj, PUMP_VENDOR, pumpVendor );
		succeeded &= m_desHelper->ExtractStringValue( m_threadName, payloadObj, PUMP_MODEL, pumpModel );
		if (!succeeded) LOG_ERROR( m_threadName, "z", "Not succeeding" );
	}
	else if (tipAxis == ComponentType::ZZAxis)
	{
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, XX_ZZ_TIP_TO_KEY_ENTRY_POSITION, xx__tipToKeyEntryPosition );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, XX_ZZ_TIP_TO_KEY_WORK_POSITION, xx__tipToKeyWorkPosition );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_THROUGH_KEY_HEIGHT, __throughKeyHeight );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_TO_TOP_OF_SEATING_KEY_HEIGHT, __toTopOfSeatingKeyHeight );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_Y_OFFSET_FROM_XX_AXIS, __yOffsetFromXXAxis );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_SEATING_SPEED, __tipSeatingSpeed );
		if (!succeeded) LOG_ERROR( m_threadName, "zz", "Not succeeding" );
	}
	else
	{
		stringstream msg;
		msg << "TipAxis(" << ComponentTypeEnumToStringName( tipAxis ) << ") is invalid";
		LOG_ERROR( m_threadName, "CxD", msg.str().c_str() );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}
	if (!succeeded)
	{
		LOG_ERROR( m_threadName, "temp", "Not succeeding" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}

	/// A. Move Z,Z, and ZZZ to safe height
	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "z", "Moving Z to safety" );
		status = Implement_moveToZNm( z_safeHeight_nm );
	}
	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "zz", "Moving ZZ to safety" );
		status = Implement_moveToZZNm( zz_safeHeight_nm );
	}
	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "zzz", "Moving ZZZ to safety" );
		status = Implement_moveToZZZNm( zzz_safeHeight_nm );
	}

	/// B. MoveToYYNM( yy_key_to_tip_path + z(z)_y_offset_to_xx_axis )
	long yy_nm = yy_keyToTipPath + __yOffsetFromXXAxis;
	if (status == STATUS_OK)
	{
		stringstream msg;
		msg << "Moving YY to " << yy_nm;
		LOG_TRACE( m_threadName, "yy", msg.str().c_str() );
		status = Implement_moveToYYNm( yy_nm );
	}

	/// C/D. a.
	if (status == STATUS_OK)
	{
		stringstream msg;
		msg << "Moving XX to xx_zz_tip_to_key_entry_position " << xx__tipToKeyEntryPosition;
		LOG_TRACE( m_threadName, "xx", msg.str().c_str() );
		status = Implement_moveToXXNm( xx__tipToKeyEntryPosition );
	}

	if (tipAxis == ComponentType::ZAxis) /// C.
	{
		/// C. b. MoveToZNM(ZAxis.z_to_top_of_seating_key_height) at ZAxis.set_speed
		if (status == STATUS_OK)
		{
			stringstream msg;
			msg << "Moving Z to z_to_top_of_seating_key_height " << __toTopOfSeatingKeyHeight;
			LOG_TRACE( m_threadName, "z", msg.str().c_str() );
			status = Implement_moveToZNm( __toTopOfSeatingKeyHeight );
		}
		/// C. c.
		if (status == STATUS_OK)
		{
			if (DesHelper::IsTecanAdpDetect( pumpVendor, pumpModel ))
			{
				if (g_PickingPump1State.load( std::memory_order_acquire ) ==  ComponentState::Mocked)
				{
					LOG_TRACE( m_threadName, "z", "Mocking the ADP library's EjectTip() command." );
				}
				else
				{
					/// Lock g_SerialPortMutex (in the CxD thread) while ejecting a tip.
					unique_lock<mutex> lock( g_SerialPortMutex ); /// IMPORTANT! The Pump thread and CxD thread share this.
					LOG_TRACE( m_threadName, "z", "Calling the ADP library's EjectTip() command." );
					/// Call to the ADP library's EjectTip() command.
					int stdError = 0;
					BoxOfErrors box;
					int count = 0;
					do {
						if (count > 0) LOG_WARN( m_threadName, "pick", "Retrying EjectTip" );
						status = m_tecanADP->EjectTip( m_threadName, stdError );
						if (stdError == cmERROR_CODE_TIP_LOST_OR_NOT_PRESENT)
						{
							status = STATUS_OK;
						}
						usleep( 10 * m_tecanADP->StandardWait_uS );
						count++;
						if (status == STATUS_OK) count = 5;
					} while (count < 5); // TODO: What is a good number of tries?

					/// Wait for EjectTipAtInitialization to end.
					if (status == STATUS_OK)
					{
						status = m_tecanADP->WaitForNotBusy( m_threadName, __func__ );
					}

					/// Check for liquid in the tip.
					float currentPickingPumpVolume = g_PickingPumpVolume.load( std::memory_order_acquire );
					if (currentPickingPumpVolume > 0.0)
					{
						stringstream msg;
						msg << "ADP pump current volume is " <<  currentPickingPumpVolume;
						LOG_WARN( m_threadName, "pick", msg.str().c_str() );
						// status = STATUS_EJECT_FLUID_NOT_ALLOWED;
					}

					if (status != STATUS_OK)
					{
						LOG_ERROR( m_threadName, "pick", "Tecan ADP failed to eject tip." );
						/// Send an alert.
						stringstream topic_stream;
						topic_stream << TOPIC_ES << CHANNEL_ALERT;

						/// Collect fields from the command's header for use in the alert.
						std::string messageType = "";
						int commandGroupId = 0;
						int commandId = 0;
						std::string sessionId = "";
						std::string machineId = "";
						bool success =   m_desHelper->SplitCommand( m_commandMessageInObj,
																	&messageType,
																	nullptr, // &transportVersion,
																	&commandGroupId,
																	&commandId,
																	&sessionId,
																	&machineId );

						json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
																			commandGroupId,
																			commandId,
																			sessionId, // Or topic_stream.str()?
																			ComponentType::XYAxes,
																			STATUS_AXIS_X_FAULTED,
																			false ); /// thisIsCausingHold
						bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
						return status;	// EARLY RETURN!!	EARLY RETURN!!
					}
				}
			}
			else /// The picking pump is not a Tecan ADP
			{
				stringstream msg;
				msg << "Unknown picking pump type! vendor=" << pumpVendor << "|model=" << pumpModel;
				LOG_ERROR( m_threadName, "pick", msg.str().c_str() );
			}
		}

	}
	else if (tipAxis == ComponentType::ZZAxis) /// D.
	{
		/// D. b.
		if (status == STATUS_OK)
		{
			long __nm = __throughKeyHeight - (tipType.tipTotalLength - tipType.tipWorkingLength) - tip_removal_slowdown_offset;
			if (true)
			{
				stringstream msg;
				msg << "zz_throughKeyHeight=" << __throughKeyHeight << "|tipType.tipTotalLength=" << tipType.tipTotalLength << "|tipType.tipWorkingLength=" << tipType.tipWorkingLength;
				msg << "|tip_removal_slowdown_offset=" << tip_removal_slowdown_offset << "|zz_nm=" << __nm;
				LOG_TRACE( m_threadName, "temp", msg.str().c_str() );
			}
			if (true)
			{
				stringstream msg;
				msg << "Moving ZZ to " << __nm;
				LOG_TRACE( m_threadName, "zz", msg.str().c_str() );
				status = Implement_moveToZZNm( __nm );
			}
		}
		/// D. c.
		if (status == STATUS_OK)
		{
			LOG_TRACE( m_threadName, "xx", "Moving XX to xx_zz_tip_to_key_work_position" );
			stringstream msg;
			msg << "Moving XX to xx_zz_tip_to_key_work_position " << xx__tipToKeyWorkPosition;
			LOG_TRACE( m_threadName, "xx", msg.str().c_str() );
			status = Implement_moveToXXNm( xx__tipToKeyWorkPosition );
		}
		/// D. d & e. MoveToZZNM(ZZAxis.zz_through_key_height) at ZZAxis.tip_seating_speed
		LOG_TRACE( m_threadName, "zz", "Steps D.(d,e,f) are commented out." );
		/*
		if (status == STATUS_OK)
		{
			stringstream msg;
			msg << "Moving ZZ to zz_through_key_height " << __throughKeyHeight;
			LOG_TRACE( m_threadName, "zz", msg.str().c_str() );
			status = Implement_moveToZZNm( __throughKeyHeight, __tipSeatingSpeed );
		}
		*/
	}

	/// E. Return Z or ZZ to safe height.
	if (tipAxis == ComponentType::ZAxis)
	{
		if (status == STATUS_OK)
		{
			LOG_TRACE( m_threadName, "z", "Returning Z to safe_height" );
			status = Implement_moveToZNm( z_safeHeight_nm );
		}
		else
		{
			LOG_WARN( m_threadName, "z", "Returning Z to safety AFTER ERROR" );
			int stat = Implement_moveToZNm( z_safeHeight_nm ); /// Stat is local so the failing status will be returned.
		}
	}
	if (tipAxis == ComponentType::ZZAxis)
	{
		if (status == STATUS_OK)
		{
			LOG_TRACE( m_threadName, "zz", "Returning ZZ to safe_height" );
			status = Implement_moveToZZNm( zz_safeHeight_nm );
		}
		else
		{
			LOG_WARN( m_threadName, "zz", "Returning ZZ to safety AFTER ERROR" );
			int stat = Implement_moveToZZNm( zz_safeHeight_nm ); /// Stat is local so the failing status will be returned.
		}
	}

	/// This status needs no payload.
	return status;
}

// TODO: Pick up here. Differentiate these and stuff.
int CommandDolerThread::DoleOut_SetOpticalColumnTurretPosition()
{
	bool succeeded = false;
	ComponentType tipAxis = ComponentType::ComponentUnknown;
	int status = STATUS_OK;
	unsigned long safe_magnification_change_height = 0L;
	unsigned long turret_position = 0L;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		json_object * smcHeight = json_object_object_get( payloadObj, SAFE_MAGNIFICATION_CHANGE_HEIGHT );
		if (smcHeight)
		{
			safe_magnification_change_height = json_object_get_uint64( smcHeight );
		}
		else
		{
			LOG_ERROR( m_threadName, "oc", "failed to get safe_magnification_change_height");
			status = STATUS_DEVICE_ERROR;
		}

		json_object * tPos = json_object_object_get( payloadObj, TURRET_POSITION );
		if (tPos)
		{
			turret_position = json_object_get_int64( tPos );
		}
		else
		{
			LOG_ERROR( m_threadName, "oc", "failed to get turret_position");
			status = STATUS_DEVICE_ERROR;
		}
		
		ComponentState OCState = g_OpticalColumnState.load( std::memory_order_acquire );
		if (OCState == ComponentState::Mocked)
		{
			m_states_long[TURRET_POSITION] = turret_position;
			return status;
		}

		FreezeWorkInProgress();
		status = Implement_SetOpticalColumnTurretPosition( m_commandMessageInObj,
														   safe_magnification_change_height,
														   turret_position );
	}
	else
	{
		LOG_ERROR( m_threadName, "oc", "failed to get payload");
		status = STATUS_DEVICE_ERROR;
	}
	/// This status needs no payload.
	return status;
}

/// Helper function for SetOpticalColumnTurretPosition command. Issues 3 commands to the optical column:
/// MoveToFocusNM( safeFocusNmDuringTurretChange )
/// RotateTurretToPosition( turretPosition )
/// Do NOT MoveToFocusNM( workingFocusNm ). Leave it at safeFocusNmDuringTurretChange instead.
/// Also updates a state variable tracking the current value of Focus_NM.
int CommandDolerThread::Implement_SetOpticalColumnTurretPosition(
	json_object * templateMessageObj,
	unsigned long safeFocusNmDuringTurretChange,
	unsigned long turretPosition )
{
	/// Mock the move if appropriate.
	ComponentState opticalColumnState = g_OpticalColumnState.load( std::memory_order_acquire );
	if (opticalColumnState == ComponentState::Mocked)
	{
		m_states_long[FOCUS_POSITION] = safeFocusNmDuringTurretChange;
		m_states_long[ACTIVE_TURRET_POSITION] = turretPosition;
		return STATUS_OK; // EARLY RETURN!	EARLY RETURN!
	}

	/// Move focus to safe position.
	FreezeWorkInProgress();
	int status = Implement_moveToFocusNM( templateMessageObj, safeFocusNmDuringTurretChange );

	/// Move to turret position.
	if (status == STATUS_OK)
	{
		HardwareType opticalColumnType = g_ComponentHardwareTypeArray[(int)ComponentType::OpticalColumn];
		if (opticalColumnType == HardwareType::IX85)
		{
			status = Implement_SetOlympus_IX85_TurretPosition( turretPosition );
		}
		else if (opticalColumnType == HardwareType::LS620)
		{
			status = Implement_SetEtaluma_LS620_TurretPosition( turretPosition );
		}
		else
		{
			LOG_FATAL( m_threadName, OPTICAL_COLUMN, "Configured to unsupported hardware type" );
			status = STATUS_HARDWARE_CFG_ERROR;
		}
	}

	/// Update our records.
	if (status == STATUS_OK)
	{
		m_states_long[ACTIVE_TURRET_POSITION] = turretPosition;
		LOG_TRACE( m_threadName, OPTICAL_COLUMN, "home turret_position succeeded" );
	}
	else
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "home turret_position failed" );
	}
	return status;
}

int CommandDolerThread::Implement_SetOlympus_IX85_TurretPosition(
	json_object * templateMessageObj,
	unsigned long safeFocusNmDuringTurretChange,
	unsigned long turretPosition )
{
	if (!m_mqttHardwareIO)
	{
		cout << m_threadName << "ERROR: m_mqttHardwareIO is null." << endl;
		return STATUS_DEVICE_ERROR; // EARLY RETURN!	EARLY RETURN!
	}

	///
	/// move_to_focus_nm
	FreezeWorkInProgress();
	int esStatusCode = Implement_moveToFocusNM( templateMessageObj, safeFocusNmDuringTurretChange );
	bool succeeded = (esStatusCode == STATUS_OK);

	///
	/// set_turret_position
	json_object * commandMessageOutOjb = NULL;
	/// Make a temporary copy of m_commandMessageInObj so we can re-use much of it for our outgoing message
	/// without risk of breaking its use in sending status to MqttSender after we are done here.
	int rc = json_object_deep_copy( templateMessageObj, &commandMessageOutOjb, NULL );
	/// Prepare variables that have one-time initialization.
	stringstream streamtopic;
	streamtopic << TOPIC_MICROSCOPE << CHANNEL_COMMAND;
	string topic = streamtopic.str();

	bool ackReceived = false;
	int ack = 0;
	json_object * jpayloadObj = nullptr;
	string turretMessageType = SET_TURRET_POSITION;
	/// Re-use most of the commandMessageOutOjb.  Calls to "*.add()" replace an object if it is already there.
	json_object * turretPayloadObj = json_object_new_object();
	rc = json_object_object_add( turretPayloadObj, TURRET_POSITION, json_object_new_uint64( turretPosition ) );
	succeeded &= (rc == 0);
	rc = json_object_object_add( commandMessageOutOjb, PAYLOAD, turretPayloadObj );
	succeeded &= (rc == 0);
	rc = json_object_object_add( commandMessageOutOjb, MESSAGE_TYPE, json_object_new_string( SET_TURRET_POSITION ) );
	succeeded &= (rc == 0);

	string desCallMessage = json_object_to_json_string( commandMessageOutOjb );
	LOG_TRACE( m_threadName, "optical column", desCallMessage.c_str() );
	FreezeWorkInProgress();
	int opticalColumnStatusCode = m_mqttHardwareIO->CommandTheHardwareClient( turretMessageType, desCallMessage, topic, ackReceived, ack, &jpayloadObj );
	if (esStatusCode == STATUS_OK)
	{
		if (ack != STATUS_OK)
		{
			LOG_ERROR( m_threadName, OPTICAL_COLUMN, "set turret position failed" );
			esStatusCode = ack;
		}
		else if (!ackReceived)
		{
			LOG_ERROR( m_threadName, OPTICAL_COLUMN, "set turret position failed" );
			esStatusCode = STATUS_DEVICE_ERROR;
		}
	}
/*
	succeeded &= ackReceived;
	succeeded &= (ack == STATUS_OK);
	succeeded &= (OpticalColumnStatusCode_to_ESStatusCode(opticalColumnStatusCode) == STATUS_OK);
*/
	json_object_put( commandMessageOutOjb ); /// Free our temporary, deep copy.

	/// move_to_focus_nm is not done again at this point. The application is expected to set the focus as part of its next set of operations.
/*
	return succeeded ? STATUS_OK : STATUS_DEVICE_ERROR;
*/
	return esStatusCode;
}


/// HomeTurret
int CommandDolerThread::Implement_HomeTurret()
{
	int status = STATUS_OK;
	ComponentState opticalColumnState = g_OpticalColumnState.load( std::memory_order_acquire );
	if (opticalColumnState == ComponentState::Mocked)
	{
		m_states_long[ACTIVE_TURRET_POSITION] = 0;
		return STATUS_OK; // EARLY RETURN!	EARLY RETURN!
	}

	/// Not mocked, so perform the change.
	HardwareType opticalColumnType = g_ComponentHardwareTypeArray[(int)ComponentType::OpticalColumn];
	if (opticalColumnType == HardwareType::IX85)
	{
		status = Implement_HomeOlympus_IX85_Turret();
	}
	else if (opticalColumnType == HardwareType::LS620)
	{
		status = Implement_HomeEtaluma_LS620_Turret();
	}
	else
	{
		LOG_FATAL( m_threadName, OPTICAL_COLUMN, "Configured to unsupported hardware type" );
		status = STATUS_HARDWARE_CFG_ERROR;
	}

	/// Update our records.
	if (status == STATUS_OK)
	{
		m_states_long[ACTIVE_TURRET_POSITION] = 0;
		LOG_TRACE( m_threadName, OPTICAL_COLUMN, "home turret_position succeeded" );
	}
	else
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "home turret_position failed" );
	}
	return status;
}

int CommandDolerThread::Implement_HomeOlympus_IX85_Turret()
{
	int status = STATUS_OK;
	// TODO: Implement a move to focus_position 0.
	LOG_WARN( m_threadName, OPTICAL_COLUMN, "NO-OP" );
	return status;
}

/// In the end, just call motor.thome().
/// This is different from lumaviewpro's turret_home() function because it doesn't do anything with a turret_bias.
/// This function moves the turret to 0 degrees. If that is not in alignment with optics and needs a small offset,
/// The caller needs to manage that and incorporate it into every call to SetOpticalTurretPosition.
int CommandDolerThread::Implement_HomeEtaluma_LS620_Turret()
{
	if (!FileExists( g_SerialPortDevice_MICRO ))
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "Serial port not found" );
		return STATUS_COMPONENT_NOT_FOUND; ///	EARLY RETURN!	EARLY RETURN!
	}

	FreezeWorkInProgress();
	string popen_cmd = HOME_T;
	string resultStr = "";
	int status = ShellInvoker::InvokePopenHand( popen_cmd, m_threadName, OPTICAL_COLUMN, resultStr );
	return status;
}


/// HomeFocus
int CommandDolerThread::Implement_HomeFocus()
{
	int status = STATUS_OK;
	/// Mock the move if appropriate.
	ComponentState opticalColumnState = g_OpticalColumnState.load( std::memory_order_acquire );
	if (opticalColumnState == ComponentState::Mocked)
	{
		m_states_long[FOCUS_POSITION] = 0;
		return STATUS_OK; // EARLY RETURN!	EARLY RETURN!
	}

	/// Not mocked, so perform the change.
	HardwareType opticalColumnType = g_ComponentHardwareTypeArray[(int)ComponentType::OpticalColumn];
	if (opticalColumnType == HardwareType::IX85)
	{
		status = Implement_HomeOlympus_IX85_Focus();
	}
	else if (opticalColumnType == HardwareType::LS620)
	{
		status = Implement_HomeEtaluma_LS620_Focus();
	}
	else
	{
		LOG_FATAL( m_threadName, OPTICAL_COLUMN, "Configured to unsupported hardware type" );
		status = STATUS_HARDWARE_CFG_ERROR;
	}

	/// Update our records.
	if (status == STATUS_OK)
	{
		m_states_long[FOCUS_POSITION] = 0;
		LOG_TRACE( m_threadName, OPTICAL_COLUMN, "home focus_position succeeded" );
	}
	else
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "home focus_position failed" );
	}
	return status;
}

int CommandDolerThread::Implement_HomeOlympus_IX85_Focus()
{
	int status = STATUS_OK;
	// TODO: Implement a move to focus_position 0.
	LOG_WARN( m_threadName, OPTICAL_COLUMN, "NO-OP" );
	return status;
}
int CommandDolerThread::Implement_HomeEtaluma_LS620_Focus()
{
	if (!FileExists( g_SerialPortDevice_MICRO ))
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "Serial port not found" );
		return STATUS_COMPONENT_NOT_FOUND; ///	EARLY RETURN!	EARLY RETURN!
	}

	FreezeWorkInProgress();
	string popen_cmd = HOME_Z;
	string resultStr = "";
	int status = ShellInvoker::InvokePopenHand( popen_cmd, m_threadName, OPTICAL_COLUMN, resultStr );
	return status;
}

///
/// YY Axis Motion
///

///
/// XX Axis Motion
///

///
/// Z Axis Motion
///

///----------------------------------------------------------------------
/// DoleOut_moveToZNm
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_moveToZNm()
{
	/// Extract z from payload.
	long z_nm = this->m_desHelper->ExtractPayloadLongValue( m_commandMessageInObj, Z_NM );

	int status = Implement_moveToZNm( z_nm ); /// Assume that speeds are already packed in the payload.

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_moveToZRelativeNm
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_moveToZRelativeNm()
{
	/// Extract z_offset_nm from payload.
	long z_offset_nm = this->m_desHelper->ExtractPayloadLongValue( m_commandMessageInObj, Z_OFFSET_NM );
	long z_nm = m_states_long[Z_NM] + z_offset_nm;

	int status = Implement_moveToZNm( z_nm ); /// Assume that speeds are already packed in the payload.

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_retractToZNm
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_retractToZNm()
{
	/// Extract z from payload.
	long retraction_height_nm = this->m_desHelper->ExtractPayloadLongValue( m_commandMessageInObj, RETRACTION_HEIGHT_NM );

	int status = Implement_moveToZNm( retraction_height_nm ); /// Assume that speeds are already packed in the payload.

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// Implement_moveToZNm
///----------------------------------------------------------------------
int CommandDolerThread::Implement_moveToZNm(long z_nm)
{
	return Implement_moveToZNm( z_nm, INVALID_SPEED );
}

int CommandDolerThread::Implement_moveToZNm(long z_nm, long speed)
{
	int status = STATUS_OK;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "z", "MoveToZNm sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState ZState = g_ZState.load( std::memory_order_acquire );

	if (ZState == ComponentState::Mocked)
	{
		LOG_TRACE( m_threadName, "z", "MOCKING Z move" );
		m_states_long[Z_NM] = z_nm;
		status = STATUS_OK;
	}
	else if ((ZState == ComponentState::Good) || (ZState == ComponentState::Homed1) || (ZState == ComponentState::Homed3))
	{
		/// Extract parameters from payload.
		long velocity = 0L;
		long acceleration = 0L;
		long deceleration = 0L;
		long stp = 0L;
		long axisResolution_nm = 0L;
		long softTravelLimitPlus_nm = 0L;
		long softTravelLimitMinus_nm = 0L;
		if (!m_desHelper->ExtractSpeedsAndLimits( m_threadName, m_commandMessageInObj, "z",
										 velocity, acceleration, deceleration, stp, axisResolution_nm,
										 softTravelLimitPlus_nm, softTravelLimitMinus_nm ))
		{
			return STATUS_NOT_FOUND;			/// EARLY RETURN!	EARLY RETURN!
		}
		if (softTravelLimitMinus_nm > z_nm)
		{
			stringstream msg;
			msg << "Z move is lower than " << softTravelLimitMinus_nm << " and was sent to upper travel limit instead";
			LOG_ERROR( m_threadName, "z", msg.str().c_str() );
			z_nm = softTravelLimitPlus_nm; // Don't go down. Instead, go up to upper travel limit.
			status = STATUS_MOTION_FORBIDDEN;
		}
		else if (z_nm > softTravelLimitPlus_nm)
		{
			stringstream msg;
			msg << "Z move attempted to go higher than " << softTravelLimitPlus_nm << " but was sent to upper travel limit instead" ;
			LOG_ERROR( m_threadName, "z", msg.str().c_str() );
			z_nm = softTravelLimitPlus_nm; // Go up, but stop at upper travel limit.
			status = STATUS_MOTION_FORBIDDEN;
		}

		if (speed == INVALID_SPEED)
		{
			stringstream msg;
			msg << "using extracted velocity=" << velocity;
			LOG_TRACE( m_threadName, "z", msg.str().c_str() );
		}
		else
		{
			/// Override the default set_speed with the supplied one.
			velocity = speed;
			stringstream msg;
			msg << "using supplied speed=" << speed;
			LOG_TRACE( m_threadName, "z", msg.str().c_str() );
		}
		FreezeWorkInProgress();
		unique_lock<mutex> lock( g_controllerBMutex ); /// Prevent the AnnularRingHolder thread from moving at the same time.

		/// Use a temporary status here, to avoid overwriting STATUS_MOTION_FORBIDDEN--unless the temporary status indicates an error.
		int tempStatus = Implement_MoveOneAxis( "Z", z_nm, axisResolution_nm, velocity, acceleration, deceleration, stp );
		/// If tempStatus is OK, we'll return the current status--be it OK or MOTION_FORBIDDEN.
		if (tempStatus != STATUS_OK)
		{
			/// If tempStatus is an error, report it.
			/// The new error is more important than the previous [OK or MOTION_FORBIDDEN]
			status = tempStatus;
		}
		/// else status is [OK or MOTION_FORBIDDEN]

		m_states_long[Z_NM] = z_nm;
		if (status == STATUS_MOVEMENT_FAILED)
		{
			/// Send an alert.
			stringstream topic_stream;
			topic_stream << TOPIC_ES << CHANNEL_ALERT;

			/// Collect fields from the command's header for use in the alert.
			std::string messageType = "";
			int commandGroupId = 0;
			int commandId = 0;
			std::string sessionId = "";
			std::string machineId = "";
			bool succeeded = m_desHelper->SplitCommand( m_commandMessageInObj,
														&messageType,
														nullptr, // &transportVersion,
														&commandGroupId,
														&commandId,
														&sessionId,
														&machineId );

			json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
																commandGroupId,
																commandId,
																sessionId, // Or topic_stream.str()?
																ComponentType::ZAxis,
																STATUS_AXIS_Z_FAULTED,
																false ); /// thisIsCausingHold
			bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "z", "Wrong Z state" );
	}

	return status;
}

///----------------------------------------------------------------------
/// DoleOut_getCurrentZNm
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_getCurrentZNm()
{
	int status = STATUS_OK;
	long z_nm = 0L;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "z", "GetCurrentZNM sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState ZState = g_ZState.load( std::memory_order_acquire );

	if (ZState == ComponentState::Mocked)
	{
		z_nm = m_states_long[Z_NM];
	}
	else if ((ZState == ComponentState::Good) || (ZState == ComponentState::Homed3))
	{
		status = Implement_GetAxisPosition( "z", z_nm );
		if (z_nm != m_states_long[Z_NM])
		{
			stringstream msg;
			msg << "Z measured-pos=" << z_nm << "|recorded-pos=" << m_states_long[Z_NM];
			LOG_WARN( m_threadName, "z", msg.str().c_str() );
			m_states_long[Z_NM] = z_nm;
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "z", "Wrong Z state" );
	}

	/// Populate the status payload
    int result = json_object_object_add( m_statusPayloadOutObj, Z_NM, json_object_new_int64( z_nm ) );
	if ((status == STATUS_OK) && (result != 0))
	{
		/// populating the payload failed and nothing else did
		status = STATUS_CMD_MSG_ERROR;
	}
	return status;
}


int CommandDolerThread::DoleOut_setZAxisSpeed()
{
	int status = STATUS_OK;
	/// Extract z_axis_speed from payload
	unsigned long z_axisSpeed = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, Z_AXIS_SPEED );
//	cout << m_threadName << ": DoleOut_setZAxisSpeed() received z_axis_speed of " << z_axisSpeed << endl;
	// TODO: Implement more here.
	FreezeWorkInProgress();
	;
	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_MoveZToSafeHeight
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveZToSafeHeight()
{
	int status = STATUS_OK;
	/// Get the payload object.
	json_object * payloadObj = nullptr;
	if (!json_object_object_get_ex( m_commandMessageInObj, PAYLOAD, &payloadObj ))
	{
		LOG_ERROR( m_threadName, "z", "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string( payloadObj );
		cout << payloadStr << endl;
	}
	long z_safeHeight_nm = 0L;
	long z_setSpeed = 0L;
	if (status == STATUS_OK)
	{
		/// Extract z_safe_height from payload.
		if (!m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_SAFE_HEIGHT, z_safeHeight_nm ))
		{
			LOG_ERROR( m_threadName, "z", "z_safe_height is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
			
		if (!m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_SET_SPEED, z_setSpeed ))
		{
			LOG_WARN( m_threadName, "z", "z_set_speed is missing" );
		}
	}
	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "z", "Moving Z to safety" );
		if (z_setSpeed == 0L)
		{
			status = Implement_moveToZNm( z_safeHeight_nm );
		}
		else
		{
			status = Implement_moveToZNm( z_safeHeight_nm, z_setSpeed );
		}
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_MoveToZAxisForceNewton
/// 	Moves the Z axis down until it either senses a specified force on
/// its axis force sensor or reaches a specified offset.
/// Uses the ZAxis.tip_pickup_force and ZAxis.z_to_top_of_yy_stage_height +
/// tip_rack_seat_to_top_of_tip - (tip_total_length - tip_working_length) -
/// ZAxis.tip_pickup_overshoot height.
/// 	Returns error status if Z Axis reaches the specified offset before
/// sensing the specified force.
///		Can be called from DoWork().
///		Is also called from DoleOut_MoveToZTipPickForceNewton()
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveToZAxisForceNewton()
{
	int status = STATUS_OK;
	/// Extract values from the payload.
	bool succeeded = true;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string( payloadObj );
		cout << payloadStr << endl;

		long tipPickupSpeed = 0L;
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_PICKUP_SPEED, tipPickupSpeed );
		float tipPickupForce = 0.0;
		succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, TIP_PICKUP_FORCE, tipPickupForce );
		long tipPickupOvershoot = 0L;
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_PICKUP_OVERSHOOT, tipPickupOvershoot );
		long z_toTopOfYYStageHeight_nm = 0L;
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_TO_TOP_OF_YY_STAGE_HEIGHT, z_toTopOfYYStageHeight_nm );
		TipType tipType = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );
		long z_velocity = 0L; /// Will not be used, but DesHelper::ExtractSpeedsAndLimits needs a velocity input parameter.
		long z_acceleration = 0L;
		long z_deceleration = 0L;
		long z_stp = 0L;
		long z_axisResolution_nm = 0L;
		long softTravelLimitPlus_nm = 0L;
		long softTravelLimitMinus_nm = 0L;
		succeeded &= m_desHelper->ExtractSpeedsAndLimits( m_threadName, m_commandMessageInObj, "z",
												z_velocity, z_acceleration, z_deceleration, z_stp, z_axisResolution_nm,
												softTravelLimitPlus_nm, softTravelLimitMinus_nm );
		long maxPosition = z_toTopOfYYStageHeight_nm + tipType.tipRackSeatToTopOfTip -
							(tipType.tipTotalLength - tipType.tipWorkingLength) -
							tipPickupOvershoot;
		if (true)
		{
			stringstream msg;
			msg << MOVE_TO_Z_AXIS_FORCE_NEWTON << " calculated max-position=" << maxPosition << "nm";
			msg << "|inputs were z_toTopOfYYStageHeight_nm=" << z_toTopOfYYStageHeight_nm;
			msg << "|tipType.tipRackSeatToTopOfTip=" << tipType.tipRackSeatToTopOfTip;
			msg << "|tipType.tipTotalLength=" << tipType.tipTotalLength;
			msg << "|tipType.tipWorkingLength=" << tipType.tipWorkingLength;
			msg << "|tipPickupOvershoot=" << tipPickupOvershoot;
			LOG_DEBUG( m_threadName, "z", msg.str().c_str() );
		}
		if ((softTravelLimitMinus_nm > maxPosition) || (maxPosition > softTravelLimitPlus_nm))
		{
			stringstream msg;
			msg << "Z move is out of (soft-limit) bounds|minus=" << softTravelLimitMinus_nm << "|Zmax=" << maxPosition << "|plus=" << softTravelLimitPlus_nm;
			LOG_ERROR( m_threadName, "z", msg.str().c_str() );
			return STATUS_MOTION_FORBIDDEN;			/// EARLY RETURN!	EARLY RETURN!
		}

		FreezeWorkInProgress();
		status = Implement_MoveToZAxisForceNewton(	tipPickupSpeed,
													tipPickupForce,
													maxPosition,
													z_acceleration,
													z_deceleration,
													z_stp,
													z_axisResolution_nm );
		if (!succeeded) LOG_ERROR( m_threadName, "z", "Not succeeding" );
		long well_bottom_height_nm = m_states_long[Z_NM];
		/// Pack well_bottom_height_nm into the status payload.
		int result = json_object_object_add( m_statusPayloadOutObj, WELL_BOTTOM_HEIGHT_NM, json_object_new_int64( well_bottom_height_nm ) );
		succeeded &= (result == 0);
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "z", "Failed to find payload" );
	}

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_MoveToZAxisSeatingForceNewton
/// 	Moves the Z axis down until it either senses a specified force
/// while seating the tip or reaches a specified offset. Uses the Z axis
/// tip seating force and ZAxis.z_to_top_of_seating_key_height +
/// tip_total_length - tip_working_length - Zaxis.tip_seating_overshoot
/// height.
///		Returns error status if Z Axis reaches the specified offset before
/// sensing the specified force.
///		Returns the tip_working_length_nm if the find seating force was successful.
/// The tip_working_length_nm = tip_type.tip_total_length -
/// (ZAxis.z_to_top_of_seating_key_height - measured_seating_height + tip_type.tip_shoulder_length).
///		Can be called from DoWork().
///		Is also called from DoleOut_MoveToZTipSeatingForceNewton()
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveToZAxisSeatingForceNewton()
{
	int status = STATUS_OK;
	/// Extract values from the payload.
	bool succeeded = true;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		/// Extract values from the payload.
		long tipSeatingSpeed = 0L;
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_SEATING_SPEED, tipSeatingSpeed );
		float tipSeatingForce = 0.0;
		succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, TIP_SEATING_FORCE, tipSeatingForce );
		long tipSeatingOvershoot = 0L;
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_SEATING_OVERSHOOT, tipSeatingOvershoot );
		long z_toTopOfSeatingKeyHeight = 0L;
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_TO_TOP_OF_SEATING_KEY_HEIGHT, z_toTopOfSeatingKeyHeight );
		TipType tipType = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );
		long z_velocity = 0L; /// Will not be used, but DesHelper::ExtractSpeedsAndLimits needs a velocity input parameter.
		long z_acceleration = 0L;
		long z_deceleration = 0L;
		long z_stp = 0L;
		long z_axisResolution_nm = 0L;
		long softTravelLimitPlus_nm = 0L;
		long softTravelLimitMinus_nm = 0L;
		succeeded &= m_desHelper->ExtractSpeedsAndLimits( m_threadName, m_commandMessageInObj, "z",
												z_velocity, z_acceleration, z_deceleration, z_stp, z_axisResolution_nm,
												softTravelLimitPlus_nm, softTravelLimitMinus_nm );
		long maxPosition = z_toTopOfSeatingKeyHeight + tipType.tipRackSeatToTopOfTip - tipType.tipRackSeatToTipSeat - tipType.tipTotalLength + tipType.tipWorkingLength - tipSeatingOvershoot;
		if (true)
		{
			stringstream msg;
			msg << MOVE_TO_Z_AXIS_SEATING_FORCE_NEWTON << " calculated max-position=" << maxPosition << "nm";
			LOG_DEBUG( m_threadName, "z", msg.str().c_str() );
		}
		if ((softTravelLimitMinus_nm > maxPosition) || (maxPosition > softTravelLimitPlus_nm))
		{
			stringstream msg;
			msg << "Z move is out of (soft-limit) bounds|minus=" << softTravelLimitMinus_nm << "|Zmax=" << maxPosition << "|plus=" << softTravelLimitPlus_nm;
			LOG_ERROR( m_threadName, "z", msg.str().c_str() );
			return STATUS_MOTION_FORBIDDEN;			/// EARLY RETURN!	EARLY RETURN!
		}

		FreezeWorkInProgress();
		status = Implement_MoveToZAxisForceNewton(	tipSeatingSpeed,
													tipSeatingForce,
													maxPosition,
													z_acceleration,
													z_deceleration,
													z_stp,
													z_axisResolution_nm );
		succeeded &= (status == STATUS_OK);
		/// Calculate tipWorkingLength_nm whether we succeeded or not, for logging purposes.
		long tipWorkingLength_nm = tipType.tipTotalLength - (z_toTopOfSeatingKeyHeight - m_states_long[Z_NM] + tipType.tipShoulderLength);
		if (true)
		{
			stringstream msg;
			msg << "tip_working_length_nm=" << tipWorkingLength_nm;
			LOG_TRACE( m_threadName, "z", msg.str().c_str() );
		}
		if (succeeded)
		{
			/// Pack tipWorkingLength_nm into the outgoing payload.
			int resultZ = json_object_object_add( m_statusPayloadOutObj, TIP_WORKING_LENGTH_NM, json_object_new_int64( tipWorkingLength_nm ) );
			if (resultZ != 0)
			{
				status = STATUS_CMD_MSG_ERROR;
				LOG_ERROR( m_threadName, "z", "Failed to pack tip_working_length_nm into status payload" );
			}
		}
		else
		{
			LOG_ERROR( m_threadName, "z", "Not succeeding" );
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "z", "Failed to find payload" );
	}

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_MoveToZAxisWellBottomForceNewton
/// Moves the Z axis down until it either senses a specified force while
/// finding the well bottom or reaches a specified offset. Uses the Z axis
/// tip find well bottom force and the payload's "error_height" properties.

/// Returns error status if Z Axis reaches the specified offset before
/// sensing the specified force.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveToZAxisWellBottomForceNewton()
{
	int status = STATUS_OK;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "z", "DoleOut_MoveToZAxisWellBottomForceNewton() sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		// TODO: Delete this temporary debug.
		string payloadStr = json_object_to_json_string( payloadObj );
		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "z", "Payload is missing" );
		return STATUS_CMD_MSG_DATA_MISSING;			/// EARLY RETURN!	EARLY RETURN!
	}

	bool succeeded = true;
	unsigned long errorHeight_nm = 0L;
	float tip_find_well_bottom_force = 0.0;
	unsigned long tip_find_well_bottom_speed = 0L;
	/// Extract values from the payload.
	/// Get a pointer to the payload object.

	if (payloadObj) /// See above for error reporting if payload is missing.
	{
		succeeded &= m_desHelper->ExtractULongValue( m_threadName, payloadObj, ERROR_HEIGHT, errorHeight_nm );
		if (!succeeded)
		{
			LOG_ERROR( m_threadName, "z", "error_height is missing" );
			return STATUS_CMD_MSG_DATA_MISSING;			/// EARLY RETURN!	EARLY RETURN!
		}

		succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, TIP_FIND_WELL_BOTTOM_FORCE, tip_find_well_bottom_force );
		if (!succeeded)
		{
			LOG_ERROR( m_threadName, "z", "tip_find_well_bottom_force is missing" );
			return STATUS_CMD_MSG_DATA_MISSING;			/// EARLY RETURN!	EARLY RETURN!
		}

		succeeded &= m_desHelper->ExtractULongValue( m_threadName, payloadObj, TIP_FIND_WELL_BOTTOM_SPEED, tip_find_well_bottom_speed );
		if (!succeeded)
		{
			LOG_ERROR( m_threadName, "z", "error_height is missing" );
			return STATUS_CMD_MSG_DATA_MISSING;			/// EARLY RETURN!	EARLY RETURN!
		}

	}
	long z_velocity = 0L; /// Will not be used, but DesHelper::ExtractSpeedsAndLimits needs a velocity input parameter.
	long z_acceleration = 0L;
	long z_deceleration = 0L;
	long z_stp = 0L;
	long z_axisResolution_nm = 0L;
	long softTravelLimitPlus_nm = 0L;
	long softTravelLimitMinus_nm = 0L;
	succeeded &= m_desHelper->ExtractSpeedsAndLimits( m_threadName, m_commandMessageInObj, "z",
											z_velocity, z_acceleration, z_deceleration, z_stp, z_axisResolution_nm,
											softTravelLimitPlus_nm, softTravelLimitMinus_nm );
	FreezeWorkInProgress();
	status = Implement_MoveToZAxisForceNewton(	tip_find_well_bottom_speed,
												tip_find_well_bottom_force,
												errorHeight_nm,
												z_acceleration,
												z_deceleration,
												z_stp,
												z_axisResolution_nm );
	if (!succeeded) LOG_ERROR( m_threadName, "z", "Not succeeding" );

	/// This status needs no payload.
	return status;
}


///----------------------------------------------------------------------
/// DoleOut_MoveToZTipPickForceNewton
///		Assumes that the tip insert is already in position above the tip
/// location. (Presume it is at Z safe height.)
/// 	For step A below, let's define "Just above tip" mean
/// "ZAxis.z_to_top_of_YY_stage_height + tip_rack_seat_to_top_of_tip +
/// ZAxis.tip_pickup_slowdown_offset".
///
/// A. Moves Z down at set-speed to just above tip.
/// B. Calls DoleOut_MoveToZAxisForceNewton to slowly pick the tip.
/// C. Moves Z up at set-speed to safe height.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveToZTipPickForceNewton()
{
	bool succeeded = true;
	int status = STATUS_OK;

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string(payloadObj);
		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "CxD", "FAILED to find payload object" );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	/// Extract needed input parameters from payload
	TipType tipType = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );

	long z_safeHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_SAFE_HEIGHT, z_safeHeight_nm );

	long tipPickupSlowdownOffset_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_PICKUP_SLOWDOWN_OFFSET, tipPickupSlowdownOffset_nm );

	long z_toTopOfYYStageHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_TO_TOP_OF_YY_STAGE_HEIGHT, z_toTopOfYYStageHeight_nm );
	
	long z_approachStartPos_nm = z_toTopOfYYStageHeight_nm + tipType.tipRackSeatToTopOfTip + tipPickupSlowdownOffset_nm;
	if (true)
	{
		stringstream msg;
		msg << MOVE_TO_Z_TIP_PICK_FORCE_NEWTON << " calculated just-above-tip=" << z_approachStartPos_nm << "nm";
		LOG_DEBUG( m_threadName, "z", msg.str().c_str() );
	}
	
	/// Move Z axis down to a position just above a tip.
	if (succeeded)
	{
		LOG_TRACE( m_threadName, "z", "Moving Z to just above tip" );
		status = Implement_moveToZNm( z_approachStartPos_nm ); /// Move to just above tip.
	}
	succeeded &= (status == STATUS_OK);

	/// Call prog0 routine MoveToForceZ,
	/// which should move at the slower approach speed until the 
	/// measured force indicates success.
	if (succeeded)
	{
		status = DoleOut_MoveToZAxisForceNewton();
	}
	succeeded &= (status == STATUS_OK);

	/// Return Z to safe height.
	if (succeeded)
	{
		LOG_TRACE( m_threadName, "z", "Returning Z to safety" );
		status = Implement_moveToZNm( z_safeHeight_nm );
	}
	else
	{
		LOG_WARN( m_threadName, "z", "Returning Z to safety AFTER ERROR" );
		int stat = Implement_moveToZNm( z_safeHeight_nm ); /// Stat is local so the failing status will be returned.
	}

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// Implement_MoveToZAxisForceNewton
/// Calls the prog0 routine called MoveToForceZ.
/// A. First, run the prog0 routine ZeroADCZ to zero out the force gauge.
/// B. Pass in input parameters ClrKillsTimeOut, CoordMovAccel, CoordMovDecel,
///    CoordMovVel, CoordMovSTP,  and ZMaxPosition
/// C. Start monitoring the output status and ForcePosition.
/// D. Use ACR7000::runProgram() to start the routine and wait for it to finish.
/// E. Collect the output parameters.
/// F. Stop monitoring the output parameters.
///----------------------------------------------------------------------
int CommandDolerThread::Implement_MoveToZAxisForceNewton(	unsigned long z_approach_speed, /// tip_pickup_speed or ...
															double z_work_force_newton, /// tip_pickup_force or ...
															long z_work_overshoot, /// tip_pickup_overshoot or ...
															long acceleration,
															long deceleration,
															long stp,
															long axisResolution_nm )
{
	LOG_TRACE( m_threadName, "temp", "Implement_MoveToZAxisForceNewton() start" );
	int status = STATUS_OK;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "z", "MoveToZAxisForceNewton sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState ZState = g_ZState.load( std::memory_order_acquire );


	if (ZState == ComponentState::Mocked)
	{
		m_states_long[Z_NM] = z_work_overshoot; /// Not realistic, but good enough.
		status = STATUS_OK;
	}
	else if (ZState == ComponentState::Good)
	{
		///
		/// Call prog0 routine ZeroADCZ,
		/// which zeroes out the force gauge.
		///
		FreezeWorkInProgress();
		/// ZeroADCZ runs on the A_CONTROLLER. We don't have a way to know whether it is available
		/// because we don't have a ComponentState for that controller. So we will use the XX-axis
		/// state as a proxy. If XX is mocked, assume that all of A_CONTROLLER is unavailable and
		/// pretend to run ZeroADCZ
		if (g_XXState.load( std::memory_order_acquire ) == ComponentState::Mocked)
		{
			LOG_WARN( m_threadName, "z", "Mocking call to ZeroADCZ" );
		}
		else
		{
			string error_msg = "";
			int prog0_routineID = ZERO_ADC_Z_ROUTINE;
			struct timespec when;
			try {
				LOG_TRACE( m_threadName, "z", "running ZeroADCZ" );
				ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at( A_CONTROLLER );
				/// No input parameters needed.
				/// No output parameters needed.
				/// Start monitoring the output parameter PNNN where NNN is prog0_routineID.
				pController->monitor( prog0_routineID, ACR7000::pvalue_type_t::pvalue_type_float, 100 );
				usleep( 500000 ); 
				/// Start the prog0 routine by writing prog0_routineID to P0.
				pController->runProgram( prog0_routineID, true );
				usleep(50000);
				pController->cancelMonitoring( prog0_routineID );
			} catch (MotionException &x) {
				error_msg = x.getEventDesc();
				status = STATUS_MOVEMENT_FAILED;
			} catch (std::exception &ex) {
				error_msg = "Setting P0 failed";
				status = STATUS_DEVICE_ERROR;
			}
			if (status != STATUS_OK)
			{
				stringstream msg;
				msg << "ZeroADCZ failed|" << error_msg;
				LOG_FATAL( m_threadName, "z", msg.str().c_str() );
				return status;			/// EARLY RETURN!	EARLY RETURN!
			}
			else
			{
				LOG_TRACE( m_threadName, "z", "ZeroADCZ succeeded" );
			}
		}

		///
		/// Call prog0 routine MoveToForceZ,
		/// which should move at the slower approach speed until the 
		/// measured force indicates success.
		///
		FreezeWorkInProgress();
		string error_msg = "";
		float forcePosition = 0.0;
		float moveToForceZStatus = 0.0;
		int prog0_routineID = MOVE_TO_FORCE_Z_ROUTINE;
		struct timespec when;
		try {
			stringstream msg;
			msg << "running MoveToForceZ";
			msg << "|force=" << z_work_force_newton << "|ClrKillsTimeout=" << DEFAULT_CLEAR_KILLS_TIMEOUT << "|vel=" << (z_approach_speed/_1MEG_L) << "|accel=" << (acceleration/_1MEG_L);
			msg << "|decel=" << (deceleration/_1MEG_L) << "|stp=" << (stp/_1MEG_L) << "|maxPos=" << ((float) z_work_overshoot/_1MEG_F);
			LOG_TRACE( m_threadName, "z", msg.str().c_str() );
			ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at( B_CONTROLLER );
			MotionAxis &axis = g_motionServices_ptr->getAxis("Z");
			/// Deliver input parameters.
			pController->binarySetFloat( MOVE_TO_FORCE_N_SN, (float) z_work_force_newton  );
			pController->binarySetLong( CLR_KILLS_TIME_OUT_SN, DEFAULT_CLEAR_KILLS_TIMEOUT );
			pController->setVelAccelDecelStop( (float)z_approach_speed/_1MEG_F, (float)acceleration/_1MEG_F, (float)deceleration/_1MEG_F, (float)stp/_1MEG_F );
			pController->binarySetFloat( Z_MAX_POSITION_SN, (float) z_work_overshoot/_1MEG_F  );
			
			/// Start monitoring output parameters.
			pController->monitor( MOVE_TO_FORCE_Z_STATUS_SN, ACR7000::pvalue_type_t::pvalue_type_float, 100 ); /// Register for the returned status value
			pController->monitor( FORCE_POSITION_SN, ACR7000::pvalue_type_t::pvalue_type_float, 100 ); /// Register for the returned final position.
			usleep( 600000 );
			/// Start the prog0 routine by writing prog0_routineID to P0.

			///
			/// This block replaces a call to ACR7000::runProgram()
			///
			if (status == STATUS_OK)
			{
				pController->launchProgram( prog0_routineID, true );
			}
			int loopCount = 0;
			while ((status == STATUS_OK) && (!pController->programIsRunning( prog0_routineID )))
			{
				usleep(100000);
				if (loopCount++ > MAX_WAIT_FOR_START_LOOPS)
				{
					LOG_ERROR( m_threadName, "z", "FAILED to start MoveToForceZ" );
					status = STATUS_TIMEOUT_ERROR; // Break out of loop.
				}
			}

			/// If all went well, MoveToForceZ is now running.
			while ((status == STATUS_OK) && ((pController->programIsRunning( prog0_routineID )) || (moveToForceZStatus == 0.0)))
			{
				stringstream msg;
				float position_mm = axis.getActualPosition() / axis.getCachedPPU();
				msg << "|pos(mm)" << position_mm << "|";
				LOG_TRACE( m_threadName, "z", msg.str().c_str() );
				moveToForceZStatus = pController->getProgramStatus( prog0_routineID );
				usleep(100000);
				if (loopCount++ > MAX_WAIT_FOR_END_LOOPS)
				{
					LOG_ERROR( m_threadName, "z", "MoveToForceZ timed out" );
					status = STATUS_TIMEOUT_ERROR; // Break out of loop.
				}
			}
			///
			/// End of replacement for ACR7000::runProgram()
			///

			usleep(50000);
			/// Collect outputs
			forcePosition = pController->inspectPValueFloat((ACR7000::pvalue_index_t)FORCE_POSITION_SN, when);
			/// Stop monitoring the output parameters.
			pController->cancelMonitoring( prog0_routineID );
			pController->cancelMonitoring( FORCE_POSITION_SN );
		} catch (MotionException &x) {
			error_msg = x.getEventDesc();
			status = STATUS_MOVEMENT_FAILED;
		} catch (std::exception &ex) {
			error_msg = "Setting P0 failed";
			status = STATUS_DEVICE_ERROR;
		}
		if (true)
		{
			stringstream msg;
			msg << "moveToForceZStatus=" << moveToForceZStatus;
			LOG_DEBUG( m_threadName, "z", msg.str().c_str() );
		}
		if (status != STATUS_OK)
		{
			stringstream msg;
			msg << "MoveToForceZ failed|error_msg=" << error_msg;
			LOG_FATAL( m_threadName, "z",  msg.str().c_str() );
			return status;			/// EARLY RETURN!	EARLY RETURN!
		}
		else if (moveToForceZStatus == 0.0)
		{
			stringstream msg;
			msg << "MoveToForceZ is still running! returning status " << moveToForceZStatus;
			LOG_FATAL( m_threadName, "z", msg.str().c_str() );
			status = STATUS_PROG_0_ERROR;
		}
		else if (moveToForceZStatus == 1.0)
		{
			stringstream msg;
			msg << "MoveToForceZ failed, returning status " << moveToForceZStatus;
			LOG_FATAL( m_threadName, "z", msg.str().c_str() );
			status = STATUS_PROG_0_ERROR;
		}
		else
		{
			m_states_long[Z_NM] = (long)(forcePosition * _1MEG_F);
			stringstream msg;
			msg << "MoveToForceZ succeeded moving to " << m_states_long[Z_NM] << " nm";
			LOG_TRACE( m_threadName, "z", msg.str().c_str() );
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "z", "Wrong Z state" );
	}

	LOG_TRACE( m_threadName, "temp", "Implement_MoveToZAxisForceNewton() end" );
	return status;
}


///----------------------------------------------------------------------
/// Implement_MoveToZZAxisForceNewton
/// Calls the prog0 routine called MoveToForceZZ.
/// See Implement_MoveToZAxisForceNewton for details because it is similar.
///----------------------------------------------------------------------
int CommandDolerThread::Implement_MoveToZZAxisForceNewton(	unsigned long zz_approach_speed, /// tip_pickup_speed or ...
															double zz_work_force_newton, /// tip_pickup_force or ...
															long zz_work_overshoot, /// tip_pickup_overshoot or ...
															long acceleration,
															long deceleration,
															long stp,
															long axisResolution_nm )
{
	LOG_TRACE( m_threadName, "temp", "Implement_MoveToZZAxisForceNewton() start" );
	int status = STATUS_OK;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "zz", "MoveToZZAxisForceNewton sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState ZZState = g_ZZState.load( std::memory_order_acquire );


	if (ZZState == ComponentState::Mocked)
	{
		m_states_long[ZZ_NM] = zz_work_overshoot; /// Not realistic, but good enough.
		status = STATUS_OK;
	}
	else if (ZZState == ComponentState::Good)
	{
		///
		/// Call prog0 routine ZeroADCZZ,
		/// which zeroes out the force gauge.
		///
		FreezeWorkInProgress();
		/// ZeroADCZZ runs on the A_CONTROLLER. We don't have a way to know whether it is available
		/// because we don't have a ComponentState for that controller. So we will use the XX-axis
		/// state as a proxy. If XX is mocked, assume that all of A_CONTROLLER is unavailable and
		/// pretend to run ZeroADCZZ
		if (g_XXState.load( std::memory_order_acquire ) == ComponentState::Mocked)
		{
			LOG_WARN( m_threadName, "zz", "Mocking call to ZeroADCZZ" );
		}
		else
		{
			string error_msg = "";
			int prog0_routineID = ZERO_ADC_ZZ_ROUTINE;
			struct timespec when;
			try {
				LOG_TRACE( m_threadName, "zz", "running ZeroADCZZ" );
				ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at( A_CONTROLLER );
				/// No input parameters needed.
				/// No output parameters needed.
				/// Start monitoring the output parameter PNNN where NNN is prog0_routineID.
				pController->monitor( prog0_routineID, ACR7000::pvalue_type_t::pvalue_type_float, 100 );
				usleep( 500000 ); 
				/// Start the prog0 routine by writing prog0_routineID to P0.
				pController->runProgram( prog0_routineID, true );
				usleep(50000);
				pController->cancelMonitoring( prog0_routineID );
			} catch (MotionException &x) {
				error_msg = x.getEventDesc();
				status = STATUS_MOVEMENT_FAILED;
			} catch (std::exception &ex) {
				error_msg = "Setting P0 failed";
				status = STATUS_DEVICE_ERROR;
			}
			if (status != STATUS_OK)
			{
				stringstream msg;
				msg << "ZeroADCZZ failed|" << error_msg;
				LOG_FATAL( m_threadName, "zz",  msg.str().c_str() );
				return status;			/// EARLY RETURN!	EARLY RETURN!
			}
			else
			{
				LOG_TRACE( m_threadName, "zz", "ZeroADCZZ succeeded" );
			}
		}

		///
		/// Call prog0 routine MoveToForceZZ,
		/// which should move at the slower approach speed until the 
		/// measured force indicates success.
		///
		FreezeWorkInProgress();
		string error_msg = "";
		float forcePosition = 0.0;
		float moveToForceZZStatus = 0.0;
		int prog0_routineID = MOVE_TO_FORCE_ZZ_ROUTINE;
		struct timespec when;
		try {
			stringstream msg;
			msg << "running MoveToForceZZ";
			msg << "|force=" << zz_work_force_newton << "|ClrKillsTimeout=" << DEFAULT_CLEAR_KILLS_TIMEOUT << "|vel=" << (zz_approach_speed/_1MEG_L) << "|accel=" << (acceleration/_1MEG_L);
			msg << "|decel=" << (deceleration/_1MEG_L) << "|stp=" << (stp/_1MEG_L) << "|maxPos=" << ((float) zz_work_overshoot/_1MEG_F);
			LOG_TRACE( m_threadName, "zz", msg.str().c_str() );
			ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at( B_CONTROLLER );
			MotionAxis &axis = g_motionServices_ptr->getAxis("ZZ");
			/// Deliver input parameters.
			pController->binarySetFloat( MOVE_TO_FORCE_N_SN, (float) zz_work_force_newton  );
			pController->binarySetLong( CLR_KILLS_TIME_OUT_SN, DEFAULT_CLEAR_KILLS_TIMEOUT );
			pController->setVelAccelDecelStop( (float)zz_approach_speed/_1MEG_F, (float)acceleration/_1MEG_F, (float)deceleration/_1MEG_F, (float)stp/_1MEG_F );
			pController->binarySetFloat( ZZ_MAX_POSITION_SN, (float) zz_work_overshoot/_1MEG_F  );
			
			/// Start monitoring output parameters.
			pController->monitor( MOVE_TO_FORCE_ZZ_STATUS_SN, ACR7000::pvalue_type_t::pvalue_type_float, 100 ); /// Register for the returned status value
			pController->monitor( FORCE_POSITION_SN, ACR7000::pvalue_type_t::pvalue_type_float, 100 ); /// Register for the returned final position.
			usleep(600000);
			/// Start the prog0 routine by writing prog0_routineID to P0.
			
			///
			/// This block replaces a call to ACR7000::runProgram()
			///
			if (status == STATUS_OK)
			{
				pController->launchProgram( prog0_routineID, true );
			}
			int loopCount = 0;
			while ((status == STATUS_OK) && (!pController->programIsRunning( prog0_routineID )))
			{
				usleep(100000);
				if (loopCount++ > MAX_WAIT_FOR_START_LOOPS)
				{
					LOG_ERROR( m_threadName, "zz", "FAILED to start MoveToForceZZ" );
					status = STATUS_TIMEOUT_ERROR; // Break out of loop.
				}
			}

			/// If all went well, MoveToForceZZ is now running.
			while ((status == STATUS_OK) && ((pController->programIsRunning( prog0_routineID )) || (moveToForceZZStatus == 0.0)))
			{
				stringstream msg;
				float position_mm = axis.getActualPosition() / axis.getCachedPPU();
				msg << "|pos(mm)" << position_mm << "|";
				LOG_TRACE( m_threadName, "zz", msg.str().c_str() );
				moveToForceZZStatus = pController->getProgramStatus( prog0_routineID );
				usleep(100000);
				if (loopCount++ > MAX_WAIT_FOR_END_LOOPS)
				{
					LOG_ERROR( m_threadName, "zz", "MoveToForceZZ timed out" );
					status = STATUS_TIMEOUT_ERROR; // Break out of loop.
				}
			}
			///
			/// End of replacement for ACR7000::runProgram()
			///

			usleep(50000);
			/// Collect outputs
			forcePosition = pController->inspectPValueFloat((ACR7000::pvalue_index_t)FORCE_POSITION_SN, when);
			/// Stop monitoring the output parameters.
			pController->cancelMonitoring( prog0_routineID );
			pController->cancelMonitoring( FORCE_POSITION_SN );
		} catch (MotionException &x) {
			error_msg = x.getEventDesc();
			status = STATUS_MOVEMENT_FAILED;
		} catch (std::exception &ex) {
			error_msg = "Setting P0 failed";
			status = STATUS_DEVICE_ERROR;
		}
		if (true)
		{
			stringstream msg;
			msg << "moveToForceZZStatus=" << moveToForceZZStatus;
			LOG_DEBUG( m_threadName, "zz", msg.str().c_str() );
		}
		if (status != STATUS_OK)
		{
			stringstream msg;
			msg << "MoveToForceZZ failed|error_msg=" << error_msg;
			LOG_FATAL( m_threadName, "zz",  msg.str().c_str() );
			return status;			/// EARLY RETURN!	EARLY RETURN!
		}
		else if (moveToForceZZStatus == 0.0)
		{
			stringstream msg;
			msg << "MoveToForceZZ is still running! returning status " << moveToForceZZStatus;
			LOG_FATAL( m_threadName, "zz", msg.str().c_str() );
			status = STATUS_PROG_0_ERROR;
		}
		else if (moveToForceZZStatus == 1.0)
		{
			stringstream msg;
			msg << "MoveToForceZZ failed, returning status " << moveToForceZZStatus;
			LOG_FATAL( m_threadName, "zz", msg.str().c_str() );
			status = STATUS_PROG_0_ERROR;
		}
		else
		{
			m_states_long[ZZ_NM] = (long)(forcePosition * _1MEG_F);
			stringstream msg;
			msg << "MoveToForceZZ succeeded moving to " << m_states_long[ZZ_NM] << " nm";
			LOG_TRACE( m_threadName, "zz", msg.str().c_str() );
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "zz", "Wrong ZZ state" );
	}

	LOG_TRACE( m_threadName, "temp", "Implement_MoveToZZAxisForceNewton() end" );
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_MoveToZTipSeatingForceNewton
///		Assumes that the tip insert is already in position above the tip
/// seating location.
/// 	For step A below, let's define "Just above tip" mean
/// "ZAxis.z_to_top_of_seating_key_height + <tip_working_length> +
/// ZAxis.tip_seating_slowdown_offset".
///
/// A. Moves Z down at set-speed to just above tip.
/// B. Calls DoleOut_MoveToZAxisSeatingNewton to slowly seat the tip.
/// C. Moves Z up at set-speed to safe height.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveToZTipSeatingForceNewton()
{
	bool succeeded = true;
	int status = STATUS_OK;

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string(payloadObj);
		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "CxD", "FAILED to find payload object" );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	/// Extract needed input parameters from payload
	TipType tipType = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );

	long z_safeHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_SAFE_HEIGHT, z_safeHeight_nm );

	long tipSeatingSlowdownOffset_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_SEATING_SLOWDOWN_OFFSET, tipSeatingSlowdownOffset_nm );

	long z_toTopOfSeatingKeyHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_TO_TOP_OF_SEATING_KEY_HEIGHT, z_toTopOfSeatingKeyHeight_nm );

	long z_approachStartPos_nm = z_toTopOfSeatingKeyHeight_nm + tipType.tipRackSeatToTopOfTip - tipType.tipRackSeatToTipSeat + tipSeatingSlowdownOffset_nm;

	/// Move Z axis down to a position just above a tip.
	if (succeeded)
	{
		LOG_TRACE( m_threadName, "z", "Moving Z to just above tip" );
		status = Implement_moveToZNm( z_approachStartPos_nm ); /// Move to just above tip.
	}
	succeeded &= (status == STATUS_OK);

	/// Call prog0 routine MoveToForceZ,
	/// which should move at the slower approach speed until the 
	/// measured force indicates success.
	if (succeeded)
	{
		status = DoleOut_MoveToZAxisSeatingForceNewton();
	}
	succeeded &= (status == STATUS_OK);

	/// Return Z to safe height.
	if (succeeded)
	{
		LOG_TRACE( m_threadName, "z", "Returning Z to safety" );
		status = Implement_moveToZNm( z_safeHeight_nm );
	}
	else
	{
		LOG_WARN( m_threadName, "z", "Returning Z to safety AFTER ERROR" );
		int stat = Implement_moveToZNm( z_safeHeight_nm ); /// Stat is local so the failing status will be returned.
	}

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_MoveToZTipWellBottomForceNewton
/// Moves the Z tip down until it experiences a specified force on the axis or reaches the specified error offset.
/// If it reaches the specified error offset before experiencing the force, an error is generated.
///
/// A.	MoveToZNM(ZAxis.z_to_top_of_xy_stage_height + tip_working_length + plate_height – well_inner_height/1.1)
/// B.	*Set the Z axis speed to the [ZAxis.tip_find_well_bottom_speed]
/// C.	MoveToZAxisWellBottomForceNewton(ZAxis.z_to_top_of_xy_stage_height + tip_working_length + plate_height – well_inner_height - ZAxis.tip_find_well_bottom_overshoot ))
/// D.	*Set Z axis speed to ZAxis.z_set_speed
/// E.	well_bottom_height_nm = GetCurrentZNM()
/// F.	If (MoveToZAxisWellBottomForceNewton returned error) Then <throw error> End If
/// G.	MoveZToSafeHeight()
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveToZTipWellBottomForceNewton()
{
	int status = STATUS_OK;
	LOG_TRACE( m_threadName, "z", "DoleOut_MoveToZTipWellBottomForceNewton() called." );
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "z", "DoleOut_MoveToZTipWellBottomForceNewton() sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		// TODO: Delete this temporary debug.
		string payloadStr = json_object_to_json_string( payloadObj );
		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "z", "Payload is missing" );
		return STATUS_CMD_MSG_DATA_MISSING;			/// EARLY RETURN!	EARLY RETURN!
	}
	LOG_TRACE( m_threadName, "z", "-" );

	bool succeeded = true;
	float tip_find_well_bottom_force_newton = 0.0;
	long z_to_top_of_xy_stage_height = 0L;
	unsigned long tip_find_well_bottom_speed = 0L;
	long tip_find_well_bottom_overshoot = 0L;
	long z_safeHeight_nm = 0L;
	long z_velocity = 0L; /// Will not be used, but DesHelper::ExtractSpeedsAndLimits needs a velocity input parameter.
	long z_acceleration = 0L;
	long z_deceleration = 0L;
	long z_stp = 0L;
	long z_axisResolution_nm = 0L;
	long softTravelLimitPlus_nm = 0L;
	long softTravelLimitMinus_nm = 0L;
	long well_bottom_height_nm = 0L;
	bool raise_tip_at_end = true;
	/// Extract values from the payload.
	if (payloadObj) /// See above for error reporting if payload is missing.
	{
		succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, TIP_FIND_WELL_BOTTOM_FORCE, tip_find_well_bottom_force_newton );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_TO_TOP_OF_XY_STAGE_HEIGHT, z_to_top_of_xy_stage_height );
		succeeded &= m_desHelper->ExtractULongValue( m_threadName, payloadObj, TIP_FIND_WELL_BOTTOM_SPEED, tip_find_well_bottom_speed );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_FIND_WELL_BOTTOM_OVERSHOOT, tip_find_well_bottom_overshoot );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_SAFE_HEIGHT, z_safeHeight_nm );
		succeeded &= m_desHelper->ExtractBoolValue( m_threadName, payloadObj, RAISE_TIP_AT_END, raise_tip_at_end );
		succeeded &= m_desHelper->ExtractSpeedsAndLimits( m_threadName, m_commandMessageInObj, "z",
														  z_velocity, z_acceleration, z_deceleration, z_stp, z_axisResolution_nm,
														  softTravelLimitPlus_nm, softTravelLimitMinus_nm );
	}
	LOG_TRACE( m_threadName, "z", "-" );

	TipType tipType = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );
	PlateType plateType = this->m_desHelper->ExtractPayloadPlateType( m_commandMessageInObj );
	if (!succeeded)
	{
		LOG_ERROR( m_threadName, "temp", "Not succeeding" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}

	/// A.	MoveToZNM(ZAxis.z_to_top_of_xy_stage_height + tip_working_length + plate_height – well_inner_height/1.1)
	float modedWellInnerHeight_float = static_cast<float>(plateType.wellInnerHeight) / 1.1;
	int modedWellInnerHeight_int = static_cast<int>( std::round( modedWellInnerHeight_float ) );
	long z_launch_position_nm = z_to_top_of_xy_stage_height + tipType.tipWorkingLength + plateType.plateHeight - modedWellInnerHeight_int;
	status = Implement_moveToZNm( z_launch_position_nm );
	LOG_TRACE( m_threadName, "z", "-" );

	/// B.	*Set the Z axis speed to the [ZAxis.tip_find_well_bottom_speed]
	/// C.	MoveToZAxisWellBottomForceNewton(ZAxis.z_to_top_of_xy_stage_height + tip_working_length + plate_height – well_inner_height - ZAxis.tip_find_well_bottom_overshoot ))
	if (status == STATUS_OK)
	{
		long z_target_overshoot_nm = z_to_top_of_xy_stage_height + tipType.tipWorkingLength + plateType.plateHeight - plateType.wellInnerHeight - tip_find_well_bottom_overshoot;
		if (true)
		{
			stringstream msg;
			msg << MOVE_TO_Z_TIP_WELL_BOTTOM_FORCE_NEWTON << " calculated z_launch_position_nm=" << z_launch_position_nm << "nm";
			msg << "|z_target_overshoot_nm=" << z_target_overshoot_nm << "nm";
			LOG_DEBUG( m_threadName, "z", msg.str().c_str() );
		}
		if ((softTravelLimitMinus_nm > z_target_overshoot_nm) || (z_target_overshoot_nm > softTravelLimitPlus_nm))
		{
			stringstream msg;
			msg << "Z move is out of (soft-limit) bounds|minus=" << softTravelLimitMinus_nm << "|Zmax=" << z_target_overshoot_nm << "|plus=" << softTravelLimitPlus_nm;
			LOG_ERROR( m_threadName, "z", msg.str().c_str() );
			return STATUS_MOTION_FORBIDDEN;			/// EARLY RETURN!	EARLY RETURN!
		}
		LOG_TRACE( m_threadName, "z", "-" );

		FreezeWorkInProgress();
		status = Implement_MoveToZAxisForceNewton(	tip_find_well_bottom_speed, /// tip_pickup_speed or ...
													tip_find_well_bottom_force_newton, /// tip_pickup_force or ...
													z_target_overshoot_nm, /// tip_pickup_overshoot or ...
													z_acceleration,
													z_deceleration,
													z_stp,
													z_axisResolution_nm );
	}
	if (status == STATUS_MOVEMENT_FAILED)
	{
		LOG_TRACE( m_threadName, "z", "-" );
		/// Send an alert.
		stringstream topic_stream;
		topic_stream << TOPIC_ES << CHANNEL_ALERT;

		/// Collect fields from the command's header for use in the alert.
		std::string messageType = "";
		int commandGroupId = 0;
		int commandId = 0;
		std::string sessionId = "";
		std::string machineId = "";
		bool success =   m_desHelper->SplitCommand( m_commandMessageInObj,
													&messageType,
													nullptr, // &transportVersion,
													&commandGroupId,
													&commandId,
													&sessionId,
													&machineId );

		json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
															commandGroupId,
															commandId,
															sessionId, // Or topic_stream.str()?
															ComponentType::ZAxis,
															STATUS_AXIS_Z_FAULTED,
															false ); /// thisIsCausingHold
		bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
	}
	
	if (status == STATUS_OK)
	{
		/// E.	well_bottom_height_nm = GetCurrentZNM()
		well_bottom_height_nm = m_states_long[Z_NM];
		/// D.	*Set Z axis speed to ZAxis.z_set_speed
		/// G.	MoveZToSafeHeight()
		if (raise_tip_at_end)
		{
			float raiseHeight_float = static_cast<float>(well_bottom_height_nm) + (static_cast<float>(plateType.wellInnerHeight + 1000000));
			int raiseHeight_int = static_cast<int>( std::round( raiseHeight_float ) );
			LOG_TRACE( m_threadName, "z", "Returning Z to top of well" );
			status = Implement_moveToZNm( raiseHeight_int );
		}
	}
	else
	{
		/// E.	well_bottom_height_nm = 0 for error condition.
		well_bottom_height_nm = 0;
		/// D.	*Set Z axis speed to ZAxis.z_set_speed
		/// G.	MoveZToSafeHeight()
		LOG_WARN( m_threadName, "z", "Returning Z to safety AFTER ERROR" );
		int stat = Implement_moveToZNm( z_safeHeight_nm ); /// Stat is local so the failing status will be returned.
	}

	/// F.	If (MoveToZAxisWellBottomForceNewton returned error) Then <throw error> End If
	/// Populate the status payload
    int resultZ = json_object_object_add( m_statusPayloadOutObj, WELL_BOTTOM_HEIGHT_NM, json_object_new_int64( well_bottom_height_nm ) );
	if (resultZ != 0)
	{
		if (status == STATUS_OK)
		{
			status = STATUS_CMD_MSG_ERROR;
		}
		stringstream msg;
		msg << "Failed to pack well_bottom_height_nm=" << well_bottom_height_nm << " into status payload";
		LOG_ERROR( m_threadName, "z", msg.str().c_str() );
	}
	return status;
}


///
/// ZZ Axis Motion
///

///----------------------------------------------------------------------
/// DoleOut_moveToZZNm
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_moveToZZNm()
{
	/// Extract zz from payload.
	long zz_nm = this->m_desHelper->ExtractPayloadLongValue( m_commandMessageInObj, ZZ_NM );

	int status = Implement_moveToZZNm( zz_nm ); /// Assume that speeds are already packed in the payload.

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_moveToZZRelativeNm
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_moveToZZRelativeNm()
{
	/// Extract zz from payload.
	long zz_offset_nm = this->m_desHelper->ExtractPayloadLongValue( m_commandMessageInObj, ZZ_OFFSET_NM );
	long zz_nm = m_states_long[ZZ_NM] + zz_offset_nm;

	int status = Implement_moveToZZNm( zz_nm ); /// Assume that speeds are already packed in the payload.

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_retractToZZNm
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_retractToZZNm()
{
	/// Extract zz from payload.
	long retraction_height_nm = this->m_desHelper->ExtractPayloadLongValue( m_commandMessageInObj, RETRACTION_HEIGHT_NM );

	int status = Implement_moveToZZNm( retraction_height_nm ); /// Assume that speeds are already packed in the payload.

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// Implement_moveToZZNm
///----------------------------------------------------------------------
int CommandDolerThread::Implement_moveToZZNm(long zz_nm)
{
	return Implement_moveToZZNm( zz_nm, INVALID_SPEED );
}

int CommandDolerThread::Implement_moveToZZNm(long zz_nm, long speed)
{
	int status = STATUS_OK;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "zz", "MoveToZZNm sent to wrong thread" );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState ZZState = g_ZZState.load( std::memory_order_acquire );

	if (ZZState == ComponentState::Mocked)
	{
		m_states_long[ZZ_NM] = zz_nm;
		status = STATUS_OK;
	}
	else if ((ZZState == ComponentState::Good) || (ZZState == ComponentState::Homed1) || (ZZState == ComponentState::Homed3))
	{
		long velocity = 0L;
		long acceleration = 0L;
		long deceleration = 0L;
		long stp = 0L;
		long axisResolution_nm = 0L;
		long softTravelLimitPlus_nm = 0L;
		long softTravelLimitMinus_nm = 0L;
		if (!m_desHelper->ExtractSpeedsAndLimits( m_threadName, m_commandMessageInObj, "zz",
										 velocity, acceleration, deceleration, stp, axisResolution_nm,
										 softTravelLimitPlus_nm, softTravelLimitMinus_nm ))
		{
			return STATUS_NOT_FOUND;			/// EARLY RETURN!	EARLY RETURN!
		}
		if ((softTravelLimitMinus_nm > zz_nm) || (zz_nm > softTravelLimitPlus_nm))
		{
			stringstream msg;
			msg << "ZZ move is out of (soft-limit) bounds|minus=" << softTravelLimitMinus_nm << "|ZZ=" << zz_nm << "|plus=" << softTravelLimitPlus_nm;
			LOG_ERROR( m_threadName, "zz", msg.str().c_str() );
			return STATUS_MOTION_FORBIDDEN;			/// EARLY RETURN!	EARLY RETURN!
		}
		if (speed != INVALID_SPEED)
		{
			/// Override the default set_speed with the supplied one.
			velocity = speed;
		}
		FreezeWorkInProgress();
		unique_lock<mutex> lock( g_controllerBMutex ); /// Prevent the AnnularRingHolder thread from moving at the same time.
		status = Implement_MoveOneAxis( "ZZ", zz_nm, axisResolution_nm,
										velocity, acceleration, deceleration, stp );
		m_states_long[ZZ_NM] = zz_nm;
		if (status == STATUS_MOVEMENT_FAILED)
		{
			/// Send an alert.
			stringstream topic_stream;
			topic_stream << TOPIC_ES << CHANNEL_ALERT;

			/// Collect fields from the command's header for use in the alert.
			std::string messageType = "";
			int commandGroupId = 0;
			int commandId = 0;
			std::string sessionId = "";
			std::string machineId = "";
			bool succeeded = m_desHelper->SplitCommand( m_commandMessageInObj,
														&messageType,
														nullptr, // &transportVersion,
														&commandGroupId,
														&commandId,
														&sessionId,
														&machineId );

			json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
																commandGroupId,
																commandId,
																sessionId, // Or topic_stream.str()?
																ComponentType::ZZAxis,
																STATUS_AXIS_ZZ_FAULTED,
																false ); /// thisIsCausingHold
			bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "zz", "Wrong ZZ state" );
	}

	return status;
}

///----------------------------------------------------------------------
/// DoleOut_getCurrentZZNm
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_getCurrentZZNm()
{
	int status = STATUS_OK;
	long zz_nm = 0L;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "zz", "GetCurrentZZNM sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState ZZState = g_ZZState.load( std::memory_order_acquire );

	if (ZZState == ComponentState::Mocked)
	{
		zz_nm = m_states_long[ZZ_NM];
	}
	else if ((ZZState == ComponentState::Good) || (ZZState == ComponentState::Homed3))
	{
		status = Implement_GetAxisPosition( "zz", zz_nm );
		if (zz_nm != m_states_long[ZZ_NM])
		{
			stringstream msg;
			msg << "ZZ measured-pos=" << zz_nm << "|recorded-pos=" << m_states_long[ZZ_NM];
			LOG_WARN( m_threadName, "zz", msg.str().c_str() );
			m_states_long[ZZ_NM] = zz_nm;
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "zz", "Wrong ZZ state" );
	}

	/// Populate the status payload
    int result = json_object_object_add( m_statusPayloadOutObj, ZZ_NM, json_object_new_int64( zz_nm ) );
	if ((status == STATUS_OK) && (result != 0))
	{
		/// populating the payload failed and nothing else did
		status = STATUS_CMD_MSG_ERROR;
	}
	return status;
}

int CommandDolerThread::DoleOut_setZZAxisSpeed()
{
	int status = STATUS_OK;
	/// Extract zz_axis_speed from payload
	unsigned long zz_axisSpeed = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, ZZ_AXIS_SPEED );
//	cout << m_threadName << ": DoleOut_setZAxisSpeed() received zz_axis_speed of " << zz_axisSpeed << endl;
	// TODO: Implement more here.
	FreezeWorkInProgress();
	;
	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_MoveToZZAxisForceNewton
/// 	Moves the ZZ axis down until it either senses a specified force on
/// its axis force sensor or reaches a specified offset.
/// Uses the ZZAxis.tip_pickup_force and ZZAxis.zz_to_top_of_yy_stage_height +
/// tip_rack_seat_to_top_of_tip - (tip_total_length - tip_working_length) -
/// ZZAxis.tip_pickup_overshoot height.
/// 	Returns error status if ZZ Axis reaches the specified offset before
/// sensing the specified force.
///		Can be called from DoWork().
///		Is also called from DoleOut_MoveToZZTipPickForceNewton()
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveToZZAxisForceNewton()
{
	int status = STATUS_OK;
	/// Extract values from the payload.
	bool succeeded = true;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string( payloadObj );
		cout << payloadStr << endl;

		long tipPickupSpeed = 0L;
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_PICKUP_SPEED, tipPickupSpeed );
		long tipPickupForce = 0L;
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_PICKUP_FORCE, tipPickupForce );
		long tipPickupOvershoot = 0L;
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_PICKUP_OVERSHOOT, tipPickupOvershoot );
		long zz_toTopOfYYStageHeight_nm = 0L;
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_TO_TOP_OF_YY_STAGE_HEIGHT, zz_toTopOfYYStageHeight_nm );
		TipType tipType = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );
		long zz_velocity = 0L; /// Will not be used, but DesHelper::ExtractSpeedsAndLimits needs a velocity input parameter.
		long zz_acceleration = 0L;
		long zz_deceleration = 0L;
		long zz_stp = 0L;
		long zz_axisResolution_nm = 0L;
		long softTravelLimitPlus_nm = 0L;
		long softTravelLimitMinus_nm = 0L;
		succeeded &= m_desHelper->ExtractSpeedsAndLimits( m_threadName, m_commandMessageInObj, "zz",
												zz_velocity, zz_acceleration, zz_deceleration, zz_stp, zz_axisResolution_nm,
												softTravelLimitPlus_nm, softTravelLimitMinus_nm );
		long maxPosition = zz_toTopOfYYStageHeight_nm + tipType.tipRackSeatToTopOfTip -
							(tipType.tipTotalLength - tipType.tipWorkingLength) -
							tipPickupOvershoot;
		if (true)
		{
			stringstream msg;
			msg << MOVE_TO_ZZ_AXIS_FORCE_NEWTON << " calculated max-position=" << maxPosition << "nm";
			msg << "|inputs were zz_toTopOfYYStageHeight_nm=" << zz_toTopOfYYStageHeight_nm;
			msg << "|tipType.tipRackSeatToTopOfTip=" << tipType.tipRackSeatToTopOfTip;
			msg << "|tipType.tipTotalLength=" << tipType.tipTotalLength;
			msg << "|tipType.tipWorkingLength=" << tipType.tipWorkingLength;
			msg << "|tipPickupOvershoot=" << tipPickupOvershoot;
			LOG_DEBUG( m_threadName, "zz", msg.str().c_str() );
		}
		if ((softTravelLimitMinus_nm > maxPosition) || (maxPosition > softTravelLimitPlus_nm))
		{
			stringstream msg;
			msg << "ZZ move is out of (soft-limit) bounds|minus=" << softTravelLimitMinus_nm << "|ZZmax=" << maxPosition << "|plus=" << softTravelLimitPlus_nm;
			LOG_ERROR( m_threadName, "zz", msg.str().c_str() );
			return STATUS_MOTION_FORBIDDEN;			/// EARLY RETURN!	EARLY RETURN!
		}

		FreezeWorkInProgress();
		status = Implement_MoveToZZAxisForceNewton(	tipPickupSpeed,
													tipPickupForce,
													maxPosition,
													zz_acceleration,
													zz_deceleration,
													zz_stp,
													zz_axisResolution_nm );
		if (!succeeded) LOG_ERROR( m_threadName, "zz", "Not succeeding" );
		long well_bottom_height_nm = m_states_long[ZZ_NM];
		/// Pack well_bottom_height_nm into the status payload.
		int result = json_object_object_add( m_statusPayloadOutObj, WELL_BOTTOM_HEIGHT_NM, json_object_new_int64( well_bottom_height_nm ) );
		succeeded &= (result == 0);
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "zz", "Failed to find payload" );
	}

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_MoveToZZAxisSeatingForceNewton
/// 	Moves the ZZ axis down until it either senses a specified force
/// while seating the tip or reaches a specified offset. Uses the ZZ axis
/// tip seating force and ZZAxis.zz_to_top_of_seating_key_height +
/// tip_total_length - tip_working_length - ZZaxis.tip_seating_overshoot
/// height.
///		Returns error status if ZZ Axis reaches the specified offset before
/// sensing the specified force.
///		Returns the tip_working_length_nm if the find seating force was successful.
/// The tip_working_length_nm = tip_type.tip_total_length -
/// (ZZAxis.zz_to_top_of_seating_key_height - measured_seating_height + tip_type.tip_shoulder_length).
///		Can be called from DoWork().
///		Is also called from DoleOut_MoveToZTipSeatingForceNewton()
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveToZZAxisSeatingForceNewton()
{
	int status = STATUS_OK;
	/// Extract values from the payload.
	bool succeeded = true;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		/// Extract values from the payload.
		long tipSeatingSpeed = 0L;
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_SEATING_SPEED, tipSeatingSpeed );
		float tipSeatingForce = 0.0;
		succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, TIP_SEATING_FORCE, tipSeatingForce );
		long tipSeatingOvershoot = 0L;
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_SEATING_OVERSHOOT, tipSeatingOvershoot );
		long zz_toTopOfSeatingKeyHeight = 0L;
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_TO_TOP_OF_SEATING_KEY_HEIGHT, zz_toTopOfSeatingKeyHeight );
		TipType tipType = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );
		long zz_velocity = 0L; /// Will not be used, but DesHelper::ExtractSpeedsAndLimits needs a velocity input parameter.
		long zz_acceleration = 0L;
		long zz_deceleration = 0L;
		long zz_stp = 0L;
		long zz_axisResolution_nm = 0L;
		long softTravelLimitPlus_nm = 0L;
		long softTravelLimitMinus_nm = 0L;
		succeeded &= m_desHelper->ExtractSpeedsAndLimits( m_threadName, m_commandMessageInObj, "zz",
												zz_velocity, zz_acceleration, zz_deceleration, zz_stp, zz_axisResolution_nm,
												softTravelLimitPlus_nm, softTravelLimitMinus_nm );
		long maxPosition = zz_toTopOfSeatingKeyHeight + tipType.tipRackSeatToTopOfTip - tipType.tipRackSeatToTipSeat - tipType.tipTotalLength + tipType.tipWorkingLength - tipSeatingOvershoot;
		if (true)
		{
			stringstream msg;
			msg << MOVE_TO_ZZ_AXIS_SEATING_FORCE_NEWTON << " calculated max-position=" << maxPosition << "nm";
			LOG_DEBUG( m_threadName, "zz", msg.str().c_str() );
		}
		if ((softTravelLimitMinus_nm > maxPosition) || (maxPosition > softTravelLimitPlus_nm))
		{
			LOG_ERROR( m_threadName, "zz", "Move is out of (soft-limit) bounds" );
			return STATUS_MOTION_FORBIDDEN;			/// EARLY RETURN!	EARLY RETURN!
		}

		FreezeWorkInProgress();
		status = Implement_MoveToZZAxisForceNewton(	tipSeatingSpeed,
													tipSeatingForce,
													maxPosition,
													zz_acceleration,
													zz_deceleration,
													zz_stp,
													zz_axisResolution_nm );
		succeeded &= (status == STATUS_OK);
		/// Calculate tipWorkingLength_nm whether we succeeded or not, for logging purposes.
		long tipWorkingLength_nm = tipType.tipTotalLength - (zz_toTopOfSeatingKeyHeight - m_states_long[ZZ_NM] + tipType.tipShoulderLength);
		if (true)
		{
			stringstream msg;
			msg << "tip_working_length_nm=" << tipWorkingLength_nm;
			LOG_TRACE( m_threadName, "zz", msg.str().c_str() );
		}
		if (succeeded)
		{
			/// Pack tipWorkingLength_nm into the outgoing payload.
			int resultZZ = json_object_object_add( m_statusPayloadOutObj, TIP_WORKING_LENGTH_NM, json_object_new_int64( tipWorkingLength_nm ) );
			if (resultZZ != 0)
			{
				status = STATUS_CMD_MSG_ERROR;
				LOG_ERROR( m_threadName, "zz", "Failed to pack tip_working_length_nm into status payload" );
			}
		}
		else
		{
			LOG_ERROR( m_threadName, "zz", "Not succeeding" );
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "zz", "Failed to find payload" );
	}

	/// This status needs no payload.
	return status;
}

int CommandDolerThread::DoleOut_MoveToZZAxisWellBottomForceNewton()
{
	int status = STATUS_OK;
	/// Extract error_height" from payload.
	unsigned long error_height = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, ERROR_HEIGHT );
//	cout << m_threadName << ": DoleOut_MoveToZZAxisWellBottomForceNewton() received error_height of " << error_height << endl;
	/// Extract (z_axis) "tip_find_well_bottom_force" from payload.
	double tip_find_well_bottom_force = this->m_desHelper->ExtractPayloadDoubleValue( m_commandMessageInObj, TIP_FIND_WELL_BOTTOM_FORCE );
//	cout << m_threadName << ": DoleOut_MoveToZZAxisWellBottomForceNewton() received tip_find_well_bottom_force of " << tip_find_well_bottom_force << endl;
	// TODO: Implement more here.
	FreezeWorkInProgress();
	;
	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_MoveToZZTipPickForceNewton
///		Moves the ZZ tip down until it experiences a specified force on the
/// axis or reaches the ZZ tip pickup height property. If it reaches the ZZ
/// tip pickup height property before experiencing the force, an error is
/// generated.
///		Assumes that the tip insert is already in position above the tip
/// location. (Presume it is at ZZ safe height.)
/// 	For step B below, let's define "Just above tip" mean
/// ZAxis.z_to_top_of_YY_stage_height + tip_rack_seat_to_top_of_tip +
/// ZAxis.tip_pickup_slowdown_offset"
///
/// A. Moves Z down at set-speed to just above tip.
/// B. Calls DoleOut_MoveToZZAxisForceNewton to slowly pick the tip.
/// C. Moves Z up at set-speed to safe height.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveToZZTipPickForceNewton()
{
	bool succeeded = true;
	int status = STATUS_OK;

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string(payloadObj);
		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "CxD", "FAILED to find payload object" );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	/// Extract needed input parameters from payload
	TipType tipType = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );

	long zz_safeHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_SAFE_HEIGHT, zz_safeHeight_nm );

	long tipPickupSlowdownOffset_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_PICKUP_SLOWDOWN_OFFSET, tipPickupSlowdownOffset_nm );

	long zz_toTopOfYYStageHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_TO_TOP_OF_YY_STAGE_HEIGHT, zz_toTopOfYYStageHeight_nm );
	
	long zz_approachStartPos_nm = zz_toTopOfYYStageHeight_nm + tipType.tipRackSeatToTopOfTip + tipPickupSlowdownOffset_nm;
	if (true)
	{
		stringstream msg;
		msg << MOVE_TO_ZZ_TIP_PICK_FORCE_NEWTON << " calculated just-above-tip=" << zz_approachStartPos_nm << "nm";
		LOG_DEBUG( m_threadName, "zz", msg.str().c_str() );
	}
	
	/// Move ZZ axis down to a position just above a tip.
	if (succeeded)
	{
		LOG_TRACE( m_threadName, "zz", "Moving ZZ to just above tip" );
		status = Implement_moveToZZNm( zz_approachStartPos_nm ); /// Move to just above tip.
	}
	succeeded &= (status == STATUS_OK);

	/// Call prog0 routine MoveToForceZZ,
	/// which should move at the slower approach speed until the 
	/// measured force indicates success.
	if (succeeded)
	{
		status = DoleOut_MoveToZZAxisForceNewton();
	}
	succeeded &= (status == STATUS_OK);

	/// Return ZZ to safe height.
	if (succeeded)
	{
		LOG_TRACE( m_threadName, "zz", "Returning ZZ to safety" );
		status = Implement_moveToZZNm( zz_safeHeight_nm );
	}
	else
	{
		LOG_WARN( m_threadName, "zz", "Returning ZZ to safe heights AFTER ERROR" );
		int stat = Implement_moveToZZNm( zz_safeHeight_nm ); /// Stat is local so the failing status will be returned.
	}

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_MoveToZZTipSeatingForceNewton
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveToZZTipSeatingForceNewton()
{
	bool succeeded = true;
	int status = STATUS_OK;

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string(payloadObj);
		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "CxD", "FAILED to find payload object" );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	/// Extract needed input parameters from payload
	TipType tipType = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );

	long zz_safeHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_SAFE_HEIGHT, zz_safeHeight_nm );

	long tipSeatingSlowdownOffset_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_SEATING_SLOWDOWN_OFFSET, tipSeatingSlowdownOffset_nm );

	long zz_toTopOfSeatingKeyHeight_nm = 0L;
	succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_TO_TOP_OF_SEATING_KEY_HEIGHT, zz_toTopOfSeatingKeyHeight_nm );

	long zz_approachStartPos_nm = zz_toTopOfSeatingKeyHeight_nm + tipType.tipRackSeatToTopOfTip - tipType.tipRackSeatToTipSeat + tipSeatingSlowdownOffset_nm;

	/// Move Z axis down to a position just above a tip.
	if (succeeded)
	{
		LOG_TRACE( m_threadName, "zz", "Moving ZZ to just above tip" );
		status = Implement_moveToZZNm( zz_approachStartPos_nm ); /// Move to just above tip.
	}
	succeeded &= (status == STATUS_OK);

	/// Call prog0 routine MoveToForceZZ,
	/// which should move at the slower approach speed until the 
	/// measured force indicates success.
	if (succeeded)
	{
		status = DoleOut_MoveToZZAxisSeatingForceNewton();
	}
	succeeded &= (status == STATUS_OK);

	/// Return Z to safe height.
	if (succeeded)
	{
		LOG_TRACE( m_threadName, "zz", "Returning ZZ to safety" );
		status = Implement_moveToZZNm( zz_safeHeight_nm );
	}
	else
	{
		LOG_TRACE( m_threadName, "zz", "Returning ZZ to safety AFTER ERROR" );
		int stat = Implement_moveToZZNm( zz_safeHeight_nm ); /// Stat is local so the failing status will be returned.
	}

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_MoveToZZTipWellBottomForceNewton
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveToZZTipWellBottomForceNewton()
{
	int status = STATUS_OK;
	LOG_TRACE( m_threadName, "zz", "DoleOut_MoveToZZTipWellBottomForceNewton() called." );
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "zz", "DoleOut_MoveToZZTipWellBottomForceNewton() sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		// TODO: Delete this temporary debug.
		string payloadStr = json_object_to_json_string( payloadObj );
		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "zz", "Payload is missing" );
		return STATUS_CMD_MSG_DATA_MISSING;			/// EARLY RETURN!	EARLY RETURN!
	}
	LOG_TRACE( m_threadName, "zz", "-" );

	bool succeeded = true;
	float tip_find_well_bottom_force_newton = 0.0;
	long zz_to_top_of_xy_stage_height = 0L;
	unsigned long tip_find_well_bottom_speed = 0L;
	long tip_find_well_bottom_overshoot = 0L;
	long zz_safeHeight_nm = 0L;
	long zz_velocity = 0L; /// Will not be used, but DesHelper::ExtractSpeedsAndLimits needs a velocity input parameter.
	long zz_acceleration = 0L;
	long zz_deceleration = 0L;
	long zz_stp = 0L;
	long zz_axisResolution_nm = 0L;
	long softTravelLimitPlus_nm = 0L;
	long softTravelLimitMinus_nm = 0L;
	long well_bottom_height_nm = 0L;
	bool raise_tip_at_end = true;
	/// Extract values from the payload.
	if (payloadObj) /// See above for error reporting if payload is missing.
	{
		succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, TIP_FIND_WELL_BOTTOM_FORCE, tip_find_well_bottom_force_newton );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_TO_TOP_OF_XY_STAGE_HEIGHT, zz_to_top_of_xy_stage_height );
		succeeded &= m_desHelper->ExtractULongValue( m_threadName, payloadObj, TIP_FIND_WELL_BOTTOM_SPEED, tip_find_well_bottom_speed );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_FIND_WELL_BOTTOM_OVERSHOOT, tip_find_well_bottom_overshoot );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_SAFE_HEIGHT, zz_safeHeight_nm );
		succeeded &= m_desHelper->ExtractBoolValue( m_threadName, payloadObj, RAISE_TIP_AT_END, raise_tip_at_end );
		succeeded &= m_desHelper->ExtractSpeedsAndLimits( m_threadName, m_commandMessageInObj, "zz",
														  zz_velocity, zz_acceleration, zz_deceleration, zz_stp, zz_axisResolution_nm,
														  softTravelLimitPlus_nm, softTravelLimitMinus_nm );
	}
	LOG_TRACE( m_threadName, "zz", "-" );

	TipType tipType = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );
	PlateType plateType = this->m_desHelper->ExtractPayloadPlateType( m_commandMessageInObj );
	if (!succeeded)
	{
		LOG_ERROR( m_threadName, "temp", "Not succeeding" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}

	/// A.	MoveToZZNM(ZZAxis.zz_to_top_of_xy_stage_height + tip_working_length + plate_height – well_inner_height/1.1)
	float modedWellInnerHeight_float = static_cast<float>(plateType.wellInnerHeight) / 1.1;
	int modedWellInnerHeight_int = static_cast<int>( std::round( modedWellInnerHeight_float ) );
	long zz_launch_position_nm = zz_to_top_of_xy_stage_height + tipType.tipWorkingLength + plateType.plateHeight - modedWellInnerHeight_int;
	status = Implement_moveToZZNm( zz_launch_position_nm );
	LOG_TRACE( m_threadName, "zz", "-" );

	/// B.	*Set the ZZ axis speed to the [ZZAxis.tip_find_well_bottom_speed]
	/// C.	MoveToZZAxisWellBottomForceNewton(ZZAxis.zz_to_top_of_xy_stage_height + tip_working_length + plate_height – well_inner_height - ZZAxis.tip_find_well_bottom_overshoot ))
	if (status == STATUS_OK)
	{
		long zz_target_overshoot_nm = zz_to_top_of_xy_stage_height + tipType.tipWorkingLength + plateType.plateHeight - plateType.wellInnerHeight - tip_find_well_bottom_overshoot;
		if (true)
		{
			stringstream msg;
			msg << MOVE_TO_ZZ_TIP_WELL_BOTTOM_FORCE_NEWTON << " calculated zz_launch_position_nm=" << zz_launch_position_nm << "nm";
			msg << "|zz_target_overshoot_nm=" << zz_target_overshoot_nm << "nm";
			LOG_DEBUG( m_threadName, "zz", msg.str().c_str() );
		}
		if ((softTravelLimitMinus_nm > zz_target_overshoot_nm) || (zz_target_overshoot_nm > softTravelLimitPlus_nm))
		{
			stringstream msg;
			msg << "ZZ move is out of (soft-limit) bounds|minus=" << softTravelLimitMinus_nm << "|ZZmax=" << zz_target_overshoot_nm << "|plus=" << softTravelLimitPlus_nm;
			LOG_ERROR( m_threadName, "zz", msg.str().c_str() );
			return STATUS_MOTION_FORBIDDEN;			/// EARLY RETURN!	EARLY RETURN!
		}
		LOG_TRACE( m_threadName, "zz", "-" );

		FreezeWorkInProgress();
		status = Implement_MoveToZZAxisForceNewton(	tip_find_well_bottom_speed, /// tip_pickup_speed or ...
													tip_find_well_bottom_force_newton, /// tip_pickup_force or ...
													zz_target_overshoot_nm, /// tip_pickup_overshoot or ...
													zz_acceleration,
													zz_deceleration,
													zz_stp,
													zz_axisResolution_nm );
	}
	if (status == STATUS_MOVEMENT_FAILED)
	{
		LOG_TRACE( m_threadName, "zz", "-" );
		/// Send an alert.
		stringstream topic_stream;
		topic_stream << TOPIC_ES << CHANNEL_ALERT;

		/// Collect fields from the command's header for use in the alert.
		std::string messageType = "";
		int commandGroupId = 0;
		int commandId = 0;
		std::string sessionId = "";
		std::string machineId = "";
		bool success =   m_desHelper->SplitCommand( m_commandMessageInObj,
													&messageType,
													nullptr, // &transportVersion,
													&commandGroupId,
													&commandId,
													&sessionId,
													&machineId );

		json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
															commandGroupId,
															commandId,
															sessionId, // Or topic_stream.str()?
															ComponentType::ZAxis,
															STATUS_AXIS_ZZ_FAULTED,
															false ); /// thisIsCausingHold
		bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
	}
	
	if (status == STATUS_OK)
	{
		/// E.	well_bottom_height_nm = GetCurrentZZNM()
		well_bottom_height_nm = m_states_long[ZZ_NM];
		/// D.	*Set ZZ axis speed to ZZAxis.zz_set_speed
		/// G.	MoveZZToSafeHeight()
		if (raise_tip_at_end)
		{
			float raiseHeight_float = static_cast<float>(well_bottom_height_nm) + (static_cast<float>(plateType.wellInnerHeight) * 1.1);
			int raiseHeight_int = static_cast<int>( std::round( raiseHeight_float ) );
			LOG_TRACE( m_threadName, "zz", "Returning ZZ to top of well" );
			status = Implement_moveToZZNm( raiseHeight_int );
		}
	}
	else
	{
		/// E.	well_bottom_height_nm = 0 on error.
		well_bottom_height_nm = 0;
		/// D.	*Set ZZ axis speed to ZZAxis.zz_set_speed
		/// G.	MoveZZToSafeHeight()
		LOG_WARN( m_threadName, "zz", "Returning ZZ to safety AFTER ERROR" );
		int stat = Implement_moveToZZNm( zz_safeHeight_nm ); /// Stat is local so the failing status will be returned.
	}

	/// F.	If (MoveToZZAxisWellBottomForceNewton returned error) Then <throw error> End If
	/// Populate the status payload
    int resultZZ = json_object_object_add( m_statusPayloadOutObj, WELL_BOTTOM_HEIGHT_NM, json_object_new_int64( well_bottom_height_nm ) );
	if (resultZZ != 0)
	{
		if (status == STATUS_OK)
		{
			status = STATUS_CMD_MSG_ERROR;
		}
		stringstream msg;
		msg << "Failed to pack well_bottom_height_nm=" << well_bottom_height_nm << " into status payload";
		LOG_ERROR( m_threadName, "zz", msg.str().c_str() );
	}
	return status;
}


int CommandDolerThread::DoleOut_MoveZZToSafeHeight()
{
	int status = STATUS_OK;
	/// Get the payload object.
	json_object * payloadObj = nullptr;
	if (!json_object_object_get_ex( m_commandMessageInObj, PAYLOAD, &payloadObj ))
	{
		LOG_ERROR( m_threadName, "zz", "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string( payloadObj );
		cout << payloadStr << endl;
	}
	long zz_safeHeight_nm = 0L;
	long zz_setSpeed = 0L;
	if (status == STATUS_OK)
	{
		/// Extract parameters from payload.
		if (!m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_SAFE_HEIGHT, zz_safeHeight_nm ))
		{
			LOG_ERROR( m_threadName, "zz", "zz_safe_height is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		if (!m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_SET_SPEED, zz_setSpeed ))
		{
			LOG_WARN( m_threadName, "zz", "zz_set_speed is missing" );
		}
	}
	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "zz", "Moving ZZ to safety" );
		if (zz_setSpeed == 0L)
		{
			status = Implement_moveToZZNm( zz_safeHeight_nm );
		}
		else
		{
			status = Implement_moveToZZNm( zz_safeHeight_nm, zz_setSpeed );
		}
	}
	return status;
}

///
/// ZZZ Axis Motion
///

///----------------------------------------------------------------------
/// DoleOut_moveToZZZNm
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_moveToZZZNm()
{
	/// Extract zzz from payload.
	long zzz_nm = this->m_desHelper->ExtractPayloadLongValue( m_commandMessageInObj, ZZZ_NM );

	int status = Implement_moveToZZZNm( zzz_nm ); /// Assume that speeds are already packed in the payload.

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_moveToZZZRelativeNm
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_moveToZZZRelativeNm()
{
	/// Extract zz from payload.
	long zzz_offset_nm = this->m_desHelper->ExtractPayloadLongValue( m_commandMessageInObj, ZZZ_OFFSET_NM );
	long zzz_nm = m_states_long[ZZZ_NM] + zzz_offset_nm;

	int status = Implement_moveToZZZNm( zzz_nm ); /// Assume that speeds are already packed in the payload.

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// Implement_moveToZZZNm
///----------------------------------------------------------------------
int CommandDolerThread::Implement_moveToZZZNm( long zzz_nm )
{
	return Implement_moveToZZZNm( zzz_nm, INVALID_SPEED );
}

int CommandDolerThread::Implement_moveToZZZNm( long zzz_nm, long speed )
{
	int status = STATUS_OK;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "zzz", "MoveToZZZNm sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState ZZZState = g_ZZZState.load( std::memory_order_acquire );

	if (ZZZState == ComponentState::Mocked)
	{
		m_states_long[ZZZ_NM] = zzz_nm;
		status = STATUS_OK;
	}
	else if ((ZZZState == ComponentState::Good) || (ZZZState == ComponentState::Homed1) || (ZZZState == ComponentState::Homed3))
	{
		long velocity = 0L;
		long acceleration = 0L;
		long deceleration = 0L;
		long stp = 0L;
		long axisResolution_nm = 0L;
		long softTravelLimitPlus_nm = 0L;
		long softTravelLimitMinus_nm = 0L;
		if (!m_desHelper->ExtractSpeedsAndLimits( m_threadName, m_commandMessageInObj, "zzz",
										 velocity, acceleration, deceleration, stp, axisResolution_nm,
										 softTravelLimitPlus_nm, softTravelLimitMinus_nm ))
		{
			return STATUS_NOT_FOUND;			/// EARLY RETURN!	EARLY RETURN!
		}
		if ((softTravelLimitMinus_nm > zzz_nm) || (zzz_nm > softTravelLimitPlus_nm))
		{
			stringstream msg;
			msg << "ZZZ move is out of (soft-limit) bounds|minus=" << softTravelLimitMinus_nm << "|ZZZ=" << zzz_nm << "|plus=" << softTravelLimitPlus_nm;
			LOG_ERROR( m_threadName, "zzz", msg.str().c_str() );
			return STATUS_MOTION_FORBIDDEN;			/// EARLY RETURN!	EARLY RETURN!
		}
		if (speed != INVALID_SPEED)
		{
			velocity = speed;
		}
		FreezeWorkInProgress();
		unique_lock<mutex> lock( g_controllerBMutex ); /// Prevent the AnnularRingHolder thread from moving at the same time.
		status = Implement_MoveOneAxis( "ZZZ", zzz_nm, axisResolution_nm,
										velocity, acceleration, deceleration, stp );
		m_states_long[ZZZ_NM] = zzz_nm;
		if (status == STATUS_MOVEMENT_FAILED)
		{
			/// Send an alert.
			stringstream topic_stream;
			topic_stream << TOPIC_ES << CHANNEL_ALERT;

			/// Collect fields from the command's header for use in the alert.
			std::string messageType = "";
			int commandGroupId = 0;
			int commandId = 0;
			std::string sessionId = "";
			std::string machineId = "";
			bool succeeded = m_desHelper->SplitCommand( m_commandMessageInObj,
														&messageType,
														nullptr, // &transportVersion,
														&commandGroupId,
														&commandId,
														&sessionId,
														&machineId );

			json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
																commandGroupId,
																commandId,
																sessionId, // Or topic_stream.str()?
																ComponentType::ZZZAxis,
																STATUS_AXIS_ZZZ_FAULTED,
																false ); /// thisIsCausingHold
			bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "zzz", "Wrong ZZZ state" );
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_getCurrentZZZNm
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_getCurrentZZZNm()
{
	int status = STATUS_OK;
	long zzz_nm = 0L;
	if (m_threadName != DOLER_FOR_CXD)
	{
		LOG_ERROR( m_threadName, "zzz", "GetCurrentZZZNM sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	ComponentState ZZZState = g_ZZZState.load( std::memory_order_acquire );

	if (ZZZState == ComponentState::Mocked)
	{
		zzz_nm = m_states_long[ZZZ_NM];
	}
	else if ((ZZZState == ComponentState::Good) || (ZZZState == ComponentState::Homed3))
	{
		status = Implement_GetAxisPosition( "zzz", zzz_nm );
		if (zzz_nm != m_states_long[ZZZ_NM])
		{
			stringstream msg;
			msg << "ZZZ measured-pos=" << zzz_nm << "|recorded-pos=" << m_states_long[ZZZ_NM];
			LOG_WARN( m_threadName, "zzz", msg.str().c_str() );
			m_states_long[ZZZ_NM] = zzz_nm;
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "zzz", "Wrong ZZZ state" );
	}

	/// Populate the status payload
    int result = json_object_object_add( m_statusPayloadOutObj, ZZZ_NM, json_object_new_int64( zzz_nm ) );
	if ((status == STATUS_OK) && (result != 0))
	{
		/// populating the payload failed and nothing else did
		status = STATUS_CMD_MSG_ERROR;
	}
	return status;
}

int CommandDolerThread::DoleOut_MoveZZZToSafeHeight()
{
	int status = STATUS_OK;
	/// Get the payload object.
	json_object * payloadObj = nullptr;
	if (!json_object_object_get_ex( m_commandMessageInObj, PAYLOAD, &payloadObj ))
	{
		LOG_ERROR( m_threadName, "zzz", "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string( payloadObj );
		cout << payloadStr << endl;
	}
	long zzz_safeHeight_nm = 0L;
	long zzz_setSpeed = 0L;
	if (status == STATUS_OK)
	{
		/// Extract parameters from payload.
		if (!m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZZ_SAFE_HEIGHT, zzz_safeHeight_nm ))
		{
			LOG_ERROR( m_threadName, "zzz", "zzz_safe_height is missing" );
			status = STATUS_CMD_MSG_DATA_MISSING;
		}
		if (!m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZZ_SET_SPEED, zzz_setSpeed ))
		{
			LOG_WARN( m_threadName, "zzz", "zzz_set_speed is missing" );
		}
	}
	if (status == STATUS_OK)
	{
		LOG_TRACE( m_threadName, "zzz", "Moving ZZZ to safety" );
		if (zzz_setSpeed == 0L)
		{
			status = Implement_moveToZZZNm( zzz_safeHeight_nm );
		}
		else
		{
			status = Implement_moveToZZZNm( zzz_safeHeight_nm, zzz_setSpeed );
		}
	}
	return status;
}


///----------------------------------------------------------------------
/// Implement_MoveOnePump
/// 1. Sends the following commands to <axisName>:
///    ACC <acc> DEC <dec> STP <stp> VEL <vel> (where vel is flowRate_nlPerSec)
///    RES <axisName>  (resets the current position to 0 uL)
///    <axisName><volume_ul>  (moves the axis to the specified volume)
/// 2. Waits for motion to start.
/// 3. Waits for motion to end IFF awaitEndOfMotion is true.
///----------------------------------------------------------------------
int CommandDolerThread::Implement_MoveOnePump( const std::string axisNameStr, float & volume_ul_inOut,
											   long flowRate_nlPerSec, long acceleration_nlPerSecPerSec,
											   long deceleration_nlPerSecPerSec, long stp_nlPerSecPerSec,
											   const bool awaitEndOfMotion )
{
	int status = DoleOut_ConfirmNoKillAllMotionRequests( C_CONTROLLER );
	if (status != STATUS_OK)
	{
		return status;	/// EARLY RETURN!!	EARLY RETURN!!
	}
	const char * axisName = axisNameStr.c_str();
	double goalVolume_ul = volume_ul_inOut;
	
	LOG_TRACE( m_threadName, axisName, axisName );
	MotionAxis &axis = g_motionServices_ptr->getAxis(axisNameStr);
	{
		stringstream msg;
		msg << "Name of axis: " << axis.getName() << axis.getChannelIndex();
		msg << "|vol=" << volume_ul_inOut << "|flowRate=" << flowRate_nlPerSec;
		msg << "|accel=" << acceleration_nlPerSecPerSec << "|decel" << deceleration_nlPerSecPerSec;
		msg << "|stp=" << stp_nlPerSecPerSec;
		LOG_DEBUG( m_threadName, axisName, msg.str().c_str() );
	}
	
	if (isnan( volume_ul_inOut ))
	{
		LOG_ERROR( m_threadName, axisName, "volume is not a number" );
		return STATUS_MATH_ERROR;
	}
	if (isnan( flowRate_nlPerSec ))
	{
		LOG_ERROR( m_threadName, axisName, "flow rate is not a number" );
		return STATUS_MATH_ERROR;
	}

	/// Prepare some state variables.
	bool isMoving = false;
	bool hasVelocity = false;
	bool isFarFromGoal = false;

	float currentVolume_ul = 0.0;
	float velocity = 0.0;

	///
	/// Do NOT detect current position in order to bail out early. Unlike linear motion,
	/// pump motion always resets the current position to 0 and moves the desired volume.
	///
	;

	///
	/// Send the command that starts mpotion.
	///
	try {
		ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllerOfAxis( axisNameStr );
		/// Convert values to milliliters.
		pController->setVelAccelDecelStop( (float)flowRate_nlPerSec/1000.0, (float)acceleration_nlPerSecPerSec/1000.0,
											(float)deceleration_nlPerSecPerSec/1000.0, (float)stp_nlPerSecPerSec/1000.0 );
		usleep(100000);
		pController->setHwLimitDecel( axisNameStr, (float) deceleration_nlPerSecPerSec/1000.0 );
		usleep(100000);
		pController->resetAxis( axisNameStr ); /// Set current volume to 0 uL.
		usleep(100000);
		axis.absoluteMove( goalVolume_ul );
		usleep(100000);
    } catch (...) {
		status = STATUS_MOVEMENT_FAILED;
		LOG_ERROR( m_threadName, axisName, "unknown EXCEPTION!" );
		return status;	//	EARLY RETURN!	EARLY RETURN!
	}

	// Temporary debug.
	{
		stringstream msg;
		msg << axisName << "_PPU=" << axis.getCachedPPU();
		LOG_DEBUG( m_threadName, axisName, msg.str().c_str() );
	}

	///
	/// Wait for motion to start
	///
	long masterFlags = 0L;
	bool awaitingStartOfMove = true;
	int loopCount = 0;
	int strikeCount = 0;
	while (awaitingStartOfMove)
	{
		try {
			masterFlags = axis.getMasterFlags();
			isMoving = ((masterFlags & MASK_IN_MOTION) == MASK_IN_MOTION);
			hasVelocity = isMoving;
			if (isMoving)
			{
				awaitingStartOfMove = false;
			}
		}  catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, axisName, "masterFlags: STILL PENDING" );
		}
		if (loopCount++ > MAX_LOOPS_AWAITING_MOTION_START)
		{
			status = STATUS_MOVEMENT_FAILED;
			LOG_ERROR( m_threadName, axisName, "timed out waiting for motion to start" );
			return status;	//	EARLY RETURN!	EARLY RETURN!
		}
		usleep(10000);
	}

	/// Wait for motion to stop
	loopCount = 0;
	strikeCount = 0;

	while (awaitEndOfMotion && (hasVelocity || isMoving || isFarFromGoal)) {
		try {
			float actualPos = axis.getActualPosition();
			currentVolume_ul = actualPos / axis.getCachedPPU();
			isFarFromGoal = (Abs( currentVolume_ul ) <= Abs(goalVolume_ul * 0.9) ); /// Not far, but not necessarily there yet.
		} catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, axisName, "position: STILL PENDING" );
		}
		try {
			velocity = axis.getCurrentVelocity();
			hasVelocity = (Abs(velocity) > MAX_ALLOWABLE_VELOCITY_ERROR); /// Has a non-zero velocity
		} catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, axisName, "velocity: STILL PENDING" );
		}
		try {
			masterFlags = axis.getMasterFlags();
			isMoving = ((masterFlags & MASK_IN_MOTION) == MASK_IN_MOTION);
		}  catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, axisName, "masterFlags: STILL PENDING" );
		}
		
		if ((!hasVelocity) && (!isMoving) && isFarFromGoal)
		{
			strikeCount++;
			{
				stringstream msg;
				msg << axisName << ": ";
				if (!hasVelocity) msg << "Lacks Velocity|";
				if (!isMoving) msg << "Lacks Movement|";
				if (isFarFromGoal) msg << "Is far from goal";
				LOG_TRACE( m_threadName, axisName, msg.str().c_str() );
			}
			if (strikeCount >= MAX_STRIKES)
			{
				/// Movement somehow stopped far from goal.
				stringstream msg;
				
				msg << axisName << ": masterFlags=" << masterFlags << "|vel=" << velocity <<  "|vol(uL)=" << currentVolume_ul << "|goalVol=" << goalVolume_ul;
				LOG_TRACE( m_threadName, axisName, msg.str().c_str() );

				status = STATUS_MOVEMENT_FAILED;
				LOG_ERROR( m_threadName, axisName, "stopped before reaching goal" );
				return status;	//	EARLY RETURN!	EARLY RETURN!
			}
		}
		else if (loopCount++ % 10 == 0)
		{
			/// Movement is on course.
			stringstream msg;
			msg << axisName << ": masterFlags=" << masterFlags << "|vel=" << velocity << "|vol(uL)=" << currentVolume_ul << "|goalVol=" << goalVolume_ul;
			LOG_TRACE( m_threadName, axisName, msg.str().c_str() );
		}
		if (loopCount > MAX_LOOPS_AWAITING_MOTION_END)
		{
			stringstream msg;
			msg << axisName << ": masterFlags=" << masterFlags << "|vel=" << velocity << "|vol(uL)=" << currentVolume_ul << "|goalVol=" << goalVolume_ul;
			LOG_TRACE( m_threadName, axisName, msg.str().c_str() );

			status = STATUS_MOVEMENT_FAILED;
			LOG_ERROR( m_threadName, axisName, "timed out waiting for motion to end" );
			return status;	//	EARLY RETURN!	EARLY RETURN!
		}
		usleep(10000);
	}

	if (awaitEndOfMotion)
	{
		/// Movement stopped at goal.
		volume_ul_inOut = currentVolume_ul;
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_MoveOnePump
/// 1. Extracts flow rates and limits from component properties and adds them
///    to the payload.
/// 2. Calls Implement_MoveOnePump()
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveOnePump( const char * axisName, float flowRate_ulPerSec, float volume_ul_inOut, const bool awaitEndOfMotion )
{
	int status = STATUS_OK;
	/// Extract parameters from payload.
	float calCoeff1 = 0L; /// intercept
	float calCoeff2 = 0L; /// slope
	unsigned long accelTime_ms = 0L;
	unsigned long decelTime_ms = 0L;

	LOG_TRACE( m_threadName, axisName, "-" );
	if (!m_desHelper->ExtractFlowRatesAndLimits( m_threadName, m_commandMessageInObj, axisName,
												 calCoeff1,
												 calCoeff2,
												 accelTime_ms,
												 decelTime_ms ))
	{
		/// Assume that ExtractFlowRatesAndLimits() logged the reason.
		return STATUS_NOT_FOUND;			/// EARLY RETURN!	EARLY RETURN!
	}
	
	if (true)
	{
		stringstream msg;
		msg << "calCoeff1=" << calCoeff1 << "|calCoeff2=" << calCoeff2 << "|accelTime=" << accelTime_ms << "|decelTime=" << decelTime_ms;
		LOG_TRACE( m_threadName, axisName, msg.str().c_str() );
	}
	
	/// Use calibration coefficients to adjust the flow rate.
	float adjustedFlowRate_uLPerSec = (flowRate_ulPerSec * calCoeff2) + calCoeff1;
	/// Use the adjusted flow rate to adjust the target volume.
	float adjustedVolume_uL = (adjustedFlowRate_uLPerSec / flowRate_ulPerSec) * volume_ul_inOut;
	float ipb_uL = 5.0; // TODO: Find a good replacement for this hard-coded value.
	if (Abs( volume_ul_inOut ) <= ipb_uL)
	{
		/// The requested volume is too small to start a move. Call it done.
		stringstream msg;
		msg << "Not pumping because volume " << volume_ul_inOut << " <= IPB " << ipb_uL;
		LOG_WARN( m_threadName, axisName, msg.str().c_str() );
		return STATUS_OK;		/// EARLY RETURN!!	EARLY RETURN!!
	}
	if (adjustedVolume_uL > MAX_SAFE_VOLUME_UL)
	{
		adjustedVolume_uL = MAX_SAFE_VOLUME_UL;
	}
	if (adjustedVolume_uL < -MAX_SAFE_VOLUME_UL)
	{
		adjustedVolume_uL = -MAX_SAFE_VOLUME_UL;
	}
	
	/// Calculate acceleration and deceleration velocities in nL/s^2
	/// slope = height / width = goal-velocity (nL/s) ÷ time (s)
	/// slope = height / width = [goal-velocity (uL/s) * 1000 (nL/uL)] ÷ [time (ms) * 1000 (ms/s)]
	/// slope = height / width = 1000 (nL/uL) * 1000 (ms/s) * goal-velocity (uL/s) ÷ time (ms)
	unsigned long acceleration_nlPerSecPerSec = adjustedFlowRate_uLPerSec * _1MEG_F / (float)accelTime_ms;
	unsigned long deceleration_nlPerSecPerSec = adjustedFlowRate_uLPerSec * _1MEG_F / (float)decelTime_ms;

	FreezeWorkInProgress();
	// TODO: Do we ever need to raise suspitions when the extracted standard flow rate does not match flowRate_ulPerSec?
	float adjustedFlowRate_nlPerSec = adjustedFlowRate_uLPerSec * 1000.0;
	status = Implement_MoveOnePump( axisName,
									adjustedVolume_uL,
									adjustedFlowRate_nlPerSec,
									acceleration_nlPerSecPerSec,
									deceleration_nlPerSecPerSec,
									deceleration_nlPerSecPerSec,
									awaitEndOfMotion );

	/// Update the in-out parameters before we leave.
	volume_ul_inOut = adjustedVolume_uL; /// This is now the reported actual volume.
	return status;
}


///----------------------------------------------------------------------
/// DoleOut_MoveStepperPump handles all pump moves of pre-defined volume.
/// 1. Use the compState pointer to determine which axis is being moved.
/// 2. Use the calcVolume and calcRate flags to identify what input
///    parameters are expected in the payload. Extract them and use them
///    to calculate volume or rate or neither.
/// 3. Set the sign of the volume to +/- depending on the component and
///    the is_normal_direction input parameter.
/// 4. Call DoleOut_MoveOnePump(... awaitEndOfMotion = true). (All pre-defined
///    moves await the end of motion before returning.)
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveStepperPump( std::atomic<ComponentState> * compState, const bool calcVolume, const bool calcRate )
{
	int status = STATUS_OK;
	
	/// Choose the axis.
	string axisNameStr = "";
	string wrongStateMsg = "";
	ComponentType compType = ComponentType::ComponentUnknown;
	if (compState == & g_AspirationPump1State)
	{
		axisNameStr = "pn";
		compType = ComponentType::AspirationPump1;
		wrongStateMsg = "Wrong AspirationPump1 state";
	}
	else if (compState == & g_DispensePump1State)
	{
		axisNameStr = "pp";
		compType = ComponentType::DispensePump1;
		wrongStateMsg = "Wrong DispensePump1 state";
	}
	else if (compState == & g_DispensePump2State)	
	{
		axisNameStr = "ppp";
		compType = ComponentType::DispensePump2;
		wrongStateMsg = "Wrong DispensePump2 state";
	}
	else if (compState == & g_DispensePump3State)	
	{
		axisNameStr = "pppp";
		compType = ComponentType::DispensePump3;
		wrongStateMsg = "Wrong DispensePump3 state";
	}
	else
	{
		LOG_ERROR( m_threadName, "unk", "Invalid Component State object" );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}
	const char * axisName = axisNameStr.c_str();
	if (m_threadName != DOLER_FOR_PUMP)
	{
		LOG_ERROR( m_threadName, axisName, "Called from wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	/// Extract values from the payload.
	bool succeeded = true;
	float flow_rate_ulPerSec = 0.0;
	float volume_ul = 0.0;
	bool isNormalDirection = true;
	float time_s = 0.0;

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		// TODO: Remove this temporary debug
		if (true)
		{
			string payloadStr = json_object_to_json_string(payloadObj);
			cout << payloadStr << endl;
		}
		succeeded &= m_desHelper->ExtractBoolValue( m_threadName, payloadObj, IS_NORMAL_DIRECTION, isNormalDirection );
		if (calcVolume)
		{
			succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, FLOW_RATE_UL_PER_S, flow_rate_ulPerSec );
			succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, TIME_S, time_s );
			volume_ul = flow_rate_ulPerSec * time_s;
		}
		else if (calcRate)
		{
			succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, VOLUME_UL, volume_ul );
			succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, TIME_S, time_s );
			/// Because we are aspirating, normal direction is positive by definition.
			flow_rate_ulPerSec = volume_ul / time_s;
		}
		else
		{
			succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, FLOW_RATE_UL_PER_S, flow_rate_ulPerSec );
			succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, VOLUME_UL, volume_ul );
		}
		if (!succeeded)
		{
			LOG_ERROR( m_threadName, axisName, "FAILED to find all input properties" );
			status = STATUS_NOT_FOUND;
		}
		
		/// Assign correct +/- sign to volume.
		if (compState == & g_AspirationPump1State)
		{
			volume_ul = Abs( volume_ul );
		}
		else
		{
			volume_ul = - Abs( volume_ul ); /// Dispense volumes are negatve values.
		}
		/// Reverse sign for abnormal direction.
		if (!isNormalDirection)
		{
			volume_ul = - volume_ul;
		}
	}
	else
	{
		LOG_ERROR( m_threadName, axisName, "Failed to find payload" );
		status = STATUS_NOT_FOUND;
	}

	/// Determine component state
	ComponentState state = compState->load( std::memory_order_acquire );

	if (status == STATUS_OK)
	{
		if (state == ComponentState::Mocked)
		{
			stringstream msg;
			if (calcVolume)
			{
				msg << "Mocking flow rate(uL/s) " << flow_rate_ulPerSec << " over time(s) " << time_s;
			}
			else if (calcRate)
			{
				msg << "Mocking volume(uL) " << Abs(volume_ul) << " over time(s) " << time_s;
			}
			else
			{
				msg << "Mocking flow rate(uL/s) " << flow_rate_ulPerSec << " of volume (uL) " << Abs(volume_ul);
			}
			LOG_TRACE( m_threadName, axisName, msg.str().c_str() );
		}
		else if (state == ComponentState::Good)
		{
			status = DoleOut_MoveOnePump( axisName, flow_rate_ulPerSec, volume_ul, true );
			if ((status == STATUS_MOVEMENT_FAILED) || (status == STATUS_MATH_ERROR))
			{
				/// Send an alert.
				stringstream topic_stream;
				topic_stream << TOPIC_ES << CHANNEL_ALERT;

			/// Collect fields from the command's header for use in the alert.
			std::string messageType = "";
			int commandGroupId = 0;
			int commandId = 0;
			std::string sessionId = "";
			std::string machineId = "";
			bool succeeded = m_desHelper->SplitCommand( m_commandMessageInObj,
														&messageType,
														nullptr, // &transportVersion,
														&commandGroupId,
														&commandId,
														&sessionId,
														&machineId );

			json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
																commandGroupId,
																commandId,
																sessionId, // Or topic_stream.str()?
																compType,
																STATUS_PUMP_FAULTED,
																false ); /// thisIsCausingHold
				bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
			}
		}
		else
		{
			status = STATUS_DEVICE_ERROR;
			LOG_ERROR( m_threadName, axisName, wrongStateMsg.c_str() );
		}
	}

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_StartStopStepperPump handles all pump-start and pump-stop commands.
/// 1. Use the compState pointer to determine which axis is being moved.
/// 2. Extract FLOW_RATE_UL_PER_S from the payload to use for flow-rate. This
///    would have been packed (by the CommandInterpreter) with the component
///    property's standard_flow_rate.
/// 3. Use a huge number for volume. MAX_SAFE_VOLUME_UL is hard-coded, but it
///    was chosen as the biggest number that satisfies this equation for all axes:
///    MAX_SAFE_VOLUME_UL * axis.PPU <  0x8000000.
///    (See "Range Error when commanding long moves - Distance limit 2^31".)
/// 4. Set the sign of the volume to +/- depending on the component and
///    the is_normal_direction input parameter.
/// 5. If (isStartPump == true)
///    A. Set a global flag showing this axis is flowing.
///    B. Call DoleOut_MoveOnePump(... awaitEndOfMotion = false). (The pump
///       will continue moving after the call returns.)
///    ELSE
///    A. Call Implement_StopOneAxis().
///    B. Clear the global flag showing this axis is flowing.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_StartStopStepperPump( std::atomic<ComponentState> * compState, const char * stateKey )
{
	int status = STATUS_OK;
	
	/// Choose the axis.
	string axisNameStr = "";
	string wrongStateMsg = "";
	ComponentType compType = ComponentType::ComponentUnknown;
	if (compState == & g_AspirationPump1State)
	{
		axisNameStr = "pn";
		compType = ComponentType::AspirationPump1;
		wrongStateMsg = "Wrong AspirationPump1 state";
	}
	else if (compState == & g_DispensePump1State)
	{
		axisNameStr = "pp";
		compType = ComponentType::DispensePump1;
		wrongStateMsg = "Wrong DispensePump1 state";
	}
	else if (compState == & g_DispensePump2State)	
	{
		axisNameStr = "ppp";
		compType = ComponentType::DispensePump2;
		wrongStateMsg = "Wrong DispensePump2 state";
	}
	else if (compState == & g_DispensePump3State)	
	{
		axisNameStr = "pppp";
		compType = ComponentType::DispensePump3;
		wrongStateMsg = "Wrong DispensePump3 state";
	}
	else
	{
		LOG_ERROR( m_threadName, "unk", "Invalid Component State object" );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}
	const char * axisName = axisNameStr.c_str();
	if (m_threadName != DOLER_FOR_PUMP)
	{
		LOG_ERROR( m_threadName, axisName, "Called from wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	string key = stateKey;
	/// Extract values from the payload.
	bool succeeded = true;
	bool isStartPump = false;
	bool isNormalDirection = true;
	bool pumpIsRunning = false;
	float flow_rate_ulPerSec = 0.0;
	float volume_ul = MAX_SAFE_VOLUME_UL; /// Drain Lake Erie.
	float time_s = 0.0;

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		// TODO: Remove this temporary debug
		if (true)
		{
			string payloadStr = json_object_to_json_string(payloadObj);
			cout << payloadStr << endl;
		}
		
		/// Extract payload variables.
		succeeded &= m_desHelper->ExtractBoolValue( m_threadName, payloadObj, IS_NORMAL_DIRECTION, isNormalDirection );
		succeeded &= m_desHelper->ExtractBoolValue( m_threadName, payloadObj, IS_START_PUMP, isStartPump );
		succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, FLOW_RATE_UL_PER_S, flow_rate_ulPerSec );
		
		if (!succeeded)
		{
			LOG_ERROR( m_threadName, axisName, "FAILED to find all input properties" );
			status = STATUS_NOT_FOUND;
		}
		
		/// Look up the current running-state of this pump.
		string key = stateKey;
		std::map<std::string, bool>::iterator it = m_states_bool.find( key );
		if (it == m_states_bool.end())
		{
			stringstream msg;
			msg << "Failed to find " << stateKey << " in m_states_bool. Setting it to false";
			LOG_WARN( m_threadName, axisName, msg.str().c_str() );
			pumpIsRunning = false;
			m_states_bool[key] = pumpIsRunning;
		}
		else
		{
			pumpIsRunning = it->second;
		}
	}
	else
	{
		LOG_ERROR( m_threadName, axisName, "Failed to find payload" );
		status = STATUS_NOT_FOUND;
	}

	/// Determine component state
	ComponentState state = compState->load( std::memory_order_acquire );

	if (status == STATUS_OK)
	{
		/// Extra checking.
		if (isStartPump && pumpIsRunning)
		{
			LOG_WARN( m_threadName, axisName, "Redundant start of pump" );
		}
		if ((!isStartPump) && (!pumpIsRunning))
		{
			LOG_WARN( m_threadName, axisName, "Redundant stop of pump" );
		}
		
		/// Assign correct +/- sign to volume.
		if (compState == & g_AspirationPump1State)
		{
			volume_ul = Abs( volume_ul );
		}
		else
		{
			volume_ul = - Abs( volume_ul ); /// Dispense volumes are negatve values.
		}
		/// Reverse sign for abnormal direction.
		if (!isNormalDirection)
		{
			volume_ul = - volume_ul;
		}

		if (state == ComponentState::Mocked)
		{
			m_states_bool[key] = isStartPump;
		}
		else if (state == ComponentState::Good)
		{
			if (isStartPump && !pumpIsRunning)
			{
				/// Start the pump.
				m_states_bool[key] = true;
				status = DoleOut_MoveOnePump( axisName, flow_rate_ulPerSec, volume_ul, false );
				if (status == STATUS_MOVEMENT_FAILED)
				{
					/// Send an alert.
					stringstream topic_stream;
					topic_stream << TOPIC_ES << CHANNEL_ALERT;

			/// Collect fields from the command's header for use in the alert.
			std::string messageType = "";
			int commandGroupId = 0;
			int commandId = 0;
			std::string sessionId = "";
			std::string machineId = "";
			bool succeeded = m_desHelper->SplitCommand( m_commandMessageInObj,
														&messageType,
														nullptr, // &transportVersion,
														&commandGroupId,
														&commandId,
														&sessionId,
														&machineId );

			json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
																commandGroupId,
																commandId,
																sessionId, // Or topic_stream.str()?
																compType,
																STATUS_PUMP_FAULTED,
																false ); /// thisIsCausingHold
					bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
				}
			}
			if (!isStartPump)
			{
				Implement_StopOneAxis( C_CONTROLLER, axisNameStr );
				m_states_bool[key] = false;
				usleep(10000);
			}
		}
		else
		{
			status = STATUS_DEVICE_ERROR;
			LOG_ERROR( m_threadName, axisName, wrongStateMsg.c_str() );
		}
	}

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_IsPumpRunning
/// Returns a global flag showing whether the axis is flowing.
/// (The stateKey string is used to look up the flag in an associative array,
/// where they are stored. There are unique keys for each pump, like
/// "is_aspiration_pump_running".)
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_IsPumpRunning( const char * stateKey )
{
	int status = STATUS_OK;
	if (m_threadName != DOLER_FOR_PUMP)
	{
		LOG_ERROR( m_threadName, stateKey, "IsPumpRunning sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	string key = stateKey;
	std::map<std::string, bool>::iterator it = m_states_bool.find( key );
	if (it == m_states_bool.end())
	{
		stringstream msg;
		msg << "Failed to find " << stateKey << " in m_states_bool";
		LOG_ERROR( m_threadName, "pump", msg.str().c_str() );
		status = STATUS_DEVICE_ERROR;
	}
	else
	{
		/// Populate the status payload
		int result = json_object_object_add( m_statusPayloadOutObj,
											 IS_PUMP_RUNNING,
											 json_object_new_boolean( it->second ) );
		status = (result == 0) ? STATUS_OK : STATUS_DEVICE_ERROR;
	}
	return status;
}

///----------------------------------------------------------------------
/// Implement_StopOneAxis
/// Stop motion of one axis on one controller.
/// 1. Send the Kill-All-Motion command to the controller. This stops motion,
///    though not right away. It also sets the following kill flags:
///    A. Kill-All-Moves bit in the Master-Flags long integer for MASTER 0.
///    B. Kill-All-Motion-Request in the Quaternary-Axis-Flags for all axes
///       controlled by MASTER 0.
///    C. Latched-Excess-Position-Error in the Quaternary-Axis-Flags for all
///       axes controlled by MASTER 0. (Maybe. Tom Adams recommends clearing
///       these flags just in case.)
/// 2. Watch the velocity and in-motion flags for the end of movement.
///    (Similar to what Implement_MoveOnePump() does.)
/// 3. Call axis.clearKills() to clear all the flags mentioned above.
/// 4. Call axis.killsAreCleared() to confirm they are cleared.
///
/// Please note: experience showed that performing these steps in a different
/// order cause problems. The bad order was 1,3,4,2.
///----------------------------------------------------------------------
int CommandDolerThread::Implement_StopOneAxis( const int controller, const std::string axisNameStr )
{
	int status = STATUS_OK;
	string error_msg = "";
	const char * axisName = axisNameStr.c_str();
	bool succeeded = true;
	long masterFlags = 0L;
	bool isMoving = false;
	bool hasVelocity = false;
	MotionAxis &axis = g_motionServices_ptr->getAxis( axisNameStr );
	try {
		stringstream msg;
		msg << "Stopping named axis " << axisNameStr;
		LOG_INFO( m_threadName, axisName, msg.str().c_str() );
		succeeded &= axis.killAllMotion();
		usleep(10000);
		masterFlags = axis.getMasterFlags();
		isMoving = ((masterFlags & MASK_IN_MOTION) == MASK_IN_MOTION);
		hasVelocity = isMoving;
	} catch (std::exception &e) {
		error_msg = std::string(e.what());
		status = STATUS_DEVICE_ERROR;
	} catch (std::string &s) {
		error_msg = s;
		status = STATUS_DEVICE_ERROR;
	} catch (std::string *s) {
		error_msg = *s;
		status = STATUS_DEVICE_ERROR;
	} catch (...) {
		error_msg = "EXCEPTION: Couldn't stop axis; unknown exception";
		status = STATUS_DEVICE_ERROR;
	}
	if (!succeeded)
	{
		status = STATUS_DEVICE_ERROR;
		stringstream msg;
		msg << "EXCEPTION: Couldn't stop " << axisNameStr << " axis: " << error_msg;
		LOG_FATAL( m_threadName, axisName, msg.str().c_str() );
	}

	///
	/// Wait for motion to stop
	///
	int loopCount = 0;
	int strikeCount = 0;
	float velocity = 0.0;
	while (hasVelocity || isMoving)
	{
		try {
			velocity = axis.getCurrentVelocity();
			hasVelocity = (Abs(velocity) > MAX_ALLOWABLE_VELOCITY_ERROR); /// Has a non-zero velocity
		} catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, axisName, "velocity: STILL PENDING" );
		}
		try {
			masterFlags = axis.getMasterFlags();
			isMoving = ((masterFlags & MASK_IN_MOTION) == MASK_IN_MOTION);
		}  catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, axisName, "masterFlags: STILL PENDING" );
		}
		
		loopCount++;
		if (loopCount % 10 == 0)
		{
			stringstream msg;
			msg << "master-flags=" << masterFlags << "|Vel=" << velocity;
			LOG_TRACE( m_threadName, axisName, msg.str().c_str() );
		}

		if (loopCount > MAX_LOOPS_AWAITING_MOTION_END)
		{
			stringstream msg;
			msg << axisNameStr << ": masterFlags=" << masterFlags << "|vel=" << velocity;
			LOG_TRACE( m_threadName, axisName, msg.str().c_str() );

			status = STATUS_MOVEMENT_FAILED;
			LOG_ERROR( m_threadName, axisName, "timed out waiting for motion to end" );
			return status;	//	EARLY RETURN!	EARLY RETURN!
		}
		usleep(10000);
	}

	try {
		LOG_INFO( m_threadName, axisName, "Clearing kills" );
		succeeded &= axis.clearKills();
		usleep(300000);
		if (!axis.killsAreCleared())
		{
			succeeded = false;
			LOG_ERROR( m_threadName, axisName, "Kills are NOT cleared" );
		}
	} catch (std::exception &e) {
		error_msg = std::string(e.what());
		status = STATUS_DEVICE_ERROR;
	} catch (std::string &s) {
		error_msg = s;
		status = STATUS_DEVICE_ERROR;
	} catch (std::string *s) {
		error_msg = *s;
		status = STATUS_DEVICE_ERROR;
	} catch (...) {
		error_msg = "EXCEPTION: Couldn't clear kills; unknown exception";
		status = STATUS_DEVICE_ERROR;
	}
	if (!succeeded)
	{
		status = STATUS_DEVICE_ERROR;
		stringstream msg;
		msg << "EXCEPTION: Couldn't clear kills|" << error_msg;
		LOG_FATAL( m_threadName, axisName, msg.str().c_str() );
	}

	return status;
}

///
/// Dispense Pump 1
///

///
/// Dispense Pump 2
///

///
/// Dispense Pump 3
///


///
/// Picking Pump
///
///----------------------------------------------------------------------
/// DoleOut_MovePickingPump is based on DoleOut_MoveStepperPump.
/// 1. Use the compState pointer to determine which axis is being moved.
/// 2. Use the calcVolume and calcRate flags to identify what input
///    parameters are expected in the payload. Extract them and use them
///    to calculate volume or rate or neither.
/// 3. Set the sign of the volume to +/- depending on the component and
///    the is_normal_direction input parameter.
/// 4. Call DoleOut_MoveOnePump(... awaitEndOfMotion = true). (All pre-defined
///    moves await the end of motion before returning.)
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MovePickingPump( std::atomic<ComponentState> * compState, const bool calcVolume, const bool calcRate )
{
	int status = STATUS_OK;
	char axisName[] = "pick";

	if (compState != & g_PickingPump1State)
	{
		LOG_ERROR( m_threadName, axisName, "Invalid Component State object" );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}
	if (m_threadName != DOLER_FOR_PUMP)
	{
		LOG_ERROR( m_threadName, axisName, "Called from wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}
	
	/// Extract values from the payload.
	bool succeeded = true;
	float flow_rate_ulPerSec = 0.0;
	float volume_ul = 0.0;
	bool isAspirate = true;
	float time_s = 0.0;
	long tipWorkingVolume_nl = 0L;
	float tipWorkingVolume_ul = 0.0;
	string pumpVendor = "";
	string pumpModel = "";

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		// TODO: Remove this temporary debug
		if (true)
		{
			string payloadStr = json_object_to_json_string(payloadObj);
			cout << payloadStr << endl;
		}
		succeeded &= m_desHelper->ExtractBoolValue( m_threadName, payloadObj, IS_ASPIRATE, isAspirate );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, TIP_WORKING_VOLUME_NL, tipWorkingVolume_nl );
		succeeded &= m_desHelper->ExtractStringValue( m_threadName, payloadObj, PUMP_VENDOR, pumpVendor );
		succeeded &= m_desHelper->ExtractStringValue( m_threadName, payloadObj, PUMP_MODEL, pumpModel );

		tipWorkingVolume_ul = (float)tipWorkingVolume_nl / 1000.0;
		if (calcVolume)
		{
			succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, FLOW_RATE_UL_PER_S, flow_rate_ulPerSec );
			succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, TIME_S, time_s );
			volume_ul = flow_rate_ulPerSec * time_s;
		}
		else if (calcRate)
		{
			succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, VOLUME_UL, volume_ul );
			succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, TIME_S, time_s );
			/// Because we are aspirating, normal direction is positive by definition.
			flow_rate_ulPerSec = volume_ul / time_s;
		}
		else
		{
			succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, FLOW_RATE_UL_PER_S, flow_rate_ulPerSec );
			succeeded &= m_desHelper->ExtractFloatValue( m_threadName, payloadObj, VOLUME_UL, volume_ul );
		}
		if (!succeeded)
		{
			LOG_ERROR( m_threadName, axisName, "FAILED to find all input properties" );
			status = STATUS_NOT_FOUND;
		}
	}
	else
	{
		LOG_ERROR( m_threadName, axisName, "Failed to find payload" );
		status = STATUS_NOT_FOUND;
	}

	/// Determine component state
	ComponentState state = compState->load( std::memory_order_acquire );

	if (status == STATUS_OK)
	{
		if (true)
		{
			stringstream msg;
			msg << "picking pump current volume = " << m_states_float[PICKING_PUMP_CURRENT_VOLUME];
			LOG_TRACE( m_threadName, axisName, msg.str().c_str() );
		}
		/// Calculate the position to which we want the pump to move.
		/// Also, check the volume for under-flow or over-flow.
		float goalPosition_uL = m_states_float[PICKING_PUMP_CURRENT_VOLUME]; /// Avoid aspiration or dispense in this error state.
		if (isAspirate)
		{
			/// If (current_volume + requested_aspirate_volume > min(pump_maximum_volume, tip_working_volume) then fail the aspirate request.
			if (volume_ul + m_states_float[PICKING_PUMP_CURRENT_VOLUME] > m_states_float[PICKING_PUMP_MAXIMUM_VOLUME])
			{
				/// Error condition.
				stringstream msg;
				msg << "Current-volume(" << m_states_float[PICKING_PUMP_CURRENT_VOLUME] << ") + requested-volume(";
				msg << volume_ul << ") > max-volume(" << m_states_float[PICKING_PUMP_MAXIMUM_VOLUME] << ")";
				LOG_ERROR( m_threadName, axisName, msg.str().c_str() );
				return STATUS_MOTION_OUT_OF_RANGE; /// EARLY RETURN!!	EARLY RETURN!!
			}
			else
			{
				goalPosition_uL = m_states_float[PICKING_PUMP_CURRENT_VOLUME] + volume_ul;
				stringstream msg;
				msg << "Aspirating.. goal position (uL) = " << goalPosition_uL;
				LOG_TRACE( m_threadName, axisName, msg.str().c_str() );
			}
		}
		else /// is Dispense
		{
			if (volume_ul > m_states_float[PICKING_PUMP_CURRENT_VOLUME])
			{
				/// Don't dispense more than current volume.
				goalPosition_uL = 0.0;
			}
			else
			{
				goalPosition_uL = m_states_float[PICKING_PUMP_CURRENT_VOLUME] - volume_ul;
				stringstream msg;
				msg << "Dispensing.. goal position (uL) = " << goalPosition_uL;
				LOG_TRACE( m_threadName, axisName, msg.str().c_str() );
			}
		}
		
		if (state == ComponentState::Mocked)
		{
			m_states_float[PICKING_PUMP_CURRENT_VOLUME] = goalPosition_uL;
			g_PickingPumpVolume.store( goalPosition_uL, std::memory_order_relaxed ); /// for users outside the FOR-PUMP thread.
			string direction = isAspirate ? "aspirate " : "dispense ";
			stringstream msg;
			if (calcVolume)
			{
				msg << "Mocking " << direction << "flow rate(uL/s) " << flow_rate_ulPerSec << " over time(s) " << time_s;
			}
			else if (calcRate)
			{
				msg << "Mocking " << direction << "volume(uL) " << Abs(volume_ul) << " over time(s) " << time_s;
			}
			else
			{
				msg << "Mocking " << direction << "flow rate(uL/s) " << flow_rate_ulPerSec << " of volume (uL) " << Abs(volume_ul);
			}
			LOG_TRACE( m_threadName, axisName, msg.str().c_str() );
		}
		else if (state == ComponentState::Good)
		{
			if (DesHelper::IsTecanAdpDetect( pumpVendor, pumpModel ))
			{
				/// Make the Tecan Air Displacement Pipettor do its work.
				status = DoleOut_MoveOneTecanAirDisplacementPipettor( flow_rate_ulPerSec, goalPosition_uL, pumpModel );
			}
			else
			{
				stringstream msg;
				msg << "Unknown picking pump vendor: " << pumpVendor << " or model: " << pumpModel;
				status = STATUS_DEVICE_ERROR;
				LOG_ERROR( m_threadName, axisName, msg.str().c_str() );
			}

			if (status == STATUS_MOVEMENT_FAILED)
			{
				/// Send an alert.
				stringstream topic_stream;
				topic_stream << TOPIC_ES << CHANNEL_ALERT;

			/// Collect fields from the command's header for use in the alert.
			std::string messageType = "";
			int commandGroupId = 0;
			int commandId = 0;
			std::string sessionId = "";
			std::string machineId = "";
			bool succeeded = m_desHelper->SplitCommand( m_commandMessageInObj,
														&messageType,
														nullptr, // &transportVersion,
														&commandGroupId,
														&commandId,
														&sessionId,
														&machineId );

			json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
																commandGroupId,
																commandId,
																sessionId, // Or topic_stream.str()?
																ComponentType::PickingPump1,
																STATUS_PUMP_FAULTED,
																false ); /// thisIsCausingHold
				bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
			}
		}
		else
		{
			status = STATUS_DEVICE_ERROR;
			LOG_ERROR( m_threadName, axisName, "Wrong PickingPump1 state" );
		}
	}

	/// This status needs no payload.
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_GetPickingPumpCurrentVolumeNl
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_GetPickingPumpCurrentVolumeNl()
{
	int status = STATUS_OK;
	char axisName[] = "pick";
	float currentVolume_ul = 0L;

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (!payloadObj)
	{
		LOG_ERROR( m_threadName, axisName, "Failed to extract payload" );
		return STATUS_CMD_MSG_DATA_MISSING;		/// EARLY RETURN!	EARLY RETURN!
	}

	if (m_threadName != DOLER_FOR_PUMP)
	{
		LOG_ERROR( m_threadName, axisName, "Called from wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	if (g_PickingPump1State.load( std::memory_order_acquire ) ==  ComponentState::Mocked)
	{
		currentVolume_ul = m_states_float[PICKING_PUMP_CURRENT_VOLUME];
	}
	else
	{
		string pumpVendor = "";
		string pumpModel = "";
		bool modelAndVendorInHand = m_desHelper->ExtractStringValue( m_threadName, payloadObj, PUMP_VENDOR, pumpVendor );
		modelAndVendorInHand &= m_desHelper->ExtractStringValue( m_threadName, payloadObj, PUMP_MODEL, pumpModel );
		if (!modelAndVendorInHand)
		{
			LOG_ERROR( m_threadName, axisName, "Model and/or Ventor missing in payload" );
			return STATUS_CMD_MSG_DATA_MISSING; /// EARLY RETURN!	EARLY RETURN!
		}

		if (DesHelper::IsTecanAdpDetect( pumpVendor, pumpModel ))
		{
			/// Lock g_SerialPortMutex (in the PUMP thread) while communicating with the ADP.
			unique_lock<mutex> lock( g_SerialPortMutex ); /// IMPORTANT! The Pump thread and CxD thread share this.

			// TODO: Consider skipping a call to the pump if m_states_float[PICKING_PUMP_CURRENT_VOLUME] is always valid.
			string currentVolumeString = "";
			int count = 0;
			do {
				status = m_tecanADP->GetPumpCurrentVolume( m_threadName, currentVolumeString );
				count++;
				if (status == STATUS_OK) count = 5;
			} while (count < 5);
			usleep( m_tecanADP->StandardWait_uS );

			stringstream msg;
			msg << "Measured picking pump current volume is " << currentVolumeString;
			LOG_TRACE( m_threadName, axisName, msg.str().c_str() );
			if (status == STATUS_OK)
			{
				currentVolume_ul = stof(currentVolumeString);
			}
		}
		else
		{
			stringstream msg;
			msg << "Unknown picking pump vendor: " << pumpVendor << " or model: " << pumpModel;
			status = STATUS_DEVICE_ERROR;
			LOG_ERROR( m_threadName, axisName, msg.str().c_str() );
		}
	}

	if (currentVolume_ul != m_states_float[PICKING_PUMP_CURRENT_VOLUME])
	{
		stringstream msg;
		msg << "Changing picking pump current volume from " << m_states_float[PICKING_PUMP_CURRENT_VOLUME];
		msg << " to " << currentVolume_ul;
		LOG_TRACE( m_threadName, axisName, msg.str().c_str() );
		m_states_float[PICKING_PUMP_CURRENT_VOLUME] = currentVolume_ul;
		g_PickingPumpVolume.store( currentVolume_ul, std::memory_order_relaxed ); /// for users outside the FOR-PUMP thread.
	}

	if (status == STATUS_OK)
	{
		/// Populate the status payload
		long currentVolume_nl = static_cast<int>( std::round( currentVolume_ul * 1000.0 ) );
		int result = json_object_object_add( m_statusPayloadOutObj,
											 CURRENT_PICKING_PUMP_VOLUME_NL,
											 json_object_new_uint64( currentVolume_nl ) );
		if (result != 0)
		{
			LOG_ERROR( m_threadName, axisName, "Failed to pack current_picking_pump_volume_nl into payload" );
			status = STATUS_DEVICE_ERROR;
		}
	}
	return status;
}

int CommandDolerThread::DoleOut_MoveOneTecanAirDisplacementPipettor( float flow_rate_ulPerSec, float goalPosition_uL, string pumpModel )
{
	int status = STATUS_OK;
	char axisName[] = "pick";
	int stdError = 0;

	if (m_threadName != DOLER_FOR_PUMP)
	{
		LOG_ERROR( m_threadName, axisName, "Called from wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	/// Lock g_SerialPortMutex (in the PUMP thread) while aspirating or dispensing.
	unique_lock<mutex> lock( g_SerialPortMutex ); /// IMPORTANT! The Pump thread and CxD thread share this.
	/// Bounds-check flow_rate_ulPerSec against the pump's min and max.
	if ((flow_rate_ulPerSec < m_tecanADP->TopSpeedMin_uL) || (m_tecanADP->TopSpeedMax_uL < flow_rate_ulPerSec))
	{
		stringstream msg;
		msg << "flow_rate " << flow_rate_ulPerSec << " is out of bounds";
		LOG_ERROR( m_threadName, axisName, msg.str().c_str() );
		return STATUS_MOTION_OUT_OF_RANGE;			/// EARLY RETURN!	EARLY RETURN!
	}
	else
	{
		stringstream msg;
		msg << "flow_rate is " << flow_rate_ulPerSec;
		LOG_TRACE( m_threadName, axisName, msg.str().c_str() );
	}

	if (g_PickingPump1State.load( std::memory_order_acquire ) ==  ComponentState::Mocked)
	{
		LOG_TRACE( m_threadName, "z", "Mocking the ADP library's Move() command." );
		m_states_float[PICKING_PUMP_CURRENT_VOLUME] = goalPosition_uL;
		g_PickingPumpVolume.store( goalPosition_uL, std::memory_order_relaxed ); /// for users outside the FOR-PUMP thread.
	}
	else
	{
		/// Call SetPumpTopSpeed.
		if (status == STATUS_OK)
		{
			int count = 0;
			do {
				status = m_tecanADP->SetTopSpeed( m_threadName, flow_rate_ulPerSec, stdError );
				usleep( m_tecanADP->StandardWait_uS );
				count++;
				if (status == STATUS_OK) count = 5;
			} while (count < 5);
		}

		/// Call MoveAbsolutePosition
		if (status == STATUS_OK)
		{
			int count = 0;
			do {
				status = m_tecanADP->MoveAbsolutePosition( m_threadName, goalPosition_uL, stdError );
				usleep( 10 * m_tecanADP->StandardWait_uS );
				count++;
				if (status == STATUS_OK) count = 5;
			} while (count < 5);
		}
		
		/// Poll the pump waiting for !busy.
		if (status == STATUS_OK)
		{
			status = m_tecanADP->WaitForNotBusy( m_threadName, __func__ );
		}

		/// Call GetPumpCurrentVolume.
		if (status == STATUS_OK)
		{
			string currentVolumeString = "";
			int count = 0;
			do {
				status = m_tecanADP->GetPumpCurrentVolume( m_threadName, currentVolumeString );
				count++;
				if (status == STATUS_OK) count = 5;
			} while (count < 5);
			stringstream msg;
			msg << "Measured picking pump current volume is " << currentVolumeString;
			LOG_TRACE( m_threadName, "pick", msg.str().c_str() );

			if (status == STATUS_OK)
			{
				float currentVol = stof(currentVolumeString);
				if (currentVol != m_states_float[PICKING_PUMP_CURRENT_VOLUME])
				{
					stringstream msg;
					msg << "Changing picking pump current volume from " << m_states_float[PICKING_PUMP_CURRENT_VOLUME];
					msg << " to " << currentVol;
					LOG_TRACE( m_threadName, "pick", msg.str().c_str() );
					m_states_float[PICKING_PUMP_CURRENT_VOLUME] = currentVol;
					g_PickingPumpVolume.store( currentVol, std::memory_order_relaxed ); /// for users outside the FOR-PUMP thread.
				}
			}
			usleep( m_tecanADP->StandardWait_uS );
		}
	}
	/// This status needs no payload.
	return status;
}


///----------------------------------------------------------------------
/// White Light Source
///----------------------------------------------------------------------
///
int CommandDolerThread::Implement_SetWhiteLightSourceOn( bool on )
{
	int status = STATUS_OK;
	/// Mock the work if appropriate.
	ComponentState whiteLightState = g_WhiteLightSourceState.load( std::memory_order_acquire );
	if (whiteLightState == ComponentState::Mocked)
	{
		m_states_bool[WHITE_LIGHT_ON_STATE] = on;
		if (on)
		{
			LOG_DEBUG( m_threadName, WHITE_LIGHT, "emualting ON" );
		}
		else
		{
			LOG_DEBUG( m_threadName, WHITE_LIGHT, "emulating OFF" );
		}
		return STATUS_OK; // EARLY RETURN!	EARLY RETURN!
	}

	/// Not mocked, so perform the change.
	HardwareType whiteLightType = g_ComponentHardwareTypeArray[(int)ComponentType::WhiteLightSource];
	if (whiteLightType == HardwareType::IX85)
	{
		status = Implement_setOlympus_IX85_WhiteLightSourceOn( on );
	}
	else if (whiteLightType == HardwareType::LS620)
	{
		status = Implement_setEtaluma_LS620_WhiteLightSourceOn( on );
	}
	else
	{
		LOG_FATAL( m_threadName, WHITE_LIGHT, "Configured to unsupported hardware type" );
		status = STATUS_HARDWARE_CFG_ERROR;
	}
	
	/// Update our records.
	if (status == STATUS_OK)
	{
		m_states_bool[WHITE_LIGHT_ON_STATE] = on;
		LOG_TRACE( m_threadName, WHITE_LIGHT, "light-on succeeded" );
	}
	else
	{
		LOG_ERROR( m_threadName, WHITE_LIGHT, "light-on failed" );
	}
	return status;
}

int CommandDolerThread::Implement_setOlympus_IX85_WhiteLightSourceOn( bool on )
{
	return STATUS_OK; /// Olympus light is always on. We have no control.
}

int CommandDolerThread::Implement_setEtaluma_LS620_WhiteLightSourceOn( bool on )
{
	if (!FileExists( g_SerialPortDevice_MICRO ))
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "Serial port not found" );
		return STATUS_COMPONENT_NOT_FOUND; ///	EARLY RETURN!	EARLY RETURN!
	}

	int status = STATUS_OK;
	string popen_cmd = on ? BRIGHT_FIELD_ON : BRIGHT_FIELD_OFF;

	FreezeWorkInProgress();
	string resultStr = "";
	status = ShellInvoker::InvokePopenHand( popen_cmd, m_threadName, WHITE_LIGHT, resultStr );
	return status;
}


///----------------------------------------------------------------------
/// Fluorescence Light Filter
///----------------------------------------------------------------------
///
int CommandDolerThread::Implement_setFluorescenceLightFilterOn( bool on )
{
	int status = STATUS_OK;
	/// Mock the work if appropriate.
	ComponentState fluroLightFilterState = g_FluorescenceLightFilterState.load( std::memory_order_acquire );
	if (fluroLightFilterState == ComponentState::Mocked)
	{
		m_states_bool[FLUORESCENCE_LIGHT_ON_STATE] = on;
		if (on)
		{
			LOG_DEBUG( m_threadName, FLUORESCENCE_LIGHT, "emulating ON" );
		}
		else
		{
			LOG_DEBUG( m_threadName, FLUORESCENCE_LIGHT, "emulating OFF" );
		}
		return STATUS_OK; // EARLY RETURN!	EARLY RETURN!
	}

	/// Not mocked, so perform the change.
	HardwareType lightFilterType = g_ComponentHardwareTypeArray[(int)ComponentType::FluorescenceLightFilter];
	if (lightFilterType == HardwareType::IX85)
	{
		status = Implement_setOlympus_IX85_FluorescenceLightFilterOn( on );
	}
	else if (lightFilterType == HardwareType::LS620)
	{
		status = Implement_setEtaluma_LS620_FluorescenceLightFilterOn( on );
	}
	else
	{
		LOG_FATAL( m_threadName, FLUORESCENCE_LIGHT, "Configured to unsupported hardware type" );
		status = STATUS_HARDWARE_CFG_ERROR;
	}

	/// Update our records.
	if (status == STATUS_OK)
	{
		m_states_bool[FLUORESCENCE_LIGHT_ON_STATE] = on;
		LOG_TRACE( m_threadName, FLUORESCENCE_LIGHT, "light-on succeeded" );
	}
	else
	{
		LOG_ERROR( m_threadName, FLUORESCENCE_LIGHT, "light-on failed" );
	}
	return status;
}

/// Implement_setFluorescenceLightFilterOn for Olympus IX85
/// Uses MQTT communications to command the Olympus Windows Service to
/// turn on/off the Lumencore light source.
int CommandDolerThread::Implement_setOlympus_IX85_FluorescenceLightFilterOn( bool on )
{
	int status = STATUS_OK;
	if (!m_mqttHardwareIO)
	{
		LOG_ERROR( m_threadName, FLUORESCENCE_LIGHT, "m_mqttHardwareIO is null." );
		return STATUS_DEVICE_ERROR; // EARLY RETURN!	EARLY RETURN!
	}

	/// Make an input parameter
	string messageType = SET_FLUORESCENCE_LIGHT_FILTER_ON;
	
	/// Create the command message.
	/// ---- step 1: Make the command payload
	json_object * payloadObj = nullptr;
	payloadObj = json_object_new_object();
	int rc = json_object_object_add( payloadObj, ON_STATUS, json_object_new_boolean( on ) );
	if (rc != 0)
	{
		LOG_ERROR( m_threadName, FLUORESCENCE_LIGHT, "Failed to make a command payload" );
		return STATUS_JSON_COMP_FAILED;	// EARLY RETURN!	EARLY RETURN!
	}
	bool succeeded = (rc == 0);
	/// ---- step 2: Embed the payload in a command message as a JSON object.
	json_object * commandMessageOutOjb = NULL;
	commandMessageOutOjb = m_desHelper->ComposeCommand(  SET_FLUORESCENCE_LIGHT_FILTER_ON,	/// std::string messageType
														0,									/// commandGroupId
														0,									/// commandId
														"fake_id",							/// std::string sessionId
														payloadObj );						/// json_object * payload
	/// ---- step 3: Convert the JSON object into a JSON string.
	string desCallMessage = json_object_to_json_string( commandMessageOutOjb );
	json_object_put( commandMessageOutOjb ); /// Free memory.
	
	/// Make the topic string
	stringstream streamtopic;
	streamtopic << TOPIC_MICROSCOPE << CHANNEL_COMMAND;
	string topicName = streamtopic.str();
	/// Make variables for three parameters that return values.
	bool ackReceived = false;
	int ack = 0;
	json_object * jStatusPayloadObj = nullptr;
	
	cout << "SENDING: [" << desCallMessage << "]" << endl;

	int fluorescencLightFilterStatusCode = m_mqttHardwareIO->CommandTheHardwareClient( messageType, desCallMessage, topicName, ackReceived, ack, & jStatusPayloadObj );
	status = FluorescencLightFilterStatusCode_to_ESStatusCode( fluorescencLightFilterStatusCode );

	// TODO: Do something with the "details" part of the return payload if status is not OK.
	return status;
}

/// Implement_setFluorescenceLightFilterOn for Etaluma LS620
/// Uses ShellInvoker class to call popen_hand.py to call functions of the LumaViewPro's ledboard.py.
/// Turn on/off one of the LED board's fluorescence lights.
int CommandDolerThread::Implement_setEtaluma_LS620_FluorescenceLightFilterOn( bool on )
{
	if (!FileExists( g_SerialPortDevice_MICRO ))
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "Serial port not found" );
		return STATUS_COMPONENT_NOT_FOUND; ///	EARLY RETURN!	EARLY RETURN!
	}

	int status = STATUS_OK;
	int position = m_states_long[ACTIVE_FLUORESCENCE_FILTER_POSITION];
	string popen_cmd = "";
	// red
	if (position == 6) // TODO: Confirm.
	{
		if (on) popen_cmd = RED_ON;
		else popen_cmd = RED_OFF;
	}

	// green
	else if (position == 5) // TODO: Confirm.
	{
		if (on) popen_cmd = GREEN_ON;
		else popen_cmd = GREEN_OFF;
	}

	// blue
	else if (position == 4) // TODO: Confirm.
	{
		if (on) popen_cmd = BLUE_ON;
		else popen_cmd = BLUE_OFF;
	}
	
	else
	{
		stringstream msg;
		msg << "position " << position << " not supported. Treating as NO-OP";
		LOG_WARN( m_threadName, FLUORESCENCE_LIGHT, msg.str().c_str() );
		return STATUS_OK; /// EARLY RETURN!  EARLY RETURN!
	}

	if (status == STATUS_OK)
	{
		FreezeWorkInProgress();
		string resultStr = "";
		status = ShellInvoker::InvokePopenHand( popen_cmd, m_threadName, FLUORESCENCE_LIGHT, resultStr );
	}
	return status;
}

///
/// Implement_setFluorescenceLightFilterPosition
/// Uses MQTT communications to command the Olympus Windows Service to
/// set the position of the light filter.
///
int CommandDolerThread::Implement_setFluorescenceLightFilterPosition( long position )
{
	LETS_ASSERT( (-32768 <= position) && (position <= 32767) );
	int status = STATUS_OK;
	/// Mock the work if appropriate.
	ComponentState fluroLightFilterState = g_FluorescenceLightFilterState.load( std::memory_order_acquire );
	if (fluroLightFilterState == ComponentState::Mocked)
	{
		m_states_long[ACTIVE_FLUORESCENCE_FILTER_POSITION] = position;
		stringstream msg;
		msg << "emulating position=" << position;
		LOG_DEBUG( m_threadName, FLUORESCENCE_LIGHT, msg.str().c_str() );
		return STATUS_OK; // EARLY RETURN!	EARLY RETURN!
	}

	/// Perform the change.
	HardwareType lightFilterType = g_ComponentHardwareTypeArray[(int)ComponentType::FluorescenceLightFilter];
	if (lightFilterType == HardwareType::IX85)
	{
		status = Implement_setOlympus_IX85_FluorescenceLightFilterPosition( position );
	}
	else if (lightFilterType == HardwareType::LS620)
	{
		status = Implement_setEtaluma_LS620_FluorescenceLightFilterPosition( position );
	}
	else
	{
		LOG_FATAL( m_threadName, FLUORESCENCE_LIGHT, "Configured to unsupported hardware type" );
		status = STATUS_HARDWARE_CFG_ERROR;
	}

	/// Update our local records.
	if (status == STATUS_OK)
	{
		m_states_long[ACTIVE_FLUORESCENCE_FILTER_POSITION] = position;
		LOG_DEBUG( m_threadName, FLUORESCENCE_LIGHT, "filter-move succeeded" );
	}
	else
	{
		LOG_ERROR( m_threadName, FLUORESCENCE_LIGHT, "filter-move failed" );
	}
	return status;
}

int CommandDolerThread::Implement_setOlympus_IX85_FluorescenceLightFilterPosition( long position )
{
	int status = STATUS_OK;
	if (!m_mqttHardwareIO)
	{
		LOG_ERROR( m_threadName, FLUORESCENCE_LIGHT, "m_mqttHardwareIO is null." );
		return STATUS_DEVICE_ERROR; // EARLY RETURN!	EARLY RETURN!
	}

	/// Prepare variables that have one-time initialization.
	/// Make an input parameter
	string messageType = SET_FLUORESCENCE_LIGHT_FILTER_POSITION;
	
	/// Create the command message.
	/// ---- step 1: Make the command payload
	json_object * payloadObj = nullptr;
	payloadObj = json_object_new_object();
	/// NOTE: filter position is a rare value that--for this component only--must be converted from 1-based to 0-based when sent to the hardware.
	/// NOTE: When position is used here, in the ES for any reason, it is maintained as 1-based.
	// TODO: Add hooks in case a different fluorescence filter light models does not need this conversion.
	unsigned long zero_based_position = (position > 0L) ? (position - 1L) : position;
	int rc = json_object_object_add( payloadObj, FLUORESCENCE_FILTER_POSITION, json_object_new_int( zero_based_position ) );
	if (rc != 0)
	{
		LOG_ERROR( m_threadName, FLUORESCENCE_LIGHT, "Failed to make a command payload" );
		return STATUS_JSON_COMP_FAILED;	// EARLY RETURN!	EARLY RETURN!
	}
	/// ---- step 2: Embed the payload in a command message as a JSON object.
	json_object * commandMessageOutOjb = NULL;
	commandMessageOutOjb = m_desHelper->ComposeCommand( SET_FLUORESCENCE_LIGHT_FILTER_POSITION,	/// std::string messageType
														0,										/// commandGroupId
														0,										/// commandId
														"",										/// std::string sessionId
														payloadObj );							/// json_object * payload
	/// ---- step 3: Convert the JSON object into a JSON string.
	string desCallMessage = json_object_to_json_string( commandMessageOutOjb );
	json_object_put( commandMessageOutOjb ); /// Free memory.
	LOG_TRACE( m_threadName, "fluo", desCallMessage.c_str() );
	
	/// Make the topic string
	stringstream streamtopic;
	streamtopic << TOPIC_MICROSCOPE << CHANNEL_COMMAND;
	string topicName = streamtopic.str();
	/// Make variables for three parameters that return values.
	bool ackReceived = false;
	int ack = 0;
	json_object * jStatusPayloadObj = nullptr;

	cout << "SENDING: [" << desCallMessage << "]" << endl;

	int fluorescencLightFilterStatusCode = m_mqttHardwareIO->CommandTheHardwareClient( messageType, desCallMessage, topicName, ackReceived, ack, & jStatusPayloadObj );
	status = FluorescencLightFilterStatusCode_to_ESStatusCode( fluorescencLightFilterStatusCode );

	// TODO: Do something with the "details" part of the return payload if status is not OK.
	return status;
}

int CommandDolerThread::Implement_setEtaluma_LS620_FluorescenceLightFilterPosition( long position )
{
	if (!FileExists( g_SerialPortDevice_MICRO ))
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "Serial port not found" );
		return STATUS_COMPONENT_NOT_FOUND; ///	EARLY RETURN!	EARLY RETURN!
	}

	int status = STATUS_OK;
	/// If light is currently ON, turn OFF the current position, set the new postion, turn ON the changed current position.
	if (m_states_bool[FLUORESCENCE_LIGHT_ON_STATE])
	{
		status = Implement_setEtaluma_LS620_FluorescenceLightFilterOn( false );
		if (status != STATUS_OK)
		{
			stringstream msg;
			msg << "Failed to turn off light at position " << m_states_long[ACTIVE_FLUORESCENCE_FILTER_POSITION];
			msg << "| Will update position and try to turn it on.";
			LOG_WARN( m_threadName, FLUORESCENCE_LIGHT, msg.str().c_str() );
		}
		/// Note that Implement_setFluorescenceLightFilterPosition will do this too, after we return.
		/// That is important to keep because it supports the Olympus path.
		m_states_long[ACTIVE_FLUORESCENCE_FILTER_POSITION] = position;
		status = Implement_setEtaluma_LS620_FluorescenceLightFilterOn( true );
	}
	/// If light is currently OFF, just set the new position.
	else
	{
		/// Note that Implement_setFluorescenceLightFilterPosition will do this too, after we return.
		/// That is important to keep because it supports the Olympus path.
		stringstream msg;
		msg << "Setting fluorescence light filter position to " << position << " even though state is OFF";
		LOG_TRACE( m_threadName, FLUORESCENCE_LIGHT, msg.str().c_str() );
		m_states_long[ACTIVE_FLUORESCENCE_FILTER_POSITION] = position;
	}

	return status;
}


///----------------------------------------------------------------------
/// Annular Ring Holder
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveToAnnularRingPosition()
{
	/// Extract z from payload.
	long annular_ring_nm = this->m_desHelper->ExtractPayloadLongValue( m_commandMessageInObj, ANNULAR_RING_NM );

	int status = DoleOut_MoveToAnnularRingPosition( annular_ring_nm ); /// Assume that speeds are already packed in the payload.

	/// This status needs no payload.
	return status;
}

int CommandDolerThread::DoleOut_MoveToAnnularRingPosition( long position_nm )
{
	int status = STATUS_OK;
	long sh_nm = position_nm;
	if (m_threadName != DOLER_FOR_RING_HOLDER)
	{
		LOG_ERROR( m_threadName, "sh", "MoveToAnnularRingPosition sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string( payloadObj );
		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "init", "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
		return status; /// EARLY RETURN!!	EARLY RETURN!!
	}

	ComponentState SHState = g_AnnularRingHolderState.load( std::memory_order_acquire );

	if (SHState == ComponentState::Mocked)
	{
		m_states_long[ANNULAR_RING_POSITION] = sh_nm;
		status = STATUS_OK;
	}
	else if (SHState == ComponentState::Good)
	{
		/// Extract parameters from payload.
		long velocity = 0L;
		long acceleration = 0L;
		long deceleration = 0L;
		long stp = 0L;
		long axisResolution_nm = 0L;
		long softTravelLimitPlus_nm = 0L;
		long softTravelLimitMinus_nm = 0L;
		if (!m_desHelper->ExtractSpeedsAndLimits( m_threadName, m_commandMessageInObj, "ar",
										 velocity, acceleration, deceleration, stp, axisResolution_nm,
										 softTravelLimitPlus_nm, softTravelLimitMinus_nm ))
		{
			return STATUS_NOT_FOUND;			/// EARLY RETURN!	EARLY RETURN!
		}
		if ((softTravelLimitMinus_nm > sh_nm) || (sh_nm > softTravelLimitPlus_nm))
		{
			stringstream msg;
			msg << "Annular ring holder move is out of (soft-limit) bounds|minus=" << softTravelLimitMinus_nm << "|SH=" << sh_nm << "|plus=" << softTravelLimitPlus_nm;
			LOG_ERROR( m_threadName, "sh", msg.str().c_str() );
			return STATUS_MOTION_FORBIDDEN;			/// EARLY RETURN!	EARLY RETURN!
		}
		FreezeWorkInProgress();
		unique_lock<mutex> lock( g_controllerBMutex ); /// Prevent the CxD thread from moving one of the Z axes at the same time.
		status = Implement_MoveOneAxis( "SH", sh_nm, axisResolution_nm,
										velocity, acceleration, deceleration, stp );
		m_states_long[ANNULAR_RING_POSITION] = sh_nm;
		if (status == STATUS_MOVEMENT_FAILED)
		{
			/// Send an alert.
			stringstream topic_stream;
			topic_stream << TOPIC_ES << CHANNEL_ALERT;

			/// Collect fields from the command's header for use in the alert.
			std::string messageType = "";
			int commandGroupId = 0;
			int commandId = 0;
			std::string sessionId = "";
			std::string machineId = "";
			bool succeeded = m_desHelper->SplitCommand( m_commandMessageInObj,
														&messageType,
														nullptr, // &transportVersion,
														&commandGroupId,
														&commandId,
														&sessionId,
														&machineId );

			json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
																commandGroupId,
																commandId,
																sessionId, // Or topic_stream.str()?
																ComponentType::ZAxis,
																STATUS_AXIS_Z_FAULTED,
																false ); /// thisIsCausingHold
			bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
		}
	}
	else
	{
		status = STATUS_DEVICE_ERROR;
		LOG_ERROR( m_threadName, "sh", "Wrong Annular Ring Holder state" );
	}

	return status;
}


///
/// Optical Column
///
int CommandDolerThread::DoleOut_moveToFocusNM()
{
	if (m_threadName != DOLER_FOR_OPTICS)
	{
		LOG_ERROR( m_threadName, "sh", "MoveToFocusNM sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	/// Extract focus distance from payload.
	unsigned long focus_nm = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, FOCUS_NM );

	FreezeWorkInProgress();
	int esStatusCode = Implement_moveToFocusNM( m_commandMessageInObj, focus_nm );
	/// This status needs no payload.
	return esStatusCode;
}

// TODO: Deprecate and remove this overloaded version of Implement_moveToFocusNM.
int CommandDolerThread::Implement_moveToFocusNM( json_object * templateInObj, unsigned long focus_nm )
{
	int status = STATUS_OK;
	/// Mock the move if appropriate.
	ComponentState opticalColumnState = g_OpticalColumnState.load( std::memory_order_acquire );
	if (opticalColumnState == ComponentState::Mocked)
	{
		m_states_ulong[FOCUS_NM] = focus_nm;
		stringstream msg;
		msg << "emulating focus_nm=" << focus_nm;
		LOG_DEBUG( m_threadName, OPTICAL_COLUMN, msg.str().c_str() );
		return STATUS_OK; // EARLY RETURN!	EARLY RETURN!
	}

	/// set focus
	HardwareType opticalColumnType = g_ComponentHardwareTypeArray[(int)ComponentType::OpticalColumn];
	if (opticalColumnType == HardwareType::IX85)
	{
		status = Implement_moveOlympus_IX85_ToFocusNM( templateInObj, focus_nm );
	}
	else if (opticalColumnType == HardwareType::LS620)
	{
		status = Implement_moveEtaluma_LS620_ToFocusNM( focus_nm );
	}
	else
	{
		LOG_FATAL( m_threadName, OPTICAL_COLUMN, "Configured to unsupported hardware type" );
		status = STATUS_HARDWARE_CFG_ERROR;
	}

	/// Update our records.
	if (status == STATUS_OK)
	{
		m_states_ulong[FOCUS_NM] = focus_nm;
		LOG_DEBUG( m_threadName, OPTICAL_COLUMN, "focus succeeded" );
	}
	else
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "focus failed" );
	}
	return status;
}

// TODO: Deprecate this function and make all calls to Implement_moveToFocusNM( unsigned long focus_nm ) instead, when testing is possible.
int CommandDolerThread::Implement_moveOlympus_IX85_ToFocusNM( json_object * templateInObj, unsigned long focus_nm )
{
	if (!m_mqttHardwareIO)
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "m_mqttHardwareIO is null." );
		return STATUS_DEVICE_ERROR; // EARLY RETURN!	EARLY RETURN!
	}

	json_object * commandMessageOut = NULL;
	/// Make a temporary copy of m_commandMessageInObj so we can re-use much of it for our outgoing message
	/// without risk of breaking its use in sending status to MqttSender after we are done here.
	int rc = json_object_deep_copy( templateInObj, &commandMessageOut, NULL );
	bool succeeded = true;
	bool ackReceived = false;
	int ack = 0;
	json_object * jpayloadObj = nullptr;
	stringstream streamtopic;
	streamtopic << TOPIC_MICROSCOPE << CHANNEL_COMMAND;
	string topic = streamtopic.str();

	/// Change a few parts of commandMessageOut.
	string focusMessageType = MOVE_TO_FOCUS_NM;
	json_object * focusPayloadObj = json_object_new_object();
	rc = json_object_object_add( focusPayloadObj, FOCUS_NM, json_object_new_uint64( focus_nm ) );
	succeeded &= (rc == 0);
	rc = json_object_object_add( commandMessageOut, PAYLOAD, focusPayloadObj );
	succeeded &= (rc == 0);
	rc = json_object_object_add( commandMessageOut, MESSAGE_TYPE, json_object_new_string( MOVE_TO_FOCUS_NM ) );
	succeeded &= (rc == 0);

	string desCallMessage = json_object_to_json_string( commandMessageOut );
	LOG_TRACE( m_threadName, OPTICAL_COLUMN, desCallMessage.c_str() );
	json_object_put( commandMessageOut ); /// Free memory.
	int opticalColumnStatusCode = m_mqttHardwareIO->CommandTheHardwareClient( focusMessageType, desCallMessage, topic, ackReceived, ack, &jpayloadObj );
	int esStatusCode = OpticalColumnStatusCode_to_ESStatusCode( opticalColumnStatusCode );
	if (esStatusCode == STATUS_OK)
	{
		if (ack != STATUS_OK)
		{
			LOG_ERROR( m_threadName, OPTICAL_COLUMN, "focus failed" );
			esStatusCode = ack;
		}
		else if (!ackReceived)
		{
			LOG_ERROR( m_threadName, OPTICAL_COLUMN, "focus failed" );
			esStatusCode = STATUS_DEVICE_ERROR;
		}
	}
	return esStatusCode;
}

int CommandDolerThread::Implement_moveToFocusNM( unsigned long focus_nm )
{
	int status = STATUS_OK;
	/// Mock the move if appropriate.
	ComponentState opticalColumnState = g_OpticalColumnState.load( std::memory_order_acquire );
	if (opticalColumnState == ComponentState::Mocked)
	{
		m_states_ulong[FOCUS_NM] = focus_nm;
		stringstream msg;
		msg << "emulating focus_nm=" << focus_nm;
		LOG_DEBUG( m_threadName, OPTICAL_COLUMN, msg.str().c_str() );
		return STATUS_OK; // EARLY RETURN!	EARLY RETURN!
	}

	/// set focus
	HardwareType opticalColumnType = g_ComponentHardwareTypeArray[(int)ComponentType::OpticalColumn];
	if (opticalColumnType == HardwareType::IX85)
	{
		status = Implement_moveOlympus_IX85_ToFocusNM( focus_nm );
	}
	else if (opticalColumnType == HardwareType::LS620)
	{
		status = Implement_moveEtaluma_LS620_ToFocusNM( focus_nm );
	}
	else
	{
		LOG_FATAL( m_threadName, OPTICAL_COLUMN, "Configured to unsupported hardware type" );
		status = STATUS_HARDWARE_CFG_ERROR;
	}

	/// Update our records.
	if (status == STATUS_OK)
	{
		m_states_ulong[FOCUS_NM] = focus_nm;
		LOG_DEBUG( m_threadName, OPTICAL_COLUMN, "focus succeeded" );
	}
	else
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "focus failed" );
	}
	return status;
}

int CommandDolerThread::Implement_moveOlympus_IX85_ToFocusNM( unsigned long focus_nm )
{
	if (!m_mqttHardwareIO)
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "m_mqttHardwareIO is null." );
		return STATUS_DEVICE_ERROR; // EARLY RETURN!	EARLY RETURN!
	}

	/// Prepare variables that have one-time initialization.
	bool succeeded = true;
	
	/// Make an input parameter
	string messageType = MOVE_TO_FOCUS_NM;
	
	/// Create the command message.
	/// ---- step 1: Make the command payload
	json_object * payloadObj = nullptr;
	payloadObj = json_object_new_object();
	int rc = json_object_object_add( payloadObj, FOCUS_NM, json_object_new_uint64( focus_nm ) );
	succeeded &= (rc == 0);
	/// ---- step 2: Embed the payload in a command message as a JSON object.
	json_object * commandMessageOutOjb = NULL;
	commandMessageOutOjb = m_desHelper->ComposeCommand(  MOVE_TO_FOCUS_NM,	/// std::string messageType
														0,					/// commandGroupId
														0,					/// commandId
														"",					/// std::string sessionId
														payloadObj );		/// json_object * payload
	/// ---- step 3: Convert the JSON object into a JSON string.
	string desCallMessage = json_object_to_json_string( commandMessageOutOjb );
	json_object_put( commandMessageOutOjb ); /// Free memory.
	
	/// Make the topic string
	stringstream streamtopic;
	streamtopic << TOPIC_MICROSCOPE << CHANNEL_COMMAND;
	string topicName = streamtopic.str();
	/// Make variables for three parameters that return values.
	bool ackReceived = false;
	int ack = 0;
	json_object * jStatusPayloadObj = nullptr;

	int statusCode = m_mqttHardwareIO->CommandTheHardwareClient( messageType, desCallMessage, topicName, ackReceived, ack, & jStatusPayloadObj );

	int esStatusCode = OpticalColumnStatusCode_to_ESStatusCode( statusCode );
	// TODO: Do something with the "details" part of the return payload if status is not OK.
	if (esStatusCode == STATUS_OK)
	{
		if (ack != STATUS_OK)
		{
			LOG_ERROR( m_threadName, OPTICAL_COLUMN, "focus failed" );
			esStatusCode = ack;
		}
		else if (!ackReceived)
		{
			LOG_ERROR( m_threadName, OPTICAL_COLUMN, "focus failed" );
			esStatusCode = STATUS_DEVICE_ERROR;
		}
	}
	return esStatusCode;
}

int CommandDolerThread::Implement_moveEtaluma_LS620_ToFocusNM( unsigned long focus_nm )
{
	if (!FileExists( g_SerialPortDevice_MICRO ))
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "Serial port not found" );
		return STATUS_COMPONENT_NOT_FOUND; ///	EARLY RETURN!	EARLY RETURN!
	}

	unsigned long focus_um = focus_nm / 1000;
	stringstream popen_cmd;
	popen_cmd << MOVE_Z_ABS << " " << focus_um;

	FreezeWorkInProgress();
	string popen_cmd_str = popen_cmd.str();
	string resultStr = "";
	int status = ShellInvoker::InvokePopenHand( popen_cmd_str, m_threadName, OPTICAL_COLUMN, resultStr );
	return status;
}

int CommandDolerThread::DoleOut_moveToFocusRelativeNM()
{
	/// Extract zz from payload.
	unsigned long focus_offset_nm = this->m_desHelper->ExtractPayloadLongValue( m_commandMessageInObj, FOCUS_OFFSET_NM );
//	cout << m_threadName << ": DoleOut_moveToFocusRelativeNM() received focus_offset_nm of " << focus_offset_nm << endl;
	if (m_states_ulong.find( FOCUS_NM ) == m_states_ulong.end())
	{
		stringstream msg;
		msg << "No cache of " << FOCUS_NM << " found";
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, msg.str().c_str() );
		return STATUS_DEVICE_ERROR; //  EARLY RETURN!	EARLY RETURN!
	}

	unsigned long focus_nm = m_states_ulong[FOCUS_NM] + focus_offset_nm;
	FreezeWorkInProgress();
	int esStatusCode = Implement_moveToFocusNM( m_commandMessageInObj, focus_nm );
	/// This status needs no payload.
	return esStatusCode;
}

int CommandDolerThread::DoleOut_getCurrentFocusNM()
{
	if (m_threadName != DOLER_FOR_OPTICS)
	{
		LOG_ERROR( m_threadName, "sh", "GetCurrentFocusNM sent to wrong thread." );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	int status = STATUS_OK;
	unsigned long focus_nm = 0L;
	/// Mock the command appropriate.
	ComponentState opticalColumnState = g_OpticalColumnState.load( std::memory_order_acquire );
	if (opticalColumnState == ComponentState::Mocked)
	{
		focus_nm = m_states_ulong[FOCUS_NM];
		stringstream msg;
		msg << "emulating focus_nm=" << focus_nm;
		LOG_DEBUG( m_threadName, OPTICAL_COLUMN, msg.str().c_str() );
		return STATUS_OK; // EARLY RETURN!	EARLY RETURN!
	}

	/// get focus
	HardwareType opticalColumnType = g_ComponentHardwareTypeArray[(int)ComponentType::OpticalColumn];
	if (opticalColumnType == HardwareType::IX85)
	{
		status = Implement_getCurrentOlympus_IX85_FocusNM( focus_nm );
	}
	else if (opticalColumnType == HardwareType::LS620)
	{
		status = Implement_getCurrentEtaluma_LS620_FocusNM( focus_nm );
	}
	else
	{
		LOG_FATAL( m_threadName, OPTICAL_COLUMN, "Configured to unsupported hardware type" );
		status = STATUS_HARDWARE_CFG_ERROR;
	}

	if (status == STATUS_OK)
	{
		m_states_ulong[FOCUS_NM] = focus_nm;
		LOG_DEBUG( m_threadName, OPTICAL_COLUMN, "get-focus succeeded" );
		
		FreezeWorkInProgress();
		/// Populate the status payload
		int result = json_object_object_add( m_statusPayloadOutObj,
											 FOCUS_NM,
											 json_object_new_uint64( focus_nm ));
	}
	else
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "get-focus failed" );
	}
	return status;
}

int CommandDolerThread::Implement_getCurrentOlympus_IX85_FocusNM( unsigned long & focusOut_nm )
{
	if (!m_mqttHardwareIO)
	{
		LOG_ERROR( m_threadName, "set-properties", "m_mqttHardwareIO is null.", "Ensure that this method is running on the correct thead." );
		return STATUS_DEVICE_ERROR; // EARLY RETURN!	EARLY RETURN!
	}

	int status = STATUS_OK;
	ComponentState opticalColumnState = g_OpticalColumnState.load( std::memory_order_acquire );
	if (opticalColumnState != ComponentState::Mocked)
	{
		/// Fetch focus_nm from hardware.
		json_object * commandMessageOut = NULL;
		/// Make a temporary copy of m_commandMessageInObj so we can re-use much of it for our outgoing message
		/// without risk of breaking its use in sending status to MqttSender after we are done here.
		int rc = json_object_deep_copy( m_commandMessageInObj, &commandMessageOut, NULL );
		FreezeWorkInProgress();
		bool succeeded = true;
		bool ackReceived = false;
		int ack = 0;
		json_object * jpayloadObj = nullptr;
		stringstream streamtopic;
		streamtopic << TOPIC_MICROSCOPE << CHANNEL_COMMAND;
		string topic = streamtopic.str();

		/// Change a few parts of commandMessageOut.
		string focusMessageType = GET_CURRENT_FOCUS_NM;
		rc = json_object_object_add( commandMessageOut, MESSAGE_TYPE, json_object_new_string( GET_CURRENT_FOCUS_NM ) );
		succeeded &= (rc == 0);

		string desCallMessage = json_object_to_json_string( commandMessageOut );
		json_object_put( commandMessageOut ); /// Free memory.
		FreezeWorkInProgress();
		int opticalColumnStatusCode = m_mqttHardwareIO->CommandTheHardwareClient( focusMessageType, desCallMessage, topic, ackReceived, ack, &jpayloadObj );
		cout << m_threadName << "DoleOut_getCurrentFocusNM received payload of " << json_object_to_json_string( jpayloadObj ) << endl;
		if (jpayloadObj)
		{
			json_object * detailsObj = json_object_object_get( jpayloadObj, DETAILS );
			if (detailsObj)
			{
				json_object * statusObj = json_object_object_get( detailsObj, TEXT_STATUS );
				if (statusObj)
				{
					string statusText = json_object_get_string( statusObj );
					stringstream msg;
					msg << "status=" << statusText;
					LOG_DEBUG( m_threadName, OPTICAL_COLUMN, msg.str().c_str() );
				}
				else
				{
					LOG_ERROR( m_threadName, OPTICAL_COLUMN, "DoleOut_getCurrentFocusNM() payload didn't contain status object." );
					succeeded = false;
				}
				json_object * focusPosObj = json_object_object_get( detailsObj, FOCUSPOS );
				if (focusPosObj)
				{
					focusOut_nm = json_object_get_uint64( focusPosObj );
					stringstream msg;
					msg << "focus_nm=" << focusOut_nm;
					LOG_DEBUG( m_threadName, OPTICAL_COLUMN, msg.str().c_str() );
					json_object_put( jpayloadObj ); /// Free memory.
				}
				else
				{
					LOG_ERROR( m_threadName, OPTICAL_COLUMN, "DoleOut_getCurrentFocusNM() status object didn't contain focuspos." );
					succeeded = false;
				}
			}
		}
		else
		{
			LOG_ERROR( m_threadName, OPTICAL_COLUMN, "DoleOut_getCurrentFocusNM() failed to get return payload." );
			succeeded = false;
		}
		status = OpticalColumnStatusCode_to_ESStatusCode(opticalColumnStatusCode);
		// TODO: Do something with the "details" part of the return payload if status is not OK.
		if (status == STATUS_OK)
		{
			if (ack != STATUS_OK)
			{
				LOG_ERROR( m_threadName, OPTICAL_COLUMN, "get-focus failed" );
				status = ack;
			}
			else if (!ackReceived)
			{
				LOG_ERROR( m_threadName, OPTICAL_COLUMN, "get-focus failed" );
				status = STATUS_DEVICE_ERROR;
			}
		}
	}
	return status;
}

int CommandDolerThread::Implement_getCurrentEtaluma_LS620_FocusNM( unsigned long & focusOut_nm )
{
	if (!FileExists( g_SerialPortDevice_MICRO ))
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "Serial port not found" );
		return STATUS_COMPONENT_NOT_FOUND; ///	EARLY RETURN!	EARLY RETURN!
	}

	string popen_cmd_str = "get_z";
	string resultStr = "";
	FreezeWorkInProgress();
	int status = ShellInvoker::InvokePopenHand( popen_cmd_str, m_threadName, OPTICAL_COLUMN, resultStr );
	if (status == STATUS_OK)
	{
		unsigned long focus_um = StringToULong( resultStr );
		focusOut_nm = focus_um * 1000L;
	}
	return status;
}


/// Implement_SetOpticalColumnTurretPosition
int CommandDolerThread::Implement_SetOpticalColumnTurretPosition( unsigned long position )
{
	int status = STATUS_OK;
	/// Mock the move if appropriate.
	ComponentState opticalColumnState = g_OpticalColumnState.load( std::memory_order_acquire );
	if (opticalColumnState == ComponentState::Mocked)
	{
		m_states_long[ACTIVE_TURRET_POSITION] = position;
		return STATUS_OK; // EARLY RETURN!	EARLY RETURN!
	}

	HardwareType opticalColumnType = g_ComponentHardwareTypeArray[(int)ComponentType::OpticalColumn];
	if (opticalColumnType == HardwareType::IX85)
	{
		status = Implement_SetOlympus_IX85_TurretPosition( position );
	}
	else if (opticalColumnType == HardwareType::LS620)
	{
		status = Implement_SetEtaluma_LS620_TurretPosition( position );
	}
	else
	{
		LOG_FATAL( m_threadName, OPTICAL_COLUMN, "Configured to unsupported hardware type" );
		status = STATUS_HARDWARE_CFG_ERROR;
	}

	/// Update our records.
	if (status == STATUS_OK)
	{
		m_states_long[ACTIVE_TURRET_POSITION] = position;
		LOG_TRACE( m_threadName, OPTICAL_COLUMN, "home turret_position succeeded" );
	}
	else
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "home turret_position failed" );
	}
	return status;
}

///
/// Implement_SetOlympus_IX85_TurretPosition
/// Uses MQTT communications to command the Olympus Windows Service to
/// set the position of the turret
///
int CommandDolerThread::Implement_SetOlympus_IX85_TurretPosition( unsigned long position )
{
/*
	ComponentState opticalColumnState = g_OpticalColumnState.load( std::memory_order_acquire );
	if (opticalColumnState == ComponentState::Mocked)
	{
		LOG_TRACE( m_threadName, "set-parameters", "opticalColumn is mocked" );
		m_states_ulong[ACTIVE_TURRET_POSITION] = position;
		return STATUS_OK; // EARLY RETURN!	EARLY RETURN!
	}
*/
	if (!m_mqttHardwareIO)
	{
		LOG_ERROR( m_threadName, "set-properties", "m_mqttHardwareIO is null.", "Ensure that this method is running on the correct thead." );
		return STATUS_DEVICE_ERROR; // EARLY RETURN!	EARLY RETURN!
	}

	/// Prepare variables that have one-time initialization.
	bool succeeded = true;
	
	/// Make an input parameter
	string messageType = SET_TURRET_POSITION;
	
	/// Create the command message.
	/// ---- step 1: Make the command payload
	json_object * payloadObj = nullptr;
	payloadObj = json_object_new_object();
	/// NOTE: turret position is a rare value that--for this component only--must be converted from 1-based to 0-based when sent to the hardware.
	/// NOTE: When position is used here, in the ES for any reason, it is maintained as 1-based.
	// TODO: Add hooks in case a different turret model does not need this conversion.
	unsigned long zero_based_position = (position > 0L) ? (position - 1L) : position;
	int rc = json_object_object_add( payloadObj, TURRET_POSITION, json_object_new_int( zero_based_position ) );
	succeeded &= (rc == 0);
	/// ---- step 2: Embed the payload in a command message as a JSON object.
	json_object * commandMessageOutOjb = NULL;
	commandMessageOutOjb = m_desHelper->ComposeCommand( SET_TURRET_POSITION,	/// std::string messageType
														0,						/// commandGroupId
														0,						/// commandId
														"",						/// std::string sessionId
														payloadObj );			/// json_object * payload
	/// ---- step 3: Convert the JSON object into a JSON string.
	string desCallMessage = json_object_to_json_string( commandMessageOutOjb );
	json_object_put( commandMessageOutOjb ); /// Free memory.
	
	/// Make the topic string
	stringstream streamtopic;
	streamtopic << TOPIC_MICROSCOPE << CHANNEL_COMMAND;
	string topicName = streamtopic.str();
	/// Make variables for three parameters that return values.
	bool ackReceived = false;
	int ack = 0;
	json_object * jStatusPayloadObj = nullptr;
	LOG_TRACE( m_threadName, OPTICAL_COLUMN, desCallMessage.c_str() );
	int hardwareStatusCode = m_mqttHardwareIO->CommandTheHardwareClient( messageType, desCallMessage, topicName, ackReceived, ack, & jStatusPayloadObj );

	int esStatusCode = OpticalColumnStatusCode_to_ESStatusCode( hardwareStatusCode );
	// TODO: Do something with the "details" part of the return payload if status is not OK.
	if (esStatusCode == STATUS_OK)
	{
		if (ack != STATUS_OK)
		{
			LOG_ERROR( m_threadName, OPTICAL_COLUMN, "filter-move failed" );
			esStatusCode = ack;
		}
		else if (!ackReceived)
		{
			LOG_ERROR( m_threadName, OPTICAL_COLUMN, "filter-move failed" );
			esStatusCode = STATUS_DEVICE_ERROR;
		}
	}
	/*
	succeeded &= ackReceived;
	succeeded &= (ack == STATUS_OK);
	succeeded &= (esStatusCode == STATUS_OK);
	if (succeeded)
	{
		m_states_ulong[ACTIVE_TURRET_POSITION] = position;
		LOG_DEBUG( m_threadName, OPTICAL_COLUMN, "filter-move succeeded" );
	}
	else
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "filter-move failed" );
	}
	*/
	return esStatusCode;
}

/// Implement_SetEtaluma_LS620_TurretPosition
/// Accepts values 0-3.
#define ETALUMA_LS620_TURRET_COUNT 4
int CommandDolerThread::Implement_SetEtaluma_LS620_TurretPosition( unsigned long position )
{
	if (!FileExists( g_SerialPortDevice_MICRO ))
	{
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, "Serial port not found" );
		return STATUS_COMPONENT_NOT_FOUND; ///	EARLY RETURN!	EARLY RETURN!
	}

	int status = STATUS_OK;
	if (position < ETALUMA_LS620_TURRET_COUNT)
	{
		unsigned long degrees = (360 / ETALUMA_LS620_TURRET_COUNT) * position;
		// TODO: Add a constant adjustment to degrees, if it proves necessary.
		;

		FreezeWorkInProgress();
		stringstream popen_cmd;
		popen_cmd << MOVE_T_ABS << " " << degrees;

		string popen_cmd_str = popen_cmd.str();
		string resultStr = "";
		status = ShellInvoker::InvokePopenHand( popen_cmd_str, m_threadName, OPTICAL_COLUMN, resultStr );
	}
	else
	{
		status = STATUS_MOTION_OUT_OF_RANGE;
		stringstream msg;
		msg << "turret_position " << position << " is out of range";
		LOG_ERROR( m_threadName, OPTICAL_COLUMN, msg.str().c_str() );
	}
	return status;
}


///
/// Camera
///
int CommandDolerThread::DoleOut_isCameraImageAvailable()
{
	cout << m_threadName << ": DoleOut_isCameraImageAvailable() returning mock data." << endl;
	bool is_image_available = false;
	// TODO: Implement more here.
	FreezeWorkInProgress();
	;
	/// Populate the status payload
    int result = json_object_object_add( m_statusPayloadOutObj,
										 IS_IMAGE_AVAILABLE,
										 json_object_new_boolean( is_image_available ) );
	return (result == 0) ? STATUS_OK : STATUS_DEVICE_ERROR;
}

///
/// CalculateImageBufferSize
/// Calculates buffer size (in bytes) for a picture.
///
unsigned long CommandDolerThread::CalculateImageBufferSize(	unsigned long pixel_count_x,
															unsigned long pixel_count_y,
															unsigned long bin_factor,
															unsigned long color_depth )
{
	/// Let us keep pixel boundaries alligned on byte boundaries. For example, pixels of 1, 2, or 8 bits
	/// will always have a full byte of space.
	unsigned long bytes_per_pixel = 1 + ((color_depth-1) / 8);
	return 	((pixel_count_x / bin_factor) * (pixel_count_y / bin_factor)) * bytes_per_pixel;
}

///
/// DoleOut_getCameraImage
///
int CommandDolerThread::DoleOut_getCameraImage()
{
	if (m_threadName != DOLER_FOR_OPTICS)
	{
		LOG_ERROR( m_threadName, CAMERA, "running on wrong thread", "Ensure that this method is running on the OPTICS thead." );
		return STATUS_DEVICE_ERROR;
	}
	unique_lock<mutex> lock( g_cameraMutex ); /// Please ensure this is done in every function that access the camera.

	/// Test for limited memory.
	/// One of the biggest use of memory is for images. Our test should be a size
	/// bigger than a typical image to help us identify a problem before it happens
	/// somewhere else.
	void * testBuffer = malloc( 8000000 );
	bool mallocFailed = (testBuffer == NULL);
	free( testBuffer );
	if (mallocFailed)
	{
		LOG_ERROR( m_threadName, "camera", "RUNNING LOW ON MEMORY. HOLDING..." );
		g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed ); /// Cause a hold.
		return STATUS_DEVICE_ERROR;		/// EARLY RETURN!	EARLY RETURN!
	}

	bool succeeded = true;
	/// Get the expected size of an image.
	/// Extract pixel_count_x and pixel_count_y from payload.
	unsigned long pixel_count_x = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, PIXEL_COUNT_X );
	unsigned long pixel_count_y = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, PIXEL_COUNT_Y );
	unsigned long bin_factor = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, BIN_FACTOR );
	unsigned long camera_color_depth = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, CAMERA_COLOR_DEPTH );
	unsigned long scaled_color_depth = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, SCALED_COLOR_DEPTH );
	
	ComponentState cameraState = g_CameraState.load( std::memory_order_acquire );
	if (cameraState == ComponentState::Mocked)
	{
		LOG_TRACE( m_threadName, "camera", "start mocking" );
		/// Allocate space for the image. It will go out in this status message and
		/// be deallocated by the MqttSender thread.
		if (*m_blobSizePtr > 0)
		{
			LOG_TRACE( m_threadName, "camera", "freeing blob" );
			free( *m_blobPtr );
		}
		LOG_TRACE( m_threadName, "camera", "calc blob size" );
		/// If possible, use the mock image file size as the blob size.
		string canned_photo_path = "";
		if ((pixel_count_x == 1920) && (pixel_count_y == 1460)) /// Size typical of Retiga camera
		{
			canned_photo_path = "/usr/share/pics/Rochacha.raw";
		}
		else if ((pixel_count_x == 2100) && (pixel_count_y == 2100)) /// Size typical of Basler camera
		{
			canned_photo_path = "/usr/share/pics/bf_2100x2.raw";
		}
		else
		{
			stringstream msg;
			msg << "Using a 1920x1460 picture when a " << pixel_count_x << "x" << pixel_count_y << " picture was requsted";
			LOG_WARN( m_threadName, "camera", msg.str().c_str() );
			canned_photo_path = "/usr/share/pics/Rochacha.raw";
		}

		long cannedFileSize = GetFileSize( canned_photo_path );
		if (false) // TODO: Enable for debugging the crash of the 1015th picture.
		{
			stringstream msg;
			msg << "canned file size is " << cannedFileSize << "B";
			LOG_TRACE( m_threadName, "camera", msg.str().c_str() );
		}
		if (cannedFileSize < 0L)
		{
			*m_blobSizePtr = pixel_count_x * pixel_count_y;
		}
		else
		{
			*m_blobSizePtr = cannedFileSize;
		}
		LOG_TRACE( m_threadName, CAMERA, "malloc blob" );
		*m_blobPtr = (unsigned char *) malloc( *m_blobSizePtr );
		if (*m_blobPtr == nullptr) /// error condition
		{
			*m_blobSizePtr = 0L;
			stringstream msg;
			msg << "FAILED to allocate " << *m_blobSizePtr << " bytes for image";
			LOG_ERROR( m_threadName, CAMERA, msg.str().c_str() );
			return STATUS_MEMORY_ERROR;		/// EARLY RETURN!	EARLY RETURN!
		}
		
		/// Capture a picture and stuff it in the blob.
		succeeded &= MockCameraCapture( *m_blobPtr, *m_blobSizePtr );
		// TODO: Enable for debugging the crash of the 1015th picture.

		FILE * pOutFile;
		pOutFile = fopen("camera_image.raw", "wb");
		fwrite( (void*)*m_blobPtr, 1, *m_blobSizePtr, pOutFile );
		fclose( pOutFile );

	}
	else
	{
		LOG_TRACE( m_threadName, "camera", "not mocking" );
		/// A sanity check.
			 
		/// Picture-snapping code starts here. This should stay identical to the code in SendPictureInStatusMessage().
		/// Before we allocate memory for the blob, free old memory if there is any.
		if (*m_blobSizePtr > 0)
		{
			stringstream msg;
			msg << "blobSize is " << *m_blobSizePtr << " when it should be 0";
			LOG_ERROR( m_threadName, CAMERA, msg.str().c_str() );
			free( *m_blobPtr );
		}

		/// Calculate size of the buffer for sending to the applciation.
		unsigned long scaledImageSize_bytes = CalculateImageBufferSize( pixel_count_x, pixel_count_y, bin_factor, scaled_color_depth );
		
		/// Allocate space for the outgoing image.
		/// This space will be inside the current WorkMsg. It will go out in the status message and
		/// be deallocated by the MqttSender thread.
		*m_blobSizePtr = scaledImageSize_bytes;
		*m_blobPtr = (unsigned char *) malloc( scaledImageSize_bytes );
		if (*m_blobPtr == nullptr) /// error condition
		{
			*m_blobSizePtr = 0L;
			stringstream msg;
			msg << "FAILED to allocate " << *m_blobSizePtr << " bytes for image";
			LOG_ERROR( m_threadName, CAMERA, msg.str().c_str() );
			return STATUS_MEMORY_ERROR;		/// EARLY RETURN!	EARLY RETURN!
		}
		
		/// Calculate size of the buffer for receiving from the camera.
		unsigned long cameraImageSize_bytes = CalculateImageBufferSize( pixel_count_x, pixel_count_y, bin_factor, camera_color_depth );

		FreezeWorkInProgress();

// TODO: Delete this block.
//		if (bin_factor > 1L)
//		{
//			g_CameraList[0]->SetBinFactor( (int) bin_factor, (int) bin_factor );
//		}
		string getFrameMsg = "";
		
		if (1 <= camera_color_depth && camera_color_depth <= 8)
		{
			unsigned char *blobPtr = (unsigned char *) *m_blobPtr;
			succeeded = false; /// Unsupported case.
		}
		else if (8 < camera_color_depth && camera_color_depth <= 16 && scaled_color_depth <= camera_color_depth)
		{
			/// assume (binary) layout:  ppDD DDDD DDdd dddd  where p=padded 0s, D=desired bits, d=undesired bits
			/// or ppDD DDDD Dddd dddd
			unsigned short unwanted_bits = camera_color_depth - scaled_color_depth; /// eg 6 or 7
			unsigned short * exposureFrame = (unsigned short *) malloc( cameraImageSize_bytes );
			succeeded &= g_CameraList[0]->GetFrame( exposureFrame, getFrameMsg );
			cout << m_threadName << getFrameMsg << endl;
			if (succeeded)
			{
				unsigned char *blobPtr = (unsigned char *) *m_blobPtr;
				for (long i = 0L; i < scaledImageSize_bytes; i++)
				{
					/// Scale down the color range of each pixel, while copying it to the output buffer.
					unsigned short pixel = exposureFrame[i] >> unwanted_bits;
					blobPtr[i] = (unsigned char) (pixel & 0x00FF);
				}
				free( exposureFrame );

				FILE * pOutFile;
				pOutFile = fopen("camera_image.raw", "wb");
				fwrite( (void*)*m_blobPtr, 1, scaledImageSize_bytes, pOutFile );
				fclose( pOutFile );
			}
			else
			{
				free( exposureFrame );
				LOG_ERROR( m_threadName, CAMERA, "Failed to grab picture" );
				return STATUS_CAMERA_FAILED_GET_FRAME;		/// EARLY RETURN!	EARLY RETURN!
			}
		}
		else
		{
			succeeded = false; /// Unsupported case.
			stringstream msg;
			msg << "Failed to change color depth;camera_color_depth=" << camera_color_depth << ";scaled_color_depth=" << scaled_color_depth;
			LOG_ERROR( m_threadName, CAMERA, msg.str().c_str() );
		}
	}
	/// Picture-snapping code ends here.

	/// Lastly, do NOT free memory. That will happen when the shared WorkMsg object gets destructed.
	;
	return succeeded ? STATUS_OK : STATUS_DEVICE_ERROR;
}

/// Pretend to capture a picture from the camera. Do it by reading a canned picture--if is is present--
///     or filling the buffer with medium gray level.
/// The caller must ensure that the blob is already allocated, with blobSize bytes.
/// The pixel counts are not REALLY needed here, as they have already been used to calculate blobSize.
///     They are present to suggest how an equivalent RealCameraCapture() method might be defined.
bool CommandDolerThread::MockCameraCapture( unsigned char *blobPtr, unsigned long blobSize )
{
	bool readSucceeded = true;

	LOG_DEBUG( m_threadName, CAMERA, "emulating camera capture" );
	string canned_photo_path = "";
	if (blobSize == 2803200) /// Size typical of Retiga camera
	{
		canned_photo_path = "/usr/share/pics/Rochacha.raw";
	}
	else if (blobSize == 4410000) /// Size typical of Basler camera
	{
		canned_photo_path = "/usr/share/pics/bf_2100x2.raw";
	}
	else
	{
		stringstream msg;
		msg << "Using a 2803200 pixel picture when a " << blobSize << " picture was requsted";
		LOG_WARN( m_threadName, "camera", msg.str().c_str() );
		canned_photo_path = "/usr/share/pics/Rochacha.raw";
	}

	/// fun fact: Rochacha.raw's filesize is 2803200 bytes.

#ifdef USE_PYTHON_TO_READ_MOCK_IMAGE
	FILE * file = fopen( canned_photo_path.c_str(), "rb" );
	if (file == nullptr)
	{
		stringstream msg;
		msg << "failed to open file " << canned_photo_path;
		LOG_ERROR( m_threadName, CAMERA, msg.str().c_str() );
		readSucceeded = false;
	}
	else
	{
		/// Measure the file size
		fseek( file, 0, SEEK_END );
		long filesize = ftell( file );
		fclose( file );

		LOG_DEBUG( m_threadName, CAMERA, "Measured size of picture file" );
		usleep(100);
		if (blobSize <= filesize)
		{
			/// Use popen() to call a Python script called rochacha.py, which reads the mock camera picture
			/// and delivers it, in the same way that a different Python script will deliver pictures from
			/// Basler cameras. We do this to test the use of popen().
			char buffer[(2 * filesize) + 1];
			FILE* fp = popen( "python3 /usr/bin/rochacha.py", "re");
			readSucceeded &= (fp != NULL);
			int bytesRead = 0;
			if (readSucceeded)
			{
				bytesRead = fread( buffer, 1, (2 * filesize), fp ); /// Expect each byte to be two ascii characters
			}
			else
			{
				LOG_ERROR( m_threadName, CAMERA, "failed to run /usr/bin/rochacha.py" );
			}
			usleep(5000);
			int closeResult = pclose( fp );
			int errro_no = errno;
			if (closeResult != 0)
			{
				stringstream msg;
				msg << "failed closing the popen file, error=" << strerror( errro_no );
				LOG_ERROR( m_threadName, CAMERA, msg.str().c_str() );
			}

			if (bytesRead == (2 * filesize))
			{
				/// Walk through the buffer, converting 2-character ascii strings (representing hex numbers) into
				/// one-byte binary numbers. Write these to the blobPtr.
				char number[3] = "qq";
				int a = 0;
				for (int b = 0; b < filesize; b++)
				{
					number[0] = buffer[a];
					number[1] = buffer[a + 1];
					a += 2;
					blobPtr[b] = (unsigned char) strtol(number, nullptr, 16);
				}
				LOG_TRACE( m_threadName, CAMERA, "Succeeded getting mocked camera image" );
			}
			else
			{
				stringstream msg;
				msg << "only read " << bytesRead << " bytes instead of the expected " << (2 * filesize);
				LOG_ERROR( m_threadName, CAMERA, msg.str().c_str() );
			}
		}
		else
		{
			stringstream msg;
			msg << "File too big! filesize=" << filesize << " > buffer size=" << blobSize;
			LOG_ERROR( m_threadName, CAMERA, msg.str().c_str() );
			readSucceeded = false;
		}
	}
#else
	FILE * file = fopen( canned_photo_path.c_str(), "rb" );
	if (file == nullptr)
	{
		stringstream msg;
		msg << "failed to open file " << canned_photo_path;
		LOG_ERROR( m_threadName, CAMERA, msg.str().c_str() );
		readSucceeded = false;
	}
	else
	{
		/// Measure the file size
		fseek( file, 0, SEEK_END );
		long filesize = ftell( file );
		rewind( file );
		if (blobSize <= filesize)
		{
			size_t result = fread( blobPtr, 1, filesize, file );
			if (result == filesize)
			{
//				cout << m_threadName << " Read succeeded. " << endl;
			}
			else
			{
				stringstream msg;
				msg << "failed to read " << canned_photo_path;
				LOG_ERROR( m_threadName, CAMERA, msg.str().c_str() );
				readSucceeded = false;
			}
		}
		else
		{
			stringstream msg;
			msg << "File too big! filesize=" << filesize << " > buffer size=" << blobSize;
			LOG_ERROR( m_threadName, CAMERA, msg.str().c_str() );
			readSucceeded = false;
		}
		fclose( file );
	}
#endif

	if (!readSucceeded)
	{
		stringstream msg;
		msg << "File read failed. Writing " << blobSize << " bytes of gray data instead.";
		LOG_ERROR( m_threadName, CAMERA, msg.str().c_str() );
		memset ( blobPtr, 128, blobSize );  /// Gray rectangle instead.
		readSucceeded = true; /// Sort of true.
	}
	return readSucceeded;
}

///----------------------------------------------------------------------
/// Implement_setCameraGain
/// Sets the gain value for a camera.
///----------------------------------------------------------------------
int CommandDolerThread::Implement_setCameraGain( int gain )
{
	int status = STATUS_OK;
	ComponentState cameraState = g_CameraState.load( std::memory_order_acquire );
	if (cameraState == ComponentState::Mocked)
	{
		stringstream msg;
		msg << "emulating set gain=" << gain;
		LOG_DEBUG( m_threadName, CAMERA, msg.str().c_str() );
		status = STATUS_OK;
	}
	else if (g_CameraList.size() > 0)
	{
		string cameraReturnMsg = "";
		g_CameraList[0]->SetGain( cameraReturnMsg, gain );
		if (cameraReturnMsg.size() == 0) /// The return string only has content on errors.
		{
			stringstream msg;
			msg << "gain set to " << gain;
			LOG_TRACE( m_threadName, "camera", msg.str().c_str() );
			status = STATUS_OK;
		}
		else
		{
			stringstream msg;
			msg << "setting gain to " << gain << " failed";
			LOG_ERROR( m_threadName, "camera", msg.str().c_str(), cameraReturnMsg.c_str() );
			status = STATUS_OK;
		}
	}
	else
	{
		LOG_ERROR( m_threadName, "camera", "no camera present" );
		status = STATUS_DEVICE_ERROR;
	}
	return status;
}

///----------------------------------------------------------------------
/// Implement_setCameraExposureTime
/// Sets the exposure time for a camera.
///----------------------------------------------------------------------
int CommandDolerThread::Implement_setCameraExposureTime( unsigned long exposureTime_us )
{
	int status = STATUS_OK;
	ComponentState cameraState = g_CameraState.load( std::memory_order_acquire );
	if (cameraState == ComponentState::Mocked)
	{
		stringstream msg;
		msg << "emulating set exposureTime_us=" << exposureTime_us;
		LOG_DEBUG( m_threadName, CAMERA, msg.str().c_str() );
		status = STATUS_OK;
	}
	else if (g_CameraList.size() > 0)
	{
		g_CameraList[0]->SetExposureTime( exposureTime_us );
		stringstream msg;
		msg << "exposure-time set to " << exposureTime_us;
		LOG_TRACE( m_threadName, "camera", msg.str().c_str() );
		status = STATUS_OK;
	}
	else
	{
		LOG_ERROR( m_threadName, "camera", "no camera present" );
		status = STATUS_DEVICE_ERROR;
	}
	return status;
}

///----------------------------------------------------------------------
/// Implement_setCameraBinFactor
/// Sets the bin factor for a camera.
///----------------------------------------------------------------------
int CommandDolerThread::Implement_setCameraBinFactor( int binFactor )
{
	int status = STATUS_OK;
	ComponentState cameraState = g_CameraState.load( std::memory_order_acquire );
	if (cameraState == ComponentState::Mocked)
	{
		stringstream msg;
		msg << "emulating set binFactor=" << binFactor;
		LOG_DEBUG( m_threadName, CAMERA, msg.str().c_str() );
		status = STATUS_OK;
	}
	else if (g_CameraList.size() > 0)
	{
		g_CameraList[0]->SetBinFactor( binFactor, /// binX
									  binFactor ); /// binY
		stringstream msg;
		msg << "bin-factor set to " << binFactor;
		LOG_TRACE( m_threadName, "camera", msg.str().c_str() );
		status = STATUS_OK;
	}
	else
	{
		LOG_ERROR( m_threadName, "camera", "no camera present" );
		status = STATUS_DEVICE_ERROR;
	}
	return status;
}


int CommandDolerThread::DoleOut_MovePlate( bool doReadBarcode )
{
	int status = STATUS_OK;
	if (doReadBarcode)
	{
		if (!ConfirmThreadIdentity( DOLER_FOR_CXPM, MOVE_PLATE_READ_BARCODE )) return STATUS_DEVICE_ERROR;
	}
	else
	{
		if (!ConfirmThreadIdentity( DOLER_FOR_CXPM, MOVE_PLATE )) return STATUS_DEVICE_ERROR;
	}

	/// Extract values from payload.
	PlateType pt = this->m_desHelper->ExtractPayloadPlateType( m_commandMessageInObj );

	string plateId = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, PLATE_ID );
	{
		stringstream msg;
		msg << "plate_id=" << plateId;
		LOG_TRACE( m_threadName, "plates", msg.str().c_str() );
	}
	string from_primary_location = 	this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, FROM_PRIMARY_LOCATION );
	{
		stringstream msg;
		msg << "from_primary_location=" << from_primary_location;
		LOG_TRACE( m_threadName, "plates", msg.str().c_str() );
	}
	string from_sub_location = 	this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, FROM_SUB_LOCATION );
	{
		stringstream msg;
		msg << "from_sub_location=" << from_sub_location;
		LOG_TRACE( m_threadName, "plates", msg.str().c_str() );
	}
	string from_sub_sub_location = 	this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, FROM_SUB_SUB_LOCATION );
	{
		stringstream msg;
		msg << "from_sub_sub_location=" << from_sub_sub_location;
		LOG_TRACE( m_threadName, "plates", msg.str().c_str() );
	}
	string to_primary_location = 	this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, TO_PRIMARY_LOCATION );
	{
		stringstream msg;
		msg << "to_primary_location=" << to_primary_location;
		LOG_TRACE( m_threadName, "plates", msg.str().c_str() );
	}
	string to_sub_location = 	this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, TO_SUB_LOCATION );
	{
		stringstream msg;
		msg << "to_sub_location=" << to_sub_location;
		LOG_TRACE( m_threadName, "plates", msg.str().c_str() );
	}
	string to_sub_sub_location = 	this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, TO_SUB_SUB_LOCATION );
	{
		stringstream msg;
		msg << "to_sub_sub_location=" << to_sub_sub_location;
		LOG_TRACE( m_threadName, "plates", msg.str().c_str() );
	}

	/// Check that the "from" locations in the command match the recorded current locations in inventory.
	if (from_primary_location.compare( m_plateInventory->GetLocation( plateId ) ) != 0)
	{
		status = STATUS_ID_MISMATCH;
		stringstream msg;
		msg << "from_primary_location=" << from_primary_location << "; stored location=" << m_plateInventory->GetLocation( plateId );
		LOG_ERROR( m_threadName, "plates", "location is wrong", msg.str().c_str() );
	}
	if (from_sub_location.compare( m_plateInventory->GetSubLocation( plateId ) ) != 0)
	{
		status = STATUS_ID_MISMATCH;
		stringstream msg;
		msg << "from_sub_location=" << from_sub_location << "; stored sub-location=" << m_plateInventory->GetSubLocation( plateId );
		LOG_ERROR( m_threadName, "plates", "sub-location is wrong", msg.str().c_str() );
	}
	if (from_sub_sub_location.compare( m_plateInventory->GetSubSubLocation( plateId ) ) != 0)
	{
		status = STATUS_ID_MISMATCH;
		stringstream msg;
		msg << "from_sub_sub_location=" << from_sub_sub_location << "; stored sub-sub-location=" << m_plateInventory->GetSubSubLocation( plateId );
		LOG_ERROR( m_threadName, "plates", "sub-sub-location is wrong", msg.str().c_str() );
	}
	
	string barcode = "Barsoom"; // Fake for unit testing. TODO: Change to "" for initial value.
	
	// TODO: Implement more here.
	// While implementing, keep the following rules in mind:
	// 1. At the start of the move, the plate inventory should be updated to position of "in-transit"
	// 2. During the move, this function obviously needs to know the current position.
	// 3. At the end of the move, the plate inventory should be updated to show the final position.
	FreezeWorkInProgress();
	;
	
	/// Populate the status payload
	if (doReadBarcode)
	{
		int result = json_object_object_add( m_statusPayloadOutObj,
											 READ_BARCODE,
											 json_object_new_string( barcode.c_str() ) );
		status = (result == 0) ? STATUS_OK : STATUS_DEVICE_ERROR;
	}
	/// else no payload needed.

	return status;
}

/// Delid the plate in the specified location and place the lid into the specified lid location.
int CommandDolerThread::DoleOut_DelidPlate()
{
	if (!ConfirmThreadIdentity( DOLER_FOR_CXPM, DELID_PLATE )) return STATUS_DEVICE_ERROR;

	PlateType pt = this->m_desHelper->ExtractPayloadPlateType( m_commandMessageInObj );

	string plate_primary_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, PLATE_PRIMARY_LOCATION );
//	cout << m_threadName << ": DoleOut_DelidPlate() received plate_primary_location of " << plate_primary_location << endl;

	string plate_sub_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, PLATE_SUB_LOCATION );
//	cout << m_threadName << ": DoleOut_DelidPlate() received plate_sub_location of " << plate_sub_location << endl;

	string plate_sub_sub_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, PLATE_SUB_SUB_LOCATION );
//	cout << m_threadName << ": DoleOut_DelidPlate() received plate_sub_sub_location of " << plate_sub_sub_location << endl;

	string lid_primary_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, LID_PRIMARY_LOCATION );
//	cout << m_threadName << ": DoleOut_DelidPlate() received lid_primary_location of " << lid_primary_location << endl;

	string lid_sub_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, LID_SUB_LOCATION );
//	cout << m_threadName << ": DoleOut_DelidPlate() received lid_sub_location of " << lid_sub_location << endl;

	string lid_sub_sub_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, LID_SUB_SUB_LOCATION );
//	cout << m_threadName << ": DoleOut_DelidPlate() received lid_sub_sub_location of " << lid_sub_sub_location << endl;

	// TODO: Implement more here.
	FreezeWorkInProgress();
	;
	
	/// Populate the status payload
	int status = STATUS_OK;
	/// No status payload needed.

	return status;
}

/// Relid the plate in the specified location with the lid from the specified location.
int CommandDolerThread::DoleOut_RelidPlate()
{
	if (!ConfirmThreadIdentity( DOLER_FOR_CXPM, RELID_PLATE )) return STATUS_DEVICE_ERROR;

	PlateType pt = this->m_desHelper->ExtractPayloadPlateType( m_commandMessageInObj );

	string plate_primary_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, PLATE_PRIMARY_LOCATION );
//	cout << m_threadName << ": DoleOut_RelidPlate() received plate_primary_location of " << plate_primary_location << endl;

	string plate_sub_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, PLATE_SUB_LOCATION );
//	cout << m_threadName << ": DoleOut_RelidPlate() received plate_sub_location of " << plate_sub_location << endl;

	string plate_sub_sub_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, PLATE_SUB_SUB_LOCATION );
//	cout << m_threadName << ": DoleOut_RelidPlate() received plate_sub_sub_location of " << plate_sub_sub_location << endl;

	string lid_primary_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, LID_PRIMARY_LOCATION );
//	cout << m_threadName << ": DoleOut_RelidPlate() received lid_primary_location of " << lid_primary_location << endl;

	string lid_sub_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, LID_SUB_LOCATION );
//	cout << m_threadName << ": DoleOut_RelidPlate() received lid_sub_location of " << lid_sub_location << endl;

	string lid_sub_sub_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, LID_SUB_SUB_LOCATION );
//	cout << m_threadName << ": DoleOut_RelidPlate() received lid_sub_sub_location of " << lid_sub_sub_location << endl;

	// TODO: Implement more here.
	FreezeWorkInProgress();
	;
	
	/// Populate the status payload
	int status = STATUS_OK;
	/// No status payload needed.

	return status;
}

///----------------------------------------------------------------------
/// DoleOut_MoveTipRack
/// Moves the tip rack from the specified location to the specified location.
///
/// At the initiation of this command, the ES will update the tip rack’s internal location
/// by removing it from its current location and marking it as enumTipRackLocations.InTransit.
///
/// Upon successful completion of the command, the Embedded server will update its internal
/// tip rack inventory by marking the tip rack as being in its new location.  The Complete Status
/// to the client will indicate success which will trigger the client to also update the
/// tip rack’s new location.
///
/// If the command does not complete successfully, the ES will make reasonable effort to determine
/// and update the tip rack’s location and issue an error in the Complete Status.
/// If desired, the client will issue a FindTipRackLocationInInventory command to learn details
/// of the lid’s location.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_MoveTipRack()
{
	int status = STATUS_OK;
	if (!ConfirmThreadIdentity( DOLER_FOR_CXPM, MOVE_TIP_RACK )) return STATUS_DEVICE_ERROR;

	bool succeeded = false;
	TipType tt = m_desHelper->ExtractPayloadTipType( m_threadName, m_commandMessageInObj, & succeeded );

	string tipRackId = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, TIP_RACK_ID );
	{
		stringstream msg;
		msg << "tip_rack_id=" << tipRackId;
		LOG_TRACE( m_threadName, "plates", msg.str().c_str() );
	}
	string from_primary_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, FROM_PRIMARY_LOCATION );
	{
		stringstream msg;
		msg << "from_primary_location=" << from_primary_location;
		LOG_TRACE( m_threadName, "tip-rack", msg.str().c_str() );
	}
	string from_sub_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, FROM_SUB_LOCATION );
	{
		stringstream msg;
		msg << "from_sub_location=" << from_sub_location;
		LOG_TRACE( m_threadName, "tip-rack", msg.str().c_str() );
	}
	string from_sub_sub_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, FROM_SUB_SUB_LOCATION );
	{
		stringstream msg;
		msg << "from_sub_sub_location=" << from_sub_sub_location;
		LOG_TRACE( m_threadName, "tip-rack", msg.str().c_str() );
	}
	string to_primary_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, TO_PRIMARY_LOCATION );
	{
		stringstream msg;
		msg << "to_primary_location=" << to_primary_location;
		LOG_TRACE( m_threadName, "tip-rack", msg.str().c_str() );
	}
	string to_sub_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, TO_SUB_LOCATION );
	{
		stringstream msg;
		msg << "to_sub_location=" << to_sub_location;
		LOG_TRACE( m_threadName, "tip-rack", msg.str().c_str() );
	}
	string to_sub_sub_location = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, TO_SUB_SUB_LOCATION );
	{
		stringstream msg;
		msg << "to_sub_sub_location=" << to_sub_sub_location;
		LOG_TRACE( m_threadName, "tip-rack", msg.str().c_str() );
	}

	/// Check that the "from" locations in the command match the recorded current locations in inventory.
	if (from_primary_location.compare( m_tipInventory->GetLocation( tipRackId ) ) != 0)
	{
		status = STATUS_ID_MISMATCH;
		stringstream msg;
		msg << "from_primary_location=" << from_primary_location << "; stored location=" << m_plateInventory->GetLocation( tipRackId );
		LOG_ERROR( m_threadName, "tip_rack", "location is wrong", msg.str().c_str() );
	}
	if (from_sub_location.compare( m_tipInventory->GetSubLocation( tipRackId ) ) != 0)
	{
		status = STATUS_ID_MISMATCH;
		stringstream msg;
		msg << "from_sub_location=" << from_sub_location << "; stored location=" << m_plateInventory->GetSubLocation( tipRackId );
		LOG_ERROR( m_threadName, "tip_rack", "location is wrong", msg.str().c_str() );
	}
	if (from_sub_sub_location.compare( m_tipInventory->GetSubSubLocation( tipRackId ) ) != 0)
	{
		status = STATUS_ID_MISMATCH;
		stringstream msg;
		msg << "from_sub_sub_location=" << from_sub_sub_location << "; stored location=" << m_plateInventory->GetSubSubLocation( tipRackId );
		LOG_ERROR( m_threadName, "tip_rack", "location is wrong", msg.str().c_str() );
	}

	// TODO: Implement more here.
	FreezeWorkInProgress();
	;
	
	/// Populate the status payload
	/// No status payload needed.

	return status;

}

///----------------------------------------------------------------------
/// DoleOut_MoveAllTipsToSafeHeight
/// A. MoveZToSafeHeight()
/// B. MoveZZToSafeHeight()
/// C. MoveZZZToSafeHeight()
int CommandDolerThread::DoleOut_MoveAllTipsToSafeHeight()
{
	int status = DoleOut_MoveZToSafeHeight();
	if (status == STATUS_OK)
	{
		status = DoleOut_MoveZZToSafeHeight();
	}
	if (status == STATUS_OK)
	{
		status = DoleOut_MoveZZZToSafeHeight();
	}
	return status;
}

///----------------------------------------------------------------------
/// Set a component's property in advance of its use.
/// This function handles a purely internal command composed by the Interpreter.
/// The properties handled here are treated as states. Some of them turn lights on/off.
/// Others move or adjust hardware--an effect that will take some time.
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_SetHardwareState()
{
	int status = STATUS_OK;
	m_doSendStatus = false;  /// No status message for set_hardware_state. It is a purely internal message.
	string component_type_str = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, COMPONENT_TYPE );
	{
		stringstream msg;
		msg << "component_type_str=" << component_type_str;
		LOG_TRACE( m_threadName, "set-parameters", msg.str().c_str() );
	}

	string data_type = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, DATA_TYPE );
	{
		stringstream msg;
		msg << "DoleOut_SetHardwareState() received data_type of " << data_type;
		LOG_TRACE( m_threadName, "set-parameters", msg.str().c_str() );
	}

	string property_name = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, PROPERTY_NAME );
	{
		stringstream msg;
		msg << "DoleOut_SetHardwareState() received property_name of " << property_name;
		LOG_TRACE( m_threadName, "set-parameters", msg.str().c_str() );
	}

	string property_value = this->m_desHelper->ExtractPayloadStringValue( m_commandMessageInObj, PROPERTY_VALUE );
	{
		stringstream msg;
		msg << "DoleOut_SetHardwareState() received property_value of " << property_value;
		LOG_TRACE( m_threadName, "set-parameters", msg.str().c_str() );
	}

// TODO: Get the vendor or model so we can tell what kind of camera we are working with.

	ComponentType componentType = StringNameToComponentTypeEnum( component_type_str.c_str() );

	FreezeWorkInProgress();
	if (componentType == ComponentType::XYAxes)
	{
		if ((property_name == Y_SET_SPEED) && (data_type == UINT32))
		{
			unsigned long ySetSpeed = stoul( property_value.c_str() );
			// TODO: Give this value to hardware.
		}
		if ((property_name == Y_ACCELERATION) && (data_type == UINT32))
		{
			unsigned long yAcceleration = stoul( property_value.c_str() );
			// TODO: Give this value to hardware.
		}
		if ((property_name == Y_DECELERATION) && (data_type == UINT32))
		{
			unsigned long yDeceleration = stoul( property_value.c_str() );
			// TODO: Give this value to hardware.
		}
		if ((property_name == Y_JOGGING_SPEED) && (data_type == UINT32))
		{
			unsigned long yJoggingSpeed = stoul( property_value.c_str() );
			// TODO: Give this value to hardware.
		}
		if ((property_name == Y_JOGGING_ACCELERATION) && (data_type == UINT32))
		{
			unsigned long yJoggingAcceleration = stoul( property_value.c_str() );
			// TODO: Give this value to hardware.
		}
		if ((property_name == Y_JOGGING_DECELERATION) && (data_type == UINT32))
		{
			unsigned long yJoggingDeceleration = stoul( property_value.c_str() );
			// TODO: Give this value to hardware.
		}
	}
	if (componentType == ComponentType::AnnularRingHolder)
	{
		if (property_name == CURRENT_RING_POSITION)
		{
			LOG_TRACE( m_threadName, "sh", "current_ring_position passed its tests" );
			long currentRingPosition = stol( property_value.c_str() );
			int ring_status = DoleOut_MoveToAnnularRingPosition( currentRingPosition );
			if (status == STATUS_OK) status = ring_status;
		}
		else
		{
			stringstream msg;
			msg << "current_ring_position FAILED a test|name=" << property_name << "|type=" << data_type;
			LOG_ERROR( m_threadName, "sh", msg.str().c_str() );
		}
	}
	if (componentType == ComponentType::WhiteLightSource)
	{
		if ((property_name == ON_STATUS) && (data_type == BOOLEAN))
		{
			std::transform( property_value.begin(), property_value.end(), property_value.begin(), (int(*)(int)) std::tolower );
			bool onStatus = (property_value == "true");
			int light_status = Implement_SetWhiteLightSourceOn( onStatus );
			if (status == STATUS_OK) status = light_status;
		}
	}
	if (componentType == ComponentType::FluorescenceLightFilter)
	{
		if ((property_name == ON_STATUS) && (data_type == BOOLEAN))
		{
			std::transform( property_value.begin(), property_value.end(), property_value.begin(), (int(*)(int)) std::tolower );
			bool onState = (property_value == "true");
			int light_status = Implement_setFluorescenceLightFilterOn( onState ); /// updates m_states_bool[FLUORESCENCE_LIGHT_ON_STATE] too.
			if (status == STATUS_OK) status = light_status;
		}
		if ((property_name == ACTIVE_FLUORESCENCE_FILTER_POSITION) && (data_type == INT16))
		{
			long activeFluorescenceFilterPosition = stol( property_value.c_str() );
			int position_status = Implement_setFluorescenceLightFilterPosition( activeFluorescenceFilterPosition ); /// updates m_states_long[ACTIVE_FLUORESCENCE_FILTER_POSITION] too.
			if (status == STATUS_OK) status = position_status;
		}
	}
	if (componentType == ComponentType::Camera)
	{
		if ((property_name == GAIN) && (data_type == UINT16))
		{
			unsigned long gain = stoul( property_value.c_str() );
			// TODO: Make sure we are working with a Retiga R3 camera.
			int gain_status = Implement_setCameraGain( (int) gain );
			if (status == STATUS_OK) status = gain_status;
		}
		if ((property_name == EXPOSURE) && (data_type == UINT32))
		{
			unsigned long exposure = stoul( property_value.c_str() );
			// TODO: Make sure we are working with a Retiga R3 camera.
			int exposure_status = Implement_setCameraExposureTime( exposure );
			if (status == STATUS_OK) status = exposure_status;
		}
		if ((property_name == BIN_FACTOR) && (data_type == UINT16))
		{
			unsigned long binFactor = stoul( property_value.c_str() );
			// TODO: Make sure we are working with a Retiga R3 camera.
			int bin_factor_status = Implement_setCameraBinFactor( (int) binFactor );
			if (status == STATUS_OK) status = bin_factor_status;
		}
	}
	if (componentType == ComponentType::OpticalColumn)
	{
		if ((property_name == ACTIVE_TURRET_POSITION) && (data_type == UINT16))
		{
			unsigned long opticalColumnPosition = stoul( property_value.c_str() );
			int positionStatus = Implement_SetOpticalColumnTurretPosition( opticalColumnPosition );
			if (status == STATUS_OK) status = positionStatus;
			LOG_TRACE( m_threadName, "set-parameters", "called Implement_SetOpticalColumnTurretPosition()" );
		}
/*
	This won't work because focus is not a property, let alone a property with a ^.
	If we want to restore the old focus at this point, that value should be packed in the command for setting
	the turret postion. See CommandInterpreterThread::SetTurretStateIfNeeded(). That is an effective way to
	guarantee that the turret position is restored *before* the focus is restored.
		if ((property_name == FOCUS_NM) && (data_type == UINT32))
		{
			LOG_WARN( m_threadName, "set-parameters", "Implement_moveToFocusNM() is currently disabled" );
			unsigned long focus_nm = stoul( property_value.c_str() );
			int focusStatus = Implement_moveToFocusNM( focus_nm );
			if (focusStatus == STATUS_OK) status = focusStatus;
			LOG_TRACE( m_threadName, "set-parameters", "called Implement_moveToFocusNM()" );
		}
*/
	}
	return status;
}

int CommandDolerThread::Implement_EnableAxis( string axisName, bool & confirmEnabled_out )
{
	int status = STATUS_OK;
	string error_msg = "";
	try {
		stringstream msg;
		msg << "Enabling named axis " << axisName;
		LOG_INFO( m_threadName, "init", msg.str().c_str() );
		MotionAxis &axis = g_motionServices_ptr->getAxis( axisName );
		axis.enable();
		confirmEnabled_out = true;
	} catch (std::exception &e) {
		error_msg = std::string(e.what());
		status = STATUS_DEVICE_ERROR;
	} catch (std::string &s) {
		error_msg = s;
		status = STATUS_DEVICE_ERROR;
	} catch (std::string *s) {
		error_msg = *s;
		status = STATUS_DEVICE_ERROR;
	} catch (...) {
		error_msg = "EXCEPTION: Couldn't enable axis; unknown exception";
		status = STATUS_DEVICE_ERROR;
	}
	if (status != STATUS_OK)
	{
		confirmEnabled_out = false;
		stringstream msg;
		msg << "EXCEPTION: Couldn't enable " << axisName << " axis: " << error_msg;
		LOG_FATAL( m_threadName, "init", msg.str().c_str() );
	}
	return status;
}

bool CommandDolerThread::Implement_SetHomeInputs( json_object * payloadObj_in, const char * homeInputsName_in,
												  int controllerID_in, std::string axisName_in )
{
	unsigned long homeOffset = 0L;
	unsigned long clearKillsTimeout = 0L;
	unsigned long coordMoveAccel = 0L;
	unsigned long coordMoveDecel = 0L;
	unsigned long coordMoveStp = 0L;
	unsigned long coordMoveVel = 0L;
	unsigned long homeFinalVel = 0L;
	unsigned long jogAccel = 0L;
	unsigned long jogDecel = 0L;
	unsigned long jogVel = 0L;
	unsigned long jogHLDC = 0L;
	string report = "";
	bool succeeded = true;

	json_object * homeInputsObj = json_object_object_get( payloadObj_in, homeInputsName_in );
	if (homeInputsObj)
	{
		succeeded = DesHelper::ExtractHomeInputsFromPayload( payloadObj_in, homeInputsName_in, homeOffset,
															 clearKillsTimeout, coordMoveAccel, coordMoveDecel,
															 coordMoveStp, coordMoveVel, homeFinalVel,
															 jogAccel, jogDecel, jogVel, jogHLDC, report );
		LOG_TRACE( m_threadName, "init", report.c_str() );
		if (succeeded)
		{
			try {
				stringstream msg;
				msg << "Homing " <<  homeInputsName_in;
				msg << "|jogAccel=" << ((float) jogAccel/_1MEG_F) << "|jogDecel=" << ((float) jogDecel/_1MEG_F) << "|jogVel" << ((float) jogVel/_1MEG_F) << "|jogHDLC=" << jogHLDC;
				msg << "|CoordMovAccel=" << ((float) coordMoveAccel/_1MEG_F) << "|CoordMovDecel=" << ((float) coordMoveDecel/_1MEG_F);
				msg << "|CoordMovStp=" << ((float) coordMoveStp/_1MEG_F) << "|CoordMovVel=" << ((float) coordMoveVel/_1MEG_F) << "|HomeFinalVel=" << ((float) homeFinalVel/_1MEG_F);
				msg << "|homeOffset=" << ((float)homeOffset/_1MEG_L) << "|ClrKillsTimeout=" << clearKillsTimeout;
				LOG_TRACE( m_threadName, homeInputsName_in, msg.str().c_str() );
				
				if ((jogAccel == 0.0) || (jogVel == 0.0))
				{
					LOG_ERROR( m_threadName, homeInputsName_in, "Homing accel or vel is 0. Aborting." );
					return false;	// EARLY RETURN!!	EARLY RETURN!!
				}

				ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at( controllerID_in );
				pController->binarySetFloat( HOME_OFFSET_SN, (float) homeOffset/_1MEG_F );
				pController->binarySetLong( CLR_KILLS_TIME_OUT_SN, (int32_t) clearKillsTimeout );
				pController->binarySetFloat( COORD_MOVE_ACCEL_SN, (float) coordMoveAccel/_1MEG_F );
				pController->binarySetFloat( COORD_MOVE_DECEL_SN, (float) coordMoveDecel/_1MEG_F );
				pController->binarySetFloat( COORD_MOVE_STP_SN, (float) coordMoveStp/_1MEG_F );
				pController->binarySetFloat( COORD_MOVE_VEL_SN, (float) coordMoveVel/_1MEG_F );
				pController->binarySetFloat( HOME_FINAL_VEL_SN, (float) homeFinalVel/_1MEG_F );
				pController->binarySetFloat( JOG_ACCEL_SN, (float) jogAccel/_1MEG_F );
				pController->binarySetFloat( JOG_DECEL_SN, (float) jogDecel/_1MEG_F );
				pController->binarySetFloat( JOG_VEL_SN, (float) jogVel/_1MEG_F );
				pController->binarySetLong( JOG_HLDEC_SN, (int32_t) jogHLDC );
				usleep(10000);
			} catch (std::exception &ex) {
				LOG_FATAL( m_threadName, "init", "Exception while setting input values" );
				succeeded = false;
			}
		}
	}
	return succeeded;
}


///----------------------------------------------------------------------
/// Implement_HomeAxis(), which does not check for status values.
/// Use this for running homing functions like the ones in Controller C.
///----------------------------------------------------------------------
int CommandDolerThread::Implement_HomeAxis( int controllerID, int prog0_routineID, string axisName )
{
	/// Run the prog0 HomeX routine.
	string error_msg = "";
	int status = STATUS_OK;
	try {
		stringstream msg;
		msg << "Homing axis " << axisName;
		LOG_TRACE( m_threadName, "init", msg.str().c_str() );
		ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at( controllerID );

		pController->launchProgram( prog0_routineID, true );
		int loopCount = 0;
		while ((status == STATUS_OK) && (!pController->programIsRunning( prog0_routineID )))
		{
			usleep(100000);
			if (loopCount++ > MAX_WAIT_FOR_START_LOOPS)
			{
				error_msg = "FAILED to start prog0 Home routine";
				status = STATUS_TIMEOUT_ERROR; // Break out of loop.
			}
		}
		/// If all went well, MoveToForceZ is now running.
		while ((status == STATUS_OK) && (pController->programIsRunning( prog0_routineID )))
		{
			usleep(100000);
			if (loopCount++ > MAX_WAIT_FOR_END_LOOPS)
			{
				error_msg = "prog0 Home routine timed out";
				status = STATUS_TIMEOUT_ERROR; // Break out of loop.
			}
		}
		usleep(50000);
	} catch (MotionException &x) {
		error_msg = x.getEventDesc();
		status = STATUS_DEVICE_ERROR;
	} catch (std::exception &ex) {
		error_msg = "Setting P0 failed";
		status = STATUS_DEVICE_ERROR;
	}
	if (status != STATUS_OK)
	{
		stringstream msg;
		msg << "EXCEPTION: Couldn't home " << axisName << " axis: " << error_msg;
		LOG_FATAL( m_threadName, "init", msg.str().c_str() );
	}
	else
	{
		stringstream msg;
		msg << "---HOME " << axisName << " DONE---";
		LOG_TRACE( m_threadName, "init", msg.str().c_str() );
	}

	return status;
}

///----------------------------------------------------------------------
/// Implement_HomeAxis() for use with homing functions that return status
/// values.
///----------------------------------------------------------------------
int CommandDolerThread::Implement_HomeAxis( int controllerID, int prog0_routineID, string axisName, bool & confirmHomed_out )
{
	/// Run the prog0 HomeX routine.
	string error_msg = "";
	int status = STATUS_OK;
	try {
		stringstream msg;
		msg << "Homing axis " << axisName;
		LOG_TRACE( m_threadName, "init", msg.str().c_str() );
		ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at( controllerID );
		/// Start monitoring output parameter PNNN where NNN is prog0_routineID.
		pController->monitor( prog0_routineID, ACR7000::pvalue_type_t::pvalue_type_float, 100 );
		usleep(500000);
		/// Start the prog0 routine by writing prog0_routineID to P0.
		pController->runProgram( prog0_routineID, true );
		usleep(50000);
		pController->cancelMonitoring( prog0_routineID );
	} catch (MotionException &x) {
		error_msg = x.getEventDesc();
		status = STATUS_DEVICE_ERROR;
	} catch (std::exception &ex) {
		error_msg = "Setting P0 failed";
		status = STATUS_DEVICE_ERROR;
	}
	if (status != STATUS_OK)
	{
		confirmHomed_out = false;
		stringstream msg;
		msg << "EXCEPTION: Couldn't home " << axisName << " axis: " << error_msg;
		LOG_FATAL( m_threadName, "init", msg.str().c_str() );
	}
	else
	{
		confirmHomed_out = true;
		stringstream msg;
		msg << "---HOME " << axisName << " DONE---";
		LOG_TRACE( m_threadName, "init", msg.str().c_str() );
	}

	return status;
}

///----------------------------------------------------------------------
/// DoleOut_ClearKills()
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_ClearKills()
{
	int status = STATUS_OK;
	if (m_threadName == DOLER_FOR_CXD)
	{
		if ((g_XYState.load( std::memory_order_acquire ) !=  ComponentState::Mocked) ||
			(g_XXState.load( std::memory_order_acquire ) !=  ComponentState::Mocked) ||
			(g_YYState.load( std::memory_order_acquire ) !=  ComponentState::Mocked))
		{
			status = Implement_ClearKills( A_CONTROLLER );
		}
		if ((g_ZState.load( std::memory_order_acquire ) !=  ComponentState::Mocked) ||
			(g_ZZState.load( std::memory_order_acquire ) !=  ComponentState::Mocked) ||
			(g_ZZZState.load( std::memory_order_acquire ) !=  ComponentState::Mocked) ||
			(g_AnnularRingHolderState.load( std::memory_order_acquire ) !=  ComponentState::Mocked))
		{
			/// Theoretically, this could happen while the dolerForRingHolder is doing work.
			unique_lock<mutex> lock( g_controllerBMutex ); /// Prevent interfering with potential annular ring holder moves.
			status = Implement_ClearKills( B_CONTROLLER );
		}
	}

	if (m_threadName == DOLER_FOR_CXD)
	{
		if ((g_AspirationPump1State.load( std::memory_order_acquire ) !=  ComponentState::Mocked) ||
			(g_DispensePump1State.load( std::memory_order_acquire ) !=  ComponentState::Mocked) ||
			(g_DispensePump2State.load( std::memory_order_acquire ) !=  ComponentState::Mocked) ||
			(g_DispensePump3State.load( std::memory_order_acquire ) !=  ComponentState::Mocked))
		{
			status = Implement_ClearKills( C_CONTROLLER );
		}
	}
	return status;
}

///----------------------------------------------------------------------
/// Implement_ClearKills()
///----------------------------------------------------------------------
int CommandDolerThread::Implement_ClearKills( int controllerID )
{
	int status = STATUS_OK;
	string error_msg = "";
	try {
		LOG_TRACE( m_threadName, "recovery", "running ClrKills" );
		ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at( controllerID );
		/// ClrKills often runs too fast to detect it starting and stopping.
		/// Since this function is just for debug support, Perform best effort.
		pController->launchProgram( CLEAR_KILLS_ROUTINE, true );
		/// Some _ClrKills functions have built-in delays of 0.5 seconds, which can happen up to 5 times.
		sleep( 3 );
	} catch (MotionException &x) {
		error_msg = x.getEventDesc();
		status = STATUS_MOVEMENT_FAILED;
	} catch (std::exception &ex) {
		error_msg = "Setting P0 failed";
		status = STATUS_DEVICE_ERROR;
	}
	if (status != STATUS_OK)
	{
		stringstream msg;
		msg << "ClrKills failed|" << error_msg;
		LOG_ERROR( m_threadName, "recovery",  msg.str().c_str() );
	}
	else
	{
		LOG_TRACE( m_threadName, "recovery", "ClrKills finished" );
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_InitHardwareControllers()
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_InitHardwareControllers()
{
	int status = STATUS_OK;
	m_doSendStatus = false;  /// No status message for set_hardware_state. It is a purely internal message.
	LOG_TRACE( m_threadName, "init", "-" );
	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string( payloadObj );
		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "init", "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
		return status; /// EARLY RETURN!!	EARLY RETURN!!
	}
	if (m_threadName == DOLER_FOR_CXD)
	{
		///
		/// If any axis of the 'A' controller is not mocked, run the controller's setup routine.
		///
		if ((g_XYState.load( std::memory_order_acquire ) == ComponentState::Uninitialized) ||
			(g_XXState.load( std::memory_order_acquire ) == ComponentState::Uninitialized) ||
			(g_YYState.load( std::memory_order_acquire ) == ComponentState::Uninitialized))
		{
			bool succeeded = true;
			long excessErrorY = 0L;
			long excessErrorX = 0L;
			long excessErrorYY = 0L;
			long excessErrorXX = 0L;
			long inPositionBandY = 0L;
			long inPositionBandX = 0L;
			long inPositionBandYY = 0L;
			long inPositionBandXX = 0L;
			
			/// These are for some long-running variables in the controller.
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, IN_POSITION_BAND_Y, inPositionBandY );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, IN_POSITION_BAND_X, inPositionBandX );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, IN_POSITION_BAND_YY, inPositionBandYY );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, IN_POSITION_BAND_XX, inPositionBandXX );

			/// These are for running the prog0 Setup routine.
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, EXCESS_ERROR_Y, excessErrorY );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, EXCESS_ERROR_X, excessErrorX );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, EXCESS_ERROR_YY, excessErrorYY );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, EXCESS_ERROR_XX, excessErrorXX );
			if (!succeeded)
			{
				status = STATUS_CMD_MSG_DATA_MISSING;
			}
			if (status != STATUS_OK)
			{
				LOG_ERROR( m_threadName, "init", "Failed to set X,Y,XX,YY IPBs" );
				return status; /// EARLY RETURN!!	EARLY RETURN!!
			}

			if (true)
			{
				stringstream msg;
				msg << "Running Init for controller A|ClrKillsTimeout=" << DEFAULT_CLEAR_KILLS_TIMEOUT;
				msg << "|Excess errors: [Y=" << excessErrorY << "|X=" << excessErrorX << "|YY=" << excessErrorYY << "|XX=" << excessErrorXX << "] ";
				msg << "|InPositionBands: [Y=1|X=1|YY=1|XX=1]";
				LOG_TRACE( m_threadName, "init", msg.str().c_str() );
			}
			if (status == STATUS_OK)
			{
				try {
					LOG_INFO( m_threadName, "init", "Running prog0 Setup for controller 0" );
					ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at(A_CONTROLLER);
					/// Send input parameters.
					pController->binarySetLong( CLR_KILLS_TIME_OUT_SN, DEFAULT_CLEAR_KILLS_TIMEOUT );
					pController->binarySetLong( EXCESS_ERROR_Y_SN, excessErrorY );
					pController->binarySetLong( EXCESS_ERROR_X_SN, excessErrorX );
					pController->binarySetLong( EXCESS_ERROR_XX_SN, excessErrorXX );
					pController->binarySetLong( EXCESS_ERROR_YY_SN, excessErrorYY );
					pController->binarySetLong( IN_POSITION_BAND_Y_SN, 1 );
					pController->binarySetLong( IN_POSITION_BAND_X_SN, 1 );
					pController->binarySetLong( IN_POSITION_BAND_XX_SN, 1 );
					pController->binarySetLong( IN_POSITION_BAND_YY_SN, 1 );
					usleep(10000);
					pController->runProgram( SETUP_ROUTINE, true );
					usleep(50000);
				} catch (MotionException &x) {
					stringstream msg;
					msg << "EXCEPTION THROWN: " << x.getEventDesc() << "; ABORTING";
					LOG_FATAL( m_threadName, "init", msg.str().c_str() );
					status = STATUS_DEVICE_ERROR;
				} catch (std::exception &ex) {
					LOG_ERROR( m_threadName, "init", "Setting P0 failed" );
					status = STATUS_DEVICE_ERROR;
				}
			}
			if (status != STATUS_OK)
			{
				LOG_ERROR( m_threadName, "init", "Failed to run prog0 for X,Y,XX,YY IPBs" );
				return status; /// EARLY RETURN!!	EARLY RETURN!!
			}

			if (status == STATUS_OK)
			{
				try {
					LOG_INFO( m_threadName, "init", "Setting IPBs for controller 0" );
					ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at(A_CONTROLLER);

					MotionAxis &Yaxis = g_motionServices_ptr->getAxis("Y");
					pController->setIPB( Yaxis.getChannelIndex(), (double)inPositionBandY/_1MEG_F );
					pController->setSecondaryAxisFlag( BIT_INDEX_FAST_IPB, Yaxis.getChannelIndex() );

					MotionAxis &Xaxis = g_motionServices_ptr->getAxis("X");
					pController->setIPB( Xaxis.getChannelIndex(), (double)inPositionBandX/_1MEG_F );
					pController->setSecondaryAxisFlag( BIT_INDEX_FAST_IPB, Xaxis.getChannelIndex() );

					MotionAxis &YYaxis = g_motionServices_ptr->getAxis("YY");
					pController->setIPB( YYaxis.getChannelIndex(), (double)inPositionBandYY/_1MEG_F );

					MotionAxis &XXaxis = g_motionServices_ptr->getAxis("XX");
					pController->setIPB( XXaxis.getChannelIndex(), (double)inPositionBandXX/_1MEG_F );
					pController->setSecondaryAxisFlag( BIT_INDEX_FAST_IPB, XXaxis.getChannelIndex() );
				} catch (MotionException &x) {
					stringstream msg;
					msg << "EXCEPTION THROWN: " << x.getEventDesc() << "; ABORTING";
					LOG_FATAL( m_threadName, "init", msg.str().c_str() );
					status = STATUS_DEVICE_ERROR;
				} catch (std::exception &ex) {
					LOG_ERROR( m_threadName, "init", "Setting P0 failed" );
					status = STATUS_DEVICE_ERROR;
				}
			}
		}
		/// If the XY axis is not mocked, enable both the X and Y axes and advance their state to "settings-confirmed".
		ComponentState state = g_XYState.load( std::memory_order_acquire );
		if ((state == ComponentState::Uninitialized) && (status == STATUS_OK))
		{
			bool x_confirmed = false;
			if (status == STATUS_OK)
			{
				status = Implement_EnableAxis( "X", x_confirmed );
			}
			bool y_confirmed = false;
			if (status == STATUS_OK)
			{
				status = Implement_EnableAxis( "Y", y_confirmed );
			}
			if (x_confirmed && y_confirmed) g_XYState.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
		}
		/// If the XX axis is not mocked, enable the XX axis and advance its state to "settings-confirmed".
		state = g_XXState.load( std::memory_order_acquire );
		if ((state == ComponentState::Uninitialized) && (status == STATUS_OK))
		{
			bool xx_confirmed = false;
			status = Implement_EnableAxis( "XX", xx_confirmed );
			if (xx_confirmed) g_XXState.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
		}
		/// If the YY axis is not mocked, enable the YY axis and advance its state to "settings-confirmed".
		state = g_YYState.load( std::memory_order_acquire );
		if ((state == ComponentState::Uninitialized) && (status == STATUS_OK))
		{
			bool yy_confirmed = false;
			status = Implement_EnableAxis( "YY", yy_confirmed );
			if (yy_confirmed) g_YYState.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
		}

		///
		/// If any axis of the 'B' controller is not mocked, run the controller's setup routine.
		///
		if ((g_ZState.load( std::memory_order_acquire ) == ComponentState::Uninitialized) ||
			(g_ZZState.load( std::memory_order_acquire ) == ComponentState::Uninitialized) ||
			(g_ZZZState.load( std::memory_order_acquire ) == ComponentState::Uninitialized) ||
			(g_AnnularRingHolderState.load( std::memory_order_acquire ) == ComponentState::Uninitialized))
		{
			bool succeeded = true;
			long excessErrorSH = 0L;
			long excessErrorZ = 0L;
			long excessErrorZZ = 0L;
			long excessErrorZZZ = 0L;
			long inPositionBandSH = 0L;
			long inPositionBandZ = 0L;
			long inPositionBandZZ = 0L;
			long inPositionBandZZZ = 0L;

			/// These are some long-running variables in the controller.
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, IN_POSITION_BAND_SH, inPositionBandSH );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, IN_POSITION_BAND_Z, inPositionBandZ );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, IN_POSITION_BAND_ZZ, inPositionBandZZ );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, IN_POSITION_BAND_ZZZ, inPositionBandZZZ );

			/// Some component of controller B is going to be used, so call controller B's Setup routine.
			/// These are for running the prog0 Setup routine.
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, EXCESS_ERROR_SH, excessErrorSH );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, EXCESS_ERROR_Z, excessErrorZ );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, EXCESS_ERROR_ZZ, excessErrorZZ );
			succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, EXCESS_ERROR_ZZZ, excessErrorZZZ );
			if (!succeeded)
			{
				status = STATUS_CMD_MSG_DATA_MISSING;
			}
			if (true)
			{
				stringstream msg;
				msg << "Running Init for controller B|ClrKillsTimeout=" << DEFAULT_CLEAR_KILLS_TIMEOUT;
				msg << "|Excess errors: [SH=" << excessErrorSH << "|Z=" << excessErrorZ << "|ZZ=" << excessErrorZZ << "|ZZZ=" << excessErrorZZZ << "] ";
				msg << "|InPositionBands: [SH=1|Z=1|ZZ=1|ZZZ=1]";
				LOG_TRACE( m_threadName, "init", msg.str().c_str() );
			}
			if (status == STATUS_OK)
			{
				try {
					LOG_INFO( m_threadName, "init", "Running prog0 Setup for controller 1" );
					ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at(B_CONTROLLER);
					/// Send input parameters.
					pController->binarySetLong( CLR_KILLS_TIME_OUT_SN, DEFAULT_CLEAR_KILLS_TIMEOUT );
					pController->binarySetLong( EXCESS_ERROR_SH_SN, excessErrorSH );
					pController->binarySetLong( EXCESS_ERROR_Z_SN, excessErrorZ );
					pController->binarySetLong( EXCESS_ERROR_ZZ_SN, excessErrorZZ );
					pController->binarySetLong( EXCESS_ERROR_ZZZ_SN, excessErrorZZZ );
					pController->binarySetLong( IN_POSITION_BAND_SH_SN, 1 );
					pController->binarySetLong( IN_POSITION_BAND_Z_SN, 1 );
					pController->binarySetLong( IN_POSITION_BAND_ZZ_SN, 1 );
					pController->binarySetLong( IN_POSITION_BAND_ZZZ_SN, 1 );
					usleep(10000);
					pController->runProgram( SETUP_ROUTINE, true );
					usleep(50000);
				} catch (MotionException &x) {
					stringstream msg;
					msg << "EXCEPTION THROWN: " << x.getEventDesc() << "; ABORTING";
					LOG_FATAL( m_threadName, "init", msg.str().c_str() );
					status = STATUS_DEVICE_ERROR;
				} catch (std::exception &ex) {
					LOG_ERROR( m_threadName, "init", "Setting P0 failed" );
					status = STATUS_DEVICE_ERROR;
				}
			}
			if (status != STATUS_OK)
			{
				LOG_ERROR( m_threadName, "init", "Failed to run prog0 for SH,Z,ZZ,ZZZ IPBs" );
				return status; /// EARLY RETURN!!	EARLY RETURN!!
			}

			if (status == STATUS_OK)
			{
				try {
					LOG_INFO( m_threadName, "init", "Setting IPBs for controller 1" );
					ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at(B_CONTROLLER);

					MotionAxis &SHaxis = g_motionServices_ptr->getAxis("SH");
					pController->setIPB(SHaxis.getChannelIndex(), (double)inPositionBandSH/_1MEG_F);

					MotionAxis &Zaxis = g_motionServices_ptr->getAxis("Z");
					pController->setIPB(Zaxis.getChannelIndex(), (double)inPositionBandZ/_1MEG_F);

					MotionAxis &ZZaxis = g_motionServices_ptr->getAxis("ZZ");
					pController->setIPB(ZZaxis.getChannelIndex(), (double)inPositionBandZZ/_1MEG_F);

					MotionAxis &ZZZaxis = g_motionServices_ptr->getAxis("ZZZ");
					pController->setIPB(ZZZaxis.getChannelIndex(), (double)inPositionBandZZZ/_1MEG_F);
				} catch (MotionException &x) {
					stringstream msg;
					msg << "EXCEPTION THROWN: " << x.getEventDesc() << "; ABORTING";
					LOG_FATAL( m_threadName, "init", msg.str().c_str() );
					status = STATUS_DEVICE_ERROR;
				} catch (std::exception &ex) {
					LOG_ERROR( m_threadName, "init", "Setting P0 failed" );
					status = STATUS_DEVICE_ERROR;
				}
			}
			if (status != STATUS_OK)
			{
				LOG_ERROR( m_threadName, "init", "Failed to set SH,Z,ZZ,ZZZ IPBs" );
				return status; /// EARLY RETURN!!	EARLY RETURN!!
			}
		}
		/// If the Z axis is not mocked, enable the Z axis and advance its state to "settings-confirmed".
		state = g_ZState.load( std::memory_order_acquire );
		if ((state == ComponentState::Uninitialized) && (status == STATUS_OK))
		{
			bool z_confirmed = false;
			status = Implement_EnableAxis( "Z", z_confirmed );
			if (z_confirmed) g_ZState.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
		}
		/// If the ZZ axis is not mocked, enable the ZZ axis and advance its state to "settings-confirmed".
		state = g_ZZState.load( std::memory_order_acquire );
		if ((state == ComponentState::Uninitialized) && (status == STATUS_OK))
		{
			bool zz_confirmed = false;
			status = Implement_EnableAxis( "ZZ", zz_confirmed );
			if (zz_confirmed) g_ZZState.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
		}
		/// If the ZZZ axis is not mocked, enable the ZZZ axis and advance its state to "settings-confirmed".
		state = g_ZZZState.load( std::memory_order_acquire );
		if ((state == ComponentState::Uninitialized) && (status == STATUS_OK))
		{
			bool zzz_confirmed = false;
			status = Implement_EnableAxis( "ZZZ", zzz_confirmed );
			if (zzz_confirmed) g_ZZZState.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
		}
		/// If the SH axis is not mocked, enable the SH axis and advance its state to "settings-confirmed".
		state = g_AnnularRingHolderState.load( std::memory_order_acquire );
		if ((state == ComponentState::Uninitialized) && (status == STATUS_OK))
		{
			bool sh_confirmed = false;
			status = Implement_EnableAxis( "SH", sh_confirmed );
			if (sh_confirmed) g_AnnularRingHolderState.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
		}

		///
		/// If any axis of the 'C' controller is not mocked, run the controller's setup routine.
		///
		if ((g_AspirationPump1State.load( std::memory_order_acquire ) == ComponentState::Uninitialized) ||
			(g_DispensePump1State.load( std::memory_order_acquire ) == ComponentState::Uninitialized) ||
			(g_DispensePump2State.load( std::memory_order_acquire ) == ComponentState::Uninitialized) ||
			(g_DispensePump3State.load( std::memory_order_acquire ) == ComponentState::Uninitialized))
		{
			/// Some component of controller C is going to be used, so call controller C's Setup routine.
			/// Unlike the others, this one does not take input parameters. But, maybe we should set ClrKillsTimeOut, for good measure.
			try {
				LOG_INFO( m_threadName, "init", "Running prog0 Setup for controller 1" );
				ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at(C_CONTROLLER);
				/// Send input parameters.
				pController->binarySetLong( CLR_KILLS_TIME_OUT_SN, DEFAULT_CLEAR_KILLS_TIMEOUT );
				usleep(10000);
				pController->runProgram( SETUP_ROUTINE, true );
				usleep(50000);
			} catch (MotionException &x) {
				stringstream msg;
				msg << "EXCEPTION THROWN: " << x.getEventDesc() << "; ABORTING";
				LOG_FATAL( m_threadName, "init", msg.str().c_str() );
				status = STATUS_DEVICE_ERROR;
			} catch (std::exception &ex) {
				LOG_ERROR( m_threadName, "init", "Setting P0 failed" );
				status = STATUS_DEVICE_ERROR;
			}
		}
		else
		{
			LOG_WARN( m_threadName, "init", "All pumps are mocked" );
		}
		/// If the AspirationPump1 axis is not mocked, enable the PN axis and advance its state to "settings-confirmed".
		state = g_AspirationPump1State.load( std::memory_order_acquire );
		if ((state == ComponentState::Uninitialized) && (status == STATUS_OK))
		{
			bool pn_confirmed = false;
			status = Implement_EnableAxis( "PN", pn_confirmed );
			g_AspirationPump1State.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
		}
		/// If the DispensePump1 axis is not mocked, enable the PP axis and advance its state to "settings-confirmed".
		state = g_DispensePump1State.load( std::memory_order_acquire );
		if ((state == ComponentState::Uninitialized) && (status == STATUS_OK))
		{
			bool pp_confirmed = false;
			status = Implement_EnableAxis( "PP", pp_confirmed );
			g_DispensePump1State.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
		}
		/// If the DispensePump2 axis is not mocked, enable the PPP axis and advance its state to "settings-confirmed".
		state = g_DispensePump2State.load( std::memory_order_acquire );
		if ((state == ComponentState::Uninitialized) && (status == STATUS_OK))
		{
			bool ppp_confirmed = false;
			status = Implement_EnableAxis( "PPP", ppp_confirmed );
			g_DispensePump2State.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
		}
		/// If the DispensePump3 axis is not mocked, enable the PPPP axis and advance its state to "settings-confirmed".
		state = g_DispensePump3State.load( std::memory_order_acquire );
		if ((state == ComponentState::Uninitialized) && (status == STATUS_OK))
		{
			bool pppp_confirmed = false;
			status = Implement_EnableAxis( "PPPP", pppp_confirmed );
			g_DispensePump3State.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
		}

		///
		/// The following components don't use PMM controllers.
		///
		// TODO: Consider breaking this up accross multiple threads.
		state = g_FluorescenceLightFilterState.load( std::memory_order_acquire );
		if (state == ComponentState::Uninitialized)	g_FluorescenceLightFilterState.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
		state = g_CameraState.load( std::memory_order_acquire );
		if (state == ComponentState::Uninitialized)	g_CameraState.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
		state = g_OpticalColumnState.load( std::memory_order_acquire );
		if (state == ComponentState::Uninitialized)	g_OpticalColumnState.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
		state = g_IncubatorState.load( std::memory_order_acquire );
		if (state == ComponentState::Uninitialized)	g_IncubatorState.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
		state = g_CxPMState.load( std::memory_order_acquire );
		if (state == ComponentState::Uninitialized)	g_CxPMState.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
		state = g_BarcodeReaderState.load( std::memory_order_acquire );
		if (state == ComponentState::Uninitialized)	g_BarcodeReaderState.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
	}
	return status;
}

int CommandDolerThread::DoleOut_InitSerialControllers()
{
	int status = STATUS_OK;
	m_doSendStatus = false;  /// No status message for set_hardware_state. It is a purely internal message.
	LOG_TRACE( m_threadName, "init", "handling INIT_SERIAL_CONTROLLER command" );
	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string( payloadObj );
		cout << payloadStr << endl;
	}
	else
	{
		LOG_WARN( m_threadName, "init", "Payload is missing" );
		status = STATUS_CMD_MSG_DATA_MISSING;
	}

	/// If the serial port wasn't initialized by main(), move the following components to error states.
	if (m_threadName == DOLER_FOR_PUMP)
	{
		/// Lock g_SerialPortMutex (in the PUMP thread) while merely testing the values of m_serialPort and m_tecanADP pointers
		unique_lock<mutex> lock( g_SerialPortMutex ); /// IMPORTANT! The Pump thread and CxD thread share this.
		/// Confirm that main() provided these objects.
		if (m_serialPort)
		{
			LOG_TRACE( m_threadName, "init", "advancing white-light state" );
			if (g_WhiteLightSourceState.load( std::memory_order_acquire ) == ComponentState::Uninitialized)
			{
				g_WhiteLightSourceState.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
			}

			if (m_tecanADP)
			{
				LOG_TRACE( m_threadName, "init", "advancing picking-pump state" );
				if (g_PickingPump1State.load( std::memory_order_acquire ) == ComponentState::Uninitialized)
				{
					g_PickingPump1State.store( ComponentState::SettingsConfirmed, std::memory_order_relaxed );
				}
			}
			else /// Serial Port exists but m_tecanADP does not
			{
				if (g_PickingPump1State.load( std::memory_order_acquire ) != ComponentState::Mocked)
				{
					LOG_ERROR( m_threadName, "init", "m_tecanADP NOT OK." );
					g_PickingPump1State.store( ComponentState::Faulty, std::memory_order_relaxed );
				}
			}
		}
		else
		{
			if (g_PickingPump1State.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				LOG_ERROR( m_threadName, "init", "Serial Port NOT OK." );
				g_PickingPump1State.store( ComponentState::Faulty, std::memory_order_relaxed );
			}
			if (g_WhiteLightSourceState.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				LOG_ERROR( m_threadName, "init", "Serial Port NOT OK." );
				g_WhiteLightSourceState.store( ComponentState::Faulty, std::memory_order_relaxed );
			}
		}
	}
	return status;
}


int CommandDolerThread::DoleOut_HomeSafeHardware()
{
	int status = STATUS_OK;
	// TODO: Fix the status variables.
	m_doSendStatus = false;  /// No status message for set_hardware_state. It is a purely internal message.
	LOG_TRACE( m_threadName, "init", "-");
	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );

	if (m_threadName == DOLER_FOR_CXD)
	{
		if (g_ZState.load( std::memory_order_acquire ) == ComponentState::SettingsConfirmed)
		{
			/// Home Z.
			int z_status = STATUS_OK;
			if (true)
			{
				/// Perform the homing of Z
				unique_lock<mutex> lock( g_controllerBMutex ); /// Prevent the ANNULAR_RING_HOLDER thread from homing at the same time.
				bool z_confirmed = false;
				if (Implement_SetHomeInputs( payloadObj, HOME_Z_INPUTS, B_CONTROLLER, "Z" ))
				{
					z_status = Implement_HomeAxis( B_CONTROLLER, HOME_Z_ROUTINE, "Z", z_confirmed );
					if ((z_status == STATUS_OK) && (z_confirmed))
					{
						m_states_long[Z_NM] = 0L;
						g_ZState.store( ComponentState::Homed1, std::memory_order_relaxed );
						LOG_TRACE( m_threadName, "init", "Homed Z axis" );
					}
					else
					{
						g_ZState.store( ComponentState::Faulty, std::memory_order_relaxed );
						/// HALT
						g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed );
						LOG_ERROR( m_threadName, "init", "Failed to enable Z axis" );
						// TODO: Send an alert.
					}
				}
				else
				{
					z_status = STATUS_INIT_FAILED;
					LOG_ERROR( m_threadName, "init", "Failed to set input values for Z axis" );
				}
				/// Release the mutex now. Implement_moveToZNm() will lock it again.
			}
			/// Move Z to safe height
			long z_safeHeight_nm = 0L;
			int succeeded = m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_SAFE_HEIGHT, z_safeHeight_nm );
			if ((z_status == STATUS_OK) && succeeded)
			{
				LOG_TRACE( m_threadName, "temp", "Moving Z to safety" );
				z_status = Implement_moveToZNm( z_safeHeight_nm );
				if (z_status != STATUS_OK)
				{
					g_ZState.store( ComponentState::Faulty, std::memory_order_relaxed );
					LOG_ERROR( m_threadName, "init", "Failed to move Z to safe height" );
				}
			}
			status = z_status;
		}

		if (g_ZZState.load( std::memory_order_acquire ) == ComponentState::SettingsConfirmed)
		{
			/// Home ZZ.
			int zz_status = STATUS_OK;
			if (true)
			{
				/// Perform the homing of ZZ
				unique_lock<mutex> lock( g_controllerBMutex ); /// Prevent the ANNULAR_RING_HOLDER thread from homing at the same time.
				bool zz_confirmed = false;
				if (Implement_SetHomeInputs( payloadObj, HOME_ZZ_INPUTS, B_CONTROLLER, "ZZ" ))
				{
					zz_status = Implement_HomeAxis( B_CONTROLLER, HOME_ZZ_ROUTINE, "ZZ", zz_confirmed );
					if ((zz_status == STATUS_OK) && (zz_confirmed))
					{
						m_states_long[ZZ_NM] = 0L;
						g_ZZState.store( ComponentState::Homed1, std::memory_order_relaxed );
						LOG_TRACE( m_threadName, "init", "Homed ZZ axis" );
					}
					else
					{
						g_ZZState.store( ComponentState::Faulty, std::memory_order_relaxed );
						/// HALT
						g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed );
						LOG_ERROR( m_threadName, "init", "Failed to enable ZZ axis" );
						// TODO: Send an alert.
					}
				}
				else
				{
					zz_status = STATUS_INIT_FAILED;
					LOG_ERROR( m_threadName, "init", "Failed to set input values for ZZ axis" );
				}
				/// Release the mutex now. Implement_moveToZZNm() will lock it again.
			}
			/// Move ZZ to safe height
			long zz_safeHeight_nm = 0L;
			int succeeded = m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZ_SAFE_HEIGHT, zz_safeHeight_nm );
			if ((zz_status == STATUS_OK) && succeeded)
			{
				LOG_TRACE( m_threadName, "temp", "Moving ZZ to safety" );
				int zz_status = Implement_moveToZZNm( zz_safeHeight_nm );
				if (zz_status != STATUS_OK)
				{
					g_ZZState.store( ComponentState::Faulty, std::memory_order_relaxed );
					LOG_ERROR( m_threadName, "init", "Failed to move ZZ to safe height" );
				}
			}
			if (status == STATUS_OK)
			{
				status = zz_status;
			}
		}

		if (g_ZZZState.load( std::memory_order_acquire ) == ComponentState::SettingsConfirmed)
		{
			/// Home ZZZ.
			int zzz_status = STATUS_OK;
			if (true)
			{
				/// Perform the homing of ZZZ
				unique_lock<mutex> lock( g_controllerBMutex ); /// Prevent the ANNULAR_RING_HOLDER thread from homing at the same time.
				bool zzz_confirmed = false;
				if (Implement_SetHomeInputs( payloadObj, HOME_ZZZ_INPUTS, B_CONTROLLER, "ZZZ" ))
				{
					zzz_status = Implement_HomeAxis( B_CONTROLLER, HOME_ZZZ_ROUTINE, "ZZZ", zzz_confirmed );
					if ((zzz_status == STATUS_OK) && (zzz_confirmed))
					{
						m_states_long[ZZZ_NM] = 0L;
						g_ZZZState.store( ComponentState::Homed1, std::memory_order_relaxed );
						LOG_TRACE( m_threadName, "init", "Homed ZZZ axis" );
					}
					else
					{
						g_ZZZState.store( ComponentState::Faulty, std::memory_order_relaxed );
						/// HALT
						g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed );
						LOG_ERROR( m_threadName, "init", "Failed to enable ZZZ axis" );
						// TODO: Send an alert.
					}
				}
				else
				{
					zzz_status = STATUS_INIT_FAILED;
					LOG_ERROR( m_threadName, "init", "Failed to set input values for ZZZ axis" );
				}
				/// Release the mutex now. Implement_moveToZZZNm() will lock it again.
			}
			/// Move ZZZ to safe height
			long zzz_safeHeight_nm = 0L;
			int succeeded = m_desHelper->ExtractLongValue( m_threadName, payloadObj, ZZZ_SAFE_HEIGHT, zzz_safeHeight_nm );
			if ((zzz_status == STATUS_OK) && succeeded)
			{
				LOG_TRACE( m_threadName, "temp", "Moving ZZZ to safety" );
				zzz_status = Implement_moveToZZZNm( zzz_safeHeight_nm );
				if (zzz_status != STATUS_OK)
				{
					g_ZZZState.store( ComponentState::Faulty, std::memory_order_relaxed );
					LOG_ERROR( m_threadName, "init", "Failed to move ZZZ to safe height" );
				}
			}
			if (status == STATUS_OK)
			{
				status = zzz_status;
			}
		}
	}
	if (m_threadName == DOLER_FOR_CXPM)
	{
		int cxpm_status = STATUS_OK;
		// TODO: Reconsider which thread should handle the barcode reader.
		ComponentState state = g_BarcodeReaderState.load( std::memory_order_acquire );
		if (state == ComponentState::SettingsConfirmed)	g_BarcodeReaderState.store( ComponentState::Homed1, std::memory_order_relaxed );
		status = cxpm_status;
	}
	if (m_threadName == DOLER_FOR_PUMP)
	{
		if (g_AspirationPump1State.load( std::memory_order_acquire ) == ComponentState::SettingsConfirmed)
		{
			/// Home PN.
			int p_status = STATUS_OK;
			if (true)
			{
				/// No need to set inputs for HomeP
				LOG_TRACE( m_threadName, "init", "calling HomePN" );
				p_status = Implement_HomeAxis( C_CONTROLLER, HOME_PN_ROUTINE, "PN" );
				if (p_status == STATUS_OK)
				{
					// TODO: Find out if there is a state associated with PN
					// m_states_long[TBD] = DBD;
					g_AspirationPump1State.store( ComponentState::Homed1, std::memory_order_relaxed );
					LOG_TRACE( m_threadName, "init", "Homed PN axis" );
				}
				else
				{
					g_AspirationPump1State.store( ComponentState::Faulty, std::memory_order_relaxed );
					/// HALT
					g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed );
					LOG_ERROR( m_threadName, "init", "Failed to enable PN axis" );
					// TODO: Send an alert.
				}
			}
			if (status == STATUS_OK)
			{
				status = p_status;
			}
		}
		if (g_DispensePump1State.load( std::memory_order_acquire ) == ComponentState::SettingsConfirmed)
		{
			/// Home PP.
			int pp_status = STATUS_OK;
			if (true)
			{
				/// No need to set inputs for HomePP
				LOG_TRACE( m_threadName, "init", "calling HomePP" );
				pp_status = Implement_HomeAxis( C_CONTROLLER, HOME_PP_ROUTINE, "PP" );
				if (pp_status == STATUS_OK)
				{
					// TODO: Find out if there is a state associated with PP
					// m_states_long[TBD] = DBD;
					g_DispensePump1State.store( ComponentState::Homed1, std::memory_order_relaxed );
					LOG_TRACE( m_threadName, "init", "Homed PP axis" );
				}
				else
				{
					g_DispensePump1State.store( ComponentState::Faulty, std::memory_order_relaxed );
					/// HALT
					g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed );
					LOG_ERROR( m_threadName, "init", "Failed to enable PP axis" );
					// TODO: Send an alert.
				}
			}
			if (status == STATUS_OK)
			{
				status = pp_status;
			}
		}
		if (g_DispensePump2State.load( std::memory_order_acquire ) == ComponentState::SettingsConfirmed)
		{
			/// Home PPP.
			int ppp_status = STATUS_OK;
			if (true)
			{
				/// No need to set inputs for HomePPP
				ppp_status = Implement_HomeAxis( C_CONTROLLER, HOME_PPP_ROUTINE, "PPP" );
				if (ppp_status == STATUS_OK)
				{
					// TODO: Find out if there is a state associated with PPP
					// m_states_long[TBD] = DBD;
					g_DispensePump2State.store( ComponentState::Homed1, std::memory_order_relaxed );
					LOG_TRACE( m_threadName, "init", "Homed PPP axis" );
				}
				else
				{
					g_DispensePump2State.store( ComponentState::Faulty, std::memory_order_relaxed );
					/// HALT
					g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed );
					LOG_ERROR( m_threadName, "init", "Failed to enable PPP axis" );
					// TODO: Send an alert.
				}
			}
			if (status == STATUS_OK)
			{
				status = ppp_status;
			}
		}
		if (g_DispensePump3State.load( std::memory_order_acquire ) == ComponentState::SettingsConfirmed)
		{
			/// Home PPPP.
			int pppp_status = STATUS_OK;
			if (true)
			{
				/// No need to set inputs for HomePPPP
				pppp_status = Implement_HomeAxis( C_CONTROLLER, HOME_PPPP_ROUTINE, "PPPP" );
				if (pppp_status == STATUS_OK)
				{
					// TODO: Find out if there is a state associated with PPPP
					// m_states_long[TBD] = DBD;
					g_DispensePump3State.store( ComponentState::Homed1, std::memory_order_relaxed );
					LOG_TRACE( m_threadName, "init", "Homed PPPP axis" );
				}
				else
				{
					g_DispensePump3State.store( ComponentState::Faulty, std::memory_order_relaxed );
					/// HALT
					g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed );
					LOG_ERROR( m_threadName, "init", "Failed to enable PPPP axis" );
					// TODO: Send an alert.
				}
			}
			if (status == STATUS_OK)
			{
				status = pppp_status;
			}
		}

		if (g_PickingPump1State.load( std::memory_order_acquire ) == ComponentState::SettingsConfirmed)
		{
			json_object * pickPump1Obj = json_object_object_get( payloadObj, HOME_PICK_PUMP1_INPUTS );
			/// Initialize Picking Pump.
			int pick_status = STATUS_OK;
			if (pickPump1Obj)
			{
				string vendor = "";
				string model = "";
				if (! m_desHelper->ExtractStringValue( m_threadName, pickPump1Obj, PUMP_VENDOR, vendor ))
				{
					LOG_ERROR( m_threadName, "pick", "Vendor is missing" );
					pick_status = STATUS_CMD_MSG_DATA_MISSING;
				}
				if (! m_desHelper->ExtractStringValue( m_threadName, pickPump1Obj, PUMP_MODEL, model ))
				{
					LOG_ERROR( m_threadName, "pick", "Model is missing" );
					pick_status = STATUS_CMD_MSG_DATA_MISSING;
				}

				if (DesHelper::IsTecanAdpDetect( vendor, model ))
				{
					/// Lock g_SerialPortMutex (in the PUMP thread) while initializing the ADP.
					unique_lock<mutex> lock( g_SerialPortMutex ); /// IMPORTANT! The Pump thread and CxD thread share this.
					/// Query a Tecan ADP.
					int stdError = 0;
					int count = 0;

					/// Query the ADP for it pump maximum volume
					if (pick_status == STATUS_OK)
					{
						string maximumVolumeString = "";
						int count = 0;
						do {
							pick_status = m_tecanADP->GetPumpMaximumVolume( m_threadName, maximumVolumeString );
							count++;
							if (pick_status == STATUS_OK) count = 5;
						} while (count < 5);

						if (pick_status == STATUS_OK)
						{
							float maxVol = stof(maximumVolumeString);
							if (maxVol != m_states_float[PICKING_PUMP_MAXIMUM_VOLUME])
							{
								stringstream msg;
								msg << "Changing picking pump maximum volume from " << m_states_float[PICKING_PUMP_MAXIMUM_VOLUME];
								msg << " to " << maxVol;
								LOG_TRACE( m_threadName, "pick", msg.str().c_str() );
								m_states_float[PICKING_PUMP_MAXIMUM_VOLUME] = maxVol;
								g_PickingPumpMaximumVolume.store( maxVol, std::memory_order_relaxed ); /// for users outside the FOR-PUMP thread.
							}
						}
						usleep( m_tecanADP->StandardWait_uS );
					}

					/// Query the ADP for it pump current volume
					if (pick_status == STATUS_OK)
					{
						string currentVolumeString = "";
						int count = 0;
						do {
							pick_status = m_tecanADP->GetPumpCurrentVolume( m_threadName, currentVolumeString );
							count++;
							if (pick_status == STATUS_OK) count = 5;
						} while (count < 5);
						stringstream msg;
						msg << "Measured picking pump current volume is " << currentVolumeString;
						LOG_TRACE( m_threadName, "pick", msg.str().c_str() );

						if (pick_status == STATUS_OK)
						{
							float currentVol = stof(currentVolumeString);
							if (currentVol != m_states_float[PICKING_PUMP_CURRENT_VOLUME])
							{
								stringstream msg;
								msg << "Changing picking pump current volume from " << m_states_float[PICKING_PUMP_CURRENT_VOLUME];
								msg << " to " << currentVol;
								LOG_TRACE( m_threadName, "pick", msg.str().c_str() );
								m_states_float[PICKING_PUMP_CURRENT_VOLUME] = currentVol;
								g_PickingPumpVolume.store( currentVol, std::memory_order_relaxed ); /// for users outside the FOR-PUMP thread.
							}
						}
						usleep( m_tecanADP->StandardWait_uS );
					}
					
					/// Update the picking pump state according to our results.
					if (pick_status == STATUS_OK)
					{
						LOG_TRACE( m_threadName, "pick", "Homed1" );
						g_PickingPump1State.store( ComponentState::Homed1, std::memory_order_relaxed );
					}
					else
					{
						LOG_TRACE( m_threadName, "pick", "Faulty" );
						g_PickingPump1State.store( ComponentState::Faulty, std::memory_order_relaxed );
					}
				}
				else /// The picking pump is not a Tecan ADP
				{
					stringstream msg;
					msg << "Unknown picking pump type! vendor=" << vendor << "|model=" << model;
					LOG_ERROR( m_threadName, "pick", msg.str().c_str() );
					LOG_TRACE( m_threadName, "pick", "Faulty" );
					g_PickingPump1State.store( ComponentState::Faulty, std::memory_order_relaxed );
				}
			}
			if (status == STATUS_OK)
			{
				status = pick_status;
			}
		}
	}
	if (m_threadName == DOLER_FOR_RING_HOLDER)
	{
		LOG_TRACE( m_threadName, "init", "Homed SH/ANR axis start homing" );
		if (g_AnnularRingHolderState.load( std::memory_order_acquire ) == ComponentState::SettingsConfirmed)
		{
			/// Home SH.
			int sh_status = STATUS_OK;
			if (true)
			{
				unique_lock<mutex> lock( g_controllerBMutex ); /// Prevent the CxD thread from homing Z, ZZ, or ZZZ axis at the same time.
				bool sh_confirmed = false;
				if (Implement_SetHomeInputs( payloadObj, HOME_SH_INPUTS, B_CONTROLLER, "SH" ))
				{
					sh_status = Implement_HomeAxis( B_CONTROLLER, HOME_SH_ROUTINE, "SH", sh_confirmed );
					if ((sh_status == STATUS_OK) && (sh_confirmed))
					{
						m_states_long[ANNULAR_RING_POSITION] = 0L;
						g_AnnularRingHolderState.store( ComponentState::Homed1, std::memory_order_relaxed );
						LOG_TRACE( m_threadName, "init", "Homed SH/ANR axis" );
					}
					else
					{
						g_AnnularRingHolderState.store( ComponentState::Faulty, std::memory_order_relaxed );
						/// HALT
						g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed );
						LOG_ERROR( m_threadName, "init", "Failed to enable SH/ANR axis" );
						// TODO: Send an alert.
					}
				}
				else
				{
					sh_status = STATUS_INIT_FAILED;
					LOG_ERROR( m_threadName, "init", "Failed to set input values for SH axis (ANR)" );
				}
			}
			if (status == STATUS_OK)
			{
				status = sh_status;
			}
		}
		else
		{
			if (g_AnnularRingHolderState.load( std::memory_order_acquire ) == ComponentState::Mocked)
			{
				LOG_TRACE( m_threadName, "init", "Mocking SH/ANR axis" );
			}
			else
			{
				LOG_ERROR( m_threadName, "init", "ANR is not 'SettingsConfirmed'" );
			}
		}
		LOG_TRACE( m_threadName, "init", "Homed SH/ANR axis end homing" );
	}
	if (m_threadName == DOLER_FOR_OPTICS)
	{
		ComponentState state = g_WhiteLightSourceState.load( std::memory_order_acquire );
		if (state == ComponentState::SettingsConfirmed)	g_WhiteLightSourceState.store( ComponentState::Homed1, std::memory_order_relaxed );
		state = g_FluorescenceLightFilterState.load( std::memory_order_acquire );
		if (state == ComponentState::SettingsConfirmed)	g_FluorescenceLightFilterState.store( ComponentState::Homed1, std::memory_order_relaxed );
		state = g_CameraState.load( std::memory_order_acquire );
		// TODO: Try moving camera init code to here.
		if (state == ComponentState::SettingsConfirmed)
		{
			LOG_DEBUG( m_threadName, "init", "initializing camera(s)");
			string camera_init_msg = "";
			InitPVCamComms( camera_init_msg );
			if (camera_init_msg != "")
			{
				cout << m_threadName << camera_init_msg << endl;
			}
			/// Detect whether init was successful.
			if (g_CameraList.size() > 0)
			{
				g_CameraState.store( ComponentState::Homed1, std::memory_order_relaxed );
				LOG_DEBUG( m_threadName, "init", "found a camera");
			}
			else
			{
				g_CameraState.store( ComponentState::Faulty, std::memory_order_relaxed );
				LOG_ERROR( m_threadName, "init", "Could not initialize camera");
			}
		}
		state = g_OpticalColumnState.load( std::memory_order_acquire );
		if (state == ComponentState::SettingsConfirmed)
		{
			LOG_DEBUG( m_threadName, "init", "Homing focus");
			int focal_status = Implement_HomeFocus();
			if (focal_status == STATUS_OK)
			{
				g_OpticalColumnState.store( ComponentState::Homed1, std::memory_order_relaxed );
				LOG_DEBUG( m_threadName, "init", "Homed focus");
			}
			else
			{
				g_OpticalColumnState.store( ComponentState::Faulty, std::memory_order_relaxed );
				LOG_ERROR( m_threadName, "init", "Failed to home focus");
			}
			if (status == STATUS_OK)
			{
				status = focal_status;
			}
		}
	}
	return status;
}

bool CommandDolerThread::Implement_disableAxis( const char * axisName )
{
	bool succeeded = true;
	stringstream context;
	context << "disable-" << axisName;
	try {
		MotionAxis & axis = g_motionServices_ptr->getAxis( axisName );

		try {
			stringstream msg;
			msg << "Disabling axis " << axisName;
			LOG_TRACE( m_threadName, context.str().c_str(), msg.str().c_str());
			axis.disable();
		} catch (std::exception &e) {
			stringstream msg;
			msg << "Couldn't disable axis " << axisName << ": " << std::string(e.what());
			LOG_FATAL( m_threadName, context.str().c_str(), msg.str().c_str() );
			succeeded = false;
		} catch (std::string &s) {
			stringstream msg;
			msg << "Couldn't disable axis " << axisName << ": " << s;
			LOG_FATAL( m_threadName, context.str().c_str(), msg.str().c_str() );
			succeeded = false;
		} catch (std::string *s) {
			stringstream msg;
			msg << "Couldn't disable axis " << axisName << ": " << *s;
			LOG_FATAL( m_threadName, context.str().c_str(), msg.str().c_str() );
			succeeded = false;
		} catch (...) {
			stringstream msg;
			msg << "Couldn't disable axis " << axisName << ": unknown exception";
			LOG_FATAL( m_threadName, context.str().c_str(), msg.str().c_str() );
			succeeded = false;
		}
	} catch( NoSuchObjectException &x ) {
		stringstream msg;
		msg << x.getTypeDesc() << "|" << x.getEventDesc();
		LOG_WARN( m_threadName, context.str().c_str(), msg.str().c_str() );
		/// Assume this is a benign case--that the axis wasn't found because
		/// the configuration file intentionally lacked it.
		succeeded = false;
	} catch( ... ) {
		succeeded = false;
	}
	stringstream msg;
	LOG_TRACE( m_threadName, context.str().c_str(), "SUCCESS" );
	return succeeded;
}

/// Sets ACC, DEC, STP, and VEL of the controller managing axisName0, then
/// starts the first and second axes moving to their new positions,
/// then waits for the motion to start,
/// then waits for the motion to end.
int CommandDolerThread::Implement_MoveTwoAxesConcurrently( std::string axisName0Str, long & position0_nm, long axisResolution0_nm,
														   std::string axisName1Str, long & position1_nm, long axisResolution1_nm,
														   long & speed_nmPerSec, long & acceleration_nmPerSecPerSec, long & deceleration_nmPerSecPerSec, long & stp_nmPerSecPerSec )
{
	int status = DoleOut_ConfirmNoKillAllMotionRequests( A_CONTROLLER );
	if (status != STATUS_OK)
	{
		return status;	/// EARLY RETURN!!	EARLY RETURN!!
	}
	const char * axisName0 = axisName0Str.c_str();
	const char * axisName1 = axisName1Str.c_str();
	double goalPosition0_mm = (float)position0_nm / _1MEG_F;
	string goalPosition0_mm_str = "";
	double goalPosition1_mm = (float)position1_nm / _1MEG_F;
	string goalPosition1_mm_str = "";
	MotionAxis &axis0 = g_motionServices_ptr->getAxis(axisName0Str);
	MotionAxis &axis1 = g_motionServices_ptr->getAxis(axisName1Str);
	string twoAxis = axisName0Str + axisName1Str;
	if (true)
	{
		stringstream msg;
		msg << "Names of axes: " << axis0.getName() << axis0.getChannelIndex() << "&" << axis1.getName() << axis1.getChannelIndex() << "|-|";
		msg << "axis0res=" << axisResolution0_nm << "|axis1res=" << axisResolution1_nm;
		LOG_DEBUG( m_threadName, twoAxis.c_str(), msg.str().c_str() );

		ostringstream goalPos0;
		goalPos0.precision(6);
		goalPos0 << std::fixed << goalPosition0_mm;
		goalPosition0_mm_str = goalPos0.str();
		ostringstream goalPos1;
		goalPos1.precision(6);
		goalPos1 << std::fixed << goalPosition1_mm;
		goalPosition1_mm_str = goalPos1.str();
	}

	/// Sanity-check the numbers we'll be working with.
	if (isnan( position0_nm ) || isnan( position1_nm ))
	{
		stringstream msg;
		msg << "One of these values is NAN: " << position0_nm << "|" << position1_nm;
		LOG_ERROR( m_threadName, twoAxis.c_str(), msg.str().c_str() );
		return STATUS_MATH_ERROR;
	}

	/// Prepare some state variables.
	bool isMoving = false;
	bool hasVelocity = false;
	bool isFarFromGoal = false;
	/// PPU is pulses per programming unit. in usteps/mm
	long goalPosition0_usteps = (long)(goalPosition0_mm * axis0.getCachedPPU());
	long goalPosition1_usteps = (long)(goalPosition1_mm * axis1.getCachedPPU());
	long axisResolution0_usteps = (axisResolution0_nm * axis0.getCachedPPU()) / _1MEG_L;
	long axisResolution1_usteps = (axisResolution1_nm * axis1.getCachedPPU()) / _1MEG_L;
	long position0_usteps = 0L;
	long position1_usteps = 0L;
	float position0_mm = 0.0;
	string position0_mm_str = "";
	float position1_mm = 0.0;
	string position1_mm_str = "";
	float velocity0 = 0.0;
	float velocity1 = 0.0;

	///
	/// Detect current position to see if motion is needed.
	///
	try {
		position0_usteps = (long)(axis0.getActualPosition());
		position1_usteps = (long)(axis1.getActualPosition());
		if ((Abs( position0_usteps - goalPosition0_usteps ) <=  axisResolution0_usteps) &&
			(Abs( position1_usteps - goalPosition1_usteps ) <=  axisResolution1_usteps))
		{
			isFarFromGoal = false;
		}
		else
		{
			isFarFromGoal = true;
		}
	} catch (PValuePendingException &ex) {
		/// PValue data requested, but not yet received
		LOG_INFO( m_threadName, twoAxis.c_str(), "position: STILL PENDING" );
		status = STATUS_MOVEMENT_FAILED;
	}
	if (!isFarFromGoal)
	{
		/// No motion is needed. Say we succeeded.
		LOG_WARN( m_threadName, twoAxis.c_str(), "Already at desired position." );
		return status;		// EARLY RETURN!	EARLY RETURN!
	}

	///
	/// Send the command that starts motion.
	///
	ACR7000 *pController = nullptr;
	try {
		pController = (ACR7000*)g_motionServices_ptr->getMotionControllerOfAxis( axisName0Str );
		/// Convert values to millimeters.
		size_t bufferSize = pController->setVelAccelDecelStop( (float)speed_nmPerSec/_1MEG_F, (float)acceleration_nmPerSecPerSec/_1MEG_F, (float)deceleration_nmPerSecPerSec/_1MEG_F, (float)stp_nmPerSecPerSec/_1MEG_F );
		stringstream msg;
		msg << "ASCII-buffer-size=" << bufferSize;
		LOG_TRACE( m_threadName, twoAxis.c_str(), msg.str().c_str() );
		usleep(500000);
		pController->concurrentAbsoluteMove(axis0.getName(), goalPosition0_mm, axis1.getName(), goalPosition1_mm);
		usleep(500000);
    } catch (...) {
		status = STATUS_MOVEMENT_FAILED;
		LOG_ERROR( m_threadName, twoAxis.c_str(), "unknown EXCEPTION!" );
		return status;	//	EARLY RETURN!	EARLY RETURN!
	}

	// Temporary debug.
	{
		stringstream msg;
		msg << axisName0 << "_PPU=" << axis0.getCachedPPU() << " " << axisName1 << "_PPU=" << axis1.getCachedPPU();
		LOG_DEBUG( m_threadName, twoAxis.c_str(), msg.str().c_str() );
	}

	///
	/// Wait for motion to start
	///
	long masterFlags = 0L; /// Assume that both axes are on the same master, otherwise a concurrent move would be impossible in the first place.
	string masterFlagsStr = "000000";
	bool awaitingStartOfMove = true;
	int loopCount = 0;
	int strikeCount = 0;
	while (awaitingStartOfMove)
	{
		try {
			masterFlags = axis0.getMasterFlags();
			isMoving = ((masterFlags & MASK_IN_MOTION) == MASK_IN_MOTION);
			hasVelocity = isMoving;
			if (isMoving)
			{
				awaitingStartOfMove = false;
			}
		}  catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, twoAxis.c_str(), "masterFlags: STILL PENDING" );
		}
		try {
			position0_usteps = (long)(axis0.getActualPosition());
			position1_usteps = (long)(axis1.getActualPosition());
			if ((Abs( position0_usteps - goalPosition0_usteps ) <=  axisResolution0_usteps) &&
				(Abs( position1_usteps - goalPosition1_usteps ) <=  axisResolution1_usteps))
			{
				/// Is at goal, so movement came and went without notice.
				awaitingStartOfMove = false;
				isFarFromGoal = false;
			}
		} catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, twoAxis.c_str(), "position: STILL PENDING" );
			status = STATUS_MOVEMENT_FAILED;
		}
		if (loopCount++ > MAX_LOOPS_AWAITING_MOTION_START)
		{
			status = STATUS_MOVEMENT_FAILED;
			LOG_ERROR( m_threadName, twoAxis.c_str(), "timed out waiting for motion to start" );
			return status;	//	EARLY RETURN!	EARLY RETURN!
		}
		usleep(10000);
	}
	LOG_TRACE( m_threadName, twoAxis.c_str(), "Motion detected" );

	loopCount = 0;
	strikeCount = 0;
	long axisFlags0 = 0L;
	long axisFlags1 = 0L;
	string axisFlags0Str = "";
	string axisFlags1Str = "";
	bool notInPositionBand0 = true;
	bool notInPositionBand1 = true;

	///
	/// Wait for motion to stop
	///
	while (hasVelocity || isMoving || notInPositionBand0 || notInPositionBand1) {
		try {
			float pos_usteps = axis0.getActualPosition();
			position0_usteps = (long)pos_usteps;
			position0_mm = pos_usteps / axis0.getCachedPPU();
			ostringstream pos0;
			pos0.precision(6);
			pos0 << std::fixed << position0_mm;
			position0_mm_str = pos0.str();
			pos_usteps = axis1.getActualPosition();
			position1_usteps = (long)pos_usteps;
			position1_mm = pos_usteps / axis1.getCachedPPU();
			ostringstream pos1;
			pos1.precision(6);
			pos1 << std::fixed << position1_mm;
			position1_mm_str = pos1.str();
			isFarFromGoal = ((Abs( position0_usteps - goalPosition0_usteps ) > axisResolution0_usteps) ||
							 (Abs( position1_usteps - goalPosition1_usteps ) >  axisResolution1_usteps));
		} catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, twoAxis.c_str(), "position: STILL PENDING" );
		}
		try {
			velocity0 = axis0.getCurrentVelocity();
			velocity1 = axis1.getCurrentVelocity();
			hasVelocity = ((Abs(velocity0) > MAX_ALLOWABLE_VELOCITY_ERROR) ||
						   (Abs(velocity1) > MAX_ALLOWABLE_VELOCITY_ERROR));
		} catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, twoAxis.c_str(), "velocity: STILL PENDING" );
		}
		try {
			masterFlags = axis0.getMasterFlags();
			isMoving = ((masterFlags & MASK_IN_MOTION) == MASK_IN_MOTION);
			std::stringstream stream;
			stream << std::hex << masterFlags;
			masterFlagsStr = stream.str();
		}  catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, twoAxis.c_str(), "masterFlags: STILL PENDING" );
		}
		try {
			axisFlags0 = axis0.getAxisFlags();
			notInPositionBand0 = ((axisFlags0 & MASK_IPB) == MASK_IPB);
			std::stringstream stream0;
			stream0 << std::hex << axisFlags0;
			axisFlags0Str = stream0.str();
			
			axisFlags1 = axis1.getAxisFlags();
			notInPositionBand1 = ((axisFlags1 & MASK_IPB) == MASK_IPB);
			std::stringstream stream1;
			stream1 << std::hex << axisFlags1;
			axisFlags1Str = stream1.str();
		}  catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, twoAxis.c_str(), "axisFlags: STILL PENDING" );
		}
		
		if ((!hasVelocity) && (!isMoving) && (notInPositionBand0 || notInPositionBand1))
		{
			/// Typically, we get here when a movement has completed but the notInPositionBand flags
			/// don't reflect that yet.
			if (strikeCount++ % 10 == 0)
			{
				stringstream msg;
				msg << "delta0=" <<  Abs( position0_usteps - goalPosition0_usteps );
				msg << "|max0=" << axisResolution0_usteps;
				msg << "|delta1=" << Abs( position1_usteps - goalPosition1_usteps );
				msg << "|max1=" << axisResolution1_usteps;
				msg << "|" << axisName0 << "_pos(mm)=" << position0_mm_str << "|" << axisName1 << "_pos(mm)=" << position1_mm_str;
//				msg << "|axisFlags0=" << axisFlags0Str; /// Tempoarary debug for Jim Wiley. Remove in future.
//				msg << "|axisFlags1=" << axisFlags1Str; /// Tempoarary debug for Jim Wiley. Remove in future.
				if (notInPositionBand0)
				{
					msg << "|notInPositionBand0=TRUE";
				}
				else
				{
					msg << "|notInPositionBand0=FALSE";
				}
				if (notInPositionBand1)
				{
					msg << "|notInPositionBand1=TRUE";
				}
				else
				{
					msg << "|notInPositionBand1=FALSE";
				}
				if (isMoving)
				{
					msg << "|isMoving=TRUE";
				}
				else
				{
					msg << "|isMoving=FALSE";
				}
				if (hasVelocity)
				{
					msg << "|hasVelocity=TRUE";
				}
				else
				{
					msg << "|hasVelocity=FALSE";
				}
				LOG_WARN( m_threadName, twoAxis.c_str(), msg.str().c_str() );
			}
			if (strikeCount >= MAX_STRIKES_TWO_AXES)
			{
				try {
					pController->printActualPositionViaAscii( 0 );
					usleep(10000);
					pController->printActualPositionViaAscii( 1 );
					usleep(10000);
					notInPositionBand0 = pController->getAxisFlag( BIT_IPB, 0 );
					usleep(10000);
					notInPositionBand1 = pController->getAxisFlag( BIT_IPB, 1 );
					usleep(10000);
				} catch (...) {
					LOG_ERROR( m_threadName, twoAxis.c_str(), "unknown EXCEPTION!" );
				}
				/// Movement somehow stopped far from goal.
				stringstream msg0;
				msg0 << axisName0 << ": masterFlags=" << masterFlagsStr << "|axisFlags=" << axisFlags0Str;
				msg0 << "|vel=" << velocity0 << "|" << axisName0 << "_pos(mm)=" << position0_mm_str << "|goal(mm)=" << goalPosition0_mm_str;
				msg0 << "|axisResolution(usteps)=" << axisResolution0_usteps;
				LOG_INFO( m_threadName, twoAxis.c_str(), msg0.str().c_str() );
				stringstream msg1;
				msg1 << axisName1 << ": masterFlags=" << masterFlagsStr << "|axisFlags=" << axisFlags1Str;
				msg1 << "|vel=" << velocity1 << "|" << axisName1 << "_pos(mm)=" << position1_mm_str << "|goal(mm)=" << goalPosition1_mm_str;
				msg1 << "|axisResolution(usteps)=" << axisResolution1_usteps;
				LOG_INFO( m_threadName, twoAxis.c_str(), msg1.str().c_str() );

				/// Expect the caller to send an alert when it sees this status.
				status = STATUS_MOVEMENT_FAILED;
				LOG_ERROR( m_threadName, twoAxis.c_str(), "stopped before reaching goal, according to IPB" );
				if ((position0_mm != 0.0) || (goalPosition0_mm == 0.0))
				{
					position0_nm = ((position0_usteps * _1MEG_L) / (long)axis0.getCachedPPU());
				}
				if ((position1_mm != 0.0) || (goalPosition1_mm == 0.0))
				{
					position1_nm = ((position1_usteps * _1MEG_L) / (long)axis1.getCachedPPU());
				}
				return status;	//	EARLY RETURN!	EARLY RETURN!
			}
		}
		else if (loopCount++ % 10 == 0)
		{
			/// Movement is on course.
			stringstream msg0;
			msg0 << axisName0 << ": masterFlags=" << masterFlagsStr << "|axisFlags=" << axisFlags0Str;
			msg0 << "|vel=" << velocity0 << "|" << axisName0 << "_pos(mm)=" << position0_mm_str << "|goalPos=" << goalPosition0_mm_str;
			LOG_TRACE( m_threadName, twoAxis.c_str(), msg0.str().c_str() );
			stringstream msg1;
			msg1 << axisName1 << ": masterFlags=" << masterFlagsStr << "|axisFlags=" << axisFlags1Str;
			msg1<< "|vel=" << velocity1 << "|" << axisName1 << "_pos(mm)=" << position1_mm_str << "|goalPos=" << goalPosition1_mm_str;
			LOG_TRACE( m_threadName, twoAxis.c_str(), msg1.str().c_str() );
		}
		if (loopCount > MAX_LOOPS_AWAITING_MOTION_END)
		{
			status = STATUS_MOVEMENT_FAILED;
			LOG_ERROR( m_threadName, twoAxis.c_str(), "timed out waiting for motion to end" );
			return status;	//	EARLY RETURN!	EARLY RETURN!
		}
		usleep(10000);
	}
	if (true)
	{
		stringstream msg0;
		msg0 << axisName0 << ": masterFlags=" << masterFlagsStr << "|axisFlags=" << axisFlags0Str;
		msg0 << "|vel=" << velocity0 << "|" << axisName0 << "_pos(mm)=" << position0_mm_str << "|goal(mm)=" << goalPosition0_mm_str;
		msg0 << "|axisResolution(μsteps)=" << axisResolution0_usteps;
		LOG_INFO( m_threadName, twoAxis.c_str(), msg0.str().c_str() );
		stringstream msg1;
		msg1 << axisName1 << ": masterFlags=" << masterFlagsStr << "|axisFlags=" << axisFlags1Str;
		msg1 << "|vel=" << velocity1 << "|" << axisName1 << "_pos(mm)=" << position1_mm_str << "|goal(mm)=" << goalPosition1_mm_str;
		msg1 << "|axisResolution(μsteps)=" << axisResolution1_usteps;
		LOG_INFO( m_threadName, twoAxis.c_str(), msg1.str().c_str() );
	}

	/// Movement stopped at goal.
	position0_nm = ((position0_usteps * _1MEG_L) / (long)axis0.getCachedPPU());
	position1_nm = ((position1_usteps * _1MEG_L) / (long)axis1.getCachedPPU());

	return status;
}

string float2str6( float num )
{
	ostringstream ostrm;
	ostrm.precision(6);
	ostrm << std::fixed << num;
	return ostrm.str();
}

string long2strHx( long num )
{
	ostringstream ostrm;
	ostrm << std::setfill('0') << std::setw(8) << std::hex << num;
	return ostrm.str();
}

string bool2str( bool b )
{
	string s = b ? "TRUE" : "FALSE";
	return s;
}

///----------------------------------------------------------------------
/// Implement_MoveOneAxis
/// Moves a single axis to position_nm, using the supplied accelerations.
/// The actual, measured, new position is returned as position_nm.
///----------------------------------------------------------------------
int CommandDolerThread::Implement_MoveOneAxis( std::string axisName, long & position_nm, long axisResolution_nm,
											   long & speed_nmPerSec, long & acceleration_nmPerSecPerSec, long & deceleration_nmPerSecPerSec, long & stp_nmPerSecPerSec )
{
	int controllerID = g_motionServices_ptr->getMotionControllerIdOfAxis( axisName );
	int status = DoleOut_ConfirmNoKillAllMotionRequests( controllerID );
	if (status != STATUS_OK)
	{
		return status;	/// EARLY RETURN!!	EARLY RETURN!!
	}
	long commandId = -1L;
	if (!m_desHelper->ExtractMessageIDs( m_threadName, m_commandMessageInObj, axisName.c_str(), commandId ))
	{
		LOG_DEBUG( m_threadName, axisName.c_str(), "Failed to get command_id" );
	}

	double goalPosition_mm = (float)position_nm / _1MEG_F;
	MotionAxis &axis = g_motionServices_ptr->getAxis(axisName);
	if (true)
	{
		stringstream msg;
		msg << "Name of axis: " << axis.getName() << axis.getChannelIndex();
		LOG_DEBUG( m_threadName, axisName.c_str(), msg.str().c_str() );
	}

	/// Prepare some state variables.
	bool isMoving = false;
	bool hasVelocity = false;
	bool isFarFromGoal = false;
	long goalPosition_usteps = (long)(goalPosition_mm * axis.getCachedPPU());
	long axisResolution_usteps = (axisResolution_nm * axis.getCachedPPU()) / _1MEG_L;
	long actualPosition_usteps = 0L;
	float actualPosition_mm = 0.0;
	float velocity = 0.0;

	if (isnan( goalPosition_mm ))
	{
		LOG_ERROR( m_threadName, axisName.c_str(), "the goal position is not a number" );
		return STATUS_MATH_ERROR;
	}
	if ((speed_nmPerSec == 0) || (acceleration_nmPerSecPerSec == 0))
	{
		LOG_ERROR( m_threadName, axisName.c_str(),  "velocity or accel is 0. Aborting." );
		return STATUS_MATH_ERROR;
	}

	///
	/// Detect current position to see if motion is needed.
	///
	try {
		actualPosition_usteps = (long)(axis.getActualPosition());
		if (true)
		{
			stringstream msg;
			msg << "goalPos_usteps=" << goalPosition_usteps << "|actualPos_usteps="<< actualPosition_usteps;
			LOG_DEBUG( m_threadName, axisName.c_str(), msg.str().c_str() );
		}
		isFarFromGoal = (Abs( actualPosition_usteps - goalPosition_usteps ) >  axisResolution_usteps);
	} catch (PValuePendingException &ex) {
		/// PValue data requested, but not yet received
		LOG_INFO( m_threadName, axisName.c_str(), "position: STILL PENDING" );
		status = STATUS_MOVEMENT_FAILED;
	}
	if (!isFarFromGoal)
	{
		/// No motion is needed. Say we succeeded.
		LOG_TRACE( m_threadName, axisName.c_str(), "Already at desired position." );
		// TODO: Add a way to print terminate-move log for this case.
		return status;		// EARLY RETURN!	EARLY RETURN!
	}

	///
	/// Send the command that starts mpotion.
	///
	ACR7000 *pController = nullptr;
	try {
		pController = (ACR7000*)g_motionServices_ptr->getMotionControllerOfAxis( axisName );
		/// Convert values to millimeters.
		pController->setVelAccelDecelStop( (float)speed_nmPerSec/_1MEG_F, (float)acceleration_nmPerSecPerSec/_1MEG_F, (float)deceleration_nmPerSecPerSec/_1MEG_F, (float)stp_nmPerSecPerSec/_1MEG_F );
		usleep(500000);
		axis.absoluteMove( goalPosition_mm );
		usleep(500000);
    } catch (...) {
		status = STATUS_MOVEMENT_FAILED;
		LOG_ERROR( m_threadName, axisName.c_str(), "unknown EXCEPTION!" );
		return status;	//	EARLY RETURN!	EARLY RETURN!
	}

	// Temporary debug.
	{
		stringstream msg;
		msg << axisName << "_PPU=" << axis.getCachedPPU();
		LOG_DEBUG( m_threadName, axisName.c_str(), msg.str().c_str() );
	}

	///
	/// Wait for motion to start
	///
	long masterFlags = 0L;
	bool awaitingStartOfMove = true;
	int loopCount = 0;
	int strikeCount = 0;
	while (awaitingStartOfMove)
	{
		try {
			masterFlags = axis.getMasterFlags();
			isMoving = ((masterFlags & MASK_IN_MOTION) == MASK_IN_MOTION);
			hasVelocity = isMoving;
			if (isMoving)
			{
				awaitingStartOfMove = false;
			}
		}  catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, axisName.c_str(), "masterFlags: STILL PENDING" );
		}
		try {
			actualPosition_usteps = (long)(axis.getActualPosition());
			if (Abs( actualPosition_usteps - goalPosition_usteps ) <=  axisResolution_usteps)
			{
				/// Is at goal, so movement came and went without notice.
				awaitingStartOfMove = false;
				isFarFromGoal = false;
			}
		} catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, axisName.c_str(), "position: STILL PENDING" );
			status = STATUS_MOVEMENT_FAILED;
		}

		if (loopCount++ > MAX_LOOPS_AWAITING_MOTION_START)
		{
			status = STATUS_MOVEMENT_FAILED;
			LOG_ERROR( m_threadName, axisName.c_str(), "timed out waiting for motion to start" );
			return status;	//	EARLY RETURN!	EARLY RETURN!
		}
		usleep(10000);
	}

	///
	/// Wait for motion to stop
	///
	loopCount = 0;
	strikeCount = 0;
	long axisFlags = 0L;
	string axisFlagsStr = "";
	bool notInPositionBand = true;
	float distanceFromGoal_mm = 0.0;

	string intermediateNipHeader = INTERMEDIATE_NIP_KEY;
	intermediateNipHeader += ",command_id,deltaPos(usteps),axisResolution(usteps),actualPos(mm),notInPositionBand,isMoving,hasVelocity";
	LOG_TRACE( m_threadName, "header", intermediateNipHeader.c_str() );

	string intermediateMoveHeader = INTERMEDIATE_MOVE_KEY;
	intermediateMoveHeader += ",command_id,masterFlags,axisFlags,velocity,actualPos(mm),goalPos(mm),distanceFromGoal(mm)";
	LOG_TRACE( m_threadName, "header", intermediateMoveHeader.c_str() );
	
	string terminateMoveHeader = TERMINATE_MOVE_KEY;
	terminateMoveHeader += ",command_id,result_type,velocity,acceleration,finalPos(mm),duration(s)";
	LOG_TRACE( m_threadName, "header", terminateMoveHeader.c_str() );

	while (hasVelocity || isMoving || notInPositionBand) {
		try {
			actualPosition_usteps = (long)axis.getActualPosition();
			actualPosition_mm = actualPosition_usteps / axis.getCachedPPU();
			isFarFromGoal = (Abs( actualPosition_usteps - goalPosition_usteps ) > axisResolution_usteps);
			distanceFromGoal_mm = Abs( actualPosition_mm - goalPosition_mm );
		} catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, axisName.c_str(), "position: STILL PENDING" );
		}
		try {
			velocity = axis.getCurrentVelocity();
			hasVelocity = (Abs(velocity) > MAX_ALLOWABLE_VELOCITY_ERROR);
		} catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, axisName.c_str(), "velocity: STILL PENDING" );
		}
		try {
			masterFlags = axis.getMasterFlags();
			isMoving = ((masterFlags & MASK_IN_MOTION) == MASK_IN_MOTION);
		}  catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, axisName.c_str(), "masterFlags: STILL PENDING" );
		}
		try {
			axisFlags = axis.getAxisFlags();
			notInPositionBand = ((axisFlags & MASK_IPB) == MASK_IPB);
		}  catch (PValuePendingException &ex) {
			/// PValue data requested, but not yet received
			LOG_INFO( m_threadName, axisName.c_str(), "axisFlags: STILL PENDING" );
		}
		
		if ((!hasVelocity) && (!isMoving) && notInPositionBand)
		{
			/// Typically, we get here when a movement has completed but the notInPositionBand flag
			/// doesn't reflect that yet.
			if (strikeCount++ % 10 == 0);
			{
				stringstream msg;
				msg << INTERMEDIATE_NIP_KEY << "," << commandId << "," << Abs( actualPosition_usteps - goalPosition_usteps ) << "," << axisResolution_usteps << "," << float2str6(actualPosition_mm) << ",";
				msg <<  bool2str(notInPositionBand) << "," << bool2str(isMoving) << "," << bool2str(hasVelocity);
				LOG_WARN( m_threadName, axisName.c_str(), msg.str().c_str() );
			}
			if (strikeCount >= MAX_STRIKES_LINEAR_AXIS)
			{
				try {
					// TODO: Find a way to get axis number from motion services.
					// notInPositionBand = pController->getAxisFlag( BIT_IPB, 0 );
					int axisNumber = 0; /// valid for "Y" or "SH"
					if (axisName.compare( "X" ) == 0) axisNumber = 1;
					if (axisName.compare( "XX" ) == 0) axisNumber = 2;
					if (axisName.compare( "YY" ) == 0) axisNumber = 3;
					if (axisName.compare( "Z" ) == 0) axisNumber = 1;
					if (axisName.compare( "ZZ" ) == 0) axisNumber = 2;
					if (axisName.compare( "ZZZ" ) == 0) axisNumber = 3;
					pController->printActualPositionViaAscii( axisNumber );
					usleep(10000);
				} catch (...) {
					LOG_ERROR( m_threadName, axisName.c_str(), "unknown EXCEPTION!" );
				}
				/// Movement somehow stopped far from goal.
				stringstream msg;
				msg << INTERMEDIATE_MOVE_KEY << "," << commandId << "," << long2strHx(masterFlags) << "," << long2strHx(axisFlags) << "," << velocity << ",";
				msg << float2str6(actualPosition_mm) << "," << float2str6(goalPosition_mm) << "," << float2str6(distanceFromGoal_mm);
				LOG_TRACE( m_threadName, axisName.c_str(), msg.str().c_str() );

				/// Expect the caller to send an alert when it sees this status.
				status = STATUS_MOVEMENT_FAILED;
				LOG_ERROR( m_threadName, axisName.c_str(), "stopped before reaching goal, according to IPB" );
				if ((actualPosition_mm != 0.0) || (goalPosition_mm == 0.0))
				{
					position_nm = ((actualPosition_usteps * _1MEG_L) / (long)axis.getCachedPPU());
				}
				/// Send an alert.
				if (true)
				{
					stringstream topic_stream;
					topic_stream << TOPIC_ES << CHANNEL_ALERT;

					/// Collect fields from the command's header for use in the alert.
					std::string messageType = "";
					int commandGroupId = 0;
					int commandId = 0;
					std::string sessionId = "";
					std::string machineId = "";
					bool succeeded = m_desHelper->SplitCommand( m_commandMessageInObj,
																&messageType,
																nullptr, // &transportVersion,
																&commandGroupId,
																&commandId,
																&sessionId,
																&machineId );

					json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
																		commandGroupId,
																		commandId,
																		sessionId, // Or topic_stream.str()?
																		ComponentType::XYAxes,
																		STATUS_AXIS_X_FAULTED,
																		false ); /// thisIsCausingHold
					bool sentOK = m_desHelper->SendAlert( alertObj, m_outBox, topic_stream.str() );
				}
				return status;	//	EARLY RETURN!	EARLY RETURN!
			}
		}
		else if (loopCount++ % 10 == 0)
		{
			/// Movement is on course.
			stringstream msg;
			msg << INTERMEDIATE_MOVE_KEY << "," << commandId << "," << long2strHx(masterFlags) << "," << long2strHx(axisFlags) << "," << velocity << ",";
			msg << float2str6(actualPosition_mm) << "," << float2str6(goalPosition_mm) << "," << float2str6(distanceFromGoal_mm);
			LOG_TRACE( m_threadName, axisName.c_str(), msg.str().c_str() );
		}
		///
		/// Handle time-out
		///
		if (loopCount > MAX_LOOPS_AWAITING_MOTION_END)
		{
			stringstream msg;
			msg << INTERMEDIATE_MOVE_KEY << "," << commandId << "," << long2strHx(masterFlags) << "," << long2strHx(axisFlags) << "," << velocity << ",";
			msg << float2str6(actualPosition_mm) << "," << float2str6(goalPosition_mm) << "," << float2str6(distanceFromGoal_mm);
			LOG_TRACE( m_threadName, axisName.c_str(), msg.str().c_str() );

			status = STATUS_MOVEMENT_FAILED;
			LOG_ERROR( m_threadName, axisName.c_str(), "timed out waiting for motion to end" );
			position_nm = ((actualPosition_usteps * _1MEG_L) / (long)axis.getCachedPPU());
			return status;	//	EARLY RETURN!	EARLY RETURN!
		}
		usleep(10000);
	}
	if (false)
	{
		stringstream msg;
		msg << axisName << ": masterFlags=" << long2strHx(masterFlags) << "|axisFlags=" << long2strHx(axisFlags) << "|vel=" << velocity;
		msg << "|actualPos(mm)=" << float2str6(actualPosition_mm) << "|goalPos(mm)=" << float2str6(goalPosition_mm) << "|distanceFromGoal(mm)=" << float2str6(distanceFromGoal_mm);
		LOG_INFO( m_threadName, axisName.c_str(), msg.str().c_str() );
	}
	if (true)
	{
		stringstream msg;
		/// The result_type will be one of "skipped", "completed", "timed-out"
		msg << TERMINATE_MOVE_KEY << "," << commandId << ",result_type,velocity,acceleration,finalPos(mm),duration(s)";
		LOG_TRACE( m_threadName, axisName.c_str(), msg.str().c_str() );
	}

	/// Movement stopped at goal.
	position_nm = ((actualPosition_usteps * _1MEG_L) / (long)axis.getCachedPPU());
    long quinaryFlags = (long)(axis.getQuinaryAxisFlags());
	if ((quinaryFlags & MASK_FOUND_HOME) == MASK_FOUND_HOME)
	{
		LOG_INFO( m_threadName, axisName.c_str(), "FOUND_HOME flag confirms at-home!" );
	}
	return status;
}

///----------------------------------------------------------------------
/// Implement_GetAxisPosition
/// Measures the actual, position and returns it as position_nm.
///----------------------------------------------------------------------
int CommandDolerThread::Implement_GetAxisPosition( std::string axisNameStr, long & position_nm )
{
	int status = STATUS_OK;
	long position_usteps = 0L;
	long ppu = 0L;
	MotionAxis &axis = g_motionServices_ptr->getAxis(axisNameStr);

	try {
		position_usteps = (long)axis.getActualPosition();
		ppu = (long) axis.getCachedPPU();
		position_nm = ((position_usteps * _1MEG_L) / ppu);
	} catch (PValuePendingException &ex) {
		/// PValue data requested, but not yet received
		LOG_ERROR( m_threadName, axisNameStr.c_str(), "position: STILL PENDING" );
		status = STATUS_MOVEMENT_FAILED;
	}
	if (true)
	{
		stringstream msg;
		msg << axisNameStr << "position(nm)=" << position_nm;
		LOG_TRACE( m_threadName, axisNameStr.c_str(), msg.str().c_str() );
	}
	return status;
}


int CommandDolerThread::DoleOut_HomeUnsafeHardware()
{
	m_doSendStatus = false;  /// No status message for set_hardware_state. It is a purely internal message.
	int status = STATUS_OK;
	LOG_DEBUG( m_threadName, "init", "-", "fin");
	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );

	if (m_threadName == DOLER_FOR_CXD)
	{
		if (g_XYState.load( std::memory_order_acquire ) == ComponentState::SettingsConfirmed)
		{
			/// Home Y and X.
			bool x_confirmed = false;
			bool y_confirmed = false;
			
			if (Implement_SetHomeInputs( payloadObj, HOME_X_INPUTS, A_CONTROLLER, "X" ))
			{
				ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at( A_CONTROLLER );
				pController->binarySetLong( HOME_MSEEK_DIST, -1000 ); // TODO: Get from a component property.
				usleep(10000);
				status = Implement_HomeAxis( A_CONTROLLER, HOME_X_ROUTINE, "X", x_confirmed );
				if ((status != STATUS_OK) || (!x_confirmed))
				{
					g_XYState.store( ComponentState::Faulty, std::memory_order_relaxed );
					/// HALT
					g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed );
					LOG_ERROR( m_threadName, "init", "Failed to enable X axis" );
					// TODO: Send an alert.
				}
			}
			else
			{
				status = STATUS_INIT_FAILED;
				LOG_ERROR( m_threadName, "init", "Failed to set input values for X axis" );
			}

			if (status == STATUS_OK)
			{
				if (Implement_SetHomeInputs( payloadObj, HOME_Y_INPUTS, A_CONTROLLER, "Y" ))
				{
					ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at( A_CONTROLLER );
					pController->binarySetLong( HOME_MSEEK_DIST, 1000 ); // TODO: Get from a component property.
					status = Implement_HomeAxis( A_CONTROLLER, HOME_Y_ROUTINE, "Y", y_confirmed );
					usleep(10000);
					if ((status != STATUS_OK) || (!y_confirmed))
					{
						g_XYState.store( ComponentState::Faulty, std::memory_order_relaxed );
						/// HALT
						g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed );
						LOG_ERROR( m_threadName, "init", "Failed to enable Y axis" );
						// TODO: Send an alert.
					}
				}
				else
				{
					status = STATUS_INIT_FAILED;
					LOG_ERROR( m_threadName, "init", "Failed to set input values for Y axis" );
				}
			}

			if ((status == STATUS_OK) && x_confirmed && y_confirmed)
			{
				m_states_long[X_NM] = 0L;
				m_states_long[Y_NM] = 0L;
				LOG_TRACE( m_threadName, "init", "XY successfully homed" );
				g_XYState.store( ComponentState::Homed2, std::memory_order_relaxed );
			}
		}

		if (g_XXState.load( std::memory_order_acquire ) == ComponentState::SettingsConfirmed)
		{
			/// Home XX.
			if (status == STATUS_OK)
			{
				bool xx_confirmed = false;
				if (Implement_SetHomeInputs( payloadObj, HOME_XX_INPUTS, A_CONTROLLER, "XX" ))
				{
					ACR7000 *pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at( A_CONTROLLER );
					pController->binarySetLong( HOME_MSEEK_DIST, -1000 ); // TODO: Get from a component property.
					usleep(10000);
					status = Implement_HomeAxis( A_CONTROLLER, HOME_XX_ROUTINE, "XX", xx_confirmed );
					if ((status == STATUS_OK) && (xx_confirmed))
					{
						m_states_long[XX_NM] = 0L;
						g_XXState.store( ComponentState::Homed2, std::memory_order_relaxed );
					}
					else
					{
						g_XXState.store( ComponentState::Faulty, std::memory_order_relaxed );
						/// HALT
						g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed );
						LOG_ERROR( m_threadName, "init", "Failed to enable XX axis" );
						// TODO: Send an alert.
					}
				}
				else
				{
					status = STATUS_INIT_FAILED;
					LOG_ERROR( m_threadName, "init", "Failed to set input values for XX axis" );
				}
			}
		}
		if (g_YYState.load( std::memory_order_acquire ) == ComponentState::SettingsConfirmed)
		{
			/// Home YY.
			if (status == STATUS_OK)
			{
				bool yy_confirmed = false;
				if (Implement_SetHomeInputs( payloadObj, HOME_YY_INPUTS, A_CONTROLLER, "YY" ))
				{
					status = Implement_HomeAxis( A_CONTROLLER, HOME_YY_ROUTINE, "YY", yy_confirmed );
					if ((status == STATUS_OK) && (yy_confirmed))
					{
						m_states_long[YY_NM] = 0L;
						g_YYState.store( ComponentState::Homed2, std::memory_order_relaxed );
					}
					else
					{
						g_YYState.store( ComponentState::Faulty, std::memory_order_relaxed );
						/// HALT
						g_EsHoldState.store( HoldType::HoldUp_Freeze, std::memory_order_relaxed );
						LOG_ERROR( m_threadName, "init", "Failed to enable YY axis" );
						// TODO: Send an alert.
					}
				}
				else
				{
					status = STATUS_INIT_FAILED;
					LOG_ERROR( m_threadName, "init", "Failed to set input values for YY axis" );
				}
			}
		}
		if (g_ZState.load( std::memory_order_acquire ) == ComponentState::Homed1)	g_ZState.store( ComponentState::Homed2, std::memory_order_relaxed );
		if (g_ZZState.load( std::memory_order_acquire ) == ComponentState::Homed1)	g_ZZState.store( ComponentState::Homed2, std::memory_order_relaxed );
		if (g_ZZZState.load( std::memory_order_acquire ) == ComponentState::Homed1)	g_ZZZState.store( ComponentState::Homed2, std::memory_order_relaxed );
	}
	if (m_threadName == DOLER_FOR_CXPM)
	{
		// TODO: Reconsider which thread should handle the barcode reader.
		ComponentState state = g_BarcodeReaderState.load( std::memory_order_acquire );
		if (state == ComponentState::Homed1)	g_BarcodeReaderState.store( ComponentState::Homed2, std::memory_order_relaxed );
	}
	if (m_threadName == DOLER_FOR_PUMP)
	{
		ComponentState state = g_AspirationPump1State.load( std::memory_order_acquire );
		if (state == ComponentState::Homed1)	g_AspirationPump1State.store( ComponentState::Homed2, std::memory_order_relaxed );
		state = g_DispensePump1State.load( std::memory_order_acquire );
		if (state == ComponentState::Homed1)	g_DispensePump1State.store( ComponentState::Homed2, std::memory_order_relaxed );
		state = g_DispensePump2State.load( std::memory_order_acquire );
		if (state == ComponentState::Homed1)	g_DispensePump2State.store( ComponentState::Homed2, std::memory_order_relaxed );
		state = g_DispensePump3State.load( std::memory_order_acquire );
		if (state == ComponentState::Homed1)	g_DispensePump3State.store( ComponentState::Homed2, std::memory_order_relaxed );
		state = g_PickingPump1State.load( std::memory_order_acquire );
		if (state == ComponentState::Homed1)	g_PickingPump1State.store( ComponentState::Homed2, std::memory_order_relaxed );
	}
	if (m_threadName == DOLER_FOR_RING_HOLDER)
	{
		ComponentState state = g_AnnularRingHolderState.load( std::memory_order_acquire );
		if (state == ComponentState::Homed1)	g_AnnularRingHolderState.store( ComponentState::Homed2, std::memory_order_relaxed );
	}
	if (m_threadName == DOLER_FOR_CXPM)
	{
		ComponentState state = g_CxPMState.load( std::memory_order_acquire );
		if (state == ComponentState::SettingsConfirmed)	g_CxPMState.store( ComponentState::Homed2, std::memory_order_relaxed );
	}
	if (m_threadName == DOLER_FOR_OPTICS)
	{
		ComponentState state = g_OpticalColumnState.load( std::memory_order_acquire );
		/// The optical column has parts that need both home1 moves and other parts that need home2 moves.
		if (state == ComponentState::Homed1)
		{
			status = Implement_HomeTurret();
			if (status == STATUS_OK)
			{
				g_OpticalColumnState.store( ComponentState::Homed2, std::memory_order_relaxed );
			}
			else
			{
				g_OpticalColumnState.store( ComponentState::Faulty, std::memory_order_relaxed );
				LOG_ERROR( m_threadName, "init", "Failed to home turret" );
			}
		}
		state = g_WhiteLightSourceState.load( std::memory_order_acquire );
		if (state == ComponentState::Homed1) g_WhiteLightSourceState.store( ComponentState::Homed2, std::memory_order_relaxed );
		state = g_FluorescenceLightFilterState.load( std::memory_order_acquire );
		if (state == ComponentState::Homed1) g_FluorescenceLightFilterState.store( ComponentState::Homed2, std::memory_order_relaxed );
		state = g_CameraState.load( std::memory_order_acquire );
		if (state == ComponentState::Homed1) g_CameraState.store( ComponentState::Homed2, std::memory_order_relaxed );
	}
	return status;
}

///----------------------------------------------------------------------
/// DoleOut_HomeInitializePickingPump
///
/// Initializes the picking pump. The picking pump needs to perform an
/// EjectTip before initialization, so this function moves the tip to
/// safe place to eject any tip that may be present.
/// 1. MoveAllTipsToSafeHeight()
/// 2. MoveToYYNM(ComponentProperties.YYAxis.yy_key_to_tip_path + ComponentProperties.ZAxis.z_y_offset_from_xx_axis)
/// 3. MoveToXXNM(ComponentProperties.XXAxis.xx_z_tip_to_key_entry_position)
/// 4. MoveToZNM(ComponentProperties.ZAxis.z_to_top_of_seating_key_height)
/// 5. ADP.InitializeDevice()
///		A. EjectTipAtInitialization
/// 	B. InitializeDevice
/// 6. MoveAllTipsToSafeHeight()
/// 7. MoveToYYNM(0) // YY home
/// 8. MoveToXXNM(0) // XX home
///----------------------------------------------------------------------
int CommandDolerThread::DoleOut_HomeInitializePickingPump()
{
	m_doSendStatus = false;  /// No status message for set_hardware_state. It is a purely internal message.
	int status = STATUS_OK;
	LOG_DEBUG( m_threadName, "init", "pick", "fin");
	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );
	ComponentState state = ComponentState::Uninitialized;

	state = g_XYState.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_XYState.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_XXState.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_XXState.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_YYState.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_YYState.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_ZState.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_ZState.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_ZZState.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_ZZState.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_ZZZState.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_ZZZState.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_AspirationPump1State.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_AspirationPump1State.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_DispensePump1State.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_DispensePump1State.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_DispensePump2State.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_DispensePump2State.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_DispensePump3State.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_DispensePump3State.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_WhiteLightSourceState.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_WhiteLightSourceState.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_FluorescenceLightFilterState.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_FluorescenceLightFilterState.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_AnnularRingHolderState.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_AnnularRingHolderState.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_CameraState.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_CameraState.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_OpticalColumnState.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_OpticalColumnState.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_IncubatorState.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_IncubatorState.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_CxPMState.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_CxPMState.store( ComponentState::Homed3, std::memory_order_relaxed );
	state = g_BarcodeReaderState.load( std::memory_order_acquire );
	if (state == ComponentState::Homed2)	g_BarcodeReaderState.store( ComponentState::Homed3, std::memory_order_relaxed );

	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string(payloadObj);
		cout << payloadStr << endl;
	}
	else
	{
		LOG_ERROR( m_threadName, "CxD", "FAILED to find payload object" );
		return STATUS_DEVICE_ERROR;			/// EARLY RETURN!	EARLY RETURN!
	}

	if (m_threadName == DOLER_FOR_CXD)
	{
		long yy_keyToTipPath = 0L;
		long z_y_offsetFromXXAxis = 0L;
		long xx_z_tipToKeyEntryPosition = 0L;
		long z_toTopOfSeatingKeyHeight = 0L;
		string pumpVendor = "";
		string pumpModel = "";
		bool pickingPumpWorked = false;
		bool succeeded = true;
		ComponentState pickingPumpState = g_PickingPump1State.load( std::memory_order_acquire );
		if (pickingPumpState == ComponentState::Mocked)
		{
			/// All the moves in this function are for the purpose of ejecting a tip.
			/// If the picking pump is mocked, the moves are not needed.
			LOG_TRACE( m_threadName, "pick", "Performing MOCK init of PickingPump1" );
			return status;			/// EARLY RETURN!	EARLY RETURN!
		}

		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, YY_KEY_TO_TIP_PATH, yy_keyToTipPath );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_Y_OFFSET_FROM_XX_AXIS, z_y_offsetFromXXAxis );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, XX_Z_TIP_TO_KEY_ENTRY_POSITION, xx_z_tipToKeyEntryPosition );
		succeeded &= m_desHelper->ExtractLongValue( m_threadName, payloadObj, Z_TO_TOP_OF_SEATING_KEY_HEIGHT, z_toTopOfSeatingKeyHeight );
		succeeded &= m_desHelper->ExtractStringValue( m_threadName, payloadObj, PUMP_VENDOR, pumpVendor );
		succeeded &= m_desHelper->ExtractStringValue( m_threadName, payloadObj, PUMP_MODEL, pumpModel );
		if (!succeeded)
		{
			status = STATUS_CMD_MSG_DATA_MISSING;
			LOG_ERROR( m_threadName, "pick", "Data missing in payload" );
		}

		if (status == STATUS_OK)
		{
			status = DoleOut_MoveAllTipsToSafeHeight();
		}

		if (status == STATUS_OK)
		{
			status = Implement_moveToYYNm( yy_keyToTipPath + z_y_offsetFromXXAxis );
		}

		if (status == STATUS_OK)
		{
			status = Implement_moveToXXNm( xx_z_tipToKeyEntryPosition );
		}

		if (status == STATUS_OK)
		{
			status = Implement_moveToZNm( z_toTopOfSeatingKeyHeight );
		}

		if ((status == STATUS_OK) && (pickingPumpState == ComponentState::Homed2))
		{
			if (DesHelper::IsTecanAdpDetect( pumpVendor, pumpModel ))
			{
				/// Lock g_SerialPortMutex (in the PUMP thread) while initializing the ADP.
				unique_lock<mutex> lock( g_SerialPortMutex ); /// IMPORTANT! The Pump thread and CxD thread share this.
				/// Initialize a Tecan ADP.
				bool isInitialized = false;
				bool isBusy = false;
				bool hasErrors = false;
				int stdError = 0;
				int count = 0;
				BoxOfErrors box;

				/// EjectTipAtInitialization
				do {
					status = m_tecanADP->EjectTipAtInitialization( m_threadName, stdError );
					if (stdError == cmERROR_CODE_TIP_LOST_OR_NOT_PRESENT)
					{
						status = STATUS_OK;
					}
					usleep( m_tecanADP->StandardWait_uS );
					count++;
					if (status == STATUS_OK) count = 5;
				} while (count < 5); // TODO: What is a good number of tries?
				usleep( 10 * m_tecanADP->StandardWait_uS );

				/// Wait for EjectTipAtInitialization to end.
				if (status == STATUS_OK)
				{
					status = m_tecanADP->WaitForNotBusy( m_threadName, __func__ );
				}

				/// Find out whether the ADP is initialized.
				if (status == STATUS_OK)
				{
					count = 0;
					do {
						status = m_tecanADP->IsDeviceInitialized( m_threadName, isBusy, hasErrors, isInitialized );
						usleep( m_tecanADP->StandardWait_uS );
						count++;
						if (status == STATUS_OK) count = 5;
					} while (count < 5);
					stringstream msg;
					msg << "IsDeviceInitialized called " << count << " times";
					LOG_TRACE(  m_threadName, "pick", msg.str().c_str() );
				}
				// TODO: Decide whether we should bypass intitialization if isInitialized.
				if ((status == STATUS_OK) && (!isInitialized))
				{
					count = 0;
					do {
						status = m_tecanADP->InitializeDevice( m_threadName, stdError );
						usleep( m_tecanADP->StandardWait_uS );
						count++;
						if (status == STATUS_OK) count = 5;
					} while (count < 5);
					stringstream msg;
					msg << "InitializeDevice called " << count << " times";
					LOG_TRACE(  m_threadName, "pick", msg.str().c_str() );
				}

				/// Loop waiting for isInitialized && notBusy.
				if (status == STATUS_OK)
				{
					status = m_tecanADP->WaitForNotBusy( m_threadName, __func__ );
				}
				if ((status == STATUS_OK) && (!isInitialized))
				{
					status = m_tecanADP->IsDeviceInitialized( m_threadName, isBusy, hasErrors, isInitialized );
					usleep( m_tecanADP->StandardWait_uS );
				}
				usleep( 20 * m_tecanADP->StandardWait_uS );

				status = (!isBusy && isInitialized) ? STATUS_OK : STATUS_PUMP_FAULTED;

				/// For now, record success locally.
				/// Don't update the global state variable until all movement is done.
				pickingPumpWorked = (status == STATUS_OK);
			}
			else /// The picking pump is not a Tecan ADP
			{
				stringstream msg;
				msg << "Unknown picking pump type! vendor=" << pumpVendor << "|model=" << pumpModel;
				LOG_ERROR( m_threadName, "pick", msg.str().c_str() );
				pickingPumpWorked = false;
			}
		}

		if (true)
		{
			int stat = DoleOut_MoveAllTipsToSafeHeight(); /// Let's not overwrite our global status variable here.
		}
		if (status == STATUS_OK)
		{
			status = Implement_moveToYYNm( 0L );
		}
		if (status == STATUS_OK)
		{
			status = Implement_moveToXXNm( 0L );
		}

		/// Now that movement is done, update the picking pump state.
		if (pickingPumpState == ComponentState::Homed2)
		{
			if (pickingPumpWorked)
			{
				LOG_DEBUG( m_threadName, "pick", "Changing state to Homed3" );
				g_PickingPump1State.store( ComponentState::Homed3, std::memory_order_relaxed );
			}
			else
			{
				LOG_WARN( m_threadName, "pick", "Changing state to Faulty" );
				g_PickingPump1State.store( ComponentState::Faulty, std::memory_order_relaxed );
			}
		}
	}

	return status;
}

int CommandDolerThread::DoleOut_MoveHardwareToActiveStates()
{
	int status = STATUS_OK;
	m_doSendStatus = false;  /// No status message for set_hardware_state. It is a purely internal message.
	cout << m_threadName << ": DoleOut_HomeUnsafeHardware() called." << endl;

	json_object * payloadObj = json_object_object_get( m_commandMessageInObj, PAYLOAD );

	if (m_threadName == DOLER_FOR_RING_HOLDER)
	{
		if (payloadObj)
		{
			long current_ring_position = this->m_desHelper->ExtractPayloadLongValue( m_commandMessageInObj, CURRENT_RING_POSITION );
			ComponentState state = g_AnnularRingHolderState.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)
			{
				if (true) /// TODO: Set the hardware state.
				{
					m_states_long[ANNULAR_RING_POSITION] = current_ring_position;
					g_AnnularRingHolderState.store( ComponentState::Good, std::memory_order_relaxed );
				}
				else
				{
					g_AnnularRingHolderState.store( ComponentState::Faulty, std::memory_order_relaxed );
				}
			}
		}
	}
	if (m_threadName == DOLER_FOR_OPTICS)
	{
		if (payloadObj)
		{
			ComponentState state = g_XYState.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)
			{
				unsigned long y_set_speed = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, Y_SET_SPEED );
				unsigned long y_acceleration = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, Y_ACCELERATION );
				unsigned long y_deceleration = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, Y_DECELERATION );
				unsigned long y_jogging_speed = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, Y_JOGGING_SPEED );
				unsigned long y_jogging_acceleration = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, Y_JOGGING_ACCELERATION );
				unsigned long y_jogging_deceleration = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, Y_JOGGING_DECELERATION );
				if (true) // TODO: Set the hardware states.
				{
					m_states_ulong[Y_SET_SPEED] = y_set_speed;
					m_states_ulong[Y_ACCELERATION] = y_acceleration;
					m_states_ulong[Y_DECELERATION] = y_deceleration;
					m_states_ulong[Y_JOGGING_SPEED] = y_jogging_speed;
					m_states_ulong[Y_JOGGING_ACCELERATION] = y_jogging_acceleration;
					m_states_ulong[Y_JOGGING_DECELERATION] = y_jogging_deceleration;
					g_XYState.store( ComponentState::Good, std::memory_order_relaxed );
				}
				else
				{
					g_XYState.store( ComponentState::Faulty, std::memory_order_relaxed );
				}
			}
			state = g_WhiteLightSourceState.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)
			{
				ComponentState whiteLightComponentState = ComponentState::Faulty; /// Init.
				bool white_light_on_state = this->m_desHelper->ExtractPayloadBooleanValue( m_commandMessageInObj, WHITE_LIGHT_ON_STATE );
				if (Implement_SetWhiteLightSourceOn( white_light_on_state ) == STATUS_OK);
				{
					whiteLightComponentState = ComponentState::Good;
				}
				g_WhiteLightSourceState.store( whiteLightComponentState, std::memory_order_relaxed );
			}
			state = g_FluorescenceLightFilterState.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)
			{
				LOG_TRACE(  m_threadName, "init", "FluorescenceLightFilterOn" );
				bool fluorescence_light_on_state = this->m_desHelper->ExtractPayloadBooleanValue( m_commandMessageInObj, FLUORESCENCE_LIGHT_ON_STATE );
				int light_status = Implement_setFluorescenceLightFilterOn( fluorescence_light_on_state ); /// updates m_states_bool[FLUORESCENCE_LIGHT_ON_STATE] too.
				if (light_status != STATUS_OK)
				{
					LOG_ERROR( m_threadName, "init", "FluorescenceLightFilterOn reports failure" );
				}

				LOG_TRACE(  m_threadName, "init", "FluorescenceLightFilterPosition" );
				long active_fluorescence_filter_position = this->m_desHelper->ExtractPayloadLongValue( m_commandMessageInObj, ACTIVE_FLUORESCENCE_FILTER_POSITION );
				cout << endl << "FluorescenceLightFilterPosition = " << active_fluorescence_filter_position << endl << endl;
				int position_status = Implement_setFluorescenceLightFilterPosition( active_fluorescence_filter_position ); /// updates m_states_long[ACTIVE_FLUORESCENCE_FILTER_POSITION] too.
				if (position_status != STATUS_OK)
				{
					LOG_ERROR( m_threadName, "init", "FluorescenceLightFilterPosition reports failure" );
				}
				if ( (light_status == STATUS_OK) && (position_status == STATUS_OK) )
				{
					g_FluorescenceLightFilterState.store( ComponentState::Good, std::memory_order_relaxed );
				}
				else
				{
					LOG_TRACE(  m_threadName, "init", "FluorescenceLightFilter goes faulty" );
					g_FluorescenceLightFilterState.store( ComponentState::Faulty, std::memory_order_relaxed );
				}
			}
			state = g_OpticalColumnState.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)
			{
				unsigned long active_turret_position = m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, ACTIVE_TURRET_POSITION );
				/// We only get to this point after all the hardware has been homed, so we
				/// can expect that the focus is already at a safe position.
				int turretState = Implement_SetOpticalColumnTurretPosition( active_turret_position );

				/// According to Tom Londo's email of 2023-07-12 re: Etaluma focus question, DO NOT change the focus here.
				/// "If wes doesn't know where to place the focus height after an operation like initialization or turret
				/// change, it should just keep it at the safe turret change height.  CxSuite will direct wes to set the
				/// correct focus height at the beginning of each Protocol as necessary.

				if (turretState == STATUS_OK)
				{
					g_OpticalColumnState.store( ComponentState::Good, std::memory_order_relaxed );
				}
				else
				{
					g_OpticalColumnState.store( ComponentState::Faulty, std::memory_order_relaxed );
				}
			}
			state = g_CameraState.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)
			{
				unsigned long gain = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, GAIN );
				unsigned long exposure = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, EXPOSURE );
				unsigned long bin_factor = this->m_desHelper->ExtractPayloadULongValue( m_commandMessageInObj, BIN_FACTOR );
				bool succeeded = true;
				if (STATUS_OK == Implement_setCameraGain( (int) gain ))
				{
					m_states_ulong[GAIN] = gain;
				}
				else
				{
					succeeded = false;
					g_CameraState.store( ComponentState::Faulty, std::memory_order_relaxed );
				}
				if (STATUS_OK == Implement_setCameraExposureTime( exposure ))
				{
					m_states_ulong[EXPOSURE] = exposure;
				}
				else
				{
					succeeded = false;
					g_CameraState.store( ComponentState::Faulty, std::memory_order_relaxed );
				}
				if (STATUS_OK == Implement_setCameraBinFactor( (int) bin_factor ))
				{
					m_states_ulong[BIN_FACTOR] = bin_factor;
				}
				else
				{
					succeeded = false;
					g_CameraState.store( ComponentState::Faulty, std::memory_order_relaxed );
				}
				if (succeeded)
				{
					g_CameraState.store( ComponentState::Good, std::memory_order_relaxed );
				}
			}
			state = g_XXState.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)	g_XXState.store( ComponentState::Good, std::memory_order_relaxed );
			state = g_YYState.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)	g_YYState.store( ComponentState::Good, std::memory_order_relaxed );
			state = g_ZState.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)	g_ZState.store( ComponentState::Good, std::memory_order_relaxed );
			state = g_ZZState.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)	g_ZZState.store( ComponentState::Good, std::memory_order_relaxed );
			state = g_ZZZState.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)	g_ZZZState.store( ComponentState::Good, std::memory_order_relaxed );
			state = g_AspirationPump1State.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)	g_AspirationPump1State.store( ComponentState::Good, std::memory_order_relaxed );
			state = g_DispensePump1State.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)	g_DispensePump1State.store( ComponentState::Good, std::memory_order_relaxed );
			state = g_DispensePump2State.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)	g_DispensePump2State.store( ComponentState::Good, std::memory_order_relaxed );
			state = g_DispensePump3State.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)	g_DispensePump3State.store( ComponentState::Good, std::memory_order_relaxed );
			state = g_PickingPump1State.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)	g_PickingPump1State.store( ComponentState::Good, std::memory_order_relaxed );
			state = g_IncubatorState.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)	g_IncubatorState.store( ComponentState::Good, std::memory_order_relaxed );
			state = g_CxPMState.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)	g_CxPMState.store( ComponentState::Good, std::memory_order_relaxed );
			state = g_BarcodeReaderState.load( std::memory_order_acquire );
			if (state == ComponentState::Homed3)	g_BarcodeReaderState.store( ComponentState::Good, std::memory_order_relaxed );
		} /// end of if(payloadOjb)
	}
	return status;
}

int CommandDolerThread::DoleOut_ConfirmNoKillAllMotionRequests( int controllerID )
{
	int status = STATUS_OK;
	LOG_TRACE( m_threadName, "helper", "-" );
	ACR7000 *pController = nullptr;
	if (controllerID == A_CONTROLLER)
	{
		if ((g_XYState.load( std::memory_order_acquire ) != ComponentState::Mocked) ||
			(g_XXState.load( std::memory_order_acquire ) != ComponentState::Mocked) ||
			(g_YYState.load( std::memory_order_acquire ) != ComponentState::Mocked))
		{
			try {
				pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at( controllerID );
			} catch (std::exception &ex) {
				stringstream msg;
				msg << "Exception while getting controller " << controllerID;
				LOG_FATAL( m_threadName, "helper", msg.str().c_str() );
				return STATUS_CMD_MSG_DATA_MISSING;	/// EARLY RETURN!!	EARLY RETURN!!
			}
			if (g_XYState.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				if (pController->getKillAllMotionRequestFlag( "y" ))
				{
					LOG_FATAL( m_threadName, "helper", "KILL ALL MOTION REQUESTED ON AXIS Y" );
					status = STATUS_KILL_MOTION_Y;
				}
				if (pController->getKillAllMotionRequestFlag( "x" ))
				{
					LOG_FATAL( m_threadName, "helper", "KILL ALL MOTION REQUESTED ON AXIS X" );
					status = STATUS_KILL_MOTION_X;
				}
			}
			if (g_XXState.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				if (pController->getKillAllMotionRequestFlag( "xx" ))
				{
					LOG_FATAL( m_threadName, "helper", "KILL ALL MOTION REQUESTED ON AXIS XX" );
					status = STATUS_KILL_MOTION_XX;
				}
			}
			if (g_YYState.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				if (pController->getKillAllMotionRequestFlag( "yy" ))
				{
					LOG_FATAL( m_threadName, "helper", "KILL ALL MOTION REQUESTED ON AXIS YY" );
					status = STATUS_KILL_MOTION_YY;
				}
			}
		}
	}

	if (controllerID == B_CONTROLLER)
	{
		if ((g_AnnularRingHolderState.load( std::memory_order_acquire ) != ComponentState::Mocked) ||
			(g_ZState.load( std::memory_order_acquire ) != ComponentState::Mocked) ||
			(g_ZZState.load( std::memory_order_acquire ) != ComponentState::Mocked) ||
			(g_ZZZState.load( std::memory_order_acquire ) != ComponentState::Mocked))
		{
			try {
				pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at( controllerID );
			} catch (std::exception &ex) {
				stringstream msg;
				msg << "Exception while getting controller " << controllerID;
				LOG_FATAL( m_threadName, "helper", msg.str().c_str() );
				return STATUS_CMD_MSG_DATA_MISSING;	/// EARLY RETURN!!	EARLY RETURN!!
			}
			if (g_AnnularRingHolderState.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				if (pController->getKillAllMotionRequestFlag( "sh" ))
				{
					LOG_FATAL( m_threadName, "helper", "KILL ALL MOTION REQUESTED ON AXIS SH" );
					status = STATUS_KILL_MOTION_SH;
				}
			}
			if (g_ZState.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				if (pController->getKillAllMotionRequestFlag( "z" ))
				{
					LOG_FATAL( m_threadName, "helper", "KILL ALL MOTION REQUESTED ON AXIS Z" );
					status = STATUS_KILL_MOTION_Z;
				}
			}
			if (g_ZZState.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				if (pController->getKillAllMotionRequestFlag( "zz" ))
				{
					LOG_FATAL( m_threadName, "helper", "KILL ALL MOTION REQUESTED ON AXIS ZZ" );
					status = STATUS_KILL_MOTION_ZZ;
				}
			}
			if (g_ZZZState.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				if (pController->getKillAllMotionRequestFlag( "zzz" ))
				{
					LOG_FATAL( m_threadName, "helper", "KILL ALL MOTION REQUESTED ON AXIS ZZZ" );
					status = STATUS_KILL_MOTION_ZZZ;
				}
			}
		}
	}

	if (controllerID == C_CONTROLLER)
	{
		if ((g_AspirationPump1State.load( std::memory_order_acquire ) != ComponentState::Mocked) ||
			(g_DispensePump1State.load( std::memory_order_acquire ) != ComponentState::Mocked) ||
			(g_DispensePump2State.load( std::memory_order_acquire ) != ComponentState::Mocked) ||
			(g_DispensePump3State.load( std::memory_order_acquire ) != ComponentState::Mocked))
		{
			try {
				pController = (ACR7000*)g_motionServices_ptr->getMotionControllers().at( controllerID );
			} catch (std::exception &ex) {
				stringstream msg;
				msg << "Exception while getting controller " << controllerID;
				LOG_FATAL( m_threadName, "helper", msg.str().c_str() );
				return STATUS_CMD_MSG_DATA_MISSING;	/// EARLY RETURN!!	EARLY RETURN!!
			}
			if (g_AspirationPump1State.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				if (pController->getKillAllMotionRequestFlag( "pn" ))
				{
					LOG_FATAL( m_threadName, "helper", "KILL ALL MOTION REQUESTED ON AXIS PN" );
					status = STATUS_KILL_MOTION_ASP1;
				}
			}
			if (g_DispensePump1State.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				if (pController->getKillAllMotionRequestFlag( "pp" ))
				{
					LOG_FATAL( m_threadName, "helper", "KILL ALL MOTION REQUESTED ON AXIS PP" );
					status = STATUS_KILL_MOTION_DISP1;
				}
			}
			if (g_DispensePump2State.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				if (pController->getKillAllMotionRequestFlag( "ppp" ))
				{
					LOG_FATAL( m_threadName, "helper", "KILL ALL MOTION REQUESTED ON AXIS PPP" );
					status = STATUS_KILL_MOTION_DISP2;
				}
			}
			if (g_DispensePump3State.load( std::memory_order_acquire ) != ComponentState::Mocked)
			{
				if (pController->getKillAllMotionRequestFlag( "pppp" ))
				{
					LOG_FATAL( m_threadName, "helper", "KILL ALL MOTION REQUESTED ON AXIS PPPP" );
					status = STATUS_KILL_MOTION_DISP3;
				}
			}
		}
	}
	stringstream msg;
	msg << "Returning status of " << status;
	LOG_TRACE( m_threadName, "helper", msg.str().c_str() );

	return status;
}


int CommandDolerThread::DoleOut_notImplemented( std::string taskAtHand )
{
	std::this_thread::sleep_for(250ms); // Fake work
	stringstream msg;
	msg << taskAtHand << " being faked as STATUS_NOT_FOUND";
	LOG_WARN( m_threadName, "skip", msg.str().c_str() );
	return STATUS_NOT_FOUND;
}

int CommandDolerThread::DoleOut_fakeImplementation( std::string taskAtHand )
{
	int status = STATUS_OK;
	std::this_thread::sleep_for(250ms); /// Fake work
	stringstream msg;
	msg << taskAtHand << " being faked as STATUS_OK";
	LOG_WARN( m_threadName, "mock", msg.str().c_str() );
	return status;
}
