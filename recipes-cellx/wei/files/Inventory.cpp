///----------------------------------------------------------------------
/// Inventory
/// 
///----------------------------------------------------------------------
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <mutex>
#include "json.h"
#include "Inventory.hpp"

using namespace std;

std::mutex inventoryFileMutex;
Inventory g_plateInventory;
Inventory g_tipInventory;

///----------------------------------------------------------------------
/// ReadFromStringFile
///----------------------------------------------------------------------
void Inventory::ReadFromStringFile( const std::string & filePath, map<string, string> & strMap )
{
	if (FileExists( filePath ) )
	{
		unique_lock<mutex> lock(inventoryFileMutex);
		std::fstream in( filePath.c_str(), std::ios::in );
		if (in.is_open())
		{
			stringstream fileContents;
			string line;
			while (getline(in, line))
			{
				fileContents << line;
			}
			in.close();
			json_object * dictionaryObj = nullptr;
			enum json_tokener_error jerr;
			dictionaryObj = json_tokener_parse_verbose( fileContents.str().c_str(), &jerr );
			if (jerr == json_tokener_success)
			{
				strMap.clear();

				struct json_object_iterator it;
				struct json_object_iterator itEnd;
				it = json_object_iter_init_default();
				it = json_object_iter_begin( dictionaryObj );
				itEnd = json_object_iter_end( dictionaryObj );
				while (!json_object_iter_equal( &it, &itEnd ))
				{
					string key = json_object_iter_peek_name( &it );
					json_object * valueObj = json_object_iter_peek_value( &it );
					string value = json_object_get_string( valueObj );
					strMap[key] = value;
					json_object_iter_next( &it );
				}
			}
			else
			{
				stringstream msg;
				msg << "Failed to parse " << filePath;
				LOG_ERROR( "unk", "reset", msg.str().c_str() );
			}
			json_object_put( dictionaryObj ); /// Free memory
		}
		else
		{
			stringstream msg;
			msg << "Failed to open " << filePath;
			LOG_ERROR( "unk", "reset", msg.str().c_str() );
		}
	}
}

///----------------------------------------------------------------------
/// ReadFromIntFile
///----------------------------------------------------------------------
void Inventory::ReadFromIntFile( const string & filePath, map<string, int> & intMap )
{
	if (FileExists( filePath ) )
	{
		unique_lock<mutex> lock(inventoryFileMutex);
		std::fstream in( filePath.c_str(), std::ios::in );
		if (in.is_open())
		{
			stringstream fileContents;
			string line;
			while (getline(in, line))
			{
				fileContents << line;
			}
			in.close();
			json_object * dictionaryObj = nullptr;
			enum json_tokener_error jerr;
			dictionaryObj = json_tokener_parse_verbose( fileContents.str().c_str(), &jerr );
			if (jerr == json_tokener_success)
			{
				intMap.clear();

				struct json_object_iterator it;
				struct json_object_iterator itEnd;
				it = json_object_iter_init_default();
				it = json_object_iter_begin( dictionaryObj );
				itEnd = json_object_iter_end( dictionaryObj );
				while (!json_object_iter_equal( &it, &itEnd ))
				{
					string key = json_object_iter_peek_name( &it );
					json_object * valueObj = json_object_iter_peek_value( &it );
					int value = json_object_get_int64( valueObj );
					intMap[key] = value;
					json_object_iter_next( &it );
				}
			}
			else
			{
				stringstream msg;
				msg << "Failed to parse " << filePath;
				LOG_ERROR( "unk", "reset", msg.str().c_str() );
			}
			json_object_put( dictionaryObj ); /// Free memory
		}
		else
		{
			stringstream msg;
			msg << "Failed to open " << filePath;
			LOG_ERROR( "unk", "reset", msg.str().c_str() );
		}
	}
}

///----------------------------------------------------------------------
/// ReadFromFiles
///----------------------------------------------------------------------
void Inventory::ReadFromFiles( const char* dirPath )
{
	string locFile = dirPath;
	locFile += '/';
	string subLocFile = locFile;
	string subSubLocFile = locFile;
	string countFile = locFile;
	locFile += "loc.inv";
	subLocFile += "subloc.inv";
	subSubLocFile += "subsubloc.inv";
	countFile += "count.inv";
	
	ReadFromStringFile( locFile, m_Location );
	ReadFromStringFile( subLocFile, m_SubLocation );
	ReadFromStringFile( subSubLocFile, m_SubSubLocation );
	ReadFromIntFile( countFile, m_unitCount );
}

///----------------------------------------------------------------------
/// StoreToStringFile
///----------------------------------------------------------------------
bool Inventory::StoreToStringFile( const std::string & filePath, map<string, string> & strMap )
{
	bool succeeded = true;
	if (FileExists( filePath ))
	{
//		cout << "StoreToStringFile() deleting file " << filePath << endl;
		unique_lock<mutex> lock(inventoryFileMutex);
		if( remove( filePath.c_str() ) != 0 )
		{
			stringstream msg;
			msg << "Failed to delete " << filePath;
			LOG_ERROR( "unk", "reset", msg.str().c_str() );
			succeeded = false;
		}
	}
	if (!FileExists( filePath ))
	{
//		cout << "StoreToStringFile() writing file " << filePath << endl;
		unique_lock<mutex> lock(inventoryFileMutex);
		std::fstream out( filePath.c_str(), std::ios::out );
		{
			if (out.is_open())
			{
				struct json_object * dictionaryObj = json_object_new_object();
				for (std::map<std::string, std::string>::iterator it = strMap.begin(); it != strMap.end(); it++)
				{
					json_object_object_add( dictionaryObj,
											it->first.c_str(),
											json_object_new_string( it->second.c_str() ) );
				}
				const char * dictionaryAsString = json_object_to_json_string( dictionaryObj );
				out << dictionaryAsString;
			}
			else
			{
				stringstream msg;
				msg << "Failed to open " << filePath;
				LOG_ERROR( "unk", "reset", msg.str().c_str() );
				succeeded = false;
			}
			out.close();
		}
	}
	else
	{
		stringstream msg;
		msg << "Failed to overwrite old version of file " << filePath;
		LOG_ERROR( "unk", "reset", msg.str().c_str() );
		succeeded = false;
	}
	return succeeded;
}

///----------------------------------------------------------------------
/// StoreToIntFile
///----------------------------------------------------------------------
bool Inventory::StoreToIntFile( const string & filePath, map<string, int> & intMap )
{
	bool succeeded = true;
	if (FileExists( filePath ))
	{
//		cout << "StoreToIntFile() deleting file " << filePath << endl;
		unique_lock<mutex> lock(inventoryFileMutex);
		if( remove( filePath.c_str() ) != 0 )
		{
			stringstream msg;
			msg << "Failed to delete " << filePath;
			LOG_ERROR( "unk", "reset", msg.str().c_str() );
			succeeded = false;
		}
	}
	if (!FileExists( filePath ))
	{
//		cout << "StoreToIntFile() writing file " << filePath << endl;
		unique_lock<mutex> lock(inventoryFileMutex);
		std::fstream out( filePath.c_str(), std::ios::out );
		{
			if (out.is_open())
			{
				struct json_object * dictionaryObj = json_object_new_object();
				for (std::map<std::string, int>::iterator it = intMap.begin(); it != intMap.end(); it++)
				{
					json_object_object_add( dictionaryObj,
											it->first.c_str(),
											json_object_new_int64( it->second ) );
				}
				const char * dictionaryAsString = json_object_to_json_string( dictionaryObj );
				out << dictionaryAsString;
			}
			else
			{
				stringstream msg;
				msg << "Failed to open " << filePath;
				LOG_ERROR( "unk", "reset", msg.str().c_str() );
				succeeded = false;
			}
			out.close();
		}
	}
	else
	{
		stringstream msg;
		msg << "Failed to overwrite old version of file " << filePath;
		LOG_ERROR( "unk", "reset", msg.str().c_str() );
		succeeded = false;
	}
	return succeeded;
}

///----------------------------------------------------------------------
/// StoreToFiles
///----------------------------------------------------------------------
bool Inventory::StoreToFiles( const char* dirPath )
{
	string locFile = dirPath;
	locFile += '/';
	string subLocFile = locFile;
	string subSubLocFile = locFile;
	string countFile = locFile;
	locFile += "loc.inv";
	subLocFile += "subloc.inv";
	subSubLocFile += "subsubloc.inv";
	countFile += "count.inv";
	
	bool succeeded = StoreToStringFile( locFile, m_Location );
	succeeded &= StoreToStringFile( subLocFile, m_SubLocation );
	succeeded &= StoreToStringFile( subSubLocFile, m_SubSubLocation );
	succeeded &= StoreToIntFile( countFile, m_unitCount );

	return succeeded;
}

///----------------------------------------------------------------------
/// Inventory
///----------------------------------------------------------------------
Inventory::Inventory()
{
	InitializeWithDefaultValues();
}

///----------------------------------------------------------------------
/// ~Inventory
///----------------------------------------------------------------------
Inventory::~Inventory()
{
//	cout << "Inventory::~Inventory() completed." << endl;
}

///----------------------------------------------------------------------
/// InitializeWithDefaultValues
///----------------------------------------------------------------------
void Inventory::InitializeWithDefaultValues()
{
//	cout << "Inventory::InitializeWithDefaultValues() completed." << endl;
}

///----------------------------------------------------------------------
/// 	Location
///----------------------------------------------------------------------
string Inventory::GetLocation(string key)
{
	string retVal = "";
	try
	{
		retVal = m_Location.at(key);
	}
	catch (const out_of_range& oor)
	{
		stringstream msg;
		msg << "Out of Range error in " << oor.what() << "; key=" << key;
		LOG_DEBUG( "unk", "inventory", msg.str().c_str() );
	}
	return retVal;
}

bool Inventory::SetLocation(string key, string location)
{
//	cout << "Inventory::SetLocation" << endl;
	m_Location[key] = location;
//	cout << "Loc[" << key << "] = " << m_Location[key] << endl;
	return true;
}

bool Inventory::DeleteLocation(std::string key)
{
	return (m_Location.erase(key) == 1);
}

void Inventory::PrintLocations()
{
	for (std::map<std::string, std::string>::iterator it = m_Location.begin(); it != m_Location.end(); it++)
	{
		cout << it->first << " => " << it->second << endl;
	}
}

///----------------------------------------------------------------------
/// 	Sub-Location
///----------------------------------------------------------------------
string Inventory::GetSubLocation(string key)
{
	string retVal = "";
	try
	{
		retVal = m_SubLocation.at(key);
	}
	catch (const out_of_range& oor)
	{
		stringstream msg;
		msg << "Out of Range error in " << oor.what() << "; key=" << key;
		LOG_DEBUG( "unk", "inventory", msg.str().c_str() );
	}
	return retVal;
}

bool Inventory::SetSubLocation(string key, string subLocation)
{
	m_SubLocation[key] = subLocation;
	return true;
}

bool Inventory::DeleteSubLocation(std::string key)
{
	return (m_SubLocation.erase(key) == 1);
}

///----------------------------------------------------------------------
/// 	Sub-Sub-Location
///----------------------------------------------------------------------
string Inventory::GetSubSubLocation(string key)
{
	string retVal = "";
	try
	{
		retVal = m_SubSubLocation.at(key);
	}
	catch (const out_of_range& oor)
	{
		stringstream msg;
		msg << "Out of Range error in " << oor.what() << "; key=" << key;
		LOG_DEBUG( "unk", "inventory", msg.str().c_str() );
	}
	return retVal;
}

bool Inventory::SetSubSubLocation(string key, string subSubLocation)
{
	m_SubSubLocation[key] = subSubLocation;
	return true;
}

bool Inventory::DeleteSubSubLocation(std::string key)
{
	return (m_SubSubLocation.erase(key) == 1);
}

///----------------------------------------------------------------------
/// 	UnitCount
///----------------------------------------------------------------------
int Inventory::GetUnitCount(string key)
{
	int retVal = 0;
	try
	{
		retVal = m_unitCount.at(key);
	}
	catch (const out_of_range& oor)
	{
		stringstream msg;
		msg << "Out of Range error in " << oor.what() << "; key=" << key;
		LOG_DEBUG( "unk", "inventory", msg.str().c_str() );
	}
	return retVal;
}
bool Inventory::SetUnitCount(string key, int count)
{
	m_unitCount[key] = count;
//	cout << "Loc[" << key << "] = " << m_unitCount[key] << endl;
	return true;
}
bool Inventory::DeleteUnitCount(std::string key)
{
	return (m_unitCount.erase(key) == 1);
}

list<string> Inventory::GetFilteredIdList( string & primaryLocation,
												string & subLocation,
												string & subSubLocation )
{
	list<string> ids;
	for (map<string, string>::iterator it = m_Location.begin(); it != m_Location.end(); it++)
	{
		string id = it->first;
		string Loc = it->second;
		if (Loc == "") cout << "ERROR! Inventory contains empty location for plate ID " << id << endl;
		string subLoc = GetSubLocation( id );
		if (subLoc == "") cout << "ERROR! Inventory contains empty sub-location for plate ID " << id << endl;
		string subSubLoc = GetSubSubLocation( id );
		if (subSubLoc == "") cout << "ERROR! Inventory contains empty sub-sub-location for plate ID " << id << endl;

		if ( ( (Loc == primaryLocation) || (primaryLocation == "") ) &&
			 ( (subLoc == subLocation) || (subLocation == "") ) &&
			 ( (subSubLoc == subSubLocation) || (subSubLocation == "") ) )
		{
			ids.push_back( id );
		}
	}
	return ids;
}

