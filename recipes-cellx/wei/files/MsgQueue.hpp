#ifndef _MSG_QUEUE_H
#define _MSG_QUEUE_H

#include <queue>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <string>
#include "main.hpp"

extern std::mutex g_holdMutex;
extern std::condition_variable_any g_holdCVA;
extern long g_numWorkMsgs;
extern std::mutex g_numWorkMsgsMutex;

struct WorkMsg
{
	/// Constructor
	WorkMsg(std::string payloadInJson, std::string originTopic, std::string destinationTopic);
	WorkMsg(const char* payloadInJson, const char* originTopic, const char* destinationTopic);
	WorkMsg(std::string payloadInJson, std::string originTopic, std::string destinationTopic,
			const char* messageType, const char* subMessageType, bool theEnd);
	/// Copy constructor
	WorkMsg( const WorkMsg& workMsg );
	
	/// Destructor
	~WorkMsg();
	
	std::string payload; // TODO: Consider changing terminology so this is mqttPayload and the other is desPayload.
	std::string origin;
	std::string destination;
	std::string msgType;
	std::string subMsgType;
	unsigned char * blob;
	unsigned long blobSize;
	bool theEndIsNow;
};

class MsgQueue
{
protected:
    std::queue<std::shared_ptr<WorkMsg>> msg_queue;
    std::mutex msg_mutex;
    std::condition_variable msg_available;
    std::atomic<bool> isWaitingOnQueue;
	const char * m_queueName;

public:

    /// Constructor
    MsgQueue(const char * queueName);
    
    /// Destructor
    ~MsgQueue();
    
    /// Add a message to the queue in a thread-safe way.
    void PushMsg(std::shared_ptr<WorkMsg>);
    
    /// One way to pop a msg. Other ways may be needed.
    std::shared_ptr<WorkMsg> WaitAndPopMsg();
	
	/// Pop and delete all pending messages.
	void DiscardAllMsgs();
	
	bool IsOnHold();
	bool IsWaitingOnQueue();
	int GetQueueSize();
	bool IsOnBackOfQueue( std::string & msgType );
};

class HoldableMsgQueue : public MsgQueue
{
	HoldType m_previousHoldState;

public:
    /// Constructor
    HoldableMsgQueue(const char * queueName);
    
    /// Destructor
    ~HoldableMsgQueue();
    
    using MsgQueue::PushMsg;
	using MsgQueue::DiscardAllMsgs;
	using MsgQueue::IsWaitingOnQueue;
	using MsgQueue::GetQueueSize;
    
    /// One way to pop a msg. Other ways may be needed.
    std::shared_ptr<WorkMsg> WaitAndPopMsg();
};

#endif 

