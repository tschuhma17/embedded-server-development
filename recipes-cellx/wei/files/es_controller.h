///----------------------------------------------------------------------
/// ES-CONTROLLER
/// This file is meant to be a place to record constants that are
/// defined in the Parker Motion Manager project files. The embedded
/// server needs to know many of these constants.
///----------------------------------------------------------------------
#ifndef _ES_CONTROLLER_H
#define _ES_CONTROLLER_H

#define A_CONTROLLER				(0) // TODO: Make sure this is always 0 on release.
#define B_CONTROLLER				(1) // TODO: Make sure this is always 1 on release.
#define C_CONTROLLER				(2) // TODO: Make sure this is always 2 on release.

#define MAX_SAFE_VOLUME_UL			(2500000.0)

#define MAX_LOOPS_AWAITING_MOTION_START	(5000)
#define MAX_LOOPS_AWAITING_MOTION_END	(50000)
#define MAX_STRIKES					(100)
#define MAX_STRIKES_TWO_AXES		(500)
#define MAX_STRIKES_LINEAR_AXIS		(500)

/// P numbers from ACR74V-A_prog0, ACR74T-B_prog0, and ACR74T-C_prog
#define ROUTINE_NUMBER_SN			(0) /// Input - Select Routine to run
#define AXIS_NUMBER_SN				(1) /// Variable - Axis Number for routines
#define COUNTER_SN					(2) /// Variable - Counter variable
#define HOME_OFFSET_SN				(3) /// 0mm Input - Incremental Home offset position
#define HOME_MSEEK_DIST				(4) /// Input - Max Incremental distance to find Z-Marker
#define CLR_KILLS_TIME_OUT_SN		(5) /// 5 Input - Clear Kill Counter Time out
#define COORD_MOVE_ACCEL_SN			(6) /// 500mm/sec^2 Input - Acceleration for coordinated moves
#define COORD_MOVE_DECEL_SN			(7) /// 500mm/sec^2 Input - Deceleration for coordinated moves
#define COORD_MOVE_VEL_SN			(8) /// 25mm/sec^2 Input - Velocity for coordinated moves
#define COORD_MOVE_STP_SN			(9) /// 500mm/sec^2 Input - Stop Ramp Deceleration for coordinated moves
#define HOME_FINAL_VEL_SN			(10) /// 0.5mm/sec Input - Final Velocity for homing
#define JOG_ACCEL_SN				(11) /// 250mm/sec^2 Input - Acceleration for Jog moves
#define JOG_DECEL_SN				(12) /// 250mm/sec^2 Input -  for Jog moves
#define JOG_VEL_SN					(13) /// 25mm/sec Input - Velocity for Jog moves
#define JOG_HLDEC_SN				(14) /// 0mm/sec^2 Input - Hardware Limit Deceleration for Jog moves
#define NUMBER_OF_SAMPLES			(15) /// Number of Sample to capture using SAMP
#define SAMPLE_PERIOD_SN			(16) /// Sample period for SAMP
#define CAPTURE_DATA_SN				(17) /// Enable SAMP capture
#define CAPTURE_TIME_SN				(18) /// Amount of time to capture SAMP

/// P numbers from ACR74V-A_prog0
#define HOME_Y_STATUS_SN			(100) /// 0 = IN PROCESS; 1 = FAILED; 2 = COMPLETED
#define HOME_X_STATUS_SN			(101) /// 0 = IN PROCESS; 1 = FAILED; 2 = COMPLETED
#define HOME_XX_STATUS_SN			(102) /// 0 = IN PROCESS; 1 = FAILED; 2 = COMPLETED
#define HOME_YY_STATUS_SN			(103) /// 0 = IN PROCESS; 1 = FAILED; 2 = COMPLETED
#define FIND_NEEDLE_Y_STATUS_SN		(104) /// 0 = IN PROCESS; 1 = FAILED; 2 = COMPLETED
#define FIND_NEEDLE_X_STATUS_SN		(105) /// 0 = IN PROCESS; 1 = FAILED; 2 = COMPLETED
#define EXCESS_ERROR_Y_SN			(106) /// 5mm - Input - Maximum Allowable Position Error
#define EXCESS_ERROR_X_SN			(107) /// 5mm - Input - Maximum Allowable Position Error
#define EXCESS_ERROR_XX_SN			(108) /// 5mm - Input - Maximum Allowable Position Error
#define EXCESS_ERROR_YY_SN			(109) /// 5mm - Input - Maximum Allowable Position Error
#define NEEDLE_Y_POS_SN				(110) /// Output - Absolute position found in mm
#define NEEDLE_X_POS_SN				(111) /// Output - Absolute position found in mm
#define NEEDLE_Y_MAX_POS_SN			(112) /// 10mm Output - Max incremental search distance
#define NEEDLE_X_MAX_POS_SN			(113) /// 10mm Output - Max incremental search distance
#define IN_POSITION_BAND_Y_SN		(114) /// 1mm - IPB In-Position Band
#define IN_POSITION_BAND_X_SN		(115) /// 1mm - IPB In-Position Band
#define IN_POSITION_BAND_XX_SN		(116) /// 1mm - IPB In-Position Band
#define IN_POSITION_BAND_YY_SN		(117) /// 1mm - IPB In-Position Band

/// Values for ROUTINE_NUMBER_SN from ACR74V-A_prog0, ACR74T-B_prog0, and ACR74T-C_prog0
#define SETUP_ROUTINE				(0)
#define CLEAR_KILLS_ROUTINE			(1)
#define GET_CELLX_VERSION_ROUTINE	(2)
#define GET_SERVICES_INFO_ROUTINE	(3)
#define GET_HOME_ORDER_INFO_ROUTINE	(4)
#define GET_PROG_ZERO_INFO_ROUTINE	(5)
/// Values for ROUTINE_NUMBER_SN from ACR74V-A_prog0
#define HOME_Y_ROUTINE				(100)
#define HOME_X_ROUTINE				(101)
#define HOME_XX_ROUTINE				(102)
#define HOME_YY_ROUTINE				(103)
#define FIND_NEEDLE_Y_POS_ROUTINE	(104)
#define FIND_NEEDLE_X_POS_ROUTINE	(105)
#define ZERO_ADC_Z_ROUTINE			(106)
#define ZERO_ADC_ZZ_ROUTINE			(107)

/// P numbers from ACR74T-B_prog0
#define HOME_SH_STATUS_SN			(200) /// 0 = IN PROCESS; 1 = FAILED; 2 = COMPLETED
#define HOME_Z_STATUS_SN			(201) /// 0 = IN PROCESS; 1 = FAILED; 2 = COMPLETED
#define HOME_ZZ_STATUS_SN			(202) /// 0 = IN PROCESS; 1 = FAILED; 2 = COMPLETED
#define HOME_ZZZ_STATUS_SN			(203) /// 0 = IN PROCESS; 1 = FAILED; 2 = COMPLETED
#define MOVE_TO_FORCE_Z_STATUS_SN	(204) /// 0 = IN PROCESS; 1 = FAILED; 2 = COMPLETED
#define MOVE_TO_FORCE_ZZ_STATUS_SN	(205) /// 0 = IN PROCESS; 1 = FAILED; 2 = COMPLETED
#define EXCESS_ERROR_SH_SN			(206) /// 2mm Input - Maximum Allowable Position Error
#define EXCESS_ERROR_Z_SN			(207) /// 2mm Input - Maximum Allowable Position Error
#define EXCESS_ERROR_ZZ_SN			(208) /// 2mm Input - Maximum Allowable Position Error
#define EXCESS_ERROR_ZZZ_SN			(209) /// 2mm Input - Maximum Allowable Position Error
#define Z_POSITION_SN				(210) /// Variable - Temp Z Position for _FindBottom and _PressNeedleOn
#define ZZ_POSITION_SN				(211) /// Variable - Temp ZZ Position for _FindBottom and _PressNeedleOn
#define Z_MAX_POSITION_SN			(212) /// Input - Absolute maximum allowable position in mm 
#define ZZ_MAX_POSITION_SN			(213) /// Input - Absolute maximum allowable position in mm 
#define MOVE_TO_FORCE_N_SN			(214) /// Input - Max Force to find needle in Newtons, value must be negative
#define FORCE_POSITION_SN			(215) /// Output - Position at MoveToForceN in mm
#define FORCE_RAW_SN				(216) /// Variable - Calcultion for _MoveToForce
#define IN_POSITION_BAND_SH_SN		(217) /// 1mm - IPB In-Position Band
#define IN_POSITION_BAND_Z_SN		(218) /// 1mm - IPB In-Position Band
#define IN_POSITION_BAND_ZZ_SN		(219) /// 1mm - IPB In-Position Band
#define IN_POSITION_BAND_ZZZ_SN		(220) /// 1mm - IPB In-Position Band

/// Values for ROUTINE_NUMBER_SN from ACR74T-B_prog0
#define HOME_SH_ROUTINE				(200)
#define HOME_Z_ROUTINE				(201)
#define HOME_ZZ_ROUTINE				(202)
#define HOME_ZZZ_ROUTINE			(203)
#define MOVE_TO_FORCE_Z_ROUTINE		(204)
#define MOVE_TO_FORCE_ZZ_ROUTINE	(205)
#define CONFIG_SCANNER_ROUTINE		(206)

/// Values for ROUTINE_NUMBER_SN from ACR74T-C_prog0
#define HOME_PN_ROUTINE				(300)
#define HOME_PP_ROUTINE				(301)
#define HOME_PPP_ROUTINE			(302)
#define HOME_PPPP_ROUTINE			(303)

#define INVALID_SPEED				(-1)
#define MAX_WAIT_FOR_START_LOOPS	(50)
#define MAX_WAIT_FOR_END_LOOPS		(50000)

#endif 

