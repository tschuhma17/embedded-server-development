///----------------------------------------------------------------------
/// PlateType.hpp
/// Defines data structures used for handling plates.
///----------------------------------------------------------------------
#ifndef _PLATE_TYPE_H
#define _PLATE_TYPE_H

#include "json.h"
#include "es_des.h"
#include "enums.hpp"


///----------------------------------------------------------------------
/// PlateType
/// Used for the following commands:
/// MoveXYToOpticalEyeNMA1Notation,
/// MoveXYToOpticalEyeNMRCNotation,
/// MoveXYToTipWorkingLocationNMA1Notation,
/// MoveXYToTipWorkingPositionNMRCNotation,
/// MoveToZTipWellBottomForceNewton,
/// MoveToZZTipWellBottomForceNewton
///----------------------------------------------------------------------
class PlateType
{
public:
    /// Constructor
	PlateType();
	~PlateType();

	int rowCount;
	int columnCount;
	int plateLength;
	int plateWidth;
	int plateHeight;
	int firstWellX;
	int firstWellY;
	int interWellX;
	int interWellY;
	int wellVolume;	// deprecated?
	int workingVolume;
	PlateWellGeometryType wellGeometry;
	int wellInnerDiameter;
	int wellInnerHeight;
	int swirlRadius;
	int swirlVelocity;
	int swirlAcceleration;
	/// Method for Populating PlateType from a json_c object.
	bool PopulateFromJsonCObject( json_object* jsonInObj );
	int GetInt( bool * succeeded, json_object* jsonInObj, const char* name );
}; 

#endif 
