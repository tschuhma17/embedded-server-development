///----------------------------------------------------------------------
/// Inventory
/// This class is a container of current state of the system. It handles
/// serializing its data to/from file.
///
/// Note that this is a singleton, but it is not entirely threadsafe.
/// https://www.aristeia.com/Papers/DDJ_Jul_Aug_2004_revised.pdf
/// explains why.
/// For this reason, we are restricting its use to only one
/// thread: the Command Interpreter.
///----------------------------------------------------------------------
#ifndef _INVENTORY_H
#define _INVENTORY_H

#include <map>
#include <list>
#include <string.h>
#include "Sundry.hpp"

///----------------------------------------------------------------------
/// Inventory
/// Suitable for implementing Plate inventory.
/// Implements storage of location, sub_location, and sub_sub_location
/// tied to unique keys.
///----------------------------------------------------------------------
class Inventory
{
public:
    /// Constructors
    Inventory();
    Inventory( const Inventory& ) = delete;
    Inventory& operator=( const Inventory& ) = delete;
    /// Destructor
    ~Inventory();
	
	std::string GetLocation( std::string key );
	bool SetLocation( std::string key, std::string location );
	bool DeleteLocation( std::string key );
	void PrintLocations();
	
	std::string GetSubLocation( std::string key );
	bool SetSubLocation( std::string key, std::string subLocation );
	bool DeleteSubLocation( std::string key );
	
	std::string GetSubSubLocation( std::string key );
	bool SetSubSubLocation( std::string key, std::string subSubLocation );
	bool DeleteSubSubLocation( std::string key );

	int GetUnitCount( std::string key );
	bool SetUnitCount( std::string key, int count );
	bool DeleteUnitCount( std::string key );

	std::list<std::string> GetFilteredIdList( std::string & primaryLocation,
												   std::string & subLocation,
												   std::string & subSubLocation );

    void ReadFromFiles(const char* dirPath);
    bool StoreToFiles(const char* dirPath);
    void InitializeWithDefaultValues();

private:
	void ReadFromStringFile( const std::string & filePath, std::map<std::string, std::string> & strMap );
	void ReadFromIntFile( const std::string & filePath, std::map<std::string, int> & intMap );
	bool StoreToStringFile( const std::string & filePath, std::map<std::string, std::string> & strMap );
	bool StoreToIntFile( const std::string & filePath, std::map<std::string, int> & intMap );
    std::string m_dirPath;

	std::map<std::string, std::string> m_Location;
	std::map<std::string, std::string> m_SubLocation;
	std::map<std::string, std::string> m_SubSubLocation;
	std::map<std::string, int> m_unitCount;
};

/// The lifetimes of these should be managed only by the Doler thread for CxPM
extern Inventory g_plateInventory;
extern Inventory g_tipInventory;

#endif 
