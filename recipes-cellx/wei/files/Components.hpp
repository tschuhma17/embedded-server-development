///----------------------------------------------------------------------
/// Components.hpp
/// Defines data structures used for storing CPD data to file in a
/// binary format.
///----------------------------------------------------------------------
#ifndef _COMPONENTS_H
#define _COMPONENTS_H

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>
#include "json.h"
#include "es_des.h"
#include "enums.hpp"

#define MAX_COMPONENT_STRING_LENGTH		256 // TODO: Seek a spec.

// TODO: Newer versions of the json-c library provide a
// json_object_get_uint64() function. Until that rev becomes
// available, we'll muddle along with this.
unsigned long json_object_get_uint64( json_object* obj );
json_object *json_object_new_uint64( unsigned long value );


enum class FieldState {
	OK,
	Missing,
	OutOfRange,
	Extra,
	Undefined
};

FieldState CheckStringAsFloat( std::string& value );
FieldState CheckStringAsInt16( std::string& value );
FieldState CheckStringAsUint16( std::string& value );
FieldState CheckStringAsInt32( std::string& value );
FieldState CheckStringAsUint32( std::string& value );
FieldState CheckStringAsEnumCalType( std::string& value );
FieldState CheckStringAsWhiteLightType( std::string& value );
FieldState CheckStringAsBoolean( std::string value );
FieldState CheckStringAsEnumPumpType( std::string& value );

struct FieldWithMetaData
{
	FieldWithMetaData();
	FieldWithMetaData(  std::string nm,
						EditableType ed,
						PersistType pst,
						std::string dt,
						std::string bu,
						std::string mi,
						std::string ma,
						std::string val,
						std::string sc,
						std::string ppd,
						std::string ddir,
						std::string dmi,
						std::string dma );
	FieldWithMetaData(  std::string nm,
						EditableType ed,
						PersistType pst,
						std::string dt,
						std::string bu,
						std::string mi,
						std::string ma,
						std::string val,
						std::string sc,
						std::string ppd,
						std::string ddir,
						std::string dmi,
						std::string dma,
						std::string anm,
						int ai );
    FieldWithMetaData(const FieldWithMetaData& f); /// Copy constructor

	/// Value and metadata to be serialized and used.
	std::string name;				// a key string
	EditableType editable;			// "editability_type"
	PersistType persist;			// "persist_type"
	std::string dataType;			// "data_type"
	std::string baseUnit;			// "base_unit"
	std::string min;				// "es_min_value"
	std::string max;				// "es_max_value"
	std::string setting;			// "setting"
	std::string scale;				// "scale_factor"
	std::string pointsPastDecimal;	// "points_past_decimal"
	std::string displayDirective;	// "display_directive"
	std::string displayMin;			// "display_min_value"
	std::string displayMax;			// "display_max_value"

	/// Fields used only by components that have arrays of fields
	std::string arrayName;
	int arrayIndex;
	/// Meta-metadata, which is transient
	/// NOTE that we only have state variables for subset of fields that are required for ES operation.
	/// Extra fields (like display_directive) are just records maintained and used by external applications.
	FieldState valueState;
	FieldState editableState;
	FieldState persistState;
	FieldState dataTypeState;
	FieldState minState;
	FieldState maxState;
	/// Various accessor functions
	long GetValueAsLong(void);
	unsigned long GetValueAsULong(void);
	float GetValueAsFloat(void);
	bool GetValueAsBool(void);
	static json_object * FieldToJsonObject( const std::string & field, const std::string & dataType );
	static json_object * FieldToJsonObjectVerbatim( const std::string & field );
	bool PropertyFieldsToJsonObject(json_object* jsonOutObj); /// Used for GetProperties command.
};
std::ostream& operator << (std::ostream& os, const FieldWithMetaData& f);
std::istream& operator >> (std::istream& is, FieldWithMetaData& f);
bool operator==( const FieldWithMetaData& lhs, const FieldWithMetaData& rhs );

///----------------------------------------------------------------------
/// ComponentProperties
///----------------------------------------------------------------------
struct ComponentProperties
{
	ComponentType componentType;
	std::vector<FieldWithMetaData> fieldVector;
	std::string fileName;
	std::string filePath;
	/// Used for access by field name
	FieldWithMetaData GetFieldByName( std::string& nameIn );
	FieldWithMetaData GetFieldByName( const char * nameIn );
	void SetField( const FieldWithMetaData fieldIn );
	static std::string ComposeCpdFileName( ComponentType componentType, unsigned long cpdId );
	static std::vector<unsigned long> DetectListOfCpds( std::string & directoryPath, ComponentType componentType ); 

};
std::ostream& operator << (std::ostream& os, const ComponentProperties& p);
std::istream& operator >> (std::istream& is, ComponentProperties& p);

extern ComponentProperties defaultCxD;
extern ComponentProperties defaultAxesXY;
extern ComponentProperties defaultAxisXX;
extern ComponentProperties defaultAxisYY;
extern ComponentProperties defaultAxisZ;
extern ComponentProperties defaultAxisZZ;
extern ComponentProperties defaultAxisZZZ;
extern ComponentProperties defaultAspirationPump1;
extern ComponentProperties defaultDispensePump1;
extern ComponentProperties defaultDispensePump2;
extern ComponentProperties defaultDispensePump3;
extern ComponentProperties defaultPickingPump1;
extern ComponentProperties defaultWhiteLightSource;
extern ComponentProperties defaultFluorescenceLightFilter;
extern ComponentProperties defaultAnnularRingHolder;
extern ComponentProperties defaultOpticalColumn;
extern ComponentProperties defaultRetigaR3Camera;
extern ComponentProperties defaultBaslerCamera;
extern ComponentProperties defaultIncubator;
extern ComponentProperties defaultCxPM;
extern ComponentProperties defaultBarcodeReader;
extern ComponentProperties defaultBaslerCamera;

#endif 
