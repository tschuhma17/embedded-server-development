///----------------------------------------------------------------------
/// MqttReceiver (Message Queue Telemetry Transport Receiver)
/// This file implements a thread, which performs the following jobs.
/// 1. Register with an MQTT broker for ES messages per the "Embedded
///    Server Data Exchange Specification (ES-DES).
/// 2. Receive all MQTT messages intended for the embedded server.
/// 3. Validate all received messages and accept or reject them.
/// 4. Create acknowledgement messages.
/// 5. Forward acknowledgements directly to the MqttSender thread.
/// 6. Forward most other messages to the command interpreter thread.
/// 7. Divert all microscope-specific commands to different topics,
///    to be handled by software outside the embedded server.
///----------------------------------------------------------------------
#ifndef _MQTT_RECEIVER_THREAD_H
#define _MQTT_RECEIVER_THREAD_H


#include <thread>
#include <string>
#include <list>
#include <vector>
#include "MsgQueue.hpp"
#include "DesHelper.hpp"
#include "Sundry.hpp"
#include "uuid/uuid.h"

#define RECEIVER_CLIENTID			"ES-MQTT-receiver-task"
#define RECEIVER_BROKER_PORT		"1883"
#define RECEIVER_KEEP_ALIVE_INTERVAL 120
#define RECEIVER_CLEAN_SESSION    1
#define RECEIVER_QOS              2
#define TIMEOUT          10000L
#define MOCK_READINESS_DELAY_AFTER_BOOT_SECONDS	60

struct outBoxMsgQueues {
	MsgQueue *commandOutBox;
	MsgQueue *ackOutBox;
    /// Not queues, but needed just as much, for managing client registrations
    std::string machineId;
    std::string masterId;
    std::vector<std::string> observerIdVector;

    const char* m_threadName;
	/// For managing hold state
    HoldType * esHoldState;
    std::mutex * esHoldMutex;
	/// For telling Main to flush queues.
	bool * esDoFlushQueues;
	std::mutex * esDoFlushMutex;
	time_t box_opening_time_seconds;
};

class MqttReceiverThread
{
public:
    /// Constructor
    MqttReceiverThread(const char* threadName);

    /// Destructor
    ~MqttReceiverThread();

    /// Called once to create the worker thread
    /// @return True if thread is created. False otherwise. 
    bool CreateThread();

    /// Called once a program exits, to exit the worker thread.
    void ExitThread();
    
    /// Called once to connect the queue that handles incoming commands.
    void AttachCommandOutBox(MsgQueue *q);
    
    /// Called once to connect the queue for sending ACKs back to the broker.
    void AttachAckOutBox(MsgQueue *q);
    
    /// Called once to enable connecting to the MQTT broker.
    void SetBrokerIpAddress(std::string ipAddress);
    
    /// Called once to configure the machine ID.
    void SetMachineId(std::string id);
    
    /// Called once to configure the master ID.
    /// This identifies which outside MQTT client is the master computer.
    void SetMasterId(std::string id);

    void SendDisconnectAlert(void);
    
private:
    MqttReceiverThread(const MqttReceiverThread&) = delete;
    MqttReceiverThread& operator=(const MqttReceiverThread&) = delete;

    /// Entry point for the worker thread
    void Process();
    bool m_continueRunning;

/* Delay this work until MqttReceiver is refactored to better handle different message types.
	/// Let us manage Calibration and Maintentenance logs in MqttReceiver to avoid the
	/// overhead of routing them to other threads.
	bool Fulfill_AppendCalibrationLog();
	bool Fulfill_GetCalibrationLog();
	bool Fulfill_ResetCalibrationLog();
	bool Fulfill_AppendMaintenanceLog();
	bool Fulfill_GetMaintenanceLog();
	bool Fulfill_ResetMaintenanceLog();
	/// MqttReceiver is the logical place for maintaining Connection logs.
	bool AppendConnectionLog( std::chrono::system_clock::rep timestamp,
							  std::string client_id,
							  std::string connection_request_type,
							  std::string connection_result );
	bool Fulfill_GetConnectionLog();
	bool Fulfill_ResetConnectionLog();
*/

	/// For inter-thread communications
    std::unique_ptr<std::thread> m_thread;
	outBoxMsgQueues m_outBoxQueues;
	
	/// For MQTT communications
    std::string m_brokerIpAddress;
    std::list<std::string> m_listOfTopics;
    DesHelper m_desHelper;
};

#endif 

