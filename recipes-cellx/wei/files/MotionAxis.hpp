/* MotionAxis.hpp */

/** TODO: Add Cell-X copyright notice */

#ifndef __MotionAxis_hpp__
#define __MotionAxis_hpp__

#include <cstdint>
#include <vector>

#include "USAFW/util/MeasurementUnit.hpp"

namespace USAFW {

    // Forward-reference
    class ACR7000;

    // Forward-reference
    class Transport;


    class MotionAxis {
    public:
        // Constructor
        MotionAxis(ACR7000 &controllerP, const std::string nameP, int channelIndexP, int masterP);

        // Destructor
        virtual ~MotionAxis();

        // Gets the name of the transport, as specified in the configuration file.
        inline const std::string &getName(void) const { return m_name; }

        // Gets the channel index of the transport, as specified in the configuration file.
        // The channel index is the electrical connection designator for the controller.
        inline int getChannelIndex(void) const { return m_channel_index; }

        // Joins a transport.
        //
        // THROWS
        // - BadState if the axis is already part of a transport.
        void join(Transport &transportP);

        // Removes the axis's link to the transport it is currently a member of.
        // Called by the Transport class during its destruction.
        //
        // THROWS
        // - BadState if the axis is not part of a transport.
        void leave(void);

        // == QUERIES ==
        //double getAbsolutePosition(const MeasurementUnit &unitsP, int timeout_msP);
        long getMasterFlags(void);
        long getAxisFlags(void);
        long getQuaternaryAxisFlags(void);
        long getQuinaryAxisFlags(void);
        long getAbsolutePosition(void);
        long getCurrentPosition(void);
        long getActualPosition(void);
        double getCurrentVelocity(void);
        int getMasterIndex(void);
		int getChannelIndex(void);
        double getPPU(void);
        bool isEnabled(void);
		bool killsAreCleared(void);
		bool isInMotion(void);
		bool isInPositionBand(void);

        // == OPERATIONS ==
        bool killAllMotion(void);
		bool clearKills(void);

        // Enables the axis.
        void enable(void);
        void disable(void);

        // Begins a move to an absolute position.
        //bool absoluteMove(double positionP, const MeasurementUnit &unitsP, int timeout_msP, uint32_t sequence_idP, uint32_t &op_snR);
        void absoluteMove(double positionP);
        
        double getCachedPPU(void);

    protected:
        // Reference to the motion controller that runs the motors on this axis.
        ACR7000             &m_controller;

        // Name of the axis, as specified in the configuration file.  Locked after construction.
        const std::string   m_name;

        // Controller channel index, as specified in the configuration file.  Locked after construction.
        int                 m_channel_index;

        // Master index is logical assignment programmed into the controller.
        int                 m_master_index;

         // Pointer to the transport this axis is a member of, if any.
         Transport          *p_transport;
         
         /// Static values to be queried from the controller once during initialization.
         double				m_ppu;	/// The axis pulse per unit ratio
    };

}

#endif
