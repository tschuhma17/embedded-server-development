#include <sstream>
#include "TipType.hpp"

using namespace std;

/// Constructors
TipType::TipType() : tipTotalLength(0), tipWorkingLength(0), tipInnerDiameterAtInsertionEnd(0),
	tipInnerDiameterAtWorkingEnd(0), tipMaximumWorkingVolume(0), tipRackRowCount(0),
	tipRackColumnCount(0), tipRackLength(0), tipRackWidth(0), tipRackHeight(0), tipRackSeatToTipSeat(0),
	tipRackSeatToTopOfTipRack(0), tipRackSeatToTopOfTip(0), firstTipX(0), firstTipY(0), interTipX(0),
	interTipY(0), usedTipCount(0)
{
}

TipType::~TipType()
{
}

bool TipType::PopulateFromJsonCObject( const char * threadName, json_object* jsonInObj )
{
	bool succeeded = true;
	tipTotalLength = GetInt( threadName, &succeeded, jsonInObj, TIP_TOTAL_LENGTH );
	tipWorkingLength = GetInt( threadName, &succeeded, jsonInObj, TIP_WORKING_LENGTH );
	tipShoulderLength = GetInt( threadName, &succeeded, jsonInObj, TIP_SHOULDER_LENGTH );
	tipInnerDiameterAtInsertionEnd = GetInt( threadName, &succeeded, jsonInObj, TIP_INNER_DIAMETER_AT_INSERTION_END );
	tipInnerDiameterAtWorkingEnd = GetInt( threadName, &succeeded, jsonInObj, TIP_INNER_DIAMETER_AT_WORKING_END );
	tipMaximumWorkingVolume = GetInt( threadName, &succeeded, jsonInObj, TIP_MAXIMUM_WORKING_VOLUME );
	tipRackRowCount = GetInt( threadName, &succeeded, jsonInObj, TIP_RACK_ROW_COUNT );
	tipRackColumnCount = GetInt( threadName, &succeeded, jsonInObj, TIP_RACK_COLUMN_COUNT );
	tipRackLength = GetInt( threadName, &succeeded, jsonInObj, TIP_RACK_LENGTH );
	tipRackWidth = GetInt( threadName, &succeeded, jsonInObj, TIP_RACK_WIDTH );
	tipRackHeight = GetInt( threadName, &succeeded, jsonInObj, TIP_RACK_HEIGHT );
	tipRackSeatToTipSeat = GetInt( threadName, &succeeded, jsonInObj, TIP_RACK_SEAT_TO_TIP_SEAT );
	tipRackSeatToTopOfTipRack = GetInt( threadName, &succeeded, jsonInObj, TIP_RACK_SEAT_TO_TOP_OF_TIP_RACK );
	tipRackSeatToTopOfTip = GetInt( threadName, &succeeded, jsonInObj, TIP_RACK_SEAT_TO_TOP_OF_TIP );
	firstTipX = GetInt( threadName, &succeeded, jsonInObj, FIRST_TIP_X );
	firstTipY = GetInt( threadName, &succeeded, jsonInObj, FIRST_TIP_Y );
	interTipX = GetInt( threadName, &succeeded, jsonInObj, INTER_TIP_X );
	interTipY = GetInt( threadName, &succeeded, jsonInObj, INTER_TIP_Y );
	usedTipCount = GetInt( threadName, &succeeded, jsonInObj, USED_TIP_COUNT );

	return succeeded;
}

// TODO: Deprecate and remove.
bool TipType::PopulateFromJsonCObject( json_object* jsonInObj )
{
	return PopulateFromJsonCObject( "unk", jsonInObj );
}

/// A helper for PopulateFromJsonCObject.
int TipType::GetInt( const char * threadName, bool * succeeded, json_object* jsonInObj, const char* name )
{
	json_object* temp = json_object_object_get( jsonInObj, name );
	if (temp == nullptr) /// Name not found.
	{
		*succeeded = false;
		stringstream msg;
		msg << "tip_type payload is missing " << name;
		LOG_ERROR( threadName, "tip", msg.str().c_str() );
		return 0;
	}
	return json_object_get_int( temp );
}

// TODO: Deprecate and remove.
/// A helper for PopulateFromJsonCObject.
int TipType::GetInt( bool * succeeded, json_object* jsonInObj, const char* name )
{
	json_object* temp = json_object_object_get( jsonInObj, name );
	if (temp == nullptr) /// Name not found.
	{
		*succeeded = false;
		return 0;
	}
	return json_object_get_int( temp );
}

