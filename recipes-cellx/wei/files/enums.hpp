#ifndef _ENUMS_H
#define _ENUMS_H

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>  /// std::setw

/// We'll use a convention of recording string sizes in configuration files as 4-byte strings.
#define STRING_LENGTH_WIDTH 4

enum class ComponentType {
	ComponentUnknown,
	ComponentNone,
	CxD,
	XYAxes,
	XXAxis,
	YYAxis,
	ZAxis,
	ZZAxis,
	ZZZAxis,
	AspirationPump1,
	DispensePump1,
	DispensePump2,
	DispensePump3,
	PickingPump1,
	WhiteLightSource,
	FluorescenceLightFilter,
	AnnularRingHolder,
	Camera,
	OpticalColumn,
	Incubator,
	CxPM,
	BarcodeReader,
	FINAL_COMPONENT_TYPE_ENTRY
};
ComponentType StringNameToComponentTypeEnum( const char * name );
const char*  ComponentTypeEnumToStringName( ComponentType type );

std::ostream& operator << (std::ostream& os, ComponentType const& e);
std::istream& operator >> (std::istream& is, ComponentType& e);

/// These allow us to serialze any class enum to and from an integer value.
template<typename T> std::ostream& operator << (typename std::enable_if<std::is_enum<T>::value, std::ostream>::type& stream, const T& e);
template<typename T> std::istream& operator >> (typename std::enable_if<std::is_enum<T>::value, std::istream>::type& stream, T& e);

enum class EditableType {
	EditableFixed,
	EditableCPDOnly,
	EditableApplicationOnly,
	EditableCPD_Application,
	Unknown
};
EditableType StringNameToEditableTypeEnum( const char * name );
const char* EditableTypeEnumToStringName( EditableType type );

enum class PersistType {
	NotPersist,
	Persist,
	HardPersist,
	Remove,
	Unknown
};
PersistType StringNameToPersistTypeEnum( const char * name );
const char* PersistTypeEnumToStringName( PersistType type );

enum class PlateLocations {
	Unknown,
	OffDeck,
	OnDeck,
	VNPS,
	VNCS,
	NC,
	NS,
	NP,
	PD,
	TS,
	Incubator,
	CxDSite,
	CxPM,
	Discarded,
	AtTaskList,
	OriginalLocation,
	None,
	SameAsPrimary,
	InTransit,
	PreStage
};

enum class LidLocations {
	Unknown,
	CxPM,
	LidStage1,
	LidStage2,
	InTransit
};

enum class TipRackLocations {
	Unknown,
	VTS,
	TipCxPM,
	TipActive,
	TipExtra,
	TipOffDeck,
	TipDiscarded,
	TipInTransit
};

enum class MediaLocations {
	Unknown,
	DispensePump
};

enum class ConnectionRequestType {
	Unknown,
	ReadOnly,
	ReadWrite
};

enum class ConnectionResultType {
	Unknown,
	SuccessfulReadWrite,
	SuccessfulReadOnly,
	DeniedAlreadyHaveReadWrite,
	DeniedInvalidCertificate
};

enum class MaintenanceType {
	Unknown,
	RoutineUser,
	UnplannedUser,
	RoutinePreventative,
	UnplannedPreventative,
	UnplannedService,
	UnplannedReturn
};

enum class CalibrationType {
	Unknown,
	None,
	ZeroOrderPolynomial,
	FirstOrderPolynomialForceZeroIntercept,
	FirstOrderPolynomial,
	SecondOrderPolynomial
};

enum class PlateWellGeometryType {
	Unknown,
	Circular,
	Rectangular
};

enum class PumpType {
	Unknown,
	Peristaltic,
	Syringe
};
PumpType StringNameToPumpTypeEnum( const char * name );
const char* PumpTypeEnumToStringName( PumpType type );

enum class WhiteLightType {
	Unknown,
	Incandescent,
	LED
};

enum class CameraGainType {
	CameraGainContinuous,
	CameraGainIntegral
};

enum DigitalInputType {
	DigitialInput_unknown,
	DigitialInput_1,
	DigitialInput_2,
	DigitialInput_3,
	DigitialInput_4,
	DigitialInput_5,
	DigitialInput_6,
	DigitialInput_7,
	DigitialInput_8
};

enum DigitalOutputType {
	DigitialOutput_unknown,
	DigitialOutput_1,
	DigitialOutput_2,
	DigitialOutput_3,
	DigitialOutput_4,
	DigitialOutput_5,
	DigitialOutput_6,
	DigitialOutput_7,
	DigitialOutput_8
};

enum class ExtractionType {
	SetProperties,
	UploadCpd,
	Unknown
};

enum class SpeedType {
	Speed_undefined,
	Speed_slow,
	Speed_medium,
	Speed_fast
};
SpeedType StringNameToMontageSpeedTypeEnum( const char * name );
const char* SpeedTypeEnumToMontageStringName( SpeedType type );

enum class HardwareType {
	ACR74T,			// a Parker motion controller
	ACR74V,			// a Parker motion controller
	CAVRO_ADP,		// a Tecan pump
	IX85,			// an Olympus combo of optical-column, white-light, fluorescence-light-filter
	LS620,			// an Etaluma combo of optical-column, white-light, fluorescence-light-filter
	R1_R2_R3,		// a Retiga camera
	daA3840_45um,	// a Basler camera
	MATRIX_120,		// a Datalogic barcode reader
	unknown
};
HardwareType StringNameToHardwareTypeEnum( const char * name );
const char*  HardwareTypeEnumToStringName( HardwareType type );

#endif
