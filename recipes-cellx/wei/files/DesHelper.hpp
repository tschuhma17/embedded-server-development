///----------------------------------------------------------------------
/// The DesHelper is a library of (mostly static) functions that do
/// useful tasks, using a strong working knowledge of the "CellX 
/// Embedded Server Data Exchange Specfication (DES)" document.
///
/// This library's entire objective is to encapsulate DES-specific
/// details, so they are all managed in one place. (As much as is
/// practicable.)
///----------------------------------------------------------------------
#ifndef _DES_HELPER_H
#define _DES_HELPER_H

#include <chrono>
#include <memory>
#include <mutex>
#include <string>
#include "enums.hpp"
#include "json.h"
#include "Components.hpp"
#include "TipType.hpp"
#include "PlateType.hpp"
#include "MsgQueue.hpp"

struct NameData
{
	/// Constructor
	NameData(std::string machineId, std::string masterId);
	NameData(const char * machineId, const char * masterId);
	
	std::string m_machineId;
	std::string m_masterId;
};


class DesHelper
{
public:
    /// Constructor
    DesHelper();
    DesHelper(std::string machineId, std::string masterId);

	json_object* ParseJsonDesMessage(const char* jsonDesMessage, bool* success);

    std::string JsonDesObjectToString(json_object* jsonDesObject);

    void SetMachineId(std::string machineId);

	/// Methods for creating JSON payloads to be sent out via MQTT.
	/// Use the function that matches the output channel to be used.
    json_object* ComposeCommand( std::string messageType, int commandGroupId, int commandId, std::string sessionId, json_object * payload );
    json_object* ComposeAck( std::string messageType,
							 int commandGroupId,
							 int commandId,
							 std::string sessionId,
							 std::string status,
							 int statusCode,
							 std::chrono::system_clock::rep timestamp,
							 json_object * payload );
    json_object* ComposeAlert( std::string messageType,
							   int commandGroupId,
							   int commandId,
							   std::string sessionId,
							   ComponentType component,
							   int statusCode,
							   bool thisIsCausingHold );
	bool SendAlert( json_object * alertMsgObj,
					MsgQueue * outBox,
					std::string originalTopic );
	json_object* ComposeStatus( int commandGroupId, int commandId, std::string sessionId, std::string messageType, std::string machineId, int statusCode, json_object * payload );
	std::string ComposeStatusString( int commandGroupId, int commandId, std::string sessionId, std::string messageType, std::string machineId, int statusCode, json_object * payload );

	/// Methods for validating incoming JSON payloads from MQTT.
	/// Use the function that matches the channedl on which it was recevied.
    bool IsValidCommand(json_object* jsonDesObject, std::string & whyInvalid_out);
    bool IsValidAck(json_object* jsonDesObject);
    bool IsValidAlert(json_object* jsonDesObject);
    bool IsValidStatus(json_object* jsonDesObject);
    
    /// Methods for extracting values from objects
    bool SplitCommand(json_object* jsonDesObject,
					  std::string* messageType,
					  std::string* transportVersion,
					  int* commandGroupId,
					  int* commandId,
					  std::string* sessionId,
					  std::string* machineId);
	bool SplitHeader( json_object* headerObj,
					  std::string* cpdFileName,
					  std::string* cpdVersion,
					  std::string* cpdDescription );

	/// Methods for determinging to which hardware component a
	/// JSON message should be routed.
    static bool IsaMicroscopeCommand(std::string messageType);
    static bool IsAnImmediateCommand(std::string messageType);

	/// Methods for creating JSON payloads for sub-messages of compounds.
	static json_object* ComposeSetZToSafeHeightPayload( unsigned long z_safeHeight_nm );
	static json_object* ComposeSetZZToSafeHeightPayload( unsigned long zz_safeHeight_nm );
	static json_object* ComposeSetZZZToSafeHeightPayload( unsigned long zzz_safeHeight_nm );
	static json_object* ComposeMoveToXXStepPayload( unsigned long xxStep );
	static json_object* ComposeMoveToYYStepPayload( unsigned long yyStep );
//	static json_object* ComposeMoveToZTipPickForceNewtonPayload( float slowDownOffsetUm );
//	static json_object* ComposeMoveToZZTipPickForceNewtonPayload( float slowDownOffsetUm );
	static json_object* ComposeSetZAxisSpeedPayload( long zSpeed );
	static json_object* ComposeUploadCpdError( const char * cpdId, const char * syntaxError );
	/// Create a payload full of prog0 input parameters for each of the following axes:
	/// Z, ZZ, ZZZ, AspirationPump1, DispensePump1, DispensePump2, DispensePump3, PickingPump1
	static json_object* ComposeHomeAllSafeHwPayload( const char * threadName,
													 ComponentProperties * propsSH,
													 ComponentProperties * propsZ,
													 ComponentProperties * propsZZ,
													 ComponentProperties * propsZZZ,
													 ComponentProperties * propsAspPump1,
													 ComponentProperties * propsDispPump1,
													 ComponentProperties * propsDispPump2,
													 ComponentProperties * propsDispPump3,
													 ComponentProperties * propsPickPump1 );
	/// Create a payload full of prog0 input parameters for each of the following axes:
	/// Y, X, YY, XX
	static json_object* ComposeHomeAllUnsafeHwPayload( const char * threadName,
													   ComponentProperties * propsXY,
													   ComponentProperties * propsYY,
													   ComponentProperties * propsXX );
	static bool ExtractHomeInputsFromPayload( json_object * payloadObj_in,
											  const char * homeInputsName_in,
											  unsigned long & homeOffset_out,
											  unsigned long & clearKillsTimeout_out,
											  unsigned long & coordMoveAccel_out,
											  unsigned long & coordMoveDecel_out,
											  unsigned long & coordMoveStp_out,
											  unsigned long & coordMoveVel_out,
											  unsigned long & homeFinalVel_out,
											  unsigned long & jogAccel_out,
											  unsigned long & jogDecel_out,
											  unsigned long & jogVel_out,
											  unsigned long & jogHLDC_out,
											  std::string & report );
	static void AddCpdError( json_object** errorList, const char * cpdId, const char * syntaxErrorDescription );
	/// Add the following component properties to payloadObj: set_speed (as VELOCITY), acceleration, and deceleration
	/// (as both DECELERATION and STP). Print an error if the X and Y versions of any of those do not match each other.
	static bool AddXYSpeeds( const char * threadName, json_object * payloadObj_in_out, ComponentProperties * propsXY );
	static bool AddMontageSpeeds( const char * threadName,
								  json_object * payloadObj_in_out,
								  ComponentProperties * propsXY,
								  ComponentProperties * propsCxD,
								  SpeedType speed );
	static bool AddSpeedsAndLimits( const char * threadName, json_object * payloadObj_in_out, const char * axisName, ComponentProperties * props );
	static bool AddFlowRatesAndLimits( const char * threadName_in, json_object * inputMsg_in_out, const char * axisName_in, ComponentProperties * props );
	static bool AddPickingPumpFlowParams( const char * threadName_in, json_object * inputMsg_in_out, const char * axisName_in, ComponentProperties * props );
	static bool ReplaceWithRetractionSpeeds( const char * threadName, json_object * payloadObj_in_out, const char * axisName, ComponentProperties * props );
	static bool AddVendorModel( const char * threadName, json_object * payloadObj_in_out, ComponentType type, ComponentProperties * props );
	static void ComposeAndAddCpdError( FieldWithMetaData* fieldWithMetaData, std::string cpdId, json_object** errorListObj );
	static bool A1ToRc( std::string a1Input, long &rowOutput, long &colOutput );

	std::string ComposeSetZToSafeHeightMessage( json_object* parent, unsigned long z_safeHeight_nm );
	std::string ComposeSetZZToSafeHeightMessage( json_object* parent, unsigned long zz_safeHeight_nm );
	std::string ComposeSetZZZToSafeHeightMessage( json_object* parent, unsigned long zzz_safeHeight_nm );
	std::string ComposeSetZAxisSpeedMessage( json_object* parent, long zSpeed );
	std::string ComposeMoveToZAxisForceNewton( json_object* parent );
	std::string ComposeMessageWithNullPayload( json_object* parent, const char * messageType );
	std::string ComposeMessageWithGivenPayload( json_object* parentObj,
												const char * messageType,
												json_object* payloadObj );

	static CalibrationType IdentifyCalibrationType( const std::string type );
	static std::string CalibrationTypeEnumToStringName( CalibrationType type );
	bool ExtractCreateMontageParameters( const char * threadName,
										 json_object* inputMsg,
										 unsigned long* pixel_count_x,
										 unsigned long* pixel_count_y,
										 unsigned long* X_stepCount,
										 unsigned long* Y_stepCount,
										 long* X_start_nm,
										 long* Y_start_nm,
										 unsigned long* bin_factor,
										 unsigned long* camera_color_depth,
										 unsigned long* scaled_color_depth,
										 float* X_stepIncrementUm,
										 float* Y_stepIncrementUm,
										 float* scanMovementDelayMs,
										 long* velocity,
										 long* acceleration,
										 long* deceleration,
										 long* stp,
										 long* axisResolutionNm );
	bool ExtractTipParameters(  const char * threadName,
								json_object* inputMsg,
								TipType & tipType,
								ComponentType & tipAxis,
								unsigned long & tipNumber );
	bool ExtractNmNotations(    const char * threadName,
								json_object* inputMsg,
								bool forOptical,
								bool useA1Notation,
								long &xOffsetNm,
								long &yOffsetNm,
								long &velocity,
								long &acceleration,
								long &deceleration,
								long &stp,
								long &axis_resolution );
	bool ExtractSpeedsAndLimits(const char * threadName_in,
								json_object* inputMsg_in,
								const char * axisName_in,
								long &velocity_out,
								long &acceleration_out,
								long &deceleration_out,
								long &stp_out,
								long &axis_resolution_out,
								long &soft_travel_limit_plus,
								long &soft_travel_limit_minus );
	bool ExtractMessageIDs(const char * threadName_in,
								json_object* inputMsg_in,
								const char * axisName_in,
								long &command_id_out );
	bool ExtractXYSpeeds(	const char * threadName_in,
							json_object* inputMsg_in,
							const char * axisName_in,
							long &velocity_out,
							long &acceleration_out,
							long &deceleration_out,
							long &stp_out,
							long &axis_resolution_out,
							long &x_soft_travel_limit_plus,
							long &x_soft_travel_limit_minus,
							long &y_soft_travel_limit_plus,
							long &y_soft_travel_limit_minus );
	bool ExtractMontageSpeeds(	const char * threadName_in,
								json_object* inputMsg_in,
								const char * axisName_in,
								long &velocity_out,
								long &acceleration_out,
								long &deceleration_out,
								long &stp_out );
	bool ExtractFlowRatesAndLimits( const char * threadName_in,
									const json_object * inputMsg_in,
									const char * axisName_in,
									float & calCoeff1_out,
									float & calCoeff2_out,
									unsigned long & accelTime_ms_out,
									unsigned long & decelTime_ms_out );

	static void ExtractSimpleField( json_object * fieldObj_in, std::string & fieldStr_out );
	static void ExtractFieldVerbatim( json_object * fieldObj_in, std::string & fieldStr_out );
	bool ExtractProperty( json_object* cpdFieldObj, FieldWithMetaData* field );
	int UpdateProperty( json_object* cpdFieldObj, FieldWithMetaData* field, bool fromCPD );
	void CheckValueDataType( std::string& dataType, std::string& value, FieldState* dataTypeState, FieldState* valueState );
	int StoreComponent_Properties( json_object* inputCPD,
								   ComponentProperties* propsStore,
								   json_object** statusPayloadObj );
	std::string ExtractPayloadStringValue( json_object* jWorkOrderObj, std::string keyName );
	double ExtractPayloadDoubleValue( json_object* jWorkOrderObj, std::string keyName );
	// TODO: Deprecate this and replace it with calls to ExtractLongValue()
	long ExtractPayloadLongValue( json_object* jWorkOrderObj, std::string keyName );
	// TODO: Deprecate this and replace it with calls to ExtractULongValue()
	unsigned long ExtractPayloadULongValue( json_object* jWorkOrderObj, std::string keyName );
	bool ExtractPayloadBooleanValue( json_object* jWorkOrderObj, std::string keyName );
	TipType ExtractPayloadTipType( json_object* jWorkOrderObj );
	TipType ExtractPayloadTipType( const char * threadName, json_object* jWorkOrderObj, bool * succeeded_out );
	PlateType ExtractPayloadPlateType( json_object* jWorkOrderObj );

	bool ExtractLongValue( const char * threadName, json_object* jObj, const char * keyName, long & value_out );
	bool ExtractULongValue( const char * threadName, json_object* jObj, const char * keyName, unsigned long & value_out );
	bool ExtractFloatValue( const char * threadName, json_object* jObj, const char * keyName, float & value_out );
	bool ExtractBoolValue( const char * threadName, json_object* jObj, const char * keyName, bool & value_out );
	bool ExtractStringValue( const char * threadName, json_object* jObj, const char * keyName, std::string & value_out );
	
// TODO: Maybe refactor this to be a method of the ComponentProperties struct
	bool ComponentPropertiesToJsonObject( ComponentProperties * collectionIn, json_object* collectionOutJsonObj );
	
	/// payloadObj is expected to be an object containing a single named object where the name is property type
	/// and the object is a collection of properties.
	int CopyValidPropertiesPayloadFromJsonToWorkingPropertiesContainer( json_object * payloadInObj,
																		ComponentProperties * collectionOut,
																		json_object ** statusPayloadOutOjb,
																		ExtractionType extractionType,
																		bool fromCPD );
// TODO: Maybe refactor this to be a method of the ComponentProperties struct
	/// Called by CopyValidPropertiesPayloadFromJsonToWorkingPropertiesContainer as it iterates through the payload.
	/// collectionInObj is expected to be an object with one or more named objects of the standard parameter type.
    int CopyValidPropertiesFromJsonToWorkingPropertiesContainer( json_object * collectionInObj,
																  ComponentProperties * collectionOut,
																  json_object ** statusPayloadOutOjb,
																  ExtractionType extractionType,
																  bool fromCPD );
// TODO: Maybe refactor this to be a method of the ComponentProperties struct
	int SavePropertyArrayToPropertiesCollection( const char * arrayName,
												   json_object * arrayObj,
												   ComponentProperties * propertiesOut,
												   json_object ** statusPayloadOutObj,
												   ExtractionType extractionType,
												   bool fromCPD );
// TODO: Maybe refactor this to be a method of the ComponentProperties and/or FieldWithMetaData structs
	int SavePropertyObjectToPropertiesCollection( const char * propertyName,
												   json_object * propertyObj,
												   ComponentProperties * propertiesOut,
												   json_object ** statusPayloadOutObj,
												   ExtractionType extractionType,
												   const char * arrayName,
												   int arrayIndex,
												   bool fromCPD );
	static bool IsTecanAdpDetect( const std::string vendor, const std::string model );
	/// Destructor
    ~DesHelper();


private:
    DesHelper(const DesHelper&) = delete;
    DesHelper& operator=(const DesHelper&) = delete;
    
	bool CheckResult( int result, const char* fieldName, const char* channelType );
	bool ValidateTransportVersion( const char* transportVersion );

    std::shared_ptr<NameData> m_names;
};

#endif 

