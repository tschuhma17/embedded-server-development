///----------------------------------------------------------------------
/// CommandDolerThread takes polished JSON commands from the
/// synchronous command queueu and DOLES THEM OUT to the devices
/// that can get them done.
/// When a command gets done, this thread creates a status message,
/// which it sends to the MQTT Sender Task via the ack-and-status-
/// queue.
///----------------------------------------------------------------------
#ifndef _COMMAND_DOLER_H
#define _COMMAND_DOLER_H


#include <thread>
#include <vector>
#include <map>
#include "MsgQueue.hpp"
#include "DesHelper.hpp"
#include "Sundry.hpp"
#include "Inventory.hpp"
#include "json.h"
//#include "es-camera.h" /// base class for camera support.
#include "camera_retiga.hpp" /// support for Retiga R3 camera.
#include "MqttHardwareIO.hpp" // support of Olympus optical column and fluorescence light filter.
#include "USAFW/util/Lockable.hpp"
#include "USAFW/exceptions/LogicExceptions.hpp"
#include "exceptions/MotionException.hpp"
#include "MotionServices.hpp"
#include "controllers/ACR7000.hpp"
#include "MotionAxis.hpp"
#include "CppLinuxSerial/SerialPort.hpp"
#include "Tecan_ADP.hpp"

#define INITIALIZATION_TIMEOUT_MS   (5000)
#define ASYNC_COMMAND_TIMEOUT_MS    (3000)
#define BLOCKING_COMMAND_TIMEOUT_MS (5000)

class CommandDolerThread
{
public:
	/// Constructor
	CommandDolerThread(const char* threadName);

	/// Destructor
	~CommandDolerThread();

	/// Called once to create the worker thread
	/// @return True if thread is created. False otherwise. 
	bool CreateThread();

	/// Called once a program exit to exit the worker thread
	void ExitThread();
	
	void AttachInBox( MsgQueue *q );
	
	void AttachOutBox( MsgQueue *q );

	void SetMachineId( std::string machineId );
	void SetBrokerIpAddress( std::string ipAddress );
	
	/// For communicating with the TecanADP and ...
	mn::CppLinuxSerial::SerialPort * m_serialPort;
	TecanADP * m_tecanADP;

private:
	CommandDolerThread( const CommandDolerThread& ) = delete;
	CommandDolerThread& operator=( const CommandDolerThread& ) = delete;

	/// Used only by the paparazzi Doler thread.
	bool InitPVCamComms ( std::string &msg );
	bool ReleasePVCamComms ( std::string &msg );

	int DoWork( std::string taskAtHand, int *commandId, int *commandGroupId, std::string *sessionId );
	void FreezeWorkInProgress( void );
	bool ConfirmThreadIdentity( const char * threadName, const char * command );

	void InitializeWorkingData( void );
	bool WriteToLogFile( std::string & logFileName, std::string & content );

	/// Entry point for the worker thread
	void Process();
	///
	/// Let's make CommandDoler be in charge of file I/O for the Hardware Events and Alarms
	/// logs. This thread is the one most likely to be aware of hardware events and alarms.
	///
	/// Called by DoWork().
	/// Changes the embedded server's internal clock.
	int Fulfill_SynchronizeTime();
	/// Called by DoWork().
	/// Reads the hardware event log file and packs it into m_statusPayloadOutObj, which
	/// will go out in the status message after DoWork() returns.
	int Fulfill_GetHardwareEventLog();
	int OpticalColumnStatusCode_to_ESStatusCode( int opticalColumnStatusCode );
	int FluorescencLightFilterStatusCode_to_ESStatusCode( int fluorescencLightFilterStatusCode );
	int DoleOut_GetOpticalColumnEventLog();
	int DoleOut_GetFluorescenceLightFilterEventLog();
	int DoleOut_GetOpticalColumnEventLog( std::string & jsonLogStr );
	int DoleOut_GetFluorescenceLightFilterEventLog( std::string & jsonLogStr );
	/// Called by DoWork().
	/// Deletes the hardware event log and recreates it as an empty array.
	int Fulfill_ResetHardwareEventLog();
	int Implement_ResetHardwareEventLog();
	/// Called by DoWork().
	/// Reads the alarms log file and packs it into m_statusPayloadOutObj, which
	/// will go out in the status message after DoWork() returns.
	int Fulfill_GetAlarmsLog();
	/// Called by DoWork().
	/// Deletes the alarms log and recreates it as an empty array.
	int Fulfill_ResetAlarmsLog();
	int Implement_ResetAlarmsLog();
	/// Ping hardware or otherwise check its health.
	int DoleOut_CheckHardware();
	int DoleOut_GetOpticalColumnStatus();
	int DoleOut_FluorescenceLightFilterStatus();

	int DoleOut_MoveXYToSpecialNotation( bool forWell, bool useA1Notation );
	int DoleOut_moveToXYNm();
	int Implement_moveToXYNm(long x_nm, long y_nm);
	int DoleOut_getCurrentXYNm();
	int DoleOut_swirlXYStage();
	int Implement_SwirlXYStage( std::string axisNameAStr, long & startA_nm, long centerA_nm, long axisResolutionA_nm,
								std::string axisNameBStr, long & startB_nm, long centerB_nm, long axisResolutionB_nm,
								float rotationRadius_nm, unsigned long numberOfRotations,
								float & velocity_nmPerSec, long & acceleration_nmPerSecPerSec, long & deceleration_nmPerSecPerSec, long & stp_nmPerSecPerSec );

	int DoleOut_moveToXXNm();
	int Implement_moveToXXNm(long xx_nm);
	int DoleOut_getCurrentXXNm();

	int DoleOut_moveToYYNm();
	int Implement_moveToYYNm(long yy_nm);
	int DoleOut_getCurrentYYNm();
	int DoleOut_MoveYYToTipRackGrabLocation();

	int DoleOut_moveToZNm();
	int DoleOut_moveToZRelativeNm();
	int DoleOut_retractToZNm();
	int Implement_moveToZNm(long z_nm);
	int Implement_moveToZNm(long z_nm, long speed);
	int DoleOut_getCurrentZNm();
	int DoleOut_setZAxisSpeed();
//	int DoleOut_MoveToZLowLimit();
//	int DoleOut_MoveToZHighLimit();
	int DoleOut_MoveZToSafeHeight();
	int DoleOut_MoveToZAxisForceNewton();
	int DoleOut_MoveToZTipPickForceNewton();
	int Implement_MoveToZAxisForceNewton(	unsigned long z_approach_speed, /// tip_pickup_speed or ...
											double z_work_force_newton, /// tip_pickup_force or ...
											long z_work_overshoot, /// tip_pickup_overshoot or ...
											long acceleration,
											long deceleration,
											long stp,
											long axisResolution_nm );
	int Implement_MoveToZZAxisForceNewton(	unsigned long zz_approach_speed, /// tip_pickup_speed or ...
											double zz_work_force_newton, /// tip_pickup_force or ...
											long zz_work_overshoot, /// tip_pickup_overshoot or ...
											long acceleration,
											long deceleration,
											long stp,
											long axisResolution_nm );
	int DoleOut_MoveToZTipSeatingForceNewton();
	int DoleOut_MoveToZTipWellBottomForceNewton();
	int DoleOut_MoveToZAxisSeatingForceNewton();
	int DoleOut_MoveToZAxisWellBottomForceNewton();

	int DoleOut_moveToZZNm();
	int DoleOut_moveToZZRelativeNm();
	int DoleOut_retractToZZNm();
	int Implement_moveToZZNm(long zz_nm);
	int Implement_moveToZZNm(long zz_nm, long speed);
	int DoleOut_getCurrentZZNm();
	int DoleOut_setZZAxisSpeed();
//	int DoleOut_MoveToZZLowLimit();
//	int DoleOut_MoveToZZHighLimit();
	int DoleOut_MoveZZToSafeHeight();
	int DoleOut_MoveToZZAxisForceNewton();
	int DoleOut_MoveToZZTipPickForceNewton();
	int DoleOut_MoveToZZTipSeatingForceNewton();
	int DoleOut_MoveToZZTipWellBottomForceNewton();
	int DoleOut_MoveToZZAxisSeatingForceNewton();
	int DoleOut_MoveToZZAxisWellBottomForceNewton();

	int DoleOut_moveToZZZNm();
	int DoleOut_moveToZZZRelativeNm();
	int Implement_moveToZZZNm( long zzz_nm );
	int Implement_moveToZZZNm( long zzz_nm, long speed );
	int DoleOut_getCurrentZZZNm();
	int DoleOut_MoveZZZToSafeHeight();


	int Implement_MoveOnePump(	const std::string axisNameStr, float & volume_ul_inOut,
								long flowRate_nlPerSec, long acceleration_nlPerSecPerSec,
								long deceleration_nlPerSecPerSec, long stp_nlPerSecPerSec,
								const bool awaitEndOfMotion );
	int DoleOut_MoveOnePump( const char * axisName, float flowRate_ulPerSec, float volume_ul_inOut, const bool awaitEndOfMotion );
	int DoleOut_MoveStepperPump( std::atomic<ComponentState> * compState, const bool calcVolume, const bool calcRate );
	int DoleOut_StartStopStepperPump( std::atomic<ComponentState> * compState, const char * stateKey );
	int DoleOut_IsPumpRunning( const char * stateKey );
	int Implement_StopOneAxis( const int controller, const std::string axisNameStr );

	int DoleOut_MovePickingPump( std::atomic<ComponentState> * compState, const bool calcVolume, const bool calcRate );
	int DoleOut_GetPickingPumpCurrentVolumeNl();
	int DoleOut_MoveOneTecanAirDisplacementPipettor( float flow_rate_ulPerSec, float goalPosition_uL, std::string pumpModel );

//	int DoleOut_setWhiteLightSourceOnOff();
	int DoleOut_isWhiteLightSourceOn();
	int Implement_SetWhiteLightSourceOn( bool on );
	int Implement_setOlympus_IX85_WhiteLightSourceOn( bool on );
	int Implement_setEtaluma_LS620_WhiteLightSourceOn( bool on );

	int Implement_setFluorescenceLightFilterOn( bool on );
	int Implement_setOlympus_IX85_FluorescenceLightFilterOn( bool on );
	int Implement_setEtaluma_LS620_FluorescenceLightFilterOn( bool on );
	int Implement_setFluorescenceLightFilterPosition( long position );
	int Implement_setOlympus_IX85_FluorescenceLightFilterPosition( long position );
	int Implement_setEtaluma_LS620_FluorescenceLightFilterPosition( long position );
	int Implement_SetOpticalColumnTurretPosition( unsigned long position );
	int Implement_SetOlympus_IX85_TurretPosition( unsigned long position );
	int Implement_SetEtaluma_LS620_TurretPosition( unsigned long position );
	int Implement_HomeTurret();
	int Implement_HomeOlympus_IX85_Turret();
	int Implement_HomeEtaluma_LS620_Turret();
	int Implement_HomeFocus();
	int Implement_HomeOlympus_IX85_Focus();
	int Implement_HomeEtaluma_LS620_Focus();

	int DoleOut_MoveToAnnularRingPosition();
	int DoleOut_MoveToAnnularRingPosition( long position_nm );

	int DoleOut_moveToFocusNM();
	int Implement_moveToFocusNM( json_object * templateInObj, unsigned long focus_nm );
	int Implement_moveOlympus_IX85_ToFocusNM( json_object * templateInObj, unsigned long focus_nm );
	int Implement_moveToFocusNM( unsigned long focus_nm );
	int Implement_moveOlympus_IX85_ToFocusNM( unsigned long focus_nm );
	int Implement_moveEtaluma_LS620_ToFocusNM( unsigned long focus_nm );
	int DoleOut_moveToFocusRelativeNM();
	int DoleOut_getCurrentFocusNM();
	int Implement_getCurrentOlympus_IX85_FocusNM( unsigned long & focusOut_nm );
	int Implement_getCurrentEtaluma_LS620_FocusNM( unsigned long & focusOut_nm );

	int DoleOut_isCameraImageAvailable();
	unsigned long CalculateImageBufferSize(	unsigned long pixel_count_x, unsigned long pixel_count_y, unsigned long bin_factor, unsigned long color_depth );
	int DoleOut_getCameraImage();
	int Implement_setCameraGain( int gain );
	int Implement_setCameraExposureTime( unsigned long exposureTime_us );
	int Implement_setCameraBinFactor( int binFactor );
	
	int DoleOut_CollectMontageImages( int commandId, int commandGroupId, std::string sessionId, std::string machineId );
	int DoleOut_AddPlateToDeckInventory();
	int DoleOut_RemovePlateFromDeckInventory();
	int DoleOut_MovePlateInDeckInventory();
	int DoleOut_GetPlateListInLocation();
	int DoleOut_FindPlateLocationInInventory();
	int DoleOut_AddTipRackToDeckInventory();
	int DoleOut_RemoveTipRackFromDeckInventory();
	int DoleOut_MoveTipRackInDeckInventory();
	int DoleOut_GetTipRackListInLocation();
	int DoleOut_FindTipRackLocationInInventory();
	int DoleOut_GetTipCountForTipRack();
	int DoleOut_SetTipCountForTipRack();

	bool AddToPayloadXY( json_object* payloadObj, int x, int y );
	int SendPictureInStatusMessage( json_object* payloadObj,
									unsigned long pixel_count_x,
									unsigned long pixel_count_y,
									unsigned long bin_factor,
									unsigned long scaled_color_depth,
									unsigned long camera_color_depth,
									int commandId,
									int commandGroupId,
									std::string & messageType,
									std::string & sessionId,
									std::string & machineId,
									std::string & originTopic,
									std::string & statusDestinationTopic );
	bool MockCameraCapture( unsigned char *blobPtr, unsigned long blobSize );

	int DoleOut_moveXToTipSkewTrigger( bool packStatusPayload );
	int DoleOut_moveYToTipSkewTrigger( bool packStatusPayload );
	int Implement_moveXYToTipSkewTrigger( const std::string axisNameStr_in,
										  const long opticalSensorToTipSkewPosition_in,
										  const long findTipSkewTravelDistance_in,
										  const unsigned long findTipSkewSpeed_in,
										  const unsigned long findTipSkewAcceleration_in,
										  const unsigned long findTipSkewDeceleration_in,
										  const unsigned int maxPosSn_in,		// eg NEEDLE_Y_MAX_POS_SN or NEEDLE_X_MAX_POS_SN
										  const unsigned long maxPosValNm_in,	// eg 10,000,000.0
										  const int routineId_in, 				// eg FIND_NEEDLE_Y_POS_ROUTINE
										  const unsigned int positionSn_in, 	// eg NEEDLE_Y_POS_SN or NEEDLE_X_POS_SN
										  const unsigned int findNeedleStatusSn_in, // eg FIND_NEEDLE_Y_STATUS_SN or FIND_NEEDLE_X_STATUS_SN
										  long & triggerLocation_nm_out );
	int DoleOut_GetTip();
	int DoleOut_MoveTipToWasteLocation3();
	int DoleOut_MoveTipToWasteLocation2();
	int DoleOut_MoveTipToWasteLocation1();
	int DoleOut_SeatTip();
	int DoleOut_GetTipSkew();
	int DoleOut_SetWorkingTip();
	int DoleOut_RemoveTip();
	int DoleOut_SetOpticalColumnTurretPosition();
	int Implement_SetOpticalColumnTurretPosition( json_object * templateMessageObj,
												  unsigned long safeFocusNmDuringTurretChange,
												  unsigned long turretPosition );
	int Implement_SetOlympus_IX85_TurretPosition( json_object * templateMessageObj,
												  unsigned long safeFocusNmDuringTurretChange,
												  unsigned long turretPosition );

	int DoleOut_GetAutoFocusImageStack( int commandId, int commandGroupId, std::string sessionId, std::string machineId );
	int DoleOut_notImplemented( std::string taskAtHand );
	int DoleOut_fakeImplementation( std::string taskAtHand );
	int DoleOut_MovePlate( bool doReadBarcode );
	int DoleOut_DelidPlate();
	int DoleOut_RelidPlate();
	int DoleOut_MoveTipRack();
	int DoleOut_MoveAllTipsToSafeHeight();
	int DoleOut_SetHardwareState();
	
	int Implement_EnableAxis( std::string axisName, bool & confirmEnabled_out );
	int Implement_HomeAxis( int controllerID, int prog0_routineID, std::string axisName );
	int Implement_HomeAxis( int controllerID, int prog0_routineID, std::string axisName, bool & confirmHomed_out );
	int DoleOut_ClearKills();
	int Implement_ClearKills( int controllerID );
	int DoleOut_InitHardwareControllers();
	int DoleOut_InitSerialControllers();
	int DoleOut_HomeSafeHardware();
	bool Implement_disableAxis( const char * axisName );

	int Implement_MoveTwoAxesConcurrently( std::string axisName0Str, long & position0_nm, long axisResolution0_nm,
										   std::string axisName1Str, long & position1_nm, long axisResolution1_nm,
										   long & speed_nmPerSec, long & acceleration_nmPerSecPerSec, long & deceleration_nmPerSecPerSec, long & stp_nmPerSecPerSec );
	int Implement_MoveOneAxis( std::string axisName, long & position_nm, long axisResolution_nm,
										   long & speed_nmPerSec, long & acceleration_nmPerSecPerSec, long & deceleration_nmPerSecPerSec, long & stp_nmPerSecPerSec );
	int Implement_GetAxisPosition( std::string axisNameStr, long & position_nm );
	bool Implement_SetHomeInputs( json_object * payloadObj_in, const char * homeInputsName_in,
								  int controllerID_in, std::string axisName_in );
	int DoleOut_HomeUnsafeHardware();
	int DoleOut_HomeInitializePickingPump();
	int DoleOut_MoveHardwareToActiveStates();
	int DoleOut_ConfirmNoKillAllMotionRequests( int controllerID );

	std::unique_ptr<std::thread> m_thread;
	MsgQueue *m_inBox;
	MsgQueue *m_outBox;
	const char* m_threadName;
	DesHelper * m_desHelper;
	/// pointers to global vars in main.cpp
	HoldType * esHoldState;
	std::mutex * esHoldMutex;
	/// Volatile state attributes.
	json_object * m_statusMessageOutObj;
	json_object * m_statusPayloadOutObj;
	json_object * m_commandMessageInObj;
	json_object * m_commandPayloadInObj;
	unsigned char ** m_blobPtr;
	unsigned long * m_blobSizePtr;
	std::string m_currentWorkDestination;
	std::string m_currentWorkOrigin;
	std::string m_currentWorkMsgType;

	/// RAM data stores with file mirrors
	Inventory * m_plateInventory;
	Inventory * m_tipInventory;
	/// RAM data stores that are ephemeral.
	std::map<std::string, unsigned long> m_states_ulong;
	std::map<std::string, long> m_states_long;
	std::map<std::string, bool> m_states_bool;
	std::map<std::string, float> m_states_float;

	bool m_doSendStatus;
	bool m_continueRunning;

	/// For MQTT communications
	std::string m_brokerIpAddress;
	MqttHardwareIO * m_mqttHardwareIO;

};

#endif 

