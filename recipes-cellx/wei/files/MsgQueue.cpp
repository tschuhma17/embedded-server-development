#include <iostream>
#include "MsgQueue.hpp"

using namespace std;

/// Public variables used for hold-related locking.
std::mutex g_holdMutex;
std::condition_variable_any g_holdCVA;
long g_numWorkMsgs;
std::mutex g_numWorkMsgsMutex;


///----------------------------------------------------------------------
/// MsgQueue
///----------------------------------------------------------------------
MsgQueue::MsgQueue(const char * queueName) :
	m_queueName( queueName )
{
	isWaitingOnQueue.store( false, std::memory_order_relaxed );
}
    
///----------------------------------------------------------------------
/// ~MsgQueue
///----------------------------------------------------------------------
MsgQueue::~MsgQueue()
{
}
    
///----------------------------------------------------------------------
/// PushMsg
///----------------------------------------------------------------------
void MsgQueue::PushMsg(std::shared_ptr<WorkMsg> msg)
{
    unique_lock<mutex> lock(msg_mutex);

    bool was_empty = msg_queue.empty();
    msg_queue.push(msg);

    lock.unlock();

    if (was_empty)
    {
        msg_available.notify_one();
    }
}
     
///----------------------------------------------------------------------
/// WaitAndPopMsg
///----------------------------------------------------------------------
std::shared_ptr<WorkMsg> MsgQueue::WaitAndPopMsg()
{
    unique_lock<mutex> lock(msg_mutex);
    while (msg_queue.empty())
    {
		isWaitingOnQueue.store(true, std::memory_order_relaxed);
        msg_available.wait(lock);
		isWaitingOnQueue.store(false, std::memory_order_relaxed);
    }

    std::shared_ptr<WorkMsg> tmp = msg_queue.front();
    msg_queue.pop();
    return tmp;
}

///----------------------------------------------------------------------
/// DiscardAllMsgs
/// (For supporting the kill_pending_operations command.)
///----------------------------------------------------------------------
void MsgQueue::DiscardAllMsgs()
{
    unique_lock<mutex> lock(msg_mutex);
	while (!msg_queue.empty())
	{
		msg_queue.pop();
	}
}

bool MsgQueue::IsWaitingOnQueue()
{
	return isWaitingOnQueue.load( std::memory_order_acquire );
}

/// Available for occasional use in main() for debugging.
int MsgQueue::GetQueueSize()
{
	return (int)msg_queue.size();
}

bool MsgQueue::IsOnBackOfQueue( std::string & msgType )
{
	if (msg_queue.size() == 0) return false;
	return (msgType.compare( msg_queue.back()->msgType ) == 0);
}


HoldableMsgQueue::HoldableMsgQueue(const char * queueName) :
	MsgQueue( queueName ),
	m_previousHoldState( HoldType::NoHold_RunFree )
{
}
    
HoldableMsgQueue::~HoldableMsgQueue()
{
}
    
std::shared_ptr<WorkMsg> HoldableMsgQueue::WaitAndPopMsg()
{
	/// Wait
    while (msg_queue.empty() || (g_EsHoldState.load(std::memory_order_acquire) != HoldType::NoHold_RunFree) )
    {
		while (msg_queue.empty())
		{
			unique_lock<mutex> lock(msg_mutex);
			/// Other threads can use isWaitingOnQueue to learn if this message queue is NOT processing a message.
			isWaitingOnQueue.store(true, std::memory_order_relaxed);
			msg_available.wait(lock);
			isWaitingOnQueue.store(false, std::memory_order_relaxed);
		}
		while (g_EsHoldState.load(std::memory_order_acquire) != HoldType::NoHold_RunFree)
		{
			unique_lock<mutex> lock2(g_holdMutex);
			/// In spite of its name, isWaitingOnQueue means this message queue is NOT processing a message.
			isWaitingOnQueue.store(true, std::memory_order_relaxed);
			g_holdCVA.wait(lock2);
			isWaitingOnQueue.store(false, std::memory_order_relaxed);
		}
    }
//	cout << "HoldableMsgQueue::WaitAndPopMsg popped one." << endl;

    std::shared_ptr<WorkMsg> tmp = msg_queue.front();
    msg_queue.pop();
    return tmp;
}



///----------------------------------------------------------------------
/// 	ThreadWorkMsg
///----------------------------------------------------------------------
WorkMsg::WorkMsg(std::string payloadInJson, std::string originTopic, std::string destinationTopic) :
    payload(payloadInJson), destination(destinationTopic), origin(originTopic), msgType(""),
    subMsgType(""), blob(nullptr), blobSize(0L), theEndIsNow(true)
{
	unique_lock<mutex> lock( g_numWorkMsgsMutex );
	g_numWorkMsgs++;
}

WorkMsg::WorkMsg(const char* payloadInJson, const char* originTopic, const char* destinationTopic):
    payload(payloadInJson), destination(destinationTopic), origin(originTopic), msgType(""),
    subMsgType(""), blob(nullptr), blobSize(0L), theEndIsNow(true)
{
	unique_lock<mutex> lock( g_numWorkMsgsMutex );
	g_numWorkMsgs++;
}

WorkMsg::WorkMsg(std::string payloadInJson, std::string originTopic, std::string destinationTopic,
				 const char* messageType, const char* subMessageType, bool theEnd) :
	payload(payloadInJson), destination(destinationTopic), origin(originTopic),
	msgType(messageType), subMsgType(subMessageType), blob(nullptr), blobSize(0L), theEndIsNow(theEnd)
{
	unique_lock<mutex> lock( g_numWorkMsgsMutex );
	g_numWorkMsgs++;
}

WorkMsg::WorkMsg( const WorkMsg& workMsg ) :
	payload(workMsg.payload), destination(workMsg.destination), origin(workMsg.origin),
	msgType(workMsg.msgType), subMsgType(workMsg.subMsgType), blob(nullptr), blobSize(0L), theEndIsNow(workMsg.theEndIsNow)
{
	unique_lock<mutex> lock( g_numWorkMsgsMutex );
	g_numWorkMsgs++;
}

WorkMsg::~WorkMsg()
{
	if (false)
	{
		/// Provide some logging to show that the destructor is called.
		cout << "~WorkMsg()#blobSize=" << blobSize << "#blob=" << reinterpret_cast<void *>(blob) << endl;
	}
	if (blobSize > 0L)
	{
		free (blob);
		blobSize = 0L;
	}
	unique_lock<mutex> lock( g_numWorkMsgsMutex );
	g_numWorkMsgs--;
}
