#ifndef _MAIN_H
#define _MAIN_H
#include <atomic>
#include <thread>
#include <vector>
#include "Sundry.hpp"
#include "enums.hpp"
//#include "es-camera.h" /// base class for camera support.
#include "camera_retiga.hpp" /// support for Retiga R3 camera.
#include "MotionServices.hpp"
#include <signal.h>

enum class ComponentState {
	Uninitialized,
	SettingsConfirmed,
	Homed1,
	Homed2,
	Homed3, /// PPInit
	Good,
	Faulty,
	Mocked,
	SizeOf
};

/// A public function for time syncronization.
bool SynchronizeTime( unsigned long unixTime );

/// Allows any thread to look up the current XX location and compare it to the default location.
extern std::atomic<long> g_XxCurrentOffset;
/// Allows any thread to look up the XX default location.
extern std::atomic<long> g_XxDefaultLocationOffset;

/// Allows any thread to look up the current YY location and compare it to the default location.
extern std::atomic<long> g_YyCurrentOffset;
/// Allows any thread to look up the YY default location.
extern std::atomic<long> g_YyDefaultLocationOffset;

/// Allows any thread to look up the current picking pump volume.
extern std::atomic<float> g_PickingPumpVolume;
extern std::atomic<float> g_PickingPumpMaximumVolume;

/// THE state variable for hold states that pause operations.
extern std::atomic<HoldType> g_EsHoldState;

/// THE state variable managing a temporary state during which queues are flushed.
extern std::atomic<bool> g_DoFlushQueues;

/// The means by which main() knows whether MqttReceiver is connected to a broker.
extern std::atomic<bool> g_ConnectedToBroker;

/// THE mutex to use for error logs, alert logs, and any other logs.
extern std::mutex g_logFileMutex;
/// THE mutex for accessing the camera.
extern std::mutex g_cameraMutex;
/// THE Camera list. (All CommandDoler threads have access, but only 2 should be using it. They should use g_cameraMutex for access and only one should initialize and destroy it.)
extern std::vector<ES_CameraBase *> g_CameraList;
extern std::atomic<bool> g_CameraIsInitialized;
/// THE mutex for keeping the annular ring holder from homing while any Z axes are homing
extern std::mutex g_controllerBMutex;
/// THE mutex for controlling access to the mn::CppLinuxSerial::SerialPort object maintained by one Doler thread.
extern std::mutex g_SerialPortMutex;
/// THE mutex for controlling access to the Parker controllers during hardware checks.
extern std::mutex g_HardwareCheckMutex;

/// Public for one specific use by MqttReceiver.cpp.
void ManageHoldStates();

extern std::atomic<ComponentState> g_EsHwState; /// Summarizes the states of all of the following hardware components.
// extern std::atomic<ComponentState> g_CxDState;
extern std::atomic<ComponentState> g_MotionServicesLibState;
extern std::atomic<ComponentState> g_XYState;
extern std::atomic<ComponentState> g_XXState;
extern std::atomic<ComponentState> g_YYState;
extern std::atomic<ComponentState> g_ZState;
extern std::atomic<ComponentState> g_ZZState;
extern std::atomic<ComponentState> g_ZZZState;
extern std::atomic<ComponentState> g_AspirationPump1State;
extern std::atomic<ComponentState> g_DispensePump1State;
extern std::atomic<ComponentState> g_DispensePump2State;
extern std::atomic<ComponentState> g_DispensePump3State;
extern std::atomic<ComponentState> g_PickingPump1State;
extern std::atomic<ComponentState> g_WhiteLightSourceState;
extern std::atomic<ComponentState> g_FluorescenceLightFilterState;
extern std::atomic<ComponentState> g_AnnularRingHolderState;
extern std::atomic<ComponentState> g_CameraState;
extern std::atomic<ComponentState> g_OpticalColumnState;
extern std::atomic<ComponentState> g_IncubatorState;
extern std::atomic<ComponentState> g_CxPMState;
extern std::atomic<ComponentState> g_BarcodeReaderState;
extern std::string m_SerialPortDevice_ADP;
extern std::string g_SerialPortDevice_MICRO;
extern std::string g_SerialPortDevice_CAMERA;

/// Start switching to arrays for maintaining component property states
extern std::array<HardwareType, (int)ComponentType::FINAL_COMPONENT_TYPE_ENTRY> g_ComponentHardwareTypeArray;
extern volatile sig_atomic_t g_signalReceived;

extern USAFW::MotionServices * g_motionServices_ptr;

extern uint32_t g_sequenceId;

#define HW_PING_RATE_SECONDS 60

#endif 
