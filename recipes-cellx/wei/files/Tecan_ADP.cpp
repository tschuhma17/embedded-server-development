
///----------------------------------------------------------------------
/// Tecan_ADP
/// A module for controlling the Tecan Air Displacement Pipettor (ADP)
///----------------------------------------------------------------------
///----------------------------------------------------------------------
#include <iostream>
#include <string>
#include <cstring>
#include <sstream>
#include <cctype>		/// std::tolower
#include <unistd.h>		/// usleep
#include "Tecan_ADP.hpp"
#include "es_des.h"
#include "Sundry.hpp"

using namespace std;
using namespace mn::CppLinuxSerial;

#ifndef LOG_TRACE
#define LOG_TRACE( thread, subsystem, message, ...) (cout << "TRACE," << thread << "," << subsystem << "," << message << ",line " << std::dec << __LINE__ << endl)
#endif
#ifndef LOG_ERROR
#define LOG_ERROR( thread, subsystem, message, ...) (cout << "ERROR," << thread << "," << subsystem << "," << message << ",line " << std::dec << __LINE__ << endl)
#endif

const int TecanADP::CommonLongLoopCount = 50;
const long TecanADP::StandardWait_uS = 10000L;
const float TecanADP::TopSpeedMin_uL = 0.025;
const float TecanADP::TopSpeedMax_uL = 1000.0;
const float TecanADP::StartSpeedMin_uL = 2.500;
const float TecanADP::StartSpeedMax_uL = 100.0;
const float TecanADP::CutOffSpeedMin_uL = 2.500;
const float TecanADP::CutOffSpeedMax_uL = 200.0;

/// Passing tests on 2023-04-06 (mdg)
///----------------------------------------------------------------------
/// TecanADP constructor.
/// Uses the CppLinuxSerial library to configure
///----------------------------------------------------------------------
TecanADP::TecanADP( mn::CppLinuxSerial::SerialPort * serialPort ) :
	m_serialPort( serialPort ),
	m_isInitialized( false ),
	m_tipIsPresent( false ),
	m_topSpeed( 0L ),
	m_startSpeed( 0L ),
	m_cutOffSpeed( 0L ),
	m_commandSequence( 1 ),
	m_pumpID( 1 ),
	m_lastError()
{
	if (serialPort)
	{
		try {
			m_serialPort->SetTimeout(0); /// Immediate
		} catch (mn::CppLinuxSerial::Exception &e) {
			stringstream msg;
			msg << "Exception during SetTimeout:" << e.what();
			LOG_ERROR( "unk", "adp", msg.str().c_str() );
		}
	}
}


TecanADP::~TecanADP()
{
	if (m_serialPort)
	{
		try {
			m_serialPort->Close();
		} catch (mn::CppLinuxSerial::Exception &e) {
			stringstream msg;
			msg << "EXCEPTION while Closing:" << e.what();
			LOG_ERROR( "unk", "adp", msg.str().c_str() );
		}
		m_serialPort = nullptr;
	}
}

/// Passing tests on 2023-04-06 (mdg)
int TecanADP::IsDeviceInitialized( const char * threadName_in, bool & isBusy, bool & hasErrors, bool & isInitialized_out )
{
	cout << "--- IsDeviceInitialized ---" << endl;
	EventRecord record = ExecuteCommand( "main", cmCOMMAND_QUERY_STATUS );
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		int stdError = 0;
		int status = ExtractBusyAndStdError( threadName_in, record.m_Text, isBusy, stdError );
		cout << "stdError = " << stdError << endl;
		hasErrors = (stdError != cmERROR_CODE_NO_ERROR);
		string payload = "";
		status = ExtractPayload( threadName_in, record.m_Text, payload );
		isInitialized_out = ((stdError != cmERROR_CODE_DEVICE_NOT_INITIALIZED) && (payload != cmDEVICE_IS_NOT_INITIALIZED_STRING));

		// TODO: Decide which command will get extended errors. If it is this one,
		// do something with extendedErrorList.
		string extendedErrorList = "";

		return status;
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		return STATUS_QUERY_FAILED;
	}
}

/// A more generic command relying on "Q1". Returns is-busy and a list of all standard or extended errors as strings.
int TecanADP::GetBusy( const char * threadName_in, bool & isBusy, BoxOfErrors & box )
{
	cout << "--- GetBusy ---" << endl;
	box.errors.clear();
	EventRecord record = ExecuteCommand( "main", cmCOMMAND_QUERY_STATUS );
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		int stdError = 0;
		int status = ExtractBusyAndStdError( threadName_in, record.m_Text, isBusy, stdError );
		cout << "stdError = " << stdError << endl;
		if (stdError != cmERROR_CODE_NO_ERROR)
		{
			if (stdError == cmERROR_CODE_EXTENDED_ERROR)
			{
				/// Pull the extended error codes out of the response string and
				/// push them into the outgoing list-of-strings in place of the
				/// standard error.
				string extendedErrorList = "";
				status = ExtractExtendedErrors( threadName_in, record.m_Text, extendedErrorList );
				if (status == STATUS_OK)
				{
					for (unsigned int i = 0; i < extendedErrorList.size(); i++)
					{
						unsigned int error = (unsigned int) extendedErrorList[i];
						if (error > extendedErrorCodes.size())
						{
							box.errors.push_back( "Undefined error" );
						}
						else
						{
							box.errors.push_back( extendedErrorCodes[error] );
						}
					}
				}
			}
			else
			{
				/// If we get here, stdError is not one of the ones we suppress.
				/// (We suppress "no error" and "extended error".
				box.errors.push_back(StandardErrorCodesToString( stdError ));
			}
		}

		return status;
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		return STATUS_QUERY_FAILED;
	}
}

int TecanADP::WaitForNotBusy( const char * threadName_in, const char * functionName )
{
	bool isBusy = true;
	int status = STATUS_OK;
	int count = 0;
	BoxOfErrors box;
	do {
		status = GetBusy( threadName_in, isBusy, box );
		/// StandardWait_uS=10 ms. CommonLongLoopCount = 50.
		/// At 10x, this loop would run for 5 seconds at most.
		/// At 20x, it will run for 10 seconds at most.
		usleep( 20 * StandardWait_uS );
	} while (isBusy && (count++ < CommonLongLoopCount));

	/// Handle a time-out if it happened.
	if (isBusy && (count >= CommonLongLoopCount))
	{
		stringstream msg;
		msg << "Tecan ADP timed out waiting for not busy in " << functionName;
		LOG_ERROR( threadName_in, "pick", msg.str().c_str() );
		ClosePort( threadName_in );
	}
	else
	{
		stringstream msg;
		msg << "Tecan ADP finished after " << count << " tries in function " << functionName;
		LOG_TRACE( threadName_in, "pick", msg.str().c_str() );
		/// No need to close the port here, because that happens the first time GetBusy detects not-busy.
		/// (See ExecuteCommand().)
	}
	return status;
}

/// Passing tests on 2023-04-06 (mdg)
int TecanADP::InitializeDevice( const char * threadName_in , int & stdError_out)
{
	cout << "--- InitializeDevice ---" << endl;
	EventRecord record = ExecuteCommand( "main", cmCOMMAND_INITIALIZE_PLUNGER );
	int status = STATUS_OK;
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		bool isBusy = false;
		status = ExtractBusyAndStdError( threadName_in, record.m_Text, isBusy, stdError_out );
//		cout << "stdError = " << stdError_out << endl;
		if (stdError_out == cmERROR_CODE_NO_ERROR)
			status = STATUS_OK;
		else
		{
			stringstream msg;
			msg << "stdError=" << stdError_out;
			LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
			status = STATUS_INIT_FAILED;
		}
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		status = STATUS_INIT_FAILED;
	}
	return status;
}

int TecanADP::EjectTip( const char * threadName_in, int & stdError_out )
{
	cout << "--- EjectTip ---" << endl;
	EventRecord record = ExecuteCommand( "main", cmCOMMAND_EJECT_TIP );
	int status = STATUS_OK;
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		m_tipIsPresent = false; /// May not be true until the process ends.
		bool isBusy = false;
		status = ExtractBusyAndStdError( threadName_in, record.m_Text, isBusy, stdError_out );
//		cout << "stdError = " << stdError_out << endl;
		if (stdError_out == cmERROR_CODE_NO_ERROR)
			status = STATUS_OK;
		else
		{
			stringstream msg;
			msg << "stdError=" << stdError_out << "(" << StandardErrorCodesToString( stdError_out ) << ")";
			LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
			status = STATUS_INIT_FAILED;
		}
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		status = STATUS_INIT_FAILED;
	}
	return status;
}

int TecanADP::EjectTipAtInitialization( const char * threadName_in, int & stdError_out )
{
	cout << "--- EjectTipAtInitialization ---" << endl;
	EventRecord record = ExecuteCommand( "main", cmCOMMAND_EJECT_TIP_AT_INITIALIZE_ONLY );
	int status = STATUS_OK;
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		m_tipIsPresent = false; /// May not be true until the process ends.
		bool isBusy = false;
		status = ExtractBusyAndStdError( threadName_in, record.m_Text, isBusy, stdError_out );
//		cout << "stdError = " << stdError_out << endl;
		/// We are attempting to eject a tip just in case one is there. If the error code indicates a
		/// tip is missing, don't treat it as an error.
		if ((stdError_out == cmERROR_CODE_NO_ERROR) || (stdError_out == cmERROR_CODE_TIP_LOST_OR_NOT_PRESENT))
			status = STATUS_OK;
		else
		{
			stringstream msg;
			msg << "stdError=" << stdError_out << "(" << StandardErrorCodesToString( stdError_out ) << ")";
			LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
			status = STATUS_INIT_FAILED;
		}
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		status = STATUS_INIT_FAILED;
	}
	return status;
}


int TecanADP::MoveAbsolutePosition( const char * threadName_in, float position_uL_in, int & stdError_out )
{
	cout << "--- MoveAbsolutePosition ---" << endl;
	stringstream cmd;
	cmd.precision(3);
	cmd << std::fixed << cmCOMMAND_MOVE_ABSOLUTE_POSITION_UL << position_uL_in << cmCOMMAND_CLOSE;
	EventRecord record = ExecuteCommand( "main", cmd.str() );
	int status = STATUS_OK;
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		bool isBusy = false;
		status = ExtractBusyAndStdError( threadName_in, record.m_Text, isBusy, stdError_out );
//		cout << "stdError = " << stdError_out << endl;
		if (stdError_out == cmERROR_CODE_NO_ERROR)
			status = STATUS_OK;
		else
		{
			stringstream msg;
			msg << "stdError=" << stdError_out << "(" << StandardErrorCodesToString( stdError_out ) << ")";
			LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
			status = STATUS_INIT_FAILED;
		}
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		status = STATUS_INIT_FAILED;
	}
	return status;
}

int TecanADP::SetTopSpeed( const char * threadName_in, float topSpeed_uL_per_sec_in, int & stdError_out )
{
	cout << "--- SetTopSpeed ---" << endl;
	// Range 0.025 to 1000.000 ul/sec
	if ((topSpeed_uL_per_sec_in < TecanADP::TopSpeedMin_uL) ||
		(topSpeed_uL_per_sec_in > TecanADP::TopSpeedMax_uL))
	{
		LOG_ERROR( threadName_in, "adp", "Stop speed out of range." );
		return STATUS_MOTION_OUT_OF_RANGE;	// EARLY RETURN!	EARLY RETURN!
	}
	stringstream cmd;
	cmd.precision(3);
	cmd << std::fixed << cmCOMMAND_SET_TOP_SPEED_UL_PER_SEC << topSpeed_uL_per_sec_in << cmCOMMAND_CLOSE;
	EventRecord record = ExecuteCommand( "main", cmd.str() );
	int status = STATUS_OK;
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		bool isBusy = false;
		status = ExtractBusyAndStdError( threadName_in, record.m_Text, isBusy, stdError_out );
//		cout << "stdError = " << stdError_out << endl;
		if (stdError_out == cmERROR_CODE_NO_ERROR)
			status = STATUS_OK;
		else
		{
			stringstream msg;
			msg << "stdError=" << stdError_out << "(" << StandardErrorCodesToString( stdError_out ) << ")";
			LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
			status = STATUS_INIT_FAILED;
		}
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		status = STATUS_INIT_FAILED;
	}
	return status;
}

int TecanADP::SetStartSpeed( const char * threadName_in, float startSpeed_uL_per_sec_in, int & stdError_out  )
{
	cout << "--- SetStartSpeed ---" << endl;
	// Range 2.500 to 100.00 ul/sec
	if ((startSpeed_uL_per_sec_in < TecanADP::StartSpeedMin_uL) ||
		(startSpeed_uL_per_sec_in > TecanADP::StartSpeedMax_uL))
	{
		LOG_ERROR( threadName_in, "adp", "Start speed out of range." );
		return STATUS_MOTION_OUT_OF_RANGE;	// EARLY RETURN!	EARLY RETURN!
	}
	stringstream cmd;
	cmd.precision(3);
	cmd << std::fixed << cmCOMMAND_SET_START_SPEED_UL_PER_SEC << startSpeed_uL_per_sec_in << cmCOMMAND_CLOSE;
	EventRecord record = ExecuteCommand( "main", cmd.str() );
	int status = STATUS_OK;
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		bool isBusy = false;
		status = ExtractBusyAndStdError( threadName_in, record.m_Text, isBusy, stdError_out );
//		cout << "stdError = " << stdError_out << endl;
		if (stdError_out == cmERROR_CODE_NO_ERROR)
			status = STATUS_OK;
		else
		{
			stringstream msg;
			msg << "stdError=" << stdError_out << "(" << StandardErrorCodesToString( stdError_out ) << ")";
			LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
			status = STATUS_INIT_FAILED;
		}
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		status = STATUS_INIT_FAILED;
	}
	return status;
}

int TecanADP::SetCutoffSpeed( const char * threadName_in, float cutOffSpeed_uL_per_sec_in, int & stdError_out )
{
	cout << "--- SetCutoffSpeed ---" << endl;
	/// Range 2.500 to 200.000 ul/sec
	if ((cutOffSpeed_uL_per_sec_in < TecanADP::CutOffSpeedMin_uL) ||
		(cutOffSpeed_uL_per_sec_in > TecanADP::CutOffSpeedMax_uL))
	{
		LOG_ERROR( threadName_in, "adp", "Cut-off speed out of range." );
		return STATUS_MOTION_OUT_OF_RANGE;	// EARLY RETURN!	EARLY RETURN!
	}
	stringstream cmd;
	cmd.precision(3);
	cmd << std::fixed << cmCOMMAND_SET_CUTOFF_SPEED_UL_PER_SEC << cutOffSpeed_uL_per_sec_in << cmCOMMAND_CLOSE;
	EventRecord record = ExecuteCommand( "main", cmd.str() );
	int status = STATUS_OK;
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		bool isBusy = false;
		status = ExtractBusyAndStdError( threadName_in, record.m_Text, isBusy, stdError_out );
		if (stdError_out == cmERROR_CODE_NO_ERROR)
			status = STATUS_OK;
		else
		{
			stringstream msg;
			msg << "stdError=" << stdError_out << "(" << StandardErrorCodesToString( stdError_out ) << ")";
			LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
			status = STATUS_INIT_FAILED;
		}
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		status = STATUS_INIT_FAILED;
	}
	return status;
}

/// Passing tests on 2023-04-06 (mdg)
int TecanADP::GetPumpPartNumber( const char * threadName_in, std::string & pumpPartNumber_out )
{
	cout << "--- GetPumpPartNumber ---" << endl;
	EventRecord record = ExecuteCommand( threadName_in, cmCOMMAND_GET_PUMP_PART_NUMBER );
	int status = STATUS_OK;
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		bool busy = false;
		int stdError = 0;
		status = ExtractBusyAndStdError( threadName_in, record.m_Text, busy, stdError );
		if (busy)
		{
			LOG_ERROR( threadName_in, "adp", "Pump is BUSY" );
			status = STATUS_QUERY_FAILED;
		}
		else if (stdError != cmERROR_CODE_NO_ERROR)
		{
			stringstream msg;
			msg << "Error reported:" << stdError;
			LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
			//status = STATUS_QUERY_FAILED;
		}
		if (status == STATUS_OK)
		{
			string pumpPartNumber = "";
			status = ExtractPayload( threadName_in, record.m_Text, pumpPartNumber );
			stringstream msg;
			msg << "Payload=" << pumpPartNumber;
			LOG_TRACE( threadName_in, "adp", msg.str().c_str() );
			pumpPartNumber_out = pumpPartNumber;
		}
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		status = STATUS_QUERY_FAILED;
	}
	return status;
}

int TecanADP::GetPumpFirmwareVersion( const char * threadName_in, std::string & pumpFirmwareVersion_out )
{
	cout << "--- GetPumpFirmwareVersion ---" << endl;
	EventRecord record = ExecuteCommand( threadName_in, cmCOMMAND_GET_PUMP_FIRMWARE_VERSION);
	int status = STATUS_OK;
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		bool busy = false;
		int stdError = 0;
		status = ExtractBusyAndStdError( threadName_in, record.m_Text, busy, stdError );
		if (busy)
		{
			LOG_ERROR( threadName_in, "adp", "Pump is BUSY" );
			status = STATUS_QUERY_FAILED;
		}
		else if (stdError != cmERROR_CODE_NO_ERROR)
		{
			stringstream msg;
			msg << "Error reported:" << stdError;
			LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
			//status = STATUS_QUERY_FAILED;
		}
		if (status == STATUS_OK)
		{
			string pumpFirmwareVersion = "";
			status = ExtractPayload( threadName_in, record.m_Text, pumpFirmwareVersion );
			stringstream msg;
			msg << "Payload=" << pumpFirmwareVersion;
			LOG_TRACE( threadName_in, "adp", msg.str().c_str() );
			pumpFirmwareVersion_out = pumpFirmwareVersion;
		}
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		status = STATUS_QUERY_FAILED;
	}
	return status;
}

int TecanADP::GetPumpSerialNumber( const char * threadName_in, std::string & pumpSerialNumber_out )
{
	cout << "--- GetPumpSerialNumber ---" << endl;
	EventRecord record = ExecuteCommand( threadName_in, cmCOMMAND_GET_PUMP_SERIAL_NUMBER );
	int status = STATUS_OK;
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		bool busy = false;
		int stdError = 0;
		status = ExtractBusyAndStdError( threadName_in, record.m_Text, busy, stdError );
		if (busy)
		{
			LOG_ERROR( threadName_in, "adp", "Pump is BUSY" );
			status = STATUS_QUERY_FAILED;
		}
		else if (stdError != cmERROR_CODE_NO_ERROR)
		{
			stringstream msg;
			msg << "Error reported:" << stdError;
			LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
			//status = STATUS_QUERY_FAILED;
		}
		if (status == STATUS_OK)
		{
			string pumpSerialNumber = "";
			status = ExtractPayload( threadName_in, record.m_Text, pumpSerialNumber );
			stringstream msg;
			msg << "Payload=" << pumpSerialNumber;
			LOG_TRACE( threadName_in, "adp", msg.str().c_str() );
			pumpSerialNumber_out = pumpSerialNumber;
		}
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		status = STATUS_QUERY_FAILED;
	}
	return status;
}

int TecanADP::GetPumpCurrentVolume ( const char * threadName_in, std::string & pumpCurrentVolume_out )
{
	cout << "--- GetPumpCurrentVolume ---" << endl;
	EventRecord record = ExecuteCommand( threadName_in, cmCOMMAND_QUERY_CURRENT_VOLUME_UL);
	int status = STATUS_OK;
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		bool busy = false;
		int stdError = 0;
		status = ExtractBusyAndStdError( threadName_in, record.m_Text, busy, stdError );
		if (busy)
		{
			LOG_ERROR( threadName_in, "adp", "Pump is BUSY" );
			status = STATUS_QUERY_FAILED;
		}
		else if (stdError != cmERROR_CODE_NO_ERROR)
		{
			stringstream msg;
			msg << "Error reported:" << stdError;
			LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
			//status = STATUS_QUERY_FAILED;
		}
		if (status == STATUS_OK)
		{
			string pumpCurrentVolume = "";
			status = ExtractPayload( threadName_in, record.m_Text, pumpCurrentVolume );
			stringstream msg;
			msg << "Payload=" << pumpCurrentVolume;
			LOG_TRACE( threadName_in, "adp", msg.str().c_str() );
			pumpCurrentVolume_out = pumpCurrentVolume;
		}
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		status = STATUS_QUERY_FAILED;
	}
	return status;
}

int TecanADP::GetPumpMaximumVolume ( const char * threadName_in, std::string & pumpMaximumVolume_out )
{
	cout << "--- GetPumpMaximumVolume ---" << endl;
	EventRecord record = ExecuteCommand( threadName_in, cmCOMMAND_QUERY_MAXIMUM_VOLUME_UL);
	int status = STATUS_OK;
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		bool busy = false;
		int stdError = 0;
		status = ExtractBusyAndStdError( threadName_in, record.m_Text, busy, stdError );
		if (busy)
		{
			LOG_ERROR( threadName_in, "adp", "Pump is BUSY" );
			status = STATUS_QUERY_FAILED;
		}
		else if (stdError != cmERROR_CODE_NO_ERROR)
		{
			stringstream msg;
			msg << "Error reported:" << stdError;
			LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
			//status = STATUS_QUERY_FAILED;
		}
		if (status == STATUS_OK)
		{
			string pumpMaximumVolume = "";
			status = ExtractPayload( threadName_in, record.m_Text, pumpMaximumVolume );
			stringstream msg;
			msg << "Payload=" << pumpMaximumVolume;
			LOG_TRACE( threadName_in, "adp", msg.str().c_str() );
			pumpMaximumVolume_out = pumpMaximumVolume;
		}
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		status = STATUS_QUERY_FAILED;
	}
	return status;
}

int TecanADP::GetPumpStartSpeed ( const char * threadName_in, std::string & pumpStartSpeed_out )
{
	cout << "--- GetPumpStartSpeed ---" << endl;
	EventRecord record = ExecuteCommand( threadName_in, cmCOMMAND_QUERY_START_SPEED_UL_PER_SEC);
	int status = STATUS_OK;
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		bool busy = false;
		int stdError = 0;
		status = ExtractBusyAndStdError( threadName_in, record.m_Text, busy, stdError );
		if (busy)
		{
			LOG_ERROR( threadName_in, "adp", "Pump is BUSY" );
			status = STATUS_QUERY_FAILED;
		}
		else if (stdError != cmERROR_CODE_NO_ERROR)
		{
			stringstream msg;
			msg << "Error reported:" << stdError;
			LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
			//status = STATUS_QUERY_FAILED;
		}
		if (status == STATUS_OK)
		{
			string pumpStartSpeed = "";
			status = ExtractPayload( threadName_in, record.m_Text, pumpStartSpeed );
			stringstream msg;
			msg << "Payload=" << pumpStartSpeed;
			LOG_TRACE( threadName_in, "adp", msg.str().c_str() );
			pumpStartSpeed_out = pumpStartSpeed;
		}
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		status = STATUS_QUERY_FAILED;
	}
	return status;
}


int TecanADP::GetPumpTopSpeed ( const char * threadName_in, std::string & pumpTopSpeed_out )
{
	cout << "--- GetPumpTopSpeed ---" << endl;
	EventRecord record = ExecuteCommand( threadName_in, cmCOMMAND_QUERY_TOP_SPEED_UL_PER_SEC);
	int status = STATUS_OK;
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		bool busy = false;
		int stdError = 0;
		status = ExtractBusyAndStdError( threadName_in, record.m_Text, busy, stdError );
		if (busy)
		{
			LOG_ERROR( threadName_in, "adp", "Pump is BUSY" );
			status = STATUS_QUERY_FAILED;
		}
		else if (stdError != cmERROR_CODE_NO_ERROR)
		{
			stringstream msg;
			msg << "Error reported:" << stdError;
			LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
			//status = STATUS_QUERY_FAILED;
		}
		if (status == STATUS_OK)
		{
			string pumpTopSpeed = "";
			status = ExtractPayload( threadName_in, record.m_Text, pumpTopSpeed );
			stringstream msg;
			msg << "Payload=" << pumpTopSpeed;
			LOG_TRACE( threadName_in, "adp", msg.str().c_str() );
			pumpTopSpeed_out = pumpTopSpeed;
		}
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		status = STATUS_QUERY_FAILED;
	}
	return status;
}


int TecanADP::GetPumpCutoffSpeed ( const char * threadName_in, std::string & pumpCutoffSpeed_out )
{
	cout << "--- GetPumpCutoffSpeed ---" << endl;
	EventRecord record = ExecuteCommand( threadName_in, cmCOMMAND_QUERY_CUTOFF_SPEED_UL_PER_SEC);
	int status = STATUS_OK;
	if (record.m_Error == ADPClassErrorCodes::NoError)
	{
		bool busy = false;
		int stdError = 0;
		status = ExtractBusyAndStdError( threadName_in, record.m_Text, busy, stdError );
		if (busy)
		{
			LOG_ERROR( threadName_in, "adp", "Pump is BUSY" );
			status = STATUS_QUERY_FAILED;
		}
		else if (stdError != cmERROR_CODE_NO_ERROR)
		{
			stringstream msg;
			msg << "Error reported:" << stdError;
			LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
			//status = STATUS_QUERY_FAILED;
		}
		if (status == STATUS_OK)
		{
			string pumpCutoffSpeed = "";
			status = ExtractPayload( threadName_in, record.m_Text, pumpCutoffSpeed );
			stringstream msg;
			msg << "Payload=" << pumpCutoffSpeed;
			LOG_TRACE( threadName_in, "adp", msg.str().c_str() );
			pumpCutoffSpeed_out = pumpCutoffSpeed;
		}
	}
	else
	{
		stringstream msg;
		msg << "ADPClassErrorCode=" << ADPClassErrorCodesToString(record.m_Error);
		LOG_ERROR( threadName_in, "adp", msg.str().c_str() );
		status = STATUS_QUERY_FAILED;
	}
	return status;
}

/// Passing tests on 2023-04-06 (mdg)
EventRecord TecanADP::ExecuteCommand( const char * threadName_in, const std::string baseCommand_in )
{
	/// Log this call.
	stringstream msg;
	msg << "Sending.. " << baseCommand_in;

	EventRecord eventRecord;
	string commandStr = BuildCommand( baseCommand_in );
	if (commandStr != "")
	{
		/// Open the port.
		try {
			if (m_serialPort->GetState() == State::CLOSED)
			{
				m_serialPort->Open();
			}
		} catch (mn::CppLinuxSerial::Exception &e) {
			EventRecord openRecord( EventType::Comm, CommEventType::CommFailedToOpen, ADPClassErrorCodes::DeviceBusyTimeout, "EXCEPTION while opening", e );
			LOG_ERROR( threadName_in, "adp", openRecord.m_Text.c_str() );
			return openRecord;	/// EARLY RETURN!!	EARLY RETURN!!
		}

		/// Send the command.
		eventRecord = SendMessageToDevice( threadName_in, commandStr );
		if (eventRecord.m_Type == EventType::ResponseReceived)
		{
//			LOG_TRACE( threadName_in, "adp", "ExecuteCommand: Message received" );
		}

		/// Close the port.
		try {
			if (m_serialPort->GetState() == State::OPEN)
			{
				/// Is the command cmCOMMAND_QUERY_STATUS?
				if (baseCommand_in.compare( cmCOMMAND_QUERY_STATUS ) == 0)
				{
					int stdError = 0;
					bool isBusy = false;
					int status = ExtractBusyAndStdError( threadName_in, eventRecord.m_Text, isBusy, stdError );
					/// Is this likely to be the last call to cmCOMMAND_QUERY_STATUS in a single do-while loop?
					if (isBusy)
					{
//						LOG_TRACE( threadName_in, "adp", "Is (still) busy, so not closing port" );
					}
					else
					{
						LOG_TRACE( threadName_in, "adp", "No longer busy, so closing port now" );
						m_serialPort->Close();
					}
				}
				else
				{
					/// All other kinds of commands close when they are done.
					m_serialPort->Close();
				}
			}
		} catch (mn::CppLinuxSerial::Exception &e) {
			EventRecord closeRecord( EventType::Comm, CommEventType::CommFailedToClose, ADPClassErrorCodes::DeviceBusyTimeout, "EXCEPTION while closing", e );
			LOG_ERROR( threadName_in, "adp", closeRecord.m_Text.c_str() );
			return closeRecord;	/// EARLY RETURN!!	EARLY RETURN!!
		}
	}
	else
	{
		LOG_ERROR( threadName_in, "adp", "Empty Command string" );
		eventRecord.m_Type = EventType::BusinessLogicError;
		eventRecord.m_Error = ADPClassErrorCodes::CommandStringError;
		eventRecord.m_Text = "Empty command string";
	}
	return eventRecord;
}

/// Passing tests on 2023-04-05 (mdg)
string TecanADP::BuildCommand( const string & baseCommand )
{
	stringstream command;
	command << (char)cCOMMAND_START_CODE << (char)cCOMMAND_START_DELIMITER << to_string(m_pumpID);
	command << to_string(m_commandSequence) << baseCommand << (char)cCOMMAND_END_DELIMITER;
	command << GetChecksum(command.str());
	m_commandSequence = (m_commandSequence == 1) ? 2 : 1;
	return command.str();
}

// TODO: This method does not function and is not used. Either fix or remove.
int TecanADP::IsTipPresent( const char * threadName_in, bool & tipIsPresent_out )
{
	EventRecord retResponse;

	retResponse = ExecuteCommand( threadName_in, cmCOMMAND_QUERY_TIP_PRESENCE );
	cout << "response type=" << (int) retResponse.m_Type << endl;
	if (retResponse.m_Type == EventType::ResponseReceived)
	{
		cout << "full response text: " << retResponse.m_Text << endl;
		string responseStr = "";
		int status = ExtractPayload( threadName_in, retResponse.m_Text, responseStr );
//		ExtractResponsePayload( retResponse.m_Text );
		cout << "response payload: " << responseStr << endl;
//		  IsTipPresent = CBool(ExtractResponsePayload(retResponse.EventText))
	}
	else
	{
//		  Call SetLastError(retResponse)
//		  Call Utilities.LogEvent(mLastError, mLastError.EventText)
	}
	return STATUS_OK;
}

int TecanADP::ClosePort( const char * threadName_in )
{
	int status = STATUS_OK;
	try
	{
		if (m_serialPort->GetState() == State::OPEN)
		{
			m_serialPort->Close();
		}
		if (m_serialPort->GetState() == State::OPEN)
		{
			status = STATUS_SERIAL_PORT_ERROR;
		}
	}
	catch (mn::CppLinuxSerial::Exception &e)
	{
		status = STATUS_SERIAL_PORT_ERROR;
	}
	return status;
}

/// Passing tests on 2023-04-06 (mdg)
EventRecord TecanADP::SendMessageToDevice( const char * threadName_in, std::string message_in )
{
	/// Create a record to return.
	EventRecord eventRecord;
	if (!m_serialPort)
	{
		LOG_WARN( threadName_in, "adp", "NULL serial port" );
		return eventRecord;
	}
	if (m_serialPort->GetState() == State::CLOSED)
	{
		LOG_WARN( threadName_in, "adp", "serial port is closed" );
		return eventRecord;
	}

	/// Log this call.
//	stringstream msg1;
//	msg1 << "SendMessageToDevice: Open says me. " << message_in;
//	LOG_TRACE( threadName_in, "adp", msg1.str().c_str() );

//	stringstream msg2;
//	msg2 << "SendMessageToDevice: Write Message. " << message_in;
//	LOG_TRACE( threadName_in, "adp", msg2.str().c_str() );
	try {
		m_serialPort->Write( message_in );
	} catch (mn::CppLinuxSerial::Exception &e) {
		EventRecord writeRecord( EventType::Comm, CommEventType::CommSendFailure, ADPClassErrorCodes::DeviceBusyTimeout, "EXCEPTION while writing", e );
		LOG_ERROR( threadName_in, "adp", writeRecord.m_Text.c_str() );
		try {
			m_serialPort->Close();
		} catch (mn::CppLinuxSerial::Exception &e) {
			EventRecord closeRecord( EventType::Comm, CommEventType::CommFailedToClose, ADPClassErrorCodes::DeviceBusyTimeout, "EXCEPTION while closing", e );
			LOG_ERROR( threadName_in, "adp", closeRecord.m_Text.c_str() );
			return closeRecord;	/// EARLY RETURN!!	EARLY RETURN!!
		}
		return writeRecord;	/// EARLY RETURN!!	EARLY RETURN!!
	}
//	stringstream msg3;
//	msg3 << "SendMessageToDevice: Message sent. " << message_in;
//	LOG_TRACE( threadName_in, "adp", msg3.str().c_str() );

	eventRecord = ReadMessageFromDevice( threadName_in );

//	stringstream msg4;
//	msg4 << "SendMessageToDevice: Port closed. " << message_in;
//	LOG_TRACE( threadName_in, "adp", msg4.str().c_str() );
	return eventRecord;
}

/// Passing tests on 2023-04-06 (mdg)
EventRecord TecanADP::ReadMessageFromDevice( const char * threadName_in )
{
	/// Create a record to return.
	EventRecord eventRecord;
	if (!m_serialPort)
	{
		LOG_WARN( threadName_in, "adp", "NULL serial port" );
		return eventRecord;
	}
	if (m_serialPort->GetState() == State::CLOSED)
	{
		LOG_WARN( threadName_in, "adp", "serial port is closed" );
		return eventRecord;
	}
	string totalReply = "";
	bool isFinished = false;
	int numberOfReads = 0;
	int bytesAvailable = 0;
	do {
		try {
			bytesAvailable = m_serialPort->Available();
		} catch (mn::CppLinuxSerial::Exception &e) {
			EventRecord availableRecord( EventType::Comm, CommEventType::CommReceiveFailure, ADPClassErrorCodes::DeviceBusyTimeout, "EXCEPTION while getting number of available bytes", e );
			LOG_ERROR( threadName_in, "adp", availableRecord.m_Text.c_str() );
			bytesAvailable = 0;
		}
		if (bytesAvailable > 0)
		{
			try {
				string partReply = "";
				m_serialPort->Read( partReply );
				totalReply = totalReply + partReply;
			} catch (mn::CppLinuxSerial::Exception &e) {
				EventRecord readRecord( EventType::Comm, CommEventType::CommReceiveFailure, ADPClassErrorCodes::DeviceBusyTimeout, "EXCEPTION while reading", e );
				LOG_ERROR( threadName_in, "adp", readRecord.m_Text.c_str() );
				try {
					m_serialPort->Close();
				} catch (mn::CppLinuxSerial::Exception &e) {
					EventRecord closeRecord( EventType::Comm, CommEventType::CommFailedToClose, ADPClassErrorCodes::DeviceBusyTimeout, "EXCEPTION while closing", e );
					LOG_ERROR( threadName_in, "adp", closeRecord.m_Text.c_str() );
					return closeRecord;	/// EARLY RETURN!!	EARLY RETURN!!
				}
				return readRecord;	/// EARLY RETURN!!	EARLY RETURN!!
			}
		}
		numberOfReads++;
		if (totalReply.size() > 1)
		{
			if ((int)totalReply[totalReply.size() - 2] == cCOMMAND_END_DELIMITER)
			{
				isFinished = true;
			}
		}
		usleep(1);
		if ((numberOfReads % 1000) == 0)
		{
			cout << "cast the line " << numberOfReads << " times." << endl;
		}
	} while(!isFinished && numberOfReads < 5000);
//	cout << "total reply:" << endl;
//	printInHex(totalReply);

	if ((isFinished) && (totalReply.size() >= cSTATUS_BYTE_LOCATION))
	{
		int checkSum = GetChecksum( totalReply );
		if (checkSum == (int) totalReply[totalReply.size() - 1])
		{
			/// Success case
			eventRecord.m_Type = EventType::ResponseReceived;
			eventRecord.m_Code = (CommEventType) totalReply[cSTATUS_BYTE_LOCATION - 1];
			eventRecord.m_Text = totalReply;
			eventRecord.m_Error = ADPClassErrorCodes::NoError;
//			stringstream msg;
//			msg << "totalReply=" << totalReply;
//			LOG_TRACE( threadName_in, "adp", msg.str().c_str() );
		}
		else
		{
			/// Checksum failed
			eventRecord.m_Type = EventType::ResponseChecksumError;
			eventRecord.m_Code = CommEventType::CommChecksumError;
			eventRecord.m_Text = "Checksum error";
			eventRecord.m_Error = ADPClassErrorCodes::CommandStringError;
			LOG_ERROR( threadName_in, "adp", "Checksum error" );
			return eventRecord;	/// EARLY RETURN!!	EARLY RETURN!!
		}
	}
	else
	{
		/// Failed (in some way) to get a full message
		eventRecord.m_Type = EventType::Comm;
		eventRecord.m_Code = CommEventType::CommReceiveFailure;
		eventRecord.m_Text = "receive failure";
		eventRecord.m_Error = ADPClassErrorCodes::DeviceBusyTimeout;
		LOG_ERROR( threadName_in, "adp", "receive failure" );
	}

	return eventRecord;
}

/// Passing tests on 2023-04-06 (mdg)
char TecanADP::GetChecksum( std::string str )
{
	int sum = 0;
	bool isAdding = false;
	for ( std::string::iterator it=str.begin(); it!=str.end(); ++it)
	{
		int c = (int) *it;
		if (c == cCOMMAND_START_DELIMITER) isAdding = true;
		if (isAdding)
		{
			sum = (sum ^ c) & 0xff;
		}
		if (c == cCOMMAND_END_DELIMITER) isAdding = false;
	}
	return (char) sum;
}

/// Passing tests on 2023-04-06 (mdg)
void TecanADP::printInHex(std::string str)
{
	cout << "str=[";
	for (unsigned int i=0; i < str.size(); i++ ) {
		cout << std::hex << (unsigned int)str[i] << ",";
	}
	cout << "], length=" << std::dec << str.size() << ", ";
}

/// Passing tests on 2023-04-06 (mdg)
/// Q1 requests status and extended commands.
/// Q (or Q0?) just requests status + standard-error. If it indicates extended errors, need to send Q1 to get them.
/// Every command sends you a status byte
/// If	I send any command and get a status of extended errors, issue a Q1 to get extended errors.
///
/// Use this on any response.
int TecanADP::ExtractBusyAndStdError( const char * threadName_in, const std::string response_in, bool & busy_out, int & stdError_out )
{
	int status = STATUS_OK;
	if (response_in.size() < (cRESPONSE_DATA_TAIL_LENGTH + cRESPONSE_DATA_START_POSITION - 1))
	{
		return STATUS_QUERY_FAILED; /// EARLY RETURN!!	 EARLY RETURN!!
	}

	int st = (int)response_in[3]; /// Extract the status byte.
	printInHex(response_in);
//	cout << std::hex << st;
	if ((st & 0xf0) == 0x40)
	{
		busy_out = true;
	}
	else if ((st & 0xf0) == 0x60)
	{
		busy_out = false;
	}
	else
	{
		status = STATUS_MATH_ERROR;
	}

	stdError_out = (st & 0x0f);

	return status;
}

/// Use this on responses from Q1 commands
int TecanADP::ExtractExtendedErrors( const char * threadName_in, const std::string response_in, std::string & extendedEerrorList_out )
{
	/// Assume the response is from a Q1 command. The expected payload size is 1.
	/// Based on that assumption, calculate the length of extended error bytes -- if any.
	int len = response_in.size() - cRESPONSE_DATA_TAIL_LENGTH - cRESPONSE_DATA_START_POSITION - 1;

	if (len < 0)
	{
		LOG_ERROR( threadName_in, "adp", "string length is lt 0" );
		return STATUS_QUERY_FAILED; // EARLY RETURN!!	EARLY RETURN!!
	}
	if (len == 0)
	{
		LOG_TRACE( threadName_in, "adp", "string length is eq 0" );
		extendedEerrorList_out = "";
	}
	if (len > 0)
	{
		LOG_TRACE( threadName_in, "adp", "string length is gt 0" );
		int start = cRESPONSE_DATA_START_POSITION;
		extendedEerrorList_out = response_in.substr( start, len );
	}
	return STATUS_OK;
}

/// Passing tests on 2023-04-06 (mdg)
/// Use this on responses that are expected to contain a string of *payload* data, such as part number.
/// Do not use this on a respose from Q1.
int TecanADP::ExtractPayload( const char * threadName_in, const std::string response_in, std::string & payload_out )
{
	int len = response_in.size() - cRESPONSE_DATA_TAIL_LENGTH - cRESPONSE_DATA_START_POSITION;
	if (len < 0)
	{
		LOG_ERROR( threadName_in, "adp", "string length is lt 0" );
		return STATUS_QUERY_FAILED; // EARLY RETURN!!	EARLY RETURN!!
	}
	if (len > 0)
	{
		LOG_TRACE( threadName_in, "adp", "string length is gt 0" );
		int start = cRESPONSE_DATA_START_POSITION;
		payload_out = response_in.substr( start, len );
	}
	return STATUS_OK;
}

/// Passing tests on 2023-04-06 (mdg)
std::string TecanADP::ADPClassErrorCodesToString( ADPClassErrorCodes code )
{
	switch( code )
	{
		case ADPClassErrorCodes::NoError :
			return "NoError";
			break;
		case ADPClassErrorCodes::CommandStringError :
			return "CommandStringError";
			break;
		case ADPClassErrorCodes::DeviceBusyTimeout :
			return "DeviceBusyTimeout";
			break;
		case ADPClassErrorCodes::ParameterLimitsExceeded :
			return "ParameterLimitsExceeded";
			break;
		case ADPClassErrorCodes::TopSpeedTooSmall :
			return "TopSpeedTooSmall";
			break;
		case ADPClassErrorCodes::StartSpeedTooBig :
			return "StartSpeedTooBig";
			break;
		case ADPClassErrorCodes::CutoffSpeedTooBig :
			return "CutoffSpeedTooBig";
			break;
		case ADPClassErrorCodes::PumpWorkingVolumeViolation :
			return "PumpWorkingVolumeViolation";
			break;
		case ADPClassErrorCodes::TipWorkingVolumeViolation :
			return "TipWorkingVolumeViolation";
			break;
	}
	return "UNKNOWN code";
}

std::string TecanADP::StandardErrorCodesToString( int stdError )
{
	switch( stdError )
	{
		case cmERROR_CODE_INITIALIZATION_ERROR :
			return "Initialization-Error";
			break;
		case cmERROR_CODE_INVALID_COMMAND :
			return "Invalid-Command-Error";
			break;
		case cmERROR_CODE_INVALID_OPERAND :
			return "Invalid-Operand-Error";
			break;
		case cmERROR_CODE_PRESSURE_SENSOR_PROBLEM :
			return "Pressure-Sensor-Error";
			break;
		case cmERROR_CODE_OVER_PRESSURE :
			return "Over-Pressure-Error";
			break;
		case cmERROR_CODE_LIQUID_LEVEL_DETECT_FAILURE :
			return "Liquid-Level-Detect-Failure-Error";
			break;
		case cmERROR_CODE_DEVICE_NOT_INITIALIZED :
			return "Device-Not-Initialized-Error";
			break;
		case cmERROR_CODE_TIP_EJECT_FAILURE :
			return "Tip-Eject-Failure-Error";
			break;
		case cmERROR_CODE_PLUNGER_OVERLOAD :
			return "Plunger-Overload-Error";
			break;
		case cmERROR_CODE_TIP_LOST_OR_NOT_PRESENT :
			return "Tip-Lost_Or-Not-Present-Error";
			break;
		case cmERROR_CODE_PLUNGER_MOVE_NOT_ALLOWED :
			return "Plunger-Move-Not-Allowed-Error";
			break;
		case cmERROR_CODE_EXTENDED_ERROR :
			return "Extended-Error";
			break;
		case cmERROR_CODE_NVMEM_ACCESS_FAILURE :
			return "NVMEM-Access-Failure-Error";
			break;
		case cmERROR_CODE_COMMAND_BUFFER_PROBLEM :
			return "Command-Buffer-Problem-Error";
			break;
		case cmERROR_CODE_COMMAND_BUFFER_OVERFLOW :
			return "Command-Buffer-Overflow-Error";
			break;
	}
	return "Unknown-error";
}

std::vector<std::string> extendedErrorCodes =
{
	"No error",						/// 0
	"Initilization error",
	"Invalid command",
	"Invalid operand",
	"Pressure sensor module",
	"Pressure sensor over limit",
	"Liquid level detect failure",
	"Device not yet initialized",
	"Tip eject failure",
	"Plunger overload",
	"Tip lost",						/// 10
	"Undefined error",
	"Undefined error",
	"FRAM access error",
	"Command buffer empty",
	"Command overflow",
	"Power-on self-test failed",
	"Watchdog reset",
	"Low voltage",
	"Pressure sensor not working",
	"Step loss detected",			 /// 20
	"No plunger steps detected",
	"Plunger home flag error",
	"Plunger home slot error",
	"Plunger reference voltage",
	"Hybrid liquid level detect failure",
	"Undefined error",
	"Plunger move terminate",
	"Undefined error",
	"Emergency brake"
	"Undefined error",				 /// 30
	"Undefined error",
	"Undefined error",
	"Application code checksum error",
	"Boot code checksum error",
	"Calibration table checksum error",
	"Detect algorithm detected an error",
	"No liquid detected during pressure algorithm",
	"FRAM checksum error",
	"Failure at end of pressure algorithm",
	"Undefined error",				 /// 40
	"Internal error",
	"Undefined error",
	"Undefined error",
	"Loop command imbalance"
};


EventRecord::EventRecord() :
	m_Type(EventType::None),
	m_Code(CommEventType::None),
	m_Error(ADPClassErrorCodes::NoError),
	m_Text(""),
	m_Payload("")
{
}

EventRecord::EventRecord( EventType type, CommEventType code, ADPClassErrorCodes error, const char * text, mn::CppLinuxSerial::Exception & e ) :
	m_Type( type ),
	m_Code( code ),
	m_Error( error ),
	m_Text(""),
	m_Payload("")
{
	stringstream temp;
	temp << text << ":" << e.what();
	m_Text = temp.str();
}


