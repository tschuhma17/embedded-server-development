/* include/MotionServices.h */

#ifndef __MotionServices_h__
#define __MotionServices_h__

#include <vector>

#include "USAFW/util/Logger.hpp"
#include "USAFW/util/MeasurementUnit.hpp"

#include "controllers/MotionController.hpp"
#include "MotionAxis.hpp"
#include "Transport.hpp"

#define DEFAULT_CONTROLLER_TIMEOUT_MS (5000)

namespace USAFW {

    class MotionServices {
    public:
        /* PUBLIC FUNCTIONS */
        // Constructor.
        // Creates a MotionServices interface object with a unique client ID that
        // can be used to issue commands or queries and get results.
        // Takes a timeout.
        MotionServices(Logger &loggerP);

        // Destructor
        virtual ~MotionServices();

        // Initializes motion services.  This function:
        // 1. Reads the specified configuration file.
        // 2. Instantiates all controllers described in those files.
        // 3. Constructs axes and transports described in those files.
        // 4. DOES NOT enable the system.
        void init(std::string config_file_pathP, unsigned int msec_timeout_P);

        void shutdown(unsigned int msec_timeout_P);

        // Energizes all motors in the system.  Motors may be enabled after this step,
        // but this step does not automatically enable them.
        void energize(unsigned int msec_timeout_P);

        // Deenergizes all motors in the system.  Motors will be disabled if they are
        // not already.
        void deenergize(unsigned int msec_timeout_P);

        // Enables all motors.  Motion commands may be sent after this step.
        // 
        // THROWS:
        // - NotEnergizedException if motors are not energized.
        // - TimeoutExpired if motors cannot be enabled in time.  In this case, walk
        //   each transport and axis and try again to troubleshoot.
        void enable(unsigned int msec_timeout_P);

        // Disables all motors.  Motion commands sent after this is done will throw
        // a NotEnabledException.
        void disable(unsigned int msec_timeout_P);

        // Gets a vector of all available motor controllers.
        inline const std::vector<MotionController*> &getMotionControllers(void) const { return m_controllers; }

        // Gets a vector of all available transports, regardless of controller.
        // To get transports for a specific controller, instead use the member
        // function of the same name (MotionController::GetTransports).
        inline const std::vector<Transport*> &getTransports(void) const { return m_transports; }

        // Gets a vector of all available axes, regardless of controller, and
        // without regard to any transports they may or may not be members of.
        // To get axes for a specific controller, use MotionController::GetAxes.
        // To get axes for a specific transport, use Transport::GetAxes.
        //inline const std::vector<MotionAxis*> &getAxes(void) const { return m_axes; }

        // Gets a specific axis by name, regardless of controller, and without
        // regard to any transport it may or may not be a member of.
        MotionAxis &getAxis(const std::string &nameP);
        
        // Gets the first MotionController found that contains a MotionAxis of the given name.
        MotionController *getMotionControllerOfAxis(const std::string &nameP);

        // Gets the controllerID of the first MotionController found that contains a MotionAxis of the given name.
        int getMotionControllerIdOfAxis(const std::string &nameP);

        // Gets the logger in use.
        inline Logger &getLogger(void) { return m_logger; }

        // PANIC / E-STOP.  Disables ALL axes on ALL transports on ALL controllers.
        // Returns a list of axes disabled.
        // 
        // The following actions are taken:
        // - Kill/abort bits/flags are set for all axes-- this aborts all moves in progress.
        // - All motors are de-energized.  CAUTION: THIS MAY CAUSE VERTICAL AXIS LOADS TO SHIFT OR DROP.
        // - All running programs are terminated.
        // - TODO FIXME: The Master Safety Loop normally-open relay should be de-energized (opened).
        //               The recommendation is that this should be a GPIO-based system that does not
        //               rely on controller logic, and can break the voltage supplied to the dedicated
        //               Master Enable inputs on motion controllers and related systems.
        //
        // WARNING: THE SYSTEM IS NOT ALLOWED TO RECOVER FROM THIS STATE WITHOUT A FULL RESTART.
        //          ATTEMPTING TO PERFORM ANY NON-INSPECTION ACTION AFTER CALLING THIS FUNCTION
        //          WILL THROW AN EmergencyStopException.
        std::vector<MotionAxis&> emergencyStop(void);

        /** Checks that the MotionServices instance, and all controllers, are in acceptable condition for an active
         *  command to execute.  Active commands are moves, jogs, motor-enables, and other work that can cause the system
         *  to change positions, power levels, etc.
         * 
         *  THROWS:
         *  - EmergencyStopException if this, or any other, controller has been emergency stopped.  Emergency stops
         *    propogate throughout the entire system, and cannot be cleared without a restart.
         */
        void preflightCheck(void);

    protected:
        /* INTERNAL FUNCTIONS */

        /* INTERNAL DATA */
        Logger      m_logger;

        // List of all motion controllers in the system.
        std::vector <MotionController *>    m_controllers;

        // List of all transports defined, regardless of controller.
        std::vector <Transport *>           m_transports;

        // List of all axes in the system, regardless of controller or transport.
        // OBSOLETED -- walk the controller list instead
        // (avoid double deletes)
        //std::vector <MotionAxis *>          m_axes;
    };

}

#endif
