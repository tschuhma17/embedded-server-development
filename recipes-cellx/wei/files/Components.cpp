#include "Components.hpp"
#include "Sundry.hpp"
#include <cctype> /// std::tolower
#include <algorithm> /// std::transform
#include <sstream>
#include <dirent.h>

using namespace std;

// TODO: Newer versions of the json-c library provide a
// json_object_get_uint64() function. Until that rev becomes
// available, we'll muddle along with this.
unsigned long json_object_get_uint64( json_object* obj )
{
	return (unsigned long) json_object_get_int64( obj );
}

json_object *json_object_new_uint64( unsigned long value )
{
	return json_object_new_int64( value );
}

std::ostream& operator << (std::ostream& os, const EditableType& e)
{
	return os << ((int) e);
}
std::istream& operator >> (std::istream& is, EditableType& e)
{
	char eAsStr[2] = {0};
	is.read(eAsStr, 1);
	e = (EditableType) stoi(eAsStr, nullptr, 10);
	return is;
}

std::ostream& operator << (std::ostream& os, const PersistType& e)
{
	return os << ((int) e);
}
std::istream& operator >> (std::istream& is, PersistType& e)
{
	char eAsStr[2] = {0};
	is.read(eAsStr, 1);
	e = (PersistType) stoi(eAsStr, nullptr, 10);
	return is;
}

///----------------------------------------------------------------------
/// Functions for FieldWithMetaData struct
///----------------------------------------------------------------------
/// Default constructor
FieldWithMetaData::FieldWithMetaData() : 
	name( "" ),
	editable( EditableType::Unknown ),
	persist( PersistType::Unknown ),
	dataType( "" ),
	baseUnit( "" ),
	min( "" ),
	max( "" ),
	setting( "" ),
	scale( "" ),
	pointsPastDecimal( "" ),
	displayDirective( "" ),
	displayMin( "" ),
	displayMax( "" ),
	arrayName( "" ),
	arrayIndex( 0 ),
	valueState( FieldState::Undefined ),
	editableState( FieldState::Undefined ),
	persistState( FieldState::Undefined ),
	dataTypeState( FieldState::Undefined ),
	minState( FieldState::Undefined ),
	maxState( FieldState::Undefined )
{
}

/// Constructor that initializes non-array fields. Is used to initialize all default components
/// except some properties in fluorescence_light_filter, annular_ring_holder, and optical_column.
FieldWithMetaData::FieldWithMetaData( std::string nm, EditableType ed, PersistType pst, std::string dt, std::string bu, std::string mi, std::string ma,
									  std::string val, std::string sc, std::string ppd, std::string ddir, std::string dmi, std::string dma ) :
	name( nm ),
	editable( ed ),
	persist( pst ),
	dataType( dt ),
	baseUnit( bu ),
	min( mi ),
	max( ma ),
	setting( val ),
	scale( sc ),
	pointsPastDecimal( ppd ),
	displayDirective( ddir ),
	displayMin( dmi ),
	displayMax( dma ),
	arrayName( "" ),
	arrayIndex( 0 ),
	valueState( FieldState::Undefined ),
	editableState( FieldState::Undefined ),
	persistState( FieldState::Undefined ),
	dataTypeState( FieldState::Undefined ),
	minState( FieldState::Undefined ),
	maxState( FieldState::Undefined )
{
}

/// Constructor that initializes fields, including arrays. Is used to initialize
/// some properties of fluorescence_light_filter, annular_ring_holder, and optical_column components.
FieldWithMetaData::FieldWithMetaData( std::string nm, EditableType ed, PersistType pst, std::string dt, std::string bu, std::string mi, std::string ma,
									  std::string val, std::string sc, std::string ppd, std::string ddir, std::string dmi, std::string dma,
									  std::string anm, int ai ) :
	name( nm ),
	editable( ed ),
	persist( pst ),
	dataType( dt ),
	baseUnit( bu ),
	min( mi ),
	max( ma ),
	setting( val ),
	scale( sc ),
	pointsPastDecimal( ppd ),
	displayDirective( ddir ),
	displayMin( dmi ),
	displayMax( dma ),
	arrayName( anm ),
	arrayIndex( ai ),
	valueState( FieldState::Undefined ),
	editableState( FieldState::Undefined ),
	persistState( FieldState::Undefined ),
	dataTypeState( FieldState::Undefined ),
	minState( FieldState::Undefined ),
	maxState( FieldState::Undefined )
{
}

/// Copy constructor
FieldWithMetaData::FieldWithMetaData(const FieldWithMetaData& f) : 
	name( f.name ),
	editable( f.editable ),
	persist( f.persist ),
	dataType( f.dataType ),
	baseUnit( f.baseUnit ),
	min( f.min ),
	max( f.max ),
	setting( f.setting ),
	scale( f.scale ),
	pointsPastDecimal( f.pointsPastDecimal ),
	displayDirective( f.displayDirective ),
	displayMin( f.displayMin ),
	displayMax( f.displayMax ),
	arrayName( f.arrayName ),
	arrayIndex( f.arrayIndex ),
	valueState( f.valueState ),
	editableState( f.editableState ),
	persistState( f.persistState ),
	dataTypeState( f.dataTypeState ),
	minState( f.minState ),
	maxState( f.maxState )
{
}


long FieldWithMetaData::GetValueAsLong(void)
{
	if (setting.length() == 0)
	{
		stringstream msg;
		msg << "Parameter " << name << " has empty setting string";
		LOG_WARN( "unk", "params", msg.str().c_str() );
		return 0L;
	}
	return StringToLong( setting );
}

unsigned long FieldWithMetaData::GetValueAsULong(void)
{
	if (setting.length() == 0)
	{
		stringstream msg;
		msg << "Parameter " << name << " has empty setting string";
		LOG_WARN( "unk", "params", msg.str().c_str() );
		return 0L;
	}
	return StringToULong( setting );
}

float FieldWithMetaData::GetValueAsFloat(void)
{
	if (setting.length() == 0)
	{
		stringstream msg;
		msg << "Parameter " << name << " has empty setting string";
		LOG_WARN( "unk", "params", msg.str().c_str() );
		return 0.0;
	}
	return StringToFloat( setting );
}

bool FieldWithMetaData::GetValueAsBool(void)
{
	if (setting.length() == 0)
	{
		stringstream msg;
		msg << "Parameter " << name << " has empty setting string";
		LOG_WARN( "unk", "params", msg.str().c_str() );
	}
	std::transform( setting.begin(), setting.end(), setting.begin(), (int(*)(int)) std::tolower );
	return (setting == "true");
}

///----------------------------------------------------------------------
/// FieldToJsonObject
/// Used by PropertyFieldsToJsonObject for property fields whose data type is defined
/// by the data_type field.
///----------------------------------------------------------------------
json_object * FieldWithMetaData::FieldToJsonObject( const std::string & field, const std::string & dataType )
{
	if (dataType == STRING)
	{
		return json_object_new_string( field.c_str() );
	}
	/// All datatypes with names beginning "enum" have their settings stored as strings.
	if (dataType.compare( 0, 4, "enum" ) == 0)
	{
		return json_object_new_string( field.c_str() );
	}
	if (dataType == BOOLEAN)
	{
		string fieldLC = field;
		std::transform( fieldLC.begin(), fieldLC.end(), fieldLC.begin(), (int(*)(int)) std::tolower );
		json_bool s = (fieldLC == "true") ? 1 : 0;
		return json_object_new_boolean( s );
	}
	if (dataType == FLOAT)
	{
		float f = 0.0;
		if (field.length() == 0)
		{
			LOG_WARN( "unk", "properties", "field is null" );
		}
		else
		{
			f = std::stof( field.c_str() );
			/// Debugging a problem with scientific notation
			stringstream msg;
			msg << field << "=" << f;
			LOG_DEBUG( "unk", "properties", msg.str().c_str() );
		}
		size_t found = field.find('e');
		if (found == std::string::npos)
		{
			return json_object_new_double( f );
		}
		else
		{
			return json_object_new_double_s( f, field.c_str() );
		}
	}
	if ((dataType == INT16) || (dataType == INT32))
	{
		long l = 0L;
		if (field.length() == 0)
		{
			LOG_WARN( "unk", "properties", "field is null" );
		}
		else
		{
			l = StringToLong( field );
		}
		return json_object_new_int64( l );
	}
	if ((dataType == UINT16) || (dataType == UINT32))
	{
		unsigned long ul = 0L;
		if (field.length() == 0)
		{
			LOG_WARN( "unk", "properties", "field is null" );
		}
		else
		{
			ul = StringToULong( field );
		}
		return json_object_new_int64( ul );
	}
	stringstream msg;
	msg << dataType << " not valid";
	LOG_ERROR( "unk", "properties", msg.str().c_str() );
	return nullptr;
}

///----------------------------------------------------------------------
/// FieldToJsonObjectVerbatim
/// Used by PropertyFieldsToJsonObject for property fields that only serve
/// as display directives. At present, there are 4 of these:
/// points_past_decimal, display_directive, display_min_value, and
/// display_max_value.
/// These are more free-form and not guaranteed to be the same datatype
/// as the property.
///----------------------------------------------------------------------
json_object * FieldWithMetaData::FieldToJsonObjectVerbatim( const std::string & field )
{
    json_object *jobj = nullptr;

	if (field.length() == 0)
	{
		LOG_TRACE( "unk", "properties", "returning NULL" );
		return jobj;
	}
	
	enum json_tokener_error jerr;
	jobj = json_tokener_parse_verbose( field.c_str(), &jerr );
	if (jerr == json_tokener_success)
	{
		return jobj;
	}
	else
	{
		stringstream msg;
		msg << field << " could not be parsed";
		LOG_ERROR( "unk", "properties", msg.str().c_str() );
		return nullptr;
	}
}

///----------------------------------------------------------------------
/// PropertyFieldsToJsonObject
/// Used for GetProperties command.
///----------------------------------------------------------------------
/// This is the inverse of DesHelper::ExtractProperty
bool FieldWithMetaData::PropertyFieldsToJsonObject(json_object* jsonOutObj)
{
	int result = 0;
	bool succeeded = true;
	string dt = dataType;
	json_object *propertyObj = json_object_new_object();
	
	/// Pack the Setting
	result = json_object_object_add( propertyObj, SETTING, FieldToJsonObject( setting, dt ) );
	succeeded &= ( result == 0 );
	/// Pack the editable_type
	string enumAsString = EditableTypeEnumToStringName( editable );
	result = json_object_object_add( propertyObj, EDITABILITY_TYPE, json_object_new_string( enumAsString.c_str() ) );
	succeeded &= ( result == 0 );
	/// Pack the persist_type
	enumAsString = PersistTypeEnumToStringName( persist );
	result = json_object_object_add( propertyObj, PERSIST_TYPE, json_object_new_string( enumAsString.c_str() ) );
	succeeded &= ( result == 0 );
	/// Pack the data_type
	result = json_object_object_add( propertyObj, DATA_TYPE, json_object_new_string( dt.c_str() ) );
	succeeded &= ( result == 0 );
	/// Pack the min
	result = json_object_object_add( propertyObj, ES_MIN_VALUE, FieldToJsonObject( min, dt ) );
	succeeded &= ( result == 0 );
	/// Pack the max
	result = json_object_object_add( propertyObj, ES_MAX_VALUE, FieldToJsonObject( max, dt ) );
	succeeded &= ( result == 0 );
	/// Pack the base_unit
	result = json_object_object_add( propertyObj, BASE_UNIT, json_object_new_string( baseUnit.c_str() ) );
	succeeded &= ( result == 0 );
	/// Pack the scale_factor
	result = json_object_object_add( propertyObj, SCALE_FACTOR, FieldToJsonObject( scale, dt ) );
	succeeded &= ( result == 0 );
	/// Pack the points_past_decimal
	result = json_object_object_add( propertyObj, POINTS_PAST_DECIMAL, FieldToJsonObjectVerbatim( pointsPastDecimal ) );
	succeeded &= ( result == 0 );
	/// Pack the display_directive
	result = json_object_object_add( propertyObj, DISPLAY_DIRECTIVE, FieldToJsonObjectVerbatim( displayDirective ) );
	succeeded &= ( result == 0 );
	/// Pack the display_min_value
	result = json_object_object_add( propertyObj, DISPLAY_MIN_VALUE, FieldToJsonObjectVerbatim( displayMin ) );
	succeeded &= ( result == 0 );
	/// Pack the display_max_value
	result = json_object_object_add( propertyObj, DISPLAY_MAX_VALUE, FieldToJsonObjectVerbatim( displayMax ) );
	succeeded &= ( result == 0 );
	
	result = json_object_object_add( jsonOutObj, name.c_str(), propertyObj );
	succeeded &= ( result == 0 );

	return succeeded;
}


string IntToStrOfLength(const int i, const int length)
{
	std::ostringstream ss;
	ss << std::setw(length) << std::setfill('0') << i;
	return ss.str();
}

string MakeLengthString(const std::string& str)
{
	std::ostringstream ss;
	return IntToStrOfLength( str.length(), STRING_LENGTH_WIDTH );
}
/// Serialize the data members but not the states. States are ephemeral;
/// They are only used while some kind of check is being done on an entire
/// CPD.
std::ostream& operator << (std::ostream& os, const FieldWithMetaData& f)
{
	string name_len = MakeLengthString(f.name);
	string setting_len = MakeLengthString(f.setting);
	string dataType_len = MakeLengthString(f.dataType);
	string min_len = MakeLengthString(f.min);
	string max_len = MakeLengthString(f.max);
	string baseUnit_len = MakeLengthString(f.baseUnit);
	string scale_len = MakeLengthString(f.scale);
	string pointsPastDecimal_len = MakeLengthString(f.pointsPastDecimal);
	string displayDirective_len = MakeLengthString(f.displayDirective);
	string displayMin_len = MakeLengthString(f.displayMin);
	string displayMax_len = MakeLengthString(f.displayMax);
	string array_name_len = MakeLengthString(f.arrayName);
	string array_index = IntToStrOfLength( f.arrayIndex, STRING_LENGTH_WIDTH );

	return os << name_len << f.name
			  << setting_len << f.setting
			  << f.editable << f.persist
			  << dataType_len << f.dataType
			  << min_len << f.min
			  << max_len << f.max
			  << baseUnit_len << f.baseUnit
			  << scale_len << f.scale
			  << pointsPastDecimal_len << f.pointsPastDecimal
			  << displayDirective_len << f.displayDirective
			  << displayMin_len << f.displayMin
			  << displayMax_len << f.displayMax		  
			  << array_name_len << f.arrayName
			  << array_index;
}

size_t GetStrLength( std::istream& is )
{
	char lengthAsStr[STRING_LENGTH_WIDTH+1] = { 0 };
	is.read(lengthAsStr, STRING_LENGTH_WIDTH);
	if (is.eof())
		return 0;
	return (size_t) stoi(lengthAsStr, nullptr, 10);
}
std::string GetStringOfLength( std::istream& is, size_t len )
{
	char * buffer = new char[len+1];
	is.read(buffer, len);
	if (is.eof())
	{
		return ""; /// EARLY RETURN!
	}
	buffer[len] = '\0';
	string str = buffer;
	delete [] buffer;
	return str;
}
std::istream& operator >> (std::istream& is, FieldWithMetaData& f)
{
	size_t len = GetStrLength( is );
	if (is.eof()) return is;   /// EARLY RETURN!
	f.name = GetStringOfLength( is, len );
	
	len = GetStrLength( is );
	f.setting = GetStringOfLength( is, len );

	is >> f.editable >> f.persist;
	
	len = GetStrLength( is );
	f.dataType = GetStringOfLength( is, len );

	len = GetStrLength( is );
	f.min = GetStringOfLength( is, len );

	len = GetStrLength( is );
	f.max = GetStringOfLength( is, len );

	len = GetStrLength( is );
	f.baseUnit = GetStringOfLength( is, len );

	len = GetStrLength( is );
	f.scale = GetStringOfLength( is, len );

	len = GetStrLength( is );
	f.pointsPastDecimal = GetStringOfLength( is, len );

	len = GetStrLength( is );
	f.displayDirective = GetStringOfLength( is, len );

	len = GetStrLength( is );
	f.displayMin = GetStringOfLength( is, len );

	len = GetStrLength( is );
	f.displayMax = GetStringOfLength( is, len );

	len = GetStrLength( is );
	f.arrayName = GetStringOfLength( is, len );
	
	f.arrayIndex = GetStrLength( is );

	return is;
}

bool operator==( const FieldWithMetaData& lhs, const FieldWithMetaData& rhs )
{
	return ( (lhs.name.compare( rhs.name ) == 0) &&
			 (lhs.setting.compare( rhs.setting ) == 0) &&
			 (lhs.editable == rhs.editable) &&
			 (lhs.persist == rhs.persist) &&
			 (lhs.dataType.compare( rhs.dataType ) == 0) &&
			 (lhs.min.compare( rhs.min ) == 0) &&
			 (lhs.max.compare( rhs.max ) == 0) &&
			 (lhs.baseUnit.compare( rhs.baseUnit ) == 0) &&
			 (lhs.scale.compare( rhs.scale ) == 0) &&
			 (lhs.pointsPastDecimal.compare( rhs.pointsPastDecimal ) == 0) &&
			 (lhs.displayDirective.compare( rhs.displayDirective ) == 0) &&
			 (lhs.displayMin.compare( rhs.displayMin ) == 0) &&
			 (lhs.displayMax.compare( rhs.displayMax ) == 0) &&
			 (lhs.arrayName.compare( rhs.arrayName ) == 0) &&
			 (lhs.arrayIndex == rhs.arrayIndex) );
}


///----------------------------------------------------------------------
/// Functions for ComponentProperties struct
///----------------------------------------------------------------------
std::ostream& operator << (std::ostream& os, const ComponentProperties& p)
{
	std::vector<FieldWithMetaData> subsetToPersist;
	os << p.componentType;
	stringstream msg;
	msg << "Out the stream ";
    for (FieldWithMetaData field : p.fieldVector) {
		if ((field.persist == PersistType::Persist) || (field.persist == PersistType::HardPersist))
		{
			os << field;
			msg << field.name << "=" << field.setting << " ";
		}
    }
    LOG_DEBUG( "unk", "properties", msg.str().c_str() );
	return os;
}
std::istream& operator >> (std::istream& is, ComponentProperties& p)
{
	int count = 0;
	p.fieldVector.resize( 0 );
	is >> p.componentType;
	while( !is.eof() )
	{
		count++;
		FieldWithMetaData field( "", EditableType::EditableCPDOnly, PersistType::NotPersist, "", "", "", "", "", "", "", "", "", "" );
		is >> field;
		if (field.name != "")
		{
			p.fieldVector.push_back(field);
		}
	}
	if (count >= 200)
	{
		LOG_ERROR( "unk", "properties", "Wheels are spinning" );
	}
	return is;
}

FieldWithMetaData ComponentProperties::GetFieldByName( const char * nameIn )
{
    for (FieldWithMetaData property : fieldVector) {
		if (property.name == nameIn)
		{
			return property;
		}
    }
    FieldWithMetaData empty = { {}, {EditableType::EditableApplicationOnly}, {PersistType::Remove}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {} };
    stringstream msg;
    msg << "returning empty for '" << nameIn << "'";
	LOG_WARN( "unk", "properties", msg.str().c_str() );

	return 	empty;
}

FieldWithMetaData ComponentProperties::GetFieldByName( std::string& nameIn )
{
	return GetFieldByName( nameIn.c_str() );
}

/// Returns true if updated, false if added.
void ComponentProperties::SetField( const FieldWithMetaData fieldIn )
{
	bool found = false;
	bool doAdd = true;
	for (std::vector<FieldWithMetaData>::iterator it = fieldVector.begin();
		 ((it != fieldVector.end()) && !found);
		 ++it)
	{
		if (it->name == fieldIn.name) /// match found
		{
			found = true;
			if (*it == fieldIn)
			{
				/// Do nothing because there is no change needed.
				doAdd = false;
			}
			else
			{
				/// Erase the old one and add the new one after we fall through to end of loop.
				fieldVector.erase(it);
			}
		}
    }
    if (doAdd)
    {
		fieldVector.push_back( fieldIn );
	}
}

string ComponentProperties::ComposeCpdFileName( ComponentType componentType, unsigned long cpdId )
{
	stringstream ss;
	ss << "c" << componentType << "id" << cpdId << ".cpd";
	return ss.str();
}

std::vector<unsigned long> ComponentProperties::DetectListOfCpds( std::string & directoryPath, ComponentType componentType )
{
	stringstream ss;
	ss << "c" << componentType << "id";
	string filter = ss.str();
	int filterLength = filter.length();
	std::vector<unsigned long> v;
	if (auto cpdDir = opendir( directoryPath.c_str() ))
	{
		while (auto f = readdir( cpdDir ))
		{
			string fstr = f->d_name;
			cout << "DetectListOfCpds checking " << fstr << endl;
			if (filterLength < fstr.length())
			{
				/// Identify a file of the same component type
				if (filter == fstr.substr( 0, filterLength ))
				{
					/// Extract the cpd_id from the file name. Expect it to be numerals following the part that matched the filter.
					int i = 0;
					unsigned long cpdId = 0L;
					while ((fstr[filterLength + i] >= '0') && (fstr[filterLength + i] <= '9') && (i < fstr.length()))
					{
						cpdId = cpdId * 10L + (unsigned long) (fstr[filterLength + i] - '0');
						i++;
					}
					v.push_back( cpdId );
//					cout << "DetectListOfCpds(): cpd_id: " << cpdId << endl;
				}
			}
		}
		closedir( cpdDir );
	}
	else
	{
		stringstream msg;
		msg << "failed to search directory " << directoryPath;
		LOG_ERROR( "unk", "properties", msg.str().c_str() );
	}
	return v;
}


FieldState CheckStringAsFloat( std::string& value )
{
	FieldState valueState = FieldState::Missing;
	float f = 0.0;
	try
	{
		f = std::stof( value );
		valueState = FieldState::OK;
//		cout << "CheckStringAsFloat() passed." << endl;
	}
	catch ( const std::invalid_argument& e )
	{
		valueState = FieldState::Undefined;
		LOG_ERROR( "unk", "properties", "returning undefined." );
	}
	catch ( const std::out_of_range& e )
	{
		valueState = FieldState::OutOfRange;
		LOG_ERROR( "unk", "properties", "returning out-of-range." );
	}
	return valueState;
}

FieldState CheckStringAsInt16( std::string& value )
{
	FieldState valueState = FieldState::Missing;
	int i = 0;
	try
	{
		i = std::stoi( value );
		valueState = FieldState::OK;

		stringstream msg;
		msg << "converting an Int16. Result is " << i;
		LOG_TRACE( "unk", "properties", msg.str().c_str() );
	}
	catch ( const std::invalid_argument& e )
	{
		valueState = FieldState::Undefined;
		cerr << "invalid_argument exception" << endl;
		LOG_ERROR( "unk", "properties", "invalid_argument exception" );
	}
	catch ( const std::out_of_range& e )
	{
		valueState = FieldState::OutOfRange;
		LOG_ERROR( "unk", "properties", "out-of-range exception" );
	}
	return valueState;
}

FieldState CheckStringAsUint16( std::string& value )
{
	FieldState valueState = FieldState::Missing;
	unsigned long l = 0L;
	try
	{
		l = std::stoul( value );
		if (l >= 0x10000)
		{
			valueState = FieldState::OutOfRange;
			LOG_ERROR( "unk", "properties", "out of range" );
		}
		else
		{
			valueState = FieldState::OK;
			stringstream msg;
			msg << "converting an unsigned Int16. Result is " << l;
			LOG_TRACE( "unk", "properties", msg.str().c_str() );
		}
	}
	catch ( const std::invalid_argument& e )
	{
		valueState = FieldState::Undefined;
		LOG_ERROR( "unk", "properties", "invalid_argument exception" );
	}
	catch ( const std::out_of_range& e )
	{
		valueState = FieldState::OutOfRange;
		cerr << "out_of_range exception" << endl;
		LOG_ERROR( "unk", "properties", "out-of-range exception" );
	}
	return valueState;
}

FieldState CheckStringAsInt32( std::string& value )
{
	FieldState valueState = FieldState::Missing;
	long l = 0L;
	try
	{
		l = std::stol( value );
		valueState = FieldState::OK;
	}
	catch ( const std::invalid_argument& e )
	{
		valueState = FieldState::Undefined;
		LOG_ERROR( "unk", "properties", "undefined exception" );
	}
	catch ( const std::out_of_range& e )
	{
		valueState = FieldState::OutOfRange;
		LOG_ERROR( "unk", "properties", "out-of-range exception" );
	}
	return valueState;
}

FieldState CheckStringAsUint32( std::string& value )
{
	FieldState valueState = FieldState::Missing;
	unsigned long l = 0L;
	try
	{
		l = std::stoull( value );
		valueState = FieldState::OK;
	}
	catch ( const std::invalid_argument& e )
	{
		valueState = FieldState::Undefined;
		LOG_ERROR( "unk", "properties", "undefined exception" );
	}
	catch ( const std::out_of_range& e )
	{
		valueState = FieldState::OutOfRange;
		LOG_ERROR( "unk", "properties", "out-of-range exception" );
	}
	return valueState;
}

FieldState CheckStringAsEnumCalType( std::string& value )
{
	if ((value == ENUM_UNKNOWN) ||
		(value == ENUM_NONE) ||
		(value == ENUM_ZERO_ORDER_POLY) ||
		(value == ENUM_FIRST_ORDER_POLY_FORCE_0_INTERCEPT) ||
		(value == ENUM_FIRST_ORDER_POLY) ||
		(value == ENUM_SECOND_ORDER_POLY))
	{
		return FieldState::OK;
	}
	else
	{
		LOG_ERROR( "unk", "properties", "out-of-range exception" );
		return FieldState::OutOfRange;
	}
}

FieldState CheckStringAsWhiteLightType( std::string& value )
{
	if ((value == ENUM_UNKNOWN) ||
		(value == ENUM_INCANDESCENT) ||
		(value == ENUM_LED))
	{
		return FieldState::OK;
	}
	else
	{
		LOG_ERROR( "unk", "properties", "out-of-range exception" );
		return FieldState::OutOfRange;
	}
}

FieldState CheckStringAsBoolean( std::string value )
{
	/// Modify value to lower case for easy checking. This modification is why
	/// CheckStringAsBoolean accepts the string by value instead of reference.
	std::transform( value.begin(), value.end(), value.begin(), (int(*)(int)) std::tolower );
	if ((value == "true") ||
	    (value == "false"))
	{
		return FieldState::OK;
	}
	else
	{
		LOG_ERROR( "unk", "properties", "out-of-range exception" );
		return FieldState::OutOfRange;
	}
}

FieldState CheckStringAsEnumPumpType( std::string& value )
{
	if ((value == ENUM_PERISTALTIC) ||
		(value == ENUM_SYRINGE) ||
		(value == ENUM_UNKNOWN))
	{
		return FieldState::OK;
	}
	else
	{
		LOG_ERROR( "unk", "properties", "out-of-range exception" );
		return FieldState::OutOfRange;
	}
}


///----------------------------------------------------------------------
/// Functions and defaults for CxD struct
///----------------------------------------------------------------------
ComponentProperties defaultCxD =
{
	ComponentType::CxD,
	{ { {"vendor"}, EditableType::EditableFixed, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Cell X Technologies"}, {}, {}, {}, {}, {} },
	  { {"model"}, EditableType::EditableFixed, PersistType::HardPersist, {"string"}, {}, {}, {}, {"CellX CxD 2.3"}, {}, {}, {}, {}, {} },
	  { {"serial_number"}, EditableType::EditableFixed, PersistType::HardPersist, {"string"}, {}, {}, {}, {"10000"}, {}, {}, {}, {}, {} },
	  { {"build_site"}, EditableType::EditableFixed, PersistType::HardPersist, {"string"}, {}, {}, {}, {}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {}, {}, {}, {}, {}, {} },
	  { {"scan_movement_delay_slow"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint16_t"}, {"millisecond"}, {"0"}, {"65535"}, {"0"}, {"1"}, {"0"}, {}, {"0"}, {"2000"} },
	  { {"scan_movement_delay_medium"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint16_t"}, {"millisecond"}, {"0"}, {"65535"}, {"0"}, {"1"}, {"0"}, {}, {"0"}, {"2000"} },
	  { {"scan_movement_delay_fast"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint16_t"}, {"millisecond"}, {"0"}, {"65535"}, {"0"}, {"1"}, {"0"}, {}, {"0"}, {"2000"} } },
	{ "cxd.cfg" },
	{ (std::string (CONFIG_DIR) + "/cxd.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for XX struct
///----------------------------------------------------------------------
ComponentProperties defaultAxisXX =
{
	ComponentType::XXAxis,
	{ { {"controller_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"xx_axis_resolution"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm"}, {"0"}, {"4294967295"}, {"100"}, {}, {}, {}, {}, {} },
	  { {"xx_axis_error_band"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm"}, {"0"}, {"4294967295"}, {"1000"}, {}, {}, {}, {}, {} },
	  { {"xx_maximum_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"1000000000"}, {"600000000"}, {"1"}, {"2"}, {}, {"0"}, {"1000000000"} },
	  { {"xx_set_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"600000000"}, {"100000000"}, {"1"}, {"2"}, {}, {"0"}, {"600000000"} },
	  { {"xx_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"150000000"}, {}, {}, {}, {}, {} },
	  { {"xx_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"150000000"}, {}, {}, {}, {}, {} },
	  { {"xx_jogging_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"600000000"}, {"25000000"}, {}, {}, {}, {}, {} },
	  { {"xx_jogging_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"500000000"}, {}, {}, {}, {}, {} },
	  { {"xx_jogging_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"500000000"}, {}, {}, {}, {}, {} },
	  { {"xx_default_location_offset"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-221000000"}, {}, {}, {}, {}, {} },
	  { {"xx_z_tip_to_tip_working_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-223510000"}, {}, {}, {}, {}, {} },
	  { {"xx_zz_tip_to_tip_working_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-171463200"}, {}, {}, {}, {}, {} },
	  { {"xx_zzz_tip_to_tip_working_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-111000000"}, {}, {}, {}, {}, {} },
	  { {"xx_z_tip_to_tip_skew_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-92980000"}, {}, {}, {}, {}, {} },
	  { {"xx_zz_tip_to_tip_skew_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-41000000"}, {}, {}, {}, {}, {} },
	  { {"xx_z_tip_to_waste_1_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-71200000"}, {}, {}, {}, {}, {} },
	  { {"xx_z_tip_to_waste_2_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-11200000"}, {}, {}, {}, {}, {} },
	  { {"xx_z_tip_to_waste_3_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"36800000"}, {}, {}, {}, {}, {} },
	  { {"xx_zz_tip_to_waste_1_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-21000000"}, {}, {}, {}, {}, {} },
	  { {"xx_zz_tip_to_waste_2_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"39000000"}, {}, {}, {}, {}, {} },
	  { {"xx_zz_tip_to_waste_3_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"87000000"}, {}, {}, {}, {}, {} },
	  { {"xx_zzz_tip_to_waste_1_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"43000000"}, {}, {}, {}, {}, {} },
	  { {"xx_zzz_tip_to_waste_2_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"102000000"}, {}, {}, {}, {}, {} },
	  { {"xx_zzz_tip_to_waste_3_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"148500000"}, {}, {}, {}, {}, {} },
	  { {"xx_z_tip_to_tip_tray_right_edge"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"180945000"}, {}, {}, {}, {}, {} },
	  { {"xx_zz_tip_to_tip_tray_right_edge"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"231685000"}, {}, {}, {}, {}, {} },
	  { {"xx_z_tip_to_key_entry_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"123400000"}, {}, {}, {}, {}, {} },
	  { {"xx_zz_tip_to_key_entry_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"173650000"}, {}, {}, {}, {}, {} },
	  { {"xx_key_entry_to_key_work_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"8880000"}, {}, {}, {}, {}, {} },
	  { {"xx_z_tip_to_key_work_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"114400000"}, {}, {}, {}, {}, {} },
	  { {"xx_zz_tip_to_key_work_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"165500000"}, {}, {}, {}, {}, {} },
	  { {"home_location"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"homing_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"25000000"}, {}, {}, {}, {}, {} },
	  { {"homing_acceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"500000000"}, {}, {}, {}, {}, {} },
	  { {"homing_deceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"500000000"}, {}, {}, {}, {}, {} },
	  { {"max_allowable_position_error"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"5"}, {}, {}, {}, {}, {} },
	  { {"end_of_travel_limit_position_+"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"224402300"}, {}, {}, {}, {}, {} },
	  { {"end_of_travel_limit_position_-"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-223855300"}, {}, {}, {}, {}, {} },
	  { {"soft_travel_limit_position_+"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"224302300"}, {}, {}, {}, {}, {} },
	  { {"soft_travel_limit_position_-"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-223755300"}, {}, {}, {}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumNone"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_factor_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_factor_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_factor_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "axis_xx.cfg" },
	{ (std::string (CONFIG_DIR) + "/axis_xx.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for YY struct
///----------------------------------------------------------------------
ComponentProperties defaultAxisYY =
{
	ComponentType::YYAxis,
	{ { {"controller_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"yy_axis_resolution"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm"}, {"0"}, {"4294967295"}, {"2500"}, {}, {}, {}, {}, {} },
	  { {"yy_axis_error_band"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm"}, {"0"}, {"4294967295"}, {"12500"}, {}, {}, {}, {}, {} },
	  { {"yy_maximum_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"1000000000"}, {"250000000"}, {"1"}, {"2"}, {}, {"0"}, {"1000000000"} },
	  { {"yy_set_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"250000000"}, {"100000000"}, {"1"}, {"2"}, {}, {"0"}, {"250000000"} },
	  { {"yy_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"150000000"}, {}, {}, {}, {}, {} },
	  { {"yy_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"150000000"}, {}, {}, {}, {}, {} },
	  { {"yy_jogging_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"250000000"}, {"15000000"}, {}, {}, {}, {}, {} },
	  { {"yy_jogging_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"yy_jogging_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"yy_default_location_offset"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"yy_key_to_tip_path"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"158700000"}, {}, {}, {}, {}, {} },
	  { {"yy_tip_tray_top_edge_to_tip_path"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"184025000"}, {}, {}, {}, {}, {} },
	  { {"yy_tip_tray_cxpm_grab_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"home_location"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"homing_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"homing_acceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"homing_deceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"max_allowable_position_error"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"2"}, {}, {}, {}, {}, {} },
	  { {"end_of_travel_limit_position_+"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"307850000"}, {}, {}, {}, {}, {} },
	  { {"end_of_travel_limit_position_-"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-3327500"}, {}, {}, {}, {}, {} },
	  { {"soft_travel_limit_position_+"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"306850000"}, {}, {}, {}, {}, {} },
	  { {"soft_travel_limit_position_-"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-2327500"}, {}, {}, {}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumNone"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_factor_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_factor_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_factor_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "axis_yy.cfg" },
	{ (std::string (CONFIG_DIR) + "/axis_yy.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for XY struct
///----------------------------------------------------------------------
ComponentProperties defaultAxesXY = 
{
	ComponentType::XYAxes,
	{ { {"controller_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Parker Hannifin"}, {}, {}, {}, {}, {} },
	  { {"controller_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"ACR 7000"}, {}, {}, {}, {}, {} },
	  { {"controller_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {}, {}, {}, {}, {}, {} },
	  { {"x_driver_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Parker Hannifin"}, {}, {}, {}, {}, {} },
	  { {"x_driver_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"ACR 7000"}, {}, {}, {}, {}, {} },
	  { {"x_driver_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"x_driver_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"y_driver_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Parker Hannifin"}, {}, {}, {}, {}, {} },
	  { {"y_driver_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"ACR 7000"}, {}, {}, {}, {}, {} },
	  { {"y_driver_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"y_driver_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"x_motor_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"x_motor_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"x_motor_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"x_motor_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"x_motor_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"y_motor_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"y_motor_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"y_motor_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"y_motor_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"y_motor_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"x_axis_resolution"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm"}, {"0"}, {"4294967295"}, {"100"}, {}, {}, {}, {}, {} },
	  { {"x_axis_error_band"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm"}, {"0"}, {"4294967295"}, {"2500"}, {}, {}, {}, {}, {} },
	  { {"x_maximum_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000000"}, {"1"}, {"2"}, {}, {"0"}, {"4294967295"} },
	  { {"x_set_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"500000000"}, {"75000000"}, {"1"}, {"2"}, {}, {"0"}, {"500000000"} },
	  { {"x_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"x_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"x_montage_speed_slow"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"6025818"}, {}, {}, {}, {}, {} },
	  { {"x_montage_acceleration_slow"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"43824132"}, {}, {}, {}, {}, {} },
	  { {"x_montage_deceleration_slow"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"43824132"}, {}, {}, {}, {}, {} },
	  { {"x_montage_speed_medium"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"13256800"}, {}, {}, {}, {}, {} },
	  { {"x_montage_acceleration_medium"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"212108800"}, {}, {}, {}, {}, {} },
	  { {"x_montage_deceleration_medium"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"212108800"}, {}, {}, {}, {}, {} },
	  { {"x_montage_speed_fast"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"24085756"}, {}, {}, {}, {}, {} },
	  { {"x_montage_acceleration_fast"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"700167320"}, {}, {}, {}, {}, {} },
	  { {"x_montage_deceleration_fast"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"700167320"}, {}, {}, {}, {}, {} },
	  { {"x_jogging_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"25000000"}, {}, {}, {}, {}, {} },
	  { {"x_jogging_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"x_jogging_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"x_find_tip_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"x_find_tip_acceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"x_find_tip_deceleraion"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"x_find_tip_skew_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"x_find_tip_skew_acceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"x_find_tip_skew_deceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"x_find_tip_skew_travel_distance"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"8000000"}, {}, {}, {}, {}, {} },
	  { {"x_weed_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"50000000"}, {"25000000"}, {}, {"0"}, {}, {"0"}, {"50000000"} },
	  { {"x_weed_acceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"x_weed_deceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"y_axis_resolution"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm"}, {"0"}, {"4294967295"}, {"100"}, {}, {}, {}, {}, {} },
	  { {"y_axis_error_band"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm"}, {"0"}, {"4294967295"}, {"2500"}, {}, {}, {}, {}, {} },
	  { {"y_maximum_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000000"}, {"1"}, {"2"}, {}, {"0"}, {"4294967295"} },
	  { {"y_set_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"500000000"}, {"75000000"}, {"1"}, {"2"}, {}, {"0"}, {"500000000"} },
	  { {"y_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"y_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"y_montage_speed_slow"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"6025818"}, {}, {}, {}, {}, {} },
	  { {"y_montage_acceleration_slow"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"43824132"}, {}, {}, {}, {}, {} },
	  { {"y_montage_deceleration_slow"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"43824132"}, {}, {}, {}, {}, {} },
	  { {"y_montage_speed_medium"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"13256800"}, {}, {}, {}, {}, {} },
	  { {"y_montage_acceleration_medium"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"212108800"}, {}, {}, {}, {}, {} },
	  { {"y_montage_deceleration_medium"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"212108800"}, {}, {}, {}, {}, {} },
	  { {"y_montage_speed_fast"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"24085756"}, {}, {}, {}, {}, {} },
	  { {"y_montage_acceleration_fast"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"700167320"}, {}, {}, {}, {}, {} },
	  { {"y_montage_deceleration_fast"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"700167320"}, {}, {}, {}, {}, {} },
	  { {"y_jogging_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"25000000"}, {}, {}, {}, {}, {} },
	  { {"y_jogging_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"y_jogging_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"y_find_tip_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"y_find_tip_acceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"y_find_tip_deceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"y_find_tip_skew_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"y_find_tip_skew_acceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"y_find_tip_skew_deceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"y_find_tip_skew_travel_distance"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"8000000"}, {}, {}, {}, {}, {} },
	  { {"y_weed_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"50000000"}, {"25000000"}, {}, {"0"}, {}, {"0"}, {"50000000"} },
	  { {"y_weed_acceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"y_weed_deceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"300000000"}, {}, {}, {}, {}, {} },
	  { {"x_default_location_offset"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-56000000"}, {}, {}, {}, {}, {} },
	  { {"x_plate_1_right_edge_to_optical_path"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"7218268"}, {}, {}, {}, {}, {} },
	  { {"x_plate_2_left_edge_to_optical_path"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-2941732"}, {}, {}, {}, {}, {} },
	  { {"x_plate_1_right_edge_to_tip_working_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"8700385"}, {}, {}, {}, {}, {} },
	  { {"x_plate_2_left_edge_to_tip_working_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-1459615"}, {}, {}, {}, {}, {} },
	  { {"x_optical_sensor_to_tip_skew_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"x_z_tip_skew_base_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"6721400"}, {}, {}, {}, {}, {} },
	  { {"x_zz_tip_skew_base_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"6654600"}, {}, {}, {}, {}, {} },
	  { {"y_default_location_offset"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-146000000"}, {}, {}, {}, {}, {} },
	  { {"y_plate_1_top_edge_to_optical_path"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-117425117"}, {}, {}, {}, {}, {} },
	  { {"y_plate_2_top_edge_to_optical_path"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-117425117"}, {}, {}, {}, {}, {} },
	  { {"y_plate_1_top_edge_to_tip_working_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"27994315"}, {}, {}, {}, {}, {} },
	  { {"y_plate_2_top_edge_to_tip_working_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"27994315"}, {}, {}, {}, {}, {} },
	  { {"y_optical_sensor_to_tip_skew_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"48400000"}, {}, {}, {}, {}, {} },
	  { {"y_z_tip_skew_base_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"54057700"}, {}, {}, {}, {}, {} },
	  { {"y_zz_tip_skew_base_position"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"54057700"}, {}, {}, {}, {}, {} },
	  { {"max_coordinated_motion_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"25000000"}, {}, {}, {}, {}, {} },
	  { {"max_coordinated_motion_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"500000000"}, {}, {}, {}, {}, {} },
	  { {"max_coordinated_motion_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"500000000"}, {}, {}, {}, {}, {} },
	  { {"x_home_location"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"x_homing_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"25000000"}, {}, {}, {}, {}, {} },
	  { {"x_homing_acceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"500000000"}, {}, {}, {}, {}, {} },
	  { {"x_homing_deceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"500000000"}, {}, {}, {}, {}, {} },
	  { {"x_max_allowable_position_error"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"5"}, {}, {}, {}, {}, {} },
	  { {"x_end_of_travel_limit_position_+"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"89785000"}, {}, {}, {}, {}, {} },
	  { {"x_end_of_travel_limit_position_-"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-85907800"}, {}, {}, {}, {}, {} },
	  { {"x_soft_travel_limit_position_+"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"89685000"}, {}, {}, {}, {}, {} },
	  { {"x_soft_travel_limit_position_-"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-85807800"}, {}, {}, {}, {}, {} },
	  { {"x_calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"1"}, {}, {}, {}, {}, {} },
	  { {"x_last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"x_calibration_factor_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"x_calibration_factor_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"x_calibration_factor_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"y_home_location"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"y_homing_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"25000000"}, {}, {}, {}, {}, {} },
	  { {"y_homing_acceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"500000000"}, {}, {}, {}, {}, {} },
	  { {"y_homing_deceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"500000000"}, {}, {}, {}, {}, {} },
	  { {"y_max_allowable_position_error"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"5"}, {}, {}, {}, {}, {} },
	  { {"y_end_of_travel_limit_position_+"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {}, {"-2147483648"}, {"2147483647"}, {"147179800"}, {}, {}, {}, {}, {} },
	  { {"y_end_of_travel_limit_position_-"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {}, {"-2147483648"}, {"2147483647"}, {"-147582500"}, {}, {}, {}, {}, {} },
	  { {"y_soft_travel_limit_position_+"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {}, {"-2147483648"}, {"2147483647"}, {"147079800"}, {}, {}, {}, {}, {} },
	  { {"y_soft_travel_limit_position_-"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {}, {"-2147483648"}, {"2147483647"}, {"-147482500"}, {}, {}, {}, {}, {} },
	  { {"y_calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumNone"}, {}, {}, {}, {}, {} },
	  { {"y_last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"y_calibration_factor_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"y_calibration_factor_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"y_calibration_factor_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "axis_xy.cfg" },
	{ (std::string (CONFIG_DIR) + "/axis_xy.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for Z struct
/// each row:  name,  value,  editable,  persist,  dataType,  min,  max;
///----------------------------------------------------------------------
ComponentProperties defaultAxisZ = 
{
	ComponentType::ZAxis,
	{ { {"controller_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"z_axis_resolution"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm"}, {"0"}, {"4294967295"}, {"390"}, {}, {}, {}, {}, {} },
	  { {"z_axis_error_band"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm"}, {"0"}, {"4294967295"}, {"1172"}, {}, {}, {}, {}, {} },
	  { {"z_maximum_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"250000000"}, {"100000000"}, {"1"}, {"2"}, {}, {"0"}, {"250000000"} },
	  { {"z_set_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"100000000"}, {"30000000"}, {"1"}, {"2"}, {}, {"0"}, {"100000000"} },
	  { {"z_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"400000000"}, {}, {}, {}, {}, {} },
	  { {"z_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"400000000"}, {}, {}, {}, {}, {} },
	  { {"z_jogging_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"100000000"}, {"15000000"}, {}, {}, {}, {}, {} },
	  { {"z_jogging_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"z_jogging_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"z_retraction_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"400000000"}, {"10000000"}, {}, {}, {}, {}, {} },
	  { {"z_retraction_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"z_retraction_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"tip_pickup_force"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {"Newtons"}, {"0"}, {"3.4e+38"}, {"-12"}, {}, {}, {}, {}, {} },
	  { {"tip_pickup_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"tip_pickup_slowdown_offset"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"tip_pickup_overshoot"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"1500000"}, {}, {}, {}, {}, {} },
	  { {"tip_seating_force"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {"Newtons"}, {"0"}, {"3.4e+38"}, {"-31"}, {}, {}, {}, {}, {} },
	  { {"tip_seating_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"tip_seating_slowdown_offset"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-8000000"}, {}, {}, {}, {}, {} },
	  { {"tip_seating_overshoot"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"1500000"}, {}, {}, {}, {}, {} },
	  { {"tip_removal_slowdown_offset"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"3000000"}, {}, {}, {}, {}, {} },
	  { {"tip_find_well_bottom_force"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {"Newtons"}, {"0"}, {"3.4e+38"}, {"-0.5"}, {}, {}, {}, {}, {} },
	  { {"tip_find_well_bottom_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"tip_find_well_bottom_overshoot"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"3000000"}, {}, {}, {}, {}, {} },
	  { {"tip_skew_safety_gap"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2000000"}, {}, {}, {}, {}, {} },
	  { {"tip_waste_1_safety_gap"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2000000"}, {}, {}, {}, {}, {} },
	  { {"tip_waste_2_safety_gap"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2000000"}, {}, {}, {}, {}, {} },
	  { {"tip_waste_3_safety_gap"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2000000"}, {}, {}, {}, {}, {} },
	  { {"tip_insert_version_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"z_safe_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2700000"}, {}, {}, {}, {}, {} },
	  { {"z_tip_offset_from_well_wall"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"3500500"}, {}, {}, {}, {}, {} },
	  { {"z_above_plate_height_percent"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"percent"}, {"0"}, {"65535"}, {"103"}, {}, {}, {}, {}, {} },
	  { {"z_to_top_of_yy_stage_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-98929400"}, {}, {}, {}, {}, {} },
	  { {"z_to_top_of_seating_key_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-93755900"}, {}, {}, {}, {}, {} },
	  { {"z_through_key_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-91037000"}, {}, {}, {}, {}, {} },
	  { {"z_to_waste_1_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-137647000"}, {}, {}, {}, {}, {} },
	  { {"z_to_waste_2_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-137647000"}, {}, {}, {}, {}, {} },
	  { {"z_to_waste_3_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-137647000"}, {}, {}, {}, {}, {} },
	  { {"z_to_tip_skew_sensor_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-90200000"}, {}, {}, {}, {}, {} },
	  { {"z_to_top_of_xy_stage_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-104373600"}, {}, {}, {}, {}, {} },
	  { {"z_aspirate_height_from_well_bottom"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"250000000"}, {"50000"}, {"1"}, {"0"}, {}, {"0"}, {"250000000"} },
	  { {"z_aspirate_height_for_weeding"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"250000000"}, {"200000"}, {"1"}, {"0"}, {}, {"0"}, {"250000000"} },
	  { {"z_dispense_height_from_well_bottom"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"250000000"}, {"10000000"}, {"1"}, {"0"}, {}, {"0"}, {"250000000"} },
	  { {"z_weeding_height_from_bottom"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"0"}, {"250000000"}, {"25000"}, {"1"}, {"2"}, {}, {"0"}, {"250000000"} },
	  { {"z_y_offset_from_xx_axis"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"250000000"}, {"0"}, {"1"}, {}, {}, {}, {} },
	  { {"home_location"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"homing_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"homing_acceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"homing_deceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"max_allowable_position_error"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"2"}, {}, {}, {}, {}, {} },
	  { {"end_of_travel_limit_position_+"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"5300000"}, {}, {}, {}, {}, {} },
	  { {"end_of_travel_limit_position_-"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-116200000"}, {}, {}, {}, {}, {} },
	  { {"soft_travel_limit_position_+"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2700001"}, {}, {}, {}, {}, {} },
	  { {"soft_travel_limit_position_-"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-115200000"}, {}, {}, {}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumNone"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_factor_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_factor_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_factor_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "axis_z.cfg" },
	{ (std::string (CONFIG_DIR) + "/axis_z.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for ZZ struct
/// each row:  name,  value,  editable,  persist,  dataType,  min,  max;
///----------------------------------------------------------------------
ComponentProperties defaultAxisZZ = 
{
	ComponentType::ZZAxis,
	{ { {"controller_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"zz_axis_resolution"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm"}, {"0"}, {"4294967295"}, {"390"}, {}, {}, {}, {}, {} },
	  { {"zz_axis_error_band"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm"}, {"0"}, {"4294967295"}, {"1172"}, {}, {}, {}, {}, {} },
	  { {"zz_maximum_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"250000000"}, {"100000000"}, {"1"}, {"2"}, {}, {"0"}, {"250000000"} },
	  { {"zz_set_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"100000000"}, {"30000000"}, {"1"}, {"2"}, {}, {"0"}, {"100000000"} },
	  { {"zz_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"400000000"}, {}, {}, {}, {}, {} },
	  { {"zz_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"400000000"}, {}, {}, {}, {}, {} },
	  { {"zz_jogging_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"100000000"}, {"15000000"}, {}, {}, {}, {}, {} },
	  { {"zz_jogging_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"zz_jogging_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"zz_retraction_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"400000000"}, {"10000000"}, {}, {}, {}, {}, {} },
	  { {"zz_retraction_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"zz_retraction_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"tip_pickup_force"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {"Newtons"}, {"0"}, {"3.4e+38"}, {"-12"}, {}, {}, {}, {}, {} },
	  { {"tip_pickup_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"tip_pickup_slowdown_offset"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"tip_pickup_overshoot"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"1500000"}, {}, {}, {}, {}, {} },
	  { {"tip_seating_force"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {"Newtons"}, {"0"}, {"3.4e+38"}, {"-31"}, {}, {}, {}, {}, {} },
	  { {"tip_seating_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"tip_seating_slowdown_offset"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-3000000"}, {}, {}, {}, {}, {} },
	  { {"tip_seating_overshoot"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"1500000"}, {}, {}, {}, {}, {} },
	  { {"tip_removal_slowdown_offset"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"1500000"}, {}, {}, {}, {}, {} },
	  { {"tip_find_well_bottom_force"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {"Newtons"}, {"0"}, {"3.4e+38"}, {"-0.5"}, {}, {}, {}, {}, {} },
	  { {"tip_find_well_bottom_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"tip_find_well_bottom_overshoot"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2000000"}, {}, {}, {}, {}, {} },
	  { {"tip_skew_safety_gap"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2000000"}, {}, {}, {}, {}, {} },
	  { {"tip_waste_1_safety_gap"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2000000"}, {}, {}, {}, {}, {} },
	  { {"tip_waste_2_safety_gap"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2000000"}, {}, {}, {}, {}, {} },
	  { {"tip_waste_3_safety_gap"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2000000"}, {}, {}, {}, {}, {} },
	  { {"tip_insert_version_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"zz_safe_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"zz_tip_offset_from_well_wall"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"3500500"}, {}, {}, {}, {}, {} },
	  { {"zz_above_plate_height_percent"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"percent"}, {"0"}, {"65535"}, {"120"}, {}, {}, {}, {}, {} },
	  { {"zz_to_top_of_yy_stage_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-107793600"}, {}, {}, {}, {}, {} },
	  { {"zz_to_top_of_seating_key_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-102557200"}, {}, {}, {}, {}, {} },
	  { {"zz_through_key_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-108103666"}, {}, {}, {}, {}, {} },
	  { {"zz_to_waste_1_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-137647000"}, {}, {}, {}, {}, {} },
	  { {"zz_to_waste_2_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-137647000"}, {}, {}, {}, {}, {} },
	  { {"zz_to_waste_3_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-137647000"}, {}, {}, {}, {}, {} },
	  { {"zz_to_tip_skew_sensor_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-98770000"}, {}, {}, {}, {}, {} },
	  { {"zz_to_top_of_xy_stage_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-113180000"}, {}, {}, {}, {}, {} },
	  { {"zz_aspirate_height"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"0"}, {"250000000"}, {"100000"}, {"1"}, {"2"}, {}, {"0"}, {"250000000"} },
	  { {"zz_aspirate_height_for_weeding"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"0"}, {"250000000"}, {"200000"}, {"1"}, {"0"}, {}, {"0"}, {"250000000"} },
	  { {"zz_weeding_height_from_bottom"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"0"}, {"250000000"}, {"25000"}, {"1"}, {"2"}, {}, {"0"}, {"250000000"} },
	  { {"zz_y_offset_from_xx_axis"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"500000"}, {"1"}, {}, {}, {}, {} },
	  { {"home_location"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"homing_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"homing_acceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"homing_deceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"max_allowable_position_error"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"2"}, {}, {}, {}, {}, {} },
	  { {"end_of_travel_limit_position_+"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2500000"}, {}, {}, {}, {}, {} },
	  { {"end_of_travel_limit_position_-"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-119400000"}, {}, {}, {}, {}, {} },
	  { {"soft_travel_limit_position_+"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2700001"}, {}, {}, {}, {}, {} },
	  { {"soft_travel_limit_position_-"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-118400000"}, {}, {}, {}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumNone"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_factor_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_factor_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_factor_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "axis_zz.cfg" },
	{ (std::string (CONFIG_DIR) + "/axis_zz.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for ZZZ struct
/// each row:  name,  value,  editable,  persist,  dataType,  min,  max;
///----------------------------------------------------------------------
ComponentProperties defaultAxisZZZ = 
{
	ComponentType::ZZZAxis,
	{ { {"controller_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"zzz_axis_resolution"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm"}, {"0"}, {"4294967295"}, {"390"}, {}, {}, {}, {}, {} },
	  { {"zzz_axis_error_band"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm"}, {"0"}, {"4294967295"}, {"1172"}, {}, {}, {}, {}, {} },
	  { {"zzz_maximum_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"250000000"}, {"100000000"}, {"1"}, {"2"}, {}, {"0"}, {"250000000"} },
	  { {"zzz_set_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"100000000"}, {"30000000"}, {"1"}, {"2"}, {}, {"0"}, {"100000000"} },
	  { {"zzz_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"400000000"}, {}, {}, {}, {}, {} },
	  { {"zzz_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"400000000"}, {}, {}, {}, {}, {} },
	  { {"zzz_jogging_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"100000000"}, {"15000000"}, {"1"}, {"2"}, {}, {"0"}, {"100000000"} },
	  { {"zzz_jogging_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"zzz_jogging_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"zzz_total_travel"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"tip_waste_1_safety_gap"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2000000"}, {}, {}, {}, {}, {} },
	  { {"tip_waste_2_safety_gap"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2000000"}, {}, {}, {}, {}, {} },
	  { {"tip_waste_3_safety_gap"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2000000"}, {}, {}, {}, {}, {} },
	  { {"dispense_tip_1_version_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"1.00"}, {}, {}, {}, {}, {} },
	  { {"dispense_tip_2_version_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"1.00"}, {}, {}, {}, {}, {} },
	  { {"dispense_tip_3_version_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"1.00"}, {}, {}, {}, {}, {} },
	  { {"zzz_safe_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"zzz_above_plate_height_percent"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"percent"}, {"0"}, {"65535"}, {"120"}, {}, {}, {}, {}, {} },
	  { {"zzz_to_waste_1_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-67000000"}, {}, {}, {}, {}, {} },
	  { {"zzz_to_waste_2_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-67000000"}, {}, {}, {}, {}, {} },
	  { {"zzz_to_waste_3_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-67000000"}, {}, {}, {}, {}, {} },
	  { {"zzz_to_top_of_xy_stage_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-41300000"}, {}, {}, {}, {}, {} },
	  { {"zzz_dispense_height_safety_gap_nm"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"5000000"}, {"1"}, {}, {}, {}, {} },
	  { {"zzz_y_offset_from_xx_axis"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"0"}, {"1"}, {}, {}, {}, {} },
	  { {"home_location"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"homing_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"homing_acceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"homing_deceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"max_allowable_position_error"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"2"}, {}, {}, {}, {}, {} },
	  { {"end_of_travel_limit_position_+"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2800000"}, {}, {}, {}, {}, {} },
	  { {"end_of_travel_limit_position_-"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-118800000"}, {}, {}, {}, {}, {} },
	  { {"soft_travel_limit_position_+"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"2700001"}, {}, {}, {}, {}, {} },
	  { {"soft_travel_limit_position_-"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-117800000"}, {}, {}, {}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumNone"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_factor_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_factor_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_factor_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "axis_zzz.cfg" },
	{ (std::string (CONFIG_DIR) + "/axis_zzz.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for AspirationPump1 struct
///----------------------------------------------------------------------
ComponentProperties defaultAspirationPump1 = 
{
	ComponentType::AspirationPump1,
	{ { {"pump_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Welco"}, {}, {}, {}, {}, {} },
	  { {"pump_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"WP11-P1/4BA2-WT6-BP"}, {}, {}, {}, {}, {} },
	  { {"pump_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"1.0"}, {}, {}, {}, {}, {} },
	  { {"pump_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"pump_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumPumpType"}, {}, {}, {}, {"enumPeristaltic"}, {}, {}, {}, {}, {} },
	  { {"pump_color"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Gray"}, {}, {}, {}, {}, {} },
	  { {"pump_p_name"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"PN"}, {}, {}, {}, {}, {} },
	  { {"standard_flow_rate"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nl/s"}, {"125000"}, {"937500"}, {"500000"}, {"1"}, {"2"}, {}, {"125000"}, {"937500"} },
	  { {"weeding_flow_rate"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nl/s"}, {"125000"}, {"937500"}, {"200000"}, {"1"}, {"2"}, {}, {"125000"}, {"937500"} },
	  { {"standard_aspirate_volume"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nl"}, {"0"}, {"100000000"}, {"100000"}, {"1"}, {"1"}, {}, {"0"}, {"100000000"} },
	  { {"weeding_aspirate_volume"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nl"}, {"0"}, {"100000000"}, {"100000"}, {"1"}, {"1"}, {}, {"0"}, {"100000000"} },
	  { {"at_height_large_aspirate_volume"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nl"}, {"0"}, {"100000000"}, {"8000000"}, {"1"}, {"1"}, {}, {"0"}, {"100000000"} },
	  { {"aspiration_delay_msec"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint16_t"}, {"ms"}, {"0"}, {"65535"}, {"0"}, {"1"}, {"2"}, {}, {"0"}, {"65535"} },
	  { {"tubing_type"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Parker p/n MBS-CCF-U008"}, {}, {}, {}, {}, {} },
	  { {"tubing_inner_diameter"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"6350000"}, {"1"}, {"2"}, {}, {"0"}, {"4294967295"} },
	  { {"tubing_last_change_date"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"volume_per_revolution"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nl/rev"}, {"0"}, {"4294967295"}, {"3000000"}, {}, {}, {}, {}, {} },
	  { {"maximum_revolutions_per_hour"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"rev/h"}, {"0"}, {"65535"}, {"1125"}, {}, {}, {}, {}, {} },
	  { {"acceleration_time"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"ms"}, {"100"}, {"500"}, {"225"}, {}, {}, {}, {}, {} },
	  { {"deceleration_time"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"ms"}, {"100"}, {"500"}, {"225"}, {}, {}, {}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumFirstOrderPolynomialForceZeroIntercept"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_coefficient_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"1.0976"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "asp_pump.cfg" },
	{ (std::string (CONFIG_DIR) + "/asp_pump.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for DispensePump1 struct
///----------------------------------------------------------------------
ComponentProperties defaultDispensePump1 = 
{
	ComponentType::DispensePump1,
	{ { {"pump_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Welco"}, {}, {}, {}, {}, {} },
	  { {"pump_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"WP11-P1/4BA2-WT6-BP"}, {}, {}, {}, {}, {} },
	  { {"pump_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"1.0"}, {}, {}, {}, {}, {} },
	  { {"pump_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"pump_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumPumpType"}, {}, {}, {}, {"enumPeristaltic"}, {}, {}, {}, {}, {} },
	  { {"pump_color"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Red"}, {}, {}, {}, {}, {} },
	  { {"pump_p_name"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"PP"}, {}, {}, {}, {}, {} },
	  { {"standard_flow_rate"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nl/s"}, {"0"}, {"937500"}, {"900000"}, {"1"}, {"2"}, {}, {"0"}, {"937500"} },
	  { {"dispense_delay_msec"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint16_t"}, {"ms"}, {"0"}, {"65535"}, {"0"}, {"1"}, {"2"}, {}, {"0"}, {"65535"} },
	  { {"tubing_type"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Parker p/n MBS-CCF-U008"}, {}, {}, {}, {}, {} },
	  { {"tubing_inner_diameter"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"6350000"}, {"1"}, {"2"}, {}, {"0"}, {"4294967295"} },
	  { {"tubing_last_change_date"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"reservoir_volume"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nl"}, {"0"}, {"4294967295"}, {"0"}, {"1"}, {"2"}, {}, {"0"}, {"4294967295"} },
	  { {"current_solution_type"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"volume_per_revolution"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nl/rev"}, {"0"}, {"4294967295"}, {"3000000"}, {}, {}, {}, {}, {} },
	  { {"maximum_revolutions_per_hour"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"rev/h"}, {"0"}, {"65535"}, {"1125"}, {}, {}, {}, {}, {} },
	  { {"acceleration_time"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"ms"}, {"100"}, {"500"}, {"225"}, {}, {}, {}, {}, {} },
	  { {"deceleration_time"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"ms"}, {"100"}, {"500"}, {"225"}, {}, {}, {}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumFirstOrderPolynomialForceZeroIntercept"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_coefficient_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"1.1304"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "disp_pump1.cfg" },
	{ (std::string (CONFIG_DIR) + "/disp_pump1.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for DispensePump2 struct
///----------------------------------------------------------------------
ComponentProperties defaultDispensePump2 = 
{
	ComponentType::DispensePump2,
	{ { {"pump_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Welco"}, {}, {}, {}, {}, {} },
	  { {"pump_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"WP11-P1/4BA2-WT6-BP"}, {}, {}, {}, {}, {} },
	  { {"pump_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"1.0"}, {}, {}, {}, {}, {} },
	  { {"pump_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"pump_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumPumpType"}, {}, {}, {}, {"enumPeristaltic"}, {}, {}, {}, {}, {} },
	  { {"pump_color"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Green"}, {}, {}, {}, {}, {} },
	  { {"pump_p_name"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"PPP"}, {}, {}, {}, {}, {} },
	  { {"standard_flow_rate"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nl/s"}, {"0"}, {"937500"}, {"900000"}, {"1"}, {"2"}, {}, {"0"}, {"937500"} },
	  { {"dispense_delay_msec"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint16_t"}, {"ms"}, {"0"}, {"65535"}, {"0"}, {"1"}, {"2"}, {}, {"0"}, {"65535"} },
	  { {"tubing_type"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Parker p/n MBS-CCF-U008"}, {}, {}, {}, {}, {} },
	  { {"tubing_inner_diameter"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"6350000"}, {"1"}, {"2"}, {}, {"0"}, {"4294967295"} },
	  { {"tubing_last_change_date"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"reservoir_volume"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nl"}, {"0"}, {"4294967295"}, {"0"}, {"1"}, {"2"}, {}, {"0"}, {"4294967295"} },
	  { {"current_solution_type"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"volume_per_revolution"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nl/rev"}, {"0"}, {"4294967295"}, {"3000000"}, {}, {}, {}, {}, {} },
	  { {"maximum_revolutions_per_hour"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"rev/h"}, {"0"}, {"65535"}, {"1125"}, {}, {}, {}, {}, {} },
	  { {"acceleration_time"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"ms"}, {"100"}, {"500"}, {"225"}, {}, {}, {}, {}, {} },
	  { {"deceleration_time"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"ms"}, {"100"}, {"500"}, {"225"}, {}, {}, {}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumFirstOrderPolynomialForceZeroIntercept"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_coefficient_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0.9986"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "disp_pump2.cfg" },
	{ (std::string (CONFIG_DIR) + "/disp_pump2.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for DispensePump3 struct
///----------------------------------------------------------------------
ComponentProperties defaultDispensePump3 = 
{
	ComponentType::DispensePump3,
	{ { {"pump_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Welco"}, {}, {}, {}, {}, {} },
	  { {"pump_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"WPX1-P1/8FA4-WT6-BP"}, {}, {}, {}, {}, {} },
	  { {"pump_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"1.0"}, {}, {}, {}, {}, {} },
	  { {"pump_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"pump_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumPumpType"}, {}, {}, {}, {"enumPeristaltic"}, {}, {}, {}, {}, {} },
	  { {"pump_color"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Blue"}, {}, {}, {}, {}, {} },
	  { {"pump_p_name"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"PPPP"}, {}, {}, {}, {}, {} },
	  { {"standard_flow_rate"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nl/s"}, {"0"}, {"156000"}, {"100000"}, {"1"}, {"2"}, {}, {"0"}, {"156000"} },
	  { {"dispense_delay_msec"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint16_t"}, {"ms"}, {"0"}, {"65535"}, {"0"}, {"1"}, {"2"}, {}, {"0"}, {"65535"} },
	  { {"tubing_type"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Parker p/n MBS-CCF-U007"}, {}, {}, {}, {}, {} },
	  { {"tubing_inner_diameter"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"3175000"}, {"1"}, {"2"}, {}, {"0"}, {"4294967295"} },
	  { {"tubing_last_change_date"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"reservoir_volume"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nl"}, {"0"}, {"4294967295"}, {"0"}, {"1"}, {"2"}, {}, {"0"}, {"4294967295"} },
	  { {"current_solution_type"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"volume_per_revolution"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nl/rev"}, {"0"}, {"4294967295"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"maximum_revolutions_per_hour"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"rev/h"}, {"0"}, {"65535"}, {"1125"}, {}, {}, {}, {}, {} },
	  { {"acceleration_time"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"ms"}, {"100"}, {"500"}, {"200"}, {}, {}, {}, {}, {} },
	  { {"deceleration_time"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"ms"}, {"100"}, {"500"}, {"200"}, {}, {}, {}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumFirstOrderPolynomialForceZeroIntercept"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_coefficient_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"1.1236"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "disp_pump3.cfg" },
	{ (std::string (CONFIG_DIR) + "/disp_pump3.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for PickingPump1 struct
///----------------------------------------------------------------------
ComponentProperties defaultPickingPump1 =
{
	ComponentType::PickingPump1,
	{ { {"pump_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Tecan"}, {}, {}, {}, {}, {} },
	  { {"pump_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"ADP Detect"}, {}, {}, {}, {}, {} },
	  { {"pump_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"1.0"}, {}, {}, {}, {}, {} },
	  { {"pump_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"pump_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumPumpType"}, {}, {}, {}, {"enumSyringe"}, {}, {}, {}, {}, {} },
	  { {"air_gap"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nl"}, {"0"}, {"1000000"}, {"10000"}, {"1"}, {}, {}, {}, {} },
	  { {"standard_aspiration_rate"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nl/s"}, {"100000"}, {"1000000"}, {"200000"}, {"1"}, {"2"}, {}, {"100000"}, {"1000000"} },
	  { {"standard_aspirate_volume"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nl"}, {"0"}, {"1000000"}, {"50000"}, {"1"}, {"1"}, {}, {"0"}, {"1000000"} },
	  { {"standard_dispense_rate"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nl/s"}, {"100000"}, {"1000000"}, {"125000"}, {"1"}, {"2"}, {}, {"100000"}, {"1000000"} },
	  { {"standard_dispense_volume"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nl"}, {"0"}, {"1000000"}, {"1000000"}, {"1"}, {"1"}, {}, {"0"}, {"1000000"} },
	  { {"maximum_stroke_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"millisecond"}, {"0"}, {"1000"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"minimum_stroke_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"millisecond"}, {"0"}, {"6660000"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"maximum_stroke_volume"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nl"}, {"0"}, {"10000000"}, {"1000000"}, {}, {}, {}, {}, {} },
	  { {"weeding_flow_rate"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nl/s"}, {"100000"}, {"937500"}, {"200000"}, {"1"}, {"2"}, {}, {"100000"}, {"937500"} },
	  { {"weeding_aspirate_volume"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nl"}, {"0"}, {"1000000"}, {"100"}, {"1"}, {"1"}, {}, {"0"}, {"1000000"} },
	  { {"aspiration_delay_msec"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint16_t"}, {"ms"}, {"0"}, {"65535"}, {"0"}, {"1"}, {"2"}, {}, {"0"}, {"65535"} },
	  { {"linear_displacement_per_step"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {"um/step"}, {"0"}, {"3.4e+38"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"syringe_diameter"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"0"}, {"1"}, {"2"}, {}, {"0"}, {"4294967295"} },
	  { {"syringe_length"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"0"}, {"1"}, {"2"}, {}, {"0"}, {"4294967295"} },
	  { {"last_change_date"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {"datetime"}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumFirstOrderPolynomialForceZeroIntercept"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_coefficient_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"1"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"pump_scaling_units"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} } },
	{ "picking_pump1.cfg" },
	{ (std::string (CONFIG_DIR) + "/picking_pump1.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for WhiteLightSource struct
///----------------------------------------------------------------------
ComponentProperties defaultWhiteLightSource = 
{
	ComponentType::WhiteLightSource,
	{ { {"vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"white_light_source_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumWhiteLightType"}, {}, {}, {}, {"enumIncandescent"}, {}, {}, {}, {}, {} },
	  { {"white_light_source_seconds_used"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {"seconds"}, {"0"}, {"4294967295"}, {"0"}, {"1"}, {"1"}, {}, {"0"}, {"4294967295"} },
	  { {"white_light_source_last_changed_date"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"on_status"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"boolean"}, {}, {}, {}, {"false"}, {}, {}, {}, {"0"}, {"1"} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumNone"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_coefficient_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "white_light.cfg" },
	{ (std::string (CONFIG_DIR) + "/white_light.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for FluorescenceLightFilter struct
///----------------------------------------------------------------------
ComponentProperties defaultFluorescenceLightFilter =
{
	ComponentType::FluorescenceLightFilter,
	{ { {"vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"require_white_light_source"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"boolean"}, {}, {}, {}, {"True"}, {}, {}, {}, {}, {} },

	  { {"fluorescence_filter_display_name"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Empty"}, {}, {}, {}, {}, {}, {"filters"}, {0}  },
	  { {"fluorescence_filter_excitation_band"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {"filters"}, {0}  },
	  { {"fluorescence_filter_emission_band"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {"filters"}, {0}  },
	  { {"fluorescence_filter_seconds_used"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {"seconds"}, {"0"}, {"4294967295"}, {"0"}, {"1"}, {"1"}, {}, {"0"}, {"4294967295"}, {"filters"}, {0}  },
	  { {"fluorescence_filter_last_replaced_date"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {}, {"filters"}, {0}  },
	  { {"fluorescence_filter_position"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int16_t"}, {}, {"-32768"}, {"32767"}, {"1"}, {}, {}, {}, {}, {}, {"filters"}, {0}  },

	  { {"fluorescence_filter_display_name"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"EGFP (FITC/CY2)"}, {}, {}, {}, {}, {}, {"filters"}, {1}  },
	  { {"fluorescence_filter_excitation_band"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"470/40 nm"}, {}, {}, {}, {}, {}, {"filters"}, {1}  },
	  { {"fluorescence_filter_emission_band"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"525/50 nm"}, {}, {}, {}, {}, {}, {"filters"}, {1}  },
	  { {"fluorescence_filter_seconds_used"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {"seconds"}, {"0"}, {"4294967295"}, {"0"}, {"1"}, {"1"}, {}, {"0"}, {"4294967295"}, {"filters"}, {1}  },
	  { {"fluorescence_filter_last_replaced_date"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {}, {"filters"}, {1}  },
	  { {"fluorescence_filter_position"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int16_t"}, {}, {"-32768"}, {"32767"}, {"2"}, {}, {}, {}, {}, {}, {"filters"}, {1}  },

	  { {"fluorescence_filter_display_name"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"mCherry/Texas Red"}, {}, {}, {}, {}, {}, {"filters"}, {2}  },
	  { {"fluorescence_filter_excitation_band"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"560/40 nm"}, {}, {}, {}, {}, {}, {"filters"}, {2}  },
	  { {"fluorescence_filter_emission_band"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"630/75 nm"}, {}, {}, {}, {}, {}, {"filters"}, {2}  },
	  { {"fluorescence_filter_seconds_used"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {"seconds"}, {"0"}, {"4294967295"}, {"0"}, {"1"}, {"1"}, {}, {"0"}, {"4294967295"}, {"filters"}, {2}  },
	  { {"fluorescence_filter_last_replaced_date"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {}, {"filters"}, {2}  },
	  { {"fluorescence_filter_position"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int16_t"}, {}, {"-32768"}, {"32767"}, {"3"}, {}, {}, {}, {}, {}, {"filters"}, {2}  },

	  { {"fluorescence_filter_display_name"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"CY5"}, {}, {}, {}, {}, {}, {"filters"}, {3}  },
	  { {"fluorescence_filter_excitation_band"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"620/60 nm"}, {}, {}, {}, {}, {}, {"filters"}, {3}  },
	  { {"fluorescence_filter_emission_band"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"700/75 nm"}, {}, {}, {}, {}, {}, {"filters"}, {3}  },
	  { {"fluorescence_filter_seconds_used"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {"seconds"}, {"0"}, {"4294967295"}, {"0"}, {"1"}, {"1"}, {}, {"0"}, {"4294967295"}, {"filters"}, {3}  },
	  { {"fluorescence_filter_last_replaced_date"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {}, {"filters"}, {3}  },
	  { {"fluorescence_filter_position"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int16_t"}, {}, {"-32768"}, {"32767"}, {"4"}, {}, {}, {}, {}, {}, {"filters"}, {3}  },

	  { {"fluorescence_filter_display_name"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"ET-DAPI"}, {}, {}, {}, {}, {}, {"filters"}, {4}  },
	  { {"fluorescence_filter_excitation_band"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"350/50 nm"}, {}, {}, {}, {}, {}, {"filters"}, {4}  },
	  { {"fluorescence_filter_emission_band"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"460/50 nm"}, {}, {}, {}, {}, {}, {"filters"}, {4}  },
	  { {"fluorescence_filter_seconds_used"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {"seconds"}, {"0"}, {"4294967295"}, {"0"}, {"1"}, {"1"}, {}, {"0"}, {"4294967295"}, {"filters"}, {4}  },
	  { {"fluorescence_filter_last_replaced_date"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {}, {"filters"}, {4}  },
	  { {"fluorescence_filter_position"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int16_t"}, {}, {"-32768"}, {"32767"}, {"5"}, {}, {}, {}, {}, {}, {"filters"}, {4}  },

	  { {"fluorescence_filter_display_name"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"BV421"}, {}, {}, {}, {}, {}, {"filters"}, {5}  },
	  { {"fluorescence_filter_excitation_band"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"395/25 nm"}, {}, {}, {}, {}, {}, {"filters"}, {5}  },
	  { {"fluorescence_filter_emission_band"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"431/28 nm"}, {}, {}, {}, {}, {}, {"filters"}, {5}  },
	  { {"fluorescence_filter_seconds_used"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {"seconds"}, {"0"}, {"4294967295"}, {"0"}, {"1"}, {"1"}, {}, {"0"}, {"4294967295"}, {"filters"}, {5}  },
	  { {"fluorescence_filter_last_replaced_date"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {}, {"filters"}, {5}  },
	  { {"fluorescence_filter_position"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int16_t"}, {}, {"-32768"}, {"32767"}, {"-1"}, {}, {}, {}, {}, {}, {"filters"}, {5}  },

	  { {"fluorescence_filter_display_name"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Red"}, {}, {}, {}, {}, {}, {"filters"}, {6}  },
	  { {"fluorescence_filter_excitation_band"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"370-410 nm"}, {}, {}, {}, {}, {}, {"filters"}, {6}  },
	  { {"fluorescence_filter_emission_band"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"429-462 nm"}, {}, {}, {}, {}, {}, {"filters"}, {6}  },
	  { {"fluorescence_filter_seconds_used"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {"seconds"}, {"0"}, {"4294967295"}, {"0"}, {"1"}, {"1"}, {}, {"0"}, {"4294967295"}, {"filters"}, {6}  },
	  { {"fluorescence_filter_last_replaced_date"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {}, {"filters"}, {6}  },
	  { {"fluorescence_filter_position"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int16_t"}, {}, {"-32768"}, {"32767"}, {"-1"}, {}, {}, {}, {}, {}, {"filters"}, {6}  },

	  { {"fluorescence_filter_display_name"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Undefined"}, {}, {}, {}, {}, {}, {"filters"}, {7}  },
	  { {"fluorescence_filter_excitation_band"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"370-410 nm"}, {}, {}, {}, {}, {}, {"filters"}, {7}  },
	  { {"fluorescence_filter_emission_band"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"429-462 nm"}, {}, {}, {}, {}, {}, {"filters"}, {7}  },
	  { {"fluorescence_filter_seconds_used"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {"seconds"}, {"0"}, {"4294967295"}, {"0"}, {"1"}, {"1"}, {}, {"0"}, {"4294967295"}, {"filters"}, {7}  },
	  { {"fluorescence_filter_last_replaced_date"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {}, {"filters"}, {7}  },
	  { {"fluorescence_filter_position"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int16_t"}, {}, {"-32768"}, {"32767"}, {"-1"}, {}, {}, {}, {}, {}, {"filters"}, {7}  },

	  { {"active_fluorescence_filter_position"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int16_t"}, {}, {"-32768"}, {"32767"}, {"5"}, {}, {}, {}, {}, {} },
	  { {"on_status"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"boolean"}, {}, {}, {}, {"false"}, {}, {}, {}, {"0"}, {"1"} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumNone"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_coefficient_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "fluoro_filter.cfg" },
	{ (std::string (CONFIG_DIR) + "/fluoro_filter.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for AnnularRingHolder struct
///----------------------------------------------------------------------
ComponentProperties defaultAnnularRingHolder =
{
	ComponentType::AnnularRingHolder,
	{ { {"controller_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"controller_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"driver_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"motor_serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"ar_axis_resolution"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm"}, {"0"}, {"4294967295"}, {"390"}, {}, {}, {}, {}, {} },
	  { {"ar_axis_error_band"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm"}, {"0"}, {"4294967295"}, {"1172"}, {}, {}, {}, {}, {} },
	  { {"ring_count"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"3"}, {}, {"0"}, {}, {}, {} },
	  { {"current_ring_position"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-1"}, {}, {"0"}, {}, {}, {} },
	  { {"ring_bright_field_position"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-158437000"}, {}, {"0"}, {}, {}, {} },
	  { {"ring_fluorescence_position"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-40000000"}, {}, {"0"}, {}, {}, {} },

	  { {"display_name_objective"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"4X"}, {}, {}, {}, {}, {}, {"rings"}, {0}  },
	  { {"ring_position_objective"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-58189000"}, {}, {"0"}, {}, {}, {}, {"rings"}, {0}  },

	  { {"display_name_objective"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"10X"}, {}, {}, {}, {}, {}, {"rings"}, {1}  },
	  { {"ring_position_objective"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-107648000"}, {}, {"0"}, {}, {}, {}, {"rings"}, {1}  },

	  { {"display_name_objective"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {"rings"}, {2}  },
	  { {"ring_position_objective"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-1"}, {}, {"0"}, {}, {}, {}, {"rings"}, {2}  },

	  { {"display_name_objective"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {"rings"}, {3}  },
	  { {"ring_position_objective"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-1"}, {}, {"0"}, {}, {}, {}, {"rings"}, {3}  },

	  { {"display_name_objective"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {"rings"}, {4}  },
	  { {"ring_position_objective"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-1"}, {}, {"0"}, {}, {}, {}, {"rings"}, {4}  },

	  { {"ar_maximum_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"20000000"}, {}, {}, {}, {}, {} },
	  { {"ar_set_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"20000000"}, {"15000000"}, {}, {}, {}, {}, {} },
	  { {"ar_acceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"400000000"}, {}, {}, {}, {}, {} },
	  { {"ar_deceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"400000000"}, {}, {}, {}, {}, {} },
	  { {"ar_jogging_speed"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"20000000"}, {"15000000"}, {}, {}, {}, {}, {} },
	  { {"ar_jogging_acceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"ar_jogging_deceleration"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"ar_default_location_offset"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"home_location"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"homing_speed"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"500000"}, {}, {}, {}, {}, {} },
	  { {"homing_acceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"homing_deceleration"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nm/s/s"}, {"0"}, {"4294967295"}, {"100000000"}, {}, {}, {}, {}, {} },
	  { {"max_allowable_position_error"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"2"}, {}, {}, {}, {}, {} },
	  { {"end_of_travel_limit_position_+"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"1400000"}, {}, {}, {}, {}, {} },
	  { {"end_of_travel_limit_position_-"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-159900000"}, {}, {}, {}, {}, {} },
	  { {"soft_travel_limit_position_+"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"400000"}, {}, {}, {}, {}, {} },
	  { {"soft_travel_limit_position_-"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"int32_t"}, {"nanometers"}, {"-2147483648"}, {"2147483647"}, {"-158900000"}, {}, {}, {}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumNone"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_factor_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_factor_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_factor_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "ann_ring_hldr.cfg" },
	{ (std::string (CONFIG_DIR) + "/ann_ring_hldr.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for OpticalColumn struct
///----------------------------------------------------------------------
ComponentProperties defaultOpticalColumn =
{
	ComponentType::OpticalColumn,
	{ { {"vendor"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Olympus"}, {}, {}, {}, {}, {} },
	  { {"model"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"version"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"serial_number"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"magnification_factor_lens"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"1.0"}, {"1.0"}, {}, {}, {}, {} },
	  { {"magnification_factor_camera_mount"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"1.0"}, {"1.0"}, {}, {}, {}, {} },

	  { {"display_name_objective"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"4X"}, {}, {}, {}, {}, {}, {"objectives"}, {0}  },
	  { {"magnification_factor_objective"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"4.0"}, {"1.0"}, {}, {}, {}, {}, {"objectives"}, {0}  },
	  { {"turret_position_objective"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int16_t"}, {}, {"-32768"}, {"32767"}, {"2"}, {}, {}, {}, {}, {}, {"objectives"}, {0}  },

	  { {"display_name_objective"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"10X"}, {}, {}, {}, {}, {}, {"objectives"}, {1}  },
	  { {"magnification_factor_objective"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"10.0"}, {"1.0"}, {}, {}, {}, {}, {"objectives"}, {1}  },
	  { {"turret_position_objective"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int16_t"}, {}, {"-32768"}, {"32767"}, {"5"}, {}, {}, {}, {}, {}, {"objectives"}, {1}  },

	  { {"display_name_objective"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {"objectives"}, {2}  },
	  { {"magnification_factor_objective"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0.0"}, {"1.0"}, {}, {}, {}, {}, {"objectives"}, {2}  },
	  { {"turret_position_objective"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int16_t"}, {}, {"-32768"}, {"32767"}, {"-1"}, {}, {}, {}, {}, {}, {"objectives"}, {2}  },

	  { {"display_name_objective"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {"objectives"}, {3}  },
	  { {"magnification_factor_objective"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"1.0"}, {"1.0"}, {}, {}, {}, {}, {"objectives"}, {3}  },
	  { {"turret_position_objective"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int16_t"}, {}, {"-32768"}, {"32767"}, {"-2"}, {}, {}, {}, {}, {}, {"objectives"}, {3}  },

	  { {"display_name_objective"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {"objectives"}, {4}  },
	  { {"magnification_factor_objective"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0.0"}, {"1.0"}, {}, {}, {}, {}, {"objectives"}, {4}  },
	  { {"turret_position_objective"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"int16_t"}, {}, {"-32768"}, {"32767"}, {"-1"}, {}, {}, {}, {}, {}, {"objectives"}, {4}  },

	  { {"active_turret_position"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"2"}, {}, {}, {}, {}, {} },
	  { {"safe_magnification_change_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"50000000"}, {}, {}, {}, {}, {} },
	  { {"focus_total_travel"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"10500000"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"auto_focus_start_offset"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"6000000"}, {}, {}, {}, {}, {} },
	  { {"auto_focus_increment"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"40000"}, {}, {}, {}, {}, {} },
	  { {"auto_focus_image_count"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"50"}, {}, {}, {}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumNone"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_coefficient_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "optical_col.cfg" },
	{ (std::string (CONFIG_DIR) + "/optical_col.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for Camera struct
///----------------------------------------------------------------------
ComponentProperties defaultRetigaR3Camera =
{
	ComponentType::Camera,
	{ { {"vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"pixel_count_x"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"ea"}, {"0"}, {"65535"}, {"1920"}, {}, {}, {}, {}, {} },
	  { {"pixel_count_y"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"ea"}, {"0"}, {"65535"}, {"1460"}, {}, {}, {}, {}, {} },
	  { {"pixel_size_x"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"4540"}, {}, {}, {}, {}, {} },
	  { {"pixel_size_y"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"4540"}, {}, {}, {}, {}, {} },
	  { {"sensor_width"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"6600000"}, {}, {}, {}, {}, {} },
	  { {"sensor_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"8800000"}, {}, {}, {}, {}, {} },
	  { {"camera_color_depth"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"bits"}, {"0"}, {"65535"}, {"14"}, {}, {}, {}, {}, {} },
	  { {"scaled_color_depth"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"bits"}, {"0"}, {"65535"}, {"8"}, {}, {}, {}, {}, {} },
	  { {"number_of_color_channels_used"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"1"}, {}, {}, {}, {}, {} },
	  { {"gain"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint16_t"}, {}, {"1"}, {"2"}, {"2"}, {"1"}, {"0"}, {}, {"1"}, {"2"} },
	  { {"exposure"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"us"}, {"1000"}, {"1000000"}, {"50000"}, {"1"}, {"3"}, {}, {"1000"}, {"1000000"} },
	  { {"bin_factor"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint16_t"}, {}, {"1"}, {"16"}, {"1"}, {"1"}, {"0"}, {"[1, 2, 4, 6, 8, 12, 16]"}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumNone"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_coefficient_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "camera.cfg" },
	{ (std::string (CONFIG_DIR) + "/camera.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for Incubator struct
///----------------------------------------------------------------------
ComponentProperties defaultIncubator =
{
	ComponentType::Incubator,
	{ { {"vendor"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"model"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"version"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"serial_number"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"plate_capacity"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"250"}, {}, {}, {}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumNone"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_coefficient_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "incubator.cfg" },
	{ (std::string (CONFIG_DIR) + "/incubator.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for CxPM struct
///----------------------------------------------------------------------
ComponentProperties defaultCxPM =
{
	ComponentType::CxPM,
	{ { {"vendor"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"model"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"version"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"serial_number"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"maxium_speed"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {"nm/s"}, {"0"}, {"4294967295"}, {"10000000"}, {}, {}, {}, {}, {} },
	  { {"home_location"}, EditableType::EditableFixed, PersistType::HardPersist, {}, {}, {}, {}, {}, {}, {}, {}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumNone"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_coefficient_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "cxpm.cfg" },
	{ (std::string (CONFIG_DIR) + "/cxpm.cfg") }
};

///----------------------------------------------------------------------
/// Functions and defaults for BarcodeReader struct
///----------------------------------------------------------------------
ComponentProperties defaultBarcodeReader =
{
	ComponentType::BarcodeReader,
	{ { {"vendor"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"model"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"version"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"serial_number"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumNone"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_coefficient_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "bcreader.cfg" },
	{ (std::string (CONFIG_DIR) + "/bcreader.cfg") }
};

ComponentProperties defaultBaslerCamera =
{
	ComponentType::Camera,
	{ { {"vendor"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Basler"}, {}, {}, {}, {}, {} },
	  { {"model"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"daA3840-45UM"}, {}, {}, {}, {}, {} },
	  { {"version"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"serial_number"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"string"}, {}, {}, {}, {"Unknown"}, {}, {}, {}, {}, {} },
	  { {"cpd_id"}, EditableType::EditableApplicationOnly, PersistType::HardPersist, {"uint32_t"}, {}, {"0"}, {"4294967295"}, {"0"}, {}, {}, {}, {}, {} },
	  { {"pixel_count_x"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"ea"}, {"0"}, {"65535"}, {"3840"}, {}, {}, {}, {}, {} },
	  { {"pixel_count_y"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"ea"}, {"0"}, {"65535"}, {"2160"}, {}, {}, {}, {}, {} },
	  { {"pixel_size_x"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"2000"}, {}, {}, {}, {}, {} },
	  { {"pixel_size_y"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"2000"}, {}, {}, {}, {}, {} },
	  { {"sensor_width"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"7700000"}, {}, {}, {}, {}, {} },
	  { {"sensor_height"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint32_t"}, {"nanometers"}, {"0"}, {"4294967295"}, {"4300000"}, {}, {}, {}, {}, {} },
	  { {"camera_color_depth"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"bits"}, {"0"}, {"65535"}, {"14"}, {}, {}, {}, {}, {} },
	  { {"scaled_color_depth"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {"bits"}, {"0"}, {"65535"}, {"8"}, {}, {}, {}, {}, {} },
	  { {"number_of_color_channels_used"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"65535"}, {"1"}, {}, {}, {}, {}, {} },
	  { {"gain"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint16_t"}, {}, {"0"}, {"48"}, {"12"}, {"1"}, {"1"}, {}, {"0"}, {"48"} },
	  { {"exposure"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint32_t"}, {"us"}, {"26"}, {"999999"}, {"150000"}, {"1"}, {"3"}, {}, {"26"}, {"999999"} },
	  { {"bin_factor"}, EditableType::EditableCPD_Application, PersistType::HardPersist, {"uint16_t"}, {}, {"1"}, {"4"}, {"1"}, {"1"}, {"0"}, {"[1, 2, 4]"}, {}, {} },
	  { {"calibration_type"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"enumCalibrationType"}, {}, {}, {}, {"enumNone"}, {}, {}, {}, {}, {} },
	  { {"last_calibration_date"}, EditableType::EditableFixed, PersistType::HardPersist, {"uint32_t"}, {}, {}, {}, {"0"}, {}, {}, {}, {}, {} },
	  { {"calibration_coefficient_1"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_2"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} },
	  { {"calibration_coefficient_3"}, EditableType::EditableCPDOnly, PersistType::HardPersist, {"float"}, {}, {"-3.4e+38"}, {"3.4e+38"}, {"0"}, {"1"}, {"3"}, {}, {"-3.4e+38"}, {"3.4e+38"} } },
	{ "camera.cfg" },
	{ (std::string (CONFIG_DIR) + "/camera.cfg") }
};
