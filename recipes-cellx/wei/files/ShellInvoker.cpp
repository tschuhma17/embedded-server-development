#include <iostream>
#include <sstream>
#include "es_des.h"
#include "ShellInvoker.hpp"

using namespace std;

/// Calls popen_hand.py, which has predefined integer inputs and returns a short string of ASCII characters.
int ShellInvoker::InvokePopenHand( const std::string & cmd, const char * threadName, const char * subsystem, string & resultStr )
{
	int status = STATUS_OK;
	stringstream popenCallStream;
	popenCallStream << "python3 /usr/bin/popen_hand.py " << cmd;
	cout << popenCallStream.str() << endl;
	FILE* fp = popen( popenCallStream.str().c_str(), "re");
	if (fp == NULL)
	{
		string errorStr = strerror(errno);
		LOG_ERROR( threadName, subsystem, errorStr.c_str() );
		status = STATUS_DEVICE_ERROR;
	}
	else
	{
		int numberOfPayloadChars = 0;
		status = ReadNumber( fp, threadName, numberOfPayloadChars );
		if (status == STATUS_OK)
		{
			char * payloadCStr = new char[numberOfPayloadChars+1]();
			int bytesRead = fread( payloadCStr, 1, numberOfPayloadChars, fp );
			resultStr = payloadCStr;
			cout << resultStr << endl;
			delete[] payloadCStr;
		}
		stringstream msg;
		if (resultStr == "ok" )
		{
			msg << " succeeded";
			LOG_TRACE( threadName, subsystem, msg.str().c_str() );
		}
		else if (resultStr == "nop" )
		{
			msg << " is a no-op";
			LOG_WARN( threadName, subsystem, msg.str().c_str() );
		}
		else if (resultStr.find_first_not_of("0123456789.") == std::string::npos )
		{
			msg << " is a number";
			LOG_TRACE( threadName, subsystem, msg.str().c_str() );
		}
		else
		{
			msg << " erred";
			LOG_ERROR( threadName, subsystem, msg.str().c_str() );
			status = STATUS_DEVICE_ERROR;
		}
	}
	if (pclose( fp ) < 0)
	{
		string errorStr = strerror(errno);
		LOG_ERROR( threadName, subsystem, errorStr.c_str() );
		status = STATUS_DEVICE_ERROR;
	}
	return status;
}

/// Calls either rochacha.py or popen_snap.py. Both scripts return long strings of hexadecimal digits, which
/// encode raw image data. InvokePopenSnap() converts them to binary data and returns them in *bufferPtrOut,
/// which must be freed by the caller. InvokePopenSnap() also returns the size of the binary data in bufferSizeOut.
int ShellInvoker::InvokePopenSnap( bool mock, const char * threadName, size_t & bufferSizeOut, void ** bufferPtrOut )
{
	int status = STATUS_OK;
	int numberOfPayloadChars = 0;
	string popenCallStr;
	if (mock)
	{
//		popenCallStr = "python3 rochacha.py";
		popenCallStr = "python3 /usr/bin/popen_snap.py"; // This currently does emulation.
		LOG_TRACE( threadName, CAMERA, popenCallStr.c_str() );
	}
	else
	{
		popenCallStr = "python3 /usr/bin/popen_snap.py";
	}
	FILE* fp = popen( popenCallStr.c_str(), "re");
	if (fp == NULL)
	{
		string errorStr = strerror(errno);
		LOG_ERROR( threadName, CAMERA, errorStr.c_str() );
		status = STATUS_DEVICE_ERROR;
	}
	else
	{
		LOG_TRACE( threadName, CAMERA, "popen call succeeded" );
		status = ReadNumber( fp, threadName, numberOfPayloadChars );
		if ((numberOfPayloadChars % 2) == 1)
		{
			stringstream msg;
			msg << "payload size " << numberOfPayloadChars << " is odd";
			LOG_ERROR( threadName, CAMERA, msg.str().c_str() );
			status = STATUS_DEVICE_ERROR;
		}
	}
	if (status == STATUS_OK)
	{
		char * tempStrBuffer = new char[numberOfPayloadChars+1]();
		int bytesRead = fread( tempStrBuffer, 1, numberOfPayloadChars, fp );
		LOG_TRACE( threadName, CAMERA, "read done" );
		if (bytesRead != numberOfPayloadChars)
		{
			stringstream msg;
			msg << "Read only " << bytesRead << " of " << numberOfPayloadChars << " bytes";
			LOG_ERROR( threadName, CAMERA, msg.str().c_str() );
			status = STATUS_DEVICE_ERROR;
		}
		if (status == STATUS_OK)
		{
			bufferSizeOut = numberOfPayloadChars/2; /// Every 2 characters will be converted into one byte.
			cout << "bufferSizeOut = " << bufferSizeOut << endl;
			*bufferPtrOut = malloc( bufferSizeOut );
			char * bytes = (char *)*bufferPtrOut;
			LOG_TRACE( threadName, CAMERA, "malloc done" );
			char number[3] = "qq";
			for (int i = 0; i < bufferSizeOut; i++)
			{
				number[1] = tempStrBuffer[2*i];
				number[0] = tempStrBuffer[(2*i)+1];
				bytes[i] = (char) strtol(number, nullptr, 16);
			}
			LOG_TRACE( threadName, CAMERA, "malloc done" );
		}
		delete[] tempStrBuffer;
		LOG_TRACE( threadName, CAMERA, "deleted tempStrBuffer" );
	}
	if (pclose( fp ) < 0)
	{
		string errorStr = strerror(errno);
		LOG_ERROR( threadName, FLUORESCENCE_LIGHT, errorStr.c_str() );
		status = STATUS_DEVICE_ERROR;
	}
	LOG_TRACE( threadName, CAMERA, "finished pclose()" );
	return status;
}

/// Reads characters until it reads ':'. Interprets the characters as a number. Returns the number. Leaves
/// fp's position at the next character after ':'.
int ShellInvoker::ReadNumber( FILE * fp, const char * threadName, int & number)
{
	int status = STATUS_OK;
	/// Expect content in the form of "###:abc" where ### is the number of characters after ':'.
	int numberStrLen = 10;
	int maxNumberChars = numberStrLen - 1; /// Protect the terminating null.
	char * numberCStr = new char[numberStrLen]();
	char * buffer = new char[2](); /// 1 character and 1 terminating null.
	int index = 0;
	int bytesRead = 0;
	char c = 0;
	while ((index < maxNumberChars) && (status == STATUS_OK)) {
		bytesRead = fread( buffer, 1, 1, fp );
		if (bytesRead != 1)
		{
			LOG_ERROR( threadName, FLUORESCENCE_LIGHT, "Failed to read number returned from popen()" );
			status = STATUS_DEVICE_ERROR;
			break;
		}
		c = buffer[0];
		if (c == ':') break; /// Exit loop without writing ':' to numberCStr.
		else
		{
			numberCStr[index] = c;
			index++;
		}
	}
	/// Double-check the terminating null.
	if ((int)numberCStr[maxNumberChars] != 0)
	{
		LOG_ERROR( threadName, FLUORESCENCE_LIGHT, "buffer overrun" );
		status = STATUS_DEVICE_ERROR;
	}
	/// Interpret the number
	if (status == STATUS_OK)
	{
		number = atoi( numberCStr );
		cout << numberCStr << "=" << number << endl;
	}
	delete[] numberCStr;
	delete[] buffer;
	return status;
}

