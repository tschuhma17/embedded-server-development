/* USAFW_TARGET_CONFIG.h */

/** Copyright (C)2022 USA Firmware, LLC.  All rights reserved.
 * 
 *  USA FIRMWARE, LLC MULTI-CLIENT PROPRIETARY INFORMATION LICENSE STATEMENT
 * 
 *  This source code file contains proprietary information, and is the
 *  intellectual property of USA Firmware, LLC.  It is developed and
 *  maintained for the common benefit of both USA Firmware, LLC, and one or
 *  more of its past, present, and future clients.  This material does not
 *  represent a "work for hire", and it is licensed, not sold.
 * 
 *  Clients of USA Firmware to whom it delivers this material are granted a
 *  permanent, but non-exclusive, license for its use, in modified or
 *  unmodified form, to produce binary materials and other derivative works
 *  to support the project for which the material was provided, and a
 *  otherwise granted by its author, USA Firmware, LLC.  The source code
 *  material remains proprietary, however, and may not be made available to
 *  the general public, or to third-parties unrelated to the development,
 *  testing, certification, or qualification of the project. 
 * 
 *  Both the source code, and any derivative works thereof, may be made
 *  available to such contractors and agents as client may find necessary to
 *  complete the project for which it was provided.  Any such contractors and
 *  agents remain equivalently bound by this agreement, and may not increase
 *  its scope without prior arrangement with USA Firmware, LLC.
 * 
 *  This license statement applies to all files in which it is present, but
 *  does not apply to client's other proprietary, application-specific source
 *  files, or other project-specific materials.
 * 
 *  In no case may this license statement be modified in any revision or
 *  derivative work without the express written permission of USA Firmware, LLC.
 */

/** This file is used to tune the behavior of the USA Firmware Standard
 *  Embedded Development Library for appropriate behavior on resource-
 *  constrained targets, or other special environments.
 */
#ifndef __USAFW_TARGET_CONFIG_H__
#define __USAFW_TARGET_CONFIG_H__

/** These defines specify the MAJOR PLATFORM TYPE.
 *  This affects:
 *  - Synchronization primitives available
 *  - Multithreading
 *  - I/O
 *  - Networking
 *
 *  EXACTLY ONE of these should be enabled:
 * 
 *  USAFW_TARGET_LINUX
 *      Intended for embedded or desktop Linux.
 * 
 *  USAFW_TARGET_FREERTOS
 *      Intended for FreeRTOS.
 * 
 *  USAFW_TARGET_SEL4
 *      Intended for seL4.
 * 
 *  USAFW_TARGET_ANDROID
 *      Intended for Android, Calyx, or LineageOS.
 */
#define USAFW_TARGET_LINUX


/** These defines specify the TARGET MEMORY SIZE CLASS.
 *  This affects:
 *  - String buffer sizes, including for debugging information.
 *  - The number of debug buffers maintained
 *  - Structure and class guard byte sizes.
 *  - Whether or not certain classes allow dynamic memory allocation.
 * 
 *  EXACTLY ONE of these should be enabled:
 * 
 *  USAFW_TARGET_TINY
 *      Intended primarily for microcontroller projects.
 *      Dynamic allocation of SEDL classes is disallowed.
 *      Recursive function calls are disallowed.
 *      Run-time type information is not available.
 *      Most string buffers will be 16 bytes or smaller.
 *      Memory guard regions are not used.
 * 
 *  USAFW_TARGET_SMALL
 *      Intended primarilSAFW_DISABLE_LOCK_DEBUGGING
 *  USAFW_TARGET_COMPACT
 *      Intended primarily for "compact targets"-- low-resource systems
 *      running embedded SAFW_DISABLE_LOCK_DEBUGGINGversions of Linux (e.g., Buildroot) or Windows
 *      (e.g., Windows Embedded Standard).
 *      Recursive function calls require bailout limits.
 *      Dynamic allocation of SEDL classes is allowed.
 *      Most string buffers will be 80 bytes or smaller.
 *      Both leading and trailing guard regions are used; 4 bytes each.
 * 
 *  USAFW_TARGET_NORMAL
 *      Intended primarily for development work within VMs, or for utility
 *      programs running on normal computers.
 *      Recursive function calls are unconstrained.
 *      Dynamic allocation of SEDL classes is allowed.
 *      Most string buffers will be 256 bytes or smaller.
 *      Both leading and trailing guard regions are used; 16 bytes each.
 *  
 *  USAFW_TARGET_LARGE
 *      Intended primarily for troubleshooting work within VMs or on heavy
 *      hardware, where large amounts of debug information may need to be
 *      collected in memory.
 *      Recursive function calls are unconstrained.
 *      Dynamic allocation of SEDL classes is allowed.
 *      Most string buffers will be 4096 bytes or smaller.
 *      Both leading and trailing guard regions are used; 64 bytes each (to
 *      catch overflows with SIMD instruction sequences).
 */
#define USAFW_TARGET_LARGE

#endif
