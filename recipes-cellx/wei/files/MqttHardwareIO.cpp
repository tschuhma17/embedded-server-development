#include <iostream>
#include <sstream>
#include <cassert>
#include "es_des.h"
#include "MqttHardwareIO.hpp"
#include "Sundry.hpp"
#include "DesHelper.hpp"

using namespace std;

ContextBucket g_context;

/// A mutex for making sure MessageArrived() finishes processing one message before being triggered for another.
std::mutex g_MqttHwMsgArrivedMutex;

///----------------------------------------------------------------------
/// Callbacks
///----------------------------------------------------------------------
volatile MQTTClient_deliveryToken deliveredtokenHwIo;

void deliveredHwIo(void *context, MQTTClient_deliveryToken dt)
{
//    cout << "--- callback --- Message with token value " << dt << " delivery confirmed" << endl;;
    deliveredtokenHwIo = dt;
}

void connlostHwIo(void *context, char *cause)
{
    cout << "--- callback --- Connection lost" << endl;
    cout << "--- callback ---      cause: " << cause << endl;
}

///----------------------------------------------------------------------
/// Handle an ack message and a status message.
///----------------------------------------------------------------------
static int MessageArrived(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
	unique_lock<mutex> lock( g_MqttHwMsgArrivedMutex );
    bool messageParsed = false;
    DesHelper desHelper("", "");
    string messageType = "";
    ContextBucket * contextBucket = (ContextBucket *)context;
    contextBucket->statusCode = 0;
    bool typeIdentified = false;
	string ackTopic = "";
	{
		stringstream streamtopic;
		streamtopic << TOPIC_MICROSCOPE << CHANNEL_ACK;
		ackTopic= streamtopic.str();
	}
	string statusTopic = "";
	{
		stringstream streamtopic;
		streamtopic << TOPIC_MICROSCOPE << CHANNEL_STATUS;
		statusTopic= streamtopic.str();
	}

	{
		stringstream msg;
		msg << "Received MQTT msg on topic " << topicName << " " << message;
		LOG_TRACE( "unk", "OC/Flouro", msg.str().c_str() );
	}
    /// The payload is JASON. convert it to an object so we can analyze it.
    json_object *jdesMsgObj = desHelper.ParseJsonDesMessage( (const char*)message->payload, &messageParsed );
    string desMsgStr = json_object_to_json_string( jdesMsgObj );
	{
		stringstream msg;
		msg << "Received MQTT payload of " << desMsgStr;
		LOG_TRACE( "unk", "OC/Flouro", msg.str().c_str() );
	}

	if (messageParsed)
	{
		json_object *jdesMsgFieldObj = json_object_object_get( jdesMsgObj, MESSAGE_TYPE );
		
		if (jdesMsgFieldObj)
		{
			messageType = json_object_get_string(jdesMsgFieldObj);
			{
				stringstream msg;
				msg << "Received message_type is " << messageType;
				LOG_TRACE( "unk", "OC/Flouro", msg.str().c_str() );
			}
			std::transform( messageType.begin(), messageType.end(), messageType.begin(), (int(*)(int)) std::tolower );
			if ( (messageType.compare( contextBucket->commandMessageType ) == 0) &&
				 (ackTopic == topicName) ) /// Is an ACK
			{
				typeIdentified = true;
				contextBucket->ackReceived = true;
				contextBucket->ack = STATUS_OK; // TODO: Extract from status_code instead.
			}
			if (messageType == TEXT_ALERT)
			{
				typeIdentified = true;
			}
			if ( (messageType.compare( contextBucket->commandMessageType ) == 0) &&
				 (statusTopic == topicName) ) /// Is a STATUS
			{
				typeIdentified = true;
				LOG_TRACE( "unk", "OP/Fluoro", "Message confirmed to be a STATUS." );
				contextBucket->statusReceived = true;
				contextBucket->statusMsg = "";
				
				json_object * jstatusPayload = json_object_object_get( jdesMsgObj, PAYLOAD );
				if (jstatusPayload)
				{
					LOG_TRACE( "unk", "OP/Fluoro", "Status payload captured." );
					json_object_put( contextBucket->statusPayloadCopyObj ); /// Free it
					contextBucket->statusPayloadCopyObj = NULL;
					int rc = json_object_deep_copy( jstatusPayload, &(contextBucket->statusPayloadCopyObj), NULL );
					if (rc != 0) LOG_ERROR( "unk", "OP/Fluoro", " Failed to free contextBucket->statusPayloadCopyObj." );
				}
				else
				{
					LOG_WARN( "unk", "OP/Fluoro", "Status payload not found." );
					json_object_put( contextBucket->statusPayloadCopyObj );
					contextBucket->statusPayloadCopyObj = NULL;
				}

				jdesMsgFieldObj = json_object_object_get( jdesMsgObj, STATUS_CODE );
				if (jdesMsgFieldObj)
				{
					long statusCode = json_object_get_int64( jdesMsgFieldObj );
					stringstream msg;
					msg << "Setting contextBucket->statusCode to " << statusCode;
					LOG_TRACE( "unk", "OC/Flouro", msg.str().c_str() );
					contextBucket->statusCode = statusCode;
				}
				else
				{
					LOG_WARN( "unk", "OP/Fluoro", "Status_code not found in status message header." );
					jdesMsgFieldObj = json_object_object_get( jstatusPayload, STATUS_CODE );
					if (jdesMsgFieldObj)
					{
						long statusCode = json_object_get_int64( jdesMsgFieldObj );
						stringstream msg;
						msg << "Setting contextBucket->statusCode to " << statusCode;
						LOG_TRACE( "unk", "OC/Flouro", msg.str().c_str() );
						contextBucket->statusCode = statusCode;
					}
					else
					{
						LOG_WARN( "unk", "OP/Fluoro", "Status_code not found in status message payload either." );
					}
				}
			}
			if (!typeIdentified)
			{
				stringstream msg;
				msg << "Failed to identify message_type " << messageType;
				LOG_ERROR( "unk", "OC/Flouro", msg.str().c_str() );
			}
		}
		else
		{
			LOG_ERROR( "unk", "OC/Flouro", "MqttHardwareIO::MessageArrived: 'message_type' not found" );
		}
	}
	return 1;
}



///----------------------------------------------------------------------
/// MqttHardwareIO constructor
///----------------------------------------------------------------------
MqttHardwareIO::MqttHardwareIO( std::string & ipBrokerIpAddress ) :
	m_brokerIpAddress(ipBrokerIpAddress),
	m_connectedToBroker(false)
{
    m_client;
    m_conn_opts = MQTTClient_connectOptions_initializer;
    m_pubmsg = MQTTClient_message_initializer;
    m_token;
};

///----------------------------------------------------------------------
/// MqttHardwareIO destructor
///----------------------------------------------------------------------
MqttHardwareIO::~MqttHardwareIO()
{
	if (m_connectedToBroker)
	{
		MQTTClient_disconnect( m_client, MQTT_HW_IO_TIMEOUT );
		MQTTClient_destroy( &m_client );
	}
}

///----------------------------------------------------------------------
/// InitializeListOfTopics
///----------------------------------------------------------------------
void MqttHardwareIO::InitializeListOfTopics( ComponentType componentType )
{
	if (componentType == ComponentType::OpticalColumn)
	{
		string topic = TOPIC_MICROSCOPE;
		topic += CHANNEL_ACK;
		m_listOfTopics.push_front(topic);
	
		topic = TOPIC_MICROSCOPE;
		topic += CHANNEL_ALERT;
		m_listOfTopics.push_front(topic);
	
		topic = TOPIC_MICROSCOPE;
		topic += CHANNEL_STATUS;
		m_listOfTopics.push_front(topic);
	}
	/// Other components are not supported. This whole class is developed
	/// to support the microscope. Much of the naming is generic just in
	/// case this class finds new, additional uses in the future.
}

///----------------------------------------------------------------------
/// ConnectToBroker
///----------------------------------------------------------------------
bool MqttHardwareIO::ConnectToBroker()
{
	if (!m_connectedToBroker)
	{
		std::stringstream m_address;
		m_address <<  "tcp://" << m_brokerIpAddress << ":" << BROKER_PORT;
		MQTTClient_create(
			&m_client,
			m_address.str().c_str(),
			SENDER_CLIENTID_MICRO,
			MQTTCLIENT_PERSISTENCE_NONE,
			NULL);
		m_conn_opts.keepAliveInterval = KEEP_ALIVE_INTERVAL;
		m_conn_opts.cleansession = CLEAN_SESSION;

		MQTTClient_setCallbacks(m_client, (void *)&g_context, connlostHwIo, MessageArrived, deliveredHwIo);

		/// Attempt to connect to the broker.
		/// See MQTTClient.h for possible return codes.
		int mqttReturnCode = MQTTClient_connect(m_client, &m_conn_opts);
		if (mqttReturnCode == MQTTCLIENT_SUCCESS)
		{
			m_connectedToBroker = true;
			stringstream msg;
			msg << "Connection succeeded; return code is " << mqttReturnCode;
			LOG_TRACE( "unk", "OC/Flouro", msg.str().c_str() );
			for (const auto& topic : m_listOfTopics)
			{
				MQTTClient_subscribe(m_client, topic.c_str(), QOS);
			}
		}
		else
		{
			m_connectedToBroker = false;
			stringstream msg;
			msg << "Failed to connect, return code " << mqttReturnCode;
			LOG_ERROR( "unk", "OC/Flouro", msg.str().c_str() );
			std::this_thread::sleep_for( 250ms );
		}
	}

	return m_connectedToBroker;
}
	
///----------------------------------------------------------------------
/// IsConnectedToBroker
///----------------------------------------------------------------------
bool MqttHardwareIO::IsConnectedToBroker()
{
	return m_connectedToBroker;
}

///----------------------------------------------------------------------
/// SendCommand
///----------------------------------------------------------------------
int MqttHardwareIO::SendCommand( std::string & desCallMessageIn, std::string & hardwareClientTopicNameIn )
{
	m_pubmsg.qos = QOS;
	m_pubmsg.retained = 0;
	char * m_payloadBuffer = new char[desCallMessageIn.size() + 1];
	strcpy(m_payloadBuffer, desCallMessageIn.c_str());
	m_pubmsg.payload = (void *) m_payloadBuffer;
	m_pubmsg.payloadlen = (int)desCallMessageIn.size();

	/// Send to the MQTT broker that which was received from the inter-thread inBox
	int mqttReturnCode = MQTTClient_publishMessage(m_client, hardwareClientTopicNameIn.c_str(), &m_pubmsg, &m_token);
	/// See MQTTClient.h for possible return codes.
	if (mqttReturnCode == MQTTCLIENT_SUCCESS)
	{
		LOG_TRACE( "unk", "OC/Flouro", "command sent" );
	}
	else
	{
		stringstream msg;
		msg << "Error code is " << mqttReturnCode;
		LOG_ERROR( "unk", "OC/Flouro", msg.str().c_str() );
	}
	while (deliveredtokenHwIo != m_token) { }

	/// See MQTTClient.h for possible return codes.
	mqttReturnCode = MQTTClient_waitForCompletion(m_client, m_token, MQTT_HW_IO_TIMEOUT);
	delete[] m_payloadBuffer; /// clears memory allocated in the handlers for all message types.
	return mqttReturnCode;
}

///----------------------------------------------------------------------
/// CommandTheHardwareClient
///
/// This method allocates memory for jpayloadObjOut, so remember to
/// free it, using json_object_put().
///----------------------------------------------------------------------
int MqttHardwareIO::CommandTheHardwareClient( std::string & messageTypeIn,
											  std::string & desCallMessageIn,
											  std::string & hardwareClientTopicNameIn,
											  bool & ackReceivedOut,
											  int & ackOut,
											  json_object ** jpayloadObjOut )
{
	/// Initialize the values that will be filled by the MessageArrived
	/// callback after we subscribe, below.
	g_context.ackReceived = false;
	g_context.ack = 0;
	g_context.statusReceived = false;
	g_context.statusMsg = "";
	g_context.commandMessageType = messageTypeIn;
	g_context.statusPayloadCopyObj = nullptr;

	if (m_connectedToBroker)
	{
		LOG_TRACE( "unk", "OC/Flouro", "connection to broker is OK" );
	}
	else
	{
		LOG_ERROR( "unk", "OC/Flouro", "no connection to broker", "Please call InitializeListOfTopics() and ConnectToBroker() before calling CommandTheHardwareClient()."  );
		return 0;
	}

    int mqttReturnCode = SendCommand( desCallMessageIn, hardwareClientTopicNameIn );
	
	time_t commandSentTime;
	time( &commandSentTime );
	int diffTime = 0;
	time_t now_time_seconds;
	int deltaTime = 0;
	bool done = false;
	bool timedOut = false;
	/// Wait until we get both an ack and a status, or time out.
	do
	{
		std::this_thread::sleep_for( 250ms );
		time( &now_time_seconds );
		deltaTime = difftime(now_time_seconds, commandSentTime);
//		if (g_context.ackReceived) cout << "MqttHardwareIO::CommandTheHardwareClient: Ack in hand." << endl;
//		if (g_context.statusReceived) cout << "MqttHardwareIO::CommandTheHardwareClient: Status in hand." << endl;
		if (g_context.ackReceived && g_context.statusReceived) done = true;
		if (deltaTime > HW_REPLY_TIMEOUT_SEC)
		{
			done = true;
			timedOut = true;
		}
	} while (!done);

	if (timedOut)
	{
		LOG_WARN( "unk", "OC/Flouro", "Timed out." );
	}
	else
	{
		LOG_TRACE( "unk", "OC/Flouro", "CommandTheHardwareClient() complete." );
		if (*jpayloadObjOut) /// Must be NULL before deep copy. Please do that before calling this method.
		{
			LOG_ERROR( "unk", "OC/Flouro", "payload object not NULL.", "Please make *jpayloadObjOut NULL before passing it to this method." );
		}
		else
		{
//			cout << "MqttHardwareIO::CommandTheHardwareClient: deep-copying payload to *jpayloadObjOut." << endl;
			int rc = json_object_deep_copy( g_context.statusPayloadCopyObj, jpayloadObjOut, NULL );
			if (rc != 0) LOG_ERROR( "unk", "OC/Fluoro", "Deep-copy FAILED." );
		}
	}
	ackOut = g_context.ack;
	ackReceivedOut = g_context.ackReceived;
	{
		stringstream msg;
		msg << "Received status code " << g_context.statusCode;
		LOG_TRACE( "unk", "OC/Flouro", msg.str().c_str() );
	}
	/// If the status message header did not contain a status_code, check the payload for one.
	if (g_context.statusCode == 0)
	{
		long statusCode = STATUS_DEVICE_ERROR;
		json_object * statusCodeObj = json_object_object_get( *jpayloadObjOut, STATUS_CODE );
		if (statusCodeObj)
		{
			statusCode = json_object_get_int64( statusCodeObj );
			stringstream msg;
			msg << "Payload contains statusCode " << statusCode;
			LOG_DEBUG( "unk", "OC/Flouro", msg.str().c_str() );
		}
		return statusCode;
	}
	/// else
	return g_context.statusCode;
}



