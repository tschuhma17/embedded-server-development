#include <sys/stat.h>
#include <iostream>
#include <stdio.h>
#include <cassert>
#include <sstream>
#include "main.hpp"
#include "enums.hpp"
#include "CommandInterpreter.hpp"
#include "Components.hpp"
#include "MsgQueue.hpp"
#include "Fault.hpp"
#include "Sundry.hpp"
#include "json.h"

using namespace std;
/*
inline bool FileExists( const std::string& name )
{
	struct stat buffer;
	return ( stat( name.c_str(), &buffer ) == 0 );
}
*/
///----------------------------------------------------------------------
/// CommandInterpreterThread
///----------------------------------------------------------------------
CommandInterpreterThread::CommandInterpreterThread(const char* threadName) :
	m_thread(nullptr),
	m_threadName(threadName),
	m_inBox(nullptr),
	m_outBoxForOptics(nullptr),
	m_outBoxForInfo(nullptr),
	m_outBoxForCxD(nullptr),
	m_outBoxForCxPM(nullptr),
	m_outBoxForPump(nullptr),
	m_outBoxForRingHolder(nullptr),
   	m_continueRunning(false)
{
//	cout << "CIT: threadName is " << threadName << endl;
	m_desHelper = new DesHelper(); // TODO: Change something here if this thread needs to know machineId or masterId.

	m_parametersCxD = nullptr;
	m_parametersAxesXY = nullptr;
	m_parametersAxisXX = nullptr;
	m_parametersAxisYY = nullptr;
	m_parametersAxisZ = nullptr;
	m_parametersAxisZZ = nullptr;
    m_parametersAxisZZZ = nullptr;
    m_parametersAspirationPump1 = nullptr;
	m_parametersDispensePump1 = nullptr;
	m_parametersDispensePump2 = nullptr;
	m_parametersDispensePump3 = nullptr;
    m_parametersPickingPump1 = nullptr;
    m_parametersWhiteLightSource = nullptr;
	m_parametersFluorescenceLightFilter = nullptr;
    m_parametersAnnularRingHolder = nullptr;
    m_parametersOpticalColumn = nullptr;
	m_parametersCamera = nullptr;
	m_parametersIncubator = nullptr;
	m_parametersCxPM = nullptr;
	m_parametersBarcodeReader = nullptr;

	m_parmetersAreInitialized = false;
}

///----------------------------------------------------------------------
/// ~CommandInterpreterThread
///----------------------------------------------------------------------
CommandInterpreterThread::~CommandInterpreterThread()
{
	delete m_parametersCxD;
	delete m_parametersAxesXY;
	delete m_parametersAxisXX;
	delete m_parametersAxisYY;
	delete m_parametersAxisZ;
	delete m_parametersAxisZZ;
	delete m_parametersAxisZZZ;
    delete m_parametersAspirationPump1;
	delete m_parametersDispensePump1;
	delete m_parametersDispensePump2;
	delete m_parametersDispensePump3;
    delete m_parametersPickingPump1;
    delete m_parametersWhiteLightSource;
	delete m_parametersFluorescenceLightFilter;
    delete m_parametersAnnularRingHolder;
    delete m_parametersOpticalColumn;
	delete m_parametersCamera;
	delete m_parametersIncubator;
	delete m_parametersCxPM;
	delete m_parametersBarcodeReader;

	ExitThread();
}

///----------------------------------------------------------------------
/// CreateThread
///----------------------------------------------------------------------
bool CommandInterpreterThread::CreateThread()
{
	bool success = false;
	if (!m_thread)
	{
		m_continueRunning = true;
		m_thread = std::unique_ptr<std::thread>(new thread(&CommandInterpreterThread::Process, this));
		success = true;
	}
	
	// TODO: Call Inventory::ReadFromFiles(const char* dirPath)

	return success;
}

///----------------------------------------------------------------------
/// ExitThread
///----------------------------------------------------------------------
void CommandInterpreterThread::ExitThread()
{
	m_continueRunning = false;
	/// Send a small message to the inbox, just to break out of listening long enough to exit.
	std::shared_ptr<WorkMsg> ender = make_shared<WorkMsg>( "", "", "", "end", "", true );
	m_inBox->PushMsg( ender );

	if (!m_thread)
		return;

    m_thread->join();
    m_thread = nullptr;
}

///----------------------------------------------------------------------
/// attachInBox
///----------------------------------------------------------------------
void CommandInterpreterThread::AttachInBox(MsgQueue *q)
{
	assert((!m_inBox) && "AttachInBox called second time?");
    m_inBox = q;
}
    
///----------------------------------------------------------------------
/// AttachOutBoxForOptics
///----------------------------------------------------------------------
void CommandInterpreterThread::AttachOutBoxForOptics(MsgQueue *q)
{
	assert((!m_outBoxForOptics) && "AttachOutBox called second time?");
	m_outBoxForOptics = q;
}

///----------------------------------------------------------------------
/// AttachOutBoxForInfo
///----------------------------------------------------------------------
void CommandInterpreterThread::AttachOutBoxForInfo(MsgQueue *q)
{
	assert((!m_outBoxForInfo) && "AttachOutBox called second time?");
	m_outBoxForInfo = q;
}

///----------------------------------------------------------------------
/// AttachOutBoxForCxD
///----------------------------------------------------------------------
void CommandInterpreterThread::AttachOutBoxForCxD(MsgQueue *q)
{
	assert((!m_outBoxForCxD) && "AttachOutBox called second time?");
	m_outBoxForCxD = q;
}

///----------------------------------------------------------------------
/// AttachOutBoxForCxPM
///----------------------------------------------------------------------
void CommandInterpreterThread::AttachOutBoxForCxPM(MsgQueue *q)
{
	assert((!m_outBoxForCxPM) && "AttachOutBox called second time?");
	m_outBoxForCxPM = q;
}

///----------------------------------------------------------------------
/// AttachOutBoxForPump
///----------------------------------------------------------------------
void CommandInterpreterThread::AttachOutBoxForPump(MsgQueue *q)
{
	assert((!m_outBoxForPump) && "AttachOutBox called second time?");
	m_outBoxForPump = q;
}

///----------------------------------------------------------------------
/// AttachOutBoxForRingHolder
///----------------------------------------------------------------------
void CommandInterpreterThread::AttachOutBoxForRingHolder(MsgQueue *q)
{
	assert((!m_outBoxForRingHolder) && "AttachOutBox called second time?");
	m_outBoxForRingHolder = q;
}


///----------------------------------------------------------------------
/// attachOutBox
///----------------------------------------------------------------------
void CommandInterpreterThread::AttachStatusRouteBox(MsgQueue *q)
{
	assert((!m_statusRouteBox) && "AttachOutBox called second time?");
	m_statusRouteBox = q;
}

///----------------------------------------------------------------------
/// SetMachineId
///----------------------------------------------------------------------
void CommandInterpreterThread::SetMachineId(std::string machineId)
{
	if (this->m_desHelper)
	{
		this->m_desHelper->SetMachineId( machineId );
	}
	else
	{
		cout << "CIT: Failed to set machine ID." << endl;
	}
}

void CommandInterpreterThread::SetHardwareStateIfNeeded( ComponentType compType,
														 ComponentProperties * oldProps,
														 ComponentProperties * newProps,
														 const char * propName,
														 MsgQueue * destinationQueue )
{
	FieldWithMetaData newProperty = newProps->GetFieldByName( propName );
	FieldWithMetaData oldProperty = oldProps->GetFieldByName( propName );
	if (oldProperty.setting != newProperty.setting)
	{
		SetHardwareState( compType, propName, newProperty.dataType, newProperty.setting, destinationQueue, nullptr );
	}
}

/// Like SetHardwareStateIfNeeded, but it also arranges moves the focus
/// to a safe distance before moving the turret.
void CommandInterpreterThread::SetTurretStateIfNeeded(  ComponentType compType,
														ComponentProperties * oldProps,
														ComponentProperties * newProps,
														const char * propName,
														MsgQueue * destinationQueue )
{
	FieldWithMetaData newProperty = newProps->GetFieldByName( propName );
	FieldWithMetaData oldProperty = oldProps->GetFieldByName( propName );
	if (oldProperty.setting != newProperty.setting)
	{
		LOG_TRACE( m_threadName, "optical_column", "changing turret state" );
		FieldWithMetaData safeMagnificationChangeHeight = newProps->GetFieldByName( SAFE_MAGNIFICATION_CHANGE_HEIGHT );
		/// This is a bit of a kludge, because we are using a set-state method to perform a move_to_focus_nm command.
		SetHardwareState( compType, FOCUS_NM, "uint32_t", safeMagnificationChangeHeight.setting, destinationQueue, nullptr );
		/// This is heading to the same destintion queue as the focus move above, so they should get processed in the proper sequence.
		SetHardwareState( compType, propName, newProperty.dataType, newProperty.setting, destinationQueue, nullptr );
	}
	else
	{
		LOG_TRACE( m_threadName, "optical_column", "not changing turret state" );
	}
}

/// Like SetHardwareStateIfNeeded, but it also packs speeds and limits for the SH/AR axis move.
void CommandInterpreterThread::SetANRPositionStateIfNeeded( ComponentType compType,
															ComponentProperties * oldProps,
															ComponentProperties * newProps,
															const char * propName,
															MsgQueue * destinationQueue )
{
	FieldWithMetaData newProperty = newProps->GetFieldByName( propName );
	FieldWithMetaData oldProperty = oldProps->GetFieldByName( propName );
	stringstream msg;
	msg << "propName=" << propName;
	msg << "|newProperty=" << newProps->GetFieldByName(CURRENT_RING_POSITION).GetValueAsLong();
	msg << "|oldProperty=" << oldProps->GetFieldByName(CURRENT_RING_POSITION).GetValueAsLong();
	LOG_TRACE( m_threadName, "sh", msg.str().c_str() );
//	if (oldProperty.setting != newProperty.setting)
	if (true)
	{
		LOG_TRACE( m_threadName, "sh", "changing position state 2" );
		json_object * payloadObj = json_object_new_object();

		long currentRingPosition = newProps->GetFieldByName(CURRENT_RING_POSITION).GetValueAsLong();
		int rc = json_object_object_add( payloadObj, CURRENT_RING_POSITION, json_object_new_uint64( currentRingPosition ) );
		bool succeeded = (rc == 0);

		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "ar", m_parametersAnnularRingHolder );

		LOG_TRACE( m_threadName, "sh", "changing position state 3" );
		succeeded &= SetHardwareState( compType, propName, newProperty.dataType, newProperty.setting, destinationQueue, payloadObj );
		if (!succeeded)
		{
			LOG_ERROR( m_threadName, "sh", "Failed to pack current_ring_position and/or speeds" );
		}
	}
}



///----------------------------------------------------------------------
/// InitOneParamsStruct
/// Input parameter paramStruct should be a pointer to a structure that
/// posesses a filePath data element.
///
/// If filePath points to a file that exists, that file is read in to
/// populate paramStruct. Otherwise, paramStruct is serialized *to*
/// the file. Assume that paramStruct was prepared with default values.
///----------------------------------------------------------------------
template<typename T>
static void  InitOneParamsStruct( T paramStruct )
{
	if (FileExists( paramStruct->filePath ))
	{
		std::fstream in( paramStruct->filePath.c_str(), std::ios::in );
		if (in.is_open())
		{
			in >> (*paramStruct);
			in.close();
		}
	}
	else
	{
		std::fstream out( paramStruct->filePath, std::ios::out );
		if (out.is_open())
		{
			out << (*paramStruct);
			out.close();
		}
	}
}

static void InitOnePropertySet( ComponentProperties * propertySet )
{
	if (FileExists( propertySet->filePath ))
	{
		std::fstream in( propertySet->filePath.c_str(), std::ios::in );
		if (in.is_open())
		{
			in >> (*propertySet);
			in.close();
		}
	}
	else
	{
		std::fstream out( propertySet->filePath, std::ios::out );
		if (out.is_open())
		{
			out << (*propertySet);
			out.close();
		}
	}
}

///----------------------------------------------------------------------
/// InitializeWorkingParameters
/// For every hardware component, initialize its working parameters
/// To default values. Then read values from its configuration file if
/// it exists. Otherwise, write the default values to the config file.
///----------------------------------------------------------------------
void CommandInterpreterThread::InitializeWorkingParameters( void )
{
	m_parametersCxD = new ComponentProperties( defaultCxD );
	InitOneParamsStruct( m_parametersCxD );
	m_parametersAxesXY = new ComponentProperties( defaultAxesXY );
	InitOneParamsStruct( m_parametersAxesXY );
    m_parametersAxisXX = new ComponentProperties( defaultAxisXX );
	InitOnePropertySet( m_parametersAxisXX );
	m_parametersAxisYY = new ComponentProperties( defaultAxisYY ); /// Serialize to/from cfg file.
	InitOnePropertySet( m_parametersAxisYY );
	m_parametersAxisZ = new ComponentProperties( defaultAxisZ );
	InitOnePropertySet( m_parametersAxisZ );
	m_parametersAxisZZ = new ComponentProperties( defaultAxisZZ );
	InitOnePropertySet( m_parametersAxisZZ );
	m_parametersAxisZZZ = new ComponentProperties( defaultAxisZZZ );
	InitOnePropertySet( m_parametersAxisZZZ );
    m_parametersAspirationPump1 = new ComponentProperties( defaultAspirationPump1 );
	InitOnePropertySet( m_parametersAspirationPump1 );
    m_parametersDispensePump1 = new ComponentProperties( defaultDispensePump1 );
	InitOnePropertySet( m_parametersDispensePump1 );
    m_parametersDispensePump2 = new ComponentProperties( defaultDispensePump2 );
	InitOnePropertySet( m_parametersDispensePump2 );
    m_parametersDispensePump3 = new ComponentProperties( defaultDispensePump3 );
	InitOnePropertySet( m_parametersDispensePump3 );
    m_parametersPickingPump1 = new ComponentProperties( defaultPickingPump1 );
	InitOnePropertySet( m_parametersPickingPump1 );
	m_parametersWhiteLightSource = new ComponentProperties( defaultWhiteLightSource );
	InitOnePropertySet( m_parametersWhiteLightSource );
	m_parametersFluorescenceLightFilter = new ComponentProperties( defaultFluorescenceLightFilter );
	InitOnePropertySet( m_parametersFluorescenceLightFilter );
    m_parametersAnnularRingHolder = new ComponentProperties( defaultAnnularRingHolder );
	InitOnePropertySet( m_parametersAnnularRingHolder );
    m_parametersOpticalColumn = new ComponentProperties( defaultOpticalColumn );
	InitOnePropertySet( m_parametersOpticalColumn );
	m_parametersCamera = new ComponentProperties( defaultRetigaR3Camera );
	InitOnePropertySet( m_parametersCamera );
	m_parametersIncubator = new ComponentProperties( defaultIncubator );
	InitOneParamsStruct( m_parametersIncubator );
	m_parametersCxPM = new ComponentProperties( defaultCxPM );
	InitOneParamsStruct( m_parametersCxPM );
	m_parametersBarcodeReader = new ComponentProperties( defaultBarcodeReader );
	InitOneParamsStruct( m_parametersBarcodeReader );
	m_parmetersAreInitialized = true;

	// TODO: Add code for all components to be supported.
	;
}

bool CommandInterpreterThread::ParametersAreReady()
{
	return m_parmetersAreInitialized;
}

/// Calls Init() for all component properties.
/// The order of the calls is TBD. First need to find out the nature of Init()
/// 1. A call to prog_0,0 (ie. "Inialization of ACR700")
/// 2. A call to direct an axis to go low, then high, then home.
/// 3. Ping the MQTT broker too.
/// 4. Something else?
bool CommandInterpreterThread::InitializeHwComponents()
{
	/// Destined for Info thread
	/// Destined for CxD thread
	/*
	XYAxes,
	XXAxis,
	YYAxis,
	ZAxis,
	ZZAxis,
	ZZZAxis,
	*/
	/// Destined for CxPM thread
	/*
	CxPM,
	Incubator,
	*/
	/// Destined for Pump thread
	/*
	AspirationPump1,
	DispensePump1,
	DispensePump2,
	DispensePump3,
	PickingPump1,
	*/
	/// Destined for AnRH thread
	/*
	AnnularRingHolder,
	*/
	/// Destined for Optics thread
	/*
	Camera,
	OpticalColumn,
	WhiteLightSource,
	FluorescenceLightFilter,
	*/
	/// Destination undecided
	/*
	BarcodeReader
	*/
	
	/// Prepare variables that have one-time initialization.
	bool succeeded = true;
	string topicName = "internal";
	json_object * payloadObj = nullptr;
	payloadObj = json_object_new_object();

	FieldWithMetaData property = m_parametersAxesXY->GetFieldByName( X_MAX_ALLOWABLE_POSITION_ERROR );
	int rc = json_object_object_add( payloadObj, EXCESS_ERROR_X, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);
	property = m_parametersAxesXY->GetFieldByName( X_AXIS_ERROR_BAND );
	rc = json_object_object_add( payloadObj, IN_POSITION_BAND_X, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);

	property = m_parametersAxesXY->GetFieldByName( Y_MAX_ALLOWABLE_POSITION_ERROR );
	rc = json_object_object_add( payloadObj, EXCESS_ERROR_Y, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);
	property = m_parametersAxesXY->GetFieldByName( Y_AXIS_ERROR_BAND );
	rc = json_object_object_add( payloadObj, IN_POSITION_BAND_Y, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);

	property = m_parametersAxisXX->GetFieldByName( MAX_ALLOWABLE_POSITION_ERROR );
	rc = json_object_object_add( payloadObj, EXCESS_ERROR_XX, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);
	property = m_parametersAxisXX->GetFieldByName( XX_AXIS_ERROR_BAND );
	rc = json_object_object_add( payloadObj, IN_POSITION_BAND_XX, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);

	property = m_parametersAxisYY->GetFieldByName( MAX_ALLOWABLE_POSITION_ERROR );
	rc = json_object_object_add( payloadObj, EXCESS_ERROR_YY, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);
	property = m_parametersAxisYY->GetFieldByName( YY_AXIS_ERROR_BAND );
	rc = json_object_object_add( payloadObj, IN_POSITION_BAND_YY, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);

	property = m_parametersAnnularRingHolder->GetFieldByName( MAX_ALLOWABLE_POSITION_ERROR );
	rc = json_object_object_add( payloadObj, EXCESS_ERROR_SH, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);
	property = m_parametersAnnularRingHolder->GetFieldByName( AR_AXIS_ERROR_BAND );
	rc = json_object_object_add( payloadObj, IN_POSITION_BAND_SH, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);

	property = m_parametersAxisZ->GetFieldByName( MAX_ALLOWABLE_POSITION_ERROR );
	rc = json_object_object_add( payloadObj, EXCESS_ERROR_Z, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);
	property = m_parametersAxisZ->GetFieldByName( Z_AXIS_ERROR_BAND );
	rc = json_object_object_add( payloadObj, IN_POSITION_BAND_Z, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);

	property = m_parametersAxisZZ->GetFieldByName( MAX_ALLOWABLE_POSITION_ERROR );
	rc = json_object_object_add( payloadObj, EXCESS_ERROR_ZZ, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);
	property = m_parametersAxisZZ->GetFieldByName( ZZ_AXIS_ERROR_BAND );
	rc = json_object_object_add( payloadObj, IN_POSITION_BAND_ZZ, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);

	property = m_parametersAxisZZZ->GetFieldByName( MAX_ALLOWABLE_POSITION_ERROR );
	rc = json_object_object_add( payloadObj, EXCESS_ERROR_ZZZ, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);
	property = m_parametersAxisZZZ->GetFieldByName( ZZZ_AXIS_ERROR_BAND );
	rc = json_object_object_add( payloadObj, IN_POSITION_BAND_ZZZ, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);

	/// Create an internal command message.
	json_object * commandMessage1OutOjb = NULL;
	commandMessage1OutOjb = m_desHelper->ComposeCommand( INIT_HARDWARE_CONTROLLERS,	/// std::string messageType
														0,							/// commandGroupId
														0,							/// commandId
														"",							/// std::string sessionId
														payloadObj );				/// json_object * payload
	string desCallMessage1 = json_object_to_json_string( commandMessage1OutOjb );
	json_object_put( commandMessage1OutOjb ); /// Free memory.

	/// NOTE: Each WorkMsg must be created for each command going to a different thread.
	/// 	At present, I am only sending to one thread, because I assume that the thread
	///		that receives it will initialize all of the hardware controllers.
	/// Create a thread message containing the composed command and meaningless topics.
	std::shared_ptr<WorkMsg> command1 =
		make_shared<WorkMsg>( desCallMessage1,						/// payloadInJson
							  topicName,							/// originTopic
							  topicName,							/// destinationTopic
							  INIT_HARDWARE_CONTROLLERS,			/// messageType
							  "",									/// subMessageType
							  true );								/// theEnd					  
	m_outBoxForCxD->PushMsg(command1);
	LOG_TRACE( m_threadName, "init", "sent INIT_HARDWARE_CONTROLLERS command" );

	/// Create a second internal command message--just for serial comms.
	json_object * commandMessage2OutOjb = NULL;
	commandMessage2OutOjb = m_desHelper->ComposeCommand( INIT_SERIAL_CONTROLLER,	/// std::string messageType
														0,							/// commandGroupId
														0,							/// commandId
														"",							/// std::string sessionId
														nullptr );					/// json_object * payload
	string desCallMessage2 = json_object_to_json_string( commandMessage2OutOjb );
	json_object_put( commandMessage2OutOjb ); /// Free memory.

	/// Create a thread message containing the composed command and meaningless topics.
	std::shared_ptr<WorkMsg> command2 =
		make_shared<WorkMsg>( desCallMessage2,						/// payloadInJson
							  topicName,							/// originTopic
							  topicName,							/// destinationTopic
							  INIT_SERIAL_CONTROLLER,				/// messageType
							  "",									/// subMessageType
							  true );								/// theEnd					  
	m_outBoxForPump->PushMsg(command2);
	LOG_TRACE( m_threadName, "init", "sent INIT_SERIAL_CONTROLLER command" );

	return succeeded;
}


/// First, turns off lights.
/// Then moves Z, ZZ, ZZZ, AspirationPump1, DispensePump1, DispensePump2, DispensePump3, PickingPump1,
/// WhiteLightSource, FluorescenceLightFilter, AnnularRingHolder, Camera, OpticalColumn(focus), Incubator,
/// and BarcodeReader to home positions. (Only affects components that support such moves.)
/// Assume the FluorescenceLightFilter source is off.
///
/// Each component should move to low, then high, then home. Hopefully, home is away from the paths of phase 2 hardare.
bool CommandInterpreterThread::HomeAllSafeHwComponents()
{
	/// Prepare variables that have one-time initialization.
	bool succeeded = true;
	string topicName = "internal";

	/// Create an internal command message.
	json_object * payloadObj = DesHelper::ComposeHomeAllSafeHwPayload( m_threadName,
																	   m_parametersAnnularRingHolder,
																	   m_parametersAxisZ,
																	   m_parametersAxisZZ,
																	   m_parametersAxisZZZ,
																	   m_parametersAspirationPump1,
																	   m_parametersDispensePump1,
																	   m_parametersDispensePump2,
																	   m_parametersDispensePump3,
																	   m_parametersPickingPump1 );
	/// Add speeds and limits for Z, ZZ, ZZZ, so they can move to safe position after being homed.
	succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );
	succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );
	succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zzz", m_parametersAxisZZZ );

	/// Three moves involve sending Z axes to safe heights
	long z_safeHeight_nm = m_parametersAxisZ->GetFieldByName( Z_SAFE_HEIGHT ).GetValueAsLong();
	int rc = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, json_object_new_int64( z_safeHeight_nm ) );
	succeeded &= (rc == 0);

	long zz_safeHeight_nm = m_parametersAxisZZ->GetFieldByName( ZZ_SAFE_HEIGHT ).GetValueAsLong();
	rc = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, json_object_new_int64( zz_safeHeight_nm ) );
	succeeded &= (rc == 0);

	long zzz_safeHeight_nm = m_parametersAxisZZZ->GetFieldByName( ZZZ_SAFE_HEIGHT ).GetValueAsLong();
	rc = json_object_object_add( payloadObj, ZZZ_SAFE_HEIGHT, json_object_new_int64( zzz_safeHeight_nm ) );
	succeeded &= (rc == 0);

	json_object * commandMessageOutOjb = nullptr;
	commandMessageOutOjb = m_desHelper->ComposeCommand( HOME_SAFE_HARDWARE,		/// std::string messageType
														0,							/// commandGroupId
														0,							/// commandId
														"",							/// std::string sessionId
														payloadObj );				/// json_object * payload
	string desCallMessage = json_object_to_json_string( commandMessageOutOjb );
	json_object_put( commandMessageOutOjb ); /// Free memory.

	/// NOTE: Each WorkMsg must be created for each command going to a different thread.
	/// 	At present, I am only sending to one thread, because I assume that the thread
	///		that receives it will initialize all of the hardware controllers.
	/// Create a thread message containing the composed command and meaningless topics.
	std::shared_ptr<WorkMsg> command =
		make_shared<WorkMsg>( desCallMessage,						/// payloadInJson
							  topicName,							/// originTopic
							  topicName,							/// destinationTopic
							  HOME_SAFE_HARDWARE,					/// messageType
							  "",									/// subMessageType
							  true );								/// theEnd					  
	m_outBoxForOptics->PushMsg(command);		/// for Camera, OpticalColumn(focus), WhiteLightSource, FluorescenceLightFilter
	m_outBoxForCxD->PushMsg(command);			/// for Z, ZZ, ZZZ axes
	m_outBoxForCxPM->PushMsg(command);			/// for incubator
	m_outBoxForPump->PushMsg(command);			/// for all pumps
	m_outBoxForRingHolder->PushMsg(command);
	return succeeded;
}

/// Moves XY, XX, YY, OpticalColumn(turret), and CxPM to home positions.
///
/// Each component should move to low, then high, then home.
bool CommandInterpreterThread::HomeAllUnsafeHwComponents()
{
	/// Prepare variables that have one-time initialization.
	bool succeeded = true;
	string topicName = "internal";

	/// Create an internal command message.
	json_object * payloadObj = DesHelper::ComposeHomeAllUnsafeHwPayload( m_threadName,
																		 m_parametersAxesXY,
																		 m_parametersAxisYY,
																		 m_parametersAxisXX );
	json_object * commandMessageOutOjb = nullptr;
	commandMessageOutOjb = m_desHelper->ComposeCommand( HOME_UNSAFE_HARDWARE,		/// std::string messageType
														0,							/// commandGroupId
														0,							/// commandId
														"",							/// std::string sessionId
														payloadObj );				/// json_object * payload
	string desCallMessage = json_object_to_json_string( commandMessageOutOjb );
	json_object_put( commandMessageOutOjb ); /// Free memory.

	/// NOTE: Each WorkMsg must be created for each command going to a different thread.
	/// 	At present, I am only sending to one thread, because I assume that the thread
	///		that receives it will initialize all of the hardware controllers.
	/// Create a thread message containing the composed command and meaningless topics.
	std::shared_ptr<WorkMsg> command =
		make_shared<WorkMsg>( desCallMessage,						/// payloadInJson
							  topicName,							/// originTopic
							  topicName,							/// destinationTopic
							  HOME_UNSAFE_HARDWARE,					/// messageType
							  "",									/// subMessageType
							  true );								/// theEnd					  
	m_outBoxForOptics->PushMsg(command);	/// for optical column(turret)
	m_outBoxForCxD->PushMsg(command);		/// for XX, XY, and YY axes.
	m_outBoxForCxPM->PushMsg(command);
	m_outBoxForRingHolder->PushMsg(command);
	m_outBoxForPump->PushMsg(command);
	
	return succeeded;
}

///----------------------------------------------------------------------
/// HomePickingPump1
///
/// Initializes the picking pump. The picking pump needs to perform an
/// EjectTip before initialization, so this function moves the tip to
/// safe place to eject any tip that may be present.
/// 1. MoveAllTipsToSafeHeight()
/// 2. MoveToYYNM(ComponentProperties.YYAxis.yy_key_to_tip_path + ComponentProperties.ZAxis.z_y_offset_from_xx_axis)
/// 3. MoveToXXNM(ComponentProperties.XXAxis.xx_z_tip_to_key_entry_position)
/// 4. MoveToZNM(ComponentProperties.ZAxis.z_to_top_of_seating_key_height)
/// 5. ADP.InitializeDevice()
///		A. EjectTipAtInitialization
/// 	B. InitializeDevice
/// 6. MoveAllTipsToSafeHeight()
/// 7. MoveToYYNM(0) // YY home
/// 8. MoveToXXNM(0) // XX home
///----------------------------------------------------------------------
bool CommandInterpreterThread::HomePickingPump1()
{
	bool succeeded = true;
	json_object * payloadObj = nullptr;
	payloadObj = json_object_new_object();

	/// Moves will be made on the Z, ZZ, ZZZ, XX, and YY axes as part of initializing the picking pump. They require speeds and accelerations.
	succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );
	succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );
	succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zzz", m_parametersAxisZZZ );
	succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "xx", m_parametersAxisXX );
	succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "yy", m_parametersAxisYY );
	succeeded &= DesHelper::AddXYSpeeds( m_threadName, payloadObj, m_parametersAxesXY );

	/// The Moves require 4 values to calculate destinations.
	long yy_key_to_tip_path = m_parametersAxisYY->GetFieldByName( YY_KEY_TO_TIP_PATH ).GetValueAsLong();
	int rc = json_object_object_add( payloadObj, YY_KEY_TO_TIP_PATH, json_object_new_int64( yy_key_to_tip_path ) );
	succeeded &= (rc == 0);

	unsigned long z_y_offset_from_xx_axis = m_parametersAxisZ->GetFieldByName( Z_Y_OFFSET_FROM_XX_AXIS ).GetValueAsULong();
	rc = json_object_object_add( payloadObj, Z_Y_OFFSET_FROM_XX_AXIS, json_object_new_uint64( z_y_offset_from_xx_axis ) );
	succeeded &= (rc == 0);

	long xx_z_tip_to_key_entry_position = m_parametersAxisXX->GetFieldByName( XX_Z_TIP_TO_KEY_ENTRY_POSITION ).GetValueAsLong();
	rc = json_object_object_add( payloadObj, XX_Z_TIP_TO_KEY_ENTRY_POSITION, json_object_new_int64( xx_z_tip_to_key_entry_position ) );
	succeeded &= (rc == 0);

	long z_to_top_of_seating_key_height = m_parametersAxisZ->GetFieldByName( Z_TO_TOP_OF_SEATING_KEY_HEIGHT ).GetValueAsLong();
	rc = json_object_object_add( payloadObj, Z_TO_TOP_OF_SEATING_KEY_HEIGHT, json_object_new_int64( z_to_top_of_seating_key_height ) );
	succeeded &= (rc == 0);

	/// Three moves involve sending Z axes to safe heights
	long z_safeHeight_nm = m_parametersAxisZ->GetFieldByName( Z_SAFE_HEIGHT ).GetValueAsLong();
	rc = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, json_object_new_int64( z_safeHeight_nm ) );
	succeeded &= (rc == 0);

	long zz_safeHeight_nm = m_parametersAxisZZ->GetFieldByName( ZZ_SAFE_HEIGHT ).GetValueAsLong();
	rc = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, json_object_new_int64( zz_safeHeight_nm ) );
	succeeded &= (rc == 0);

	long zzz_safeHeight_nm = m_parametersAxisZZZ->GetFieldByName( ZZZ_SAFE_HEIGHT ).GetValueAsLong();
	rc = json_object_object_add( payloadObj, ZZZ_SAFE_HEIGHT, json_object_new_int64( zzz_safeHeight_nm ) );
	succeeded &= (rc == 0);

	/// Any command to the picking pump needs the vendor and model. This is to support multiple types of pump in future.
	std::string name = PUMP_VENDOR;
	std::string pumpVendor = m_parametersPickingPump1->GetFieldByName( name ).setting;
	rc = json_object_object_add( payloadObj, PUMP_VENDOR, json_object_new_string( pumpVendor.c_str() ) );
	succeeded &= (rc == 0);

	name = PUMP_MODEL;
	std::string pumpModel = m_parametersPickingPump1->GetFieldByName( name ).setting;
	rc = json_object_object_add( payloadObj, PUMP_MODEL, json_object_new_string( pumpModel.c_str() ) );
	succeeded &= (rc == 0);

	/// Prepare variables that have one-time initialization.
	string topicName = "internal";

	/// Create an internal command message.
	json_object * commandMessageOutOjb = NULL;
	commandMessageOutOjb = m_desHelper->ComposeCommand( INITIALIZE_PICKING_PUMP1,	/// std::string messageType
														0,							/// commandGroupId
														0,							/// commandId
														"",							/// std::string sessionId
														payloadObj );				/// json_object * payload
	string desCallMessage = json_object_to_json_string( commandMessageOutOjb );
	json_object_put( commandMessageOutOjb ); /// Free memory.

	/// NOTE: Each WorkMsg must be created for each command going to a different thread.
	/// 	At present, I am only sending to one thread, because I assume that the thread
	///		that receives it will initialize all of the hardware controllers.
	/// Create a thread message containing the composed command and meaningless topics.
	std::shared_ptr<WorkMsg> command =
		make_shared<WorkMsg>( desCallMessage,			/// payloadInJson
							  topicName,				/// originTopic
							  topicName,				/// destinationTopic
							  INITIALIZE_PICKING_PUMP1,	/// messageType
							  "",						/// subMessageType
							  true );					/// theEnd					  
	m_outBoxForCxD->PushMsg(command);
	
	return succeeded;

}

/// Moves FluorescenceLightFilter(filter position), AnnularRingHolder, and OpticalColumn(turret) to their active positions.
/// Then, sets white and fluorescence lights to their active states.
bool CommandInterpreterThread::RestoreAllHwComponentsToLastActiveStates()
{
	bool succeeded = true;
	json_object * payloadObj = nullptr;
	payloadObj = json_object_new_object();

	FieldWithMetaData property = m_parametersAxesXY->GetFieldByName( Y_SET_SPEED );
	int rc = json_object_object_add( payloadObj, Y_SET_SPEED, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);
	property = m_parametersAxesXY->GetFieldByName( Y_ACCELERATION );
	rc = json_object_object_add( payloadObj, Y_ACCELERATION, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);
	property = m_parametersAxesXY->GetFieldByName( Y_DECELERATION );
	rc = json_object_object_add( payloadObj, Y_DECELERATION, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);
	property = m_parametersAxesXY->GetFieldByName( Y_JOGGING_SPEED );
	rc = json_object_object_add( payloadObj, Y_JOGGING_SPEED, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);
	property = m_parametersAxesXY->GetFieldByName( Y_JOGGING_ACCELERATION );
	rc = json_object_object_add( payloadObj, Y_JOGGING_ACCELERATION, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);
	property = m_parametersAxesXY->GetFieldByName( Y_JOGGING_DECELERATION );
	rc = json_object_object_add( payloadObj, Y_JOGGING_DECELERATION, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);
	
	property = m_parametersWhiteLightSource->GetFieldByName( ON_STATUS );
	json_bool onStatus = (property.GetValueAsBool()) ? 1 : 0;
	rc = json_object_object_add( payloadObj, WHITE_LIGHT_ON_STATE, json_object_new_boolean( onStatus ) );
	succeeded &= (rc == 0);
	
	property = m_parametersFluorescenceLightFilter->GetFieldByName( ON_STATUS );
	onStatus = (property.GetValueAsBool()) ? 1 : 0;
	rc = json_object_object_add( payloadObj, FLUORESCENCE_LIGHT_ON_STATE, json_object_new_boolean( onStatus ) );
	succeeded &= (rc == 0);
	property = m_parametersFluorescenceLightFilter->GetFieldByName( ACTIVE_FLUORESCENCE_FILTER_POSITION );
	rc = json_object_object_add( payloadObj, ACTIVE_FLUORESCENCE_FILTER_POSITION, json_object_new_int64( property.GetValueAsLong() ) );
	succeeded &= (rc == 0);
	
	property = m_parametersAnnularRingHolder->GetFieldByName( CURRENT_RING_POSITION );
	rc = json_object_object_add( payloadObj, CURRENT_RING_POSITION, json_object_new_int64( property.GetValueAsLong() ) );
	succeeded &= (rc == 0);

	property = m_parametersOpticalColumn->GetFieldByName( ACTIVE_TURRET_POSITION );
	rc = json_object_object_add( payloadObj, ACTIVE_TURRET_POSITION, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);

	property = m_parametersCamera->GetFieldByName( GAIN );
	rc = json_object_object_add( payloadObj, GAIN, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);
	property = m_parametersCamera->GetFieldByName( EXPOSURE );
	rc = json_object_object_add( payloadObj, EXPOSURE, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);
	property = m_parametersCamera->GetFieldByName( BIN_FACTOR );
	rc = json_object_object_add( payloadObj, BIN_FACTOR, json_object_new_uint64( property.GetValueAsULong() ) );
	succeeded &= (rc == 0);

	/// Prepare variables that have one-time initialization.
	string topicName = "internal";

	/// Create an internal command message.
	json_object * commandMessageOutOjb = NULL;
	commandMessageOutOjb = m_desHelper->ComposeCommand( MOVE_HARDWARE_TO_ACTIVE_STATES,	/// std::string messageType
														0,								/// commandGroupId
														0,								/// commandId
														"",								/// std::string sessionId
														payloadObj );					/// json_object * payload
	string desCallMessage = json_object_to_json_string( commandMessageOutOjb );
	json_object_put( commandMessageOutOjb ); /// Free memory.

	/// NOTE: Each WorkMsg must be created for each command going to a different thread.
	/// 	At present, I am only sending to one thread, because I assume that the thread
	///		that receives it will initialize all of the hardware controllers.
	/// Create a thread message containing the composed command and meaningless topics.
	std::shared_ptr<WorkMsg> command =
		make_shared<WorkMsg>( desCallMessage,						/// payloadInJson
							  topicName,							/// originTopic
							  topicName,							/// destinationTopic
							  MOVE_HARDWARE_TO_ACTIVE_STATES,		/// messageType
							  "",									/// subMessageType
							  true );								/// theEnd					  
	m_outBoxForOptics->PushMsg(command);		/// for FluorescenceLightFilter(filter position), optical column(turret)
	m_outBoxForRingHolder->PushMsg(command);
	
	return succeeded;
}

/// Calls the prog0 ClrKills command on all Parker controllers.
bool CommandInterpreterThread::ClearKills()
{
	bool succeeded = true;
	json_object * payloadObj = nullptr;
	payloadObj = json_object_new_object();

	/// Prepare variables that have one-time initialization.
	string topicName = "internal";

	/// Create an internal command message.
	json_object * commandMessageOutOjb = NULL;
	commandMessageOutOjb = m_desHelper->ComposeCommand( CLEAR_KILLS,			/// std::string messageType
														0,						/// commandGroupId
														0,						/// commandId
														"",						/// std::string sessionId
														payloadObj );			/// json_object * payload
	string desCallMessage = json_object_to_json_string( commandMessageOutOjb );
	json_object_put( commandMessageOutOjb ); /// Free memory.

	/// NOTE: Each WorkMsg must be created for each command going to a different thread.
	/// 	At present, I am only sending to one thread, because I assume that the thread
	///		that receives it will initialize all of the hardware controllers.
	/// Create a thread message containing the composed command and meaningless topics.
	std::shared_ptr<WorkMsg> command =
		make_shared<WorkMsg>( desCallMessage,	/// payloadInJson
							  topicName,		/// originTopic
							  topicName,		/// destinationTopic
							  CLEAR_KILLS,		/// messageType
							  "",				/// subMessageType
							  true );			/// theEnd					  
	m_outBoxForCxD->PushMsg(command);		/// for FluorescenceLightFilter(filter position), optical column(turret)
	m_outBoxForPump->PushMsg(command);
	
	return succeeded;
}


bool CommandInterpreterThread::SendAlertMessage(
		int commandGroupId,
		int commandId,
		std::string sessionId,
		ComponentType componentType,
		int statusCode,
		bool thisIsCausingHold )
{
	bool succeeded = true;
	string fakeOriginalTopic = TOPIC_ES;
	fakeOriginalTopic += CHANNEL_ALERT;
	json_object * alertObj = m_desHelper->ComposeAlert( TEXT_ALERT,
														commandGroupId,
														commandId,
														sessionId,
														componentType,
														statusCode,
														true );
	bool sentOK = m_desHelper->SendAlert( alertObj, m_statusRouteBox, fakeOriginalTopic );
	if (sentOK)
	{
		LOG_TRACE( m_threadName, "unk", "Sent alert message" );
	}
	else
	{
		LOG_ERROR( m_threadName, "unk", "Failed to send alert message" );
		succeeded = false;
	}
	
	return succeeded;
}


//----------------------------------------------------------------------
// Process
//----------------------------------------------------------------------
void CommandInterpreterThread::Process()
{
    std::string messageType = "";
    bool forwardingIsComplete = false;
	bool succeeded = false;
    
    InitializeWorkingParameters();

	while (m_continueRunning && m_inBox && m_outBoxForInfo)
	{
		/// Wait for a bit of work to do.
		std::shared_ptr<WorkMsg> workMsg = m_inBox->WaitAndPopMsg();
		
		/// See ExitThread().
		if (workMsg->msgType == "end") break;

		forwardingIsComplete = false;
		/// The payload is JASON. convert it to an object so we can analyze it.
		bool messageParsed = false;
		json_object *jWorkOrderObj = m_desHelper->ParseJsonDesMessage( workMsg->payload.c_str(), &messageParsed );
		if (!messageParsed)
		{
			LOG_ERROR( m_threadName, "mqtt", "failed to parse message" );
		}
        
        /// Plate-mover commands need to have coordinates translated here.
        ;
        
        /// Tip-finder commands need to have coordineates translated here.
        ;
        
        /// Compound commands need to be decomposed into series of
        /// Simple commands here. Some of those may also need to have
        /// coordinates translated.
        if (messageParsed)
        {
			messageType = workMsg->msgType;
			if (false)
			{
				stringstream msg;
				msg << "Received: " << messageType;
				LOG_TRACE( m_threadName, "mqtt", msg.str().c_str() );
			}

			if ((!forwardingIsComplete) && (messageType == SYNCHRONIZE_TIME))
			{
				m_outBoxForInfo->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_CURRENT_SYSTEM_CONFIGURATION))
			{
				succeeded = GetCurrentSystemConfiguration( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_FIRMWARE_VERSION_NUMBER))
			{
				succeeded = GetFirmwareVersionNumber( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_SYSTEM_SERIAL_NUMBER))
			{
				succeeded = GetSystemSerialNumber( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_SYSTEM_SERIAL_NUMBER))
			{
				succeeded = SetSystemSerialNumber( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}


			/// --------------- Component Properties ---------------
			/// Handle the following commands here.
			/// These commands don't involve communications with hardware
			/// Also, these commands affect data stores to which this thread
			/// has easy access. If another thread was to start mucking
			/// with these data stores, we'd have to start locking them up.
			if ((!forwardingIsComplete) && (messageType == UPLOAD_AND_APPLY_CPD))
			{
				succeeded = StoreCpd( jWorkOrderObj, workMsg, true );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == UPLOAD_CPD))
			{
				succeeded = StoreCpd( jWorkOrderObj, workMsg, false );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_CPD_LIST_FROM_TYPE))
			{
				succeeded = GetCpdListFromType( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_CPD_CONTENTS))
			{
				succeeded = GetCpdContents( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == DELETE_CPD))
			{
				forwardingIsComplete = true;
				succeeded = DeleteCpd( jWorkOrderObj, workMsg );
			}
			if ((!forwardingIsComplete) && (messageType == GET_COMPONENT_PROPERTIES))
			{
				std::this_thread::sleep_for(250ms); // Fake work
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_COMPONENT_PROPERTIES))
			{
				std::this_thread::sleep_for(250ms); // Fake work
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == APPLY_CPD_TO_COMPONENT))
			{
				succeeded = ApplyCpdToComponent( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}

			/// --------------- CxD ---------------
			if ((!forwardingIsComplete) && (messageType == GET_CXD_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersCxD );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_CXD_PROPERTIES))
			{
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersCxD );
				forwardingIsComplete = true;
			}
			
			/// --------------- XY ---------------
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_XY_NM))
			{
				succeeded = MeteOut_moveToXYNm( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_XY_RELATIVE_NM))
			{
				/// It is safe to use MeteOut_moveToXYNm here, because it just adds
				/// velocity, accel, decel, and stp to the payload. The rest of the
				/// payload is unaffected.
				succeeded = MeteOut_moveToXYNm( jWorkOrderObj, workMsg ); // KLUDGE! See note above.
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_CURRENT_XY_NM))
			{
				m_outBoxForCxD->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_X_LOW_LIMIT))
			{
				m_outBoxForCxD->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_X_HIGH_LIMIT))
			{
				m_outBoxForCxD->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_X_TO_TIP_SKEW_TRIGGER))
			{
				succeeded = MeteOut_MoveXToTipSkewTrigger( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_Y_LOW_LIMIT))
			{
				m_outBoxForCxD->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_Y_HIGH_LIMIT))
			{
				m_outBoxForCxD->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_Y_TO_TIP_SKEW_TRIGGER))
			{
				succeeded = MeteOut_MoveYToTipSkewTrigger( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}

			if ((!forwardingIsComplete) && (messageType == MOVE_XY_TO_OPTICAL_EYE_NM_A1_NOTATION))
			{
				// TODO: Make this approach more common. Make forwardingIsComplete mean only that
				// the messageType found its matching function call.
				succeeded = MeteOut_MoveXYTo_withPlatePositionDetails( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_XY_TO_OPTICAL_EYE_NM_RC_NOTATION))
			{
				succeeded = MeteOut_MoveXYTo_withPlatePositionDetails( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_XY_TO_TIP_WORKING_LOCATION_NM_A1_NOTATION))
			{
				succeeded = MeteOut_MoveXYTo_withPlatePositionDetails( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_XY_TO_TIP_WORKING_POSITION_NM_RC_NOTATION))
			{
				succeeded = MeteOut_MoveXYTo_withPlatePositionDetails( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SWIRL_XY_STAGE))
			{
				succeeded = MeteOut_SwirlXYStage( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_XY_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersAxesXY );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_XY_PROPERTIES))
			{
				ComponentProperties originalCompProperties;
				originalCompProperties.fieldVector.push_back( m_parametersAxesXY->GetFieldByName( Y_SET_SPEED ) );
				originalCompProperties.fieldVector.push_back( m_parametersAxesXY->GetFieldByName( Y_ACCELERATION ) );
				originalCompProperties.fieldVector.push_back( m_parametersAxesXY->GetFieldByName( Y_DECELERATION ) );
				originalCompProperties.fieldVector.push_back( m_parametersAxesXY->GetFieldByName( Y_JOGGING_SPEED ) );
				originalCompProperties.fieldVector.push_back( m_parametersAxesXY->GetFieldByName( Y_JOGGING_ACCELERATION ) );
				originalCompProperties.fieldVector.push_back( m_parametersAxesXY->GetFieldByName( Y_JOGGING_DECELERATION ) );
				
				forwardingIsComplete = true;
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersAxesXY );
				
				SetHardwareStateIfNeeded( ComponentType::XYAxes, &originalCompProperties, m_parametersAxesXY, Y_SET_SPEED, m_outBoxForCxD );
				SetHardwareStateIfNeeded( ComponentType::XYAxes, &originalCompProperties, m_parametersAxesXY, Y_ACCELERATION, m_outBoxForCxD );
				SetHardwareStateIfNeeded( ComponentType::XYAxes, &originalCompProperties, m_parametersAxesXY, Y_DECELERATION, m_outBoxForCxD );
				SetHardwareStateIfNeeded( ComponentType::XYAxes, &originalCompProperties, m_parametersAxesXY, Y_JOGGING_SPEED, m_outBoxForCxD );
				SetHardwareStateIfNeeded( ComponentType::XYAxes, &originalCompProperties, m_parametersAxesXY, Y_JOGGING_ACCELERATION, m_outBoxForCxD );
				SetHardwareStateIfNeeded( ComponentType::XYAxes, &originalCompProperties, m_parametersAxesXY, Y_JOGGING_DECELERATION, m_outBoxForCxD );
			}

			/// --------------- XX ---------------
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_XX_NM))
			{
				MeteOut_MoveSingleAxis( jWorkOrderObj, workMsg, m_outBoxForCxD, "xx", m_parametersAxisXX );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_XX_RELATIVE_NM))
			{
				MeteOut_MoveSingleAxis( jWorkOrderObj, workMsg, m_outBoxForCxD, "xx", m_parametersAxisXX );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_CURRENT_XX_NM))
			{
				m_outBoxForCxD->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_XX_LOW_LIMIT))
			{
				MeteOut_MoveSingleAxis( jWorkOrderObj, workMsg, m_outBoxForCxD, "xx", m_parametersAxisXX );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_XX_HIGH_LIMIT))
			{
				MeteOut_MoveSingleAxis( jWorkOrderObj, workMsg, m_outBoxForCxD, "xx", m_parametersAxisXX );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_XX_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersAxisXX );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_XX_PROPERTIES))
			{
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersAxisXX );
				forwardingIsComplete = true;
			}

			/// --------------- YY ---------------
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_YY_NM))
			{
				MeteOut_MoveSingleAxis( jWorkOrderObj, workMsg, m_outBoxForCxD, "yy", m_parametersAxisYY );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_YY_RELATIVE_NM))
			{
				MeteOut_MoveSingleAxis( jWorkOrderObj, workMsg, m_outBoxForCxD, "yy", m_parametersAxisYY );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_CURRENT_YY_NM))
			{
				m_outBoxForCxD->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_YY_LOW_LIMIT))
			{
				MeteOut_MoveSingleAxis( jWorkOrderObj, workMsg, m_outBoxForCxD, "yy", m_parametersAxisYY );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_YY_HIGH_LIMIT))
			{
				MeteOut_MoveSingleAxis( jWorkOrderObj, workMsg, m_outBoxForCxD, "yy", m_parametersAxisYY );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_YY_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersAxisYY );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_YY_PROPERTIES))
			{
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersAxisYY );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_YY_TO_TIP_RACK_GRAB_LOCATION))
			{
				succeeded = MeteOut_MoveYYToTipRackGrabLocation( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}

			/// --------------- Z ---------------
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_Z_NM))
			{
				MeteOut_MoveSingleAxis( jWorkOrderObj, workMsg, m_outBoxForCxD, "z", m_parametersAxisZ );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_Z_RELATIVE_NM))
			{
				MeteOut_MoveSingleAxis( jWorkOrderObj, workMsg, m_outBoxForCxD, "z", m_parametersAxisZ );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == RETRACT_TO_Z_NM))
			{
				MeteOut_RetractSingleAxis( jWorkOrderObj, workMsg, m_outBoxForCxD, "z", m_parametersAxisZ );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_CURRENT_Z_NM))
			{
				m_outBoxForCxD->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_Z_LOW_LIMIT))
			{
				succeeded = MeteOut_MoveToZLowLimit( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_Z_HIGH_LIMIT))
			{
				succeeded = MeteOut_MoveToZHighLimit( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_Z_TO_SAFE_HEIGHT))
			{
				succeeded = MeteOut_MoveZToSafeHeight( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_Z_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersAxisZ );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_Z_PROPERTIES))
			{
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersAxisZ );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_Z_AXIS_FORCE_NEWTON))
			{
				succeeded = MeteOut_MoveToZAxisForceNewton( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_Z_AXIS_SEATING_FORCE_NEWTON))
			{
				succeeded = MeteOut_MoveToZAxisSeatingForceNewton( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_Z_AXIS_WELL_BOTTOM_FORCE_NEWTON))
			{
				succeeded = MeteOut_MoveToZAxisWellBottomForceNewton( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			/// --------------- ZZ ---------------
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_ZZ_NM))
			{
				MeteOut_MoveSingleAxis( jWorkOrderObj, workMsg, m_outBoxForCxD, "zz", m_parametersAxisZZ );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_ZZ_RELATIVE_NM))
			{
				MeteOut_MoveSingleAxis( jWorkOrderObj, workMsg, m_outBoxForCxD, "zz", m_parametersAxisZZ );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == RETRACT_TO_ZZ_NM))
			{
				MeteOut_RetractSingleAxis( jWorkOrderObj, workMsg, m_outBoxForCxD, "zz", m_parametersAxisZZ );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_CURRENT_ZZ_NM))
			{
				m_outBoxForCxD->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_ZZ_LOW_LIMIT))
			{
				succeeded = MeteOut_MoveToZZLowLimit( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_ZZ_HIGH_LIMIT))
			{
				succeeded = MeteOut_MoveToZZHighLimit( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_ZZ_TO_SAFE_HEIGHT))
			{
				succeeded = MeteOut_MoveZZToSafeHeight( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_ZZ_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersAxisZZ );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_ZZ_PROPERTIES))
			{
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersAxisZZ );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_ZZ_AXIS_FORCE_NEWTON))
			{
				succeeded = MeteOut_MoveToZZAxisForceNewton( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_ZZ_AXIS_SEATING_FORCE_NEWTON))
			{
				succeeded = MeteOut_MoveToZZAxisSeatingForceNewton( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_ZZ_AXIS_WELL_BOTTOM_FORCE_NEWTON))
			{
				succeeded = MeteOut_MoveToZZAxisWellBottomForceNewton( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			/// --------------- ZZZ ---------------
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_ZZZ_NM))
			{
				MeteOut_MoveSingleAxis( jWorkOrderObj, workMsg, m_outBoxForCxD, "zzz", m_parametersAxisZZZ );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_ZZZ_RELATIVE_NM))
			{
				MeteOut_MoveSingleAxis( jWorkOrderObj, workMsg, m_outBoxForCxD, "zzz", m_parametersAxisZZZ );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_CURRENT_ZZZ_NM))
			{
				m_outBoxForCxD->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_ZZZ_LOW_LIMIT))
			{
				m_outBoxForCxD->PushMsg(workMsg); // TODO: Implement a MeteOut_MoveToZZZLowLimit
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_ZZZ_HIGH_LIMIT))
			{
				m_outBoxForCxD->PushMsg(workMsg); // TODO: Implement a MeteOut_MoveToZZZHighLimit
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_ZZZ_TO_SAFE_HEIGHT))
			{
				succeeded = MeteOut_MoveZZZToSafeHeight( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_ZZZ_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersAxisZZZ );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_ZZZ_PROPERTIES))
			{
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersAxisZZZ );
				forwardingIsComplete = true;
			}
			/// --------------- Aspiration Pump ---------------
			if ((!forwardingIsComplete) && (messageType == ASPIRATE_ZZ_FLOW_RATE_TIME))
			{
				succeeded = MeteOut_Aspirate( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == ASPIRATE_ZZ_FLOW_RATE_VOLUME))
			{
				succeeded = MeteOut_Aspirate( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == ASPIRATE_ZZ_VOLUME_TIME))
			{
				succeeded = MeteOut_Aspirate( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == ASPIRATION_PUMP_START_STOP))
			{
				/// Forward to the thread that manages the plate inventory file.
				succeeded = MeteOut_AspirateStartStop( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == IS_ASPIRATION_PUMP_RUNNING))
			{
				/// Forward to the thread that manages the plate inventory file.
				m_outBoxForPump->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_ASPIRATION_PUMP_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersAspirationPump1 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_ASPIRATION_PUMP_PROPERTIES))
			{
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersAspirationPump1 );
				forwardingIsComplete = true;
			}

			/// --------------- Dispense Pump 1 ---------------
			if ((!forwardingIsComplete) && (messageType == DISPENSE_1_FLOW_RATE_TIME))
			{
				succeeded = MeteOut_Dispense( jWorkOrderObj, workMsg, 1 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == DISPENSE_1_FLOW_RATE_VOLUME))
			{
				succeeded = MeteOut_Dispense( jWorkOrderObj, workMsg, 1 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == DISPENSE_1_VOLUME_TIME))
			{
				succeeded = MeteOut_Dispense( jWorkOrderObj, workMsg, 1 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == DISPENSE_PUMP_1_START_STOP))
			{
				/// Forward to the thread that manages the plate inventory file.
				succeeded = MeteOut_DispenseStartStop( jWorkOrderObj, workMsg, 1 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == IS_DISPENSE_PUMP_1_RUNNING))
			{
				/// Forward to the thread that manages the plate inventory file.
				m_outBoxForPump->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_DISPENSE_PUMP_1_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersDispensePump1 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_DISPENSE_PUMP_1_PROPERTIES))
			{
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersDispensePump1 );
				forwardingIsComplete = true;
			}

			/// --------------- Dispense Pump 2 ---------------
			if ((!forwardingIsComplete) && (messageType == DISPENSE_2_FLOW_RATE_TIME))
			{
				succeeded = MeteOut_Dispense( jWorkOrderObj, workMsg, 2 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == DISPENSE_2_FLOW_RATE_VOLUME))
			{
				succeeded = MeteOut_Dispense( jWorkOrderObj, workMsg, 2 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == DISPENSE_2_VOLUME_TIME))
			{
				succeeded = MeteOut_Dispense( jWorkOrderObj, workMsg, 2 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == DISPENSE_PUMP_2_START_STOP))
			{
				/// Forward to the thread that manages the plate inventory file.
				succeeded = MeteOut_DispenseStartStop( jWorkOrderObj, workMsg, 2 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == IS_DISPENSE_PUMP_2_RUNNING))
			{
				/// Forward to the thread that manages the plate inventory file.
				m_outBoxForPump->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_DISPENSE_PUMP_2_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersDispensePump2 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_DISPENSE_PUMP_2_PROPERTIES))
			{
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersDispensePump2 );
				forwardingIsComplete = true;
			}

			/// --------------- Dispense Pump 3 ---------------
			if ((!forwardingIsComplete) && (messageType == DISPENSE_3_FLOW_RATE_TIME))
			{
				succeeded = MeteOut_Dispense( jWorkOrderObj, workMsg, 3 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == DISPENSE_3_FLOW_RATE_VOLUME))
			{
				succeeded = MeteOut_Dispense( jWorkOrderObj, workMsg, 3 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == DISPENSE_3_VOLUME_TIME))
			{
				succeeded = MeteOut_Dispense( jWorkOrderObj, workMsg, 3 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == DISPENSE_PUMP_3_START_STOP))
			{
				/// Forward to the thread that manages the plate inventory file.
				succeeded = MeteOut_DispenseStartStop( jWorkOrderObj, workMsg, 3 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == IS_DISPENSE_PUMP_3_RUNNING))
			{
				/// Forward to the thread that manages the plate inventory file.
				m_outBoxForPump->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_DISPENSE_PUMP_3_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersDispensePump3 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_DISPENSE_PUMP_3_PROPERTIES))
			{
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersDispensePump3 );
				forwardingIsComplete = true;
			}

			/// --------------- Picking Pump ---------------
			if ((!forwardingIsComplete) && (messageType == PICKING_PUMP_FLOW_RATE_TIME))
			{
				/// Use MeteOut_Picking() instead of PushMsg() so the pump's vendor and model types are packed in the payload.
				succeeded = MeteOut_Picking( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == PICKING_PUMP_FLOW_RATE_VOLUME))
			{
				succeeded = MeteOut_Picking( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == PICKING_PUMP_VOLUME_TIME))
			{
				succeeded = MeteOut_Picking( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_PICKING_PUMP_CURRENT_VOLUME_NL))
			{
				/// Use MeteOut_Picking() instead of PushMsg() so the pump's vendor and model types are packed in the payload.
				succeeded = MeteOut_Picking( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_PICKING_PUMP_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersPickingPump1 );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_PICKING_PUMP_PROPERTIES))
			{
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersPickingPump1 );
				forwardingIsComplete = true;
			}

			/// --------------- White Light Source ---------------
			if ((!forwardingIsComplete) && (messageType == GET_WHITE_LIGHT_SOURCE_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersWhiteLightSource );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_WHITE_LIGHT_SOURCE_PROPERTIES))
			{
				ComponentProperties originalCompProperties;
				originalCompProperties.fieldVector.push_back( m_parametersWhiteLightSource->GetFieldByName( ON_STATUS ) );

				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersWhiteLightSource );
				forwardingIsComplete = true;

				SetHardwareStateIfNeeded( ComponentType::WhiteLightSource, &originalCompProperties, m_parametersWhiteLightSource, ON_STATUS, m_outBoxForOptics );
			}

			/// --------------- Fluorescence Light Source ---------------
			if ((!forwardingIsComplete) && (messageType == GET_FLUORESCENCE_LIGHT_FILTER_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersFluorescenceLightFilter );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_FLUORESCENCE_LIGHT_FILTER_PROPERTIES))
			{
				ComponentProperties originalCompProperties;
				originalCompProperties.fieldVector.push_back( m_parametersFluorescenceLightFilter->GetFieldByName( ON_STATUS ) );
				originalCompProperties.fieldVector.push_back( m_parametersFluorescenceLightFilter->GetFieldByName( ACTIVE_FLUORESCENCE_FILTER_POSITION ) );

				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersFluorescenceLightFilter );
				forwardingIsComplete = true;

				/// The order is important, here. filter_position should be updated befor on_status. This is so
				/// because the Doler's implementation for the Etaluma assumes this order to avoid a flicker of
				/// the current filter color.
				SetHardwareStateIfNeeded( ComponentType::FluorescenceLightFilter, &originalCompProperties, m_parametersFluorescenceLightFilter, ACTIVE_FLUORESCENCE_FILTER_POSITION, m_outBoxForOptics );
				SetHardwareStateIfNeeded( ComponentType::FluorescenceLightFilter, &originalCompProperties, m_parametersFluorescenceLightFilter, ON_STATUS, m_outBoxForOptics );
			}

			/// --------------- Anular Ring Holder ---------------
			if ((!forwardingIsComplete) && (messageType == GET_ANNULAR_RING_HOLDER_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersAnnularRingHolder );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_ANNULAR_RING_HOLDER_PROPERTIES))
			{
				ComponentProperties originalCompProperties;
				originalCompProperties.fieldVector.push_back( m_parametersAnnularRingHolder->GetFieldByName( CURRENT_RING_POSITION ) );

				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersAnnularRingHolder );

				SetANRPositionStateIfNeeded( ComponentType::AnnularRingHolder, &originalCompProperties, m_parametersAnnularRingHolder, CURRENT_RING_POSITION, m_outBoxForRingHolder );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_ANNULAR_RING_NM))
			{
				MeteOut_MoveSingleAxis( jWorkOrderObj, workMsg, m_outBoxForRingHolder, "ar", m_parametersAnnularRingHolder );
				forwardingIsComplete = true;
			}
				
			/// --------------- Optical Column ---------------
			if ((!forwardingIsComplete) && (messageType == GET_OPTICAL_COLUMN_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersOpticalColumn );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_OPTICAL_COLUMN_PROPERTIES))
			{
				ComponentProperties originalCompProperties;
				originalCompProperties.fieldVector.push_back( m_parametersOpticalColumn->GetFieldByName( ACTIVE_TURRET_POSITION ) );
				
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersOpticalColumn );
				forwardingIsComplete = true;
				SetTurretStateIfNeeded( ComponentType::OpticalColumn, &originalCompProperties, m_parametersOpticalColumn, ACTIVE_TURRET_POSITION, m_outBoxForOptics );
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_FOCUS_NM))
			{
				m_outBoxForOptics->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_FOCUS_RELATIVE_NM))
			{
				m_outBoxForOptics->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_CURRENT_FOCUS_NM))
			{
				m_outBoxForOptics->PushMsg(workMsg);
				forwardingIsComplete = true;
			}

			/// --------------- Camera ---------------
			if ((!forwardingIsComplete) && (messageType == GET_CAMERA_IMAGE))
			{
				succeeded = MeteOut_GetCameraImage( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_CAMERA_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersCamera );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_CAMERA_PROPERTIES))
			{
				ComponentProperties originalCompProperties;
				originalCompProperties.fieldVector.push_back( m_parametersCamera->GetFieldByName( GAIN ) );
				originalCompProperties.fieldVector.push_back( m_parametersCamera->GetFieldByName( EXPOSURE ) );
				originalCompProperties.fieldVector.push_back( m_parametersCamera->GetFieldByName( BIN_FACTOR ) );
				
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersCamera );
				forwardingIsComplete = true;

				SetHardwareStateIfNeeded( ComponentType::Camera, &originalCompProperties, m_parametersCamera, GAIN, m_outBoxForOptics );
				SetHardwareStateIfNeeded( ComponentType::Camera, &originalCompProperties, m_parametersCamera, EXPOSURE, m_outBoxForOptics );
				SetHardwareStateIfNeeded( ComponentType::Camera, &originalCompProperties, m_parametersCamera, BIN_FACTOR, m_outBoxForOptics );
			}

			/// --------------- Incubator ---------------
			if ((!forwardingIsComplete) && (messageType == GET_INCUBATOR_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersIncubator );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_INCUBATOR_PROPERTIES))
			{
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersIncubator );
				forwardingIsComplete = true;
			}

			/// --------------- CxPM ---------------
			if ((!forwardingIsComplete) && (messageType == ADD_PLATE_TO_DECK_INVENTORY))
			{
				/// Forward to the thread that manages the plate inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == REMOVE_PLATE_FROM_DECK_INVENTORY))
			{
				/// Forward to the thread that manages the plate inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_PLATE_IN_DECK_INVENTORY))
			{
				/// Forward to the thread that manages the plate inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_PLATE_LIST_IN_LOCATION))
			{
				/// Forward to the thread that manages the plate inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == FIND_PLATE_LOCATION_IN_INVENTORY))
			{
				/// Forward to the thread that manages the plate inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}

			if ((!forwardingIsComplete) && (messageType == ADD_TIP_RACK_TO_DECK_INVENTORY))
			{
				/// Forward to the thread that manages the tip rack inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == REMOVE_TIP_RACK_FROM_DECK_INVENTORY))
			{
				/// Forward to the thread that manages the tip rack inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_TIP_RACK_LIST_IN_LOCATION))
			{
				/// Forward to the thread that manages the tip rack inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_TIP_COUNT_FOR_TIP_RACK))
			{
				/// Forward to the thread that manages the tip rack inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_TIP_COUNT_FOR_TIP_RACK))
			{
				/// Forward to the thread that manages the tip rack inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TIP_RACK_IN_DECK_INVENTORY))
			{
				/// Forward to the thread that manages the tip rack inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == FIND_TIP_RACK_LOCATION_IN_INVENTORY))
			{
				/// Forward to the thread that manages the tip rack inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_PLATE))
			{
				/// Forward to the thread that manages the plate inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_PLATE_READ_BARCODE))
			{
				/// Forward to the thread that manages the plate inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == DELID_PLATE))
			{
				/// Forward to the thread that manages the plate inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == RELID_PLATE))
			{
				/// Forward to the thread that manages the plate inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TIP_RACK))
			{
				/// Forward to the thread that manages the plate inventory file.
				m_outBoxForCxPM->PushMsg(workMsg);
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_CXPM_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersCxPM );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_CXPM_PROPERTIES))
			{
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersCxPM );
				forwardingIsComplete = true;
			}

			/// --------------- Barcode Reader ---------------
			if ((!forwardingIsComplete) && (messageType == GET_BARCODE_READER_PROPERTIES))
			{
				succeeded = GetProperties( jWorkOrderObj, workMsg, m_parametersBarcodeReader );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_BARCODE_READER_PROPERTIES))
			{
				succeeded = SetProperties( jWorkOrderObj, workMsg, m_parametersBarcodeReader );
				forwardingIsComplete = true;
			}

			/// --------------- Digital Inputs ---------------

			/// --------------- Digital Outputs ---------------

			/// --------------- Internal Commands ---------------
			if ((!forwardingIsComplete) && (messageType == CHECK_HARDWARE))
			{
				/// Forward to the thread that manages the plate inventory file.
				forwardingIsComplete = MeteOut_CheckHardware( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}

			/// --------------- Compound Commands ---------------
			/// Decompose compound messages into simple messages and send them all
			/// out to the queue.
			if ((!forwardingIsComplete) && (messageType == MOVE_ALL_TIPS_TO_SAFE_HEIGHT))
			{
				succeeded = MeteOut_MoveAllTipsToSafeHeight( jWorkOrderObj, workMsg );
				if (!succeeded)
				{
//					cout << m_threadName << "MeteOut_MoveAllTipsToSafeHeight() failed." << endl;
				}
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_TIP))
			{
				succeeded = MeteOut_GetTip( jWorkOrderObj, workMsg );
				if (!succeeded)
				{
//					cout << m_threadName << "MeteOut_GetTip() failed." << endl;
				}
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TIP_TO_WASTE_LOCATION_3))
			{
				succeeded = MeteOut_MoveTipToWasteLocation3( jWorkOrderObj, workMsg );
				if (!succeeded)
				{
//					cout << m_threadName << "MeteOut_MoveTipToWasteLocation3() failed." << endl;
				}
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TIP_TO_WASTE_LOCATION_2))
			{
				succeeded = MeteOut_MoveTipToWasteLocation2( jWorkOrderObj, workMsg );
				if (!succeeded)
				{
//					cout << m_threadName << "MeteOut_MoveTipToWasteLocation2() failed." << endl;
				}
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TIP_TO_WASTE_LOCATION_1))
			{
				succeeded = MeteOut_MoveTipToWasteLocation1( jWorkOrderObj, workMsg );
				if (!succeeded)
				{
//					cout << m_threadName << "MeteOut_MoveTipToWasteLocation1() failed." << endl;
				}
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SEAT_TIP))
			{
				succeeded = MeteOut_SeatTip( jWorkOrderObj, workMsg );
				if (!succeeded)
				{
//					cout << m_threadName << "MeteOut_SeatTip() failed." << endl;
				}
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_TIP_SKEW))
			{
				succeeded = MeteOut_GetTipSkew( jWorkOrderObj, workMsg );
				if (!succeeded)
				{
//					cout << m_threadName << "MeteOut_GetTipSkew failed." << endl;
				}
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_WORKING_TIP))
			{
				succeeded = MeteOut_SetWorkingTip( jWorkOrderObj, workMsg );
				if (!succeeded)
				{
//					cout << m_threadName << "MeteOut_SetWorkingTip failed." << endl;
				}
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == REMOVE_TIP))
			{
				succeeded = MeteOut_RemoveTip( jWorkOrderObj, workMsg );
				if (!succeeded)
				{
//					cout << m_threadName << "MeteOut_RemoveTip failed." << endl;
				}
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == COLLECT_MONTAGE_IMAGES))
			{
				succeeded = MeteOut_CollectMontageImages( jWorkOrderObj, workMsg );
				if (!succeeded)
				{
//					cout << m_threadName << "MeteOut_CollectMontageImages failed." << endl;
				}
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_Z_TIP_PICK_FORCE_NEWTON))
			{
				succeeded = MeteOut_MoveToZTipPickForceNewton( jWorkOrderObj, workMsg );
				if (!succeeded)
				{
//					cout << m_threadName << "MeteOut_MoveToZTipPickForceNewton failed." << endl;
				}
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_Z_TIP_SEATING_FORCE_NEWTON))
			{
				succeeded = MeteOut_MoveToZTipSeatingForceNewton( jWorkOrderObj, workMsg );
				if (!succeeded)
				{
//					cout << m_threadName << "MeteOut_MoveToZTipSeatingForceNewton failed." << endl;
				}
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_Z_TIP_WELL_BOTTOM_FORCE_NEWTON))
			{
				succeeded = MeteOut_MoveToZTipWellBottomForceNewton( jWorkOrderObj, workMsg );
				if (!succeeded)
				{
//					cout << m_threadName << "MeteOut_MoveToZTipWellBottomForceNewton failed." << endl;
				}
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_ZZ_TIP_PICK_FORCE_NEWTON))
			{
				succeeded = MeteOut_MoveToZZTipPickForceNewton( jWorkOrderObj, workMsg );
				if (!succeeded)
				{
//					cout << m_threadName << "MeteOut_MoveToZZTipPickForceNewton failed." << endl;
				}
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_ZZ_TIP_SEATING_FORCE_NEWTON))
			{
				succeeded = forwardingIsComplete = MeteOut_MoveToZZTipSeatingForceNewton( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == MOVE_TO_ZZ_TIP_WELL_BOTTOM_FORCE_NEWTON))
			{
				succeeded = MeteOut_MoveToZZTipWellBottomForceNewton( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == SET_OPTICAL_COLUMN_TURRET_POSITION))
			{
				succeeded = MeteOut_SetOpticalColumnTurretPosition( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == GET_AUTO_FOCUS_IMAGE_STACK))
			{
				succeeded = MeteOut_GetAutoFocusImageStack( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}
			if ((!forwardingIsComplete) && (messageType == CHANGE_ACTIVE_TIP_RACK))
			{
				succeeded = MeteOut_ChangeActiveTipRack( jWorkOrderObj, workMsg );
				forwardingIsComplete = true;
			}

        }
        if (messageParsed && (!forwardingIsComplete))
        {
			/// All other commands can pass through unchanged.
			;
			
			stringstream msg;
			msg << "Unhandled message type " << messageType << " being forwarded to Info thread";
			LOG_WARN( m_threadName, "ci", msg.str().c_str() );
			/// CAUTION: We are using the "Info" Doler for all commands not handled above.
			/// We try to restrict use of Info Doler to fast commands that don't move motors.
			/// Ensure that all motor-moving commands get handled above, before we get here.
			m_outBoxForInfo->PushMsg(workMsg);
		}
		
		/// De-allocate memory.
		LETS_ASSERT(1 == json_object_put( jWorkOrderObj ));
	}
}

/// Foward the check_hardware command to all Doler input queues, except for any that
/// already have one at the back of the queue.
/// The idea is that idle Doler threads should ping their hardware.
/// Avoid needlessly accumulating check_hardware commands on busy threads.
bool CommandInterpreterThread::MeteOut_CheckHardware( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	string checkHardware = CHECK_HARDWARE;

	if (!m_outBoxForOptics->IsOnBackOfQueue( checkHardware ))
	{
		m_outBoxForOptics->PushMsg(workMsg);
	}
	if (!m_outBoxForInfo->IsOnBackOfQueue( checkHardware ))
	{
		m_outBoxForInfo->PushMsg(workMsg);
	}
	if (!m_outBoxForCxD->IsOnBackOfQueue( checkHardware ))
	{
		m_outBoxForCxD->PushMsg(workMsg);
	}
	if (!m_outBoxForCxPM->IsOnBackOfQueue( checkHardware ))
	{
		m_outBoxForCxPM->PushMsg(workMsg);
	}
	if (!m_outBoxForPump->IsOnBackOfQueue( checkHardware ))
	{
		m_outBoxForPump->PushMsg(workMsg);
	}
	if (!m_outBoxForRingHolder->IsOnBackOfQueue( checkHardware ))
	{
		m_outBoxForRingHolder->PushMsg(workMsg);
	}
	return succeeded;
}


bool CommandInterpreterThread::MeteOut_MoveAllTipsToSafeHeight( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (!payloadObj) /// Either it was NULL or both the "payload" key and object are missing.
	{
		payloadObj = json_object_new_object();
		/// Add this empty object. This incurs loss of ownership, meaning someone else will delete it, later.
		int rc = json_object_object_add( jWorkOrderObj, PAYLOAD, payloadObj );
		succeeded &= (rc == 0);
	}
	if (payloadObj)
	{
		/// Stuff some extra json_objects in the payload.
		long z_safeHeight_nm = m_parametersAxisZ->GetFieldByName( Z_SAFE_HEIGHT ).GetValueAsLong();
		int rc = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, json_object_new_int64( z_safeHeight_nm ) );
		succeeded &= (rc == 0);
/*
		unsigned long z_set_speed = m_parametersAxisZ->GetFieldByName( Z_SET_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, Z_SET_SPEED, json_object_new_uint64( z_set_speed ) );
		succeeded &= (rc == 0);
*/
		long zz_safeHeight_nm = m_parametersAxisZZ->GetFieldByName( ZZ_SAFE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, json_object_new_int64( zz_safeHeight_nm ) );
		succeeded &= (rc == 0);
/*
		unsigned long zz_set_speed = m_parametersAxisZZ->GetFieldByName( ZZ_SET_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, ZZ_SET_SPEED, json_object_new_uint64( zz_set_speed ) );
		succeeded &= (rc == 0);
*/
		long zzz_safeHeight_nm = m_parametersAxisZZZ->GetFieldByName( ZZZ_SAFE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZZ_SAFE_HEIGHT, json_object_new_int64( zzz_safeHeight_nm ) );
		succeeded &= (rc == 0);
/*
		unsigned long zzz_set_speed = m_parametersAxisZZZ->GetFieldByName( ZZZ_SET_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, ZZZ_SET_SPEED, json_object_new_uint64( zzz_set_speed ) );
		succeeded &= (rc == 0);
*/
	}
	succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );
	succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );
	succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zzz", m_parametersAxisZZZ );

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveZZZToSafeHeight( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (!payloadObj) /// Either it was NULL or both the "payload" key and object are missing.
	{
		payloadObj = json_object_new_object();
		/// Add this empty object. This incurs loss of ownership, meaning someone else will delete it, later.
		int rc = json_object_object_add( jWorkOrderObj, PAYLOAD, payloadObj );
		succeeded &= (rc == 0);
	}
	if (payloadObj)
	{
		/// Stuff some extra json_objects in the payload.
		long zzz_safeHeight_nm = m_parametersAxisZZZ->GetFieldByName( ZZZ_SAFE_HEIGHT ).GetValueAsLong();
		int rc = json_object_object_add( payloadObj, ZZZ_SAFE_HEIGHT, json_object_new_int64( zzz_safeHeight_nm ) );
		succeeded &= (rc == 0);
/*
		unsigned long zzz_set_speed = m_parametersAxisZZZ->GetFieldByName( ZZZ_SET_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, ZZZ_SET_SPEED, json_object_new_uint64( zzz_set_speed ) );
		succeeded &= (rc == 0);
*/
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zzz", m_parametersAxisZZZ );
	}
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_GetTip( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		A.	MoveAllTipsToSafeHeight()
		B.	a-MoveToXXStep XXAxis.xx_z[z]_tip_to_tip_tray_right_edge - TipType.first_tip_x -
			(<N>/TipType.tip_rack_column_count * TipType.inter_tip_x)
		C.	a-MoveToYYStep YYAxis.yy_tip_tray_top_edge_to_tip_path - TipType.first_tip_y -
			(<N>/TipType.tip_rack_column_count * TipType.inter_tip_y)
		D.	If (tip_axis = Z) Then MoveToZTipPickForceNewton(<tip_type>) End If
		E.	If (tip_axis = ZZ) Then MoveToZZTipPickForceNewton(<tip_type>) End If
		F.	MoveAllTipsToSafeHeight()
	**/

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Read the tip_axis value from the payload to learn which axis to draw from.
		json_object * tipAxisObj = json_object_object_get( payloadObj, TIP_AXIS );
		if (tipAxisObj)
		{
			string tipAxisStr = json_object_get_string( tipAxisObj );
			ComponentType tip_axis = StringNameToComponentTypeEnum( tipAxisStr.c_str() );

			/// Stuff some extra json_objects in the payload.
			long z_safeHeight_nm = m_parametersAxisZ->GetFieldByName( Z_SAFE_HEIGHT ).GetValueAsLong();
			int rc = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, json_object_new_int64( z_safeHeight_nm ) );
			succeeded &= (rc == 0);

			long zz_safeHeight_nm = m_parametersAxisZZ->GetFieldByName( ZZ_SAFE_HEIGHT ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, json_object_new_int64( zz_safeHeight_nm ) );
			succeeded &= (rc == 0);

			long zzz_safeHeight_nm = m_parametersAxisZZZ->GetFieldByName( ZZZ_SAFE_HEIGHT ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, ZZZ_SAFE_HEIGHT, json_object_new_int64( zzz_safeHeight_nm ) );
			succeeded &= (rc == 0);

			long yy_tip_tray_top_edge_to_tip_path = m_parametersAxisYY->GetFieldByName( YY_TIP_TRAY_TOP_EDGE_TO_TIP_PATH ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, YY_TIP_TRAY_TOP_EDGE_TO_TIP_PATH, json_object_new_int64( yy_tip_tray_top_edge_to_tip_path ) );
			succeeded &= (rc == 0);

			succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "xx", m_parametersAxisXX );
			succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "yy", m_parametersAxisYY );
			if (tip_axis == ComponentType::ZAxis)
			{
				long xx_z_tip_to_tip_tray_right_edge = m_parametersAxisXX->GetFieldByName( XX_Z_TIP_TO_TIP_TRAY_RIGHT_EDGE ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, XX_Z_TIP_TO_TIP_TRAY_RIGHT_EDGE, json_object_new_int64( xx_z_tip_to_tip_tray_right_edge ) );
				succeeded &= (rc == 0);
				
				long z_y_offset_from_xx_axis = m_parametersAxisZ->GetFieldByName( Z_Y_OFFSET_FROM_XX_AXIS ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, Z_Y_OFFSET_FROM_XX_AXIS, json_object_new_int64( z_y_offset_from_xx_axis ) );
				succeeded &= (rc == 0);
				
				long z_to_top_of_YY_stage_height_nm = m_parametersAxisZ->GetFieldByName( Z_TO_TOP_OF_YY_STAGE_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, Z_TO_TOP_OF_YY_STAGE_HEIGHT, json_object_new_int64( z_to_top_of_YY_stage_height_nm ) );
				succeeded &= (rc == 0);

				long tip_pickup_slowdown_offset_nm = m_parametersAxisZ->GetFieldByName( TIP_PICKUP_SLOWDOWN_OFFSET ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_PICKUP_SLOWDOWN_OFFSET, json_object_new_int64( tip_pickup_slowdown_offset_nm ) );
				succeeded &= (rc == 0);

				unsigned long tip_pickup_speed = m_parametersAxisZ->GetFieldByName( TIP_PICKUP_SPEED ).GetValueAsULong();
				rc = json_object_object_add( payloadObj, TIP_PICKUP_SPEED, json_object_new_uint64( tip_pickup_speed ) );
				succeeded &= (rc == 0);

				double tip_pickup_force = m_parametersAxisZ->GetFieldByName( TIP_PICKUP_FORCE ).GetValueAsFloat();
				rc = json_object_object_add( payloadObj, TIP_PICKUP_FORCE, json_object_new_double( tip_pickup_force ) );
				succeeded &= (rc == 0);

				long tip_pickup_overshoot = m_parametersAxisZ->GetFieldByName( TIP_PICKUP_OVERSHOOT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_PICKUP_OVERSHOOT, json_object_new_int64( tip_pickup_overshoot ) );
				succeeded &= (rc == 0);
/*
				unsigned long z_set_speed = m_parametersAxisZ->GetFieldByName( Z_SET_SPEED ).GetValueAsULong();
				rc = json_object_object_add( payloadObj, Z_SET_SPEED, json_object_new_uint64( z_set_speed ) );
				succeeded &= (rc == 0);
*/
				succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );
				succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ ); /// For adjacent moves to safe height.
				succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zzz", m_parametersAxisZZZ ); /// For adjacent moves to safe height.
			}
			else if (tip_axis == ComponentType::ZZAxis)
			{
				long xx_zz_tip_to_tip_tray_right_edge = m_parametersAxisXX->GetFieldByName( XX_ZZ_TIP_TO_TIP_TRAY_RIGHT_EDGE ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, XX_ZZ_TIP_TO_TIP_TRAY_RIGHT_EDGE, json_object_new_int64( xx_zz_tip_to_tip_tray_right_edge ) );
				succeeded &= (rc == 0);

				long zz_y_offset_from_xx_axis = m_parametersAxisZZ->GetFieldByName( ZZ_Y_OFFSET_FROM_XX_AXIS ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, ZZ_Y_OFFSET_FROM_XX_AXIS, json_object_new_int64( zz_y_offset_from_xx_axis ) );
				succeeded &= (rc == 0);
				
				long zz_to_top_of_YY_stage_height_nm = m_parametersAxisZZ->GetFieldByName( ZZ_TO_TOP_OF_YY_STAGE_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, ZZ_TO_TOP_OF_YY_STAGE_HEIGHT, json_object_new_int64( zz_to_top_of_YY_stage_height_nm ) );
				succeeded &= (rc == 0);

				long tip_pickup_slowdown_offset_nm = m_parametersAxisZZ->GetFieldByName( TIP_PICKUP_SLOWDOWN_OFFSET ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_PICKUP_SLOWDOWN_OFFSET, json_object_new_int64( tip_pickup_slowdown_offset_nm ) );
				succeeded &= (rc == 0);

				unsigned long tip_pickup_speed = m_parametersAxisZZ->GetFieldByName( TIP_PICKUP_SPEED ).GetValueAsULong();
				rc = json_object_object_add( payloadObj, TIP_PICKUP_SPEED, json_object_new_uint64( tip_pickup_speed ) );
				succeeded &= (rc == 0);

				double tip_pickup_force = m_parametersAxisZZ->GetFieldByName( TIP_PICKUP_FORCE ).GetValueAsFloat();
				rc = json_object_object_add( payloadObj, TIP_PICKUP_FORCE, json_object_new_double( tip_pickup_force ) );
				succeeded &= (rc == 0);

				long tip_pickup_overshoot = m_parametersAxisZZ->GetFieldByName( TIP_PICKUP_OVERSHOOT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_PICKUP_OVERSHOOT, json_object_new_int64( tip_pickup_overshoot ) );
				succeeded &= (rc == 0);
/*
				unsigned long zz_set_speed = m_parametersAxisZZ->GetFieldByName( ZZ_SET_SPEED ).GetValueAsULong();
				rc = json_object_object_add( payloadObj, ZZ_SET_SPEED, json_object_new_uint64( zz_set_speed ) );
				succeeded &= (rc == 0);
*/			
				succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ ); /// For adjacent moves to safe height.
				succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );
				succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zzz", m_parametersAxisZZZ ); /// For adjacent moves to safe height.
			}
			else
			{
				stringstream msg;
				msg << "Invalid tip_axis:" << tipAxisStr;
				LOG_ERROR( m_threadName, "tip", msg.str().c_str() );
				succeeded = false;
			}
		}
	}
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveTipToWasteLocation3( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
	A.	MoveAllTipsToSafeHeight()
	B.	If (<tip_axis> = Z) Then 
		a.	MoveToXXNM(XXAxis.xx_z_tip_to_waste_3_position)
		b.	MoveToZNM(ZAxis.z_to_waste_3_height + ZAxis.tip_waste_3_safety_gap)
		c.	End If
	C.	If (<tip_axis> = ZZ) Then 
		a.	MoveToXXNM(XXAxis.xx_zz_tip_to_waste_3_position)
		b.	MoveToZZNM(ZZAxis.zz_to_waste_3_height + ZZAxis.tip_waste_3_safety_gap)
		c.	End If
	D.	If (<tip_axis> = ZZZ) Then 
		a.	MoveToXXNM(XXAxis.xx_zzz_tip_to_waste_3_position)
		b.	MoveToZZZNM(ZZZAxis.zzz_to_waste_3_height + ZZZAxis.tip_waste_3_safety_gap)
		c.	End If
	**/

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Read the tip_axis value from the payload to learn which axis to draw from.
		json_object * tipAxisObj = json_object_object_get( payloadObj, TIP_AXIS );
		if (tipAxisObj)
		{
			string tipAxisStr = json_object_get_string( tipAxisObj );
			ComponentType tip_axis = StringNameToComponentTypeEnum( tipAxisStr.c_str() );

			if (tip_axis == ComponentType::ZAxis)
			{
				long xx_z_tip_to_waste_3_position = m_parametersAxisXX->GetFieldByName( XX_Z_TIP_TO_WASTE_3_POSITION ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, XX_Z_TIP_TO_WASTE_3_POSITION, json_object_new_int64( xx_z_tip_to_waste_3_position ) );
				succeeded &= (rc == 0);
				
				long z_to_waste_3_height = m_parametersAxisZ->GetFieldByName( Z_TO_WASTE_3_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, Z_TO_WASTE_3_HEIGHT, json_object_new_int64( z_to_waste_3_height ) );
				succeeded &= (rc == 0);
				
				long tip_waste_3_safety_gap = m_parametersAxisZ->GetFieldByName( TIP_WASTE_3_SAFETY_GAP ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_WASTE_3_SAFETY_GAP, json_object_new_int64( tip_waste_3_safety_gap ) );
				succeeded &= (rc == 0);
			}
			else if (tip_axis == ComponentType::ZZAxis)
			{
				long xx_zz_tip_to_waste_3_position = m_parametersAxisXX->GetFieldByName( XX_ZZ_TIP_TO_WASTE_3_POSITION ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, XX_ZZ_TIP_TO_WASTE_3_POSITION, json_object_new_int64( xx_zz_tip_to_waste_3_position ) );
				succeeded &= (rc == 0);
				
				long zz_to_waste_3_height = m_parametersAxisZZ->GetFieldByName( ZZ_TO_WASTE_3_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, ZZ_TO_WASTE_3_HEIGHT, json_object_new_int64( zz_to_waste_3_height ) );
				succeeded &= (rc == 0);
				
				long tip_waste_3_safety_gap = m_parametersAxisZZ->GetFieldByName( TIP_WASTE_3_SAFETY_GAP ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_WASTE_3_SAFETY_GAP, json_object_new_int64( tip_waste_3_safety_gap ) );
			}
			else if (tip_axis == ComponentType::ZZZAxis)
			{
				long xx_zzz_tip_to_waste_3_position = m_parametersAxisXX->GetFieldByName( XX_ZZZ_TIP_TO_WASTE_3_POSITION ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, XX_ZZZ_TIP_TO_WASTE_3_POSITION, json_object_new_int64( xx_zzz_tip_to_waste_3_position ) );
				succeeded &= (rc == 0);
				
				long zzz_to_waste_3_height = m_parametersAxisZZZ->GetFieldByName( ZZZ_TO_WASTE_3_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, ZZZ_TO_WASTE_3_HEIGHT, json_object_new_int64( zzz_to_waste_3_height ) );
				succeeded &= (rc == 0);
				
				long tip_waste_3_safety_gap = m_parametersAxisZZZ->GetFieldByName( TIP_WASTE_3_SAFETY_GAP ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_WASTE_3_SAFETY_GAP, json_object_new_int64( tip_waste_3_safety_gap ) );
			}
			else
			{
				succeeded = false;
			}
		}
		
		/// Add the data needed by MoveAllTipsToSafeHeight.
		long z_safeHeight_nm = m_parametersAxisZ->GetFieldByName( Z_SAFE_HEIGHT ).GetValueAsLong();
		int rc = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, json_object_new_int64( z_safeHeight_nm ) );
		succeeded &= (rc == 0);
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );

		long zz_safeHeight_nm = m_parametersAxisZZ->GetFieldByName( ZZ_SAFE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, json_object_new_int64( zz_safeHeight_nm ) );
		succeeded &= (rc == 0);
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );

		long zzz_safeHeight_nm = m_parametersAxisZZZ->GetFieldByName( ZZZ_SAFE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZZ_SAFE_HEIGHT, json_object_new_int64( zzz_safeHeight_nm ) );
		succeeded &= (rc == 0);
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zzz", m_parametersAxisZZZ );

		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "xx", m_parametersAxisXX );
	}
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveTipToWasteLocation2( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
	A.	MoveAllTipsToSafeHeight()
	B.	If (<tip_axis> = Z) Then 
		a.	MoveToXXNM(XXAxis.xx_z_tip_to_waste_2_position)
		b.	MoveToZNM(ZAxis.z_to_waste_2_height + tip_working_length + ZAxis.tip_waste_2_safety_gap)
		c.	End If
	C.	If (<tip_axis> = ZZ) Then 
		a.	MoveToXXNM(XXAxis.xx_zz_tip_to_waste_2_position)
		b.	MoveToZZNM(ZZAxis.zz_to_waste_2_height + tip_working_length + ZZAxis.tip_waste_2_safety_gap)
		c.	End If
	D.	If (<tip_axis> = ZZZ) Then 
		a.	MoveToXXNM(XXAxis.xx_zzz_tip_to_waste_2_position)
		b.	MoveToZZZNM(ZZZAxis.zzz_to_waste_2_height + tip_working_length + ZZZAxis.tip_waste_2_safety_gap)
		c.	End If
	**/

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Read the tip_axis value from the payload to learn which axis to draw from.
		json_object * tipAxisObj = json_object_object_get( payloadObj, TIP_AXIS );
		if (tipAxisObj)
		{
			string tipAxisStr = json_object_get_string( tipAxisObj );
			ComponentType tip_axis = StringNameToComponentTypeEnum( tipAxisStr.c_str() );

			if (tip_axis == ComponentType::ZAxis)
			{
				long xx_z_tip_to_waste_2_position = m_parametersAxisXX->GetFieldByName( XX_Z_TIP_TO_WASTE_2_POSITION ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, XX_Z_TIP_TO_WASTE_2_POSITION, json_object_new_int64( xx_z_tip_to_waste_2_position ) );
				succeeded &= (rc == 0);
				
				long z_to_waste_2_height = m_parametersAxisZ->GetFieldByName( Z_TO_WASTE_2_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, Z_TO_WASTE_2_HEIGHT, json_object_new_int64( z_to_waste_2_height ) );
				succeeded &= (rc == 0);
				
				long tip_waste_2_safety_gap = m_parametersAxisZ->GetFieldByName( TIP_WASTE_2_SAFETY_GAP ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_WASTE_2_SAFETY_GAP, json_object_new_int64( tip_waste_2_safety_gap ) );
				succeeded &= (rc == 0);
			}
			else if (tip_axis == ComponentType::ZZAxis)
			{
				long xx_zz_tip_to_waste_2_position = m_parametersAxisXX->GetFieldByName( XX_ZZ_TIP_TO_WASTE_2_POSITION ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, XX_ZZ_TIP_TO_WASTE_2_POSITION, json_object_new_int64( xx_zz_tip_to_waste_2_position ) );
				succeeded &= (rc == 0);
				
				long zz_to_waste_2_height = m_parametersAxisZZ->GetFieldByName( ZZ_TO_WASTE_2_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, ZZ_TO_WASTE_2_HEIGHT, json_object_new_int64( zz_to_waste_2_height ) );
				succeeded &= (rc == 0);
				
				long tip_waste_2_safety_gap = m_parametersAxisZZ->GetFieldByName( TIP_WASTE_2_SAFETY_GAP ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_WASTE_2_SAFETY_GAP, json_object_new_int64( tip_waste_2_safety_gap ) );
				succeeded &= (rc == 0);
			}
			else if (tip_axis == ComponentType::ZZZAxis)
			{
				long xx_zzz_tip_to_waste_2_position = m_parametersAxisXX->GetFieldByName( XX_ZZZ_TIP_TO_WASTE_2_POSITION ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, XX_ZZZ_TIP_TO_WASTE_2_POSITION, json_object_new_int64( xx_zzz_tip_to_waste_2_position ) );
				succeeded &= (rc == 0);
				
				long zzz_to_waste_2_height = m_parametersAxisZZZ->GetFieldByName( ZZZ_TO_WASTE_2_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, ZZZ_TO_WASTE_2_HEIGHT, json_object_new_int64( zzz_to_waste_2_height ) );
				succeeded &= (rc == 0);
				
				long tip_waste_2_safety_gap = m_parametersAxisZZZ->GetFieldByName( TIP_WASTE_2_SAFETY_GAP ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_WASTE_2_SAFETY_GAP, json_object_new_int64( tip_waste_2_safety_gap ) );
				succeeded &= (rc == 0);
			}
			else
			{
				succeeded = false;
			}
		}
		
		/// Add the data needed by MoveAllTipsToSafeHeight.
		long z_safeHeight_nm = m_parametersAxisZ->GetFieldByName( Z_SAFE_HEIGHT ).GetValueAsLong();
		int rc = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, json_object_new_int64( z_safeHeight_nm ) );
		succeeded &= (rc == 0);
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );

		long zz_safeHeight_nm = m_parametersAxisZZ->GetFieldByName( ZZ_SAFE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, json_object_new_int64( zz_safeHeight_nm ) );
		succeeded &= (rc == 0);
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );

		long zzz_safeHeight_nm = m_parametersAxisZZZ->GetFieldByName( ZZZ_SAFE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZZ_SAFE_HEIGHT, json_object_new_int64( zzz_safeHeight_nm ) );
		succeeded &= (rc == 0);
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zzz", m_parametersAxisZZZ );

		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "xx", m_parametersAxisXX );
	}
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveTipToWasteLocation1( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
	A.	SetAllTipsToSafeHeight()
	B.	If (<tip_axis> = Z) Then 
		a.	MoveToXXNM(XXAxis.xx_z_tip_to_waste_1_position)
		b.	MoveToZNM(ZAxis.z_to_waste_1_height + tip_working_length + ZAxis.tip_waste_1_safety_gap)
		c.	End If
	C.	If (<tip_axis> = ZZ) Then 
		a.	MoveToXXNM(XXAxis.xx_zz_tip_to_waste_1_position)
		b.	MoveToZZNM(ZZAxis.zz_to_waste_1_height + tip_working_length + ZZAxis.tip_waste_1_safety_gap)
		c.	End If
	D.	If (<tip_axis> = ZZZ) Then 
		a.	MoveToXXNM(XXAxis.xx_zzz_tip_to_waste_1_position)
		b.	MoveToZZZNM(ZZZAxis.zzz_to_waste_1_height + tip_working_length + ZZZAxis.tip_waste_1_safety_gap)
		c.	End If
	**/

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Read the tip_axis value from the payload to learn which axis to draw from.
		json_object * tipAxisObj = json_object_object_get( payloadObj, TIP_AXIS );
		if (tipAxisObj)
		{
			string tipAxisStr = json_object_get_string( tipAxisObj );
			ComponentType tip_axis = StringNameToComponentTypeEnum( tipAxisStr.c_str() );

			if (tip_axis == ComponentType::ZAxis)
			{
				long xx_z_tip_to_waste_1_position = m_parametersAxisXX->GetFieldByName( XX_Z_TIP_TO_WASTE_1_POSITION ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, XX_Z_TIP_TO_WASTE_1_POSITION, json_object_new_int64( xx_z_tip_to_waste_1_position ) );
				succeeded &= (rc == 0);
				
				long z_to_waste_1_height = m_parametersAxisZ->GetFieldByName( Z_TO_WASTE_1_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, Z_TO_WASTE_1_HEIGHT, json_object_new_int64( z_to_waste_1_height ) );
				succeeded &= (rc == 0);
				
				long tip_waste_1_safety_gap = m_parametersAxisZ->GetFieldByName( TIP_WASTE_1_SAFETY_GAP ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_WASTE_1_SAFETY_GAP, json_object_new_int64( tip_waste_1_safety_gap ) );
				succeeded &= (rc == 0);
			}
			else if (tip_axis == ComponentType::ZZAxis)
			{
				long xx_zz_tip_to_waste_1_position = m_parametersAxisXX->GetFieldByName( XX_ZZ_TIP_TO_WASTE_1_POSITION ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, XX_ZZ_TIP_TO_WASTE_1_POSITION, json_object_new_int64( xx_zz_tip_to_waste_1_position ) );
				succeeded &= (rc == 0);
				
				long zz_to_waste_1_height = m_parametersAxisZZ->GetFieldByName( ZZ_TO_WASTE_1_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, ZZ_TO_WASTE_1_HEIGHT, json_object_new_int64( zz_to_waste_1_height ) );
				succeeded &= (rc == 0);
				
				long tip_waste_1_safety_gap = m_parametersAxisZZ->GetFieldByName( TIP_WASTE_1_SAFETY_GAP ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_WASTE_1_SAFETY_GAP, json_object_new_int64( tip_waste_1_safety_gap ) );
				succeeded &= (rc == 0);
			}
			else if (tip_axis == ComponentType::ZZZAxis)
			{
				long xx_zzz_tip_to_waste_1_position = m_parametersAxisXX->GetFieldByName( XX_ZZZ_TIP_TO_WASTE_1_POSITION ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, XX_ZZZ_TIP_TO_WASTE_1_POSITION, json_object_new_int64( xx_zzz_tip_to_waste_1_position ) );
				succeeded &= (rc == 0);
				
				long zzz_to_waste_1_height = m_parametersAxisZZZ->GetFieldByName( ZZZ_TO_WASTE_1_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, ZZZ_TO_WASTE_1_HEIGHT, json_object_new_int64( zzz_to_waste_1_height ) );
				succeeded &= (rc == 0);
				
				long tip_waste_1_safety_gap = m_parametersAxisZZZ->GetFieldByName( TIP_WASTE_1_SAFETY_GAP ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_WASTE_1_SAFETY_GAP, json_object_new_int64( tip_waste_1_safety_gap ) );
				succeeded &= (rc == 0);
			}
			else
			{
				succeeded = false;
			}
		}
		
		/// Add the data needed by MoveAllTipsToSafeHeight.
		long z_safeHeight_nm = m_parametersAxisZ->GetFieldByName( Z_SAFE_HEIGHT ).GetValueAsLong();
		int rc = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, json_object_new_int64( z_safeHeight_nm ) );
		succeeded &= (rc == 0);
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );

		long zz_safeHeight_nm = m_parametersAxisZZ->GetFieldByName( ZZ_SAFE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, json_object_new_int64( zz_safeHeight_nm ) );
		succeeded &= (rc == 0);
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );

		long zzz_safeHeight_nm = m_parametersAxisZZZ->GetFieldByName( ZZZ_SAFE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZZ_SAFE_HEIGHT, json_object_new_int64( zzz_safeHeight_nm ) );
		succeeded &= (rc == 0);
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zzz", m_parametersAxisZZZ );

		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "xx", m_parametersAxisXX );
	}
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_SeatTip( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		A.	MoveAllTipsToSafeHeight()
		B.	a-MoveToYYNM(<YYAxis.yy_key_to_tip_path>)
		C.	If (<tip_axis> = Z) Then 
			a.	a-MoveToXXNM(XXAxis.xx_z_tip_to_key_work_position)
			b.	MoveToZTipSeatingForceNewton(<tip_type>) 
			c.	End If
		D.	If (<tip_axis> = ZZ) Then 
			a.	a-MoveToXXNM(XXAxis.xx_zz_tip_to_key_work_position)
			b.	MoveToZZTipSeatingForceNewton(<tip_type>) 
			c.	End If
		E.	MoveAllTipsToSafeHeight()
	**/

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Read the tip_axis value from the payload to learn which axis to draw from.
		json_object * tipAxisObj = json_object_object_get( payloadObj, TIP_AXIS );
		if (tipAxisObj)
		{
			string tipAxisStr = json_object_get_string( tipAxisObj );
			ComponentType tip_axis = StringNameToComponentTypeEnum( tipAxisStr.c_str() );
			
			/// Stuff some extra json_objects in the payload.
			long z_safeHeight_nm = m_parametersAxisZ->GetFieldByName( Z_SAFE_HEIGHT ).GetValueAsLong();
			int rc = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, json_object_new_int64( z_safeHeight_nm ) );
			succeeded &= (rc == 0);

			long zz_safeHeight_nm = m_parametersAxisZZ->GetFieldByName( ZZ_SAFE_HEIGHT ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, json_object_new_int64( zz_safeHeight_nm ) );
			succeeded &= (rc == 0);

			long zzz_safeHeight_nm = m_parametersAxisZZZ->GetFieldByName( ZZZ_SAFE_HEIGHT ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, ZZZ_SAFE_HEIGHT, json_object_new_int64( zzz_safeHeight_nm ) );
			succeeded &= (rc == 0);

			long yy_key_to_tip_path = m_parametersAxisYY->GetFieldByName( YY_KEY_TO_TIP_PATH ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, YY_KEY_TO_TIP_PATH, json_object_new_int64( yy_key_to_tip_path ) );
			succeeded &= (rc == 0);
			succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "xx", m_parametersAxisXX );
			succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "yy", m_parametersAxisYY );

			if (tip_axis == ComponentType::ZAxis)
			{
				long xx_z_tip_to_key_work_position = m_parametersAxisXX->GetFieldByName( XX_Z_TIP_TO_KEY_WORK_POSITION ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, XX_Z_TIP_TO_KEY_WORK_POSITION, json_object_new_int64( xx_z_tip_to_key_work_position ) );
				succeeded &= (rc == 0);

				long z_y_offset_from_xx_axis = m_parametersAxisZ->GetFieldByName( Z_Y_OFFSET_FROM_XX_AXIS ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, Z_Y_OFFSET_FROM_XX_AXIS, json_object_new_int64( z_y_offset_from_xx_axis ) );
				succeeded &= (rc == 0);
				
				long tip_seating_slowdown_offset = m_parametersAxisZ->GetFieldByName( TIP_SEATING_SLOWDOWN_OFFSET ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_SEATING_SLOWDOWN_OFFSET, json_object_new_int64( tip_seating_slowdown_offset ) );
				succeeded &= (rc == 0);

				unsigned long tip_seating_speed = m_parametersAxisZ->GetFieldByName( TIP_SEATING_SPEED ).GetValueAsULong();
				rc = json_object_object_add( payloadObj, TIP_SEATING_SPEED, json_object_new_uint64( tip_seating_speed ) );
				succeeded &= (rc == 0);
/*
				unsigned long z_set_speed = m_parametersAxisZ->GetFieldByName( Z_SET_SPEED ).GetValueAsULong();
				rc = json_object_object_add( payloadObj, Z_SET_SPEED, json_object_new_uint64( z_set_speed ) );
				succeeded &= (rc == 0);
*/
				double tip_seating_force = m_parametersAxisZ->GetFieldByName( TIP_SEATING_FORCE ).GetValueAsFloat();
				rc = json_object_object_add( payloadObj, TIP_SEATING_FORCE, json_object_new_double( tip_seating_force ) );
				succeeded &= (rc == 0);
				
				long z_to_top_of_seating_key_height = m_parametersAxisZ->GetFieldByName( Z_TO_TOP_OF_SEATING_KEY_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, Z_TO_TOP_OF_SEATING_KEY_HEIGHT, json_object_new_int64( z_to_top_of_seating_key_height ) );
				succeeded &= (rc == 0);
				
				long tip_seating_overshoot = m_parametersAxisZ->GetFieldByName( TIP_SEATING_OVERSHOOT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_SEATING_OVERSHOOT, json_object_new_int64( tip_seating_overshoot ) );
				succeeded &= (rc == 0);

				long z_safeHeight_nm = m_parametersAxisZ->GetFieldByName( Z_SAFE_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, json_object_new_int64( z_safeHeight_nm ) );
				succeeded &= (rc == 0);
				
				succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );
				succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ ); /// For adjacent moves to safe height.
				succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zzz", m_parametersAxisZZZ ); /// For adjacent moves to safe height.
			}
			else if (tip_axis == ComponentType::ZZAxis)
			{
				long xx_zz_tip_to_key_work_position = m_parametersAxisXX->GetFieldByName( XX_ZZ_TIP_TO_KEY_WORK_POSITION ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, XX_ZZ_TIP_TO_KEY_WORK_POSITION, json_object_new_int64( xx_zz_tip_to_key_work_position ) );
				succeeded &= (rc == 0);

				long zz_y_offset_from_xx_axis = m_parametersAxisZZ->GetFieldByName( ZZ_Y_OFFSET_FROM_XX_AXIS ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, ZZ_Y_OFFSET_FROM_XX_AXIS, json_object_new_int64( zz_y_offset_from_xx_axis ) );
				succeeded &= (rc == 0);
				
				long tip_seating_slowdown_offset = m_parametersAxisZZ->GetFieldByName( TIP_SEATING_SLOWDOWN_OFFSET ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_SEATING_SLOWDOWN_OFFSET, json_object_new_int64( tip_seating_slowdown_offset ) );
				succeeded &= (rc == 0);

				unsigned long tip_seating_speed = m_parametersAxisZZ->GetFieldByName( TIP_SEATING_SPEED ).GetValueAsULong();
				rc = json_object_object_add( payloadObj, TIP_SEATING_SPEED, json_object_new_uint64( tip_seating_speed ) );
				succeeded &= (rc == 0);
/*
				unsigned long zz_set_speed = m_parametersAxisZZ->GetFieldByName( ZZ_SET_SPEED ).GetValueAsULong();
				rc = json_object_object_add( payloadObj, ZZ_SET_SPEED, json_object_new_uint64( zz_set_speed ) );
				succeeded &= (rc == 0);
*/
				double tip_seating_force = m_parametersAxisZZ->GetFieldByName( TIP_SEATING_FORCE ).GetValueAsFloat();
				rc = json_object_object_add( payloadObj, TIP_SEATING_FORCE, json_object_new_double( tip_seating_force ) );
				succeeded &= (rc == 0);
				
				long zz_to_top_of_seating_key_height = m_parametersAxisZZ->GetFieldByName( ZZ_TO_TOP_OF_SEATING_KEY_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, ZZ_TO_TOP_OF_SEATING_KEY_HEIGHT, json_object_new_int64( zz_to_top_of_seating_key_height ) );
				succeeded &= (rc == 0);
				
				long tip_seating_overshoot = m_parametersAxisZZ->GetFieldByName( TIP_SEATING_OVERSHOOT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_SEATING_OVERSHOOT, json_object_new_int64( tip_seating_overshoot ) );
				succeeded &= (rc == 0);

				long zz_safeHeight_nm = m_parametersAxisZZ->GetFieldByName( ZZ_SAFE_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, json_object_new_int64( zz_safeHeight_nm ) );
				succeeded &= (rc == 0);
				
				succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ ); /// For adjacent moves to safe height.
				succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );
				succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zzz", m_parametersAxisZZZ ); /// For adjacent moves to safe height.
			}
			else
			{
				succeeded = false;
			}
		}
	}
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_GetTipSkew( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		Moves the specified tip through the skew sensor.
		Returns the x_skew_nm and y_skew_num values if the operation was successful.
		Returns "status_code" = 4040 if either the x_skew_nm or y_skew_nm calls failed.

		A.	MoveAllTipsToSafeHeight()
		B.	a-MoveToXYNM(XYAxis.x_optical_sensor_to_tip_skew_position, XYAxis.y_optical_sensor_to_tip_skew_position + <tip_axis> y offset to xx axis)
		
		C.	*SetXTo(XYAxes.x_find_tip_skew_speed, XYAxes.x_find_tip_skew_acceleration, XYAxes.x_find_tip_skew_deceleration)
		D.	*SetYTo(XYAxes.y_find_tip_skew_speed, XYAxes.y_find_tip_skew_acceleration, XYAxes.y_find_tip_skew_deceleration)
		
		E.	If (<tip_axis> = Z) Then 
			a.	a-MoveToXXNM(XXAxis.xx_z_tip_to_tip_skew_position)
			b.	MoveToZNM(ZAxis.z_to_tip_skew_sensor_height + tip_working_length + ZAxis.tip_skew_safety_gap)
			c.	End If
		F.	If (<tip_axis> = ZZ) Then
			a.	a-MoveToXXNM(XXAxis.xx_zz_tip_to_tip_skew_position)
			b.	MoveToZZNM(ZZAxis.zz_to_tip_skew_sensor_height + tip_working_length + ZZAxis.tip_skew_safety_gap)
			c.	End If
		G.	<x_skew_nm> = MoveXToTipSkewTrigger (this is a prog0 command)
				Move in the X direction XYAxis.X_tip_skew_travel_distance and capture the X location when the Z or ZZ axis triggers the tip X skew light sensor.
				Assumes the XY, XX, and Z or ZZ axes are in the proper locations before this command is called.
				Returns error with null payload if the X axis travels the XYAxis.x_tip_skew_travel_distance without triggering the light sensor.
				Returns the X axis location where the trigger occurred if it did.
		H.	MoveToXYNM(XYAxis.x_optical_sensor_to_tip_skew_position, XYAxis.y_optical_sensor_to_tip_skew_position + <tip_axis> y offset to xx axis)
		I.	<y_skew_nm> = MoveYToTipSkewTrigger (this is a prog0 command)

		J.	*SetXTo(XYAxes.x_speed, XYAxes.x_acceleration, XYAxes.x_deceleration)
		K.	*SetYTo(XYAxes.y_speed, XYAxes.y_acceleration, XYAxes.y_deceleration)

		L.	MoveAllTipsToSafeHeight()
		M.	If (either x_skew_nm or y_skew_nm failed) Then return "status_code" = 4040
	**/

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Read the tip_axis value from the payload to learn which axis to draw from.
		json_object * tipAxisObj = json_object_object_get( payloadObj, TIP_AXIS );
		if (tipAxisObj)
		{
			string tipAxisStr = json_object_get_string( tipAxisObj );
			ComponentType tip_axis = StringNameToComponentTypeEnum( tipAxisStr.c_str() );

			/// Stuff some extra json_objects in the payload.
			long z_safeHeight_nm = m_parametersAxisZ->GetFieldByName( Z_SAFE_HEIGHT ).GetValueAsLong();
			int rc = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, json_object_new_int64( z_safeHeight_nm ) );
			succeeded &= (rc == 0);

			long zz_safeHeight_nm = m_parametersAxisZZ->GetFieldByName( ZZ_SAFE_HEIGHT ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, json_object_new_int64( zz_safeHeight_nm ) );
			succeeded &= (rc == 0);

			long zzz_safeHeight_nm = m_parametersAxisZZZ->GetFieldByName( ZZZ_SAFE_HEIGHT ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, ZZZ_SAFE_HEIGHT, json_object_new_int64( zzz_safeHeight_nm ) );
			succeeded &= (rc == 0);

			long x_optical_sensor_to_tip_skew_position = m_parametersAxesXY->GetFieldByName( X_OPTICAL_SENSOR_TO_TIP_SKEW_POSITION ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, X_OPTICAL_SENSOR_TO_TIP_SKEW_POSITION, json_object_new_int64( x_optical_sensor_to_tip_skew_position ) );
			succeeded &= (rc == 0);

			long y_optical_sensor_to_tip_skew_position = m_parametersAxesXY->GetFieldByName( Y_OPTICAL_SENSOR_TO_TIP_SKEW_POSITION ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, Y_OPTICAL_SENSOR_TO_TIP_SKEW_POSITION, json_object_new_int64( y_optical_sensor_to_tip_skew_position ) );
			succeeded &= (rc == 0);

			long x_find_tip_skew_travel_distance = m_parametersAxesXY->GetFieldByName( X_FIND_TIP_SKEW_TRAVEL_DISTANCE ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, X_FIND_TIP_SKEW_TRAVEL_DISTANCE, json_object_new_int64( x_find_tip_skew_travel_distance ) );
			succeeded &= (rc == 0);

			long y_find_tip_skew_travel_distance = m_parametersAxesXY->GetFieldByName( Y_FIND_TIP_SKEW_TRAVEL_DISTANCE ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, Y_FIND_TIP_SKEW_TRAVEL_DISTANCE, json_object_new_int64( y_find_tip_skew_travel_distance ) );
			succeeded &= (rc == 0);

			unsigned long x_find_tip_skew_speed = m_parametersAxesXY->GetFieldByName( X_FIND_TIP_SKEW_SPEED ).GetValueAsULong();
			rc = json_object_object_add( payloadObj, X_FIND_TIP_SKEW_SPEED, json_object_new_uint64( x_find_tip_skew_speed ) );
			succeeded &= (rc == 0);

			unsigned long y_find_tip_skew_speed = m_parametersAxesXY->GetFieldByName( Y_FIND_TIP_SKEW_SPEED ).GetValueAsULong();
			rc = json_object_object_add( payloadObj, Y_FIND_TIP_SKEW_SPEED, json_object_new_uint64( y_find_tip_skew_speed ) );
			succeeded &= (rc == 0);

			unsigned long x_find_tip_skew_acceleration = m_parametersAxesXY->GetFieldByName( X_FIND_TIP_SKEW_ACCELERATION ).GetValueAsULong();
			rc = json_object_object_add( payloadObj, X_FIND_TIP_SKEW_ACCELERATION, json_object_new_uint64( x_find_tip_skew_acceleration ) );
			succeeded &= (rc == 0);

			unsigned long y_find_tip_skew_acceleration = m_parametersAxesXY->GetFieldByName( Y_FIND_TIP_SKEW_ACCELERATION ).GetValueAsULong();
			rc = json_object_object_add( payloadObj, Y_FIND_TIP_SKEW_ACCELERATION, json_object_new_uint64( y_find_tip_skew_acceleration ) );
			succeeded &= (rc == 0);

			unsigned long x_find_tip_skew_deceleration = m_parametersAxesXY->GetFieldByName( X_FIND_TIP_SKEW_DECELERATION ).GetValueAsULong();
			rc = json_object_object_add( payloadObj, X_FIND_TIP_SKEW_DECELERATION, json_object_new_uint64( x_find_tip_skew_deceleration ) );
			succeeded &= (rc == 0);

			unsigned long y_find_tip_skew_deceleration = m_parametersAxesXY->GetFieldByName( Y_FIND_TIP_SKEW_DECELERATION ).GetValueAsULong();
			rc = json_object_object_add( payloadObj, Y_FIND_TIP_SKEW_DECELERATION, json_object_new_uint64( y_find_tip_skew_deceleration ) );
			succeeded &= (rc == 0);

			if (tip_axis == ComponentType::ZAxis)
			{
				long xx_z_tip_to_tip_skew_position = m_parametersAxisXX->GetFieldByName( XX_Z_TIP_TO_TIP_SKEW_POSITION ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, XX_Z_TIP_TO_TIP_SKEW_POSITION, json_object_new_int64( xx_z_tip_to_tip_skew_position ) );
				succeeded &= (rc == 0);

				long z_to_tip_skew_sensor_height = m_parametersAxisZ->GetFieldByName( Z_TO_TIP_SKEW_SENSOR_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, Z_TO_TIP_SKEW_SENSOR_HEIGHT, json_object_new_int64( z_to_tip_skew_sensor_height ) );
				succeeded &= (rc == 0);

				long tip_skew_safety_gap = m_parametersAxisZ->GetFieldByName( TIP_SKEW_SAFETY_GAP ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_SKEW_SAFETY_GAP, json_object_new_int64( tip_skew_safety_gap ) );
				succeeded &= (rc == 0);

				unsigned long z_y_offset_from_xx_axis = m_parametersAxisZ->GetFieldByName( Z_Y_OFFSET_FROM_XX_AXIS ).GetValueAsULong();
				rc = json_object_object_add( payloadObj, Z_Y_OFFSET_FROM_XX_AXIS, json_object_new_uint64( z_y_offset_from_xx_axis ) );
				succeeded &= (rc == 0);

				unsigned long x_z_tip_skew_base_position = m_parametersAxesXY->GetFieldByName( X_Z_TIP_SKEW_BASE_POSITION ).GetValueAsULong();
				rc = json_object_object_add( payloadObj, X_Z_TIP_SKEW_BASE_POSITION, json_object_new_uint64( x_z_tip_skew_base_position ) );
				succeeded &= (rc == 0);

				unsigned long y_z_tip_skew_base_position = m_parametersAxesXY->GetFieldByName( Y_Z_TIP_SKEW_BASE_POSITION ).GetValueAsULong();
				rc = json_object_object_add( payloadObj, Y_Z_TIP_SKEW_BASE_POSITION, json_object_new_uint64( y_z_tip_skew_base_position ) );
				succeeded &= (rc == 0);
			}
			else if (tip_axis == ComponentType::ZZAxis)
			{
				long xx_zz_tip_to_tip_skew_position = m_parametersAxisXX->GetFieldByName( XX_ZZ_TIP_TO_TIP_SKEW_POSITION ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, XX_ZZ_TIP_TO_TIP_SKEW_POSITION, json_object_new_int64( xx_zz_tip_to_tip_skew_position ) );
				succeeded &= (rc == 0);

				long zz_to_tip_skew_sensor_height = m_parametersAxisZZ->GetFieldByName( ZZ_TO_TIP_SKEW_SENSOR_HEIGHT ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, ZZ_TO_TIP_SKEW_SENSOR_HEIGHT, json_object_new_int64( zz_to_tip_skew_sensor_height ) );
				succeeded &= (rc == 0);

				long tip_skew_safety_gap = m_parametersAxisZZ->GetFieldByName( TIP_SKEW_SAFETY_GAP ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, TIP_SKEW_SAFETY_GAP, json_object_new_int64( tip_skew_safety_gap ) );
				succeeded &= (rc == 0);

				unsigned long zz_y_offset_from_xx_axis = m_parametersAxisZZ->GetFieldByName( ZZ_Y_OFFSET_FROM_XX_AXIS ).GetValueAsULong();
				rc = json_object_object_add( payloadObj, ZZ_Y_OFFSET_FROM_XX_AXIS, json_object_new_uint64( zz_y_offset_from_xx_axis ) );
				succeeded &= (rc == 0);

				unsigned long x_zz_tip_skew_base_position = m_parametersAxesXY->GetFieldByName( X_ZZ_TIP_SKEW_BASE_POSITION ).GetValueAsULong();
				rc = json_object_object_add( payloadObj, X_ZZ_TIP_SKEW_BASE_POSITION, json_object_new_uint64( x_zz_tip_skew_base_position ) );
				succeeded &= (rc == 0);

				unsigned long y_zz_tip_skew_base_position = m_parametersAxesXY->GetFieldByName( Y_ZZ_TIP_SKEW_BASE_POSITION ).GetValueAsULong();
				rc = json_object_object_add( payloadObj, Y_ZZ_TIP_SKEW_BASE_POSITION, json_object_new_uint64( y_zz_tip_skew_base_position ) );
				succeeded &= (rc == 0);
			}
			else
			{
				succeeded = false;
			}
		}
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "xx", m_parametersAxisXX );
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zzz", m_parametersAxisZZZ );
		if (DesHelper::AddXYSpeeds( m_threadName, payloadObj, m_parametersAxesXY ))
		{
			json_object * xySpeedsObj = json_object_object_get( payloadObj, XY_SPEEDS );
			unsigned long x_find_tip_skew_speed = m_parametersAxesXY->GetFieldByName( X_FIND_TIP_SKEW_SPEED ).GetValueAsULong();
			if (x_find_tip_skew_speed != m_parametersAxesXY->GetFieldByName( Y_FIND_TIP_SKEW_SPEED ).GetValueAsULong())
			{
				LOG_WARN( m_threadName, "xy", "X and Y find_tip_skew_speeds don't match. Defaulting to X value" );
			}
			int rc = json_object_object_add( xySpeedsObj, X_FIND_TIP_SKEW_SPEED, json_object_new_int64( x_find_tip_skew_speed ) );
			succeeded &= (rc == 0);
		}
		else
		{
			succeeded = false;
		}
	}
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

// TODO: Implement decomposition.
bool CommandInterpreterThread::MeteOut_SetWorkingTip( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	/***
	Moves the XX axis to place the specified tip over the working tip location
	
	A.	MoveAllTipsToSafeHeight()
	B.	MoveToXXStep(<tip_axis_working_location_offset>)
	***/

	bool succeeded = false;
	ComponentType tip_axis = ComponentType::ComponentUnknown;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Read the tip_axis value from the payload to learn which axis to draw from.
		json_object * tipAxisObj = json_object_object_get( payloadObj, TIP_AXIS );
		if (tipAxisObj)
		{
			string tipAxisStr = json_object_get_string( tipAxisObj );
			tip_axis = StringNameToComponentTypeEnum( tipAxisStr.c_str() );
			succeeded = ((tip_axis == ComponentType::ZAxis) || (tip_axis == ComponentType::ZZAxis) || (tip_axis == ComponentType::ZZZAxis));
		}
		if (tip_axis == ComponentType::ZAxis)
		{
			long xx_z_tip_to_tip_working_position = m_parametersAxisXX->GetFieldByName( XX_Z_TIP_TO_TIP_WORKING_POSITION ).GetValueAsLong();
			int rc = json_object_object_add( payloadObj, XX_Z_TIP_TO_TIP_WORKING_POSITION, json_object_new_int64( xx_z_tip_to_tip_working_position ) );
			succeeded &= (rc == 0);
		}
		else if (tip_axis == ComponentType::ZZAxis)
		{
			long xx_zz_tip_to_tip_working_position = m_parametersAxisXX->GetFieldByName( XX_ZZ_TIP_TO_TIP_WORKING_POSITION ).GetValueAsLong();
			int rc = json_object_object_add( payloadObj, XX_ZZ_TIP_TO_TIP_WORKING_POSITION, json_object_new_int64( xx_zz_tip_to_tip_working_position ) );
			succeeded &= (rc == 0);
		}
		else if (tip_axis == ComponentType::ZZZAxis)
		{
			long xx_zzz_tip_to_tip_working_position = m_parametersAxisXX->GetFieldByName( XX_ZZZ_TIP_TO_TIP_WORKING_POSITION ).GetValueAsLong();
			int rc = json_object_object_add( payloadObj, XX_ZZZ_TIP_TO_TIP_WORKING_POSITION, json_object_new_int64( xx_zzz_tip_to_tip_working_position ) );
			succeeded &= (rc == 0);
		}
		long z_safe_height = m_parametersAxisZ->GetFieldByName( Z_SAFE_HEIGHT ).GetValueAsLong();
		int rc = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, json_object_new_int64( z_safe_height ) );
		succeeded &= (rc == 0);

		long zz_safe_height = m_parametersAxisZZ->GetFieldByName( ZZ_SAFE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, json_object_new_int64( zz_safe_height ) );
		succeeded &= (rc == 0);

		long zzz_safe_height = m_parametersAxisZZZ->GetFieldByName( ZZZ_SAFE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZZ_SAFE_HEIGHT, json_object_new_int64( zzz_safe_height ) );
		succeeded &= (rc == 0);

		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "xx", m_parametersAxisXX );
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zzz", m_parametersAxisZZZ );
	}
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

// TODO: Implement decomposition.
bool CommandInterpreterThread::MeteOut_RemoveTip( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	/***
	Removes the specified tip from its tip axis.
	
	A.	MoveAllTipsToSafeHeight()
	B.	a-MoveToYYStep(<YYAxis.yy_key_to_tip_path>)
	C.	If (<tip_axis> = Z) Then 
		a.	a-MoveToXXNM(XXAxis.xx_z_tip_to_key_entry_position)
		b.	MoveToZNM(ZAxis.z_through_key_height – (tip_total_length – tip_working_length) – Zaxis.tip_removal_slowdown_offset)
		c.	MoveToXXNM(XXAxis.xx_z_tip_to_key_work_position) 
		d.	*Set the ZAxis speet to ZAxis.tip_seating_speed
		e.	MoveToZNM(ZAxis.z_to_top_of_seating_key_height)
		f.	*Set the ZAxis speed to ZAxis.set_speed
		g.	End If
	D.	If (<tip_axis> = ZZ) Then 
		a.	a-MoveToXXNM(XXAxis.xx_zz_tip_to_key_entry_position)
		b.	MoveToZZNM(ZZAxis.zz_through_key_height – (tip_total_length – tip_working_length) – ZZaxis.tip_removal_slowdown_offset)
		c.	MoveToXXNM(XXAxis,xx_zz_tip_to_key_work_position) 
		d.	*Set the ZZAxis speet to ZZAxis.tip_seating_speed
		e.	MoveToZZNM(ZZAxis.zz_to_top_of_seating_key_height)
		f.	*Set the ZZAxis speed to ZZAxis.set_speed
		g.	End If
	E.	MoveAllTipsToSafeHeight()
	***/

	bool succeeded = false;
	ComponentType tip_axis = ComponentType::ComponentUnknown;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Read the tip_axis value from the payload to learn which axis to draw from.
		json_object * tipAxisObj = json_object_object_get( payloadObj, TIP_AXIS );
		if (tipAxisObj)
		{
			string tipAxisStr = json_object_get_string( tipAxisObj );
			tip_axis = StringNameToComponentTypeEnum( tipAxisStr.c_str() );
			succeeded = ((tip_axis == ComponentType::ZAxis) || (tip_axis == ComponentType::ZZAxis));
		}
		if (tip_axis == ComponentType::ZAxis)
		{
			unsigned long z_y_offset_from_xx_axis = m_parametersAxisZ->GetFieldByName( Z_Y_OFFSET_FROM_XX_AXIS ).GetValueAsULong();
			int rc = json_object_object_add( payloadObj, Z_Y_OFFSET_FROM_XX_AXIS, json_object_new_uint64( z_y_offset_from_xx_axis ) );
			succeeded &= (rc == 0);

			long xx_z_tip_to_key_entry_position = m_parametersAxisXX->GetFieldByName( XX_Z_TIP_TO_KEY_ENTRY_POSITION ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, XX_Z_TIP_TO_KEY_ENTRY_POSITION, json_object_new_int64( xx_z_tip_to_key_entry_position ) );
			succeeded &= (rc == 0);

			long z_through_key_height = m_parametersAxisZ->GetFieldByName( Z_THROUGH_KEY_HEIGHT ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, Z_THROUGH_KEY_HEIGHT, json_object_new_int64( z_through_key_height ) );
			succeeded &= (rc == 0);

			long tip_removal_slowdown_offset = m_parametersAxisZ->GetFieldByName( TIP_REMOVAL_SLOWDOWN_OFFSET ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, TIP_REMOVAL_SLOWDOWN_OFFSET, json_object_new_int64( tip_removal_slowdown_offset ) );
			succeeded &= (rc == 0);

			long z_to_top_of_seating_key_height = m_parametersAxisZ->GetFieldByName( Z_TO_TOP_OF_SEATING_KEY_HEIGHT ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, Z_TO_TOP_OF_SEATING_KEY_HEIGHT, json_object_new_int64( z_to_top_of_seating_key_height ) );
			succeeded &= (rc == 0);
			
			std::string name = PUMP_VENDOR;
			std::string pumpVendor = m_parametersPickingPump1->GetFieldByName( name ).setting;
			rc = json_object_object_add( payloadObj, PUMP_VENDOR, json_object_new_string( pumpVendor.c_str() ) );
			succeeded &= (rc == 0);

			name = PUMP_MODEL;
			std::string pumpModel = m_parametersPickingPump1->GetFieldByName( name ).setting;
			rc = json_object_object_add( payloadObj, PUMP_MODEL, json_object_new_string( pumpModel.c_str() ) );
			succeeded &= (rc == 0);

		}
		else if (tip_axis == ComponentType::ZZAxis)
		{
			unsigned long zz_y_offset_from_xx_axis = m_parametersAxisZZ->GetFieldByName( ZZ_Y_OFFSET_FROM_XX_AXIS ).GetValueAsULong();
			int rc = json_object_object_add( payloadObj, ZZ_Y_OFFSET_FROM_XX_AXIS, json_object_new_uint64( zz_y_offset_from_xx_axis ) );
			succeeded &= (rc == 0);

			long xx_zz_tip_to_key_entry_position = m_parametersAxisXX->GetFieldByName( XX_ZZ_TIP_TO_KEY_ENTRY_POSITION ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, XX_ZZ_TIP_TO_KEY_ENTRY_POSITION, json_object_new_int64( xx_zz_tip_to_key_entry_position ) );
			succeeded &= (rc == 0);

			long zz_through_key_height = m_parametersAxisZZ->GetFieldByName( ZZ_THROUGH_KEY_HEIGHT ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, ZZ_THROUGH_KEY_HEIGHT, json_object_new_int64( zz_through_key_height ) );
			succeeded &= (rc == 0);

			long tip_removal_slowdown_offset = m_parametersAxisZZ->GetFieldByName( TIP_REMOVAL_SLOWDOWN_OFFSET ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, TIP_REMOVAL_SLOWDOWN_OFFSET, json_object_new_int64( tip_removal_slowdown_offset ) );
			succeeded &= (rc == 0);

			long xx_zz_tip_to_key_work_position = m_parametersAxisXX->GetFieldByName( XX_ZZ_TIP_TO_KEY_WORK_POSITION ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, XX_ZZ_TIP_TO_KEY_WORK_POSITION, json_object_new_int64( xx_zz_tip_to_key_work_position ) );
			succeeded &= (rc == 0);

			unsigned long tip_seating_speed = m_parametersAxisZZ->GetFieldByName( TIP_SEATING_SPEED ).GetValueAsULong();
			rc = json_object_object_add( payloadObj, TIP_SEATING_SPEED, json_object_new_uint64( tip_seating_speed ) );
			succeeded &= (rc == 0);

			long zz_to_top_of_seating_key_height = m_parametersAxisZZ->GetFieldByName( ZZ_TO_TOP_OF_SEATING_KEY_HEIGHT ).GetValueAsLong();
			rc = json_object_object_add( payloadObj, ZZ_TO_TOP_OF_SEATING_KEY_HEIGHT, json_object_new_int64( zz_to_top_of_seating_key_height ) );
			succeeded &= (rc == 0);
		}
		long yy_keyToTipPath = m_parametersAxisYY->GetFieldByName( YY_KEY_TO_TIP_PATH ).GetValueAsLong();
		int rc = json_object_object_add( payloadObj, YY_KEY_TO_TIP_PATH, json_object_new_int64( yy_keyToTipPath ) );
		succeeded &= (rc == 0);

		long z_safe_height = m_parametersAxisZ->GetFieldByName( Z_SAFE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, json_object_new_int64( z_safe_height ) );
		succeeded &= (rc == 0);

		long zz_safe_height = m_parametersAxisZZ->GetFieldByName( ZZ_SAFE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, json_object_new_int64( zz_safe_height ) );
		succeeded &= (rc == 0);

		long zzz_safe_height = m_parametersAxisZZZ->GetFieldByName( ZZZ_SAFE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZZ_SAFE_HEIGHT, json_object_new_int64( zzz_safe_height ) );
		succeeded &= (rc == 0);

		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "xx", m_parametersAxisXX );
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "yy", m_parametersAxisYY );
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zzz", m_parametersAxisZZZ );
	}
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_SetOpticalColumnTurretPosition( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/***
	Sets the optical column’s turret position
	
	A.	MoveToFocusStep([safe magnification change height])
	B.	RotateTurretToPosition(<turret_position>)
	C.	Do NOT move focus again. This is a change to the spec.
	***/
	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		unsigned long safe_magnification_change_height = m_parametersOpticalColumn->GetFieldByName( SAFE_MAGNIFICATION_CHANGE_HEIGHT ).GetValueAsULong();
		int rc = json_object_object_add( payloadObj, SAFE_MAGNIFICATION_CHANGE_HEIGHT, json_object_new_int64( safe_magnification_change_height ) );
		succeeded &= (rc == 0);
	}
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForOptics->PushMsg(workMsg);
	return succeeded;
}

// TODO: Implement decomposition.
bool CommandInterpreterThread::MeteOut_MoveToZLowLimit( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	m_outBoxForCxD->PushMsg(workMsg);
	return false;
}

// TODO: Implement decomposition.
bool CommandInterpreterThread::MeteOut_MoveToZHighLimit( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	m_outBoxForCxD->PushMsg(workMsg);
	return false;
}

bool CommandInterpreterThread::MeteOut_MoveZToSafeHeight( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (!payloadObj) /// Either it was NULL or both the "payload" key and object are missing.
	{
		payloadObj = json_object_new_object();
		/// Add this empty object. This incurs loss of ownership, meaning someone else will delete it, later.
		int rc = json_object_object_add( jWorkOrderObj, PAYLOAD, payloadObj );
		succeeded &= (rc == 0);
	}
	if (payloadObj)
	{
		/// Stuff some extra json_objects in the payload.
		long z_safeHeight_nm = m_parametersAxisZ->GetFieldByName( Z_SAFE_HEIGHT ).GetValueAsLong();
		int rc = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, json_object_new_int64( z_safeHeight_nm ) );
		succeeded &= (rc == 0);
/*
		unsigned long z_set_speed = m_parametersAxisZ->GetFieldByName( Z_SET_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, Z_SET_SPEED, json_object_new_uint64( z_set_speed ) );
		succeeded &= (rc == 0);
*/
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );
	}
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveToZAxisForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		Moves the Z axis down until it either senses a specified force on its axis force sensor or reaches a specified offset.
		Uses the ZAxis.tip_pickup_force and ZAxis.z_to_top_of_yy_stage_height + tip_rack_seat_to_top_of_tip -
		(tip_total_length - tip_working_length) - ZAxis.tip_pickup_overshoot height.
	**/

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Stuff some extra json_objects in the payload.
		unsigned long tip_pickup_speed = m_parametersAxisZ->GetFieldByName( TIP_PICKUP_SPEED ).GetValueAsULong();
		int rc = json_object_object_add( payloadObj, TIP_PICKUP_SPEED, json_object_new_uint64( tip_pickup_speed ) );
		succeeded &= (rc == 0);

		double tip_pickup_force = m_parametersAxisZ->GetFieldByName( TIP_PICKUP_FORCE ).GetValueAsFloat();
		rc = json_object_object_add( payloadObj, TIP_PICKUP_FORCE, json_object_new_double( tip_pickup_force ) );
		succeeded &= (rc == 0);

		long tip_pickup_overshoot = m_parametersAxisZ->GetFieldByName( TIP_PICKUP_OVERSHOOT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, TIP_PICKUP_OVERSHOOT, json_object_new_int64( tip_pickup_overshoot ) );
		succeeded &= (rc == 0);

		long z_to_top_of_yy_stage_height = m_parametersAxisZ->GetFieldByName( Z_TO_TOP_OF_YY_STAGE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, Z_TO_TOP_OF_YY_STAGE_HEIGHT, json_object_new_int64( z_to_top_of_yy_stage_height ) );
		succeeded &= (rc == 0);

		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );
		if (!succeeded)
		{
			LOG_ERROR( m_threadName, "z", "Failed to pack payload" );
		}
	}

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveToZAxisSeatingForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		Moves the Z axis down until it either senses a specified force while seating the tip or reaches a specified offset.
		Uses the Z axis tip_seating_force and ZAxis.z_to_top_of_seating_key_height +
		tip_total_length - tip_working_length - Zaxis.tip_seating_overshoot height.
	**/

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Stuff some extra json_objects in the payload.
		double tip_seating_force = m_parametersAxisZ->GetFieldByName( TIP_SEATING_FORCE ).GetValueAsFloat();
		int rc = json_object_object_add( payloadObj, TIP_SEATING_FORCE, json_object_new_double( tip_seating_force ) );
		succeeded &= (rc == 0);
		
		unsigned long tip_seating_speed = m_parametersAxisZ->GetFieldByName( TIP_SEATING_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, TIP_SEATING_SPEED, json_object_new_uint64( tip_seating_speed ) );
		succeeded &= (rc == 0);

		long z_to_top_of_seating_key_height = m_parametersAxisZ->GetFieldByName( Z_TO_TOP_OF_SEATING_KEY_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, Z_TO_TOP_OF_SEATING_KEY_HEIGHT, json_object_new_int64( z_to_top_of_seating_key_height ) );
		succeeded &= (rc == 0);
		
		long tip_seating_overshoot = m_parametersAxisZ->GetFieldByName( TIP_SEATING_OVERSHOOT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, TIP_SEATING_OVERSHOOT, json_object_new_int64( tip_seating_overshoot ) );
		succeeded &= (rc == 0);

		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );
	}

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveToZAxisWellBottomForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		Moves the Z axis down until it either senses a specified force while finding the well bottom or reaches a specified offset.
		Uses the Z axis tip find well bottom force and the payload's "error_height" properties.
	**/

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Stuff some extra json_objects in the payload.
		double tip_find_well_bottom_force = m_parametersAxisZ->GetFieldByName( TIP_FIND_WELL_BOTTOM_FORCE ).GetValueAsFloat();
		int rc = json_object_object_add( payloadObj, TIP_FIND_WELL_BOTTOM_FORCE, json_object_new_double( tip_find_well_bottom_force ) );
		succeeded &= (rc == 0);

		unsigned long tip_find_well_bottom_speed = m_parametersAxisZ->GetFieldByName( TIP_FIND_WELL_BOTTOM_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, TIP_FIND_WELL_BOTTOM_SPEED, json_object_new_uint64( tip_find_well_bottom_speed ) );
		succeeded &= (rc == 0);

		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );
	}

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveToZTipPickForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/**
		Moves the Z tip down until it experiences a specified force on the axis or reaches the Z tip pickup height property.
		If it reaches the Z tip pickup height property before experiencing the force, an error is generated.
		
		A.	MoveToZNM(ZAxis.z_to_top_of_YY_stage_height + tip_rack_seat_to_top_of_tip + ZAxis.tip_pickup_slowdown_offset)
		B.	*Set the Z axis speed to the [ZAxis.tip_pickup_speed]
		C.	MoveToZAxisForceNewton(<tip_type>)
				Moves the Z axis down until it either senses a specified force on its axis force sensor or reaches a specified offset.
				Uses the ZAxis.tip_pickup_force and ZAxis.z_to_top_of_yy_stage_height + tip_rack_seat_to_top_of_tip -
				(tip_total_length - tip_working_length) - ZAxis.tip_pickup_overshoot height.
		D.	*Set the Z axis speed to ZAxis.z_set_speed
		E.	If (MoveToZAxisForceNewton returned error) Then <throw error> End If
		F.	MoveZToSafeHeight()
		**/

		/// No need to add tip_rack_seat_to_top_of_tip, tip_total_length, or tip_working_length. They should already be present.

		/// Stuff some extra json_objects in the payload.
		long z_safeHeight_nm = m_parametersAxisZ->GetFieldByName( Z_SAFE_HEIGHT ).GetValueAsLong();
		int rc = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, json_object_new_int64( z_safeHeight_nm ) );
		succeeded &= (rc == 0);

		long z_to_top_of_YY_stage_height_nm = m_parametersAxisZ->GetFieldByName( Z_TO_TOP_OF_YY_STAGE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, Z_TO_TOP_OF_YY_STAGE_HEIGHT, json_object_new_int64( z_to_top_of_YY_stage_height_nm ) );
		succeeded &= (rc == 0);

		long tip_pickup_slowdown_offset_nm = m_parametersAxisZ->GetFieldByName( TIP_PICKUP_SLOWDOWN_OFFSET ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, TIP_PICKUP_SLOWDOWN_OFFSET, json_object_new_int64( tip_pickup_slowdown_offset_nm ) );
		succeeded &= (rc == 0);

		unsigned long tip_pickup_speed = m_parametersAxisZ->GetFieldByName( TIP_PICKUP_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, TIP_PICKUP_SPEED, json_object_new_uint64( tip_pickup_speed ) );
		succeeded &= (rc == 0);

		double tip_pickup_force = m_parametersAxisZ->GetFieldByName( TIP_PICKUP_FORCE ).GetValueAsFloat();
		rc = json_object_object_add( payloadObj, TIP_PICKUP_FORCE, json_object_new_double( tip_pickup_force ) );
		succeeded &= (rc == 0);

		long tip_pickup_overshoot = m_parametersAxisZ->GetFieldByName( TIP_PICKUP_OVERSHOOT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, TIP_PICKUP_OVERSHOOT, json_object_new_int64( tip_pickup_overshoot ) );
		succeeded &= (rc == 0);
/*
		long z_set_speed = m_parametersAxisZ->GetFieldByName( Z_SET_SPEED ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, Z_SET_SPEED, json_object_new_int64( z_set_speed ) );
		succeeded &= (rc == 0);
*/
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );
	}

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveToZTipSeatingForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		A.	MoveToZNM(ZAxis.z_to_top_of_seating_key_height + <tip_working_length + ZAxis.tip_seating_slowdown_offset)
		B.	*Set the Z axis speed to the [ZAxis.tip_seating_speed]
		C.	MoveToZAxisSeatingForceNewton(<tip_type>)
		D.	*Set the Z axis speed to ZAxis.z_set_speed
		E.	If (MoveToZAxisSeatingForceNewton returned error) Then <throw error> End If
		F.	MoveZToSafeHeight()
	**/

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Stuff some extra json_objects in the payload.
		long tip_seating_slowdown_offset = m_parametersAxisZ->GetFieldByName( TIP_SEATING_SLOWDOWN_OFFSET ).GetValueAsLong();
		int rc = json_object_object_add( payloadObj, TIP_SEATING_SLOWDOWN_OFFSET, json_object_new_int64( tip_seating_slowdown_offset ) );
		succeeded &= (rc == 0);

		unsigned long tip_seating_speed = m_parametersAxisZ->GetFieldByName( TIP_SEATING_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, TIP_SEATING_SPEED, json_object_new_uint64( tip_seating_speed ) );
		succeeded &= (rc == 0);
/*
		unsigned long z_set_speed = m_parametersAxisZ->GetFieldByName( Z_SET_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, Z_SET_SPEED, json_object_new_uint64( z_set_speed ) );
		succeeded &= (rc == 0);
*/
		double tip_seating_force = m_parametersAxisZ->GetFieldByName( TIP_SEATING_FORCE ).GetValueAsFloat();
		rc = json_object_object_add( payloadObj, TIP_SEATING_FORCE, json_object_new_double( tip_seating_force ) );
		succeeded &= (rc == 0);
		
		long z_to_top_of_seating_key_height = m_parametersAxisZ->GetFieldByName( Z_TO_TOP_OF_SEATING_KEY_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, Z_TO_TOP_OF_SEATING_KEY_HEIGHT, json_object_new_int64( z_to_top_of_seating_key_height ) );
		succeeded &= (rc == 0);
		
		long tip_seating_overshoot = m_parametersAxisZ->GetFieldByName( TIP_SEATING_OVERSHOOT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, TIP_SEATING_OVERSHOOT, json_object_new_int64( tip_seating_overshoot ) );
		succeeded &= (rc == 0);

		long z_safeHeight_nm = m_parametersAxisZ->GetFieldByName( Z_SAFE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, json_object_new_int64( z_safeHeight_nm ) );
		succeeded &= (rc == 0);

		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );
	}

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

// TODO: Implement decomposition.
bool CommandInterpreterThread::MeteOut_MoveToZTipWellBottomForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		Moves the Z tip down until it experiences a specified force on the axis or reaches the specified error offset.
		If it reaches the specified error offset before experiencing the force, an error is generated.

		A.	MoveToZNM(ZAxis.z_to_top_of_xy_stage_height + tip_working_length + plate_height – well_inner_height/2)
		B.	*Set the Z axis speed to the [ZAxis.tip_find_well_bottom_speed]
		C.	MoveToZAxisWellBottomForceNewton(ZAxis.z_to_top_of_xy_stage_height + tip_working_length + plate_height – well_inner_height - ZAxis.tip_find_well_bottom_overshoot ))
		D.	*Set Z axis speed to ZAxis.z_set_speed
		E.	well_bottom_height_nm = GetCurrentZNM()
		F.	If (MoveToZAxisWellBottomForceNewton returned error) Then <throw error> End If
		G.	MoveZToSafeHeight()
	**/

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Stuff some extra json_objects in the payload.
		double tip_find_well_bottom_force = m_parametersAxisZ->GetFieldByName( TIP_FIND_WELL_BOTTOM_FORCE ).GetValueAsFloat();
		int rc = json_object_object_add( payloadObj, TIP_FIND_WELL_BOTTOM_FORCE, json_object_new_double( tip_find_well_bottom_force ) );
		succeeded &= (rc == 0);
		
		long z_to_top_of_xy_stage_height = m_parametersAxisZ->GetFieldByName( Z_TO_TOP_OF_XY_STAGE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, Z_TO_TOP_OF_XY_STAGE_HEIGHT, json_object_new_int64( z_to_top_of_xy_stage_height ) );
		succeeded &= (rc == 0);
		
		unsigned long tip_find_well_bottom_speed = m_parametersAxisZ->GetFieldByName( TIP_FIND_WELL_BOTTOM_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, TIP_FIND_WELL_BOTTOM_SPEED, json_object_new_uint64( tip_find_well_bottom_speed ) );
		succeeded &= (rc == 0);
		
		long tip_find_well_bottom_overshoot = m_parametersAxisZ->GetFieldByName( TIP_FIND_WELL_BOTTOM_OVERSHOOT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, TIP_FIND_WELL_BOTTOM_OVERSHOOT, json_object_new_int64( tip_find_well_bottom_overshoot ) );
		succeeded &= (rc == 0);

		long z_safe_height = m_parametersAxisZ->GetFieldByName( Z_SAFE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, Z_SAFE_HEIGHT, json_object_new_int64( z_safe_height ) );
		succeeded &= (rc == 0);

		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "z", m_parametersAxisZ );
	}

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

// TODO: Implement decomposition.
bool CommandInterpreterThread::MeteOut_MoveToZZLowLimit( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	m_outBoxForCxD->PushMsg(workMsg);
	return false;
}

// TODO: Implement decomposition.
bool CommandInterpreterThread::MeteOut_MoveToZZHighLimit( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	m_outBoxForCxD->PushMsg(workMsg);
	return false;
}

bool CommandInterpreterThread::MeteOut_MoveZZToSafeHeight( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (!payloadObj) /// Either it was NULL or both the "payload" key and object are missing.
	{
		payloadObj = json_object_new_object();
		/// Add this empty object. This incurs loss of ownership, meaning someone else will delete it, later.
		int rc = json_object_object_add( jWorkOrderObj, PAYLOAD, payloadObj );
		succeeded &= (rc == 0);
	}
	if (payloadObj)
	{
		/// Stuff some extra json_objects in the payload.
		long zz_safeHeight_nm = m_parametersAxisZZ->GetFieldByName( ZZ_SAFE_HEIGHT ).GetValueAsLong();
		int rc = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, json_object_new_int64( zz_safeHeight_nm ) );
		succeeded &= (rc == 0);
/*
		unsigned long zz_set_speed = m_parametersAxisZZ->GetFieldByName( ZZ_SET_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, ZZ_SET_SPEED, json_object_new_uint64( zz_set_speed ) );
		succeeded &= (rc == 0);
*/
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );
	}
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveToZZTipPickForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/**
		Moves the ZZ tip down until it experiences a specified force on the axis or reaches the ZZ tip pickup height property.
		If it reaches the ZZ tip pickup height property before experiencing the force, an error is generated.
		
		A.	MoveToZZNM(ZZAxis.ZZ_to_top_of_YY_stage_height + tip_rack_seat_to_top_of_tip + ZZAxis.tip_pickup_slowdown_offset )
		B.	*Set the ZZ axis speed to the [ZZAxis.tip_pickup_speed]
		C.	MoveToZZAxisForceNewton(<tip_type>)
				Moves the ZZ axis down until it either senses a specified tip pickup force on its axis force sensor or reaches a specified offset.
				Uses the ZZAxis.tip_pickup_force and ZZAxis.zz_to_top_of_yy_stage_height + tip_rack_seat_to_top_of_tip -
				(tip_total_length - tip_working_length + ZZAxis.tip_pickup_overshoot) height.
		D.	*Set the ZZ axis speed to ZZAxis.z_set_speed
		E.	If (MoveToZZAxisForceNewton ) Then <throw error> End If
		F.	MoveZZToSafeHeight()
		**/
		/// tip_rack_seat_to_top_of_tip, tip_total_length, tip_working_length should already be present.

		/// Stuff some extra json_objects in the payload.
		long zz_safeHeight_nm = m_parametersAxisZZ->GetFieldByName( ZZ_SAFE_HEIGHT ).GetValueAsLong();
		int rc = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, json_object_new_int64( zz_safeHeight_nm ) );
		succeeded &= (rc == 0);

		long zz_to_top_of_YY_stage_height_nm = m_parametersAxisZZ->GetFieldByName( ZZ_TO_TOP_OF_YY_STAGE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZ_TO_TOP_OF_YY_STAGE_HEIGHT, json_object_new_int64( zz_to_top_of_YY_stage_height_nm ) );
		succeeded &= (rc == 0);

		long tip_pickup_slowdown_offset_nm = m_parametersAxisZZ->GetFieldByName( TIP_PICKUP_SLOWDOWN_OFFSET ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, TIP_PICKUP_SLOWDOWN_OFFSET, json_object_new_int64( tip_pickup_slowdown_offset_nm ) );
		succeeded &= (rc == 0);

		unsigned long tip_pickup_speed = m_parametersAxisZZ->GetFieldByName( TIP_PICKUP_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, TIP_PICKUP_SPEED, json_object_new_uint64( tip_pickup_speed ) );
		succeeded &= (rc == 0);

		double tip_pickup_force = m_parametersAxisZZ->GetFieldByName( TIP_PICKUP_FORCE ).GetValueAsFloat();
		rc = json_object_object_add( payloadObj, TIP_PICKUP_FORCE, json_object_new_double( tip_pickup_force ) );
		succeeded &= (rc == 0);

		long tip_pickup_overshoot = m_parametersAxisZZ->GetFieldByName( TIP_PICKUP_OVERSHOOT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, TIP_PICKUP_OVERSHOOT, json_object_new_int64( tip_pickup_overshoot ) );
		succeeded &= (rc == 0);
/*
		long zz_set_speed = m_parametersAxisZZ->GetFieldByName( ZZ_SET_SPEED ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZ_SET_SPEED, json_object_new_int64( zz_set_speed ) );
		succeeded &= (rc == 0);
*/
		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ ); /// For adjacent moves to safe height.
	}

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveToZZTipSeatingForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		Moves the ZZ tip down until it experiences a specified force on the axis or reaches the ZZ tip seating height offset property.
		If it reaches the ZZ seating height offset property before experiencing the force, an error is generated.
		
		A.	MoveToZZNM(ZZAxis.ZZ_to_top_of_seating_key_height + tip_working_length + ZZAxis.tip_seating_slowdown_offset)
		B.	*Set the ZZ axis speed to the [ZZAxis.tip_seating_speed]
		C.	MoveToZZAxisSeatingForceNewton(<tip_type>)
		D.	*Set the ZZ axis speed to ZZAxis.z_set_speed
		E.	If (MoveToZZAxisSeatingForceNewton returned error) Then <throw error> End If
		F.	MoveZZToSafeHeight()
	**/

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Stuff some extra json_objects in the payload.
		long tip_seating_slowdown_offset = m_parametersAxisZZ->GetFieldByName( TIP_SEATING_SLOWDOWN_OFFSET ).GetValueAsLong();
		{
			stringstream msg;
			msg << TIP_SEATING_SLOWDOWN_OFFSET << "=" << tip_seating_slowdown_offset;
			LOG_TRACE( m_threadName, "packing", msg.str().c_str() );
		}
		int rc = json_object_object_add( payloadObj, TIP_SEATING_SLOWDOWN_OFFSET, json_object_new_int64( tip_seating_slowdown_offset ) );
		if (rc != 0) cout << m_threadName << ": ERROR! Failed to add " << TIP_SEATING_SLOWDOWN_OFFSET << endl;
		succeeded &= (rc == 0);

		unsigned long tip_seating_speed = m_parametersAxisZZ->GetFieldByName( TIP_SEATING_SPEED ).GetValueAsULong();
		{
			stringstream msg;
			msg << TIP_SEATING_SPEED << "=" << tip_seating_speed;
			LOG_TRACE( m_threadName, "packing", msg.str().c_str() );
		}
		rc = json_object_object_add( payloadObj, TIP_SEATING_SPEED, json_object_new_uint64( tip_seating_speed ) );
		if (rc != 0) cout << m_threadName << ": ERROR! Failed to get " << TIP_SEATING_SPEED << endl;
		succeeded &= (rc == 0);
/*
		unsigned long zz_set_speed = m_parametersAxisZZ->GetFieldByName( ZZ_SET_SPEED ).GetValueAsULong();
		{
			stringstream msg;
			msg << ZZ_SET_SPEED << "=" << zz_set_speed;
			LOG_TRACE( m_threadName, "packing", msg.str().c_str() );
		}
		rc = json_object_object_add( payloadObj, ZZ_SET_SPEED, json_object_new_uint64( zz_set_speed ) );
		if (rc != 0) cout << m_threadName << ": ERROR! Failed to get " << ZZ_SET_SPEED << endl;
		succeeded &= (rc == 0);
*/
		double tip_seating_force = m_parametersAxisZZ->GetFieldByName( TIP_SEATING_FORCE ).GetValueAsFloat();
		{
			stringstream msg;
			msg << TIP_SEATING_FORCE << "=" << tip_seating_force;
			LOG_TRACE( m_threadName, "packing", msg.str().c_str() );
		}
		json_object * doubleObj = json_object_new_double( tip_seating_force );
		if (doubleObj)
		{
			rc = json_object_object_add( payloadObj, TIP_SEATING_FORCE, doubleObj );
			if (rc != 0) cout << m_threadName << ": ERROR! Failed to get " << TIP_SEATING_FORCE << endl;
			succeeded &= (rc == 0);
		}
		else
		{
			LOG_ERROR( m_threadName, "packing", "json_object_new_double() returned NULL" );
			succeeded &= false;
		}
		
		long zz_to_top_of_seating_key_height = m_parametersAxisZZ->GetFieldByName( ZZ_TO_TOP_OF_SEATING_KEY_HEIGHT ).GetValueAsLong();
		{
			stringstream msg;
			msg << ZZ_TO_TOP_OF_SEATING_KEY_HEIGHT << "=" << zz_to_top_of_seating_key_height;
			LOG_TRACE( m_threadName, "packing", msg.str().c_str() );
		}
		rc = json_object_object_add( payloadObj, ZZ_TO_TOP_OF_SEATING_KEY_HEIGHT, json_object_new_int64( zz_to_top_of_seating_key_height ) );
		if (rc != 0) cout << m_threadName << ": ERROR! Failed to get " << ZZ_TO_TOP_OF_SEATING_KEY_HEIGHT << endl;
		succeeded &= (rc == 0);
		
		long tip_seating_overshoot = m_parametersAxisZZ->GetFieldByName( TIP_SEATING_OVERSHOOT ).GetValueAsLong();
		{
			stringstream msg;
			msg << TIP_SEATING_OVERSHOOT << "=" << tip_seating_overshoot;
			LOG_TRACE( m_threadName, "packing", msg.str().c_str() );
		}
		rc = json_object_object_add( payloadObj, TIP_SEATING_OVERSHOOT, json_object_new_int64( tip_seating_overshoot ) );
		if (rc != 0) cout << m_threadName << ": ERROR! Failed to get " << TIP_SEATING_OVERSHOOT << endl;
		succeeded &= (rc == 0);

		long zz_safeHeight_nm = m_parametersAxisZZ->GetFieldByName( ZZ_SAFE_HEIGHT ).GetValueAsLong();
		{
			stringstream msg;
			msg << ZZ_SAFE_HEIGHT << "=" << zz_safeHeight_nm;
			LOG_TRACE( m_threadName, "packing", msg.str().c_str() );
		}
		rc = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, json_object_new_int64( zz_safeHeight_nm ) );
		if (rc != 0) cout << m_threadName << ": ERROR! Failed to get " << ZZ_SAFE_HEIGHT << endl;
		succeeded &= (rc == 0);

		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );
	}

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveToZZTipWellBottomForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		Moves the ZZ tip down until it experiences a specified force on the axis or reaches the specified error offset.
		If it reaches the specified error offset before experiencing the force, an error is generated.

		A.	MoveToZZNM(ZZAxis.zz_to_top_of_xy_stage_height + tip_working_length + plate_height – well_inner_height/2)
		B.	*Set the ZZ axis speed to the [ZZAxis.tip_find_well_bottom_speed]
		C.	MoveToZZAxisWellBottomForceNewton(ZZAxis.zz_to_top_of_xy_stage_height + tip_working_length + plate_height – well_inner_height - ZZAxis.tip_find_well_bottom_overshoot ))
		D.	*Set ZZ axis speed to ZZAxis.zz_set_speed
		E.	well_bottom_height_nm = GetCurrentZZNM()
		F.	If (MoveToZZAxisWellBottomForceNewton returned error) Then <throw error> End If
		G.	MoveZZToSafeHeight()
	**/

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Stuff some extra json_objects in the payload.
		double tip_find_well_bottom_force = m_parametersAxisZZ->GetFieldByName( TIP_FIND_WELL_BOTTOM_FORCE ).GetValueAsFloat();
		int rc = json_object_object_add( payloadObj, TIP_FIND_WELL_BOTTOM_FORCE, json_object_new_double( tip_find_well_bottom_force ) );
		succeeded &= (rc == 0);
		
		long zz_to_top_of_xy_stage_height = m_parametersAxisZZ->GetFieldByName( ZZ_TO_TOP_OF_XY_STAGE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZ_TO_TOP_OF_XY_STAGE_HEIGHT, json_object_new_int64( zz_to_top_of_xy_stage_height ) );
		succeeded &= (rc == 0);
		
		unsigned long tip_find_well_bottom_speed = m_parametersAxisZZ->GetFieldByName( TIP_FIND_WELL_BOTTOM_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, TIP_FIND_WELL_BOTTOM_SPEED, json_object_new_uint64( tip_find_well_bottom_speed ) );
		succeeded &= (rc == 0);
		
		long tip_find_well_bottom_overshoot = m_parametersAxisZZ->GetFieldByName( TIP_FIND_WELL_BOTTOM_OVERSHOOT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, TIP_FIND_WELL_BOTTOM_OVERSHOOT, json_object_new_int64( tip_find_well_bottom_overshoot ) );
		succeeded &= (rc == 0);
/*
		unsigned long zz_set_speed = m_parametersAxisZZ->GetFieldByName( ZZ_SET_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, ZZ_SET_SPEED, json_object_new_uint64( zz_set_speed ) );
		succeeded &= (rc == 0);
*/
		long zz_safe_height = m_parametersAxisZZ->GetFieldByName( ZZ_SAFE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZ_SAFE_HEIGHT, json_object_new_int64( zz_safe_height ) );
		succeeded &= (rc == 0);

		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );
	}

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}


bool CommandInterpreterThread::MeteOut_MoveToZZAxisForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		Moves the ZZ axis down until it either senses a specified tip pickup force on its axis force sensor or reaches a specified offset.
		Uses the ZZAxis.tip_pickup_force and ZZAxis.zz_to_top_of_yy_stage_height + tip_rack_seat_to_top_of_tip -
		(tip_total_length - tip_working_length + ZZAxis.tip_pickup_overshoot) height.
	**/

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Stuff some extra json_objects in the payload.
		unsigned long tip_pickup_speed = m_parametersAxisZZ->GetFieldByName( TIP_PICKUP_SPEED ).GetValueAsULong();
		int rc = json_object_object_add( payloadObj, TIP_PICKUP_SPEED, json_object_new_uint64( tip_pickup_speed ) );
		succeeded &= (rc == 0);

		double tip_pickup_force = m_parametersAxisZZ->GetFieldByName( TIP_PICKUP_FORCE ).GetValueAsFloat();
		rc = json_object_object_add( payloadObj, TIP_PICKUP_FORCE, json_object_new_double( tip_pickup_force ) );
		succeeded &= (rc == 0);

		long tip_pickup_overshoot = m_parametersAxisZZ->GetFieldByName( TIP_PICKUP_OVERSHOOT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, TIP_PICKUP_OVERSHOOT, json_object_new_int64( tip_pickup_overshoot ) );
		succeeded &= (rc == 0);

		long zz_to_top_of_yy_stage_height = m_parametersAxisZZ->GetFieldByName( ZZ_TO_TOP_OF_YY_STAGE_HEIGHT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, ZZ_TO_TOP_OF_YY_STAGE_HEIGHT, json_object_new_int64( zz_to_top_of_yy_stage_height ) );
		succeeded &= (rc == 0);

		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );
		if (!succeeded)
		{
			LOG_ERROR( m_threadName, "zz", "Failed to pack payload" );
		}
	}

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

/// Here's what let's do. Let's try taking a command that has a tip_type in the payload and add a couple ZZAxis values to the payload and then just forward it on.
/// Make sure all the needed data is there, but let the Doler break it down.
bool CommandInterpreterThread::MeteOut_MoveToZZAxisSeatingForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		Moves the ZZ axis down until it either senses a specified force while seating the tip or reaches a specified offset.
		Uses the zz axis tip_seating_force and ZZAxis.zz_to_top_of_seating_key_height +
		tip_total_length - tip_working_length - ZZaxis.tip_seating_overshoot height.
	**/
	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Stuff some extra json_objects in the payload.
		long zz_to_top_of_seating_key_height = m_parametersAxisZZ->GetFieldByName( ZZ_TO_TOP_OF_SEATING_KEY_HEIGHT ).GetValueAsLong();
		int rc = json_object_object_add( payloadObj, ZZ_TO_TOP_OF_SEATING_KEY_HEIGHT, json_object_new_int64( zz_to_top_of_seating_key_height ) );
		succeeded &= (rc == 0);

		unsigned long tip_seating_speed = m_parametersAxisZZ->GetFieldByName( TIP_SEATING_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, TIP_SEATING_SPEED, json_object_new_uint64( tip_seating_speed ) );
		succeeded &= (rc == 0);

		double tip_seating_force = m_parametersAxisZZ->GetFieldByName( TIP_SEATING_FORCE ).GetValueAsFloat();
		rc = json_object_object_add( payloadObj, TIP_SEATING_FORCE, json_object_new_double( tip_seating_force ) );
		succeeded &= (rc == 0);

		long tip_seating_overshoot = m_parametersAxisZZ->GetFieldByName( TIP_SEATING_OVERSHOOT ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, TIP_SEATING_OVERSHOOT, json_object_new_int64( tip_seating_overshoot ) );
		succeeded &= (rc == 0);

		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );
	}
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveToZZAxisWellBottomForceNewton( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		Moves the ZZ axis down until it either senses a specified force while finding the well bottom or reaches a specified offset.
		Uses the ZZ axis tip find well bottom force and the payload's "error_height" properties.
	**/

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Stuff some extra json_objects in the payload.
		double tip_find_well_bottom_force = m_parametersAxisZZ->GetFieldByName( TIP_FIND_WELL_BOTTOM_FORCE ).GetValueAsFloat();
		int rc = json_object_object_add( payloadObj, TIP_FIND_WELL_BOTTOM_FORCE, json_object_new_double( tip_find_well_bottom_force ) );
		succeeded &= (rc == 0);

		unsigned long tip_find_well_bottom_speed = m_parametersAxisZZ->GetFieldByName( TIP_FIND_WELL_BOTTOM_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, TIP_FIND_WELL_BOTTOM_SPEED, json_object_new_uint64( tip_find_well_bottom_speed ) );
		succeeded &= (rc == 0);

		succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "zz", m_parametersAxisZZ );
	}

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_moveToXYNm( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	succeeded &= DesHelper::AddXYSpeeds( m_threadName, payloadObj, m_parametersAxesXY );
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );

	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveXToTipSkewTrigger( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Get needed property values and add them to the payload.
		long x_find_tip_skew_travel_distance = m_parametersAxesXY->GetFieldByName( X_FIND_TIP_SKEW_TRAVEL_DISTANCE ).GetValueAsLong();
		int rc = json_object_object_add( payloadObj, X_FIND_TIP_SKEW_TRAVEL_DISTANCE, json_object_new_int64( x_find_tip_skew_travel_distance ) );
		succeeded &= (rc == 0);

		long x_optical_sensor_to_tip_skew_position = m_parametersAxesXY->GetFieldByName( X_OPTICAL_SENSOR_TO_TIP_SKEW_POSITION ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, X_OPTICAL_SENSOR_TO_TIP_SKEW_POSITION, json_object_new_int64( x_optical_sensor_to_tip_skew_position ) );
		succeeded &= (rc == 0);

		unsigned long x_find_tip_skew_speed = m_parametersAxesXY->GetFieldByName( X_FIND_TIP_SKEW_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, X_FIND_TIP_SKEW_SPEED, json_object_new_uint64( x_find_tip_skew_speed ) );
		succeeded &= (rc == 0);

		unsigned long x_find_tip_skew_acceleration = m_parametersAxesXY->GetFieldByName( X_FIND_TIP_SKEW_ACCELERATION ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, X_FIND_TIP_SKEW_ACCELERATION, json_object_new_uint64( x_find_tip_skew_acceleration ) );
		succeeded &= (rc == 0);

		unsigned long x_find_tip_skew_deceleration = m_parametersAxesXY->GetFieldByName( X_FIND_TIP_SKEW_DECELERATION ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, X_FIND_TIP_SKEW_DECELERATION, json_object_new_uint64( x_find_tip_skew_deceleration ) );
		succeeded &= (rc == 0);

		succeeded &= DesHelper::AddXYSpeeds( m_threadName, payloadObj, m_parametersAxesXY );
	}
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveYToTipSkewTrigger( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Get needed property values and add them to the payload.
		long y_find_tip_skew_travel_distance = m_parametersAxesXY->GetFieldByName( Y_FIND_TIP_SKEW_TRAVEL_DISTANCE ).GetValueAsLong();
		int rc = json_object_object_add( payloadObj, Y_FIND_TIP_SKEW_TRAVEL_DISTANCE, json_object_new_int64( y_find_tip_skew_travel_distance ) );
		succeeded &= (rc == 0);

		long y_optical_sensor_to_tip_skew_position = m_parametersAxesXY->GetFieldByName( Y_OPTICAL_SENSOR_TO_TIP_SKEW_POSITION ).GetValueAsLong();
		rc = json_object_object_add( payloadObj, Y_OPTICAL_SENSOR_TO_TIP_SKEW_POSITION, json_object_new_int64( y_optical_sensor_to_tip_skew_position ) );
		succeeded &= (rc == 0);

		unsigned long y_find_tip_skew_speed = m_parametersAxesXY->GetFieldByName( Y_FIND_TIP_SKEW_SPEED ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, Y_FIND_TIP_SKEW_SPEED, json_object_new_uint64( y_find_tip_skew_speed ) );
		succeeded &= (rc == 0);

		unsigned long y_find_tip_skew_acceleration = m_parametersAxesXY->GetFieldByName( Y_FIND_TIP_SKEW_ACCELERATION ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, Y_FIND_TIP_SKEW_ACCELERATION, json_object_new_uint64( y_find_tip_skew_acceleration ) );
		succeeded &= (rc == 0);

		unsigned long y_find_tip_skew_deceleration = m_parametersAxesXY->GetFieldByName( Y_FIND_TIP_SKEW_DECELERATION ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, Y_FIND_TIP_SKEW_DECELERATION, json_object_new_uint64( y_find_tip_skew_deceleration ) );
		succeeded &= (rc == 0);

		succeeded &= DesHelper::AddXYSpeeds( m_threadName, payloadObj, m_parametersAxesXY );
	}
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

/// MeteOut_MoveXYTo_withPlatePositionDetails can be shared by 4 different MoveXYTo... commands. They all
/// data: the X,Y distance to the appropriate plate edge. Here, we add that data to the message and send
/// it on to the Doler, which handles each of them differently.
bool CommandInterpreterThread::MeteOut_MoveXYTo_withPlatePositionDetails( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Read the tip_axis value from the payload to learn which axis to draw from.
		json_object * platePositionObj = json_object_object_get( payloadObj, PLATE_POSITION );
		if (platePositionObj)
		{
			int platePosition = json_object_get_int64( platePositionObj );
			if (platePosition == 1)
			{
				/// Stuff some extra json_objects in the payload.
				long x_plate_1_right_edge_to_optical_path = m_parametersAxesXY->GetFieldByName( X_PLATE_1_RIGHT_EDGE_TO_OPTICAL_PATH ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, X_PLATE_1_RIGHT_EDGE_TO_OPTICAL_PATH, json_object_new_int64( x_plate_1_right_edge_to_optical_path ) );
				succeeded &= (rc == 0);

				long y_plate_1_top_edge_to_optical_path = m_parametersAxesXY->GetFieldByName( Y_PLATE_1_TOP_EDGE_TO_OPTICAL_PATH ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, Y_PLATE_1_TOP_EDGE_TO_OPTICAL_PATH, json_object_new_int64( y_plate_1_top_edge_to_optical_path ) );
				succeeded &= (rc == 0);

				long x_plate_1_right_edge_to_tip_working_position = m_parametersAxesXY->GetFieldByName( X_PLATE_1_RIGHT_EDGE_TO_TIP_WORKING_POSITION ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, X_PLATE_1_RIGHT_EDGE_TO_TIP_WORKING_POSITION, json_object_new_int64( x_plate_1_right_edge_to_tip_working_position ) );
				succeeded &= (rc == 0);

				long y_plate_1_top_edge_to_tip_working_position = m_parametersAxesXY->GetFieldByName( Y_PLATE_1_TOP_EDGE_TO_TIP_WORKING_POSITION ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, Y_PLATE_1_TOP_EDGE_TO_TIP_WORKING_POSITION, json_object_new_int64( y_plate_1_top_edge_to_tip_working_position ) );
				succeeded &= (rc == 0);
			}
			else if (platePosition == 2)
			{
				/// Stuff some extra json_objects in the payload.
				long x_plate_2_left_edge_to_optical_path = m_parametersAxesXY->GetFieldByName( X_PLATE_2_LEFT_EDGE_TO_OPTICAL_PATH ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, X_PLATE_2_LEFT_EDGE_TO_OPTICAL_PATH, json_object_new_int64( x_plate_2_left_edge_to_optical_path ) );
				succeeded &= (rc == 0);

				long y_plate_2_top_edge_to_optical_path = m_parametersAxesXY->GetFieldByName( Y_PLATE_2_TOP_EDGE_TO_OPTICAL_PATH ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, Y_PLATE_2_TOP_EDGE_TO_OPTICAL_PATH, json_object_new_int64( y_plate_2_top_edge_to_optical_path ) );
				succeeded &= (rc == 0);

				long x_plate_2_left_edge_to_tip_working_position = m_parametersAxesXY->GetFieldByName( X_PLATE_2_LEFT_EDGE_TO_TIP_WORKING_POSITION ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, X_PLATE_2_LEFT_EDGE_TO_TIP_WORKING_POSITION, json_object_new_int64( x_plate_2_left_edge_to_tip_working_position ) );
				succeeded &= (rc == 0);

				long y_plate_2_top_edge_to_tip_working_position = m_parametersAxesXY->GetFieldByName( Y_PLATE_2_TOP_EDGE_TO_TIP_WORKING_POSITION ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, Y_PLATE_2_TOP_EDGE_TO_TIP_WORKING_POSITION, json_object_new_int64( y_plate_2_top_edge_to_tip_working_position ) );
				succeeded &= (rc == 0);
			}
			else
			{
				succeeded = false;
			}
		}
		else
		{
			succeeded = false;
		}
		succeeded &= DesHelper::AddXYSpeeds( m_threadName, payloadObj, m_parametersAxesXY );
	}
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

// TODO: Collect all the MeteOut commands that are identical to MeteOut_moveToXYNm and collapse them into one.
bool CommandInterpreterThread::MeteOut_SwirlXYStage( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;

	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	succeeded &= DesHelper::AddXYSpeeds( m_threadName, payloadObj, m_parametersAxesXY );
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );

	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveYYToTipRackGrabLocation( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		Moves the YY axis so the rack is in the proper position to be grabbed by the tip rack mover.
	**/
	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (!payloadObj) /// Either it was NULL or both the "payload" key and object are missing.
	{
		payloadObj = json_object_new_object();
		/// Add this empty object. This incurs loss of ownership, meaning someone else will delete it, later.
		int rc = json_object_object_add( jWorkOrderObj, PAYLOAD, payloadObj );
		succeeded &= (rc == 0);
	}

	/// Stuff some extra json_objects in the payload.
	long yy_tip_tray_cxpm_grab_position = m_parametersAxisYY->GetFieldByName( YY_TIP_TRAY_CXPM_GRAB_POSITION ).GetValueAsLong();
	int rc = json_object_object_add( payloadObj, YY_TIP_TRAY_CXPM_GRAB_POSITION, json_object_new_int64( yy_tip_tray_cxpm_grab_position ) );
	succeeded &= (rc == 0);

	succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, "yy", m_parametersAxisYY );
	
	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_MoveSingleAxis( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg, MsgQueue * outBox, const char * axisName, ComponentProperties * props )
{
	bool succeeded = true;
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (!payloadObj) /// Either it was NULL or both the "payload" key and object are missing.
	{
		payloadObj = json_object_new_object();
		/// Add this empty object. This incurs loss of ownership, meaning someone else will delete it, later.
		int rc = json_object_object_add( jWorkOrderObj, PAYLOAD, payloadObj );
		succeeded &= (rc == 0);
	}

	succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, axisName, props );

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	outBox->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_RetractSingleAxis( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg, MsgQueue * outBox, const char * axisName, ComponentProperties * props )
{
	bool succeeded = true;
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (!payloadObj) /// Either it was NULL or both the "payload" key and object are missing.
	{
		payloadObj = json_object_new_object();
		/// Add this empty object. This incurs loss of ownership, meaning someone else will delete it, later.
		int rc = json_object_object_add( jWorkOrderObj, PAYLOAD, payloadObj );
		succeeded &= (rc == 0);
	}

	succeeded &= DesHelper::AddSpeedsAndLimits( m_threadName, payloadObj, axisName, props );
	succeeded &= DesHelper::ReplaceWithRetractionSpeeds( m_threadName, payloadObj, axisName, props ); /// Must be called AFTER AddSpeedsAndLimits().

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	outBox->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_Aspirate( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (!payloadObj) /// Either it was NULL or both the "payload" key and object are missing.
	{
		payloadObj = json_object_new_object();
		/// Add this empty object. This incurs loss of ownership, meaning someone else will delete it, later.
		int rc = json_object_object_add( jWorkOrderObj, PAYLOAD, payloadObj );
		succeeded &= (rc == 0);
	}

	succeeded &= DesHelper::AddFlowRatesAndLimits( m_threadName, payloadObj, "pn", m_parametersAspirationPump1 );

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForPump->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_AspirateStartStop( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (!payloadObj) /// Either it was NULL or both the "payload" key and object are missing.
	{
		payloadObj = json_object_new_object();
		/// Add this empty object. This incurs loss of ownership, meaning someone else will delete it, later.
		int rc = json_object_object_add( jWorkOrderObj, PAYLOAD, payloadObj );
		succeeded &= (rc == 0);
	}

	/// Pack the calibration coefficients and accelerations.
	succeeded &= DesHelper::AddFlowRatesAndLimits( m_threadName, payloadObj, "pn", m_parametersAspirationPump1 );

	/// Pack the aspiration pump's standard_flow_rate (nl/s) as "flow_rate_ul_per_s".
	string name = STANDARD_FLOW_RATE;
	float flowRate_nlPerSec = m_parametersAspirationPump1->GetFieldByName( name ).GetValueAsFloat();
	int rc = json_object_object_add( payloadObj, FLOW_RATE_UL_PER_S, json_object_new_double( flowRate_nlPerSec / 1000.0 ) );
	succeeded &= (rc == 0);

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForPump->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_Dispense( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg, int pumpNumber )
{
	bool succeeded = true;
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (!payloadObj) /// Either it was NULL or both the "payload" key and object are missing.
	{
		payloadObj = json_object_new_object();
		/// Add this empty object. This incurs loss of ownership, meaning someone else will delete it, later.
		int rc = json_object_object_add( jWorkOrderObj, PAYLOAD, payloadObj );
		succeeded &= (rc == 0);
	}

	if (pumpNumber == 1)
	{
		succeeded &= DesHelper::AddFlowRatesAndLimits( m_threadName, payloadObj, "pp", m_parametersDispensePump1 );
	}
	else if (pumpNumber == 2)
	{
		succeeded &= DesHelper::AddFlowRatesAndLimits( m_threadName, payloadObj, "ppp", m_parametersDispensePump2 );
	}
	else if (pumpNumber == 3)
	{
		succeeded &= DesHelper::AddFlowRatesAndLimits( m_threadName, payloadObj, "pppp", m_parametersDispensePump3);
	}
	else
	{
		stringstream msg;
		msg << "Invalid pumpNumber=" << pumpNumber << "|valid values are[1,2,3]";
		LOG_ERROR( m_threadName, "pumps", msg.str().c_str() );
		succeeded = false;
	}

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForPump->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_DispenseStartStop( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg, int pumpNumber )
{
	bool succeeded = true;
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (!payloadObj) /// Either it was NULL or both the "payload" key and object are missing.
	{
		payloadObj = json_object_new_object();
		/// Add this empty object. This incurs loss of ownership, meaning someone else will delete it, later.
		int rc = json_object_object_add( jWorkOrderObj, PAYLOAD, payloadObj );
		succeeded &= (rc == 0);
	}

	string name = STANDARD_FLOW_RATE;
	float flowRate_nlPerSec = 0.0;

	if (pumpNumber == 1)
	{
		succeeded &= DesHelper::AddFlowRatesAndLimits( m_threadName, payloadObj, "pp", m_parametersDispensePump1 );
		flowRate_nlPerSec = m_parametersDispensePump1->GetFieldByName( name ).GetValueAsFloat();
	}
	else if (pumpNumber == 2)
	{
		succeeded &= DesHelper::AddFlowRatesAndLimits( m_threadName, payloadObj, "ppp", m_parametersDispensePump2 );
		flowRate_nlPerSec = m_parametersDispensePump2->GetFieldByName( name ).GetValueAsFloat();
	}
	else if (pumpNumber == 3)
	{
		succeeded &= DesHelper::AddFlowRatesAndLimits( m_threadName, payloadObj, "pppp", m_parametersDispensePump3);
		flowRate_nlPerSec = m_parametersDispensePump3->GetFieldByName( name ).GetValueAsFloat();
	}
	else
	{
		stringstream msg;
		msg << "Invalid pumpNumber=" << pumpNumber << "|valid values are[1,2,3]";
		LOG_ERROR( m_threadName, "pumps", msg.str().c_str() );
		succeeded = false;
	}

	/// Pack the aspiration pump's standard_flow_rate (nl/s) as "flow_rate_ul_per_s".
	int rc = json_object_object_add( payloadObj, FLOW_RATE_UL_PER_S, json_object_new_double( flowRate_nlPerSec / 1000.0 ) );
	succeeded &= (rc == 0);

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForPump->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_Picking( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (!payloadObj) /// Either it was NULL or both the "payload" key and object are missing.
	{
		payloadObj = json_object_new_object();
		/// Add this empty object. This incurs loss of ownership, meaning someone else will delete it, later.
		int rc = json_object_object_add( jWorkOrderObj, PAYLOAD, payloadObj );
		succeeded &= (rc == 0);
	}

	/// This is the picking-pump equivalent to AddFlowRatesAndLimits
	succeeded &= DesHelper::AddPickingPumpFlowParams( m_threadName, payloadObj, "picking", m_parametersPickingPump1 );
	
	std::string name = PUMP_VENDOR;
	std::string pumpVendor = m_parametersPickingPump1->GetFieldByName( name ).setting;
	int rc = json_object_object_add( payloadObj, PUMP_VENDOR, json_object_new_string( pumpVendor.c_str() ) );
	succeeded &= (rc == 0);

	name = PUMP_MODEL;
	std::string pumpModel = m_parametersPickingPump1->GetFieldByName( name ).setting;
	rc = json_object_object_add( payloadObj, PUMP_MODEL, json_object_new_string( pumpModel.c_str() ) );
	succeeded &= (rc == 0);

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForPump->PushMsg(workMsg); // TODO: Consider sending this to another thread, if it makes more sense to do so.
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_GetAutoFocusImageStack( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		Collects a series of images at different optical column focus heights.
		The receiver then analyzes the images to determine where focus is.
	**/
	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (!payloadObj) /// Either it was NULL or both the "payload" key and object are missing.
	{
		payloadObj = json_object_new_object();
		/// Add this empty object. This incurs loss of ownership, meaning someone else will delete it, later.
		int rc = json_object_object_add( jWorkOrderObj, PAYLOAD, payloadObj );
		succeeded &= (rc == 0);
	}

	/// Stuff some extra json_objects in the payload.
	unsigned long auto_focus_start_offset_nm = m_parametersOpticalColumn->GetFieldByName( AUTO_FOCUS_START_OFFSET ).GetValueAsULong();
	int rc = json_object_object_add( payloadObj, AUTO_FOCUS_START_OFFSET, json_object_new_uint64( auto_focus_start_offset_nm ) );
	succeeded &= (rc == 0);

	unsigned long auto_focus_image_count = m_parametersOpticalColumn->GetFieldByName( AUTO_FOCUS_IMAGE_COUNT ).GetValueAsULong();
	rc = json_object_object_add( payloadObj, AUTO_FOCUS_IMAGE_COUNT, json_object_new_uint64( auto_focus_image_count ) );
	succeeded &= (rc == 0);
	
	unsigned long auto_focus_increment_nm = m_parametersOpticalColumn->GetFieldByName( AUTO_FOCUS_INCREMENT ).GetValueAsULong();
	rc = json_object_object_add( payloadObj, AUTO_FOCUS_INCREMENT, json_object_new_uint64( auto_focus_increment_nm ) );
	succeeded &= (rc == 0);

	/// Stuff some extra json_objects in the payload.
	unsigned long pixel_count_x = m_parametersCamera->GetFieldByName( PIXEL_COUNT_X ).GetValueAsULong();
	rc = json_object_object_add( payloadObj, PIXEL_COUNT_X, json_object_new_int64( pixel_count_x ) );
	succeeded &= (rc == 0);

	unsigned long pixel_count_y = m_parametersCamera->GetFieldByName( PIXEL_COUNT_Y ).GetValueAsULong();
	rc = json_object_object_add( payloadObj, PIXEL_COUNT_Y, json_object_new_int64( pixel_count_y ) );
	succeeded &= (rc == 0);
	
	unsigned long bin_factor = m_parametersCamera->GetFieldByName( BIN_FACTOR ).GetValueAsULong();
	rc = json_object_object_add( payloadObj, BIN_FACTOR, json_object_new_int64( bin_factor ) );
	succeeded &= (rc == 0);

	unsigned long camera_color_depth = m_parametersCamera->GetFieldByName( CAMERA_COLOR_DEPTH ).GetValueAsULong();
	rc = json_object_object_add( payloadObj, CAMERA_COLOR_DEPTH, json_object_new_int64( camera_color_depth ) );
	succeeded &= (rc == 0);

	unsigned long scaled_color_depth = m_parametersCamera->GetFieldByName( SCALED_COLOR_DEPTH ).GetValueAsULong();
	rc = json_object_object_add( payloadObj, SCALED_COLOR_DEPTH, json_object_new_int64( scaled_color_depth ) );
	succeeded &= (rc == 0);

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForOptics->PushMsg(workMsg);
	return succeeded;
}

// TODO: Implement decomposition.
bool CommandInterpreterThread::MeteOut_ChangeActiveTipRack( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	return false;
}

///----------------------------------------------------------------------
/// MeteOut_CollectMontageImages
/// Talks to the camera to get pictures, and talks to the CxD for moving
/// the plate around between pictures. Communications with these two
/// pieces of hardware are normally kept separate on the Doler threads
/// for Camara and CxD, respectively.
/// For this special case, we will pick one thread for all this work
/// and use a mutex to block the other thread from using the camera
/// at the same time.
///----------------------------------------------------------------------
bool CommandInterpreterThread::MeteOut_CollectMontageImages( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		Collects montage images returning each one to each step increment. Montage images are
		collected in a serpentine pattern and are Y diretion dominant. This routine assumes that
		the XY stage is placed at the lower left corner of the montage array.
	**/
	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// Stuff some extra json_objects in the payload.

		/// Read the tip_axis value from the payload to learn which axis to draw from.
		json_object * platePositionObj = json_object_object_get( payloadObj, PLATE_POSITION );
		if (platePositionObj)
		{
			int platePosition = json_object_get_int64( platePositionObj );
			if (platePosition == 1)
			{
				/// Stuff some extra json_objects in the payload.
				long x_plate_1_right_edge_to_optical_path = m_parametersAxesXY->GetFieldByName( X_PLATE_1_RIGHT_EDGE_TO_OPTICAL_PATH ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, X_PLATE_1_RIGHT_EDGE_TO_OPTICAL_PATH, json_object_new_int64( x_plate_1_right_edge_to_optical_path ) );
				succeeded &= (rc == 0);

				long y_plate_1_top_edge_to_optical_path = m_parametersAxesXY->GetFieldByName( Y_PLATE_1_TOP_EDGE_TO_OPTICAL_PATH ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, Y_PLATE_1_TOP_EDGE_TO_OPTICAL_PATH, json_object_new_int64( y_plate_1_top_edge_to_optical_path ) );
				succeeded &= (rc == 0);
			}
			else if (platePosition == 2)
			{
				/// Stuff some extra json_objects in the payload.
				long x_plate_2_left_edge_to_optical_path = m_parametersAxesXY->GetFieldByName( X_PLATE_2_LEFT_EDGE_TO_OPTICAL_PATH ).GetValueAsLong();
				int rc = json_object_object_add( payloadObj, X_PLATE_2_LEFT_EDGE_TO_OPTICAL_PATH, json_object_new_int64( x_plate_2_left_edge_to_optical_path ) );
				succeeded &= (rc == 0);

				long y_plate_2_top_edge_to_optical_path = m_parametersAxesXY->GetFieldByName( Y_PLATE_2_TOP_EDGE_TO_OPTICAL_PATH ).GetValueAsLong();
				rc = json_object_object_add( payloadObj, Y_PLATE_2_TOP_EDGE_TO_OPTICAL_PATH, json_object_new_int64( y_plate_2_top_edge_to_optical_path ) );
				succeeded &= (rc == 0);
			}
			else
			{
				succeeded = false;
			}
		}
		else
		{
			succeeded = false;
		}
		SpeedType montageSpeedType = SpeedType::Speed_undefined;
		json_object * montageSpeedObj = json_object_object_get( payloadObj, MONTAGE_SPEED );
		if (montageSpeedObj)
		{
			string montageSpeedStr = json_object_get_string( montageSpeedObj );
			montageSpeedType = StringNameToMontageSpeedTypeEnum( montageSpeedStr.c_str() );
			stringstream msg;
			msg << "payload contains " << montageSpeedStr << "|int form is " << (int) montageSpeedType;
			LOG_TRACE( m_threadName, "montage", msg.str().c_str() );
		}
		else
		{
			LOG_WARN( m_threadName, "montage", "payload lacks montage_speed" );
		}

		unsigned long pixel_count_x = m_parametersCamera->GetFieldByName( PIXEL_COUNT_X ).GetValueAsULong();
		int rc = json_object_object_add( payloadObj, PIXEL_COUNT_X, json_object_new_int64( pixel_count_x ) );
		succeeded &= (rc == 0);

		unsigned long pixel_count_y = m_parametersCamera->GetFieldByName( PIXEL_COUNT_Y ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, PIXEL_COUNT_Y, json_object_new_int64( pixel_count_y ) );
		succeeded &= (rc == 0);
		
		unsigned long bin_factor = m_parametersCamera->GetFieldByName( BIN_FACTOR ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, BIN_FACTOR, json_object_new_int64( bin_factor ) );
		succeeded &= (rc == 0);

		unsigned long camera_color_depth = m_parametersCamera->GetFieldByName( CAMERA_COLOR_DEPTH ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, CAMERA_COLOR_DEPTH, json_object_new_int64( camera_color_depth ) );
		succeeded &= (rc == 0);

		unsigned long scaled_color_depth = m_parametersCamera->GetFieldByName( SCALED_COLOR_DEPTH ).GetValueAsULong();
		rc = json_object_object_add( payloadObj, SCALED_COLOR_DEPTH, json_object_new_int64( scaled_color_depth ) );
		succeeded &= (rc == 0);

		succeeded &= DesHelper::AddXYSpeeds( m_threadName, payloadObj, m_parametersAxesXY );
		succeeded &= DesHelper::AddMontageSpeeds( m_threadName, payloadObj, m_parametersAxesXY, m_parametersCxD, montageSpeedType );
	}

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForCxD->PushMsg(workMsg);
	return succeeded;
}

bool CommandInterpreterThread::MeteOut_GetCameraImage( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	bool succeeded = true;
	/**
		Sends a camera image.
	**/
	/// Get a pointer to the payload object.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (!payloadObj) /// Either it was NULL or both the "payload" key and object are missing.
	{
		payloadObj = json_object_new_object();
		/// Add this empty object. This incurrs loss of ownership.
		int rc = json_object_object_add( jWorkOrderObj, PAYLOAD, payloadObj );
		succeeded &= (rc == 0);
	}

	/// Stuff some extra json_objects in the payload.
	unsigned long pixel_count_x = m_parametersCamera->GetFieldByName( PIXEL_COUNT_X ).GetValueAsULong();
	int rc = json_object_object_add( payloadObj, PIXEL_COUNT_X, json_object_new_int64( pixel_count_x ) );
	succeeded &= (rc == 0);

	unsigned long pixel_count_y = m_parametersCamera->GetFieldByName( PIXEL_COUNT_Y ).GetValueAsULong();
	rc = json_object_object_add( payloadObj, PIXEL_COUNT_Y, json_object_new_int64( pixel_count_y ) );
	succeeded &= (rc == 0);

	unsigned long bin_factor = m_parametersCamera->GetFieldByName( BIN_FACTOR ).GetValueAsULong();
	rc = json_object_object_add( payloadObj, BIN_FACTOR, json_object_new_int64( bin_factor ) );
	succeeded &= (rc == 0);

	unsigned long camera_color_depth = m_parametersCamera->GetFieldByName( CAMERA_COLOR_DEPTH ).GetValueAsULong();
	rc = json_object_object_add( payloadObj, CAMERA_COLOR_DEPTH, json_object_new_int64( camera_color_depth ) );
	succeeded &= (rc == 0);

	unsigned long scaled_color_depth = m_parametersCamera->GetFieldByName( SCALED_COLOR_DEPTH ).GetValueAsULong();
	rc = json_object_object_add( payloadObj, SCALED_COLOR_DEPTH, json_object_new_int64( scaled_color_depth ) );
	succeeded &= (rc == 0);

	/// Create message with this new payload
	std::string mqttMsg = json_object_to_json_string( jWorkOrderObj );
	workMsg->payload = mqttMsg;
	m_outBoxForOptics->PushMsg(workMsg);
	return succeeded;
}
/*
string CommandInterpreterThread::ComposeCpdFileName( ComponentType componentType, unsigned int cpdId )
{
}
*/
///----------------------------------------------------------------------
/// StoreCpd
/// Interprets "cpd_id" to a file name.
/// Validates the structure of "cpd_body".
/// If the file is not found, write "cpd_body" to the file.
/// If the file *is* found, do a merge instead.
///
/// Merging entails reading the file into a json_object and adding all
/// of the new parameters to the object--overwriting any like-named
/// parameters already present, but possibly adding new parameters.
/// Then write the modified object to file, overwriting the original.
///
/// Finally, compose a status and send it on to the next thread, which
/// will forward it to the MQTT sender thread.
///----------------------------------------------------------------------
bool CommandInterpreterThread::StoreCpd( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg, bool updateToo )
{
	bool succeeded = false;
	unsigned long cpdId;
	string cpdFilePath = "";

	std::string messageType = "";
	// std::string transportVersion = ""; /// Not of interest?
	int commandGroupId = 0; /// Also not interesting?
	int commandId = 0;
	std::string sessionId = "";
	std::string machineId = "";
	int statusCode = STATUS_OK; /// success value
	json_object * statusPayload = nullptr; /// success value.
	ComponentType componentType = ComponentType::ComponentUnknown;
	/// Extract all top-level details except payload.
	succeeded = m_desHelper->SplitCommand( jWorkOrderObj,
											&messageType,
											nullptr, // &transportVersion,
											&commandGroupId,
											&commandId,
											&sessionId,
											&machineId );
	LETS_ASSERT( succeeded );
	/// Now extract payload.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		string payloadStr = json_object_to_json_string(payloadObj);
		cout << payloadStr << endl;

		/// This payload is expected to have just two fields: cpd_id, and cpd_body.
		/// Extract cpd_id.
		json_object * cpdIdObj = json_object_object_get( payloadObj, CPD_ID );
		if (cpdIdObj)
		{
			errno = 0;
			cpdId = json_object_get_uint64( cpdIdObj );
			LETS_ASSERT(errno == 0);
		}
		/// Extract cpd_body. It is expected to be the contents of a cpd file.
		json_object * cpdBodyObj = json_object_object_get( payloadObj, CPD_BODY );
		if (cpdBodyObj)
		{
			/// The cpd file is expected to have two objects: a header named 'header'
			/// and an object (whose name is a component-type) containing all properties.
			struct json_object_iterator it;
			struct json_object_iterator itEnd;
			it = json_object_iter_init_default();
			it = json_object_iter_begin(cpdBodyObj);
			itEnd = json_object_iter_end(cpdBodyObj);
			
			/// The CPD_BODY has a HEADER inside. Strange but true.
			/// Get the header
			string headerName = json_object_iter_peek_name( &it );
			json_object * headerObj = json_object_iter_peek_value( &it );
			LETS_ASSERT(headerName == HEADER);
			
			/// Get the object containing all properties.`
			string enumComponentTypeStr = "";
			json_object * compPropsObj = nullptr;
			json_object_iter_next(&it);
			if (!json_object_iter_equal(&it, &itEnd))
			{
				enumComponentTypeStr = json_object_iter_peek_name( &it );
				compPropsObj = json_object_iter_peek_value( &it );
			}
			if (nullptr == compPropsObj)
			{
				cout << "ERROR reading CPD Body" << endl;
				return false;				/// EARLY RETURN!  EARLY RETURN!
			}
			
			/// Extract values from the header.
			string cpdFileName = "";
			string cpdVersion = "";
			string cpdDescription = "";
			if (headerObj)
			{
				succeeded &= m_desHelper->SplitHeader( headerObj,
													   &cpdFileName,
													   &cpdVersion,
													   &cpdDescription );
			}
			
			componentType = StringNameToComponentTypeEnum( enumComponentTypeStr.c_str() );
			if (componentType == ComponentType::ComponentUnknown)
			{
				cout << "ERROR! Could not interpret " << enumComponentTypeStr << " as a component type." << endl;
				return false;				/// EARLY RETURN!  EARLY RETURN!
			}
			
			string cpdEsFileName = ComponentProperties::ComposeCpdFileName( componentType, cpdId );
			cpdFilePath = (std::string (CPD_DIR) + "/" + cpdEsFileName);

			// TODO: Move the file-saving block to a later point, so it can be skipped if the file doesn't parse OK.
			/// Write the entire CPD body to cpdFilePath, in JSON format.
			if (FileExists( cpdFilePath ))
			{
				cout << "WARNING: Overwriting " << cpdFilePath << endl;
			}
			int fd = open( cpdFilePath.c_str(), O_WRONLY | O_CREAT, 0600 );
			LETS_ASSERT( fd >= 0 );
			int rv = json_object_to_fd( fd, cpdBodyObj, JSON_C_TO_STRING_PLAIN );
			LETS_ASSERT( rv == 0 );
			close( fd );
			
			if (updateToo)
			{
				succeeded &= ApplyCpdFileToComponent( payloadObj, cpdFilePath, enumComponentTypeStr );
			}
		}

		/// Create status message.
		workMsg->payload = m_desHelper->ComposeStatusString( commandGroupId,
															 commandId,
															 sessionId,
															 messageType,
															 machineId,
															 statusCode,
															 statusPayload );
		/// destination topic becomes "/status".
		workMsg->destination = ReplaceTopicChannel(workMsg->destination, TEXT_STATUS);
		/// NOTICE that we send this to m_statusRouteBox, which sends it to MqttSender.
		/// All functions that handle file IO--not component driving--should do the same.
		/// The reason is this: During a hold, the command doler will stop processessing messages
		/// but a hold need not stop file IO commands.
		m_statusRouteBox->PushMsg(workMsg);
	}
	return true;
}

bool CommandInterpreterThread::GetCpdListFromType( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	ComponentType componentType = ComponentType::ComponentUnknown;
	std::string messageType = "";
	int commandGroupId = 0;
	int commandId = 0;
	std::string sessionId = "";
	std::string machineId = "";

	/// Extract all top-level details except payload.
	bool succeeded = m_desHelper->SplitCommand( jWorkOrderObj,
												&messageType,
												nullptr, // &transportVersion,
												&commandGroupId,
												&commandId,
												&sessionId,
												&machineId );

	/// Now extract payload.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// This payload is expected to have just one field: cpd_type.
		/// Extract cpd_type.
		json_object * cpdTypeObj = json_object_object_get( payloadObj, CPD_TYPE );
		if (cpdTypeObj)
		{
			string enumComponentTypeStr = json_object_get_string( cpdTypeObj );
			componentType = StringNameToComponentTypeEnum( enumComponentTypeStr.c_str() );
			if (componentType == ComponentType::ComponentUnknown)
			{
				cout << "ERROR! Could not interpret " << enumComponentTypeStr << " as a component type." << endl;
			}
		}
	}
	succeeded &= (componentType != ComponentType::ComponentUnknown);

	string cpdDir = CPD_DIR;
	vector<unsigned long> cpdIdVector = ComponentProperties::DetectListOfCpds( cpdDir, componentType );

	json_object * cpdIdArrayObj = json_object_new_array();
	int arrayInitialSize = json_object_array_length( cpdIdArrayObj );
	for (int i = 0; i < cpdIdVector.size(); i++)
	{
		if (i < arrayInitialSize)
		{
			json_object_array_put_idx( cpdIdArrayObj, i, json_object_new_uint64( cpdIdVector[i] ));
		}
		else
		{
			json_object_array_add( cpdIdArrayObj, json_object_new_uint64( cpdIdVector[i] ));
		}
	}

	json_object * statusPayload = json_object_new_object();
	int result = json_object_object_add( statusPayload, CPD_LIST, cpdIdArrayObj );

	/// Create status message.
	int statusCode = (cpdIdVector.size() > 0) ? STATUS_OK : STATUS_CPD_NOT_FOUND;
	workMsg->payload = m_desHelper->ComposeStatusString( commandGroupId,
														 commandId,
														 sessionId,
														 messageType,
														 machineId,
														 statusCode,
														 statusPayload );
	/// destination topic becomes "/status".
	workMsg->destination = ReplaceTopicChannel(workMsg->destination, TEXT_STATUS);
	/// NOTICE that we send this to m_statusRouteBox, which sends it to MqttSender.
	m_statusRouteBox->PushMsg(workMsg);
	return true; /// forwarding is complete.
}

bool CommandInterpreterThread::GetCpdContents( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	unsigned int cpdId = 0;
	ComponentType componentType = ComponentType::ComponentUnknown;
	std::string messageType = "";
	// std::string transportVersion = ""; /// Not of interest?
	int commandGroupId = 0; /// Also not interesting?
	int commandId = 0;
	std::string sessionId = "";
	std::string machineId = "";

	/// Extract all top-level details except payload.
	bool succeeded = m_desHelper->SplitCommand( jWorkOrderObj,
												&messageType,
												nullptr, // &transportVersion,
												&commandGroupId,
												&commandId,
												&sessionId,
												&machineId );
	LETS_ASSERT( succeeded );

	/// Now extract payload.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// This payload is expected to have just two fields: cpd_id, and cpd_body.
		/// Extract cpd_id.
		json_object * cpdIdObj = json_object_object_get( payloadObj, CPD_ID );
		if (cpdIdObj)
		{
			cpdId = (unsigned int) json_object_get_int64( cpdIdObj );
		}

		json_object * componentTypeObj = json_object_object_get( payloadObj, COMPONENT_TYPE );
		if (componentTypeObj)
		{
			string enumComponentTypeStr = json_object_get_string( componentTypeObj );
			componentType = StringNameToComponentTypeEnum( enumComponentTypeStr.c_str() );
			if (componentType == ComponentType::ComponentUnknown)
			{
				cout << "ERROR! Could not interpret " << enumComponentTypeStr << " as a component type." << endl;
			}
		}
	}
	succeeded &= (cpdId != 0);
	succeeded &= (componentType != ComponentType::ComponentUnknown);

	std::stringstream strStream;
	bool streamIsReady = false;
	if (succeeded)
	{
		string cpdEsFileName = ComponentProperties::ComposeCpdFileName( componentType, cpdId );
		string cpdFilePath = (std::string (CPD_DIR) + "/" + cpdEsFileName);

		if (FileExists( cpdFilePath ))
		{
			std::ifstream cpdFile;
			cpdFile.open( cpdFilePath.c_str() );
			if (cpdFile.is_open())
			{
				strStream << cpdFile.rdbuf();
				streamIsReady = true;
				cpdFile.close();
			}
			else
			{
				cout << m_threadName << ": ERROR! GetCpdContents failed to open file " << cpdFilePath << endl;
			}
		}
	}

	/// Create payload
	json_object * cpdObj = json_object_new_object();
	int result = json_object_object_add( cpdObj, CPD_ID, json_object_new_int64( cpdId ) );
	if (result != 0)
	{
		cout << "ERROR: GetCpdContents() failed to add cpd ID to payload " << cpdId << endl;
		succeeded = false;
	}
	result = json_object_object_add( cpdObj, CPD_CONTENTS, json_object_new_string( strStream.str().c_str() ) );
	if (result != 0)
	{
		cout << "ERROR: GetCpdContents() failed to add cpd contents to payload " << cpdId << endl;
		succeeded = false;
	}
	json_object * arrayObj = json_object_new_array();
	json_object_array_put_idx(arrayObj, 0, cpdObj);
	
	json_object * statusPayload = arrayObj; // TODO: CONFIRM THIS. It doesn't match the DES. (I'm not sure the DES even shows valid JSON.)
	
	/// Create status message.
	int statusCode = (succeeded && streamIsReady) ? STATUS_OK : STATUS_CPD_NOT_FOUND;
	workMsg->payload = m_desHelper->ComposeStatusString( commandGroupId,
														 commandId,
														 sessionId,
														 messageType,
														 machineId,
														 statusCode,
														 statusPayload );
	/// destination topic becomes "/status".
	workMsg->destination = ReplaceTopicChannel(workMsg->destination, TEXT_STATUS);
	/// NOTICE that we send this to m_statusRouteBox, which sends it to MqttSender.
	m_statusRouteBox->PushMsg(workMsg);
	return true; /// forwarding is complete.
}

bool CommandInterpreterThread::DeleteCpd( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	unsigned int cpdId = 0;
	ComponentType componentType = ComponentType::ComponentUnknown;
	std::string messageType = "";
	int commandGroupId = 0;
	int commandId = 0;
	std::string sessionId = "";
	std::string machineId = "";
	json_object * statusPayload = nullptr; /// success value.

	/// Extract all top-level details except payload.
	bool succeeded = m_desHelper->SplitCommand( jWorkOrderObj,
												&messageType,
												nullptr, // &transportVersion,
												&commandGroupId,
												&commandId,
												&sessionId,
												&machineId );
	LETS_ASSERT( succeeded );

	/// Now extract payload.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// This payload is expected to have just two fields: cpd_id, and cpd_body.
		/// Extract cpd_id.
		json_object * cpdIdObj = json_object_object_get( payloadObj, CPD_ID );
		if (cpdIdObj)
		{
			cpdId = (unsigned int) json_object_get_int64( cpdIdObj );
		}
		json_object * componentTypeObj = json_object_object_get( payloadObj, COMPONENT_TYPE );
		if (componentTypeObj)
		{
			string enumComponentTypeStr = json_object_get_string( componentTypeObj );
			componentType = StringNameToComponentTypeEnum( enumComponentTypeStr.c_str() );
			if (componentType == ComponentType::ComponentUnknown)
			{
				cout << "ERROR! Could not interpret " << enumComponentTypeStr << " as a component type." << endl;
			}
		}
	}
	succeeded &= (cpdId != 0);
	succeeded &= (componentType != ComponentType::ComponentUnknown);

	if (succeeded)
	{
		string cpdFileName = ComponentProperties::ComposeCpdFileName( componentType, cpdId );
		string cpdFilePath = CPD_DIR;
		cpdFilePath = cpdFilePath + "/";
		cpdFilePath = cpdFilePath + cpdFileName;
		if (FileExists( cpdFilePath ))
		{
			std::remove( cpdFilePath.c_str() );
		}
		/// No else here. If we didn't find a file, we still succeeded.
	}

	/// Create status message.
	int statusCode = (succeeded) ? STATUS_OK : STATUS_CPD_NOT_FOUND;
	workMsg->payload = m_desHelper->ComposeStatusString( commandGroupId,
														 commandId,
														 sessionId,
														 messageType,
														 machineId,
														 statusCode,
														 statusPayload );
	/// destination topic becomes "/status".
	workMsg->destination = ReplaceTopicChannel(workMsg->destination, TEXT_STATUS);
	/// NOTICE that we send this to m_statusRouteBox, which sends it to MqttSender.
	m_statusRouteBox->PushMsg(workMsg);
	return true; /// forwarding is complete.
}

bool CommandInterpreterThread::ApplyCpdToComponent( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	unsigned int cpdId = 0;
	string enumComponentTypeStr = "";
	ComponentType componentType = ComponentType::ComponentUnknown;
	string componentId = "";
	string messageType = "";
	int commandGroupId = 0;
	int commandId = 0;
	string sessionId = "";
	string machineId = "";
	json_object * statusPayload = nullptr; /// success value.
	bool fileFound = false;
	int status = STATUS_OK;

	/// Extract all top-level details except payload.
	bool succeeded = m_desHelper->SplitCommand( jWorkOrderObj,
												&messageType,
												nullptr, // &transportVersion,
												&commandGroupId,
												&commandId,
												&sessionId,
												&machineId );
	LETS_ASSERT( succeeded );

	/// Now extract payload.
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		/// This payload is expected to have just two fields: cpd_id, and cpd_body.
		/// Extract cpd_id.
		json_object * cpdIdObj = json_object_object_get( payloadObj, CPD_ID );
		if (cpdIdObj)
		{
			cpdId = (unsigned int) json_object_get_int64( cpdIdObj );
		}
		json_object * componentTypeObj = json_object_object_get( payloadObj, COMPONENT_TYPE );
		if (componentTypeObj)
		{
			enumComponentTypeStr = json_object_get_string( componentTypeObj );
			componentType = StringNameToComponentTypeEnum( enumComponentTypeStr.c_str() );
			if (componentType == ComponentType::ComponentUnknown)
			{
				stringstream msg;
				msg << "Could not interpret " << enumComponentTypeStr << " as a component type.";
				LOG_ERROR( m_threadName, "cpd", msg.str().c_str() );
				succeeded = false;
			}
		}
	}
	succeeded &= (cpdId != 0);
//	if (!succeeded) LOG_ERROR( m_threadName, "cpd", "failing" );

	if (succeeded)
	{
		string cpdFileName = ComponentProperties::ComposeCpdFileName( componentType, cpdId );
		string cpdFilePath = CPD_DIR;
		cpdFilePath = cpdFilePath + "/";
		cpdFilePath = cpdFilePath + cpdFileName;
		succeeded = ApplyCpdFileToComponent( payloadObj, cpdFilePath, enumComponentTypeStr );
	}
//	if (!succeeded) LOG_ERROR( m_threadName, "cpd", "failing" );

	/// Create status message.
	status = (succeeded) ? STATUS_OK : STATUS_DEVICE_ERROR;
	workMsg->payload = m_desHelper->ComposeStatusString( commandGroupId,
														 commandId,
														 sessionId,
														 messageType,
														 machineId,
														 status,
														 statusPayload );
	/// destination topic becomes "/status".
	workMsg->destination = ReplaceTopicChannel(workMsg->destination, TEXT_STATUS);
	/// NOTICE that we send this to m_statusRouteBox, which sends it to MqttSender.
	m_statusRouteBox->PushMsg(workMsg);
	return true; /// forwarding is complete.
}


int CommandInterpreterThread::ApplyCpdFileToComponent( json_object *payloadObj, std::string & cpdFilePath, std::string & enumComponentTypeStr )
{
	bool succeeded = false;
	int status = STATUS_OK;
	ComponentType componentType = StringNameToComponentTypeEnum( enumComponentTypeStr.c_str() );
	if (FileExists( cpdFilePath ))
	{
		/// Read the file
		json_object * cpdBodyObj = json_object_from_file( cpdFilePath.c_str() );
		succeeded = (cpdBodyObj != NULL);

		/// Read the header and confirm that the enumComponentTypeStr matches.
		struct json_object_iterator it;
		struct json_object_iterator itEnd;
		if (succeeded)
		{
			it = json_object_iter_init_default();
			it = json_object_iter_begin(cpdBodyObj);
			itEnd = json_object_iter_end(cpdBodyObj);
			/// Skip the header.
			json_object_iter_next(&it);
			/// Get the key name of the object containing all properties. It should be an enumComponentType in string form.
			string enumComponentTypeFromFile = "";
			if (!json_object_iter_equal(&it, &itEnd))
			{
				enumComponentTypeFromFile = json_object_iter_peek_name( &it );
				std::transform( enumComponentTypeFromFile.begin(), enumComponentTypeFromFile.end(), enumComponentTypeFromFile.begin(), (int(*)(int)) std::tolower );
				string enumComponentTypeFromParam = enumComponentTypeStr;
				std::transform( enumComponentTypeFromParam.begin(), enumComponentTypeFromParam.end(), enumComponentTypeFromParam.begin(), (int(*)(int)) std::tolower );
				succeeded &= (enumComponentTypeFromFile.compare( enumComponentTypeFromParam ) == 0);
				if (!succeeded)
				{
					stringstream msg;
					msg << "'" << enumComponentTypeFromFile << "' does not match '" << enumComponentTypeFromParam << "'";
					LOG_ERROR( m_threadName, "cpd", msg.str().c_str() );
				}
			}
		}

		json_object * compPropsObj = json_object_object_get( cpdBodyObj, enumComponentTypeStr.c_str() );
		succeeded &= (compPropsObj != NULL);

		/// Find the matching ComponentProperties object and record the current states of all properties that are treated as states.
		/// Remember them past the point of writing to file, so we can send immediate updates to the hardware.
		ComponentProperties * selectedCompProperties = nullptr;
		ComponentProperties originalCompProperties;
		switch (componentType)
		{
			case ComponentType::CxD:
				selectedCompProperties = m_parametersCxD;
				break;
			case ComponentType::YYAxis:
				selectedCompProperties = m_parametersAxisYY;
				break;
			case ComponentType::XXAxis:
				selectedCompProperties = m_parametersAxisXX;
				break;
			case ComponentType::XYAxes:
				selectedCompProperties = m_parametersAxesXY;
				originalCompProperties.fieldVector.push_back(selectedCompProperties->GetFieldByName( Y_SET_SPEED ));
				originalCompProperties.fieldVector.push_back(selectedCompProperties->GetFieldByName( Y_ACCELERATION ));
				originalCompProperties.fieldVector.push_back(selectedCompProperties->GetFieldByName( Y_DECELERATION ));
				originalCompProperties.fieldVector.push_back(selectedCompProperties->GetFieldByName( Y_JOGGING_SPEED ));
				originalCompProperties.fieldVector.push_back(selectedCompProperties->GetFieldByName( Y_JOGGING_ACCELERATION ));
				originalCompProperties.fieldVector.push_back(selectedCompProperties->GetFieldByName( Y_JOGGING_DECELERATION ));
				break;
			case ComponentType::ZAxis:
				selectedCompProperties = m_parametersAxisZ;
				break;
			case ComponentType::ZZAxis:
				selectedCompProperties = m_parametersAxisZZ;
				break;
			case ComponentType::ZZZAxis:
				selectedCompProperties = m_parametersAxisZZZ;
				break;
			case ComponentType::AspirationPump1:
				selectedCompProperties = m_parametersAspirationPump1;
				break;
			case ComponentType::DispensePump1:
				selectedCompProperties = m_parametersDispensePump1;
				break;
			case ComponentType::DispensePump2:
				selectedCompProperties = m_parametersDispensePump2;
				break;
			case ComponentType::DispensePump3:
				selectedCompProperties = m_parametersDispensePump3;
				break;
			case ComponentType::PickingPump1:
				selectedCompProperties = m_parametersPickingPump1;
				break;
			case ComponentType::WhiteLightSource:
				selectedCompProperties = m_parametersWhiteLightSource;
				originalCompProperties.fieldVector.push_back(selectedCompProperties->GetFieldByName( ON_STATUS ));
				break;
			case ComponentType::FluorescenceLightFilter:
				selectedCompProperties = m_parametersFluorescenceLightFilter;
				originalCompProperties.fieldVector.push_back(selectedCompProperties->GetFieldByName( ON_STATUS ));
				originalCompProperties.fieldVector.push_back(selectedCompProperties->GetFieldByName( ACTIVE_FLUORESCENCE_FILTER_POSITION ));
				break;
			case ComponentType::AnnularRingHolder:
				selectedCompProperties = m_parametersAnnularRingHolder;
				originalCompProperties.fieldVector.push_back(selectedCompProperties->GetFieldByName( CURRENT_RING_POSITION ));
				break;
			case ComponentType::OpticalColumn:
				LOG_TRACE( m_threadName, "optical_column", "-" );
				selectedCompProperties = m_parametersOpticalColumn;
				originalCompProperties.fieldVector.push_back(selectedCompProperties->GetFieldByName( ACTIVE_TURRET_POSITION ));
				break;
			case ComponentType::Camera:
				selectedCompProperties = m_parametersCamera;
				originalCompProperties.fieldVector.push_back(selectedCompProperties->GetFieldByName( GAIN ));
				originalCompProperties.fieldVector.push_back(selectedCompProperties->GetFieldByName( EXPOSURE ));
				originalCompProperties.fieldVector.push_back(selectedCompProperties->GetFieldByName( BIN_FACTOR ));
				break;

			// TODO: Add handlers for all of the component types.

			default:
				cout << "WARNING: In StoreCpd, default path taken" << endl;
				selectedCompProperties = nullptr;
				break;
		}
		if (nullptr != selectedCompProperties)
		{
			/// Update the matching ComponentProperties object with the compPropsObj, which was read from the CPD file.
			/// Alsoe, serialize the updated ComponentProperties object to its own .cfg file.
			status =  m_desHelper->CopyValidPropertiesFromJsonToWorkingPropertiesContainer( compPropsObj,
																						   selectedCompProperties,
																						   nullptr,
																						   ExtractionType::UploadCpd,
																						   true ); /// fromCPD

			/// If any properties-that-are-treated-like-states changed, send those changes to a Doler thread to update the hardware.
			switch (componentType)
			{
				case ComponentType::XYAxes:
					SetHardwareStateIfNeeded( ComponentType::XYAxes, &originalCompProperties, selectedCompProperties, Y_SET_SPEED, m_outBoxForCxD );
					SetHardwareStateIfNeeded( ComponentType::XYAxes, &originalCompProperties, selectedCompProperties, Y_ACCELERATION, m_outBoxForCxD );
					SetHardwareStateIfNeeded( ComponentType::XYAxes, &originalCompProperties, selectedCompProperties, Y_DECELERATION, m_outBoxForCxD );
					SetHardwareStateIfNeeded( ComponentType::XYAxes, &originalCompProperties, selectedCompProperties, Y_JOGGING_SPEED, m_outBoxForCxD );
					SetHardwareStateIfNeeded( ComponentType::XYAxes, &originalCompProperties, selectedCompProperties, Y_JOGGING_ACCELERATION, m_outBoxForCxD );
					SetHardwareStateIfNeeded( ComponentType::XYAxes, &originalCompProperties, selectedCompProperties, Y_JOGGING_DECELERATION, m_outBoxForCxD );
					succeeded = true;
					break;
				case ComponentType::WhiteLightSource:
					SetHardwareStateIfNeeded( ComponentType::WhiteLightSource, &originalCompProperties, selectedCompProperties, ON_STATUS, m_outBoxForInfo );
					succeeded = true;
					break;
				case ComponentType::FluorescenceLightFilter:
					SetHardwareStateIfNeeded( ComponentType::FluorescenceLightFilter, &originalCompProperties, selectedCompProperties, ON_STATUS, m_outBoxForInfo );
					SetHardwareStateIfNeeded( ComponentType::FluorescenceLightFilter, &originalCompProperties, selectedCompProperties, ACTIVE_FLUORESCENCE_FILTER_POSITION, m_outBoxForInfo );
					succeeded = true;
					break;
				case ComponentType::AnnularRingHolder:
					SetANRPositionStateIfNeeded( ComponentType::AnnularRingHolder, &originalCompProperties, selectedCompProperties, CURRENT_RING_POSITION, m_outBoxForRingHolder );
					break;
				case ComponentType::OpticalColumn:
					LOG_TRACE( m_threadName, "optical_column", "-" );
					SetTurretStateIfNeeded( ComponentType::OpticalColumn, &originalCompProperties, m_parametersOpticalColumn, ACTIVE_TURRET_POSITION, m_outBoxForOptics );
					succeeded = true;
					break;
				case ComponentType::Camera:
					SetHardwareStateIfNeeded( ComponentType::Camera, &originalCompProperties, selectedCompProperties, GAIN, m_outBoxForOptics );
					SetHardwareStateIfNeeded( ComponentType::Camera, &originalCompProperties, selectedCompProperties, EXPOSURE, m_outBoxForOptics );
					SetHardwareStateIfNeeded( ComponentType::Camera, &originalCompProperties, selectedCompProperties, BIN_FACTOR, m_outBoxForOptics );
					succeeded = true;
					break;
			}
		}
	}
	if ((status == STATUS_OK) && (!succeeded))
	{
		status = STATUS_DEVICE_ERROR;
	}
	return status;
}


bool CommandInterpreterThread::GetFirmwareVersionNumber( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	std::string messageType = "";
	int commandGroupId = 0;
	int commandId = 0;
	std::string sessionId = "";
	std::string machineId = "";
	/// Extract all top-level details except payload.
	bool succeeded = m_desHelper->SplitCommand( jWorkOrderObj,
												&messageType,
												nullptr, /// transportVersion
												&commandGroupId,
												&commandId,
												&sessionId,
												&machineId );
	LETS_ASSERT( succeeded );
	LOG_TRACE( m_threadName, "version", FIRMWARE_VERSION_NUMBER );

	/// Create payload
	json_object *statusPayloadObj = json_object_new_object();
    int result = json_object_object_add( statusPayloadObj,
										 FIRMWARE_VERSION_NUMBER,
										 json_object_new_string( ES_FIRMWARE_VERSION ) );

	/// Create status message.
	int statusCode = (succeeded && (result == 0)) ? STATUS_OK : STATUS_DEVICE_ERROR;
	/// Make Status message and pack the payload in it.
	workMsg->payload = m_desHelper->ComposeStatusString( commandGroupId,
														 commandId,
														 sessionId,
														 messageType,
														 machineId,
														 statusCode,
														 statusPayloadObj );
	/// destination topic becomes "/status".
	workMsg->destination = ReplaceTopicChannel(workMsg->destination, TEXT_STATUS);
	/// Send this to m_statusRouteBox, which sends it to MqttSender.
	m_statusRouteBox->PushMsg(workMsg);
	return true;
}

bool CommandInterpreterThread::GetSystemSerialNumber( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	std::string messageType = "";
	int commandGroupId = 0;
	int commandId = 0;
	std::string sessionId = "";
	std::string machineId = "";
	/// Extract all top-level details except payload.
	bool succeeded = m_desHelper->SplitCommand( jWorkOrderObj,
												&messageType,
												nullptr, /// transportVersion
												&commandGroupId,
												&commandId,
												&sessionId,
												&machineId );
	LETS_ASSERT( succeeded );

	/// Create payload
	string serialNumber = "";
	string snFile = SERIAL_NUMBER_FILE;
	if (FileExists( snFile ))
	{
		std::ifstream in( snFile.c_str() );
		if (in.is_open()) {
			std::getline(in, serialNumber);
		}
		else
		{
			cout << m_threadName << "GetSystemSerialNumber() failed to read file " << snFile << endl;
			succeeded = false;
		}
		in.close();
	}
	else
	{
		serialNumber = "NOT_SET";
	}

	/// Create status message.
	json_object *statusPayloadObj = json_object_new_object();
    int result = json_object_object_add( statusPayloadObj,
										 SERIAL_NUMBER,
										 json_object_new_string( serialNumber.c_str() ) );
	int statusCode = (succeeded && (result == 0)) ? STATUS_OK : STATUS_DEVICE_ERROR;
	/// Make Status message and pack the paylopad in it.
	workMsg->payload = m_desHelper->ComposeStatusString( commandGroupId,
														 commandId,
														 sessionId,
														 messageType,
														 machineId,
														 statusCode,
														 statusPayloadObj );
	/// destination topic becomes "/status".
	workMsg->destination = ReplaceTopicChannel(workMsg->destination, TEXT_STATUS);
	/// Send this to m_statusRouteBox, which sends it to MqttSender.
	m_statusRouteBox->PushMsg(workMsg);
	return true; /// forwarding is complete.
}

bool CommandInterpreterThread::SetSystemSerialNumber( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	std::string messageType = "";
	int commandGroupId = 0;
	int commandId = 0;
	std::string sessionId = "";
	std::string machineId = "";
	/// Extract all top-level details except payload.
	bool succeeded = m_desHelper->SplitCommand( jWorkOrderObj,
												&messageType,
												nullptr, /// transportVersion
												&commandGroupId,
												&commandId,
												&sessionId,
												&machineId );

	/// Parse and save the list of parameters.
	int statusCode = STATUS_OK;
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		json_object * snObj = json_object_object_get( payloadObj, SERIAL_NUMBER );
		if (snObj)
		{
			string serialNumber = json_object_get_string( snObj );
			string snFile = SERIAL_NUMBER_FILE;
			std::fstream out( snFile.c_str(), std::ios::out );
			{
				if (out.is_open())
				out << serialNumber << endl;
				out.close();
			}
		}
	}
	else
	{
		cout << m_threadName << ": " << "payload NOT found." << endl;
		statusCode = STATUS_REJECTED;
		succeeded = false;
	}

	/// Make Status message with null payload in it.
	workMsg->payload = m_desHelper->ComposeStatusString( commandGroupId,
														 commandId,
														 sessionId,
														 messageType,
														 machineId,
														 statusCode,
														 nullptr );
	/// destination topic becomes "/status".
	workMsg->destination = ReplaceTopicChannel(workMsg->destination, TEXT_STATUS);
	/// Send this to m_statusRouteBox, which sends it to MqttSender.
	m_statusRouteBox->PushMsg(workMsg);
	return true; /// forwarding is complete.
}

bool CommandInterpreterThread::GetCurrentSystemConfiguration( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg )
{
	std::string messageType = "";
	int commandGroupId = 0;
	int commandId = 0;
	std::string sessionId = "";
	std::string machineId = "";
	/// Extract all top-level details except payload.
	bool succeeded = m_desHelper->SplitCommand( jWorkOrderObj,
												&messageType,
												nullptr, /// transportVersion
												&commandGroupId,
												&commandId,
												&sessionId,
												&machineId );
	succeeded &= ( succeeded );
	/// Create payload
	int result = 0;
	json_object *statusPayloadObj = json_object_new_object();
	if (result == 0)
	{
		json_object *systemConfigObj = json_object_new_object();
		std::chrono::system_clock::rep timeInSec = TimeSinceEpoch();
		result = json_object_object_add( systemConfigObj, TIMESTAMP, json_object_new_int64( timeInSec ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, FIRMWARE_VERSION_NUMBER, json_object_new_string( ES_FIRMWARE_VERSION ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_CXD, GetCurrentComponentConfiguration( ComponentType::CxD ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_XYAXES, GetCurrentComponentConfiguration( ComponentType::XYAxes ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_XXAXIS, GetCurrentComponentConfiguration( ComponentType::XXAxis ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_YYAXIS, GetCurrentComponentConfiguration( ComponentType::YYAxis ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_ZAXIS, GetCurrentComponentConfiguration( ComponentType::ZAxis ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_ZZAXIS, GetCurrentComponentConfiguration( ComponentType::ZZAxis ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_ZZZAXIS, GetCurrentComponentConfiguration( ComponentType::ZZZAxis ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_ASPIRATION_PUMP_1, GetCurrentComponentConfiguration( ComponentType::AspirationPump1 ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_DISPENSE_PUMP_1, GetCurrentComponentConfiguration( ComponentType::DispensePump1 ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_DISPENSE_PUMP_2, GetCurrentComponentConfiguration( ComponentType::DispensePump2 ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_DISPENSE_PUMP_3, GetCurrentComponentConfiguration( ComponentType::DispensePump3 ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_PICKING_PUMP_1, GetCurrentComponentConfiguration( ComponentType::PickingPump1 ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_WHITE_LIGHT_SOURCE, GetCurrentComponentConfiguration( ComponentType::WhiteLightSource ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_FLUORESCENCE_LIGHT_FILTER, GetCurrentComponentConfiguration( ComponentType::FluorescenceLightFilter ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_ANNULAR_RING_HOLDER, GetCurrentComponentConfiguration( ComponentType::AnnularRingHolder ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_OPTICAL_COLUMN, GetCurrentComponentConfiguration( ComponentType::OpticalColumn ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_CAMERA, GetCurrentComponentConfiguration( ComponentType::Camera ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_INCUBATOR, GetCurrentComponentConfiguration( ComponentType::Incubator ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_CXPM, nullptr );
//		result = json_object_object_add( systemConfigObj, ENUM_CXPM, GetCurrentComponentConfiguration( ComponentType::CxPM ) );
		succeeded &= ( result == 0 );
		result = json_object_object_add( systemConfigObj, ENUM_BARCODE_READER, GetCurrentComponentConfiguration( ComponentType::BarcodeReader ) );
		succeeded &= ( result == 0 );

		result = json_object_object_add( statusPayloadObj,
										 SYSTEM_CONFIGURATION,
										 systemConfigObj );
		succeeded &= (result == 0);
	}

	/// Make Status message and pack the paylopad in it.
	workMsg->payload = m_desHelper->ComposeStatusString( commandGroupId,
														 commandId,
														 sessionId,
														 messageType,
														 machineId,
														 STATUS_OK, /// statusCode
														 statusPayloadObj );
	/// destination topic becomes "/status".
	workMsg->destination = ReplaceTopicChannel(workMsg->destination, TEXT_STATUS);
	/// Send this to m_statusRouteBox, which sends it to MqttSender.
	m_statusRouteBox->PushMsg(workMsg);
	return true;
}

/// Called by GetCurrentSystemConfiguration().
/// Current implementation creates a JSON object containing the component properties, identical to
/// those provided by the Get<component>Properties commands. But it is possible that "current system
/// configuration" might get redefined in the future. It could, for instance, include information about
/// component states. So I am maintaining this separately from GetProperties().
json_object * CommandInterpreterThread::GetCurrentComponentConfiguration( ComponentType type )
{
	json_object *configOpj = nullptr;
	bool succeeded = true;
	int result = 0;
	switch(type)
	{
		case ComponentType::CxD:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersCxD, configOpj );
			break;
		case ComponentType::XYAxes:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersAxesXY, configOpj );
			break;
		case ComponentType::XXAxis:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersAxisXX, configOpj );
			break;
		case ComponentType::YYAxis:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersAxisYY, configOpj );
			break;
		case ComponentType::ZAxis:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersAxisZ, configOpj );
			break;
		case ComponentType::ZZAxis:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersAxisZZ, configOpj );
			break;
		case ComponentType::ZZZAxis:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersAxisZZZ, configOpj );
			break;
		case ComponentType::AspirationPump1:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersAspirationPump1, configOpj );
			break;
		case ComponentType::DispensePump1:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersDispensePump1, configOpj );
			break;
		case ComponentType::DispensePump2:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersDispensePump2, configOpj );
			break;
		case ComponentType::DispensePump3:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersDispensePump3, configOpj );
			break;
		case ComponentType::PickingPump1:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersPickingPump1, configOpj );
			break;
		case ComponentType::WhiteLightSource:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersWhiteLightSource, configOpj );
			break;
		case ComponentType::FluorescenceLightFilter:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersFluorescenceLightFilter, configOpj );
			break;
		case ComponentType::AnnularRingHolder:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersAnnularRingHolder, configOpj );
			break;
		case ComponentType::Camera:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersCamera, configOpj );
			break;
		case ComponentType::OpticalColumn:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersOpticalColumn, configOpj );
			break;
		case ComponentType::Incubator:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersIncubator, configOpj );
			break;
		case ComponentType::CxPM:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersCxPM, configOpj );
			break;
		case ComponentType::BarcodeReader:
			configOpj = json_object_new_object();
			succeeded =  m_desHelper->ComponentPropertiesToJsonObject( m_parametersBarcodeReader, configOpj );
			break;
		case ComponentType::ComponentUnknown:
			configOpj = json_object_new_object();
			/// Return an empty object.
			break;
	}
	return configOpj;
}

bool CommandInterpreterThread::GetProperties( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg, ComponentProperties * p )
{
	std::string messageType = "";
	int commandGroupId = 0;
	int commandId = 0;
	std::string sessionId = "";
	std::string machineId = "";
	/// Extract all top-level details except payload.
	bool succeeded = m_desHelper->SplitCommand( jWorkOrderObj,
												&messageType,
												nullptr, /// transportVersion
												&commandGroupId,
												&commandId,
												&sessionId,
												&machineId );
//	cout << m_threadName << ": " << "GetZPoperties parsed "<< messageType << " for messageType" << endl;
	LETS_ASSERT( succeeded );
	/// Create payload
	int result = 0;
	json_object *statusPayloadObj = json_object_new_object();
	if (result == 0)
	{
		json_object *propertiesJsonObj = json_object_new_object();
		ComponentType type = p->componentType;
		string typeName = ComponentTypeEnumToStringName( type );
		if (result == 0)
		{
			/// all properties from a ComponentProperties structure.
			succeeded &=  m_desHelper->ComponentPropertiesToJsonObject( p, propertiesJsonObj );
			if (succeeded)
			{
//				cout << m_threadName << ": Succeded" << endl;
			}
			else
			{
				cout << m_threadName << ": ERROR: GetProperties() Failed" << endl;
			}
		}
		result = json_object_object_add( statusPayloadObj,
										 typeName.c_str(),
										 propertiesJsonObj );
		LETS_ASSERT(result == 0);
	}

	/// Make Status message and pack the paylopad in it.
	workMsg->payload = m_desHelper->ComposeStatusString( commandGroupId,
														 commandId,
														 sessionId,
														 messageType,
														 machineId,
														 STATUS_OK, /// statusCode
														 statusPayloadObj );
	/// destination topic becomes "/status".
	workMsg->destination = ReplaceTopicChannel(workMsg->destination, TEXT_STATUS);
	/// Send this to m_statusRouteBox, which sends it to MqttSender.
	m_statusRouteBox->PushMsg(workMsg);
	return true;
}

bool CommandInterpreterThread::SetProperties( json_object *jWorkOrderObj, std::shared_ptr<WorkMsg> workMsg, ComponentProperties * p )
{
	std::string messageType = "";
	int commandGroupId = 0;
	int commandId = 0;
	std::string sessionId = "";
	std::string machineId = "";
	/// Extract all top-level details except payload.
	bool succeeded = m_desHelper->SplitCommand( jWorkOrderObj,
												&messageType,
												nullptr, /// transportVersion
												&commandGroupId,
												&commandId,
												&sessionId,
												&machineId );
	LETS_ASSERT( succeeded );
	/// Parse and save the list of parameters.
	int statusCode = STATUS_OK;
	json_object * payloadObj = json_object_object_get( jWorkOrderObj, PAYLOAD );
	if (payloadObj)
	{
		LOG_TRACE( m_threadName, "set-properties", "payload found" );
		if (json_object_object_length( payloadObj ) > 0)
		{
			statusCode = m_desHelper->CopyValidPropertiesPayloadFromJsonToWorkingPropertiesContainer( payloadObj,
																									  p,
																									  nullptr,
																									  ExtractionType::SetProperties,
																									  false ); /// fromCPD

			// TOOD: Delete this temp code.
			string payloadStr = json_object_to_json_string(payloadObj);
			cout << payloadStr << endl;
		}
		else
		{
			LOG_ERROR( m_threadName, "set-properties", "payload is empty" );
			statusCode = STATUS_REJECTED;
			succeeded = false;
		}
	}
	else
	{
		LOG_ERROR( m_threadName, "set-properties", "payload NOT found" );
		statusCode = STATUS_REJECTED;
		succeeded = false;
	}

	/// Make Status message with null payload in it.
	workMsg->payload = m_desHelper->ComposeStatusString( commandGroupId,
														 commandId,
														 sessionId,
														 messageType,
														 machineId,
														 statusCode,
														 nullptr );
	/// destination topic becomes "/status".
	workMsg->destination = ReplaceTopicChannel(workMsg->destination, TEXT_STATUS);
	/// Send this to m_statusRouteBox, which sends it to MqttSender.
	m_statusRouteBox->PushMsg(workMsg);
	return true;
}

bool CommandInterpreterThread::SetHardwareState( ComponentType componentType,
												 std::string propertyName,
												 std::string dataType,
												 std::string propertyValue,
												 MsgQueue * destinationQueue,
												 json_object *payloadObj )
{
	bool succeeded = true;
	json_object *noteToSelf_jobj = json_object_new_object();
	{
		stringstream msg;
		msg << "property name=" << propertyName << "; dataType=" << dataType << "; value=" << propertyValue;
		LOG_TRACE( m_threadName, "-", msg.str().c_str() );
	}
	/// Add content to the COMMAND object.
	int result = json_object_object_add( noteToSelf_jobj, MESSAGE_TYPE, json_object_new_string( SET_HARDWARE_STATE ) );
	succeeded &= (result == 0);

	if (payloadObj == nullptr)
	{
		payloadObj = json_object_new_object();
	}
	string compType = ComponentTypeEnumToStringName( componentType );
	result = json_object_object_add( payloadObj, COMPONENT_TYPE, json_object_new_string( compType.c_str() ) );
	succeeded &= (result == 0);
	result = json_object_object_add( payloadObj, DATA_TYPE, json_object_new_string( dataType.c_str() ) );
	succeeded &= (result == 0);
	result = json_object_object_add( payloadObj, PROPERTY_NAME, json_object_new_string( propertyName.c_str() ) );
	succeeded &= (result == 0);
	result = json_object_object_add( payloadObj, PROPERTY_VALUE, json_object_new_string( propertyValue.c_str() ) );
	succeeded &= (result == 0);

	result = json_object_object_add( noteToSelf_jobj, PAYLOAD, payloadObj );
	succeeded &= (result == 0);

	if (succeeded)
	{
		/// Create a thread message to the Doler thread. It will be deleted there.
		/// This is NOT meant to go out via MQTT.
		std::shared_ptr<WorkMsg> noteToSelf =
			make_shared<WorkMsg>( json_object_to_json_string(noteToSelf_jobj),	// payloadInJson
								  "",					// originTopic
								  "",					// destinationTopic
								  SET_HARDWARE_STATE,	// messageType
								  "",					// subMessageType
								  true );				// theEnd
		destinationQueue->PushMsg( noteToSelf );
	}
	else
	{
		cout << m_threadName << ": ERROR! SetHardwareState() failed." << endl;
	}
	return succeeded;
}
