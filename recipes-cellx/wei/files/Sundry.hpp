///----------------------------------------------------------------------
/// Sundry
/// This is a catch-all file for anything that does not yet warrant
/// its own file.
///----------------------------------------------------------------------
#ifndef _SUNDRY_H
#define _SUNDRY_H

#include "spdlog/spdlog.h"
#include "spdlog/logger.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/rotating_file_sink.h"
#include "es_des.h"

void InitializeSundry();
bool FileExists( const std::string& name );

std::chrono::system_clock::rep TimeSinceEpoch();
std::string NowAsYyyyMnDdHhMmSs();
#define ES_FIRMWARE_VERSION "0.1.29" /// major.minor.patch

/// A topic has two parts--a device and a channel. eg "tbd/machine" + "/" + "channel"
/// ReplaceTopicChannel: old-device + "/" + old-channel -> old-device + "/" + new-channel
std::string ReplaceTopicChannel(std::string topic, std::string channel);
/// ReplaceTopicDevice: old-device + "/" + old-channel -> new-device + "/" + old-channel
std::string ReplaceTopicDevice(std::string topic, std::string device);
/// old-device + "/" + old-channel -> old-channel
std::string ExtractChannel(std::string topic);

std::string DetectHostIpAddress( void );

long StringToLong( std::string str );
unsigned long StringToULong( std::string str );
float StringToFloat( std::string str );
void RemoveBrackets( std::string & str );

void StrictlyAssert(const char* fileName, const int lineNumber, bool assertion);
void LenientlyAssert(const char* fileName, const int lineNumber, bool assertion);
#ifdef DEBUG
#define LETS_ASSERT( expression ) StrictlyAssert( __FILE__, __LINE__, expression );
#else
#define LETS_ASSERT( expression ) LenientlyAssert( __FILE__, __LINE__, expression );
#endif // DEBUG

enum class SeverityType {
	Undefined,
	Trace,
	Debug,
	Info,
	Warn,
	Error,
	Fatal
};

void LogAnything(	const char* programName,
					const char* threadName,
					const char* subsystemName,
					const char* severityName,
					const char* message,
					const char* funcName,
					const char* srcFileName,
					int srcLineNumber,
					SeverityType enumSeverity,
					std::string optional ); // See https://www.geeksforgeeks.org/variadic-function-templates-c/
					
void SetLogSuppressionSeverity( std::string severityToSuppress );

/* Maybe get variadic params working, if needed.
template<typename Arg>
std::string ConcatParameters2(Arg param);
template<typename Arg, typename... Arguments>
std::string ConcatParameters2(Arg param, Arguments... parameters);
template<typename... Arguments>
std::string ConcatParameters(Arguments... parameters);
*/
std::string ConcatParameters(const char * param);
std::string ConcatParameters();

#define LOG_TRACE( thread, subsystem, message, ...) LogAnything( "wes", thread, subsystem, "TRACE", message, __func__, __FILE__, __LINE__, SeverityType::Trace, ConcatParameters( __VA_ARGS__ ) )
#define LOG_DEBUG( thread, subsystem, message, ...) LogAnything( "wes", thread, subsystem, "DEBUG", message, __func__, __FILE__, __LINE__, SeverityType::Debug, ConcatParameters( __VA_ARGS__ ) )
#define LOG_INFO( thread, subsystem, message, ...) LogAnything( "wes", thread, subsystem, "INFO", message, __func__, __FILE__, __LINE__, SeverityType::Info, ConcatParameters( __VA_ARGS__ ) )
#define LOG_WARN( thread, subsystem, message, ...) LogAnything( "wes", thread, subsystem, "WARN", message, __func__, __FILE__, __LINE__, SeverityType::Warn, ConcatParameters( __VA_ARGS__ ) )
#define LOG_ERROR( thread, subsystem, message, ...) LogAnything( "wes", thread, subsystem, "ERROR", message, __func__, __FILE__, __LINE__, SeverityType::Error, ConcatParameters( __VA_ARGS__ ) )
#define LOG_FATAL( thread, subsystem, message, ...) LogAnything( "wes", thread, subsystem, "FATAL", message, __func__, __FILE__, __LINE__, SeverityType::Fatal, ConcatParameters( __VA_ARGS__ ) )

enum class HoldType {
	NoHold_RunFree,
	HoldButFinishCurrentWork,
	HoldUp_Freeze
};

std::string GetEnv( const std::string & var );

long GetFileSize( const std::string & filepath );

#define Abs(x)	((x) < 0 ? -(x) : (x))
#define Max(a, b)	((a) > (b) ? (a) : (b))

#endif 
