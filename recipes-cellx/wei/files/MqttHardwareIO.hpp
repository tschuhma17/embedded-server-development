///----------------------------------------------------------------------
/// MqttHardwareIO (Message Queue Telemetry Transport Receiver)
/// This file provides an interface to a hardware component managed by
/// an external MQTT client.
///----------------------------------------------------------------------
#ifndef _MQTT_HARDWARE_IO_H
#define _MQTT_HARDWARE_IO_H


#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <list>
#include "DesHelper.hpp"
#include "MQTTClient.h"
#include "Sundry.hpp"
#include "uuid/uuid.h"
#include "enums.hpp"

#define SENDER_CLIENTID_MICRO   "ES-MQTT-microscope-io"
#define BROKER_PORT      		"1883"
#define KEEP_ALIVE_INTERVAL 	200
#define CLEAN_SESSION    		1
#define QOS              		1
#define MQTT_HW_IO_TIMEOUT          		100000L
#define HW_REPLY_TIMEOUT_SEC	300

struct ContextBucket {
	bool ackReceived;
	int ack;
	bool statusReceived;
	std::string statusMsg;
	int statusCode;
	json_object * statusPayloadCopyObj;
	std::string commandMessageType;
};
extern ContextBucket g_context;

class MqttHardwareIO
{
public:
	/// Constructor
	MqttHardwareIO( std::string & ipBrokerIpAddress );

	/// Destructor
	~MqttHardwareIO();

	/// Best to call this before calling ConnectToBroker()
	void InitializeListOfTopics( ComponentType componentType );

	/// Best to call this before calling CommandTheHardwareClient()
	bool ConnectToBroker();

	bool IsConnectedToBroker();
	
	/// Sends desCallMessage(in) to the hardware client.
	/// Receives an ack and status messages or times out.
	/// If ack received, returns ackReceived(out)=true and the ack(out)=ack.
	/// if ack not received, returns ackReceived(out)=false and ack(out)=0.
	/// If status received, returns the status code. messageType(in) is used to identify a status message.
	int CommandTheHardwareClient( std::string & messageTypeIn,
								  std::string & desCallMessageIn,
								  std::string & hardwareClientTopicNameIn,
								  bool & ackReceivedOut,
								  int & ackOut,
								  json_object ** jpayloadObjOut );
	
private:
	MqttHardwareIO(const MqttHardwareIO&) = delete;
	MqttHardwareIO& operator=(const MqttHardwareIO&) = delete;

	int SendCommand( std::string & desCallMessageIn, std::string & hardwareClientTopicNameIn );

	/// For MQTT communications
	std::string m_brokerIpAddress;
	bool m_connectedToBroker;
	MQTTClient m_client;
	MQTTClient_connectOptions m_conn_opts;
	MQTTClient_message m_pubmsg;
	MQTTClient_deliveryToken m_token;
    std::list<std::string> m_listOfTopics;
};

#endif 

