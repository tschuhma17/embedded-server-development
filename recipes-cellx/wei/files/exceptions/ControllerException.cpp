/* src/exceptions/ControllerException.cpp */

#include "exceptions/ControllerException.hpp"

namespace USAFW {

    ControllerException::ControllerException(const char *fileP, const char *functionP, int lineP) :
        MotionException(fileP, functionP, lineP)
    {
        ;
    }

    ControllerException::~ControllerException() {
        ;
    }

    std::string ControllerException::getTypeDesc(void) const {
        return std::string("Controller");
    }

    std::string ControllerException::getEventDesc(void) const {
        return std::string("A motion controller exception has occurred.");
    }


    /* == Operation Timed Out == */
    OperationTimedOutException::OperationTimedOutException(const char *fileP, const char *functionP, int lineP) :
        ControllerException(fileP, functionP, lineP)
    {
        ;
    }

    OperationTimedOutException::~OperationTimedOutException() {
        ;
    }

    std::string OperationTimedOutException::getTypeDesc(void) const {
        return std::string("BadConfiguration");
    }

    std::string OperationTimedOutException::getEventDesc(void) const {
        return std::string("The configuration file could not be found or read, is incomplete, or is invalid.");
    }


    /* == PValue Not Monitored == */
    PValueNotMonitoredException::PValueNotMonitoredException(const char *fileP, const char *functionP, int lineP) :



        ControllerException(fileP, functionP, lineP)
    {
        ;
    }

    PValueNotMonitoredException::~PValueNotMonitoredException() {
        ;
    }

    std::string PValueNotMonitoredException::getTypeDesc(void) const {
        return std::string("PValueNotMonitored");
    }

    std::string PValueNotMonitoredException::getEventDesc(void) const {
        return std::string("The PValue index requested has not been configured with monitor().");
    }


    /* == PValue Pending (but not yet received) == */
    PValuePendingException::PValuePendingException(const char *fileP, const char *functionP, int lineP) :
        ControllerException(fileP, functionP, lineP)
    {
        ;
    }

    PValuePendingException::~PValuePendingException() {
        ;
    }

    std::string PValuePendingException::getTypeDesc(void) const {
        return std::string("PValuePending");
    }

    std::string PValuePendingException::getEventDesc(void) const {
        return std::string("The PValue is being monitored, but no response has been received yet.");
    }


    /* == PValue Wrong Data Type (e.g., latch is a float but you asked for a long) == */
    PValueWrongDataTypeException::PValueWrongDataTypeException(const char *fileP, const char *functionP, int lineP) :
        ControllerException(fileP, functionP, lineP)
    {
        ;
    }

    PValueWrongDataTypeException::~PValueWrongDataTypeException() {
        ;
    }

    std::string PValueWrongDataTypeException::getTypeDesc(void) const {
        return std::string("PValueWrongDataType");
    }

    std::string PValueWrongDataTypeException::getEventDesc(void) const {
        return std::string("The PValue is being monitored, but is not of the requested data type.");
    }

}
