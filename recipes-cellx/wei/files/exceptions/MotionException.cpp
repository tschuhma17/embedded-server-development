/* src/exceptions/MotionException.cpp */

#include "exceptions/MotionException.hpp"

namespace USAFW {

    MotionException::MotionException(const char *fileP, const char *functionP, int lineP) :
        InstrumentedException(fileP, functionP, lineP)
    {
        ;
    }

    MotionException::~MotionException() {
        ;
    }


    /* == BAD CONFIGURATION == */
    BadConfigurationException::BadConfigurationException(const char *fileP, const char *functionP, int lineP) :
        MotionException(fileP, functionP, lineP)
    {
        ;
    }

    BadConfigurationException::~BadConfigurationException() {
        ;
    }

    std::string BadConfigurationException::getTypeDesc(void) const {
        return std::string("BadConfiguration");
    }

    std::string BadConfigurationException::getEventDesc(void) const {
        return std::string("The configuration file could not be found or read, is incomplete, or is invalid.");
    }


    /* == BAD STATE == */
    BadStateException::BadStateException(const char *fileP, const char *functionP, int lineP) :
        MotionException(fileP, functionP, lineP)
    {
        ;
    }

    BadStateException::~BadStateException() {
        ;
    }

    std::string BadStateException::getTypeDesc(void) const {
        return std::string("BadState");
    }

    std::string BadStateException::getEventDesc(void) const {
        return std::string("The system is in an inappropriate state for the operation attempted.");
    }


    /* == EMERGENCY STOP == */
    EmergencyStopException::EmergencyStopException(const char *fileP, const char *functionP, int lineP) :
        MotionException(fileP, functionP, lineP)
    {
        ;
    }

    EmergencyStopException::~EmergencyStopException() {
        ;
    }

    std::string EmergencyStopException::getTypeDesc(void) const {
        return std::string("EmergencyStop");
    }

    std::string EmergencyStopException::getEventDesc(void) const {
        return std::string("The system has executed an emergency stop and must be fully restarted.");
    }


}
