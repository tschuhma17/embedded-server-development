/* ControllerException.hpp */

#ifndef __ControllerException_hpp__
#define __ControllerException_hpp__

#include <string>

#include "controllers/MotionController.hpp"
#include "exceptions/MotionException.hpp"

namespace USAFW {

    class ControllerException : public MotionException {
    public:
        // Constructor
        ControllerException(const char *fileP, const char *functionP, int LineP);

        // Destructor
        virtual ~ControllerException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;

        // Exceptions for controllers return the controller name as the supplemental context description.
        // TODO - requires a better way to supply the controller
        //std::string getSupplementalContextDesc(void);

    protected:
        //MotionController &controller;
    };


    class OperationTimedOutException : public ControllerException{
    public:
        // Constructor
        OperationTimedOutException(const char *fileP, const char *functionP, int LineP);

        // Destructor
        virtual ~OperationTimedOutException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;        
    };


    class PValueNotMonitoredException : public ControllerException{
    public:
        // Constructor
        PValueNotMonitoredException(const char *fileP, const char *functionP, int LineP);

        // Destructor
        virtual ~PValueNotMonitoredException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };


    class PValuePendingException : public ControllerException{
    public:
        // Constructor
        PValuePendingException(const char *fileP, const char *functionP, int LineP);

        // Destructor
        virtual ~PValuePendingException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };


    class PValueWrongDataTypeException : public ControllerException{
    public:
        // Constructor
        PValueWrongDataTypeException(const char *fileP, const char *functionP, int LineP);

        // Destructor
        virtual ~PValueWrongDataTypeException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };

}

#endif
