/* exceptions/MotionException.hpp */

#ifndef __MotionException_hpp__
#define __MotionException_hpp__

#include <exception>
#include <string>

#include "USAFW/exceptions/InstrumentedException.hpp"
#include "USAFW/util/threadutils.hpp"

namespace USAFW {

    /* Base class for Motion Services Excepetions. */
    // TODO FIXME - This should really derive from std::exception
    class MotionException : public InstrumentedException {
    public:
        // Constructor
        MotionException(const char *fileP, const char *functionP, int Line);

        // Destructor
        virtual ~MotionException();

        // Gets a description of the exception type.
        virtual std::string getTypeDesc(void) const=0;

        // Gets a human-readable name for the supplemental context.
        //virtual std::string getSupplementalContextDesc(void) const;

        // Prints a meaningful description of this specific exception event.
        virtual std::string getEventDesc(void) const=0;

    };


    // This exception is thrown when a method is called that has not yet been implemented.
    class BadConfigurationException : public MotionException{
    public:
        // Constructor
        BadConfigurationException(const char *fileP, const char *functionP, int lineP);

        // Destructor
        virtual ~BadConfigurationException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };

    // This exception is thrown when an operation is attempted that is inappropriate for
    // the current state of the system, such as attempting to join an axis to a transport
    // when it is already joined to one.
    class BadStateException : public MotionException{
    public:
        // Constructor
        BadStateException(const char *fileP, const char *functionP, int lineP);

        // Destructor
        virtual ~BadStateException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };

    // This exception is thrown when any command other than a value or state inquiry is performed after an emergency stop.
    // Emergency stops apply to ALL connected controllers, and ALL connected transports.
    class EmergencyStopException : public MotionException{
    public:
        // Constructor
        EmergencyStopException(const char *fileP, const char *functionP, int LineP);

        // Destructor
        virtual ~EmergencyStopException();

        std::string getTypeDesc(void) const;
        std::string getEventDesc(void) const;
    };
}

#endif
