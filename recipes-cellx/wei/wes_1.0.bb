DESCRIPTION = "Worker of Embedded Server"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS += "paho-mqtt-c"
DEPENDS += "json-c"
DEPENDS += "util-linux"
DEPENDS += "pvcam pvcamddi libes-hw"
DEPENDS += "pywrap"
RDEPENDS_${PN} += "pvcam pvcamddi"
RDEPENDS_${PN} += "pywrap"
JSON_C_DIR = "${TOPDIR}/tmp/sysroots-components/aarch64/json-c/usr"
SPDLOG_DIR = "${TOPDIR}/tmp/sysroots-components/aarch64/spdlog/usr"
PYWRAP_DIR = "${TOPDIR}/tmp/sysroots-components/aarch64/pywrap/usr"

SRC_URI =  " \
			file://base64.cpp \
			file://base64.h \
			file://CommandDoler.cpp \
			file://CommandDoler.hpp \
			file://CommandInterpreter.cpp \
			file://CommandInterpreter.hpp \
			file://Components.cpp \
			file://Components.hpp \
			file://controllers \
			file://DesHelper.cpp \
			file://DesHelper.hpp \
			file://enums.cpp \
			file://enums.hpp \
			file://es_des.h \
			file://es_controller.h \
			file://es_parts.json \
			file://es_hardware.json \
			file://es_hardware_etaluma.json \
			file://es_hardware_olympus.json \
			file://es_hardware_reference.json \
			file://exceptions \
			file://Fault.cpp \
			file://Fault.hpp \
			file://Inventory.cpp \
			file://Inventory.hpp \
			file://main.hpp \
			file://MotionAxis.cpp \
			file://MotionAxis.hpp \
			file://MotionServices.cpp \
			file://MotionServices.hpp \
			file://MotionServices-config.json \
			file://MqttHardwareIO.cpp \
			file://MqttHardwareIO.hpp \
			file://MqttReceiver.cpp \
			file://MqttReceiver.hpp \
			file://MqttSender.cpp \
			file://MqttSender.hpp \
			file://MsgQueue.cpp \
			file://MsgQueue.hpp \
			file://PlateType.cpp \
			file://PlateType.hpp \
			file://Rochacha.raw \
			file://stdafx.cpp \
			file://stdafx.h \
			file://Sundry.cpp \
			file://Sundry.hpp \
			file://targetver.h \
			file://TipType.cpp \
			file://TipType.hpp \
			file://Transport.cpp \
			file://Transport.hpp \
			file://USAFW_TARGET_CONFIG.h \
			file://uscope_ut_main.cpp \
			file://wes.cpp \
			file://wes.service \
			file://controllers/ACR7000.cpp \
			file://controllers/ACR7000.hpp \
			file://controllers/MotionController.cpp \
			file://controllers/MotionController.hpp \
			file://exceptions/ControllerException.cpp \
			file://exceptions/ControllerException.hpp \
			file://exceptions/MotionException.cpp \
			file://exceptions/MotionException.hpp \
			file://thirdparty/json11/json11.cpp \
			file://thirdparty/json11/json11.hpp \
			file://include/USAFW/details.h \
			file://include/USAFW/sizes.h \
			file://USAFW/SLED.cpp \
			file://USAFW/SLED.hpp \
			file://USAFW/SLED_POST.cpp \
			file://USAFW/SLED_POST.hpp \
			file://USAFW/debugging/CodePosition.cpp \
			file://include/USAFW/debugging/CodePosition.hpp \
			file://USAFW/debugging/Debuggable.cpp \
			file://include/USAFW/debugging/Debuggable.hpp \
			file://USAFW/exceptions/CommExceptions.cpp \
			file://USAFW/exceptions/CommExceptions.hpp \
			file://USAFW/exceptions/InstrumentedException.cpp \
			file://USAFW/exceptions/InstrumentedException.hpp \
			file://USAFW/exceptions/LogicExceptions.cpp \
			file://USAFW/exceptions/LogicExceptions.hpp \
			file://USAFW/exceptions/MemoryExceptions.cpp \
			file://USAFW/exceptions/MemoryExceptions.hpp \
			file://USAFW/exceptions/SecurityExceptions.cpp \
			file://USAFW/exceptions/SecurityExceptions.hpp \
			file://USAFW/memory/Guarded.cpp \
			file://USAFW/memory/Guarded.hpp \
			file://USAFW/memory/guarded_buffer.cpp \
			file://USAFW/memory/guarded_buffer.hpp \
			file://USAFW/memory/ProperBuffer.cpp \
			file://USAFW/memory/ProperBuffer.hpp \
			file://USAFW/memory/RingBuffer.cpp \
			file://USAFW/memory/RingBuffer.hpp \
			file://USAFW/ProperComms/ProperCommBase.cpp \
			file://USAFW/ProperComms/ProperCommBase.hpp \
			file://USAFW/ProperComms/ProperCommFactory.hpp \
			file://USAFW/ProperComms/TCPLink.cpp \
			file://USAFW/ProperComms/TCPLink.hpp \
			file://USAFW/sync/Semaphore.cpp \
			file://USAFW/sync/Semaphore.hpp \
			file://USAFW/timing/timeout.h \
			file://USAFW/util/Lockable.cpp \
			file://USAFW/util/Lockable.hpp \
			file://USAFW/util/Logger.cpp \
			file://USAFW/util/Logger.hpp \
			file://USAFW/util/math.hpp \
			file://USAFW/util/MeasurementUnit.cpp \
			file://USAFW/util/MeasurementUnit.hpp \
			file://USAFW/util/SoftFuse.cpp \
			file://USAFW/util/SoftFuse.hpp \
			file://USAFW/util/stringutils.cpp \
			file://USAFW/util/stringutils.hpp \
			file://USAFW/util/threadutils.cpp \
			file://USAFW/util/threadutils.hpp \
			file://CppLinuxSerial/Exception.hpp \
			file://CppLinuxSerial/SerialPort.cpp \
			file://CppLinuxSerial/SerialPort.hpp \
			file://Tecan_ADP.hpp\
			file://Tecan_ADP.cpp\
			file://ShellInvoker.cpp \
			file://ShellInvoker.hpp \
			file://popen_hand.py \
			file://popen_snap.py \
			file://es_camera.hpp \
			file://camera_retiga.hpp \
			file://camera_retiga.cpp \
			file://PVCam_helper.h \
		   "

# SRC = "stdafx.cpp enums.cpp TipType.cpp PlateType.cpp MsgQueue.cpp MqttSender.cpp MqttReceiver.cpp CommandInterpreter.cpp CommandDoler.cpp DesHelper.cpp Fault.cpp Inventory.cpp Sundry.cpp Components.cpp main.cpp base64.cpp MqttHardwareIO.cpp MotionServices.cpp SerialPort.cpp Tecan_ADP.cpp camera_retiga.cpp"

SRC =  " \
		base64.cpp CommandDoler.cpp CommandInterpreter.cpp Components.cpp DesHelper.cpp enums.cpp \
		Fault.cpp Inventory.cpp MotionAxis.cpp MotionServices.cpp MqttHardwareIO.cpp MqttReceiver.cpp \
		MqttSender.cpp MsgQueue.cpp PlateType.cpp stdafx.cpp Sundry.cpp TipType.cpp Transport.cpp \
		wes.cpp controllers/ACR7000.cpp controllers/MotionController.cpp exceptions/ControllerException.cpp \
		exceptions/MotionException.cpp thirdparty/json11/json11.cpp USAFW/SLED.cpp USAFW/SLED_POST.cpp \
		USAFW/debugging/CodePosition.cpp USAFW/debugging/Debuggable.cpp USAFW/exceptions/CommExceptions.cpp \
		USAFW/exceptions/InstrumentedException.cpp USAFW/exceptions/LogicExceptions.cpp \
		USAFW/exceptions/MemoryExceptions.cpp USAFW/exceptions/SecurityExceptions.cpp USAFW/memory/Guarded.cpp \
		USAFW/memory/guarded_buffer.cpp USAFW/memory/ProperBuffer.cpp USAFW/memory/RingBuffer.cpp \
		USAFW/ProperComms/ProperCommBase.cpp USAFW/ProperComms/TCPLink.cpp USAFW/sync/Semaphore.cpp \
		USAFW/util/Lockable.cpp USAFW/util/Logger.cpp USAFW/util/MeasurementUnit.cpp USAFW/util/SoftFuse.cpp \
		USAFW/util/stringutils.cpp USAFW/util/threadutils.cpp CppLinuxSerial/SerialPort.cpp Tecan_ADP.cpp \
		ShellInvoker.cpp camera_retiga.cpp \
		"

# USCOPE = "uscope_ut_main.cpp MqttHardwareIO.cpp DesHelper.cpp Sundry.cpp Components.cpp PlateType.cpp TipType.cpp enums.cpp"

S = "${WORKDIR}"
CXXFLAGS += " -g "
LDFLAGS += " -Llibs/aarch64/ -L${JSON_C_DIR}/lib"
LDFLAGS += " -L${PYWRAP_DIR}/lib -lPyWrap"
LIBS += " -ldl -lpthread -lrt "
LIBS += " -luuid"
LIBS += " ${WORKDIR}/recipe-sysroot/usr/lib/es-hw.a"
LIBS += " -lpvcam -lpvcamDDI"
LIBS += " -ljson-c"
LIBS += " -lpaho-mqtt3c"
INCFLAGS += " -I${JSON_C_DIR}/include/json-c -I${SPDLOG_DIR}/include"
INCFLAGS += " -I${WORKDIR}"
INCFLAGS += " -I${WORKDIR}/include"
INCFLAGS += " -I${WORKDIR}/recipe-sysroot/usr/include"

do_compile() {
    echo "Wes is compiling."
	${CXX} -O0 ${SRC} ${INCFLAGS} ${CXXFLAGS} ${LDFLAGS} -o wes ${LIBS}
#	${CXX} -O0 ${USCOPE} ${INCFLAGS} ${CXXFLAGS} ${LDFLAGS} -o uscope ${LIBS}
}

do_install() {
    echo "Wes is installing"
	install -d ${D}${bindir}
	echo "${D}${bindir}"
	ls ${D}${bindir}
	install -m 0755 wes ${D}${bindir}
#	install -m 0755 uscope ${D}${bindir}
	install -d ${D}${includedir}
	install -m 0755 ${WORKDIR}/*.h ${D}${includedir}
	install -d ${D}${datadir}/cfgs
	echo "${D}${datadir}/cfgs"
	install -m 0644 es_parts.json ${D}${datadir}/cfgs/
	install -m 0644 es_hardware.json ${D}${datadir}/cfgs/
	install -m 0644 es_hardware_etaluma.json ${D}${datadir}/cfgs/
	install -m 0644 es_hardware_olympus.json ${D}${datadir}/cfgs/
	install -m 0644 es_hardware_reference.json ${D}${datadir}/cfgs/
	install -m 0644 MotionServices-config.json ${D}${datadir}/cfgs/
	install -d ${D}${datadir}/pics
	install -m 0644 Rochacha.raw ${D}${datadir}/pics/
	install -m 0644 popen_hand.py ${D}${bindir}
	install -m 0644 popen_snap.py ${D}${bindir}
}

FILES_${PN} += " \
                ${includedir}/*.h \
                ${datadir}/pics \
                ${datadir}/cfgs \
"
FILES_${PN}-dev = " \
				${includedir} \
"

