
REALPN = "cellx-platemover"

LICENSE = "CLOSED"

S = "${WORKDIR}/${REALPN}-${PV}"

PREBUILD = "${TOPDIR}/../layers/meta-cellx/prebuilt_dotnet"

do_install() {
    install -d "${D}${bindir}/PlateMover"
    install -m 0755 ${PREBUILD}/PlateMover/*.exe ${D}${bindir}/PlateMover
    install -m 0755 ${PREBUILD}/PlateMover/*.dll ${D}${bindir}/PlateMover
    install -m 0644 ${PREBUILD}/PlateMover/*.xml ${D}${bindir}/PlateMover
}
